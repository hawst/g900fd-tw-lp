.class Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;
.super Ljava/lang/Object;
.source "PlaylistView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 82
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 83
    .local v2, "item":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v4

    .line 85
    .local v4, "video_id":J
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)I

    move-result v3

    if-ne v3, v7, :cond_3

    .line 86
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iget v3, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    if-ne v3, v7, :cond_2

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v3, v4, v5, p3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->playVideo(JI)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$100(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/widget/BaseAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 132
    :cond_0
    :goto_0
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_1

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v3, :cond_a

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateActionBarBtn()V

    .line 140
    :cond_1
    :goto_1
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 145
    :goto_2
    return-void

    .line 90
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v6

    # invokes: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->callPlayer(JI)V
    invoke-static {v3, v4, v5, v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$200(Lcom/sec/android/app/mv/player/playlist/PlaylistView;JI)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 95
    :cond_3
    const v3, 0x7f0d002b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 96
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 98
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_5

    .line 99
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_6

    .line 100
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v3

    if-ne v3, p3, :cond_4

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 103
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p3, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 99
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 109
    .end local v1    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v6, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-direct {v6, p3, v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-boolean v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v3, :cond_7

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iput-boolean v7, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    .line 117
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p3, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 121
    :cond_6
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_9

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v3, :cond_8

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    goto/16 :goto_0

    .line 115
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iput-boolean v8, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    goto :goto_4

    .line 124
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v3, :cond_0

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    goto/16 :goto_0

    .line 127
    :cond_9
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v3, :cond_0

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBar(I)V

    goto/16 :goto_0

    .line 135
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v3, :cond_1

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBarBtn()V

    goto/16 :goto_1

    .line 143
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_2
.end method
