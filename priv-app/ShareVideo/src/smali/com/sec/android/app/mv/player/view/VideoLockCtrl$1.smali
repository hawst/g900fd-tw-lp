.class Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;
.super Ljava/lang/Object;
.source "VideoLockCtrl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 52
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 67
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 57
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->access$000(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->access$000(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
