.class Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
.super Ljava/lang/Thread;
.source "ShareVideoMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenChordServiceThread"
.end annotation


# instance fields
.field handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;Landroid/os/Handler;)V
    .locals 1
    .param p2, "mHandler"    # Landroid/os/Handler;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 139
    iput-object p2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->handler:Landroid/os/Handler;

    .line 140
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->setDaemon(Z)V

    .line 141
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 144
    const-string v1, "ShareVideoMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "run E. mServerIP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$000(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mTimeout : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mTimeout:Z
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$100(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->handler:Landroid/os/Handler;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 147
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mTimeout:Z
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$100(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$000(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 149
    const-string v1, "ShareVideoMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ListenChordServiceThread. mServerIP is : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$000(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->interrupt()V

    .line 161
    return-void

    .line 154
    :cond_1
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
