.class Lcom/sec/android/app/mv/player/view/VideoBtnController$20;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field IsFirstTouchOnProgressbar:Z

.field position:J

.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1721
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1727
    if-eqz p3, :cond_0

    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1751
    :cond_0
    :goto_0
    return-void

    .line 1733
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->reprocessProgress(I)I

    move-result p2

    .line 1736
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J

    move-result-wide v0

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x186a0

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->position:J

    .line 1737
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1739
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->IsFirstTouchOnProgressbar:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1740
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->IsFirstTouchOnProgressbar:Z

    .line 1745
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1746
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-wide v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->position:J

    long-to-int v2, v2

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3100(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1749
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 1742
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->position:J

    long-to-int v1, v2

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v4, 0x1

    .line 1776
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1777
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1792
    :goto_0
    return-void

    .line 1780
    :cond_0
    invoke-virtual {p1}, Landroid/widget/SeekBar;->invalidate()V

    .line 1781
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$902(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z

    .line 1782
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v1

    int-to-long v2, v1

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2902(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J

    .line 1783
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->IsFirstTouchOnProgressbar:Z

    .line 1791
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 1754
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "onStopTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1756
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgress(I)V

    .line 1773
    :goto_0
    return-void

    .line 1760
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$902(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z

    .line 1762
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "onStopTrackingTouch local play"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->position:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    .line 1771
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgress()I

    .line 1772
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0
.end method
