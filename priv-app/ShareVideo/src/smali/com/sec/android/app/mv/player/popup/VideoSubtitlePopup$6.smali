.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleSizePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 601
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 603
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 605
    packed-switch p2, :pswitch_data_0

    .line 622
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->refreshSubtitleMenu()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1600(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 623
    return-void

    .line 607
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextSize(I)V

    goto :goto_0

    .line 611
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextSize(I)V

    goto :goto_0

    .line 615
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextSize(I)V

    goto :goto_0

    .line 605
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
