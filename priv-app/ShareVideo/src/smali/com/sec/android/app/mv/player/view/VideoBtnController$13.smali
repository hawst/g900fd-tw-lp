.class Lcom/sec/android/app/mv/player/view/VideoBtnController$13;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1190
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1192
    const/4 v1, 0x0

    .line 1193
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1194
    sparse-switch p2, :sswitch_data_0

    .line 1212
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$600(Lcom/sec/android/app/mv/player/view/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1213
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 1231
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 1197
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1199
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1202
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1203
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1216
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 1218
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1219
    goto :goto_0

    .line 1222
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1223
    goto :goto_0

    .line 1194
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 1197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1216
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
