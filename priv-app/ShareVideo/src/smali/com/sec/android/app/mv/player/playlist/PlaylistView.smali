.class public abstract Lcom/sec/android/app/mv/player/playlist/PlaylistView;
.super Landroid/widget/RelativeLayout;
.source "PlaylistView.java"


# static fields
.field public static mLastPlayedID:J


# instance fields
.field private TAG:Ljava/lang/String;

.field protected emptyData:Landroid/widget/RelativeLayout;

.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mContext:Landroid/content/Context;

.field protected mGridView:Landroid/widget/GridView;

.field private mHandler:Landroid/os/Handler;

.field public mIsEnterDeleteModeListView:Z

.field protected mListView:Landroid/widget/ListView;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field protected mOrientation:I

.field protected mPlaylistAttr:I

.field private mPlaylistType:I

.field protected mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mRoot:Landroid/view/View;

.field private mSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mLastPlayedID:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "attr"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 240
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 46
    const-class v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->TAG:Ljava/lang/String;

    .line 64
    iput-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    .line 72
    iput v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    .line 76
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    .line 78
    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView$1;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 151
    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 241
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 243
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 245
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030010

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mRoot:Landroid/view/View;

    .line 246
    iput p2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    .line 247
    iput p3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mRoot:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->addView(Landroid/view/View;)V

    .line 250
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->initCustomActionBar()V

    .line 252
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->updateOrientation()V

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mRoot:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOnItemLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->PlaylistListViewInit(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 256
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mHandler:Landroid/os/Handler;

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    .line 260
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/widget/BaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/playlist/PlaylistView;JI)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    .param p1, "x1"    # J
    .param p3, "x2"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->callPlayer(JI)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private callPlayer(JI)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "position"    # I

    .prologue
    .line 476
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 477
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 478
    const-string v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 479
    const-string v1, "uri"

    sget-object v2, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 481
    return-void
.end method

.method private getPlaylistItem(Landroid/database/Cursor;)Lcom/sec/android/app/mv/player/type/PlaylistItem;
    .locals 19
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 535
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 536
    .local v14, "idIndex":I
    const-string v2, "title"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 537
    .local v18, "titleIndex":I
    const-string v2, "_data"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 538
    .local v11, "dataIndex":I
    const-string v2, "duration"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 539
    .local v13, "durationIndex":I
    const-string v2, "resumePos"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 540
    .local v15, "resumePosIndex":I
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 541
    .local v3, "_id":J
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 542
    .local v5, "title":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 543
    .local v8, "path":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 544
    .local v12, "durText":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 546
    .local v6, "duration":J
    if-eqz v12, :cond_0

    .line 547
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 550
    :cond_0
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 552
    .local v16, "resumePos":J
    new-instance v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    const/4 v9, 0x0

    move-wide/from16 v0, v16

    long-to-int v10, v0

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/mv/player/type/PlaylistItem;-><init>(JLjava/lang/String;JLjava/lang/String;II)V

    return-object v2
.end method

.method private getPlaylistItem(Lcom/sec/android/app/mv/player/type/PlaylistItem;)Lcom/sec/android/app/mv/player/type/PlaylistItem;
    .locals 9
    .param p1, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 556
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v1

    .line 557
    .local v1, "_id":J
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 558
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v4

    .line 559
    .local v4, "duration":J
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 560
    .local v6, "path":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v8

    .line 562
    .local v8, "resumePos":I
    new-instance v0, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/mv/player/type/PlaylistItem;-><init>(JLjava/lang/String;JLjava/lang/String;II)V

    return-object v0
.end method

.method private initCustomActionBar()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method private updateOrientation()V
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOrientation:I

    .line 227
    return-void
.end method


# virtual methods
.method public abstract PlaylistListViewInit(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemLongClickListener;)V
.end method

.method public addOrRemoveSelection(Landroid/view/View;I)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 212
    const/4 v1, 0x0

    .line 213
    .local v1, "isSelectedBefore":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v2

    if-ne v2, p2, :cond_2

    .line 215
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 216
    const/4 v1, 0x1

    .line 220
    :cond_0
    if-nez v1, :cond_1

    .line 221
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    invoke-direct {v4, p2, v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_1
    return-void

    .line 213
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public changeAllState(Z)V
    .locals 9
    .param p1, "checked"    # Z

    .prologue
    .line 358
    if-eqz p1, :cond_5

    .line 359
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isCursorAdapter()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 362
    const/4 v3, 0x0

    .line 363
    .local v3, "mVideoCursor":Landroid/database/Cursor;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    instance-of v6, v6, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    if-eqz v6, :cond_0

    .line 364
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v6, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 367
    :cond_0
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 368
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 369
    const/4 v5, 0x0

    .line 372
    .local v5, "pos":I
    :cond_1
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getPlaylistItem(Landroid/database/Cursor;)Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v8

    invoke-direct {v7, v5, v8}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    add-int/lit8 v5, v5, 0x1

    .line 378
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 397
    .end local v3    # "mVideoCursor":Landroid/database/Cursor;
    .end local v5    # "pos":I
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v6, :cond_3

    .line 398
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    .line 400
    :cond_3
    return-void

    .line 373
    .restart local v3    # "mVideoCursor":Landroid/database/Cursor;
    .restart local v5    # "pos":I
    :catch_0
    move-exception v0

    .line 374
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 381
    .end local v0    # "e":Landroid/database/CursorIndexOutOfBoundsException;
    .end local v3    # "mVideoCursor":Landroid/database/Cursor;
    .end local v5    # "pos":I
    :cond_4
    const/4 v5, 0x0

    .line 382
    .restart local v5    # "pos":I
    const/4 v2, 0x0

    .line 384
    .local v2, "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    .line 385
    .local v4, "pl":Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 387
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 388
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 389
    .restart local v2    # "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getPlaylistItem(Lcom/sec/android/app/mv/player/type/PlaylistItem;)Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v8

    invoke-direct {v7, v5, v8}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 394
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    .end local v2    # "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    .end local v4    # "pl":Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    .end local v5    # "pos":I
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method protected getColumnsCount()I
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x1

    .line 232
    .local v0, "n":I
    iget v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mOrientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 233
    const/4 v0, 0x2

    .line 236
    :cond_0
    return v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method public getPlaylistType()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    return v0
.end method

.method public getSelectedList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public initAdapter()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const v5, 0x7f030014

    const v2, 0x7f03000d

    const/4 v1, 0x1

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isCursorAdapter()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    if-ne v0, v1, :cond_0

    .line 277
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    iget v5, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;IILcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setAdapter(Landroid/widget/BaseAdapter;)V

    .line 289
    return-void

    .line 279
    :cond_0
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    move v2, v5

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    goto :goto_0

    .line 282
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    if-ne v0, v1, :cond_2

    .line 283
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    goto :goto_0

    .line 285
    :cond_2
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2, p0}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    goto :goto_0
.end method

.method public initStartView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 292
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    if-nez v2, :cond_0

    .line 329
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->TAG:Ljava/lang/String;

    const-string v3, "initStartView() E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "lastPlayedItem"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 300
    .local v0, "lastFile":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v2

    sput-wide v2, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mLastPlayedID:J

    .line 304
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "firstVisibleItem"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 306
    .local v1, "selectIndex":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 307
    const/4 v1, 0x0

    .line 310
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    instance-of v2, v2, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    if-eqz v2, :cond_3

    .line 311
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setSelection(I)V

    .line 314
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v2

    if-eqz v2, :cond_5

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->emptyData:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_4

    .line 316
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->emptyData:Landroid/widget/RelativeLayout;

    .line 328
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setEmptyView()V

    goto :goto_0

    .line 319
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->emptyData:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_4

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0036

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->emptyData:Landroid/widget/RelativeLayout;

    .line 321
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-eqz v2, :cond_6

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0037

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 324
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->emptyData:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public isAllSelected()Z
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 404
    const/4 v0, 0x1

    .line 406
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllUnSelected()Z
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 412
    const/4 v0, 0x1

    .line 414
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCursorAdapter()Z
    .locals 2

    .prologue
    .line 463
    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 464
    :cond_0
    const/4 v0, 0x1

    .line 466
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isListEmpty()Z
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const/4 v0, 0x0

    .line 422
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSelectableListType()Z
    .locals 2

    .prologue
    .line 471
    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public listRefreshForCheck()V
    .locals 2

    .prologue
    .line 186
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    .line 196
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_4

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateActionBarBtn()V

    .line 204
    :cond_1
    :goto_1
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionMode()Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 209
    :goto_2
    return-void

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    goto :goto_0

    .line 192
    :cond_3
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBar(I)V

    goto :goto_0

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBarBtn()V

    goto :goto_1

    .line 207
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_2
.end method

.method public listgetCount()I
    .locals 2

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isCursorAdapter()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 428
    const/4 v0, 0x0

    .line 430
    .local v0, "mVideoCursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    instance-of v1, v1, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v1, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 434
    :cond_0
    if-eqz v0, :cond_1

    .line 435
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 440
    .end local v0    # "mVideoCursor":Landroid/database/Cursor;
    :goto_0
    return v1

    .line 437
    .restart local v0    # "mVideoCursor":Landroid/database/Cursor;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 440
    .end local v0    # "mVideoCursor":Landroid/database/Cursor;
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView$3;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 339
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 149
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->saveState()V

    .line 272
    return-void
.end method

.method protected playVideo(JI)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "pos"    # I

    .prologue
    const/4 v3, 0x1

    .line 445
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 447
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 453
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getResumePos(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setResumePosition(J)V

    .line 457
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    .line 460
    return-void

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v3, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    goto :goto_0
.end method

.method public removeDeletedIdsFromSelectedList()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 496
    const/4 v1, 0x0

    .line 498
    .local v1, "uri":Landroid/net/Uri;
    const/4 v11, 0x0

    .line 499
    .local v11, "plType":Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;
    const/4 v8, 0x0

    .line 501
    .local v8, "cur":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 502
    .local v9, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 505
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "plType":Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;
    check-cast v11, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    .line 506
    .restart local v11    # "plType":Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;
    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getItem()Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v6

    .line 507
    .local v6, "_id":J
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 510
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 511
    :cond_1
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 514
    :cond_2
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 516
    const/4 v8, 0x0

    goto :goto_0

    .line 520
    .end local v6    # "_id":J
    :cond_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 524
    :cond_4
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_5

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_6

    .line 526
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    .line 532
    :cond_5
    :goto_1
    return-void

    .line 527
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v0, :cond_5

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    goto :goto_1
.end method

.method public abstract saveState()V
.end method

.method public abstract setAdapter(Landroid/widget/BaseAdapter;)V
.end method

.method public setAdapterListType(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "attr"    # I

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/adapter/PlaylistArrayAdapter;->setListType(II)V

    .line 567
    return-void
.end method

.method public abstract setEmptyView()V
.end method

.method public setListType(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "attr"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    .line 267
    iput p2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    .line 268
    return-void
.end method

.method public abstract setNumberOfColumns()V
.end method

.method public setPlaylistType(I)V
    .locals 0
    .param p1, "listType"    # I

    .prologue
    .line 492
    iput p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I

    .line 493
    return-void
.end method

.method public abstract setSelection(I)V
.end method

.method public swapCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isCursorAdapter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 347
    :cond_0
    return-void
.end method
