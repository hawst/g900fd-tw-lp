.class Lcom/sec/android/app/mv/player/activity/ShareVideo$7;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1599
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 1601
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1602
    .local v0, "action":Ljava/lang/String;
    const-string v6, "ShareVideo"

    const-string v7, "mMVPlaylistReceiver - onReceive"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1604
    const-string v6, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1605
    const-string v6, "ShareVideo"

    const-string v7, "mMVPlaylistReceiver. ACTION_VIDEO_FROM_MV_PLAYLIST"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    const/4 v5, 0x0

    .line 1608
    .local v5, "uri":Landroid/net/Uri;
    const-wide/16 v2, -0x1

    .line 1609
    .local v2, "mResumePosition":J
    const-string v6, "uri"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1611
    if-nez v5, :cond_1

    .line 1632
    .end local v2    # "mResumePosition":J
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 1615
    .restart local v2    # "mResumePosition":J
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    .line 1617
    .local v4, "pl":Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v1

    .line 1619
    .local v1, "idx":I
    if-ltz v1, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v6

    if-lt v1, v6, :cond_3

    .line 1620
    :cond_2
    invoke-virtual {v4, v8}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 1623
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 1624
    invoke-virtual {v4, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getResumePos(I)I

    move-result v6

    int-to-long v2, v6

    .line 1625
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6, v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1627
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1628
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopSubtitle()V

    .line 1629
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    goto :goto_0
.end method
