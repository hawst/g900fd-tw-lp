.class Lcom/sec/android/app/mv/player/activity/ShareVideo$14;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->createErrorDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2342
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x3

    .line 2344
    const/4 v0, 0x0

    .line 2345
    .local v0, "resultOnKey":Z
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3602(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    .line 2347
    sparse-switch p2, :sswitch_data_0

    .line 2377
    :cond_0
    :goto_0
    return v0

    .line 2350
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2351
    const-string v1, "ShareVideo"

    const-string v2, "dialog - KEYCODE_POWER_ACTION_UP"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2353
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2354
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2355
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 2357
    :cond_1
    const/4 v0, 0x1

    .line 2358
    goto :goto_0

    .line 2361
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2362
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2363
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2365
    :cond_2
    const/4 v0, 0x1

    .line 2366
    goto :goto_0

    .line 2369
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2370
    const/4 v0, 0x1

    goto :goto_0

    .line 2347
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x1a -> :sswitch_0
        0x54 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
