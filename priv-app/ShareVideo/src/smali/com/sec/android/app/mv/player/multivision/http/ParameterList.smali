.class public Lcom/sec/android/app/mv/player/multivision/http/ParameterList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ParameterList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/sec/android/app/mv/player/multivision/http/Parameter;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public at(I)Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;

    return-object v0
.end method

.method public getParameter(I)Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;

    return-object v0
.end method

.method public getParameter(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 47
    if-nez p1, :cond_1

    move-object v2, v3

    .line 56
    :cond_0
    :goto_0
    return-object v2

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->size()I

    move-result v1

    .line 51
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 52
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->at(I)Lcom/sec/android/app/mv/player/multivision/http/Parameter;

    move-result-object v2

    .line 53
    .local v2, "param":Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v2    # "param":Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    :cond_2
    move-object v2, v3

    .line 56
    goto :goto_0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->getParameter(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/Parameter;

    move-result-object v0

    .line 61
    .local v0, "param":Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    if-nez v0, :cond_0

    .line 62
    const-string v1, ""

    .line 63
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
