.class Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;
.super Landroid/os/Handler;
.source "ShareVideoMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    .line 166
    const-string v4, "ShareVideoMainActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleMessage. msg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getHandlerMsg(I)Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$300(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 209
    :goto_0
    return-void

    .line 170
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopChordService()V

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mFromLocalVideoPlayer:Z
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callMoviePlayerAsServer()V
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$500(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    .line 178
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->finish()V

    goto :goto_0

    .line 175
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callPlaylist(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;I)V

    goto :goto_1

    .line 182
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopChordService()V

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callMoviePlayerAsClient()V
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$700(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->finish()V

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopChordService()V

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->interrupt()V

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->finish()V

    goto :goto_0

    .line 194
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    const v5, 0x7f0a006c

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-static {v4, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 196
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 197
    .local v2, "toastLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 198
    .local v3, "tv":Landroid/widget/TextView;
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 199
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopChordService()V

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->interrupt()V

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->finish()V

    goto/16 :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
