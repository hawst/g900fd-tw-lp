.class Lcom/sec/android/app/mv/player/view/VideoBtnController$10;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 853
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 864
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 865
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->moveSubtitleDown()V

    .line 866
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setInvisibleAllViews()V

    .line 867
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 861
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 856
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "hide animation start"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z

    .line 858
    return-void
.end method
