.class Lcom/sec/android/app/mv/player/activity/ShareVideo$27;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2908
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 2910
    const-string v0, "ShareVideo"

    const-string v1, "mBluetoothHeadsetServiceListener: onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2911
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v0, p2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4102(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;

    .line 2913
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isBluetoothDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2914
    const-string v0, "ShareVideo"

    const-string v1, "mBluetoothHeadsetServiceListener - BT is connected but path is device"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2915
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    .line 2919
    :goto_0
    return-void

    .line 2917
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "profile"    # I

    .prologue
    .line 2922
    const-string v0, "ShareVideo"

    const-string v1, "mBluetoothHeadsetServiceListener: onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2923
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4102(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;

    .line 2924
    return-void
.end method
