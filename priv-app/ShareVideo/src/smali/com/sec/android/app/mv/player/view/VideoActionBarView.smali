.class public Lcom/sec/android/app/mv/player/view/VideoActionBarView;
.super Landroid/widget/RelativeLayout;
.source "VideoActionBarView.java"


# static fields
.field private static final BACK_SHAREVIDEO_LIST:I = 0x3

.field private static final HOVER_DETECT_TIME_MS:I = 0x12c

.field private static final LAYOUT_ID:I = 0x190

.field private static final MSG_FROM_USER:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VideoActionBarView"

.field private static mPopupCreated:Z

.field private static mShowing:Z


# instance fields
.field private final TO_MULTI_VISION:Z

.field private final TO_SINGLE_VISION:Z

.field private listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

.field private mAcionMoreClickListener:Landroid/view/View$OnClickListener;

.field private mActionBack:Landroid/widget/RelativeLayout;

.field private mActionBackClickListener:Landroid/view/View$OnClickListener;

.field private mActionGroupClickListener:Landroid/view/View$OnClickListener;

.field private mActionGroupLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mActionMoreLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mAddBtn:Landroid/widget/RelativeLayout;

.field private mAddBtnClickListener:Landroid/view/View$OnClickListener;

.field private mAddBtnLayout:Landroid/widget/LinearLayout;

.field private mAddBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mCancelBtn:Landroid/widget/RelativeLayout;

.field private mCancelBtnClickListener:Landroid/view/View$OnClickListener;

.field private mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

.field private mCloseSelectBtn:Landroid/widget/RelativeLayout;

.field private mCloseSelectClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mCounterSelectAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCounterSpinner:Landroid/widget/Spinner;

.field private mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteBtn:Landroid/widget/RelativeLayout;

.field private mDeleteBtnClickListener:Landroid/view/View$OnClickListener;

.field private mDeleteBtnLayout:Landroid/widget/LinearLayout;

.field private mDeleteBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mDoneBtn:Landroid/widget/RelativeLayout;

.field private mDoneBtnClickListener:Landroid/view/View$OnClickListener;

.field private mExitPopup:Landroid/app/AlertDialog;

.field private mGroupCountText:Landroid/widget/TextView;

.field private mGroupListPopup:Landroid/widget/PopupWindow;

.field private mHandler:Landroid/os/Handler;

.field mIsPopupMenuShowing:Z

.field private final mListPopupOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private mListSelect:Landroid/widget/RelativeLayout;

.field private mLongPressChecker:Ljava/lang/Runnable;

.field private mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

.field private mMVSettingSwitchCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mMVSwitchButton:Landroid/widget/ImageView;

.field private mMVSwitchClickListener:Landroid/view/View$OnClickListener;

.field private mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field private final mMenuPopupOnDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mRoot:Landroid/view/View;

.field private mSVSwitchButton:Landroid/widget/ImageView;

.field private mSVSwitchClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 85
    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    .line 86
    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupCreated:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtn:Landroid/widget/RelativeLayout;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCloseSelectBtn:Landroid/widget/RelativeLayout;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mExitPopup:Landroid/app/AlertDialog;

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mIsPopupMenuShowing:Z

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->TO_SINGLE_VISION:Z

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->TO_MULTI_VISION:Z

    .line 501
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mHandler:Landroid/os/Handler;

    .line 548
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$7;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchClickListener:Landroid/view/View$OnClickListener;

    .line 556
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$8;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchClickListener:Landroid/view/View$OnClickListener;

    .line 564
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$9;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBackClickListener:Landroid/view/View$OnClickListener;

    .line 576
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$10;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCloseSelectClickListener:Landroid/view/View$OnClickListener;

    .line 584
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$11;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionGroupClickListener:Landroid/view/View$OnClickListener;

    .line 639
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$12;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionGroupLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 657
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$13;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 671
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$14;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 684
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionMoreLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 700
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$16;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAcionMoreClickListener:Landroid/view/View$OnClickListener;

    .line 739
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$18;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMenuPopupOnDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    .line 749
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$19;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListPopupOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 756
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$20;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitchCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 775
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$21;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnClickListener:Landroid/view/View$OnClickListener;

    .line 784
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$22;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnClickListener:Landroid/view/View$OnClickListener;

    .line 795
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$23;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtnClickListener:Landroid/view/View$OnClickListener;

    .line 803
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$24;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtnClickListener:Landroid/view/View$OnClickListener;

    .line 95
    iput-object p0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    .line 96
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    .line 97
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initFloatingWindow()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mExitPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/mv/player/view/VideoActionBarView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mExitPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/widget/ChannelSwitch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 51
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    return v0
.end method

.method static synthetic access$802(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 51
    sput-boolean p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupCreated:Z

    return p0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v11, 0x5153

    const/16 v10, 0x12c

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 160
    const v3, 0x7f0d0045

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_0

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBackClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v3

    if-nez v3, :cond_5

    .line 166
    const v3, 0x7f0d004c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 167
    .local v2, "sl":Landroid/widget/RelativeLayout;
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 198
    .end local v2    # "sl":Landroid/widget/RelativeLayout;
    :goto_0
    const v3, 0x7f0d004d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 199
    .local v0, "actionGroup":Landroid/widget/RelativeLayout;
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-nez v3, :cond_1

    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v3, :cond_8

    .line 200
    :cond_1
    if-eqz v0, :cond_2

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionGroupClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionGroupLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 204
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v3, :cond_2

    .line 205
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 206
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 207
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 208
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v7, v9}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 212
    :cond_2
    const v3, 0x7f0d004f

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    const-string v4, "01"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "1 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0098

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 219
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->changeActionLayoutForUpgradeModels(Landroid/view/View;)V

    .line 221
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v3, :cond_3

    .line 222
    const v3, 0x7f0d0051

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 223
    .local v1, "actionMore":Landroid/widget/RelativeLayout;
    if-eqz v1, :cond_3

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAcionMoreClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionMoreLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0005

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v3, :cond_3

    .line 228
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 229
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 230
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 231
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v7, v9}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 236
    .end local v1    # "actionMore":Landroid/widget/RelativeLayout;
    :cond_3
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_4

    .line 237
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initPlayerListBtn(Landroid/view/View;)V

    .line 239
    :cond_4
    return-void

    .line 169
    .end local v0    # "actionGroup":Landroid/widget/RelativeLayout;
    :cond_5
    const v3, 0x7f0d001b

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    if-eqz v3, :cond_6

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00a5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 173
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v3, :cond_6

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v7, v9}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 183
    :cond_6
    const v3, 0x7f0d001d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    .line 184
    const v3, 0x7f0d001c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00a3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v3, :cond_7

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 190
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    invoke-virtual {v3, v7, v9}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 195
    :cond_7
    invoke-direct {p0, v7}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setMVSwitch(Z)V

    goto/16 :goto_0

    .line 216
    .restart local v0    # "actionGroup":Landroid/widget/RelativeLayout;
    :cond_8
    const v3, 0x7f0d004d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private initCounterAdapters()V
    .locals 6

    .prologue
    const v5, 0x7f03000e

    const v4, 0x7f03000b

    .line 353
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$3;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$3;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 367
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$4;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$4;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 381
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$5;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$5;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 394
    return-void
.end method

.method private initCounterView()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0049

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCloseSelectBtn:Landroid/widget/RelativeLayout;

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCloseSelectBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCloseSelectClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d004b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    .line 316
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initCounterAdapters()V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 350
    return-void
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "mDecor":Landroid/view/View;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 115
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getRootLayout()Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method private initPlayerListBtn(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x5153

    const/16 v6, 0x12c

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 242
    const v0, 0x7f0d0054

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    .line 243
    const v0, 0x7f0d0057

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    .line 244
    const v0, 0x7f0d005a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    .line 245
    const v0, 0x7f0d0055

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    .line 246
    const v0, 0x7f0d0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    .line 247
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtn:Landroid/widget/RelativeLayout;

    .line 248
    const v0, 0x7f0d005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 258
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 266
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initCounterView()V

    .line 290
    return-void
.end method

.method public static isShowing()Z
    .locals 1

    .prologue
    .line 155
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    return v0
.end method

.method private setMVSwitch(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const v1, 0x3ecccccd    # 0.4f

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    if-eqz v0, :cond_0

    .line 436
    if-eqz p1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setClickable(Z)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setAlpha(F)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitchCheckedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setClickable(Z)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mSVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSwitchButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setAlpha(F)V

    goto :goto_0
.end method

.method private showConfirmDeleteFiles()V
    .locals 6

    .prologue
    const v3, 0x7f0a0020

    const/4 v5, 0x1

    .line 953
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getSelectedItemSize()I

    move-result v1

    .line 954
    .local v1, "size":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 956
    .local v0, "msg":Ljava/lang/String;
    if-ne v1, v5, :cond_2

    .line 957
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0021

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 962
    :cond_0
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupCreated:Z

    if-nez v2, :cond_1

    .line 963
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a001f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0060

    new-instance v4, Lcom/sec/android/app/mv/player/view/VideoActionBarView$28;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$28;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0016

    new-instance v4, Lcom/sec/android/app/mv/player/view/VideoActionBarView$27;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$27;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/mv/player/view/VideoActionBarView$26;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$26;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 985
    sput-boolean v5, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupCreated:Z

    .line 987
    :cond_1
    return-void

    .line 958
    :cond_2
    if-le v1, v5, :cond_0

    .line 959
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public changeDropdownMenu()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 307
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 308
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->isAllUnSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method public forceHide()V
    .locals 1

    .prologue
    .line 858
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    if-eqz v0, :cond_0

    .line 859
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    .line 860
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibility(I)V

    .line 861
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setInvisibleAllViews()V

    .line 862
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    .line 864
    :cond_0
    return-void
.end method

.method public getDeleteBtn()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getDoneBtn()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 832
    sget-boolean v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    if-eqz v1, :cond_0

    .line 833
    const/4 v0, 0x0

    .line 834
    .local v0, "translateOff":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 835
    .restart local v0    # "translateOff":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 836
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 837
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 838
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibility(I)V

    .line 839
    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView$25;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$25;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 853
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    .line 855
    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public hide(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 867
    if-eqz p1, :cond_0

    .line 868
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    .line 869
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibility(I)V

    .line 870
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    .line 872
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 874
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 876
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public hideGrouplistPopup()Z
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 634
    const/4 v0, 0x1

    .line 636
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hidePlayerListBtn()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 430
    return-void
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 139
    const-string v1, "VideoActionBarView"

    const-string v2, "makeControllerView"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 142
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030015

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setPadding()V

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initControllerView(Landroid/view/View;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->initControllerView(Landroid/view/View;)V

    .line 108
    :cond_0
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    .line 883
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->unregisterDeviceObserver(Lcom/sec/android/app/mv/player/view/ParentDeviceListView;)V

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 885
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 886
    return-void
.end method

.method public removeLongPressCallback()V
    .locals 2

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mLongPressChecker:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mLongPressChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 924
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mLongPressChecker:Ljava/lang/Runnable;

    .line 926
    :cond_0
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 121
    const-string v3, "VideoActionBarView"

    const-string v4, "setAnchorView"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->removeAllViews()V

    .line 124
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 127
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 128
    .local v2, "v":Landroid/view/View;
    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 129
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 132
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 134
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 136
    :cond_0
    return-void
.end method

.method public setCounterSpinnerText(I)V
    .locals 7
    .param p1, "size"    # I

    .prologue
    const v6, 0x7f0a0082

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    const v2, 0x7f0d002d

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 294
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCounterSpinner:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 297
    return-void
.end method

.method public setDeleteBtnEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 929
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 931
    return-void
.end method

.method public setDoneBtnEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 934
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v2, 0x7f0d005e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 935
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz p1, :cond_0

    .line 936
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 940
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDoneBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 941
    return-void

    .line 938
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method protected setInvisibleAllViews()V
    .locals 2

    .prologue
    .line 889
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setInvisibleNormalLayout()V

    .line 891
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setNullBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$myView;)V

    .line 893
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    const-string v1, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Ljava/lang/String;)V

    .line 896
    :cond_0
    return-void
.end method

.method protected setInvisibleNormalLayout()V
    .locals 2

    .prologue
    .line 899
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 900
    return-void
.end method

.method public setPadding()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const/16 v1, 0x53

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 152
    :cond_0
    return-void
.end method

.method protected setVisibleAllViews()V
    .locals 1

    .prologue
    .line 903
    sget-object v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibleAllViews(Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;)V

    .line 904
    return-void
.end method

.method protected setVisibleAllViews(Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;)V
    .locals 6
    .param p1, "layout"    # Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0d0048

    const v3, 0x7f0d0047

    const/16 v2, 0x8

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0043

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 908
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 919
    :goto_0
    return-void

    .line 911
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 915
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 814
    sget-boolean v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    if-nez v1, :cond_0

    .line 815
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibleAllViews()V

    .line 818
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->updateActionBarForSV()V

    .line 820
    const/4 v0, 0x0

    .line 821
    .local v0, "translateOn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 822
    .restart local v0    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 823
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 824
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 825
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibility(I)V

    .line 827
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mShowing:Z

    .line 829
    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public showGrouplist()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 595
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 597
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v3, 0x7f030008

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 599
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    if-nez v3, :cond_0

    .line 600
    new-instance v3, Lcom/sec/android/app/mv/player/view/DeviceListView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/mv/player/view/DeviceListView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

    .line 601
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/view/DeviceListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 603
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/common/SUtils;->registerDeviceObserver(Lcom/sec/android/app/mv/player/view/ParentDeviceListView;)V

    .line 605
    const v3, 0x7f0d0018

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 606
    .local v1, "popupListLayout":Landroid/widget/RelativeLayout;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->listView:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 607
    new-instance v3, Landroid/widget/PopupWindow;

    invoke-direct {v3, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    .line 608
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080039

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 610
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->getMVMembersNumber()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_2

    .line 611
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    const/4 v4, -0x2

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 615
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v7}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 616
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 617
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-eqz v3, :cond_3

    .line 618
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 623
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListPopupOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 626
    .end local v1    # "popupListLayout":Landroid/widget/RelativeLayout;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    move-result v3

    if-nez v3, :cond_1

    .line 627
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    const/16 v4, 0x35

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080037

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v3, v2, v4, v7, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 629
    :cond_1
    return-void

    .line 613
    .restart local v1    # "popupListLayout":Landroid/widget/RelativeLayout;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_0

    .line 620
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupListPopup:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020060

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public showPlayerListBtn(I)V
    .locals 3
    .param p1, "listType"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 397
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 422
    :goto_0
    return-void

    .line 403
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 409
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 416
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mActionBack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mListSelect:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mAddBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mDeleteBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mCancelDoneBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public showPopupMenu()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 712
    const-string v1, "VideoActionBarView"

    const-string v2, "showPopupMenu()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    if-nez v1, :cond_0

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0051

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 715
    .local v0, "mMoreMenu":Landroid/widget/RelativeLayout;
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoActionBarView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$17;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMenuPopupOnDismissListener:Landroid/widget/PopupMenu$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 726
    .end local v0    # "mMoreMenu":Landroid/widget/RelativeLayout;
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mIsPopupMenuShowing:Z

    if-ne v1, v5, :cond_1

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    .line 730
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 732
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    .line 734
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mIsPopupMenuShowing:Z

    .line 735
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const v2, 0x36ee80

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 737
    return-void
.end method

.method public updateActionBarForSV()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 465
    const-string v0, "VideoActionBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateActionBarForSV getDeviceSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v0, :cond_1

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0098

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 477
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v0

    if-nez v0, :cond_3

    .line 499
    :goto_1
    return-void

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mGroupCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0099

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 479
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    if-nez v0, :cond_4

    .line 480
    const-string v0, "VideoActionBarView"

    const-string v1, "updateActionBarForSV mMVSettingSwitch is NULL"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 485
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 487
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    .line 493
    :goto_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isRequestMultiVision()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 494
    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setMVSwitch(Z)V

    goto :goto_1

    .line 490
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMVSettingSwitch:Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    goto :goto_2

    .line 496
    :cond_6
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setMVSwitch(Z)V

    goto :goto_1
.end method
