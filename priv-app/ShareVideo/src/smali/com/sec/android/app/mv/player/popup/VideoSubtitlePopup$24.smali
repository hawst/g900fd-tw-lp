.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1120
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1125
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2402(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1128
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 1131
    :cond_1
    return-void
.end method
