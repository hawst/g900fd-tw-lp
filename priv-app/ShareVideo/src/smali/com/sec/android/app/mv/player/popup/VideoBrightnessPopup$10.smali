.class Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    if-nez v2, :cond_0

    .line 472
    :goto_0
    return v0

    .line 438
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v1

    .line 472
    goto :goto_0

    .line 440
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 443
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 444
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 446
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 450
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 451
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 454
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 456
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v2, :cond_6

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v2, :cond_6

    .line 457
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1300(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 458
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 460
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 463
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 438
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
