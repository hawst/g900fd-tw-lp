.class public final Lcom/sec/android/app/mv/player/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final bottom_padding_progress_view_dp:I = 0x7f090000

.field public static final current_time_text_height:I = 0x7f090001

.field public static final current_time_text_width:I = 0x7f090002

.field public static final gesture_max_range:I = 0x7f090003

.field public static final hover_image_gap:I = 0x7f090004

.field public static final left_padding_progress_view_dp:I = 0x7f090005

.field public static final max_thumb_image_height_land_dp:I = 0x7f090006

.field public static final max_thumb_image_height_port_dp:I = 0x7f090007

.field public static final max_thumb_image_width_land_dp:I = 0x7f090008

.field public static final max_thumb_image_width_port_dp:I = 0x7f090009

.field public static final micro_bubble_rightMargin_port:I = 0x7f09000a

.field public static final micro_bubble_width_land:I = 0x7f09000b

.field public static final micro_bubble_width_port:I = 0x7f09000c

.field public static final progress_margin:I = 0x7f09000d

.field public static final right_padding_progress_view_dp:I = 0x7f09000e

.field public static final subtitle_control_height:I = 0x7f09000f

.field public static final subtitle_height_port:I = 0x7f090010

.field public static final subtitle_large_size:I = 0x7f090011

.field public static final subtitle_margin_from_bottom:I = 0x7f090012

.field public static final subtitle_medium_size:I = 0x7f090013

.field public static final subtitle_small_size:I = 0x7f090014

.field public static final thumbnail_landscape_count:I = 0x7f090015

.field public static final top_padding_progress_view_dp:I = 0x7f090016


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
