.class Lcom/sec/android/app/mv/player/activity/ShareVideo$4;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1343
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1346
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1347
    .local v0, "action":Ljava/lang/String;
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mWifiConnetionReceiver E. action : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    const-string v10, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1350
    const-string v10, "networkInfo"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/NetworkInfo;

    .line 1351
    .local v7, "nwInfo":Landroid/net/NetworkInfo;
    if-nez v7, :cond_1

    .line 1397
    .end local v7    # "nwInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 1354
    .restart local v7    # "nwInfo":Landroid/net/NetworkInfo;
    :cond_1
    sget-object v10, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1355
    const-string v10, "ShareVideo"

    const-string v11, "NetworkInfo.State.DISCONNECTED"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifi:Landroid/net/wifi/WifiManager;
    invoke-static {v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/net/wifi/WifiManager;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v9

    .line 1359
    .local v9, "wmMethods":[Ljava/lang/reflect/Method;
    move-object v1, v9

    .local v1, "arr$":[Ljava/lang/reflect/Method;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v6, v1, v3

    .line 1360
    .local v6, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v6}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "isWifiApEnabled"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1361
    const/4 v4, 0x0

    .line 1363
    .local v4, "isWifiAPenalbed":Z
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifi:Landroid/net/wifi/WifiManager;
    invoke-static {v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/net/wifi/WifiManager;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v6, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    .line 1372
    :goto_2
    if-nez v4, :cond_0

    .line 1373
    const-string v10, "ShareVideo"

    const-string v11, "isWifiAPenalbed == false"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 1364
    :catch_0
    move-exception v2

    .line 1365
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    .line 1366
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 1367
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    .line 1368
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 1369
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_2

    .line 1359
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v4    # "isWifiAPenalbed":Z
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1381
    .end local v1    # "arr$":[Ljava/lang/reflect/Method;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "method":Ljava/lang/reflect/Method;
    .end local v9    # "wmMethods":[Ljava/lang/reflect/Method;
    :cond_3
    sget-object v10, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1382
    const-string v10, "ShareVideo"

    const-string v11, "NetworkInfo.State.UNKNOWN"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1389
    .end local v7    # "nwInfo":Landroid/net/NetworkInfo;
    :cond_4
    const-string v10, "android.net.wifi.WIFI_APSTATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1390
    const-string v10, "wifi_state"

    const/4 v11, 0x4

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 1392
    .local v8, "state":I
    const/16 v10, 0xa

    if-ne v8, v10, :cond_0

    .line 1393
    const-string v10, "ShareVideo"

    const-string v11, "mWifiConnetionReceiver. WifiManager.WIFI_AP_STATE_DISABLING"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1394
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto/16 :goto_0
.end method
