.class public Lcom/sec/android/app/mv/player/playlist/PlayerListView;
.super Landroid/widget/RelativeLayout;
.source "PlayerListView.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoPlayerListView"

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ADD:I

.field private final DEL:I

.field final HANDLE_NOTIFY_DATA_CHANGED:I

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mContext:Landroid/content/Context;

.field private mCounterLayout:Landroid/widget/RelativeLayout;

.field private mCounterText:Landroid/widget/TextView;

.field private mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

.field private mPlayerListView:Landroid/widget/RelativeLayout;

.field private final mPlaylistReceiver:Landroid/content/BroadcastReceiver;

.field private mPlaylistType:I

.field private mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field mProgressLayout:Landroid/widget/LinearLayout;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 188
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$1;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;I)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "listType"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 53
    iput v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->HANDLE_NOTIFY_DATA_CHANGED:I

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 62
    iput v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->ADD:I

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->DEL:I

    .line 230
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;-><init>(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 68
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    .line 69
    iput p2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    .line 70
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->restartLoader()V

    return-void
.end method

.method private extractItem(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    sget-object v2, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mComparator:Ljava/util/Comparator;

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 202
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getItem()Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 204
    :cond_0
    return-object v0
.end method

.method private extractPos(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    sget-object v2, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mComparator:Ljava/util/Comparator;

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 212
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    :cond_0
    return-object v0
.end method

.method private initCounter()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d001f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterLayout:Landroid/widget/RelativeLayout;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterText:Landroid/widget/TextView;

    .line 288
    return-void
.end method

.method private registerBroadcastReciever()V
    .locals 3

    .prologue
    .line 254
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 255
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 259
    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 260
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 263
    return-void
.end method

.method private resetPlayerLayout()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 372
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 374
    .local v0, "resetMainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 377
    .local v1, "resetSurfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hidePlayerListBtn()V

    .line 379
    return-void
.end method

.method private restartLoader()V
    .locals 3

    .prologue
    .line 330
    const-string v0, "VideoPlayerListView"

    const-string v1, "restartLoader() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 332
    return-void
.end method

.method private sendUpdatedPlaylist()V
    .locals 5

    .prologue
    .line 120
    const-wide/16 v0, 0x0

    .line 121
    .local v0, "mCurPlayID":J
    const/4 v2, -0x1

    .line 123
    .local v2, "mCurPlayIndex":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoDbId()J

    move-result-wide v0

    .line 126
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getIndexOf(J)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 127
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v2

    .line 128
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 132
    :cond_0
    return-void
.end method

.method private setImageCacheListener()V
    .locals 1

    .prologue
    .line 335
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;-><init>(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V

    invoke-static {v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setOnUpdatedListener(Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;)V

    .line 344
    return-void
.end method

.method private setListView()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 96
    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->initCounter()V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0021

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 104
    .local v0, "layout":Landroid/widget/RelativeLayout;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    return-void
.end method

.method private unRegisterBroadcastReciever()V
    .locals 3

    .prologue
    .line 266
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 268
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updatePlayerLayout(Z)V
    .locals 14
    .param p1, "isLandScape"    # Z

    .prologue
    .line 382
    const/4 v6, 0x0

    .line 383
    .local v6, "mainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, 0x0

    .line 384
    .local v3, "listLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v10, 0x0

    .line 385
    .local v10, "surfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x0

    .line 386
    .local v8, "padding":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08016e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 388
    .local v0, "actionBarHeight":I
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 389
    .local v1, "dm":Landroid/util/DisplayMetrics;
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 391
    if-eqz p1, :cond_1

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08017a

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 393
    .local v4, "listWidth":I
    iget v11, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v7, v11, v4

    .line 395
    .local v7, "mainWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080178

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 398
    .local v9, "surfaceHeight":I
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v6    # "mainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, -0x1

    invoke-direct {v6, v7, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 400
    .restart local v6    # "mainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v3    # "listLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, -0x1

    invoke-direct {v3, v4, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 402
    .restart local v3    # "listLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v10    # "surfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {v10, v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 405
    .restart local v10    # "surfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v3, v11, v0, v12, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 406
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v0, v12, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 407
    const/16 v11, 0xb

    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 408
    const/16 v11, 0x9

    invoke-virtual {v3, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 409
    const/16 v11, 0xa

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 429
    .end local v4    # "listWidth":I
    .end local v7    # "mainWidth":I
    :goto_0
    if-eqz v6, :cond_0

    if-eqz v3, :cond_0

    .line 430
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v11, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 431
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 432
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v11, v11, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 433
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v8, v12, v8, v13}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 434
    iget-object v11, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setNumberOfColumns()V

    .line 437
    :cond_0
    return-void

    .line 411
    .end local v9    # "surfaceHeight":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08016c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 412
    .local v2, "listHeight":I
    iget v11, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v5, v11, v2

    .line 413
    .local v5, "mainHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080179

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 416
    .restart local v9    # "surfaceHeight":I
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v6    # "mainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, -0x1

    invoke-direct {v6, v11, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 418
    .restart local v6    # "mainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v3    # "listLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, -0x1

    invoke-direct {v3, v11, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 420
    .restart local v3    # "listLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v10    # "surfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, -0x1

    invoke-direct {v10, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 423
    .restart local v10    # "surfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v0, v12, v13}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 424
    const/16 v11, 0xa

    invoke-virtual {v6, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 425
    const/16 v11, 0xc

    invoke-virtual {v3, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 426
    const/16 v11, 0xa

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0
.end method

.method private updatePlaylist(ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p2, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    const-string v0, "VideoPlayerListView"

    const-string v1, "updatePlaylist E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    if-nez p1, :cond_0

    .line 221
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->extractItem(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->add(Ljava/util/ArrayList;)V

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->initAdapter()V

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->sendUpdatedPlaylist()V

    .line 228
    return-void

    .line 223
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->extractPos(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->remove(Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 74
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 77
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03000c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 79
    return-void
.end method

.method public changeAllState(Z)V
    .locals 6
    .param p1, "checked"    # Z

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 153
    .local v0, "childCount":I
    iget-object v5, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->changeAllState(Z)V

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 157
    .local v4, "mView":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 160
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 161
    .local v3, "itemView":Landroid/view/View;
    const v5, 0x7f0d002b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 162
    .local v2, "itemCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v2, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    .end local v2    # "itemCheckBox":Landroid/widget/CheckBox;
    .end local v3    # "itemView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public getPlayerListView()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getSelectedItemSize()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public hideList()V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->resetPlayerLayout()V

    .line 362
    return-void
.end method

.method public isAllSelected()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isAllSelected()Z

    move-result v0

    return v0
.end method

.method public isAllUnSelected()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isAllUnSelected()Z

    move-result v0

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    const-string v1, "VideoPlayerListView"

    const-string v2, "onCreateLoader() E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;-><init>(Landroid/content/Context;ILcom/sec/android/app/mv/player/common/PlaylistUtils;)V

    .line 295
    .local v0, "l":Landroid/content/AsyncTaskLoader;, "Landroid/content/AsyncTaskLoader<Landroid/database/Cursor;>;"
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/content/AsyncTaskLoader;->setUpdateThrottle(J)V

    .line 296
    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 6
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v3, 0x1

    .line 301
    const-string v0, "VideoPlayerListView"

    const-string v1, "onLoadFinished() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->swapCursor(Landroid/database/Cursor;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->listgetCount()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateActionBarBtn()V

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateCounter()V

    .line 313
    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isListEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->listgetCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 315
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->hideList()V

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 320
    return-void

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a001e

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->listgetCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "VideoPlayerListView"

    const-string v1, "onLoaderReset() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->swapCursor(Landroid/database/Cursor;)V

    .line 327
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->unRegisterBroadcastReciever()V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    .line 358
    return-void
.end method

.method public setNumberOfColumns()V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->setNumberOfColumns()V

    .line 348
    return-void
.end method

.method public setPlayerList()V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setListView()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setContext(Landroid/content/Context;)V

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setImageCacheListener()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->registerBroadcastReciever()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->initAdapter()V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 93
    return-void
.end method

.method public setPlayerListType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x1

    .line 109
    iput p1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->setListType(II)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->initAdapter()V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->restartLoader()V

    .line 113
    if-ne p1, v1, :cond_0

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->changeAllState(Z)V

    .line 116
    :cond_0
    return-void
.end method

.method public showList(Z)V
    .locals 2
    .param p1, "isLandScape"    # Z

    .prologue
    .line 365
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updatePlayerLayout(Z)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->showPlayerListBtn(I)V

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updateActionBarBtn()V

    .line 368
    return-void
.end method

.method public toggleSelectAll()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->changeAllState(Z)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->changeAllState(Z)V

    goto :goto_0
.end method

.method public updateActionBarBtn()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 442
    .local v0, "actionBar":Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    if-nez v0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 447
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getDoneBtn()Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 449
    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setDoneBtnEnabled(Z)V

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setDoneBtnEnabled(Z)V

    goto :goto_0

    .line 454
    :cond_3
    iget v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 455
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getDeleteBtn()Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    .line 457
    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setDeleteBtnEnabled(Z)V

    goto :goto_0

    .line 459
    :cond_4
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    if-le v1, v4, :cond_5

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_6

    .line 462
    :cond_5
    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setDeleteBtnEnabled(Z)V

    goto :goto_0

    .line 464
    :cond_6
    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setDeleteBtnEnabled(Z)V

    goto :goto_0
.end method

.method public updateCounter()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isListEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mCounterLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateList()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v0, "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    .line 173
    iget v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 174
    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlaylistType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 176
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 178
    const/4 v1, 0x0

    .line 179
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a006b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-static {v2, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 182
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a006d

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
