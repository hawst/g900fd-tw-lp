.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;
.super Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
.source "HTTPRequest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HTTPRequest"


# instance fields
.field private mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

.field private mMethod:Ljava/lang/String;

.field private mPostSocket:Ljava/net/Socket;

.field private mRequestHost:Ljava/lang/String;

.field private mRequestPort:I

.field private mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;-><init>()V

    .line 155
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 194
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 260
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    .line 270
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestPort:I

    .line 309
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    .line 471
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 135
    const-string v0, "USER-AGENT"

    sget-object v1, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method private closeInputStream(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)V
    .locals 3
    .param p1, "httpRes"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    .prologue
    .line 453
    if-nez p1, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 457
    .local v1, "is":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 459
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getFirstLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->hasFirstLine()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 358
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 3

    .prologue
    .line 371
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 373
    .local v1, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getFirstLineString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHeaderString()Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "headerString":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 378
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getSocket()Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mMethod:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 164
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getParameterList()Lcom/sec/android/app/mv/player/multivision/http/ParameterList;
    .locals 10

    .prologue
    .line 229
    new-instance v5, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;

    invoke-direct {v5}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;-><init>()V

    .line 230
    .local v5, "paramList":Lcom/sec/android/app/mv/player/multivision/http/ParameterList;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v6

    .line 231
    .local v6, "uri":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 247
    :cond_0
    return-object v5

    .line 234
    :cond_1
    const/16 v8, 0x3f

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 235
    .local v4, "paramIdx":I
    if-ltz v4, :cond_0

    .line 237
    :goto_0
    if-lez v4, :cond_0

    .line 238
    const/16 v8, 0x3d

    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 239
    .local v0, "eqIdx":I
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "name":Ljava/lang/String;
    const/16 v8, 0x26

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 241
    .local v2, "nextParamIdx":I
    add-int/lit8 v9, v0, 0x1

    if-lez v2, :cond_2

    move v8, v2

    :goto_1
    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 243
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/mv/player/multivision/http/Parameter;

    invoke-direct {v3, v1, v7}, Lcom/sec/android/app/mv/player/multivision/http/Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .local v3, "param":Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    invoke-virtual {v5, v3}, Lcom/sec/android/app/mv/player/multivision/http/ParameterList;->add(Ljava/lang/Object;)Z

    .line 245
    move v4, v2

    .line 246
    goto :goto_0

    .line 241
    .end local v3    # "param":Lcom/sec/android/app/mv/player/multivision/http/Parameter;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_1
.end method

.method public getRequestAddress()Ljava/net/InetAddress;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    if-nez v2, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-object v1

    .line 285
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    .line 286
    .local v0, "sock":Ljava/net/Socket;
    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    goto :goto_0
.end method

.method public getRequestHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestPort()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestPort:I

    return v0
.end method

.method public getSocket()Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    return-object v0
.end method

.method public getTargetHostPort()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    if-nez v2, :cond_1

    .line 302
    :cond_0
    :goto_0
    return v1

    .line 298
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    .line 299
    .local v0, "sock":Ljava/net/Socket;
    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v0}, Ljava/net/Socket;->getPort()I

    move-result v1

    goto :goto_0
.end method

.method public getURI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 212
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 221
    :cond_0
    :goto_0
    return-object v0

    .line 215
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "uriTemp":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    goto :goto_0
.end method

.method public isGetRequest()Z
    .locals 1

    .prologue
    .line 175
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isHeadRequest()Z
    .locals 1

    .prologue
    .line 183
    const-string v0, "HEAD"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isKeepAlive()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isCloseConnection()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 394
    :cond_0
    :goto_0
    return v2

    .line 388
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isKeepAliveConnection()Z

    move-result v4

    if-ne v4, v3, :cond_2

    move v2, v3

    .line 389
    goto :goto_0

    .line 390
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v0

    .line 391
    .local v0, "httpVer":Ljava/lang/String;
    const-string v4, "1.0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    move v1, v3

    .line 392
    .local v1, "isHTTP10":Z
    :goto_1
    if-eq v1, v3, :cond_0

    move v2, v3

    .line 394
    goto :goto_0

    .end local v1    # "isHTTP10":Z
    :cond_3
    move v1, v2

    .line 391
    goto :goto_1
.end method

.method public isMethod(Ljava/lang/String;)Z
    .locals 2
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "headerMethod":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 170
    const/4 v1, 0x0

    .line 171
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isNotifyRequest()Z
    .locals 1

    .prologue
    .line 187
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPostRequest()Z
    .locals 1

    .prologue
    .line 179
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/lang/String;I)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 571
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->post(Ljava/lang/String;IZ)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;IZ)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .locals 17
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "isKeepAlive"    # Z

    .prologue
    .line 480
    new-instance v7, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    invoke-direct {v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;-><init>()V

    .line 482
    .local v7, "httpRes":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_9

    const-string v14, "Keep-Alive"

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->setConnection(Ljava/lang/String;)V

    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isHeadRequest()Z

    move-result v10

    .line 486
    .local v10, "isHeaderRequest":Z
    const/4 v11, 0x0

    .line 487
    .local v11, "out":Ljava/io/OutputStream;
    const/4 v8, 0x0

    .line 488
    .local v8, "in":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 492
    .local v12, "pout":Ljava/io/PrintStream;
    :try_start_0
    invoke-static/range {p1 .. p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    .line 493
    .local v2, "addr":Ljava/net/InetAddress;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    if-nez v14, :cond_0

    .line 495
    new-instance v14, Ljava/net/Socket;

    move/from16 v0, p2

    invoke-direct {v14, v2, v0}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 496
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 498
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const v15, 0x1d4c0

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 499
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setReuseAddress(Z)V

    .line 500
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    .line 501
    new-instance v13, Ljava/io/PrintStream;

    invoke-direct {v13, v11}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    .end local v12    # "pout":Ljava/io/PrintStream;
    .local v13, "pout":Ljava/io/PrintStream;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 503
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 505
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isChunked()Z

    move-result v9

    .line 507
    .local v9, "isChunkedRequest":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v4

    .line 508
    .local v4, "content":Ljava/lang/String;
    const/4 v5, 0x0

    .line 509
    .local v5, "contentLength":I
    if-eqz v4, :cond_1

    .line 510
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 512
    :cond_1
    if-lez v5, :cond_3

    .line 513
    const/4 v14, 0x1

    if-ne v9, v14, :cond_2

    .line 514
    int-to-long v14, v5

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 515
    .local v3, "chunSizeBuf":Ljava/lang/String;
    invoke-virtual {v13, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 516
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 518
    .end local v3    # "chunSizeBuf":Ljava/lang/String;
    :cond_2
    invoke-virtual {v13, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 519
    const/4 v14, 0x1

    if-ne v9, v14, :cond_3

    .line 520
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 523
    :cond_3
    const/4 v14, 0x1

    if-ne v9, v14, :cond_4

    .line 524
    const/16 v14, 0x30

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(C)V

    .line 525
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 528
    :cond_4
    invoke-virtual {v13}, Ljava/io/PrintStream;->flush()V

    .line 530
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 532
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 533
    invoke-virtual {v7, v8, v10}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->set(Ljava/io/InputStream;Z)Z
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 545
    :cond_5
    if-nez p3, :cond_7

    .line 546
    if-eqz v8, :cond_6

    .line 548
    :try_start_2
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 553
    :cond_6
    :goto_1
    if-eqz v11, :cond_7

    .line 555
    :try_start_3
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 560
    :goto_2
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 563
    :cond_7
    if-eqz v13, :cond_13

    .line 564
    invoke-virtual {v13}, Ljava/io/PrintStream;->close()V

    move-object v12, v13

    .line 567
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "content":Ljava/lang/String;
    .end local v5    # "contentLength":I
    .end local v9    # "isChunkedRequest":Z
    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    :cond_8
    :goto_3
    return-object v7

    .line 482
    .end local v8    # "in":Ljava/io/InputStream;
    .end local v10    # "isHeaderRequest":Z
    .end local v11    # "out":Ljava/io/OutputStream;
    .end local v12    # "pout":Ljava/io/PrintStream;
    :cond_9
    const-string v14, "close"

    goto/16 :goto_0

    .line 549
    .restart local v2    # "addr":Ljava/net/InetAddress;
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v5    # "contentLength":I
    .restart local v8    # "in":Ljava/io/InputStream;
    .restart local v9    # "isChunkedRequest":Z
    .restart local v10    # "isHeaderRequest":Z
    .restart local v11    # "out":Ljava/io/OutputStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_0
    move-exception v6

    .line 550
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post - in.close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 557
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 558
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post -out/postScket .close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 535
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "content":Ljava/lang/String;
    .end local v5    # "contentLength":I
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v9    # "isChunkedRequest":Z
    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    :catch_2
    move-exception v6

    .line 536
    .local v6, "e":Ljava/net/SocketTimeoutException;
    :goto_4
    const/16 v14, 0x19a

    :try_start_4
    invoke-virtual {v7, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 537
    const-string v14, "HTTPRequest"

    const-string v15, "post -SocketTimeoutException "

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 545
    if-nez p3, :cond_b

    .line 546
    if-eqz v8, :cond_a

    .line 548
    :try_start_5
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 553
    .end local v6    # "e":Ljava/net/SocketTimeoutException;
    :cond_a
    :goto_5
    if-eqz v11, :cond_b

    .line 555
    :try_start_6
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 560
    :goto_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 563
    :cond_b
    if-eqz v12, :cond_8

    .line 564
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto :goto_3

    .line 549
    .restart local v6    # "e":Ljava/net/SocketTimeoutException;
    :catch_3
    move-exception v6

    .line 550
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post - in.close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 557
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v6

    .line 558
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post -out/postScket .close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 538
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v6

    .line 539
    .local v6, "e":Ljava/net/SocketException;
    :goto_7
    const/16 v14, 0x19a

    :try_start_7
    invoke-virtual {v7, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 540
    const-string v14, "HTTPRequest"

    const-string v15, "post -SocketException "

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 545
    if-nez p3, :cond_d

    .line 546
    if-eqz v8, :cond_c

    .line 548
    :try_start_8
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 553
    .end local v6    # "e":Ljava/net/SocketException;
    :cond_c
    :goto_8
    if-eqz v11, :cond_d

    .line 555
    :try_start_9
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 560
    :goto_9
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 563
    :cond_d
    if-eqz v12, :cond_8

    .line 564
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto :goto_3

    .line 549
    .restart local v6    # "e":Ljava/net/SocketException;
    :catch_6
    move-exception v6

    .line 550
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post - in.close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 557
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v6

    .line 558
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post -out/postScket .close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 541
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v6

    .line 542
    .restart local v6    # "e":Ljava/lang/Exception;
    :goto_a
    const/16 v14, 0x1f4

    :try_start_a
    invoke-virtual {v7, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 543
    const-string v14, "HTTPRequest"

    const-string v15, "post - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 545
    if-nez p3, :cond_f

    .line 546
    if-eqz v8, :cond_e

    .line 548
    :try_start_b
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 553
    :cond_e
    :goto_b
    if-eqz v11, :cond_f

    .line 555
    :try_start_c
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    .line 560
    :goto_c
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 563
    :cond_f
    if-eqz v12, :cond_8

    .line 564
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto/16 :goto_3

    .line 549
    :catch_9
    move-exception v6

    .line 550
    const-string v14, "HTTPRequest"

    const-string v15, "post - in.close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_b

    .line 557
    :catch_a
    move-exception v6

    .line 558
    const-string v14, "HTTPRequest"

    const-string v15, "post -out/postScket .close - Exception"

    invoke-static {v14, v15, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_c

    .line 545
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    :goto_d
    if-nez p3, :cond_11

    .line 546
    if-eqz v8, :cond_10

    .line 548
    :try_start_d
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b

    .line 553
    :cond_10
    :goto_e
    if-eqz v11, :cond_11

    .line 555
    :try_start_e
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 556
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    .line 560
    :goto_f
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 563
    :cond_11
    if-eqz v12, :cond_12

    .line 564
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    :cond_12
    throw v14

    .line 549
    :catch_b
    move-exception v6

    .line 550
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v15, "HTTPRequest"

    const-string v16, "post - in.close - Exception"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e

    .line 557
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v6

    .line 558
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v15, "HTTPRequest"

    const-string v16, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_f

    .line 545
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v2    # "addr":Ljava/net/InetAddress;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catchall_1
    move-exception v14

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto :goto_d

    .line 541
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_d
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto :goto_a

    .line 538
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_e
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_7

    .line 535
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_f
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_4

    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v5    # "contentLength":I
    .restart local v9    # "isChunkedRequest":Z
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :cond_13
    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_3
.end method

.method public post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)Z
    .locals 17
    .param p1, "httpRes"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    .prologue
    .line 417
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getSocket()Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    move-result-object v10

    .line 418
    .local v10, "httpSock":Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
    const-wide/16 v12, 0x0

    .line 419
    .local v12, "offset":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContentLength()J

    move-result-wide v8

    .line 420
    .local v8, "length":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->hasContentRange()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-eqz v2, :cond_5

    .line 421
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getContentRangeFirstPosition()J

    move-result-wide v4

    .line 422
    .local v4, "firstPos":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getContentRangeLastPosition()J

    move-result-wide v6

    .line 424
    .local v6, "lastPos":J
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gtz v2, :cond_0

    .line 425
    const-wide/16 v2, 0x1

    sub-long v6, v8, v2

    .line 428
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-gez v2, :cond_1

    .line 429
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->closeInputStream(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)V

    .line 430
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnBadRequest()Z

    move-result v2

    .line 449
    .end local v4    # "firstPos":J
    .end local v6    # "lastPos":J
    :goto_0
    return v2

    .line 433
    .restart local v4    # "firstPos":J
    .restart local v6    # "lastPos":J
    :cond_1
    cmp-long v2, v4, v8

    if-gtz v2, :cond_2

    cmp-long v2, v6, v8

    if-lez v2, :cond_3

    .line 434
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->closeInputStream(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)V

    .line 435
    const/16 v2, 0x1a0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    move-result v2

    goto :goto_0

    .line 438
    :cond_3
    cmp-long v2, v4, v6

    if-lez v2, :cond_4

    .line 439
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->closeInputStream(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)V

    .line 440
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnBadRequest()Z

    move-result v2

    goto :goto_0

    :cond_4
    move-object/from16 v3, p1

    .line 443
    invoke-virtual/range {v3 .. v9}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentRange(JJJ)V

    .line 444
    const/16 v2, 0xce

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 446
    move-wide v12, v4

    .line 447
    sub-long v2, v6, v4

    const-wide/16 v14, 0x1

    add-long v8, v2, v14

    .line 449
    .end local v4    # "firstPos":J
    .end local v6    # "lastPos":J
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isHeadRequest()Z

    move-result v16

    move-object/from16 v11, p1

    move-wide v14, v8

    invoke-virtual/range {v10 .. v16}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;JJZ)Z

    move-result v2

    goto :goto_0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 625
    return-void
.end method

.method public readData()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getSocket()Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->read(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)Z

    move-result v0

    return v0
.end method

.method public returnBadRequest()Z
    .locals 1

    .prologue
    .line 599
    const/16 v0, 0x190

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnNotFoundRequest()Z
    .locals 1

    .prologue
    .line 603
    const/16 v0, 0x194

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnOK()Z
    .locals 1

    .prologue
    .line 595
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnResponse(I)Z
    .locals 4
    .param p1, "statusCode"    # I

    .prologue
    .line 588
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;-><init>()V

    .line 589
    .local v0, "httpRes":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 590
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentLength(J)V

    .line 591
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)Z

    move-result v1

    return v1
.end method

.method public set(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V
    .locals 1
    .param p1, "httpReq"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .prologue
    .line 579
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->set(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;)V

    .line 580
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getSocket()Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->setSocket(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)V

    .line 581
    return-void
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 159
    return-void
.end method

.method protected setRequestHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    .line 264
    return-void
.end method

.method protected setRequestPort(I)V
    .locals 0
    .param p1, "host"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mRequestPort:I

    .line 274
    return-void
.end method

.method public setSocket(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)V
    .locals 0
    .param p1, "value"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mHttpSocket:Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    .line 313
    return-void
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->setURI(Ljava/lang/String;Z)V

    .line 206
    return-void
.end method

.method public setURI(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "isCheckRelativeURL"    # Z

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 198
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->mUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 612
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 613
    .local v0, "str":Ljava/lang/String;
    return-object v0
.end method
