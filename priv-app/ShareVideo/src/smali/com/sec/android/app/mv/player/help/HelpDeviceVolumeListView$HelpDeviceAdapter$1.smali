.class Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;
.super Ljava/lang/Object;
.source "HelpVolumeDialogFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "position"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 337
    if-eqz p3, :cond_0

    .line 338
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 339
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->setVolumeProgress(II)V

    .line 340
    sget-object v1, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProgressChanged index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 346
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->TAG:Ljava/lang/String;

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 351
    sget-object v1, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->TAG:Ljava/lang/String;

    const-string v2, "onStopTrackingTouch"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 353
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->setVolume(II)V

    .line 354
    return-void
.end method
