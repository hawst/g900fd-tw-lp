.class public Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.super Landroid/app/Service;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    }
.end annotation


# static fields
.field private static final CHECK_CNT:I = 0xa

.field private static final CHECK_DELAY:I = 0xc8

.field private static final CMD_NOACTION:I = 0x64

.field private static final CMD_PAUSE:I = 0x66

.field private static final CMD_PLAY:I = 0x65

.field private static final CMD_STOP:I = 0x67

.field private static final CROP_REQUEST:I = 0x3039

.field private static final FADEIN:I = 0x4

.field private static final HANDLER_EXIT_PLAYER:I = 0xe

.field private static final HANDLER_PLAYBACK_BUFFERING_END:I = 0xd

.field private static final IDLE_DELAY:I = 0x3a98

.field public static final IFRAME_SEEK:I = 0x0

.field private static final INVOKE_ID_GET_SERVER_TIMESTAMP_INFO:I = 0x303f

.field private static final ISPAUSECHECK:I = 0x6

.field private static final ISPLAYCHECK:I = 0x5

.field private static final ISSTOPCHECK:I = 0x7

.field private static final KEY_PARAMETER_MULTIVISION_TYPE:I = 0x645

.field private static final LIVE_PLAY:I = 0x3ea

.field private static final LOCAL_PLAY:I = 0x3e8

.field public static final MAX_MULTI_SUBTITLE:I = 0x32

.field public static final PLAYBACK_BUFFERING_END:I = 0x8d

.field public static final PLAYBACK_COMPLETE:I = 0x65

.field public static final PLAYBACK_CURRUPT_ERROR:I = 0x6a

.field public static final PLAYBACK_INVALID_ERROR:I = 0x67

.field public static final PLAYBACK_NOTSUPPORT_ERROR:I = 0x69

.field public static final PLAYBACK_PREPARED:I = 0x85

.field public static final PLAYBACK_SUBTITLE:I = 0x82

.field public static final PLAYBACK_SUBTITLE_UPDATED:I = 0x83

.field public static final PLAYBACK_UNKNOWN_ERROR:I = 0x68

.field public static final PLAYBACK_UPDATE:I = 0x66

.field private static final PLAY_ENDED:I = 0x1

.field public static final REAL_SEEK:I = 0x1

.field private static final RELEASE_WAKELOCK:I = 0x2

.field private static final RESUME_PLAYBACK_BY_CALLSTATE:I = 0xc

.field private static final SEND_MASTER_CLOCK:I = 0x303a

.field private static final SET_AUDIO_CHANNEL:I = 0x303e

.field private static final STREAM_PLAY:I = 0x3e9

.field private static final TAG:Ljava/lang/String; = "MoviePlaybackService"

.field public static mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

.field public static mCompletedListenrCall:Z

.field public static mContext:Landroid/content/Context;

.field private static mHasAudioFocus:Z

.field public static mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;


# instance fields
.field private isIFrameSeekChecker:Z

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mBinder:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;

.field private final mBroadcastListener:Landroid/content/BroadcastReceiver;

.field private mBufferPercentage:I

.field public mBufferingToast:Landroid/widget/Toast;

.field private mCMDAction:I

.field private mCMDCheckCnt:I

.field private mCallState:I

.field private mCallStateChangedResumePlaybackCnt:I

.field private mCallStateString:[Ljava/lang/String;

.field mCurrentVolume:F

.field private final mDelayedStopHandler:Landroid/os/Handler;

.field private mDimWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

.field private mIsMuteDevice:I

.field private mKeepAudioFocus:Z

.field private mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;

.field private mPausedByTransientLossOfFocus:Z

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPlayType:I

.field private mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

.field private mSA:Landroid/media/audiofx/SoundAlive;

.field private mServiceInUse:Z

.field mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

.field private mVideoHeight:I

.field private final mVideoPlayerHandler:Landroid/os/Handler;

.field private mVideoWidth:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    sput-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    .line 63
    sput-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 65
    sput-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 85
    sput-boolean v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 87
    sput-boolean v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferingToast:Landroid/widget/Toast;

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    .line 89
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mKeepAudioFocus:Z

    .line 91
    iput v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallState:I

    .line 99
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 101
    iput v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 103
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 179
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z

    .line 183
    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 185
    iput v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    .line 187
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CALL_STATE_IDLE"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "CALL_STATE_RINGING"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CALL_STATE_OFFHOOK"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateString:[Ljava/lang/String;

    .line 193
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 262
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$2;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    .line 331
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    .line 448
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    .line 671
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2337
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBinder:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;

    return-void
.end method

.method private abandonAudioFocus()V
    .locals 2

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 745
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    .line 748
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateString:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->checkIsRinging(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 52
    sput-boolean p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    return p0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I

    return p1
.end method

.method static synthetic access$1702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I

    return p1
.end method

.method static synthetic access$3102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    return p1
.end method

.method static synthetic access$3202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlayCheck()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPauseCheck()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isStopCheck()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return p1
.end method

.method static synthetic access$708(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    return-object v0
.end method

.method private checkIsRinging(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 662
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 663
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkIsRinging() : state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 668
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private gotoIdleState()V
    .locals 4

    .prologue
    .line 860
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 861
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 862
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 863
    return-void
.end method

.method private isPauseCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x6

    .line 225
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->pause()V

    .line 233
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 234
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 241
    :goto_0
    return-void

    .line 228
    :cond_0
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 237
    :cond_1
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 238
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private isPlayCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    .line 205
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    .line 213
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 214
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 222
    :goto_0
    return-void

    .line 208
    :cond_0
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 218
    :cond_1
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 219
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private isStopCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x7

    .line 244
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stop()V

    .line 252
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 253
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 260
    :goto_0
    return-void

    .line 247
    :cond_0
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 256
    :cond_1
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 257
    iput v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private notifyChange(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 497
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 498
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;->onSvcNotification(ILjava/lang/String;)V

    .line 499
    :cond_0
    return-void
.end method

.method private requestAudioFocus()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 730
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    if-nez v2, :cond_1

    .line 731
    sput-boolean v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 733
    .local v0, "ret":I
    if-ne v0, v1, :cond_0

    .line 740
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 736
    .restart local v0    # "ret":I
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 740
    .end local v0    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z

    goto :goto_0
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 791
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 792
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->showToast(Landroid/content/Context;I)V

    .line 793
    :cond_0
    return-void
.end method

.method private stop(Z)V
    .locals 2
    .param p1, "removeStatusIcon"    # Z

    .prologue
    .line 796
    const-string v0, "MoviePlaybackService"

    const-string v1, "stop() : start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->stop()V

    .line 800
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    .line 803
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mKeepAudioFocus:Z

    if-nez v0, :cond_1

    .line 804
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->abandonAudioFocus()V

    .line 807
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    .line 809
    if-eqz p1, :cond_2

    .line 810
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->gotoIdleState()V

    .line 813
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v0, :cond_3

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 815
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 817
    :cond_3
    return-void
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 889
    return-void
.end method

.method public checkDRMFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 504
    if-nez p1, :cond_1

    .line 523
    :cond_0
    :goto_0
    return v2

    .line 507
    :cond_1
    sget-object v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 509
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    if-nez v3, :cond_2

    .line 510
    const-string v3, "MoviePlaybackService"

    const-string v4, "checkDRMFile => mDrmUtil is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 514
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->reset()V

    .line 515
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    .line 517
    .local v0, "DrmType":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 518
    const-string v2, "MoviePlaybackService"

    const-string v3, "checkDRMFile(). This is DRM file. return false"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const/16 v2, 0x68

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    .line 520
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->duration()J

    move-result-wide v0

    .line 583
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I

    .line 559
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I

    return v0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 943
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFPS()I
    .locals 2

    .prologue
    .line 642
    const/4 v0, -0x1

    .line 643
    .local v0, "fps":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 644
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->getFPS()I

    move-result v0

    .line 646
    :cond_0
    return v0
.end method

.method public getServerTimestampInfo()[J
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->getServerTimestampInfo()[J

    move-result-object v0

    .line 628
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 572
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I

    .line 575
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 564
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I

    .line 567
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I

    return v0
.end method

.method public initSelectSubtitleTrack()V
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 901
    return-void
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 892
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSubtitle(Ljava/lang/String;Z)V

    .line 893
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isPlaying()Z

    move-result v0

    .line 638
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isSecVideo()Z

    move-result v0

    .line 654
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public muteDevice()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->muteDevice()V

    .line 875
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 410
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBinder:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 346
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 347
    const-string v4, "MoviePlaybackService"

    const-string v5, "onCreate() : start!"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    sput-boolean v6, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 350
    new-instance v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .line 351
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setHandler(Landroid/os/Handler;)V

    .line 353
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 354
    iput v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 356
    invoke-static {p0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 357
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    .line 359
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v4, :cond_0

    .line 360
    new-instance v4, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 363
    :cond_0
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 364
    .local v3, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 371
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 372
    .local v1, "powermanager":Landroid/os/PowerManager;
    const/16 v4, 0xa

    const-string v5, "MoviePlayer-Sleep"

    invoke-virtual {v1, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 373
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 375
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 376
    .local v2, "powermanagerForDim":Landroid/os/PowerManager;
    const v4, 0x20000006

    const-string v5, "MoviePlayer-Dim"

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 379
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 380
    .local v0, "intentfilter":Landroid/content/IntentFilter;
    const-string v4, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferingToast:Landroid/widget/Toast;

    .line 383
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I

    .line 386
    new-instance v4, Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    new-instance v6, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/mv/player/common/manager/MotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/mv/player/common/manager/MotionManager$OnMotionListener;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    .line 406
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 459
    const-string v1, "MoviePlaybackService"

    const-string v2, "onDestroy() : start!"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 464
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 465
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 475
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v1, :cond_0

    .line 476
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1}, Landroid/drm/DrmManagerClient;->release()V

    .line 477
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 480
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 481
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 416
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z

    .line 417
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 422
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 424
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 427
    const/4 v1, 0x1

    return v1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 431
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z

    .line 433
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 445
    :goto_0
    return v1

    .line 437
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 438
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 439
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 443
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stopForeground(Z)V

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stopSelf()V

    .line 445
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    goto :goto_0
.end method

.method public openPath(Ljava/lang/String;)Z
    .locals 5
    .param p1, "videoUri"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 527
    const-string v2, "MoviePlaybackService"

    const-string v3, "openPath"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getContentURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 531
    const-string v2, "MoviePlaybackService"

    const-string v3, "openPath. This is client. ShareVideo URL is changed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getContentURL()Ljava/lang/String;

    move-result-object p1

    .line 535
    :cond_0
    if-eqz p1, :cond_1

    .line 536
    const/16 v2, 0x64

    iput v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 537
    iput v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 540
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setDataSourcePrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 547
    const/4 v1, 0x1

    .line 550
    :cond_1
    :goto_0
    return v1

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 543
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException occured  1 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isLiveStreaming()Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$1400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->pause()V

    .line 833
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    .line 837
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->abandonAudioFocus()V

    .line 838
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->gotoIdleState()V

    .line 839
    return-void
.end method

.method public pauseOrStopByAlertSound()V
    .locals 3

    .prologue
    .line 842
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 843
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->pause()V

    .line 848
    :goto_0
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    .line 851
    :cond_0
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseOrStopByAlertSound() E. mPlayer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->gotoIdleState()V

    .line 853
    return-void

    .line 846
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->stop()V

    goto :goto_0
.end method

.method public play()V
    .locals 4

    .prologue
    .line 751
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play() : start! mPlayer.isInitialized() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceExists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/mv/player/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_PLAYING_VZW:Z

    if-nez v1, :cond_2

    .line 759
    const-string v1, "MoviePlaybackService"

    const-string v2, "play() - call connect. Do not play video."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->showToast(I)V

    goto :goto_0

    .line 764
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 766
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MV_SERVER play() mPlayer.isInitialized is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 770
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_4

    .line 771
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->updatePlayedState(Landroid/net/Uri;)V

    .line 774
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->start()V

    .line 776
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->requestAudioFocus()Z

    move-result v1

    if-nez v1, :cond_5

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_PLAYING_VZW:Z

    if-nez v1, :cond_5

    .line 777
    const-string v1, "MoviePlaybackService"

    const-string v2, "play() - requestAudioFocus failed. do not play this time."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 781
    :cond_5
    const/16 v1, 0x66

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V

    .line 783
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 784
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 785
    .local v0, "gatePath":Ljava/lang/String;
    const-string v1, "GATE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<GATE-M> VIDEO_PLAYING: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " </GATE-M>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public position()J
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->position()J

    move-result-wide v0

    .line 591
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public realSeek(II)J
    .locals 3
    .param p1, "pos"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 603
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "realSeek pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seekMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->realSeek(II)J

    move-result-wide v0

    .line 607
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->reset()V

    .line 857
    return-void
.end method

.method public resetSubtitle()V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetSubtitle()V

    .line 905
    return-void
.end method

.method public resetTrackInfo()V
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 935
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 937
    :cond_0
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->seek(J)J

    move-result-wide v0

    .line 599
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public sendMasterClock(JJJJJ)V
    .locals 13
    .param p1, "currentTime"    # J
    .param p3, "clockdelta"    # J
    .param p5, "videoTime"    # J
    .param p7, "currentSecMediaClock"    # J
    .param p9, "currentAudioTimestamp"    # J

    .prologue
    .line 612
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    move-wide v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->sendMasterClock(JJJJJ)V

    .line 615
    :cond_0
    return-void
.end method

.method public setAudioChannel(I)V
    .locals 3
    .param p1, "audioChannel"    # I

    .prologue
    .line 618
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioChannel()audioChannel is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAudioChannel(I)V

    .line 622
    :cond_0
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setInbandSubtitle()V

    .line 897
    return-void
.end method

.method public setKeepAudioFocus(Z)V
    .locals 3
    .param p1, "keepAudioFocus"    # Z

    .prologue
    .line 820
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeepAudioFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mKeepAudioFocus:Z

    .line 822
    return-void
.end method

.method public setSubtitleLanguage(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 912
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPlayer.setSubtitleLanguage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setSubtitleLanguage(I)V

    .line 914
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 1
    .param p1, "synctime"    # I

    .prologue
    .line 908
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setSubtitleSyncTime(I)V

    .line 909
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setVideoCrop(IIII)V

    .line 885
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "vol"    # F

    .prologue
    .line 866
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 869
    :cond_0
    return-void
.end method

.method public setWakeMode(Z)V
    .locals 1
    .param p1, "mode"    # Z

    .prologue
    .line 484
    if-eqz p1, :cond_1

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0
.end method

.method public startPlay()V
    .locals 4

    .prologue
    .line 917
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I

    .line 918
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 921
    :try_start_0
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 922
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setDataSourcePrepare(Landroid/net/Uri;)V

    .line 930
    :goto_0
    return-void

    .line 924
    :cond_0
    const-string v1, "MoviePlaybackService"

    const-string v2, "mPlayer.startPlay() : mServiceUtil or mServiceUtil.getProperPathToOpen() is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 926
    :catch_0
    move-exception v0

    .line 927
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 928
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException occured  1 :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 826
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stop(Z)V

    .line 827
    return-void
.end method

.method public unMuteDevice()V
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->unMuteDevice()V

    .line 881
    :cond_0
    return-void
.end method
