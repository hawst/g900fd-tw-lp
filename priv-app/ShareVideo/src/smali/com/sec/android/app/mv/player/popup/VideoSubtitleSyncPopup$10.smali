.class Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;
.super Ljava/lang/Object;
.source "VideoSubtitleSyncPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 318
    sparse-switch p2, :sswitch_data_0

    .line 352
    :cond_0
    :goto_0
    return v0

    .line 321
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->callSubtitleDismiss()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$300(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)Lcom/sec/android/app/mv/player/activity/ShareVideo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 328
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->hide()V

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->callSubtitleMenu()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$000(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 342
    goto :goto_0

    .line 347
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 350
    goto :goto_0

    .line 318
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x7a -> :sswitch_0
        0xa4 -> :sswitch_3
    .end sparse-switch
.end method
