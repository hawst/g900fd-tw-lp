.class Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;
.super Ljava/lang/Object;
.source "PlaylistActionBar.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initCounterView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-nez p3, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->toggleSelectAll()V

    .line 117
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getSelectedListSize()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBarBtn()V

    .line 119
    return-void

    .line 114
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
