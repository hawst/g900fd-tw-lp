.class Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;
.super Ljava/lang/Object;
.source "VideoActionBarView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoActionBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 688
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0004

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 689
    .local v0, "toast":Landroid/widget/Toast;
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v1, :cond_1

    .line 690
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 693
    :cond_1
    const/16 v1, 0x35

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800f7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 694
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 695
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$15;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->performHapticFeedback(I)Z

    .line 696
    const/4 v1, 0x1

    return v1
.end method
