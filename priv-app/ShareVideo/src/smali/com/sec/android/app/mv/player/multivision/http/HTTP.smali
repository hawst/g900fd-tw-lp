.class public Lcom/sec/android/app/mv/player/multivision/http/HTTP;
.super Ljava/lang/Object;
.source "HTTP.java"


# static fields
.field public static final ACCEPT_LANGUAGE:Ljava/lang/String; = "Accept-Language"

.field public static final CACHE_CONTROL:Ljava/lang/String; = "Cache-Control"

.field public static final CALLBACK:Ljava/lang/String; = "CALLBACK"

.field public static final CHUNKED:Ljava/lang/String; = "Chunked"

.field public static final CLOSE:Ljava/lang/String; = "close"

.field public static final CONNECTION:Ljava/lang/String; = "Connection"

.field public static final CONTENT_LANGUAGE:Ljava/lang/String; = "Content-Language"

.field public static final CONTENT_LENGTH:Ljava/lang/String; = "Content-Length"

.field public static final CONTENT_RANGE:Ljava/lang/String; = "Content-Range"

.field public static final CONTENT_RANGE_BYTES:Ljava/lang/String; = "bytes"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field public static final CONTINUE_100:Ljava/lang/String; = "100-continue"

.field public static final CRLF:Ljava/lang/String; = "\r\n"

.field public static final DATE:Ljava/lang/String; = "Date"

.field public static final DEFAULT_CHUNK_SIZE:I = 0x4b000

.field public static final DEFAULT_DEVICENAME:Ljava/lang/String; = "TEST"

.field private static DEFAULT_PATH:Ljava/lang/String; = null

.field public static final DEFAULT_PORT:I = 0x50

.field public static final DEFAULT_TIMEOUT:I = 0x1e

.field static DEVICE_NAME:Ljava/lang/String; = null

.field public static DEVICE_TYPE:Ljava/lang/String; = null

.field static final DEVICE_TYPE_LIST:[Ljava/lang/String;

.field public static final EXPECT:Ljava/lang/String; = "Expect"

.field public static final FILESHARE_HTTP_READ_TIMEOUT:I = 0x61a8

.field public static FRIENDLYNAME:Ljava/lang/String; = null

.field public static final GET:Ljava/lang/String; = "GET"

.field public static final HEAD:Ljava/lang/String; = "HEAD"

.field public static final HEADER_LINE_DELIM:Ljava/lang/String; = " :"

.field public static final HOST:Ljava/lang/String; = "HOST"

.field public static final HTTP_POST_TIMEOUT:I = 0x1d4c0

.field public static final INTERNETRELAY_HTTP_READ_TIMEOUT:I = 0x61a8

.field public static final KEEP_ALIVE:Ljava/lang/String; = "Keep-Alive"

.field public static final LOCATION:Ljava/lang/String; = "LOCATION"

.field public static final MAX_AGE:Ljava/lang/String; = "max-age"

.field public static final MYNAME:Ljava/lang/String; = "MYNAME"

.field public static final NOTIFY:Ljava/lang/String; = "NOTIFY"

.field public static final NO_CACHE:Ljava/lang/String; = "no-cache"

.field public static final PARSER_HTTP_READ_TIMEOUT:I = 0x61a8

.field public static final POST:Ljava/lang/String; = "POST"

.field public static final RANGE:Ljava/lang/String; = "Range"

.field public static final REQEST_LINE_DELIM:Ljava/lang/String; = " "

.field public static final SERVER:Ljava/lang/String; = "Server"

.field public static final STATUS_LINE_DELIM:Ljava/lang/String; = " "

.field public static final TAB:Ljava/lang/String; = "\t"

.field private static final TAG:Ljava/lang/String; = "HTTP"

.field public static final TIMEOUT:Ljava/lang/String; = "TIMEOUT"

.field public static final TRANSFER_ENCODING:Ljava/lang/String; = "Transfer-Encoding"

.field public static final USER_AGENT:Ljava/lang/String; = "User-Agent"

.field public static final VERSION:Ljava/lang/String; = "1.1"

.field public static final VERSION_10:Ljava/lang/String; = "1.0"

.field public static final VERSION_11:Ljava/lang/String; = "1.1"

.field private static uploadPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 170
    const-string v0, "TEST"

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    .line 172
    const-string v0, "TEST"

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->FRIENDLYNAME:Ljava/lang/String;

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Android/data/com.sec.android.test/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEFAULT_PATH:Ljava/lang/String;

    .line 177
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    .line 179
    const-string v0, "Mobile"

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_TYPE:Ljava/lang/String;

    .line 181
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Mobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Tablet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_TYPE_LIST:[Ljava/lang/String;

    .line 187
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getAbsoluteURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "baseURLStr"    # Ljava/lang/String;
    .param p1, "relURlStr"    # Ljava/lang/String;

    .prologue
    .line 332
    if-nez p0, :cond_0

    .line 333
    const-string v2, ""

    .line 341
    :goto_0
    return-object v2

    .line 335
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 336
    .local v0, "baseURL":Ljava/net/URL;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/URL;->getPort()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 338
    .local v2, "url":Ljava/lang/String;
    goto :goto_0

    .line 339
    .end local v0    # "baseURL":Ljava/net/URL;
    .end local v2    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "HTTP"

    const-string v4, "getAbsoluteURL  - catch Exception "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 341
    const-string v2, ""

    goto :goto_0
.end method

.method public static final getBaseUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "urlStr"    # Ljava/lang/String;

    .prologue
    .line 346
    if-nez p0, :cond_1

    .line 347
    const-string p0, ""

    .line 353
    .end local p0    # "urlStr":Ljava/lang/String;
    .local v0, "i":I
    :cond_0
    :goto_0
    return-object p0

    .line 348
    .end local v0    # "i":I
    .restart local p0    # "urlStr":Ljava/lang/String;
    :cond_1
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 350
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 351
    .restart local v0    # "i":I
    if-gez v0, :cond_2

    .line 352
    const-string p0, ""

    goto :goto_0

    .line 353
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getCacheDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    return-object v0
.end method

.method public static final getHost(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "urlStr"    # Ljava/lang/String;

    .prologue
    .line 256
    if-nez p0, :cond_0

    .line 257
    const-string v3, ""

    .line 266
    :goto_0
    return-object v3

    .line 259
    :cond_0
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    .local v2, "url":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    .line 261
    .end local v2    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 262
    .local v1, "me":Ljava/net/MalformedURLException;
    const-string v3, "HTTP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getHost - MalformedURL : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v3, ""

    goto :goto_0

    .line 264
    .end local v1    # "me":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HTTP"

    const-string v4, "getHost - Exception catched..."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    const-string v3, ""

    goto :goto_0
.end method

.method public static final getPort(Ljava/lang/String;)I
    .locals 6
    .param p0, "urlStr"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x50

    .line 271
    if-nez p0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 276
    .local v3, "url":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->getPort()I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 277
    .local v2, "port":I
    if-gtz v2, :cond_0

    .line 278
    const/16 v2, 0x50

    goto :goto_0

    .line 280
    .end local v2    # "port":I
    .end local v3    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 281
    .local v1, "me":Ljava/net/MalformedURLException;
    const-string v4, "HTTP"

    const-string v5, "getPort - MalformedURL catched "

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    .end local v1    # "me":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "HTTP"

    const-string v5, "getPort - Exception catched "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getUserAgentValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final isAbsoluteURL(Ljava/lang/String;)Z
    .locals 5
    .param p0, "urlStr"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 244
    if-eqz p0, :cond_0

    .line 245
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 246
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->isAbsolute()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 251
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return v2

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HTTP"

    const-string v4, "isAbsoluteURL- Exception "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static setCacheDirectory(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 190
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 191
    :cond_0
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    .line 204
    :goto_0
    return v1

    .line 195
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_3

    .line 197
    :cond_2
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEFAULT_PATH:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    goto :goto_0

    .line 201
    :cond_3
    sput-object p0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    .line 202
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->uploadPath:Ljava/lang/String;

    .line 204
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static setDeviceName(Ljava/lang/String;)Z
    .locals 2
    .param p0, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 216
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 217
    :cond_0
    const/4 v1, 0x0

    .line 235
    :goto_0
    return v1

    .line 219
    :cond_1
    sput-object p0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->FRIENDLYNAME:Ljava/lang/String;

    .line 220
    const/4 v0, 0x1

    .line 228
    .local v0, "isAscii":Z
    if-eqz v0, :cond_2

    .line 229
    sput-object p0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    .line 235
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 232
    :cond_2
    const-string v1, "TEST"

    sput-object v1, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    goto :goto_1
.end method

.method public static setDeviceType(Ljava/lang/String;)Z
    .locals 7
    .param p0, "deviceType"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 362
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_1

    :cond_0
    move v4, v5

    .line 370
    :goto_0
    return v4

    .line 364
    :cond_1
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_TYPE_LIST:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 365
    .local v3, "type":Ljava/lang/String;
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v4, :cond_2

    .line 366
    sput-object p0, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->DEVICE_TYPE:Ljava/lang/String;

    goto :goto_0

    .line 364
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v3    # "type":Ljava/lang/String;
    :cond_3
    move v4, v5

    .line 370
    goto :goto_0
.end method

.method public static final toRelativeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "urlStr"    # Ljava/lang/String;

    .prologue
    .line 328
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->toRelativeURL(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final toRelativeURL(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5
    .param p0, "urlStr"    # Ljava/lang/String;
    .param p1, "withParam"    # Z

    .prologue
    const/4 v4, 0x0

    .line 300
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 301
    :cond_0
    const-string v1, ""

    .line 324
    :cond_1
    :goto_0
    return-object v1

    .line 303
    :cond_2
    move-object v1, p0

    .line 304
    .local v1, "uri":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 305
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2f

    if-eq v3, v4, :cond_1

    .line 306
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 309
    :cond_3
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 310
    .local v2, "url":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 311
    const/4 v3, 0x1

    if-ne p1, v3, :cond_4

    .line 312
    invoke-virtual {v2}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "queryStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    .line 314
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 317
    .end local v0    # "queryStr":Ljava/lang/String;
    :cond_4
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 318
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 319
    .end local v2    # "url":Ljava/net/URL;
    :catch_0
    move-exception v3

    goto :goto_0
.end method
