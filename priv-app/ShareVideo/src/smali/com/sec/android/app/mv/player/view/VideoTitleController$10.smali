.class Lcom/sec/android/app/mv/player/view/VideoTitleController$10;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0

    .prologue
    .line 881
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 885
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 886
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_1

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$200(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->toggleRotateScreen()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$700(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    .line 895
    :goto_0
    return-void

    .line 890
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->toggleRotateSecondScreen()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$800(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    goto :goto_0

    .line 893
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->toggleRotateScreen()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$700(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    goto :goto_0
.end method
