.class Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceBinder"
.end annotation


# instance fields
.field mCallback:Landroid/content/ServiceConnection;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Landroid/content/ServiceConnection;)V
    .locals 0
    .param p2, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 1642
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1643
    iput-object p2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 1644
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-static {p2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$102(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 1650
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 1652
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 1658
    :cond_0
    return-void
.end method
