.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1887
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 1889
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: mVideoSizeChangedListener() E - video real size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1891
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1892
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v1, p3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1893
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_0

    .line 1894
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 1896
    .local v0, "sh":Landroid/view/SurfaceHolder;
    if-eqz v0, :cond_1

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceExists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1897
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 1905
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1906
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->duration()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1907
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsPauseEnable(Z)V

    .line 1908
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x3e8

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1909
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x66

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    .line 1913
    .end local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_0
    :goto_0
    return-void

    .line 1899
    .restart local v0    # "sh":Landroid/view/SurfaceHolder;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    goto :goto_0
.end method
