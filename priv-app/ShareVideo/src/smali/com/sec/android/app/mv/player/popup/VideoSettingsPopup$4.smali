.class Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$4;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 366
    sparse-switch p2, :sswitch_data_0

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 368
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$4;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    .line 370
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 377
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 378
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 387
    goto :goto_0

    .line 392
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 395
    goto :goto_0

    .line 366
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
        0xa4 -> :sswitch_3
    .end sparse-switch
.end method
