.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
.super Ljava/lang/Object;
.source "HTTPHeader.java"


# instance fields
.field private mName:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "lineStr"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v3, ""

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setName(Ljava/lang/String;)V

    .line 58
    const-string v3, ""

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    .line 59
    if-nez p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    const/16 v3, 0x3a

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 63
    .local v0, "colonIdx":I
    if-ltz v0, :cond_0

    .line 66
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "name":Ljava/lang/String;
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setName(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setName(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    .line 54
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 96
    :cond_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mName:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->mValue:Ljava/lang/String;

    .line 84
    return-void
.end method
