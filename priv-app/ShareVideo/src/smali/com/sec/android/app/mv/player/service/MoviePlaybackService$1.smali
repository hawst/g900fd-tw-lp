.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;
.super Landroid/telephony/PhoneStateListener;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 195
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPhoneStateListener() call state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallStateString:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallState:I
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCallState:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$1;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 201
    :cond_0
    return-void
.end method
