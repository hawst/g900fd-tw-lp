.class Lcom/sec/android/app/mv/player/view/VideoBtnController$23;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1969
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 1972
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1973
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1976
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1977
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a009e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1981
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1979
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a009f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
