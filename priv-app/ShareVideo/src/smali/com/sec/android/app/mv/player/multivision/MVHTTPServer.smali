.class public Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;
.super Ljava/lang/Object;
.source "MVHTTPServer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MVHTTPServer"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHttpPort:I

.field private mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

.field private mUsedItem:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mHttpPort:I

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    .line 26
    const-string v0, "MVHTTPServer"

    const-string v1, "MVHTTPServer E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    .line 29
    return-void
.end method


# virtual methods
.method public getResourceUrl(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "item"    # Landroid/net/Uri;

    .prologue
    .line 130
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->isRegisteredContent(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->getRegisteredContentsInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isServerStarted()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->isOpened()Z

    move-result v0

    goto :goto_0
.end method

.method public registerContent(Landroid/net/Uri;)Ljava/lang/String;
    .locals 14
    .param p1, "item"    # Landroid/net/Uri;

    .prologue
    const/4 v13, 0x0

    .line 57
    const-string v0, "MVHTTPServer"

    const-string v1, "registerContent E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    :cond_0
    const-string v0, "MVHTTPServer"

    const-string v1, "registerContent. Uri is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v13

    .line 105
    :cond_1
    :goto_0
    return-object v8

    .line 64
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    .line 65
    .local v11, "scheme":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://ipaddr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mHttpPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, "id":Ljava/lang/String;
    const-string v0, "MVHTTPServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerContent. scheme : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-nez v11, :cond_3

    .line 69
    const-string v0, "MVHTTPServer"

    const-string v1, "registerContent-no scheme"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v13

    .line 70
    goto :goto_0

    .line 71
    :cond_3
    const-string v0, "content"

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 72
    const/4 v6, 0x0

    .line 74
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 76
    if-nez v6, :cond_4

    .line 77
    const-string v0, "MVHTTPServer"

    const-string v1, "registerContent-Invalid cursor."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v13

    .line 78
    goto :goto_0

    .line 81
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 83
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 84
    .local v10, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v12

    .line 86
    .local v12, "uri":Landroid/net/Uri;
    const-string v0, "mime_type"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 88
    .local v9, "mimeType":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v12, v2}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->registerContent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    move-object v8, v13

    .line 93
    goto/16 :goto_0

    .line 94
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v10    # "path":Ljava/lang/String;
    .end local v12    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 95
    .local v7, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 97
    if-eqz v6, :cond_5

    .line 98
    const-string v0, "MVHTTPServer"

    const-string v1, "Cursor should be closed in exception."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v8, v13

    .line 101
    goto/16 :goto_0

    .line 104
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_6
    const-string v0, "MVHTTPServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerContent-Unsupported scheme : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v13

    .line 105
    goto/16 :goto_0
.end method

.method public startServer()Z
    .locals 2

    .prologue
    .line 32
    const-string v0, "MVHTTPServer"

    const-string v1, "startServer-EXEC"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->start()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->getPort()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mHttpPort:I

    .line 36
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopServer()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "MVHTTPServer"

    const-string v1, "stopServer-EXEC"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->unregisterAllContent()V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->stop()V

    .line 47
    :cond_0
    return-void
.end method

.method public unregisterAllContent()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->unregisterAllContent()V

    .line 127
    :cond_0
    return-void
.end method

.method public unregisterContent(Landroid/net/Uri;)V
    .locals 2
    .param p1, "item"    # Landroid/net/Uri;

    .prologue
    .line 111
    if-eqz p1, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mUsedItem:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/ContentServer;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->unregisterContent(Ljava/lang/String;)V

    goto :goto_0
.end method
