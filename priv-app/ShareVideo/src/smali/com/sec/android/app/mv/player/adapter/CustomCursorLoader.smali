.class public Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;
.super Landroid/content/AsyncTaskLoader;
.source "CustomCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field mBucketId:I

.field mCancellationSignal:Landroid/os/CancellationSignal;

.field mCursor:Landroid/database/Cursor;

.field mDB:Lcom/sec/android/app/mv/player/db/VideoDB;

.field private mListType:I

.field final mObserver:Landroid/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field mPlaylistUtils:Lcom/sec/android/app/mv/player/common/PlaylistUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mBucketId:I

    .line 51
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/app/mv/player/common/PlaylistUtils;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I
    .param p3, "playlist"    # Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mBucketId:I

    .line 56
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 57
    invoke-static {p1}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 58
    iput-object p3, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mPlaylistUtils:Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    .line 59
    iput p2, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mListType:I

    .line 60
    return-void
.end method

.method private getCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 177
    const/4 v0, 0x0

    .line 186
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursorALL()Landroid/database/Cursor;

    move-result-object v0

    .line 187
    return-object v0
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->cancelLoadInBackground()V

    .line 92
    monitor-enter p0

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 96
    :cond_0
    monitor-exit p0

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deliverResult(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->isReset()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 118
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 124
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 65
    monitor-enter p0

    .line 66
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->isLoadInBackgroundCanceled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    new-instance v1, Landroid/os/OperationCanceledException;

    invoke-direct {v1}, Landroid/os/OperationCanceledException;-><init>()V

    throw v1

    .line 70
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 69
    :cond_0
    :try_start_1
    new-instance v1, Landroid/os/CancellationSignal;

    invoke-direct {v1}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 70
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 74
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 76
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 82
    :cond_1
    monitor-enter p0

    .line 83
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 84
    monitor-exit p0

    return-object v0

    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 82
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v1

    monitor-enter p0

    .line 83
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 84
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public onCanceled(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 158
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 161
    :cond_0
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->onCanceled(Landroid/database/Cursor;)V

    return-void
.end method

.method protected onReset()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->onStopLoading()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 173
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 174
    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->forceLoad()V

    .line 145
    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->cancelLoad()Z

    .line 154
    return-void
.end method

.method registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 105
    return-void
.end method
