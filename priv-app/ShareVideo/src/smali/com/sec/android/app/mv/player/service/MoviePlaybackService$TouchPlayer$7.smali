.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnTimedTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1916
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
    .locals 10
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "text"    # Landroid/media/TimedText;

    .prologue
    .line 1918
    const-string v7, "MoviePlaybackService"

    const-string v8, "TouchPlayer :: onTimedText()"

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v7, :cond_0

    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v7, :cond_0

    .line 1921
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z

    move-result v7

    if-nez v7, :cond_1

    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleType()I

    move-result v7

    if-eqz v7, :cond_1

    .line 1922
    const-string v7, "MoviePlaybackService"

    const-string v8, "onTimedText() :: mIsOutbandSubtitle is false and not inband type"

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963
    :cond_0
    :goto_0
    return-void

    .line 1926
    :cond_1
    if-eqz p2, :cond_8

    .line 1928
    const/16 v7, 0x11

    :try_start_0
    invoke-virtual {p2, v7}, Landroid/media/TimedText;->getObject(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1929
    .local v4, "textIndex":I
    const/4 v7, 0x7

    invoke-virtual {p2, v7}, Landroid/media/TimedText;->getObject(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1930
    .local v5, "textStartTimeMS":I
    invoke-virtual {p2}, Landroid/media/TimedText;->getText()Ljava/lang/String;

    move-result-object v6

    .line 1931
    .local v6, "textString":Ljava/lang/String;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onTimedText(). textIndex : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " textStartTimeMS : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " text = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1933
    if-nez v6, :cond_2

    .line 1934
    const-string v6, ""

    .line 1937
    :cond_2
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v7

    if-eqz v7, :cond_7

    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    if-eqz v7, :cond_7

    .line 1938
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    .line 1939
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    const/16 v8, 0x32

    new-array v8, v8, [Ljava/lang/String;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;[Ljava/lang/String;)[Ljava/lang/String;

    .line 1941
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    aput-object v8, v7, v4

    .line 1942
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    .line 1943
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    const-string v8, ""

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Ljava/lang/String;)Ljava/lang/String;

    .line 1945
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    array-length v7, v7

    if-ge v1, v7, :cond_6

    .line 1946
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    aget v3, v7, v1

    .line 1947
    .local v3, "selectedIndex":I
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    move-result-object v7

    iget-object v7, v7, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    aget v2, v7, v3

    .line 1948
    .local v2, "idx":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v2

    if-nez v7, :cond_5

    const-string v7, ""

    :goto_2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v8, v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Ljava/lang/String;)Ljava/lang/String;

    .line 1949
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-eq v1, v7, :cond_4

    .line 1950
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\r\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Ljava/lang/String;)Ljava/lang/String;

    .line 1945
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1948
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v2

    goto :goto_2

    .line 1953
    .end local v2    # "idx":I
    .end local v3    # "selectedIndex":I
    :cond_6
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->updateSubtitle(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1958
    .end local v1    # "i":I
    .end local v4    # "textIndex":I
    .end local v5    # "textStartTimeMS":I
    .end local v6    # "textString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1959
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v7, "MoviePlaybackService"

    const-string v8, "IllegalArgumentException"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1955
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    .restart local v4    # "textIndex":I
    .restart local v5    # "textStartTimeMS":I
    .restart local v6    # "textString":Ljava/lang/String;
    :cond_7
    :try_start_1
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "TouchPlayer :: onTimedText() - , text = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1956
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->updateSubtitle(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1961
    .end local v4    # "textIndex":I
    .end local v5    # "textStartTimeMS":I
    .end local v6    # "textString":Ljava/lang/String;
    :cond_8
    sget-object v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->clearSubtitle()V

    goto/16 :goto_0
.end method
