.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1377
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1379
    sparse-switch p2, :sswitch_data_0

    .line 1421
    :cond_0
    :goto_0
    return v0

    .line 1382
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1383
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->dismiss()V

    .line 1385
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 1391
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1392
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1393
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 1394
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V

    .line 1398
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 1399
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 1396
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "VideoSubtitlePopup"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_2
    move v0, v1

    .line 1411
    goto :goto_0

    .line 1415
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1418
    goto :goto_0

    .line 1379
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
        0x7a -> :sswitch_0
        0xa4 -> :sswitch_3
    .end sparse-switch
.end method
