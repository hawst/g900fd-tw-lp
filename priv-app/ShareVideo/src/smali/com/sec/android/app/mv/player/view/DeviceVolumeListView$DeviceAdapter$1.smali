.class Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;
.super Ljava/lang/Object;
.source "DeviceVolumeListView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "position"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 267
    if-eqz p3, :cond_0

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->access$700(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeMVBarPopup()V

    .line 270
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 271
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V

    .line 272
    sget-object v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProgressChanged index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .end local v0    # "index":I
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 278
    sget-object v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 283
    sget-object v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    const-string v2, "onStopTrackingTouch"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 285
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;->this$1:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V

    .line 286
    return-void
.end method
