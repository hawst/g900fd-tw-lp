.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1867
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    const/16 v4, 0xd

    .line 1869
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v0, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1870
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: mBufferingUpdateListener - total download : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1872
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1873
    if-nez p2, :cond_1

    .line 1874
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->bufferstate()V

    .line 1884
    :cond_0
    :goto_0
    return-void

    .line 1875
    :cond_1
    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    .line 1876
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 1878
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1879
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1880
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
