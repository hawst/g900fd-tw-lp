.class Lcom/sec/android/app/mv/player/activity/ShareVideo$36;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 3417
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 3420
    const-string v2, "ShareVideo"

    const-string v3, "onServiceConnected()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 3421
    check-cast v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;

    .line 3422
    .local v0, "binder":Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;->getService()Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v3

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$902(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 3424
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->initialize(Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;)V

    .line 3425
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->startChord()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3429
    :goto_0
    return-void

    .line 3426
    :catch_0
    move-exception v1

    .line 3427
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 3433
    const-string v0, "ShareVideo"

    const-string v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3434
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$902(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 3435
    return-void
.end method
