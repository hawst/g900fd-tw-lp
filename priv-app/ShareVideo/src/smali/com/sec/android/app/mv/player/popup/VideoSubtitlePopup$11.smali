.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSubtitleFontPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 782
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 784
    .local p1, "adapterview":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-eqz p1, :cond_0

    .line 785
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 786
    .local v0, "subtitleFont":Ljava/lang/String;
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callSubtitleFontPopup() :: onItemClick = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFont(Ljava/lang/String;)V

    .line 790
    .end local v0    # "subtitleFont":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 791
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2202(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 795
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->refreshSubtitleMenu()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1600(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 796
    return-void
.end method
