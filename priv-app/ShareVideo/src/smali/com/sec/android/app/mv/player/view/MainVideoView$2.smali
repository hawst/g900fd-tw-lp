.class Lcom/sec/android/app/mv/player/view/MainVideoView$2;
.super Ljava/lang/Object;
.source "MainVideoView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    .line 300
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 301
    const-string v4, "MainVideoView"

    const-string v5, "mTouchListener. LockState true"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$100(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$100(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 399
    :cond_0
    :goto_0
    return v8

    .line 310
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v4, :cond_2

    .line 311
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 314
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 316
    .local v0, "action":I
    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 317
    const-string v4, "MainVideoView"

    const-string v5, "mChangeViewDone false"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 322
    :cond_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 323
    const-string v4, "MainVideoView"

    const-string v5, "MV TouchMotion onTouchEvent"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 335
    :cond_4
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 362
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$902(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I
    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1002(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    move-result v5

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$702(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    .line 365
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isVolumeCtrlShowing()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 368
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$602(Lcom/sec/android/app/mv/player/view/MainVideoView;Z)Z

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1100(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 370
    .local v2, "screenWidth":I
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$900(Lcom/sec/android/app/mv/player/view/MainVideoView;)I

    move-result v4

    div-int/lit8 v6, v2, 0x2

    if-le v4, v6, :cond_7

    sget-object v4, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    :goto_1
    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    invoke-static {v5, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$802(Lcom/sec/android/app/mv/player/view/MainVideoView;Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    .line 372
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$400(Lcom/sec/android/app/mv/player/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 373
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$400(Lcom/sec/android/app/mv/player/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v9, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 338
    .end local v2    # "screenWidth":I
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$400(Lcom/sec/android/app/mv/player/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 342
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$600(Lcom/sec/android/app/mv/player/view/MainVideoView;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isVolumeCtrlShowing()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 348
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 349
    .local v3, "y":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$700(Lcom/sec/android/app/mv/player/view/MainVideoView;)I

    move-result v4

    sub-int v1, v4, v3

    .line 351
    .local v1, "moveVal":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$800(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    if-ne v4, v5, :cond_6

    .line 352
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolume(I)V

    .line 356
    :cond_5
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$902(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    .line 357
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$702(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    goto/16 :goto_0

    .line 353
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$800(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    if-ne v4, v5, :cond_5

    .line 354
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightness(I)V

    goto :goto_2

    .line 370
    .end local v1    # "moveVal":I
    .end local v3    # "y":I
    .restart local v2    # "screenWidth":I
    :cond_7
    sget-object v4, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    goto/16 :goto_1

    .line 378
    .end local v2    # "screenWidth":I
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->finishUpVideoGesture()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 379
    const-string v4, "MainVideoView"

    const-string v5, "mTouchListener - VideoGesture is cancel"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 384
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->finishUpVideoGesture()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 385
    const-string v4, "MainVideoView"

    const-string v5, "mTouchListener - VideoGesture is finishing up"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 386
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    # invokes: Lcom/sec/android/app/mv/player/view/MainVideoView;->isNotificationAreaTouched(I)Z
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1200(Lcom/sec/android/app/mv/player/view/MainVideoView;I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 387
    const-string v4, "MainVideoView"

    const-string v5, "mTouchListener - Notification Area Touched"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 388
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    move-result-object v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 389
    const-string v4, "MainVideoView"

    const-string v5, "mTouchListener - GroupList showing, dissmiss grouplist"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 391
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 335
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_4
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x105 -> :sswitch_1
    .end sparse-switch
.end method
