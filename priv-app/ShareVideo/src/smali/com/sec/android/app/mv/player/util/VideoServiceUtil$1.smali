.class Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 942
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v4, 0x1

    .line 944
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPlayingChecker , mPlayingCheckTime:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v1

    if-gez v1, :cond_0

    .line 947
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 994
    :goto_0
    return-void

    .line 951
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v2, 0x12c

    # -= operator for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$020(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;I)I

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 955
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    if-lez v1, :cond_2

    .line 956
    const-string v1, "VideoServiceUtil"

    const-string v2, "mPlayingChecker..playing"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 959
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideStateView()V

    .line 962
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 980
    :catch_0
    move-exception v0

    .line 984
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 985
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException occured  2 :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 964
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPlayingChecker..not playing -- state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # invokes: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->checkLockScreenOn()Z
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 966
    const-string v1, "VideoServiceUtil"

    const-string v2, "checkLockScreenOn checking again "

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 968
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 970
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    if-eq v1, v4, :cond_5

    .line 978
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 974
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 975
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideStateView()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 992
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
