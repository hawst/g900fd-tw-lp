.class Lcom/sec/android/app/mv/player/view/VideoFlickView$2;
.super Ljava/lang/Object;
.source "VideoFlickView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/view/VideoFlickView;->leftFlickView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 152
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$400(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoFlickView$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView$2$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoFlickView$2;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 150
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 133
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$400(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$400(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$002(Lcom/sec/android/app/mv/player/view/VideoFlickView;I)I

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 130
    return-void
.end method
