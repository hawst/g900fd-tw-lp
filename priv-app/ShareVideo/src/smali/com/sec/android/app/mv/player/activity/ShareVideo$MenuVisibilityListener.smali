.class Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/app/ActionBar$OnMenuVisibilityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuVisibilityListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2773
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/activity/ShareVideo$1;

    .prologue
    .line 2773
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    return-void
.end method


# virtual methods
.method public onMenuVisibilityChanged(Z)V
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    .line 2775
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMenuVisibilityChanged - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2777
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-nez v0, :cond_0

    .line 2785
    :goto_0
    return-void

    .line 2780
    :cond_0
    if-eqz p1, :cond_1

    .line 2781
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 2783
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    goto :goto_0
.end method
