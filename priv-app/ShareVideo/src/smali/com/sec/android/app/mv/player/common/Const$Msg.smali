.class public Lcom/sec/android/app/mv/player/common/Const$Msg;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/common/Const;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Msg"
.end annotation


# static fields
.field public static final CURRENT_CONTENT_STOP:I = 0x68

.field public static final MULTIVISION_DEVICE_DISCONNECTED:I = 0x69

.field public static final NEED_MULTIVISION_VIEW:I = 0x66

.field public static final OPEN_PATH:I = 0x67

.field public static final SEND_VOLUME_VALUE:I = 0x6c

.field public static final SET_SCREEN_ROTATION:I = 0x6d

.field public static final SET_VOLUME_MUTE:I = 0x6a

.field public static final SET_VOLUME_ON:I = 0x6b

.field public static final START_SHAREVIDEO:I = 0x64

.field public static final STOP_SHAREVIDEO:I = 0x65

.field public static final TYPE_SET_BOTH_AUDIO:I = 0x12c

.field public static final TYPE_SET_LANDSCAPE_MODE:I = 0x130

.field public static final TYPE_SET_LEFT_AUDIO:I = 0x12d

.field public static final TYPE_SET_PORTRAIT_MODE:I = 0x12f

.field public static final TYPE_SET_RIGHT_AUDIO:I = 0x12e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
