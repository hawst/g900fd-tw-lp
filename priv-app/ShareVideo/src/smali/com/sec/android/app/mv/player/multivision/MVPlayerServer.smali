.class public Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
.super Lcom/sec/android/app/mv/player/multivision/MVPlayer;
.source "MVPlayerServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;,
        Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MVPlayerServer"


# instance fields
.field private DEBUG:Z

.field private isSeeking:Z

.field private mAdjustHeightInch:I

.field private mAdjustLeftCropRatio:F

.field private mAdjustRightCropRatio:F

.field private mBottom:I

.field private mBufferStatus:I

.field private mClientDeviceName:Ljava/lang/String;

.field private mClientIp:Ljava/lang/String;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mExpired:Z

.field private mGroupMember:Z

.field private mHide:Z

.field private mLastSeekPos:I

.field private mLeft:I

.field private mMVClientPosition:I

.field private mMVPlayNumber:I

.field private mMVSupport:Z

.field private mMeasuredHeightRatio:I

.field private mNumOfExpired:I

.field private mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

.field private mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

.field private mPlayerStatus:I

.field private mReqDirection:I

.field private mReqTouchTime:J

.field private mRight:I

.field private mTop:I

.field private mXdpi:I

.field private mYdpi:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "clientIp"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVPlayer;-><init>()V

    .line 15
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVClientPosition:I

    .line 16
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mGroupMember:Z

    .line 17
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVSupport:Z

    .line 19
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 20
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 22
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    .line 23
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqDirection:I

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqTouchTime:J

    .line 27
    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mClientDeviceName:Ljava/lang/String;

    .line 29
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mHide:Z

    .line 30
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mExpired:Z

    .line 31
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mNumOfExpired:I

    .line 34
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayWidth:I

    .line 35
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayHeight:I

    .line 36
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mXdpi:I

    .line 37
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mYdpi:I

    .line 39
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustHeightInch:I

    .line 40
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustLeftCropRatio:F

    .line 41
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustRightCropRatio:F

    .line 42
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMeasuredHeightRatio:I

    .line 44
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    .line 727
    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    .line 777
    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    .line 47
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVPlayerServer() - id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", clientIp : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mClientIp:Ljava/lang/String;

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    .line 52
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;-><init>(ILcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    .line 53
    return-void
.end method


# virtual methods
.method public checkHide()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mHide:Z

    return v0
.end method

.method public endConnection(I)V
    .locals 3
    .param p1, "endMsg"    # I

    .prologue
    .line 183
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 185
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->finished()V

    .line 190
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getAdjustHeightInch()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustHeightInch:I

    return v0
.end method

.method public getAdjustLeftCropRatio()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustLeftCropRatio:F

    return v0
.end method

.method public getAdjustRightCropRatio()F
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustRightCropRatio:F

    return v0
.end method

.method public getBufferStatus()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    return v0
.end method

.method public getClientDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mClientDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayHeight()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayHeight:I

    return v0
.end method

.method public getDisplayWidth()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayWidth:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    return v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mClientIp:Ljava/lang/String;

    return-object v0
.end method

.method public getMVClientPosition()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVClientPosition:I

    return v0
.end method

.method public getMVPlayerServerReady()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNumOfExpired()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mNumOfExpired:I

    return v0
.end method

.method public getPlayPosition()V
    .locals 4

    .prologue
    .line 353
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 354
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] getPlayPosition()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_0
    const/4 v0, 0x0

    .line 356
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public getPlayerState()V
    .locals 4

    .prologue
    .line 361
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 362
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] getPlayerState()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_0
    const/4 v0, 0x0

    .line 364
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method public getPlayerStatus()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    return v0
.end method

.method public getRequestDirection()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqDirection:I

    return v0
.end method

.method public getRequestTouchTime()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqTouchTime:J

    return-wide v0
.end method

.method public getXdpi()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mXdpi:I

    return v0
.end method

.method public getYdpi()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mYdpi:I

    return v0
.end method

.method public isGroupMember()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mGroupMember:Z

    return v0
.end method

.method public isRespExpired()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mExpired:Z

    return v0
.end method

.method public isSeeking()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    return v0
.end method

.method public isSupportMultiVision()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVSupport:Z

    return v0
.end method

.method public notifyMessageResponse(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 426
    packed-switch p1, :pswitch_data_0

    .line 612
    :pswitch_0
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Unknown response message type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 428
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 429
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SET_DATA_SOURCE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSetDataSource(I)V

    goto :goto_0

    .line 435
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 436
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse PLAY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onPlay(I)V

    goto :goto_0

    .line 442
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 443
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse PAUSE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onPause(I)V

    goto/16 :goto_0

    .line 449
    :pswitch_4
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 450
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse STOP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onStop(I)V

    goto/16 :goto_0

    .line 456
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 457
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse RESUME"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onResume(I)V

    goto/16 :goto_0

    .line 463
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 464
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEEK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_6
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_7

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSeek(I)V

    .line 468
    :cond_7
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] RECEIVED SEEK_seeking mLastSeekPos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    if-eq v0, v4, :cond_0

    .line 470
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->seekTo(I)V

    .line 471
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    goto/16 :goto_0

    .line 476
    :pswitch_7
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_8

    .line 477
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SET_PLAY_POSITION"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSetPlayPosition(I)V

    goto/16 :goto_0

    .line 483
    :pswitch_8
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_9

    .line 484
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse GET_PLAY_POSITION"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onGetPlayPosition(I)V

    goto/16 :goto_0

    .line 490
    :pswitch_9
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_a

    .line 491
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse GET_PLAYER_STATE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onGetPlayerState(I)V

    goto/16 :goto_0

    .line 498
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendMasterClock(I)V

    goto/16 :goto_0

    .line 503
    :pswitch_b
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_b

    .line 504
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_PLAYER_STATE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendPlayerState(I)V

    goto/16 :goto_0

    .line 510
    :pswitch_c
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_c

    .line 511
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SET_MULTIVISION_SIZE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onChangeToMultiVision(I)V

    goto/16 :goto_0

    .line 517
    :pswitch_d
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_d

    .line 518
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SET_SINGLEVISION_SIZE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onChangeToSingleVision(I)V

    goto/16 :goto_0

    .line 524
    :pswitch_e
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_e

    .line 525
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_CLIENT_DISCONNECT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_e
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_f

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_f

    .line 529
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onReady(I)V

    .line 534
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendClientDisconnect(I)V

    goto/16 :goto_0

    .line 539
    :pswitch_f
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_10

    .line 540
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse JOIN_MV_GROUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onJoinMVGroup(I)V

    goto/16 :goto_0

    .line 546
    :pswitch_10
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_11

    .line 547
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse EXIT_MV_GROUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onExitMVGroup(I)V

    goto/16 :goto_0

    .line 553
    :pswitch_11
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_12

    .line 554
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_CLIENT_MVREQ"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqDirection:I

    iget-wide v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqTouchTime:J

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendClientMVRequest(IIJ)V

    goto/16 :goto_0

    .line 560
    :pswitch_12
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_13

    .line 561
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_CONTENT_URL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :cond_13
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendContentURL(I)V

    goto/16 :goto_0

    .line 567
    :pswitch_13
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_14

    .line 568
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_CLIENT_INITIAL_INFO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :cond_14
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onSendClientInitialInfo(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    goto/16 :goto_0

    .line 575
    :pswitch_14
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_15

    .line 576
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse PAUSE_SERVER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_15
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onClientPause(I)V

    goto/16 :goto_0

    .line 583
    :pswitch_15
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_16

    .line 584
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse PLAY_SERVER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_16
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onClientPlay(I)V

    goto/16 :goto_0

    .line 591
    :pswitch_16
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_17

    .line 592
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse HIDE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    :cond_17
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onHide(I)V

    goto/16 :goto_0

    .line 598
    :pswitch_17
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_18

    .line 599
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SHOW"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_18
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->onShow(I)V

    goto/16 :goto_0

    .line 605
    :pswitch_18
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_19

    .line 606
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyMessageResponse SEND_KEEP_ALIVE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_19
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mExpired:Z

    .line 608
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mNumOfExpired:I

    goto/16 :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_13
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public notifyStatusChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 618
    packed-switch p1, :pswitch_data_0

    .line 692
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Unknown state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    :cond_0
    :goto_0
    return-void

    .line 620
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 621
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged PREPARED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 623
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onPrepared(I)V

    goto :goto_0

    .line 629
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 630
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged PLAYED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 632
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onPlayed(I)V

    goto :goto_0

    .line 638
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 639
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged STOPPED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 641
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onStopped(I)V

    goto/16 :goto_0

    .line 647
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 648
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged PAUSED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 650
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onPaused(I)V

    goto/16 :goto_0

    .line 656
    :pswitch_4
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 657
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged RESUMED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 659
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onResumed(I)V

    goto/16 :goto_0

    .line 665
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 666
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged READY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 668
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onReady(I)V

    goto/16 :goto_0

    .line 674
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_7

    .line 675
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged BUFFERING"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 677
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onBuffering(I)V

    goto/16 :goto_0

    .line 683
    :pswitch_7
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_8

    .line 684
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] notifyStatusChanged MEDIA_PLAYBACK_COMPLETE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    if-eqz v0, :cond_0

    .line 686
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mPlayerStatus:I

    .line 687
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;->onComplete(I)V

    goto/16 :goto_0

    .line 618
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 272
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 273
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] pause()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    const/4 v0, 0x0

    .line 275
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public play(I)V
    .locals 4
    .param p1, "startTime"    # I

    .prologue
    .line 264
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 265
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] play()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_0
    const/4 v0, 0x0

    .line 267
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 288
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 289
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] resume()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_0
    const/4 v0, 0x0

    .line 291
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v2, 0x1

    .line 296
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    if-ne v1, v2, :cond_0

    .line 297
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    .line 298
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] seekTo() seek is working, Last Seek Pos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLastSeekPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :goto_0
    const/16 v1, 0x8

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 309
    return-void

    .line 301
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking:Z

    .line 302
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] seekTo() pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendContentURL(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 391
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 392
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendContentURL() url : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_0
    const/4 v0, 0x0

    .line 394
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public sendDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V
    .locals 4
    .param p1, "list"    # Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;

    .prologue
    .line 377
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "message":Ljava/lang/String;
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendDeviceInfo(). message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;Ljava/lang/Object;)V

    .line 380
    return-void
.end method

.method public sendKeepAlive()V
    .locals 4

    .prologue
    .line 407
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 408
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendKeepAlive()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mExpired:Z

    .line 410
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mNumOfExpired:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mNumOfExpired:I

    .line 411
    const/4 v0, 0x0

    .line 412
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 414
    return-void
.end method

.method public sendMasterClock(JJJJ)V
    .locals 3
    .param p1, "clock"    # J
    .param p3, "videoTime"    # J
    .param p5, "currentSecMediaClock"    # J
    .param p7, "currentAudioTimestamp"    # J

    .prologue
    .line 369
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_0

    .line 370
    const/4 v0, 0x0

    .line 371
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 374
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public sendMultiVisionDeviceDisconnected(IILjava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "position"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 417
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 418
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendMultiVisionDeviceDisconnected() id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    const/4 v0, 0x0

    .line 421
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method public sendPlayerState(I)V
    .locals 4
    .param p1, "playerState"    # I

    .prologue
    .line 383
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 384
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerState() playerState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_0
    const/4 v0, 0x0

    .line 386
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method public sendVolumeValue(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 399
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 400
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendVolumeValue() value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_0
    const/4 v0, 0x0

    .line 402
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method public sendVolumeValueUpdateById(II)V
    .locals 1
    .param p1, "mid"    # I
    .param p2, "vVol"    # I

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;->sendVolumeValueUpdateById(II)V

    .line 781
    return-void
.end method

.method public setAdjustHeightInch(I)V
    .locals 0
    .param p1, "adjustHeightInch"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustHeightInch:I

    .line 121
    return-void
.end method

.method public setAdjustLeftCropRatio(F)V
    .locals 0
    .param p1, "adjustLeftCropRatio"    # F

    .prologue
    .line 135
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustLeftCropRatio:F

    .line 136
    return-void
.end method

.method public setAdjustRightCropRatio(F)V
    .locals 0
    .param p1, "adjustRightCropRatio"    # F

    .prologue
    .line 143
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mAdjustRightCropRatio:F

    .line 144
    return-void
.end method

.method public setAudioChannel(I)V
    .locals 4
    .param p1, "audioChannel"    # I

    .prologue
    .line 321
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 322
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setAudioChannel, Audio Channel is"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_0
    const/4 v0, 0x0

    .line 324
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x19

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 325
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method public setClientDeviceName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 227
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] setClientDeviceName()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mClientDeviceName:Ljava/lang/String;

    .line 229
    return-void
.end method

.method public setClientDisplayInfo(IIII)V
    .locals 3
    .param p1, "displayWidth"    # I
    .param p2, "displayHeight"    # I
    .param p3, "xdpi"    # I
    .param p4, "ydpi"    # I

    .prologue
    .line 236
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayWidth:I

    .line 237
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mDisplayHeight:I

    .line 238
    iput p3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mXdpi:I

    .line 239
    iput p4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mYdpi:I

    .line 241
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] setClientDisplayInfo. displayWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", displayHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", xdpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ydpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_0
    return-void
.end method

.method public setClientMultiVisionMode()V
    .locals 4

    .prologue
    .line 312
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 313
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setClientMultiVisionMode left "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLeft:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", top : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mTop:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", right : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mRight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", bottom : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBottom:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMVPlayNumber : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVPlayNumber:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMeasuredHeightRatio : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMeasuredHeightRatio:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mClientPosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVClientPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    const/4 v0, 0x0

    .line 316
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLeft:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mTop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mRight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVPlayNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMeasuredHeightRatio:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVClientPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 318
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 4
    .param p1, "contentURL"    # Ljava/lang/String;

    .prologue
    .line 256
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setDataSource()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    const/16 v1, 0x8

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBufferStatus:I

    .line 258
    const/4 v0, 0x0

    .line 259
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public setGroupMember(Z)V
    .locals 0
    .param p1, "join"    # Z

    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mGroupMember:Z

    .line 160
    return-void
.end method

.method public setHide(Z)V
    .locals 0
    .param p1, "hide"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mHide:Z

    .line 77
    return-void
.end method

.method public setMVClientPosition(I)V
    .locals 0
    .param p1, "mvClientPosition"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVClientPosition:I

    .line 65
    return-void
.end method

.method public setMVPlayNumber(I)V
    .locals 0
    .param p1, "mvPlayNumber"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVPlayNumber:I

    .line 209
    return-void
.end method

.method public setMeasuredClientCropSize(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mLeft:I

    .line 194
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mTop:I

    .line 195
    iput p3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mRight:I

    .line 196
    iput p4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mBottom:I

    .line 197
    return-void
.end method

.method public setMeasuredHeightRatio(I)V
    .locals 3
    .param p1, "measuredHeightRatio"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMeasuredHeightRatio:I

    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] setMeasuredHeightRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMeasuredHeightRatio:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_0
    return-void
.end method

.method public setOnMessageResponseListener(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    .prologue
    .line 774
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    .line 775
    return-void
.end method

.method public setOnStatusChangedListener(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    .prologue
    .line 724
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mOnStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    .line 725
    return-void
.end method

.method public setPlayPosition(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 345
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 346
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setPlayPosition() pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_0
    const/4 v0, 0x0

    .line 348
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method public setRequestDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqDirection:I

    .line 69
    return-void
.end method

.method public setRequestTouchTime(J)V
    .locals 1
    .param p1, "touchTime"    # J

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mReqTouchTime:J

    .line 85
    return-void
.end method

.method public setSVCropSize()V
    .locals 4

    .prologue
    .line 200
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 201
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setSVCropSize"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    const/4 v0, 0x0

    .line 203
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public setServerInitialInfo(Z)V
    .locals 5
    .param p1, "serverSupportMultiVision"    # Z

    .prologue
    .line 212
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 213
    const-string v2, "MVPlayerServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] setServerInitialInfo() serverSupportMultiVision : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_0
    const/4 v1, 0x0

    .line 216
    .local v1, "sMVSupport":I
    if-eqz p1, :cond_1

    .line 217
    const/4 v1, 0x1

    .line 220
    :cond_1
    const/4 v0, 0x0

    .line 221
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public setSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->setSocket(Ljava/net/Socket;)V

    .line 164
    return-void
.end method

.method public setSupportMultiVision(Z)V
    .locals 3
    .param p1, "support_multivision"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVSupport:Z

    .line 251
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "MVPlayerServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] setSupportMultiVision : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mMVSupport:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :cond_0
    return-void
.end method

.method public setVolumeMute()V
    .locals 4

    .prologue
    .line 329
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 330
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setVolumeMute "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_0
    const/4 v0, 0x0

    .line 332
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method public setVolumeOn()V
    .locals 4

    .prologue
    .line 337
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 338
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setVolumeOn "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_0
    const/4 v0, 0x0

    .line 340
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 280
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 281
    const-string v1, "MVPlayerServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] stop()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_0
    const/4 v0, 0x0

    .line 283
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 285
    return-void
.end method
