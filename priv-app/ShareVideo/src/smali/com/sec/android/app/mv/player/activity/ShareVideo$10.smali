.class Lcom/sec/android/app/mv/player/activity/ShareVideo$10;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2103
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2105
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2106
    .local v0, "action":Ljava/lang/String;
    const-string v7, "ShareVideo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mStatusReceiver - action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2108
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2243
    :cond_0
    :goto_0
    return-void

    .line 2111
    :cond_1
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2112
    const-string v6, "ShareVideo"

    const-string v7, "mStatusReceiver - ACTION_BATTERY_CHANGED."

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const-string v7, "status"

    invoke-virtual {p2, v7, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2602(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I

    .line 2114
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const-string v7, "scale"

    const/16 v8, 0x64

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2702(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I

    .line 2115
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const-string v7, "level"

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v8

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2802(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I

    .line 2117
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mStatusReceiver. battScale : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", battLevel : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", battStatus : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v6

    if-gt v6, v4, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    .line 2121
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->popupLowBattery(Landroid/app/Activity;)V
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$2900(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 2123
    :cond_2
    const-string v7, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2124
    const-string v7, "state"

    invoke-virtual {p2, v7, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v4, :cond_8

    .line 2125
    .local v4, "isHeadsetPlugged":Z
    :goto_1
    const-string v7, "ShareVideo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mStatusReceiver - receive ACTION_HEADSET_PLUG - isHeadsetPlugged : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/view/Menu;

    move-result-object v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/view/Menu;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2128
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/view/Menu;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/Menu;->close()V

    .line 2131
    :cond_3
    if-nez v4, :cond_9

    .line 2134
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 2135
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v7

    const-string v8, "sound_effect"

    invoke-virtual {v7, v8, v6}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 2138
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->invalidateOptionsMenu()V

    .line 2140
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 2141
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->setDismiss4UnpluggedHeadset()V

    .line 2142
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismiss()V

    .line 2145
    :cond_5
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2146
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v7

    if-nez v7, :cond_6

    .line 2147
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    .line 2160
    :cond_6
    :goto_2
    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z
    invoke-static {}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3200()Z

    move-result v7

    if-nez v7, :cond_7

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2162
    const/16 v7, 0x32

    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sleep(I)V

    .line 2165
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v7, :cond_7

    .line 2166
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupVolbar(Z)V

    .line 2167
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 2168
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2175
    :cond_7
    :goto_3
    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z
    invoke-static {v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3202(Z)Z

    goto/16 :goto_0

    .end local v4    # "isHeadsetPlugged":Z
    :cond_8
    move v4, v6

    .line 2124
    goto/16 :goto_1

    .line 2152
    .restart local v4    # "isHeadsetPlugged":Z
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->invalidateOptionsMenu()V

    .line 2154
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 2155
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v5

    .line 2156
    .local v5, "volume":F
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setMovieVolume(F)V

    goto :goto_2

    .line 2170
    .end local v5    # "volume":F
    :catch_0
    move-exception v1

    .line 2171
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v7, "ShareVideo"

    const-string v8, "mStatusReceiver - RuntimeException"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2176
    .end local v1    # "e":Ljava/lang/RuntimeException;
    .end local v4    # "isHeadsetPlugged":Z
    :cond_a
    const-string v7, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2177
    const-string v7, "ShareVideo"

    const-string v8, "mStatusReceiver - receive ACTION_AUDIO_BECOMING_NOISY"

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2178
    const-string v7, "android.bluetooth.a2dp.extra.DISCONNECT_A2DP"

    invoke-virtual {p2, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2180
    .local v2, "fromBT":Z
    if-eqz v2, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v7

    if-nez v7, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 2181
    :cond_b
    sget-boolean v7, Lcom/sec/android/app/mv/player/common/feature/Feature;->I_MIRROR_CALL:Z

    if-eqz v7, :cond_d

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2193
    :cond_c
    :goto_4
    if-eqz v2, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2194
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z
    invoke-static {v7, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    .line 2195
    const-string v6, "ShareVideo"

    const-string v7, "mStatusReceiver - init isTransferToDeviceSelected flag"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2184
    :cond_d
    if-eqz v2, :cond_e

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2189
    :goto_5
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    goto :goto_4

    .line 2187
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto :goto_5

    .line 2197
    .end local v2    # "fromBT":Z
    :cond_f
    const-string v7, "android.intent.action.PALM_DOWN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 2198
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2201
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v7, :cond_10

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 2202
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v7

    if-nez v7, :cond_12

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v7

    if-nez v7, :cond_12

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v7

    if-nez v7, :cond_12

    .line 2203
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 2204
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    .line 2210
    :cond_10
    :goto_6
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v6, :cond_11

    .line 2211
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 2213
    :cond_11
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v6, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setRemoveSystemUI(Z)V

    goto/16 :goto_0

    .line 2206
    :cond_12
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    goto :goto_6

    .line 2214
    :cond_13
    const-string v7, "android.intent.action.PALM_UP"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 2215
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2217
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v7, v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    goto/16 :goto_0

    .line 2218
    :cond_14
    const-string v7, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 2219
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/16 v7, 0x1a

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z
    invoke-static {v6, v7, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3400(Lcom/sec/android/app/mv/player/activity/ShareVideo;IZ)Z

    .line 2220
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/16 v7, 0xbb

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z
    invoke-static {v6, v7, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3400(Lcom/sec/android/app/mv/player/activity/ShareVideo;IZ)Z

    goto/16 :goto_0

    .line 2221
    :cond_15
    const-string v7, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2222
    const-string v7, "coverOpen"

    invoke-virtual {p2, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2223
    .local v3, "isCoverOpen":Z
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mStatusReceiver. CLEAR_COVER : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2225
    if-nez v3, :cond_0

    .line 2226
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v6, :cond_16

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 2227
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 2228
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    .line 2229
    const-string v6, "ShareVideo"

    const-string v7, "mStatusReceiver. CLEAR_COVER : Paused by Clear Cover!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2232
    :cond_16
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isLongSeekMode()Z

    move-result v6

    if-eqz v6, :cond_17

    .line 2233
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 2234
    const-string v6, "ShareVideo"

    const-string v7, "mStatusReceiver. CLEAR_COVER : LongSeekMode"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2237
    :cond_17
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v6, v6, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_0

    .line 2238
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    .line 2239
    const-string v6, "ShareVideo"

    const-string v7, "mStatusReceiver. CLEAR_COVER : PLAYER_PREPARING"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
