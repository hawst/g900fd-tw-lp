.class Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;
.super Landroid/os/Handler;
.source "VideoActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoActionBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V
    .locals 0

    .prologue
    .line 501
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 503
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 505
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_0

    .line 506
    const-string v0, "VideoActionBarView"

    const-string v1, "mHandler - exit player by H/W back key"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$100(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0009

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a006f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0060

    new-instance v3, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0016

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    # setter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mExitPopup:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$202(Lcom/sec/android/app/mv/player/view/VideoActionBarView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mExitPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$200(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$300(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$400(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$400(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeHandler()V

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$400(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto :goto_0

    .line 503
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
