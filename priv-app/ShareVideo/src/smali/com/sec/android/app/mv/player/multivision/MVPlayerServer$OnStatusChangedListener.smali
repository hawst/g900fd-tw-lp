.class public interface abstract Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;
.super Ljava/lang/Object;
.source "MVPlayerServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnStatusChangedListener"
.end annotation


# virtual methods
.method public abstract onBuffering(I)V
.end method

.method public abstract onComplete(I)V
.end method

.method public abstract onPaused(I)V
.end method

.method public abstract onPlayed(I)V
.end method

.method public abstract onPrepared(I)V
.end method

.method public abstract onReady(I)V
.end method

.method public abstract onResumed(I)V
.end method

.method public abstract onStopped(I)V
.end method
