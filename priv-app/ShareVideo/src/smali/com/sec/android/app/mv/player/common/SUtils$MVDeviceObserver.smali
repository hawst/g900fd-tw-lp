.class Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;
.super Landroid/database/Observable;
.source "SUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/common/SUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MVDeviceObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/database/Observable",
        "<",
        "Lcom/sec/android/app/mv/player/view/ParentDeviceListView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/common/SUtils;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/common/SUtils;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;->this$0:Lcom/sec/android/app/mv/player/common/SUtils;

    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/common/SUtils;Lcom/sec/android/app/mv/player/common/SUtils$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/common/SUtils;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/common/SUtils$1;

    .prologue
    .line 426
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;-><init>(Lcom/sec/android/app/mv/player/common/SUtils;)V

    return-void
.end method


# virtual methods
.method public notifyChange()V
    .locals 4

    .prologue
    .line 428
    const-string v2, "SUtils"

    const-string v3, "MVDeviceObserver - notifyChange E"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/view/ParentDeviceListView;

    .line 431
    .local v1, "listview":Lcom/sec/android/app/mv/player/view/ParentDeviceListView;
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/ParentDeviceListView;->notifyDeviceChanged()V

    goto :goto_0

    .line 433
    .end local v1    # "listview":Lcom/sec/android/app/mv/player/view/ParentDeviceListView;
    :cond_0
    return-void
.end method
