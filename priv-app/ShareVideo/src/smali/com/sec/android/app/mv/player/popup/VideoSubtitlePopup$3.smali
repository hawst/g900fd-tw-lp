.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adapterview":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 247
    if-nez p1, :cond_1

    .line 248
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "onItemClick() AdapterView is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onItemClick() position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    if-nez p3, :cond_3

    .line 254
    const v3, 0x7f0d00a0

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 256
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 257
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    goto :goto_0

    .line 260
    :cond_2
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    goto :goto_0

    .line 266
    .end local v0    # "cb":Landroid/widget/CheckBox;
    :cond_3
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 267
    .local v2, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 270
    const-string v3, "MENU_TITLE"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 271
    .local v1, "clickItem":Ljava/lang/String;
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onItemClick() item : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0091

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->clickSync()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 294
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    goto/16 :goto_0

    .line 275
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0081

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSelectSubtitlePopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto :goto_1

    .line 277
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0040

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleLanguagePopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$800(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto/16 :goto_0

    .line 280
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0030

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleFontPopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$900(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto/16 :goto_0

    .line 283
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a008f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleTextColorPopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto/16 :goto_0

    .line 286
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0031

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleSizePopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto/16 :goto_0

    .line 289
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a008e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleBackgroundColorPopup()V
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$1200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    goto/16 :goto_0
.end method
