.class Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;
.super Ljava/lang/Object;
.source "ChordService.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->start(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted(Ljava/lang/String;I)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reason"    # I

    .prologue
    const/4 v6, 0x1

    .line 254
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : onStarted chord"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # setter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mbStarted:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$102(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;Z)Z

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$200(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/chord/SchordManager;->getIp()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v3, p1, v4, v5}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;I)V

    .line 261
    :cond_0
    if-ne v6, p2, :cond_2

    .line 262
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : STARTED_BY_RECONNECTION"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 269
    :cond_2
    const/4 v0, 0x0

    .line 272
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$200(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$300(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/chord/SchordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 280
    :goto_1
    if-eqz v0, :cond_1

    .line 284
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "serverip":Ljava/lang/String;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startChord. server own ip = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/common/SUtils;->setServerIP(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    .end local v2    # "serverip":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 276
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 277
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onStopped(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 294
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onStopped - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    const/16 v0, 0x3eb

    if-ne p1, v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onNetworkDisconnected()V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mbStarted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$102(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;Z)Z

    .line 302
    return-void
.end method
