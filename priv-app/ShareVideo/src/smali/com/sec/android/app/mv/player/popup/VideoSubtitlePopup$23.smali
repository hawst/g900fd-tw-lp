.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1106
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 1109
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V

    .line 1115
    :goto_0
    return-void

    .line 1114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_0
.end method
