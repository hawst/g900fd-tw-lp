.class Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$PosCompare;
.super Ljava/lang/Object;
.source "DeviceVolumeListView.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PosCompare"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;)I
    .locals 4
    .param p1, "arg0"    # Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
    .param p2, "arg1"    # Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 349
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v0

    .line 351
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v2

    if-ne v2, v1, :cond_2

    move v0, v1

    .line 352
    goto :goto_0

    .line 355
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v3

    if-ge v2, v3, :cond_3

    move v0, v1

    .line 356
    goto :goto_0

    .line 357
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 358
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 344
    check-cast p1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$PosCompare;->compare(Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;)I

    move-result v0

    return v0
.end method
