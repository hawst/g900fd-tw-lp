.class Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;
.super Landroid/content/BroadcastReceiver;
.source "PlayerListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/playlist/PlayerListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 233
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "action":Ljava/lang/String;
    const-string v1, "VideoPlayerListView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPlaylistReceiver() action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    const-string v1, "VideoPlayerListView"

    const-string v2, "mPlaylistReceiver. Intent.ACTION_MEDIA_MOUNTED"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 240
    const-string v1, "VideoPlayerListView"

    const-string v2, "mPlaylistReceiver. Intent.ACTION_MEDIA_SCANNER_STARTED"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :cond_2
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 242
    const-string v1, "VideoPlayerListView"

    const-string v2, "mPlaylistReceiver - Intent.ACTION_MEDIA_SCANNER_FINISHED."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->access$000(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isListEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    # invokes: Lcom/sec/android/app/mv/player/playlist/PlayerListView;->restartLoader()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->access$100(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V

    goto :goto_0

    .line 246
    :cond_3
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    const-string v1, "VideoPlayerListView"

    const-string v2, "mPlaylistReceiver. Intent.ACTION_MEDIA_UNMOUNTED"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
