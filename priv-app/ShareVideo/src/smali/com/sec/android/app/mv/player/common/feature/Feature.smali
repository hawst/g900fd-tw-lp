.class public Lcom/sec/android/app/mv/player/common/feature/Feature;
.super Ljava/lang/Object;
.source "Feature.java"


# static fields
.field public static final A_MID_VOLUME_CONTROL:Z = false

.field public static final A_SOUNDALIVE_FX_SEC:Z = true

.field public static final C_CANADA:Z

.field public static final C_CHINA:Z

.field public static final C_KDRM_FEATURE:Z = false

.field public static final C_KOREA:Z

.field public static final C_USA:Z

.field public static final F_HOVERING:Z

.field public static final F_IS_DUAL_LCD:Z

.field public static final F_IS_FOLDER_TYPE:Z

.field public static final F_MULTIWINDOW_TRAY:Z

.field public static final F_OVERLAY_NOTIFICATION_BAR:Z

.field public static final F_SUPPORT_L_SCREEN_CONCEPT:Z

.field public static final F_TOOLTIP_HOVERING:Z

.field public static final GATE_MESSAGE:Z

.field public static final G_MDNIE:Z = false

.field public static final G_PLAYING_VZW:Z

.field public static final G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

.field public static final I_MIRROR_CALL:Z

.field public static final MODEL_CHAGALL:Z

.field public static final MODEL_FLTE:Z

.field public static final MODEL_FRESCO:Z

.field public static final MODEL_HA3G:Z

.field public static final MODEL_HLTE:Z

.field public static final MODEL_JA3G:Z

.field public static final MODEL_JACTIVELTE:Z

.field public static final MODEL_JACTIVELTEATT:Z

.field public static final MODEL_JALTE:Z

.field public static final MODEL_JFLTE:Z

.field public static final MODEL_JFTDD:Z

.field public static final MODEL_JFVE3G:Z

.field public static final MODEL_JFVELTE:Z

.field public static final MODEL_K:Z

.field public static final MODEL_KLIMT:Z

.field public static final MODEL_KS01:Z

.field public static final MODEL_MONDRIAN:Z

.field public static final MODEL_T:Z

.field public static final MODEL_TABLET:Z

.field public static final MODEL_UPGRADE:Z

.field public static final M_SUPPORT_AIR_DUAL_VIEW:Z

.field public static final M_SUPPORT_REALMEDIA:Z = false

.field public static final R_DUOS_CDMAGSM:Z

.field public static final V_ANIMATED_BRIGHTNESS_ICON:Z

.field public static final V_BLOCK_AIA_TVOUT:Z = true

.field public static final V_BRIGHTNESS_N_VOLUME_CONTROL:Z = true

.field public static final V_HIGH_SPEED_PLAY:Z

.field public static final V_LAST_PLAY_ICON_NOT_SUPPORT:Z

.field public static final V_ROTATE_SECOND_SCREEN:Z

.field public static final V_SUBTITLE_FONT:Z = true

.field public static final V_VIDEO_SUBTITLE:Z

.field public static final V_WMDRM_NOT_SUPPORT:Z

.field public static final X_NULL_BACKGROUND_COLOR:Z = true

.field public static final X_QUALCOMM_CELOX_BASE_CHIP:Z

.field public static final X_QUALCOMM_JF_CHIP:Z

.field public static final X_SLOW_CPU:Z

.field public static final X_SLSI_CHIP:Z

.field public static final X_SLSI_JA_CHIP:Z

.field public static final X_SLSI_Q1_U1_CHIP:Z

.field public static final X_SLSI_SMDK_CHIP:Z

.field public static final X_STE_CHIP:Z

.field public static final X_STE_GOLDEN_CHIP:Z

.field public static final X_STE_JANICE_CHIP:Z

.field public static final X_SUBTITLE_RESIZE_MODEL:Z

.field public static final X_VERY_SLOW_CPU:Z

.field public static mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 16
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/util/GateConfig;->isGateLcdtextEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->GATE_MESSAGE:Z

    .line 25
    const-string v0, "USA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_USA:Z

    .line 26
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "China"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "china"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "CHINA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CHINA:Z

    .line 30
    const-string v0, "KOREA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_KOREA:Z

    .line 32
    const-string v0, "CANADA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CANADA:Z

    .line 38
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->hasHoveringFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    .line 39
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->hasHoveringFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    .line 40
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->hasMultiWindowFeature()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_MULTIWINDOW_TRAY:Z

    .line 44
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->isFolderType()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    .line 45
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->isDualLCD()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    .line 50
    const-string v0, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_PLAYING_VZW:Z

    .line 52
    invoke-static {}, Lcom/sec/android/app/mv/player/common/feature/Feature;->isSupportAutoBrightnessDetail()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    .line 56
    sget-object v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_UWA"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->I_MIRROR_CALL:Z

    .line 60
    const/4 v0, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/feature/Feature;->isMMFWFeatureEnabled(IZ)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    .line 65
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trhlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tre3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-01G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SCL24"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "trhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tbhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_T:Z

    .line 76
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kqlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "kmini"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "m2lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "m2alte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "m2a3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-04F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "lentis"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "slte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mega2lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_3
    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    .line 89
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ks01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_KS01:Z

    .line 90
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jflte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-04E"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_4
    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFLTE:Z

    .line 92
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jfvelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFVELTE:Z

    .line 93
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jfve3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFVE3G:Z

    .line 94
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jftdd"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFTDD:Z

    .line 95
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jactivelteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JACTIVELTEATT:Z

    .line 96
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jactivelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JACTIVELTE:Z

    .line 97
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "jalte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JALTE:Z

    .line 98
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ja3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JA3G:Z

    .line 99
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "hlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-01F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SCL22"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-02F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_5
    move v0, v2

    :goto_5
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_HLTE:Z

    .line 103
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ha3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_HA3G:Z

    .line 104
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "flte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_FLTE:Z

    .line 105
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "fresco"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "hl3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "hllte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_6
    move v0, v2

    :goto_6
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_FRESCO:Z

    .line 106
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mondrian"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_MONDRIAN:Z

    .line 107
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "chagall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "KEW_jp_kdi"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SCT21"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_7
    move v0, v2

    :goto_7
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_CHAGALL:Z

    .line 110
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "klimt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SC-03G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_8
    move v0, v2

    :goto_8
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_KLIMT:Z

    .line 113
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFLTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFTDD:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JALTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JACTIVELTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JACTIVELTEATT:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFVELTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JFVE3G:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_JA3G:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_HLTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_HA3G:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_KS01:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_FLTE:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_FRESCO:Z

    if-eqz v0, :cond_19

    :cond_9
    move v0, v2

    :goto_9
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    .line 115
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    .line 118
    sget-object v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_RIL_SIM_DUOS_GSMGSM"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->R_DUOS_CDMAGSM:Z

    .line 127
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ANIMATED_BRIGHTNESS_ICON:Z

    .line 132
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_USA:Z

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_WMDRM_NOT_SUPPORT:Z

    .line 134
    sget-object v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_VIDEOPLAYER_SUPPORT_SUBTITLE"

    invoke-virtual {v0, v3, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_VIDEO_SUBTITLE:Z

    .line 138
    sget-object v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_VIDEOPLAYER_HIGH_SPEED_PLAY"

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_HIGH_SPEED_PLAY:Z

    .line 144
    const-string v0, "ATT"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_LAST_PLAY_ICON_NOT_SUPPORT:Z

    .line 146
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CHINA:Z

    if-eqz v0, :cond_1a

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_1a

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v0, :cond_1a

    move v0, v2

    :goto_a
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    .line 149
    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "melius"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "ro.product.device"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ks02"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_a
    move v0, v2

    :goto_b
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SUBTITLE_RESIZE_MODEL:Z

    .line 152
    const-string v0, "smdk4210"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_Q1_U1_CHIP:Z

    .line 153
    const-string v0, "ro.product.board"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "smdk"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "ro.product.board"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SMDK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_b
    move v0, v2

    :goto_c
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_SMDK_CHIP:Z

    .line 154
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_SMDK_CHIP:Z

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_CHIP:Z

    .line 157
    const-string v0, "universal5410"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_JA_CHIP:Z

    .line 163
    const-string v0, "DB8520H"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_GOLDEN_CHIP:Z

    .line 164
    const-string v0, "montblanc"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_JANICE_CHIP:Z

    .line 165
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_GOLDEN_CHIP:Z

    if-nez v0, :cond_c

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_JANICE_CHIP:Z

    if-eqz v0, :cond_1d

    :cond_c
    move v0, v2

    :goto_d
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_CHIP:Z

    .line 168
    const-string v0, "MSM8660_SURF"

    const-string v3, "ro.product.board"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_QUALCOMM_CELOX_BASE_CHIP:Z

    .line 170
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_JANICE_CHIP:Z

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_VERY_SLOW_CPU:Z

    .line 172
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_VERY_SLOW_CPU:Z

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_Q1_U1_CHIP:Z

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_STE_GOLDEN_CHIP:Z

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_QUALCOMM_CELOX_BASE_CHIP:Z

    if-eqz v0, :cond_1e

    :cond_d
    move v0, v2

    :goto_e
    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLOW_CPU:Z

    .line 179
    const-string v0, "grande"

    const-string v3, "ro.build.scafe.size"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "tall"

    const-string v3, "ro.build.scafe.size"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_e
    move v1, v2

    :cond_f
    sput-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    .line 181
    const-string v0, "latte"

    const-string v1, "ro.build.scafe"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_OVERLAY_NOTIFICATION_BAR:Z

    return-void

    :cond_10
    move v0, v1

    .line 16
    goto/16 :goto_0

    :cond_11
    move v0, v1

    .line 26
    goto/16 :goto_1

    :cond_12
    move v0, v1

    .line 65
    goto/16 :goto_2

    :cond_13
    move v0, v1

    .line 76
    goto/16 :goto_3

    :cond_14
    move v0, v1

    .line 90
    goto/16 :goto_4

    :cond_15
    move v0, v1

    .line 99
    goto/16 :goto_5

    :cond_16
    move v0, v1

    .line 105
    goto/16 :goto_6

    :cond_17
    move v0, v1

    .line 107
    goto/16 :goto_7

    :cond_18
    move v0, v1

    .line 110
    goto/16 :goto_8

    :cond_19
    move v0, v1

    .line 113
    goto/16 :goto_9

    :cond_1a
    move v0, v1

    .line 146
    goto/16 :goto_a

    :cond_1b
    move v0, v1

    .line 149
    goto/16 :goto_b

    :cond_1c
    move v0, v1

    .line 153
    goto/16 :goto_c

    :cond_1d
    move v0, v1

    .line 165
    goto :goto_d

    :cond_1e
    move v0, v1

    .line 172
    goto :goto_e
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static hasHoveringFeature()Z
    .locals 2

    .prologue
    .line 201
    invoke-static {}, Lcom/sec/android/app/mv/player/common/ShareVideoApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 202
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 203
    const/4 v1, 0x0

    .line 205
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static hasMultiWindowFeature()Z
    .locals 2

    .prologue
    .line 209
    invoke-static {}, Lcom/sec/android/app/mv/player/common/ShareVideoApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 210
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 211
    const/4 v1, 0x0

    .line 213
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isDualLCD()Z
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcom/sec/android/app/mv/player/common/ShareVideoApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 194
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 195
    const/4 v1, 0x0

    .line 197
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.dual_lcd"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isFolderType()Z
    .locals 2

    .prologue
    .line 185
    invoke-static {}, Lcom/sec/android/app/mv/player/common/ShareVideoApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 186
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 187
    const/4 v1, 0x0

    .line 189
    :goto_0
    return v1

    :cond_0
    const-string v1, "com.sec.feature.folder_type"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isMMFWFeatureEnabled(IZ)Z
    .locals 1
    .param p0, "feature"    # I
    .param p1, "staticFeature"    # Z

    .prologue
    .line 228
    :try_start_0
    invoke-static {p0}, Landroid/media/SecMMFWConfiguration;->isEnabledFeature(I)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 231
    .end local p1    # "staticFeature":Z
    :goto_0
    return p1

    .line 229
    .restart local p1    # "staticFeature":Z
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static isSupportAutoBrightnessDetail()Z
    .locals 4

    .prologue
    .line 217
    const/4 v0, 0x1

    .line 218
    .local v0, "result":Z
    sget-object v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->mSFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v2, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_AUTOMATIC_BRIGHTNESS_DETAIL"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "off"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Setting_ConfigAutomaticBrightnessDetail"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    :cond_0
    const/4 v0, 0x0

    .line 223
    :cond_1
    return v0
.end method
