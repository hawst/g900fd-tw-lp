.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;
.super Landroid/os/Handler;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceInUse:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1100(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$5;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stopSelf()V

    goto :goto_0
.end method
