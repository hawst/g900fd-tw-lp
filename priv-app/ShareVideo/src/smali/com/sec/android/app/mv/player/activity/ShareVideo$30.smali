.class Lcom/sec/android/app/mv/player/activity/ShareVideo$30;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Lcom/samsung/android/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 3003
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 4
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 3005
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    .line 3006
    .local v0, "motion":I
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMotionListener : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3008
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3024
    :cond_0
    :goto_0
    return-void

    .line 3011
    :cond_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 3013
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3014
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isPauseWorkByMotion()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3015
    const-string v1, "ShareVideo"

    const-string v2, "Turn Over : MoviePlayer pausePlaying"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3016
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto :goto_0

    .line 3011
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
