.class public Lcom/sec/android/app/mv/player/common/VUtils;
.super Ljava/lang/Object;
.source "VUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/common/VUtils$myView;,
        Lcom/sec/android/app/mv/player/common/VUtils$Color;
    }
.end annotation


# static fields
.field public static final HEIGHT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Utils"

.field public static final TOAST_AUDIO_ONLY:I = 0x6

.field public static final TOAST_BACK_KEY_INFO:I = 0xb

.field public static final TOAST_DURING_CALL:I = 0x0

.field public static final TOAST_EXIT_PRESENTATION_FOR_ANOTHER_APP:I = 0x9

.field public static final TOAST_EXIT_VIDEO_PLAYER:I = 0x7

.field public static final TOAST_EXIT_VIDEO_PLAYER_FOR_ANOTHER_PLAYING:I = 0x8

.field public static final TOAST_FILE_ERROR:I = 0x4

.field public static final TOAST_NO_RESULT_VIDEO_FOUND:I = 0x3

.field public static final TOAST_RESULT_VIDEO_FOUND:I = 0x2

.field public static final TOAST_START_SCANNING:I = 0x1

.field public static final TOAST_VIDEO_ONLY:I = 0x5

.field public static final TOAST_ZOOM_NOT_SUPPORTED:I = 0xa

.field public static final VIDEO_LANDSCAPE_VIEW:I = 0x0

.field public static final VIDEO_ORIENTATION_UNSPECIFIED:I = -0x1

.field public static final VIDEO_PORTRAIT_VIEW:I = 0x1

.field public static final WIDTH:I = 0x0

.field public static final XPOS:I = 0x0

.field public static final YPOS:I = 0x1

.field private static mBusBooster:Landroid/os/DVFSHelper;

.field private static mCpuBooster:Landroid/os/DVFSHelper;

.field private static final mResID:[I

.field private static mScreenOrientation:I

.field private static mSecondScreenOrientation:I

.field private static mSecondScreenUserOrientation:I

.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/common/VUtils;

.field private static mUserOrientation:I


# instance fields
.field mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mPrevColor:Lcom/sec/android/app/mv/player/common/VUtils$Color;

.field private mToast:[Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 41
    new-instance v0, Lcom/sec/android/app/mv/player/common/VUtils;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/common/VUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/common/VUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/VUtils;

    .line 61
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/mv/player/common/VUtils;->mResID:[I

    .line 81
    sput-object v2, Lcom/sec/android/app/mv/player/common/VUtils;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 84
    sput-object v2, Lcom/sec/android/app/mv/player/common/VUtils;->mBusBooster:Landroid/os/DVFSHelper;

    .line 567
    sput v1, Lcom/sec/android/app/mv/player/common/VUtils;->mScreenOrientation:I

    .line 568
    sput v1, Lcom/sec/android/app/mv/player/common/VUtils;->mUserOrientation:I

    .line 569
    sput v1, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenOrientation:I

    .line 570
    sput v1, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenUserOrientation:I

    return-void

    .line 61
    nop

    :array_0
    .array-data 4
        0x7f0a002d
        0x7f0a00ac
        0x7f0a00aa
        0x7f0a00ab
        0x7f0a002f
        0x7f0a00ca
        0x7f0a000a
        0x7f0a0008
        0x7f0a008c
        0x7f0a0007
        0x7f0a00d5
        0x7f0a0010
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 76
    const/16 v0, 0xc

    new-array v0, v0, [Landroid/widget/Toast;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    aput-object v2, v0, v1

    const/16 v1, 0xb

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    .line 458
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->NONE:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mPrevColor:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    .line 676
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/mv/player/common/VUtils;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/VUtils;

    return-object v0
.end method

.method public static getScreenOrientation()I
    .locals 1

    .prologue
    .line 590
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mScreenOrientation:I

    return v0
.end method

.method public static getSecondScreenOrientation()I
    .locals 1

    .prologue
    .line 613
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenOrientation:I

    return v0
.end method

.method public static getSecondScreenUserOrientation()I
    .locals 1

    .prologue
    .line 636
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenUserOrientation:I

    return v0
.end method

.method public static getUserOrientation()I
    .locals 1

    .prologue
    .line 582
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mUserOrientation:I

    return v0
.end method

.method public static isAutoRotation(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 594
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isFolderClose(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 640
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 641
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 642
    const/4 v1, 0x1

    .line 644
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isFolderOpen(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 649
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 650
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v1, :cond_0

    .line 653
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLandscape()Z
    .locals 1

    .prologue
    .line 605
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mScreenOrientation:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLandscape(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 598
    if-nez p0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape()Z

    move-result v1

    .line 601
    :goto_0
    return v1

    .line 600
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 601
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSecondScreenAutoRotation(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 617
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation_second"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isSecondScreenLandscape()Z
    .locals 1

    .prologue
    .line 628
    sget v0, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenOrientation:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecondScreenLandscape(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 621
    if-nez p0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenLandscape()Z

    move-result v1

    .line 624
    :goto_0
    return v1

    .line 623
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 624
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized setScreenOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 586
    const-class v0, Lcom/sec/android/app/mv/player/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/mv/player/common/VUtils;->mScreenOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    monitor-exit v0

    return-void

    .line 586
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSecondScreenOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 609
    const-class v0, Lcom/sec/android/app/mv/player/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 610
    monitor-exit v0

    return-void

    .line 609
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSecondScreenUserOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 632
    const-class v0, Lcom/sec/android/app/mv/player/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/mv/player/common/VUtils;->mSecondScreenUserOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    monitor-exit v0

    return-void

    .line 632
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setUserOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 578
    const-class v0, Lcom/sec/android/app/mv/player/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/mv/player/common/VUtils;->mUserOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 579
    monitor-exit v0

    return-void

    .line 578
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public acquireBusBooster()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public acquireCpuBooster()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public changeActionLayoutForUpgradeModels(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0d0050

    const v5, 0x7f0d004f

    const v2, 0x7f0d004d

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 728
    if-nez p1, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v1, :cond_3

    .line 731
    const v1, 0x7f0d0051

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 732
    const v1, 0x7f0d0053

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 733
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 734
    .local v0, "LP_Group":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 735
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 736
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 737
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 739
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 740
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "01"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 741
    const v1, 0x7f0d0048

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 748
    .end local v0    # "LP_Group":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    :goto_1
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-eqz v1, :cond_0

    .line 749
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 750
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "01"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 743
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v1, :cond_2

    .line 744
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 745
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public changeTitleLayoutForUpgradeModels(Landroid/content/Context;Landroid/view/View;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x9

    const/4 v4, 0x1

    const v7, 0x7f0d00b2

    const v6, 0x7f0d00b0

    const/4 v5, 0x0

    .line 697
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v3, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 725
    :cond_0
    :goto_0
    return-void

    .line 699
    :cond_1
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 700
    .local v0, "LP_RotateBtn":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 701
    .local v2, "LP_VolumeBtn":Landroid/widget/RelativeLayout$LayoutParams;
    const v3, 0x7f0d00b3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 703
    .local v1, "LP_Title_text":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 704
    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 706
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 707
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 709
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 710
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 712
    invoke-virtual {v1, v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 713
    invoke-virtual {v1, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 715
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08008d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 716
    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 718
    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 719
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080202

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 722
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 723
    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 724
    const v3, 0x7f0d00b3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/mv/player/common/VUtils$Color;)V
    .locals 2
    .param p1, "win"    # Landroid/view/Window;
    .param p2, "color"    # Lcom/sec/android/app/mv/player/common/VUtils$Color;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mPrevColor:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    if-eq v0, p2, :cond_0

    .line 466
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->NULL:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    if-ne p2, v0, :cond_1

    .line 467
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 468
    const-string v0, "Utils"

    const-string v1, "changeWindowBackgroundColor NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mPrevColor:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    .line 478
    return-void

    .line 469
    :cond_1
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->BLACK:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    if-ne p2, v0, :cond_2

    .line 470
    const v0, 0x7f07000a

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 471
    const-string v0, "Utils"

    const-string v1, "changeWindowBackgroundColor BLACK"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 472
    :cond_2
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->TRANSPARENCY:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    if-ne p2, v0, :cond_0

    .line 473
    const v0, 0x7f070060

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 474
    const-string v0, "Utils"

    const-string v1, "changeWindowBackgroundColor TRANSPARENCY"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public changeWindowBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$Color;)V
    .locals 1
    .param p1, "color"    # Lcom/sec/android/app/mv/player/common/VUtils$Color;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/mv/player/common/VUtils$Color;)V

    .line 483
    :cond_0
    return-void
.end method

.method public checkIsCalling(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 115
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->I_MIRROR_CALL:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 141
    :goto_0
    return v3

    .line 119
    :cond_0
    :try_start_0
    const-string v3, "audio"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    .line 120
    .local v0, "audio_mode":I
    const-string v3, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkIsCalling() - audiomode = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    if-eq v0, v5, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    .line 122
    :cond_1
    const-string v3, "Utils"

    const-string v6, "checkIsCalling() - An audio/video chat or VoIP call is established."

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v5

    .line 123
    goto :goto_0

    .line 125
    .end local v0    # "audio_mode":I
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Utils"

    const-string v6, "checkIsCalling() - Fail to get an AUDIO_SERVICE !!!"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 138
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    const-string v3, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TelephonyManager checkIsCalling tm.getCallState()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v3

    if-eqz v3, :cond_3

    move v3, v5

    .line 140
    goto :goto_0

    :cond_3
    move v3, v4

    .line 141
    goto :goto_0
.end method

.method public destroyToast()V
    .locals 3

    .prologue
    .line 428
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    if-eqz v1, :cond_0

    .line 429
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 429
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public dropLCDfps(Z)Z
    .locals 4
    .param p1, "set"    # Z

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v1, :cond_0

    .line 100
    const-string v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dropLCDfps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 102
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string v1, "SSRM_STATUS_NAME"

    const-string v2, "MoviePlayer_play"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 105
    const-string v1, "PackageName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v1, "PID"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 108
    const/4 v1, 0x1

    .line 111
    .end local v0    # "boostIntent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 110
    :cond_0
    const-string v1, "Utils"

    const-string v2, "dropLCDfps : mApp is null please re-call setVUtilsData()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInitAIAxyPosition(III)[I
    .locals 7
    .param p1, "screenWidth"    # I
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 377
    const/4 v2, 0x2

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 378
    .local v0, "i":[I
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/mv/player/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v1

    .line 379
    .local v1, "w":[I
    aget v2, v1, v5

    sub-int v2, p1, v2

    add-int/lit8 v2, v2, -0xa

    aput v2, v0, v5

    .line 380
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08009f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v0, v6

    .line 381
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInitAIAxyPosition : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    return-object v0

    .line 377
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getScreenWidthHeight(Landroid/content/Context;)[I
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 386
    const/4 v3, 0x2

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 387
    .local v1, "i":[I
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 388
    .local v0, "fullScreen":Landroid/graphics/Point;
    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 389
    .local v2, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 390
    const/4 v3, 0x0

    iget v4, v0, Landroid/graphics/Point;->x:I

    aput v4, v1, v3

    .line 391
    const/4 v3, 0x1

    iget v4, v0, Landroid/graphics/Point;->y:I

    aput v4, v1, v3

    .line 392
    return-object v1

    .line 386
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getVideoClipWidthHeight(Ljava/lang/String;)[I
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 528
    const/4 v9, 0x2

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    .line 529
    .local v2, "i":[I
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 530
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 532
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 533
    const/16 v9, 0x12

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    .line 534
    .local v7, "sWidth":Ljava/lang/String;
    const/16 v9, 0x13

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    .line 535
    .local v5, "sHeight":Ljava/lang/String;
    const/16 v9, 0x18

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    .line 537
    .local v6, "sOrientation":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 538
    .local v8, "width":I
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 539
    .local v1, "height":I
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 541
    .local v3, "orientation":I
    if-eqz v3, :cond_0

    const/16 v9, 0xb4

    if-ne v3, v9, :cond_1

    .line 542
    :cond_0
    const/4 v9, 0x0

    aput v8, v2, v9

    .line 543
    const/4 v9, 0x1

    aput v1, v2, v9

    .line 549
    :goto_0
    const-string v9, "Utils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getVideoClipWidthHeight "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " & "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 558
    .end local v1    # "height":I
    .end local v3    # "orientation":I
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v5    # "sHeight":Ljava/lang/String;
    .end local v6    # "sOrientation":Ljava/lang/String;
    .end local v7    # "sWidth":Ljava/lang/String;
    .end local v8    # "width":I
    :goto_1
    return-object v2

    .line 545
    .restart local v1    # "height":I
    .restart local v3    # "orientation":I
    .restart local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v5    # "sHeight":Ljava/lang/String;
    .restart local v6    # "sOrientation":Ljava/lang/String;
    .restart local v7    # "sWidth":Ljava/lang/String;
    .restart local v8    # "width":I
    :cond_1
    const/4 v9, 0x0

    :try_start_1
    aput v1, v2, v9

    .line 546
    const/4 v9, 0x1

    aput v8, v2, v9
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 550
    .end local v1    # "height":I
    .end local v3    # "orientation":I
    .end local v5    # "sHeight":Ljava/lang/String;
    .end local v6    # "sOrientation":Ljava/lang/String;
    .end local v7    # "sWidth":Ljava/lang/String;
    .end local v8    # "width":I
    :catch_0
    move-exception v0

    .line 551
    .local v0, "ex":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v9, "Utils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getVideoClipWidthHeight RuntimeException occured : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 553
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v9

    .line 556
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_2
    const-string v9, "Utils"

    const-string v10, "getVideoClipWidthHeight path is null or empty!!!"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 528
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getWidthAndHeight(II)[I
    .locals 3
    .param p1, "videoWidth"    # I
    .param p2, "videoHeight"    # I

    .prologue
    .line 353
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 354
    .local v0, "initWidth":I
    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/mv/player/common/VUtils;->getWidthAndHeight(III)[I

    move-result-object v1

    return-object v1
.end method

.method public getWidthAndHeight(III)[I
    .locals 7
    .param p1, "windowWidth"    # I
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 358
    const/4 v4, 0x2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    .line 360
    .local v0, "i":[I
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 361
    :cond_0
    div-int/lit8 v4, p1, 0x2

    aput v4, v0, v3

    .line 362
    aget v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    aput v3, v0, v2

    .line 373
    :goto_0
    return-object v0

    .line 364
    :cond_1
    if-le p2, p3, :cond_2

    move v1, v2

    .line 365
    .local v1, "isWideClip":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 366
    aput p1, v0, v3

    .line 367
    int-to-float v3, p1

    int-to-float v4, p2

    int-to-float v5, p3

    div-float/2addr v4, v5

    div-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v0, v2

    goto :goto_0

    .end local v1    # "isWideClip":Z
    :cond_2
    move v1, v3

    .line 364
    goto :goto_1

    .line 369
    .restart local v1    # "isWideClip":Z
    :cond_3
    int-to-float v4, p1

    int-to-float v5, p3

    int-to-float v6, p2

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v0, v3

    .line 370
    aput p1, v0, v2

    goto :goto_0

    .line 358
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public isInformationPreviewEnable(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 664
    if-nez p1, :cond_0

    .line 665
    const-string v2, "Utils"

    const-string v4, "isInformationPreviewEnable context is NULL"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :goto_0
    return v3

    .line 669
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 670
    .local v0, "fingerAirView":Z
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view_information_preview"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    .line 672
    .local v1, "fingerAirViewInformationPreview":Z
    :goto_2
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isInformationPreviewEnable E. fingerAirView : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", fingerAirViewInformationPreview : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    :goto_3
    move v3, v2

    goto :goto_0

    .end local v0    # "fingerAirView":Z
    .end local v1    # "fingerAirViewInformationPreview":Z
    :cond_1
    move v0, v3

    .line 669
    goto :goto_1

    .restart local v0    # "fingerAirView":Z
    :cond_2
    move v1, v3

    .line 670
    goto :goto_2

    .restart local v1    # "fingerAirViewInformationPreview":Z
    :cond_3
    move v2, v3

    .line 673
    goto :goto_3
.end method

.method public isLayoutShowing(Lcom/sec/android/app/mv/player/common/VUtils$myView;)Z
    .locals 4
    .param p1, "view"    # Lcom/sec/android/app/mv/player/common/VUtils$myView;

    .prologue
    .line 682
    const/4 v0, 0x0

    .line 684
    .local v0, "cont":I
    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    if-eq p1, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    add-int/lit8 v0, v0, 0x1

    .line 688
    :cond_0
    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleViewVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 689
    add-int/lit8 v0, v0, 0x1

    .line 692
    :cond_1
    const-string v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLayoutShowing : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    if-lez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMWTrayOpen()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 515
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_MULTIWINDOW_TRAY:Z

    if-eqz v3, :cond_1

    .line 517
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "multi_window_expanded"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-ne v3, v1, :cond_0

    .line 524
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 517
    goto :goto_0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "Utils"

    const-string v3, "isMWTrayOpen : NullPointerException"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_1
    :goto_1
    move v1, v2

    .line 524
    goto :goto_0

    .line 520
    :catch_1
    move-exception v0

    .line 521
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v1, "Utils"

    const-string v3, "isMWTrayOpen : SettingNotFoundException"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isMultiWindowMode()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method

.method public isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p1, "PackageName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 337
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 339
    .local v1, "pkm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to find Package : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPortraitVideoClip(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 573
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->getVideoClipWidthHeight(Ljava/lang/String;)[I

    move-result-object v0

    .line 574
    .local v0, "i":[I
    aget v3, v0, v1

    aget v4, v0, v2

    if-le v3, v4, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isRmsConnected(Landroid/content/Context;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 146
    const/4 v8, 0x0

    .line 147
    .local v8, "status":I
    const/4 v6, 0x0

    .line 149
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 150
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 151
    const-string v1, "content://com.lguplus.rms/service"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 152
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    const-string v1, "connected"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 159
    :cond_0
    if-eqz v6, :cond_1

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 161
    const/4 v6, 0x0

    .line 164
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    if-ne v8, v9, :cond_3

    move v1, v9

    :goto_1
    return v1

    .line 156
    :catch_0
    move-exception v7

    .line 157
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "Utils"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    if-eqz v6, :cond_1

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 161
    const/4 v6, 0x0

    goto :goto_0

    .line 159
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 161
    const/4 v6, 0x0

    :cond_2
    throw v1

    .line 164
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 435
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 436
    const-string v3, "activity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 437
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v3, 0x64

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 439
    .local v2, "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-nez v2, :cond_0

    move v3, v4

    .line 448
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :goto_0
    return v3

    .line 443
    .restart local v0    # "am":Landroid/app/ActivityManager;
    .restart local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 444
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    const/4 v3, 0x1

    goto :goto_0

    .line 443
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i":I
    .end local v2    # "serviceList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_2
    move v3, v4

    .line 448
    goto :goto_0
.end method

.method public isTalkBackOn(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 317
    if-nez p1, :cond_1

    .line 318
    const-string v3, "Utils"

    const-string v4, "isTalkBackOn E. context is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    :goto_0
    return v2

    .line 322
    :cond_1
    const/4 v0, 0x0

    .line 325
    .local v0, "check":I
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "accessibility_enabled"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 330
    :goto_1
    if-ne v0, v3, :cond_0

    move v2, v3

    .line 331
    goto :goto_0

    .line 326
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v4, "Utils"

    const-string v5, "isTalkBackOn. SettingNotFoundException occure"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public releaseBusBooster()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public releaseCpuBooster()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public resetColor()V
    .locals 1

    .prologue
    .line 461
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->NONE:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mPrevColor:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    .line 462
    return-void
.end method

.method public setHapticEffect(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 658
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "finger_air_view_sound_and_haptic_feedback"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 659
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 661
    :cond_0
    return-void
.end method

.method public setNullBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$myView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/app/mv/player/common/VUtils$myView;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->isLayoutShowing(Lcom/sec/android/app/mv/player/common/VUtils$myView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    .line 512
    :cond_0
    return-void
.end method

.method public setVUtilsData(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 0
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/VUtils;->resetColor()V

    .line 93
    return-void
.end method

.method public setWindowBackgroundColor()V
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    .line 506
    :cond_0
    return-void
.end method

.method public setWindowBackgroundColor(Landroid/view/Window;)V
    .locals 3
    .param p1, "win"    # Landroid/view/Window;

    .prologue
    .line 486
    if-nez p1, :cond_0

    .line 487
    const-string v0, "Utils"

    const-string v1, "setWindowBackgroundColor win is NULL!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :goto_0
    return-void

    .line 492
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$Color;->NULL:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/mv/player/common/VUtils$Color;)V

    .line 493
    const-string v0, "Utils"

    const-string v1, "setWindowBackgroundColor null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWindowBackgroundColor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.product.board"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setWindowFlag(Landroid/view/Window;Ljava/lang/String;)V
    .locals 7
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 258
    const-string v4, "Utils"

    const-string v5, "setWindowFlag E:"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v4, p2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 263
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 264
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 265
    .local v3, "privateFlags":I
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 266
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 267
    invoke-virtual {p1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 282
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :goto_0
    return-void

    .line 269
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 271
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - SecurityException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NullPointerException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 275
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NoSuchFieldException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalArgumentException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 279
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 280
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalAccessException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public setWindowFlag(Ljava/lang/String;)V
    .locals 1
    .param p1, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Landroid/view/Window;Ljava/lang/String;)V

    .line 255
    :cond_0
    return-void
.end method

.method public showToast(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "what"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 397
    if-nez p1, :cond_1

    .line 398
    const-string v1, "Utils"

    const-string v2, "showToast context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-static {v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->stackTrace(I)V

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    array-length v1, v1

    if-ge p2, v1, :cond_2

    if-gez p2, :cond_3

    .line 405
    :cond_2
    const-string v1, "Utils"

    const-string v2, "showToast what is out of bound"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-static {v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->stackTrace(I)V

    goto :goto_0

    .line 410
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    aget-object v1, v1, p2

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    sget-object v2, Lcom/sec/android/app/mv/player/common/VUtils;->mResID:[I

    aget v2, v2, p2

    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    aput-object v2, v1, p2

    .line 423
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 412
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    aget-object v1, v1, p2

    if-nez v1, :cond_4

    .line 414
    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils;->mResID:[I

    aget v1, v1, p2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const v3, 0x7f0a0009

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mToast:[Landroid/widget/Toast;

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    aput-object v2, v1, p2

    goto :goto_1

    .line 410
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public unsetWindowFlag(Landroid/view/Window;Ljava/lang/String;)V
    .locals 7
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 290
    const-string v4, "Utils"

    const-string v5, "setWindowFlag E:"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v4, p2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 295
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 296
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 297
    .local v3, "privateFlags":I
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 298
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    xor-int/lit8 v5, v3, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 299
    invoke-virtual {p1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 314
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :goto_0
    return-void

    .line 301
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 303
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - SecurityException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 305
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NullPointerException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 307
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NoSuchFieldException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 309
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalArgumentException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 311
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "Utils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalAccessException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public unsetWindowFlag(Ljava/lang/String;)V
    .locals 1
    .param p1, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/VUtils;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->unsetWindowFlag(Landroid/view/Window;Ljava/lang/String;)V

    .line 287
    :cond_0
    return-void
.end method
