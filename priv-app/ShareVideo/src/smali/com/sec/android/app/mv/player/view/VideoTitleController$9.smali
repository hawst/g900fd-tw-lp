.class Lcom/sec/android/app/mv/player/view/VideoTitleController$9;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0

    .prologue
    .line 755
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 757
    const/4 v1, 0x0

    .line 758
    .local v1, "retVal":Z
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v3, p2, p3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$400(Lcom/sec/android/app/mv/player/view/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 759
    .local v0, "isReturn":Z
    if-eqz v0, :cond_0

    move v2, v1

    .line 773
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 761
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    move v2, v1

    .line 773
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 763
    .end local v2    # "retVal":I
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 764
    goto :goto_1

    .line 767
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 768
    goto :goto_1

    .line 761
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
