.class public final Lcom/sec/android/app/mv/player/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Buffering:I = 0x7f0a00d6

.field public static final Error_Subtitles:I = 0x7f0a0000

.field public static final IDS_COM_BODY_NETWORK_ERROR_OCCURRED:I = 0x7f0a0001

.field public static final IDS_COM_POP_EXPIREAFTERFIRSTUSE:I = 0x7f0a0002

.field public static final IDS_DRM_BODY_IMPOSSIBLE:I = 0x7f0a00d7

.field public static final IDS_DRM_BODY_RIGHTSTATUS_FORWARDING:I = 0x7f0a00d8

.field public static final Nolanguage:I = 0x7f0a0003

.field public static final ShareList_done:I = 0x7f0a00d9

.field public static final actionbar_icon_name_participants:I = 0x7f0a0004

.field public static final actionbar_more_menu:I = 0x7f0a0005

.field public static final add:I = 0x7f0a0006

.field public static final add_bookmark:I = 0x7f0a00da

.field public static final all_videos_cloud:I = 0x7f0a00db

.field public static final allshare_error_fail:I = 0x7f0a00dc

.field public static final allsharecast_exit_msg:I = 0x7f0a0007

.field public static final app_in_app_exit_dialog:I = 0x7f0a0008

.field public static final app_name:I = 0x7f0a0009

.field public static final ask_allshare_close:I = 0x7f0a00dd

.field public static final att_mobile_share:I = 0x7f0a00de

.field public static final audio_play_only:I = 0x7f0a000a

.field public static final audio_track:I = 0x7f0a000b

.field public static final auto_brightness_check:I = 0x7f0a000c

.field public static final autoon:I = 0x7f0a000d

.field public static final autoplayeoff:I = 0x7f0a000e

.field public static final autoplayeon:I = 0x7f0a000f

.field public static final autoplaysettingtitle:I = 0x7f0a00df

.field public static final back_key_infromation:I = 0x7f0a0010

.field public static final black_color:I = 0x7f0a0011

.field public static final blue_color:I = 0x7f0a0012

.field public static final bookmark_added:I = 0x7f0a00e0

.field public static final bookmark_already_exists:I = 0x7f0a00e1

.field public static final bright:I = 0x7f0a0013

.field public static final brightness_text_check:I = 0x7f0a0014

.field public static final brightness_text_uncheck:I = 0x7f0a0015

.field public static final cancel:I = 0x7f0a0016

.field public static final cannot_play_video:I = 0x7f0a0017

.field public static final cannot_shared:I = 0x7f0a00e2

.field public static final category:I = 0x7f0a00e3

.field public static final change_subtitle_file:I = 0x7f0a0018

.field public static final chapterview_name:I = 0x7f0a00e4

.field public static final client_disconnet:I = 0x7f0a0019

.field public static final close:I = 0x7f0a001a

.field public static final cold:I = 0x7f0a00e5

.field public static final colortone:I = 0x7f0a00e6

.field public static final constraint_details:I = 0x7f0a00e7

.field public static final constraint_tag:I = 0x7f0a00e8

.field public static final count:I = 0x7f0a001b

.field public static final count_single_folder:I = 0x7f0a001c

.field public static final count_single_video:I = 0x7f0a001d

.field public static final count_video:I = 0x7f0a001e

.field public static final delete:I = 0x7f0a001f

.field public static final delete_complete:I = 0x7f0a00e9

.field public static final delete_fail:I = 0x7f0a00ea

.field public static final delete_list_title:I = 0x7f0a00eb

.field public static final delete_popup_all:I = 0x7f0a00ec

.field public static final delete_popup_multi:I = 0x7f0a0020

.field public static final delete_popup_nitems:I = 0x7f0a00ed

.field public static final delete_popup_single:I = 0x7f0a0021

.field public static final delete_popup_text:I = 0x7f0a00ee

.field public static final delete_popup_title:I = 0x7f0a00ef

.field public static final details_date:I = 0x7f0a00f0

.field public static final details_duration:I = 0x7f0a00f1

.field public static final details_format:I = 0x7f0a00f2

.field public static final details_name:I = 0x7f0a00f3

.field public static final details_resolution:I = 0x7f0a00f4

.field public static final details_size:I = 0x7f0a00f5

.field public static final do_not_ask_again:I = 0x7f0a0022

.field public static final done:I = 0x7f0a0023

.field public static final dot:I = 0x7f0a0024

.field public static final download_done:I = 0x7f0a00f6

.field public static final drm_MTP_connect_block:I = 0x7f0a0025

.field public static final drm_count_expired:I = 0x7f0a0026

.field public static final drm_date_expired:I = 0x7f0a0027

.field public static final drm_exit_popup:I = 0x7f0a0028

.field public static final drm_replay_popup:I = 0x7f0a0029

.field public static final drm_tag:I = 0x7f0a00f7

.field public static final drm_warning_popup1:I = 0x7f0a002a

.field public static final drm_warning_popup2:I = 0x7f0a002b

.field public static final drm_warning_popup3:I = 0x7f0a002c

.field public static final duration_short:I = 0x7f0a00f8

.field public static final during_call:I = 0x7f0a002d

.field public static final empty_video:I = 0x7f0a00f9

.field public static final error_network_error_occurred:I = 0x7f0a002e

.field public static final faster:I = 0x7f0a00fa

.field public static final file_not_found:I = 0x7f0a002f

.field public static final first_sync_notice:I = 0x7f0a00fb

.field public static final font:I = 0x7f0a0030

.field public static final font_size:I = 0x7f0a0031

.field public static final green_color:I = 0x7f0a0032

.field public static final half_speed_scrubbing:I = 0x7f0a0033

.field public static final help_adjust_volume:I = 0x7f0a0034

.field public static final help_select_video_all:I = 0x7f0a0035

.field public static final help_select_video_ok:I = 0x7f0a0036

.field public static final help_string_connected_device:I = 0x7f0a0037

.field public static final help_string_mydevice:I = 0x7f0a0038

.field public static final help_switch_screen:I = 0x7f0a0039

.field public static final help_toast_finish:I = 0x7f0a003a

.field public static final help_toast_wrong_input:I = 0x7f0a003b

.field public static final hi_speed_scrubbing:I = 0x7f0a003c

.field public static final hover_fast_forward:I = 0x7f0a00fc

.field public static final hover_mute:I = 0x7f0a00fd

.field public static final hover_rewind:I = 0x7f0a00fe

.field public static final hover_rotate:I = 0x7f0a003d

.field public static final hover_speed:I = 0x7f0a00ff

.field public static final hover_volume:I = 0x7f0a003e

.field public static final hover_volume_down:I = 0x7f0a0100

.field public static final hover_volume_up:I = 0x7f0a0101

.field public static final invalid_subtitle_file:I = 0x7f0a003f

.field public static final invalid_video:I = 0x7f0a0102

.field public static final language:I = 0x7f0a0040

.field public static final large:I = 0x7f0a0041

.field public static final license_present:I = 0x7f0a0103

.field public static final list:I = 0x7f0a0104

.field public static final lowbattery_info:I = 0x7f0a0042

.field public static final lowbattery_title:I = 0x7f0a0043

.field public static final mediascanner_finished:I = 0x7f0a0105

.field public static final mediascanner_running:I = 0x7f0a0106

.field public static final mediascanner_started:I = 0x7f0a0107

.field public static final medium:I = 0x7f0a0044

.field public static final menu_Delete:I = 0x7f0a0108

.field public static final menu_ShareVia:I = 0x7f0a0109

.field public static final menu_Shop:I = 0x7f0a010a

.field public static final menu_Subtitle:I = 0x7f0a0045

.field public static final menu_VideoMaker:I = 0x7f0a010b

.field public static final menu_ViewBookmark:I = 0x7f0a010c

.field public static final menu_audio:I = 0x7f0a010d

.field public static final menu_closedcaption:I = 0x7f0a0046

.field public static final menu_details:I = 0x7f0a010e

.field public static final menu_download:I = 0x7f0a010f

.field public static final menu_edit:I = 0x7f0a0110

.field public static final menu_trim:I = 0x7f0a0111

.field public static final menu_upload:I = 0x7f0a0112

.field public static final menu_view_participants:I = 0x7f0a0047

.field public static final micro_control_message:I = 0x7f0a0113

.field public static final monotype_default_font:I = 0x7f0a0048

.field public static final monotype_dialog_font_applemint:I = 0x7f0a0049

.field public static final monotype_dialog_font_choco:I = 0x7f0a004a

.field public static final monotype_dialog_font_cool:I = 0x7f0a004b

.field public static final monotype_dialog_font_girl:I = 0x7f0a004c

.field public static final monotype_dialog_font_kaiti:I = 0x7f0a004d

.field public static final monotype_dialog_font_maruberi:I = 0x7f0a004e

.field public static final monotype_dialog_font_miao:I = 0x7f0a004f

.field public static final monotype_dialog_font_mincho:I = 0x7f0a0050

.field public static final monotype_dialog_font_rose:I = 0x7f0a0051

.field public static final monotype_dialog_font_tinkerbell:I = 0x7f0a0052

.field public static final mosaic_search:I = 0x7f0a0114

.field public static final multivision:I = 0x7f0a0053

.field public static final multivision_num:I = 0x7f0a0054

.field public static final mv_add:I = 0x7f0a0055

.field public static final mv_firmware_not_support:I = 0x7f0a0056

.field public static final mv_playlist:I = 0x7f0a0057

.field public static final mv_remove:I = 0x7f0a0058

.field public static final mv_select_all:I = 0x7f0a0059

.field public static final mv_unselect_all:I = 0x7f0a005a

.field public static final mv_unsupport_play_next_video:I = 0x7f0a005b

.field public static final mv_unsupport_video:I = 0x7f0a005c

.field public static final my_device:I = 0x7f0a0115

.field public static final no_bookmark_found:I = 0x7f0a0116

.field public static final no_connectivity:I = 0x7f0a0117

.field public static final no_file_selected:I = 0x7f0a0118

.field public static final no_file_to_deleted:I = 0x7f0a0119

.field public static final no_license:I = 0x7f0a011a

.field public static final no_service:I = 0x7f0a011b

.field public static final no_videos:I = 0x7f0a005d

.field public static final none:I = 0x7f0a005e

.field public static final normal:I = 0x7f0a011c

.field public static final not_support:I = 0x7f0a005f

.field public static final ok:I = 0x7f0a0060

.field public static final one:I = 0x7f0a0061

.field public static final out_of_bookmark:I = 0x7f0a011d

.field public static final outdoor:I = 0x7f0a011e

.field public static final pemrission_tag:I = 0x7f0a011f

.field public static final percent:I = 0x7f0a0062

.field public static final playback:I = 0x7f0a0063

.field public static final playback_from:I = 0x7f0a0064

.field public static final playback_until:I = 0x7f0a0065

.field public static final player_error_aia_error_msg:I = 0x7f0a0066

.field public static final player_error_text_LicenseExpired:I = 0x7f0a0120

.field public static final player_error_text_LicenseNotFound:I = 0x7f0a0121

.field public static final player_error_text_acqFailed:I = 0x7f0a0122

.field public static final player_error_text_currupt:I = 0x7f0a0067

.field public static final player_error_text_invalid_progressive_playback:I = 0x7f0a0068

.field public static final player_error_text_notsupport:I = 0x7f0a0069

.field public static final player_error_text_unknown:I = 0x7f0a006a

.field public static final playing_video_hdmi:I = 0x7f0a0123

.field public static final playspeed:I = 0x7f0a0124

.field public static final popup_message_1_video_removed:I = 0x7f0a006b

.field public static final popup_message_maximin_participants_reached:I = 0x7f0a006c

.field public static final popup_message_num_videos_removed:I = 0x7f0a006d

.field public static final popup_message_share_video_multiview_info:I = 0x7f0a006e

.field public static final popup_message_share_video_query_exit:I = 0x7f0a006f

.field public static final popup_message_sharevideo_loading:I = 0x7f0a0070

.field public static final popup_message_sharing_seession_has_already_started:I = 0x7f0a0071

.field public static final popup_message_stop_being_sharing:I = 0x7f0a0072

.field public static final popup_message_this_video_is_in_an_unsupported_format:I = 0x7f0a0073

.field public static final popup_message_unable_ff_rew:I = 0x7f0a015a

.field public static final popup_message_unable_to_join_share_session:I = 0x7f0a0074

.field public static final popup_message_unable_to_play_the_current_video:I = 0x7f0a0075

.field public static final popup_message_volumedialog_list_master_volume:I = 0x7f0a0076

.field public static final processing_popup:I = 0x7f0a0125

.field public static final quarter_speed_scrubbing:I = 0x7f0a0077

.field public static final refresh:I = 0x7f0a0126

.field public static final reset:I = 0x7f0a0078

.field public static final rights_tag:I = 0x7f0a0127

.field public static final row_item_count:I = 0x7f0a0079

.field public static final save:I = 0x7f0a007a

.field public static final sd_unmounted:I = 0x7f0a007b

.field public static final search_empty:I = 0x7f0a0128

.field public static final search_hint:I = 0x7f0a0129

.field public static final search_label:I = 0x7f0a012a

.field public static final sec:I = 0x7f0a007c

.field public static final sec_faster:I = 0x7f0a007d

.field public static final sec_slower:I = 0x7f0a007e

.field public static final seekspeed:I = 0x7f0a007f

.field public static final select_all:I = 0x7f0a012b

.field public static final select_at_least_one:I = 0x7f0a0080

.field public static final select_subtitle:I = 0x7f0a0081

.field public static final selected:I = 0x7f0a0082

.field public static final settings:I = 0x7f0a0083

.field public static final setvideoautooff:I = 0x7f0a012c

.field public static final share_video:I = 0x7f0a0084

.field public static final sharevideo_explain:I = 0x7f0a0085

.field public static final singlevision:I = 0x7f0a0086

.field public static final slower:I = 0x7f0a012d

.field public static final small:I = 0x7f0a0087

.field public static final sortyby:I = 0x7f0a0088

.field public static final sortybydate:I = 0x7f0a012e

.field public static final sortybyname:I = 0x7f0a012f

.field public static final sortybysize:I = 0x7f0a0130

.field public static final sortybytype:I = 0x7f0a0131

.field public static final sound_alive:I = 0x7f0a0132

.field public static final speaker_sound_channel_left:I = 0x7f0a0089

.field public static final speaker_sound_channel_right:I = 0x7f0a008a

.field public static final stms_appgroup:I = 0x7f0a008b

.field public static final stop_playing_appinapp:I = 0x7f0a008c

.field public static final subtitle_Empty:I = 0x7f0a008d

.field public static final subtitle_backgroundcolor:I = 0x7f0a008e

.field public static final subtitle_fontcolor:I = 0x7f0a008f

.field public static final subtitle_on_off:I = 0x7f0a0090

.field public static final surround:I = 0x7f0a0133

.field public static final sync:I = 0x7f0a0091

.field public static final sync_with_dropbox_cloud:I = 0x7f0a0134

.field public static final tab_Nearby_devices:I = 0x7f0a0135

.field public static final tab_allshare:I = 0x7f0a0136

.field public static final tab_folders:I = 0x7f0a0137

.field public static final tab_lists:I = 0x7f0a0138

.field public static final tab_thumbnail:I = 0x7f0a0139

.field public static final times:I = 0x7f0a0092

.field public static final title_unknown:I = 0x7f0a0093

.field public static final tts_button:I = 0x7f0a0094

.field public static final tts_ff_button:I = 0x7f0a0095

.field public static final tts_fit_to_height:I = 0x7f0a0096

.field public static final tts_keep_aspect_ratio_button:I = 0x7f0a0097

.field public static final tts_member:I = 0x7f0a0098

.field public static final tts_members:I = 0x7f0a0099

.field public static final tts_minus_button:I = 0x7f0a009a

.field public static final tts_navigate_up:I = 0x7f0a009b

.field public static final tts_original_size:I = 0x7f0a009c

.field public static final tts_overlook_aspect_ratio_button:I = 0x7f0a009d

.field public static final tts_pause_button:I = 0x7f0a009e

.field public static final tts_play_button:I = 0x7f0a009f

.field public static final tts_plus_button:I = 0x7f0a00a0

.field public static final tts_rew_button:I = 0x7f0a00a1

.field public static final tts_screen_mode:I = 0x7f0a00a2

.field public static final tts_split_view:I = 0x7f0a00a3

.field public static final tts_stop_button:I = 0x7f0a00a4

.field public static final tts_synced_view:I = 0x7f0a00a5

.field public static final tts_videoprogress_percent:I = 0x7f0a00a6

.field public static final tts_videoprogress_seek_control:I = 0x7f0a00a7

.field public static final tts_videoprogress_swipe:I = 0x7f0a00a8

.field public static final tts_volume_button:I = 0x7f0a00a9

.field public static final unauthorized_video_output_so_Analog:I = 0x7f0a013a

.field public static final unauthorized_video_output_so_HDMI:I = 0x7f0a013b

.field public static final upload_done:I = 0x7f0a013c

.field public static final via_Bluetooth:I = 0x7f0a013d

.field public static final via_Device:I = 0x7f0a013e

.field public static final video_51_chanel_sound_effect_not_works_in_line_out:I = 0x7f0a013f

.field public static final video_51_chanel_sound_effect_works_in_earphone_mode_only:I = 0x7f0a0140

.field public static final video_allshare_busy:I = 0x7f0a0141

.field public static final video_allshare_change_player:I = 0x7f0a0142

.field public static final video_allshare_derr_need_to_reset:I = 0x7f0a0143

.field public static final video_allshare_derr_no_response:I = 0x7f0a0144

.field public static final video_allshare_devices_found:I = 0x7f0a00aa

.field public static final video_allshare_download_fail:I = 0x7f0a0145

.field public static final video_allshare_local_dms_full:I = 0x7f0a0146

.field public static final video_allshare_no_contents_location:I = 0x7f0a0147

.field public static final video_allshare_no_devices_found:I = 0x7f0a00ab

.field public static final video_allshare_no_player:I = 0x7f0a0148

.field public static final video_allshare_play_error:I = 0x7f0a0149

.field public static final video_allshare_scan_for_nearby_devices:I = 0x7f0a014a

.field public static final video_allshare_scan_started:I = 0x7f0a00ac

.field public static final video_detail_drm_constraint_type_accumulated_time:I = 0x7f0a00ad

.field public static final video_detail_drm_constraint_type_count:I = 0x7f0a00ae

.field public static final video_detail_drm_constraint_type_date:I = 0x7f0a00af

.field public static final video_detail_drm_constraint_type_interval:I = 0x7f0a00b0

.field public static final video_detail_drm_constraint_type_time:I = 0x7f0a00b1

.field public static final video_detail_drm_constraint_type_timed_count:I = 0x7f0a00b2

.field public static final video_detail_drm_constraint_type_unlimited:I = 0x7f0a00b3

.field public static final video_detail_drm_permission_type_display:I = 0x7f0a00b4

.field public static final video_detail_drm_permission_type_excute:I = 0x7f0a00b5

.field public static final video_detail_drm_permission_type_play:I = 0x7f0a00b6

.field public static final video_detail_drm_permission_type_print:I = 0x7f0a00b7

.field public static final video_drm_acquiring_license:I = 0x7f0a00b8

.field public static final video_drm_detail_available_uses:I = 0x7f0a00b9

.field public static final video_drm_detail_drm_validity:I = 0x7f0a00ba

.field public static final video_drm_divx_back_key:I = 0x7f0a00bb

.field public static final video_drm_divx_continue:I = 0x7f0a00bc

.field public static final video_drm_divx_not_authorized:I = 0x7f0a00bd

.field public static final video_drm_divx_register_at:I = 0x7f0a00be

.field public static final video_drm_divx_register_code:I = 0x7f0a00bf

.field public static final video_drm_divx_rental_expired1:I = 0x7f0a00c0

.field public static final video_drm_divx_rental_expired2:I = 0x7f0a00c1

.field public static final video_drm_first_render:I = 0x7f0a00c2

.field public static final video_drm_item_expired:I = 0x7f0a00c3

.field public static final video_drm_locked:I = 0x7f0a00c4

.field public static final video_drm_play_now_q:I = 0x7f0a00c5

.field public static final video_drm_this_item_no_longer_use:I = 0x7f0a00c6

.field public static final video_drm_unlock_it_q:I = 0x7f0a00c7

.field public static final video_drm_use_1_time:I = 0x7f0a00c8

.field public static final video_drm_use_n_times:I = 0x7f0a00c9

.field public static final video_not_bookmark:I = 0x7f0a014b

.field public static final video_play_only:I = 0x7f0a00ca

.field public static final video_selected:I = 0x7f0a014c

.field public static final video_sound_alive_sound_effect_works_in_earphone_mode_only:I = 0x7f0a014d

.field public static final video_subtitle_lang_ch:I = 0x7f0a00cb

.field public static final video_subtitle_lang_de:I = 0x7f0a00cc

.field public static final video_subtitle_lang_eng:I = 0x7f0a00cd

.field public static final video_subtitle_lang_es:I = 0x7f0a00ce

.field public static final video_subtitle_lang_fr:I = 0x7f0a00cf

.field public static final video_subtitle_lang_it:I = 0x7f0a00d0

.field public static final video_subtitle_lang_jp:I = 0x7f0a00d1

.field public static final video_subtitle_lang_kr:I = 0x7f0a00d2

.field public static final video_text:I = 0x7f0a014e

.field public static final videoautooff:I = 0x7f0a014f

.field public static final videoautooff_15m:I = 0x7f0a0150

.field public static final videoautooff_1h:I = 0x7f0a0151

.field public static final videoautooff_1h30m:I = 0x7f0a0152

.field public static final videoautooff_2h:I = 0x7f0a0153

.field public static final videoautooff_30m:I = 0x7f0a0154

.field public static final videoautooff_off:I = 0x7f0a0155

.field public static final videos_selected:I = 0x7f0a0156

.field public static final view_by_cloud:I = 0x7f0a0157

.field public static final voice:I = 0x7f0a0158

.field public static final warm:I = 0x7f0a0159

.field public static final white_color:I = 0x7f0a00d3

.field public static final widget_name:I = 0x7f0a00d4

.field public static final zoom_not_support:I = 0x7f0a00d5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
