.class public Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
.super Landroid/app/Service;
.source "ChordService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;,
        Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    }
.end annotation


# static fields
.field private static final CHORD_APITEST_MESSAGE_TYPE:Ljava/lang/String; = "CHORD_APITEST_MESSAGE_TYPE"

.field public static CHORD_OPEN_CHANNEL:Ljava/lang/String; = null

.field private static final MESSAGE_TYPE_FILE_NOTIFICATION:Ljava/lang/String; = "FILE_NOTIFICATION_V2"

.field private static final PUBLIC_CHANNEL:Ljava/lang/String; = "Chord"

.field private static final SHARE_FILE_TIMEOUT_SECONDS:I = 0x36ee80

.field private static final TAG:Ljava/lang/String; = "MVChordService"

.field private static final TAGClass:Ljava/lang/String; = "MVChordServiceClass : "

.field public static final chordFilePath:Ljava/lang/String;


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

.field private mChord:Lcom/samsung/android/sdk/chord/SchordManager;

.field private mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

.field private mOwnIP:Ljava/lang/String;

.field private mPrivateChannelName:Ljava/lang/String;

.field private mRecivedFileName:Ljava/lang/String;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mbStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-string v0, "com.samsung.mv.player.Channel"

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Chord_ShareVideo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mRecivedFileName:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mOwnIP:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mbStarted:Z

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 175
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$ChordServiceBinder;-><init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mBinder:Landroid/os/IBinder;

    .line 326
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;-><init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mbStarted:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mRecivedFileName:Ljava/lang/String;

    return-object p1
.end method

.method private acqureWakeLock()V
    .locals 3

    .prologue
    .line 1359
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 1360
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1361
    .local v0, "powerMgr":Landroid/os/PowerManager;
    const/16 v1, 0x1a

    const-string v2, "ChordApiDemo Lock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1363
    const-string v1, "MVChordService"

    const-string v2, "acqureWakeLock : new"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1366
    .end local v0    # "powerMgr":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1367
    const-string v1, "MVChordService"

    const-string v2, "acqureWakeLock : already acquire"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1371
    :cond_1
    const-string v1, "MVChordService"

    const-string v2, "acqureWakeLock : acquire"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1373
    return-void
.end method

.method private releaseWakeLock()V
    .locals 2

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1377
    const-string v0, "MVChordService"

    const-string v1, "releaseWakeLock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1380
    :cond_0
    return-void
.end method


# virtual methods
.method public acceptFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "fromChannel"    # Ljava/lang/String;
    .param p2, "exchangeId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 960
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : acceptFile()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 963
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    .line 964
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : acceptFile() : invalid channel instance"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    .line 980
    :goto_0
    return v1

    .line 976
    :cond_0
    const/16 v2, 0x7530

    const/4 v3, 0x2

    const-wide/32 v4, 0x4b000

    move-object v1, p2

    :try_start_0
    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->acceptFile(Ljava/lang/String;IIJ)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 977
    const/4 v1, 0x1

    goto :goto_0

    .line 978
    :catch_0
    move-exception v6

    .line 979
    .local v6, "e":Ljava/lang/RuntimeException;
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : acceptFile : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    .line 980
    goto :goto_0
.end method

.method public acceptFiles(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "fromChannel"    # Ljava/lang/String;
    .param p2, "taskId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 1040
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : acceptFiles()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v1, :cond_0

    .line 1043
    const-string v1, "MVChordService"

    const-string v2, "acceptFiles. mChord is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    .line 1068
    :goto_0
    return v1

    .line 1048
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1050
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 1051
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : acceptFiles() : invalid channel instance"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    .line 1052
    goto :goto_0

    .line 1064
    :cond_1
    const/16 v2, 0x7530

    const/4 v3, 0x2

    const-wide/32 v4, 0x4b000

    move-object v1, p2

    :try_start_0
    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->acceptMultiFiles(Ljava/lang/String;IIJ)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065
    const/4 v1, 0x1

    goto :goto_0

    .line 1066
    :catch_0
    move-exception v6

    .line 1067
    .local v6, "e":Ljava/lang/RuntimeException;
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : acceptFiles : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v7

    .line 1068
    goto :goto_0
.end method

.method public cancelFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "exchangeId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 986
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : cancelFile()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v3, :cond_0

    .line 989
    const-string v3, "MVChordService"

    const-string v4, "cancelFile. mChord is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    :goto_0
    return v2

    .line 994
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 996
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 997
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : cancelFile() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1003
    :cond_1
    :try_start_0
    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/chord/SchordChannel;->cancelFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1004
    const/4 v2, 0x1

    goto :goto_0

    .line 1005
    :catch_0
    move-exception v1

    .line 1006
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : acceptFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public cancelFiles(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "taskId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1074
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : cancelFiles()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1077
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    .line 1078
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : cancelFiles() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    :goto_0
    return v2

    .line 1084
    :cond_0
    :try_start_0
    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/chord/SchordChannel;->cancelMultiFiles(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1085
    const/4 v2, 0x1

    goto :goto_0

    .line 1086
    :catch_0
    move-exception v1

    .line 1087
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : cancelFiles : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableInterfaceTypes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1259
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : getAvailableInterfaceTypes()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getAvailableInterfaceTypes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getChannel()Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 4

    .prologue
    .line 1388
    const/4 v0, 0x0

    .line 1389
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chord/SchordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1391
    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 881
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getJoinedChannelHandle()Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 2

    .prologue
    .line 877
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    return-object v0
.end method

.method public getJoinedChannelList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1270
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : getJoinedChannelList()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannelList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getJoinedNodeList(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "channelName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1114
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : getJoinedNodeList()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1117
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    .line 1118
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : getJoinedNodeList() : invalid channel instance-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    const/4 v1, 0x0

    .line 1122
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getJoinedNodeList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getMVChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    return-object v0
.end method

.method public getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 1243
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : getNodeIpAddress() channelName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", nodeName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1247
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    const-string v1, ""

    .line 1254
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/chord/SchordChannel;->getNodeIpAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getOwnIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicChannel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 865
    const-string v0, "Chord"

    return-object v0
.end method

.method public getRecievedFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mRecivedFileName:Ljava/lang/String;

    return-object v0
.end method

.method public initialize(Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    const-string v3, "MVChordService"

    const-string v4, "initialize E"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v3, :cond_0

    .line 182
    const-string v3, "MVChordService"

    const-string v4, "initialize. mChord is not null"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/chord/Schord;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chord/Schord;-><init>()V

    .line 190
    .local v0, "chord":Lcom/samsung/android/sdk/chord/Schord;
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/chord/Schord;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_1
    :goto_1
    new-instance v3, Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/chord/SchordManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 198
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : [Initialize] Chord Initialized. chordFilePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 202
    .local v1, "chordPath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2

    .line 203
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 206
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    sget-object v4, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chord/SchordManager;->setTempDirectory(Ljava/lang/String;)V

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    new-instance v4, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$1;-><init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chord/SchordManager;->setNetworkListener(Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V

    goto :goto_0

    .line 191
    .end local v1    # "chordPath":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 192
    .local v2, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->getType()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1
.end method

.method public joinChannel(Ljava/lang/String;Z)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 4
    .param p1, "channelName"    # Ljava/lang/String;
    .param p2, "bUseSecurity"    # Z

    .prologue
    .line 1278
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : joinChannel()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1281
    :cond_0
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : joinChannel > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is invalid! Default private channel join"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    .line 1294
    :cond_1
    if-eqz p2, :cond_2

    .line 1295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    .line 1299
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChannelListener:Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chord/SchordManager;->joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1306
    :goto_0
    return-object v1

    .line 1300
    :catch_0
    move-exception v0

    .line 1301
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1306
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1302
    :catch_1
    move-exception v0

    .line 1303
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1
.end method

.method public leaveChannel()V
    .locals 2

    .prologue
    .line 1311
    const-string v0, "MVChordService"

    const-string v1, "leaveChannel E."

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1316
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    .line 1319
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->leaveChannel(Ljava/lang/String;)V

    .line 1320
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mPrivateChannelName:Ljava/lang/String;

    .line 1321
    return-void
.end method

.method public multisendFile(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p3, "toNode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 930
    .local p2, "strFilePath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : multisendFile() "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 932
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    .line 933
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : sendFile() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    const/4 v2, 0x0

    .line 954
    :goto_0
    return-object v2

    .line 937
    :cond_0
    const/4 v2, 0x0

    .line 940
    .local v2, "trId":Ljava/lang/String;
    :try_start_0
    const-string v3, "FILE_NOTIFICATION_V2"

    const v4, 0x36ee80

    invoke-interface {v0, p3, v3, p2, v4}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendMultiFiles(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 941
    :catch_0
    move-exception v1

    .line 942
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : multisendFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 130
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : onBind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 136
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 138
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 142
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 149
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 153
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : onRebind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 155
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 159
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v0, 0x2

    invoke-super {p0, p1, v0, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 165
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : onUnbind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public rejectFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "fromChannel"    # Ljava/lang/String;
    .param p2, "exchangeId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1013
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : rejectFile(). exchangeId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v3, :cond_0

    .line 1016
    const-string v3, "MVChordService"

    const-string v4, "rejectFile. mChord is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    :goto_0
    return v2

    .line 1021
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1023
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 1024
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : rejectFile() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1030
    :cond_1
    :try_start_0
    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/chord/SchordChannel;->rejectFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1031
    const/4 v2, 0x1

    goto :goto_0

    .line 1032
    :catch_0
    move-exception v1

    .line 1033
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : acceptFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public rejectFiles(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "fromChannel"    # Ljava/lang/String;
    .param p2, "taskId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1094
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : rejectFiles()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1097
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_0

    .line 1098
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : rejectFiles() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    :goto_0
    return v2

    .line 1104
    :cond_0
    :try_start_0
    invoke-interface {v0, p2}, Lcom/samsung/android/sdk/chord/SchordChannel;->rejectMultiFiles(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1105
    const/4 v2, 0x1

    goto :goto_0

    .line 1106
    :catch_0
    move-exception v1

    .line 1107
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : rejectFiles : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public release()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 854
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 857
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    .line 858
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    .line 859
    const-string v0, "MVChordService"

    const-string v1, "[UNREGISTER] Chord unregistered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    :cond_0
    return-void
.end method

.method public sendData(Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 8
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p2, "buf"    # [B
    .param p3, "nodeName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1127
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v5, :cond_0

    .line 1128
    const-string v4, "MVChordService"

    const-string v5, "sendData : mChord IS NULL  !!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    :goto_0
    return v3

    .line 1133
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v5, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1135
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 1136
    const-string v4, "MVChordService"

    const-string v5, "MVChordServiceClass : sendData : invalid channel instance"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1140
    :cond_1
    if-nez p3, :cond_2

    .line 1141
    const-string v4, "MVChordService"

    const-string v5, "sendData : NODE Name IS NULL !!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1145
    :cond_2
    new-array v2, v4, [[B

    .line 1146
    .local v2, "payload":[[B
    aput-object p2, v2, v3

    .line 1148
    const-string v5, "MVChordService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MVChordServiceClass : sendData : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", des : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    :try_start_0
    const-string v5, "CHORD_APITEST_MESSAGE_TYPE"

    invoke-interface {v0, p3, v5, v2}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendData(Ljava/lang/String;Ljava/lang/String;[[B)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 1160
    goto :goto_0

    .line 1161
    :catch_0
    move-exception v1

    .line 1162
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v4, "MVChordService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MVChordServiceClass : sendData : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendDataToAll(Ljava/lang/String;[B)Z
    .locals 8
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p2, "buf"    # [B

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1169
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v5, :cond_0

    .line 1170
    const-string v4, "MVChordService"

    const-string v5, "sendDataToAll : mChord IS NULL  !!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    :goto_0
    return v3

    .line 1175
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v5, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 1177
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 1178
    const-string v4, "MVChordService"

    const-string v5, "MVChordServiceClass : sendDataToAll : invalid channel instance"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1182
    :cond_1
    new-array v2, v4, [[B

    .line 1183
    .local v2, "payload":[[B
    aput-object p2, v2, v3

    .line 1185
    const-string v5, "MVChordService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MVChordServiceClass : sendDataToAll : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    :try_start_0
    const-string v5, "CHORD_APITEST_MESSAGE_TYPE"

    invoke-interface {v0, v5, v2}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendDataToAll(Ljava/lang/String;[[B)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 1202
    goto :goto_0

    .line 1197
    :catch_0
    move-exception v1

    .line 1198
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v4, "MVChordService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MVChordServiceClass : sendDataToAll : IllegalStateException -"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p2, "strFilePath"    # Ljava/lang/String;
    .param p3, "toNode"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 890
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : sendFile() "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-nez v3, :cond_0

    .line 893
    const-string v3, "MVChordService"

    const-string v4, "sendFile. mchord is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    :goto_0
    return-object v2

    .line 898
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 900
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-nez v0, :cond_1

    .line 901
    const-string v3, "MVChordService"

    const-string v4, "MVChordServiceClass : sendFile() : invalid channel instance"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 915
    :cond_1
    const/4 v2, 0x0

    .line 918
    .local v2, "exchangeId":Ljava/lang/String;
    :try_start_0
    const-string v3, "FILE_NOTIFICATION_V2"

    const v4, 0x36ee80

    invoke-interface {v0, p3, v3, p2, v4}, Lcom/samsung/android/sdk/chord/SchordChannel;->sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 920
    :catch_0
    move-exception v1

    .line 921
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : sendFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 922
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 923
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "MVChordService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MVChordServiceClass : sendFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public start(IZ)V
    .locals 3
    .param p1, "interfaceType"    # I
    .param p2, "bSecureMode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 239
    const-string v1, "MVChordService"

    const-string v2, "start E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->setSecureModeEnabled(Z)V

    .line 251
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$2;-><init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V

    .line 306
    .local v0, "listener":Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->start(ILcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V

    .line 307
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 1325
    const-string v0, "MVChordService"

    const-string v1, "MVChordServiceClass : stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mbStarted:Z

    .line 1327
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    if-eqz v0, :cond_0

    .line 1328
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 1330
    :cond_0
    return-void
.end method
