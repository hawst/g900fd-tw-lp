.class public Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;
.super Ljava/lang/Object;
.source "MVTouchMotion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;,
        Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    }
.end annotation


# static fields
.field private static final MSG_CANCEL_TOUCHINFO:I = 0x2

.field private static final MSG_REQUEST_MV_JOIN:I = 0x1

.field private static final MSG_TOUCH_EXPIRED_TIME:I = 0x3

.field private static final MV_JOINABLE_MAX_THRESHOLD:I = 0x1f4

.field private static final MV_JOINABLE_MIN_THRESHOLD:I = 0x32

.field private static final MV_JOIN_EXPIRED_TIME:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "MVTouchMotion"


# instance fields
.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mHandler:Landroid/os/Handler;

.field private mMVFirstTouchInputX:I

.field private mMVFirstTouchInputY:I

.field private mMVTouchRecognizer:Z

.field private mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

.field private mTouchInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;II)V
    .locals 2
    .param p1, "util"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p2, "displayWidth"    # I
    .param p3, "displayHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;

    .line 22
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 23
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayWidth:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayHeight:I

    .line 84
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;-><init>(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mHandler:Landroid/os/Handler;

    .line 177
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 178
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayWidth:I

    .line 179
    iput p3, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayHeight:I

    .line 180
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getTouchTime()J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 220
    const-wide/16 v2, 0x0

    .line 221
    .local v2, "systemTimeBase":J
    const-wide/16 v0, 0x0

    .line 223
    .local v0, "clientSystemClockDelta":J
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-nez v6, :cond_0

    .line 241
    :goto_0
    return-wide v4

    .line 226
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getSystemTimeBase()J

    move-result-wide v2

    .line 227
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 228
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getSystemClockDelta()J

    move-result-wide v0

    .line 231
    :cond_1
    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v6

    if-eqz v6, :cond_3

    cmp-long v6, v0, v4

    if-nez v6, :cond_3

    .line 232
    :cond_2
    const-string v6, "MVTouchMotion"

    const-string v7, "getTouchEventTime, systemTimeBase or clientSystemClockDelta were not initialize"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 235
    :cond_3
    const-wide/16 v4, 0x0

    .line 236
    .local v4, "touchTime":J
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 237
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v2

    sub-long v4, v6, v0

    goto :goto_0

    .line 239
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v2

    goto :goto_0
.end method

.method public isMVJoinableTouchInfo(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;)Z
    .locals 6
    .param p1, "reqTouchInfo"    # Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    .param p2, "prevTouchInfo"    # Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .prologue
    const/4 v0, 0x0

    .line 184
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 189
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 194
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x32

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 195
    const-string v1, "MVTouchMotion"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isMVJoinableTouchInfo reqTouchInfo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") - prevTouchInfo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") < MV_JOINABLE_MIN_THRESHOLD("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 200
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 201
    const-string v1, "MVTouchMotion"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isMVJoinableTouchInfo reqTouchInfo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") - prevTouchInfo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") > MV_JOINABLE_MAX_THRESHOLD("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 205
    :cond_3
    const-string v0, "MVTouchMotion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isMVJoinableTouchInfo found MVJoinable TouchInfo (reqId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prevId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), (reqDirection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prevDirection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), (reqTouchTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prevTouchTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 246
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 248
    .local v0, "action":I
    sparse-switch v0, :sswitch_data_0

    .line 373
    const/4 v10, 0x1

    :goto_0
    return v10

    .line 251
    :sswitch_0
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 252
    const/4 v10, 0x1

    goto :goto_0

    .line 255
    :sswitch_1
    iget-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    if-eqz v10, :cond_0

    .line 256
    const/4 v5, 0x0

    .line 257
    .local v5, "limit":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v6, v10

    .line 258
    .local v6, "moveX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v7, v10

    .line 261
    .local v7, "moveY":I
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getScreenOrientation()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 262
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    if-le v10, v6, :cond_1

    .line 263
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    sub-int v5, v10, v6

    .line 268
    :goto_1
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayWidth:I

    div-int/lit8 v10, v10, 0x5

    if-le v5, v10, :cond_0

    .line 269
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 270
    const-string v10, "MVTouchMotion"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_MOVE (LANDSCAPE_VIEW) touch over limitation, limit : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    .end local v5    # "limit":I
    .end local v6    # "moveX":I
    .end local v7    # "moveY":I
    :cond_0
    :goto_2
    const/4 v10, 0x1

    goto :goto_0

    .line 265
    .restart local v5    # "limit":I
    .restart local v6    # "moveX":I
    .restart local v7    # "moveY":I
    :cond_1
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    sub-int v5, v6, v10

    goto :goto_1

    .line 273
    :cond_2
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    if-le v10, v7, :cond_3

    .line 274
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    sub-int v5, v10, v7

    .line 279
    :goto_3
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayHeight:I

    div-int/lit8 v10, v10, 0x5

    if-le v5, v10, :cond_0

    .line 280
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 281
    const-string v10, "MVTouchMotion"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_MOVE (PORTRAIT_VIEW) touch over limitation, limit : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 276
    :cond_3
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    sub-int v5, v7, v10

    goto :goto_3

    .line 288
    .end local v5    # "limit":I
    .end local v6    # "moveX":I
    .end local v7    # "moveY":I
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v10, v10

    iput v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    .line 289
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v10, v10

    iput v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    .line 291
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getScreenOrientation()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_4

    .line 296
    :cond_4
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 298
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 302
    :sswitch_3
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 303
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 306
    :sswitch_4
    iget-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    if-eqz v10, :cond_6

    .line 307
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v3, v10

    .line 308
    .local v3, "lastX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v4, v10

    .line 309
    .local v4, "lastY":I
    const/4 v5, 0x0

    .line 310
    .restart local v5    # "limit":I
    const/4 v1, -0x1

    .line 311
    .local v1, "direction":I
    const/4 v2, 0x0

    .line 313
    .local v2, "eventHappend":Z
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getScreenOrientation()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_a

    .line 314
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    if-le v4, v10, :cond_7

    .line 315
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    sub-int v5, v4, v10

    .line 320
    :goto_4
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayHeight:I

    div-int/lit8 v10, v10, 0x5

    if-le v5, v10, :cond_9

    .line 321
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    if-le v4, v10, :cond_8

    .line 323
    const/4 v1, 0x0

    .line 324
    const/4 v2, 0x1

    .line 355
    :goto_5
    if-eqz v2, :cond_5

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->getTouchTime()J

    move-result-wide v8

    .line 357
    .local v8, "touchTime":J
    const-string v10, "MVTouchMotion"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_UP touchGestureTime : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", touch limit value : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-wide/16 v10, -0x1

    cmp-long v10, v8, v10

    if-eqz v10, :cond_5

    .line 360
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v10

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v10, :cond_e

    .line 361
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10, v1, v8, v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendTouchInfo(IJ)V

    .line 368
    .end local v8    # "touchTime":J
    :cond_5
    :goto_6
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVTouchRecognizer:Z

    .line 370
    .end local v1    # "direction":I
    .end local v2    # "eventHappend":Z
    .end local v3    # "lastX":I
    .end local v4    # "lastY":I
    .end local v5    # "limit":I
    :cond_6
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 317
    .restart local v1    # "direction":I
    .restart local v2    # "eventHappend":Z
    .restart local v3    # "lastX":I
    .restart local v4    # "lastY":I
    .restart local v5    # "limit":I
    :cond_7
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputY:I

    sub-int v5, v10, v4

    goto :goto_4

    .line 327
    :cond_8
    const/4 v1, 0x1

    .line 328
    const/4 v2, 0x1

    goto :goto_5

    .line 331
    :cond_9
    const-string v10, "MVTouchMotion"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_UP (LANDSCAPE_VIEW) limitation value : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " < mDisplayHeight / 5 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayHeight:I

    div-int/lit8 v12, v12, 0x5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 334
    :cond_a
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    if-le v3, v10, :cond_b

    .line 335
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    sub-int v5, v3, v10

    .line 340
    :goto_7
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayWidth:I

    div-int/lit8 v10, v10, 0x5

    if-le v5, v10, :cond_d

    .line 341
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    if-le v3, v10, :cond_c

    .line 343
    const/4 v1, 0x1

    .line 344
    const/4 v2, 0x1

    goto/16 :goto_5

    .line 337
    :cond_b
    iget v10, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVFirstTouchInputX:I

    sub-int v5, v10, v3

    goto :goto_7

    .line 347
    :cond_c
    const/4 v1, 0x0

    .line 348
    const/4 v2, 0x1

    goto/16 :goto_5

    .line 351
    :cond_d
    const-string v10, "MVTouchMotion"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ACTION_UP (PORTRAIT_VIEW) limitation value : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " < mDisplayWidth / 5 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mDisplayWidth:I

    div-int/lit8 v12, v12, 0x5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 363
    .restart local v8    # "touchTime":J
    :cond_e
    const/4 v10, 0x0

    invoke-virtual {p0, v10, v1, v8, v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->sendTouchInfo(IIJ)V

    goto :goto_6

    .line 248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_4
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x105 -> :sswitch_0
    .end sparse-switch
.end method

.method public sendTouchInfo(IIJ)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "direction"    # I
    .param p3, "touchtime"    # J

    .prologue
    .line 213
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;-><init>(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;IIJ)V

    .line 215
    .local v0, "touchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 216
    .local v6, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 217
    return-void
.end method
