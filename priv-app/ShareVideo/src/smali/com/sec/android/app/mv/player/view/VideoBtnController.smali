.class public Lcom/sec/android/app/mv/player/view/VideoBtnController;
.super Landroid/widget/RelativeLayout;
.source "VideoBtnController.java"


# static fields
.field private static final CENTER_M_PROGRESS_VIEW:I = 0x0

.field public static final FFSHORTSEEK:I = 0x6

.field private static final HOVER_DETECT_TIME_MS:I = 0x12c

.field private static final LEFT_M_PROGRESS_VIEW:I = 0x1

.field public static final LONG_PRESS_TIME:J = 0x1f4L

.field public static final MEDIUM_PRESS_TIME:J = 0x15eL

.field private static final OPEN_MV_PLAYLIST:I = 0x2

.field private static final OPEN_PLAYERLIST:I = 0x3

.field public static final PLAYSHORT:I = 0x5

.field private static final PROGRESS_POPUP_FLOATING_POINT:I = 0x19

.field private static final PROGRESS_RESOLUTION:J = 0x186a0L

.field public static final REWSHORTSEEK:I = 0x7

.field private static final RIGHT_M_PROGRESS_VIEW:I = 0x2

.field public static final SHORT_PRESS_TIME:J = 0x64L

.field private static final SHOW_PROGRESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoPlayerBtnController"

.field private static final UNINITIALIZED_MEDIAPLAYERSERVICE:I = -0x1


# instance fields
.field private HOVER_IMAGE_GAP:I

.field private PROGRESS_MARGIN:I

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

.field private mBottomPadding:I

.field private mButtonEnable:Z

.field private mContext:Landroid/content/Context;

.field private mCtrlLayoutShow:Z

.field private mCtrlLayoutState:Z

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDownKeyPressTime:J

.field private mDuration:J

.field private mEndTime:Landroid/widget/TextView;

.field private mFfButton:Landroid/widget/ImageButton;

.field private mFfKeyListener:Landroid/view/View$OnKeyListener;

.field private mFfTouchListener:Landroid/view/View$OnTouchListener;

.field private mFitToSrcBtn:Landroid/widget/ImageButton;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerProgressPreview:Landroid/os/Handler;

.field private mHoverDuration:J

.field private mHoverEventStart:Z

.field private mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

.field private mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

.field private mIsSearch:Z

.field private mLeftPadding:I

.field private mMVPlaylistButton:Landroid/widget/ImageButton;

.field private mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mPauseKeyListener:Landroid/view/View$OnKeyListener;

.field private mPauseTouchListener:Landroid/view/View$OnTouchListener;

.field private mPlayPauseBtnHoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field private mPlayerListButton:Landroid/widget/ImageButton;

.field private mProgressBar:Landroid/widget/SeekBar;

.field private mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressContent:Landroid/view/View;

.field private mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

.field private mProgressPreviewImage:Landroid/widget/ImageView;

.field private mProgressPreviewShowTime:Landroid/widget/TextView;

.field private mProgressThumbViewState:I

.field private mRewButton:Landroid/widget/ImageButton;

.field private mRewKeyListener:Landroid/view/View$OnKeyListener;

.field private mRewTouchListener:Landroid/view/View$OnTouchListener;

.field private mRightPadding:I

.field protected mRoot:Landroid/view/View;

.field private mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

.field private mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mTimetextHeight:I

.field private mTimetextWidth:I

.field private mTopPadding:I

.field private mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

.field private mVideoBtnSeekBarPosionX:F

.field private mVideoBtnSeekBarPosionY:F

.field private mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

.field private mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

.field private mViewModeRes:[I

.field private mbProgressDragStatus:Z

.field private mbResume:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 7
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const v6, 0x7f080022

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 194
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 114
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mIsSearch:Z

    .line 118
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 120
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mButtonEnable:Z

    .line 128
    iput v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    .line 130
    iput v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    .line 132
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J

    .line 140
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    .line 142
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverDuration:J

    .line 144
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;

    .line 146
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    .line 148
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    .line 152
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->PROGRESS_MARGIN:I

    .line 154
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->HOVER_IMAGE_GAP:I

    .line 160
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    .line 162
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I

    .line 164
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    .line 166
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I

    .line 168
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTopPadding:I

    .line 170
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBottomPadding:I

    .line 172
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextHeight:I

    .line 174
    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextWidth:I

    .line 178
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 180
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 182
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z

    .line 184
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    .line 186
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewModeRes:[I

    .line 642
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$6;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    .line 679
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$7;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    .line 718
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$8;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    .line 1013
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    .line 1164
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    .line 1190
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$13;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    .line 1235
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$14;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    .line 1270
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$15;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    .line 1324
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$16;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    .line 1355
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    .line 1483
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    .line 1706
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$19;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1721
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$20;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1795
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 1827
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$22;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 1969
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$23;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseBtnHoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

    .line 1985
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    .line 195
    iput-object p0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    .line 196
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 197
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    .line 198
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 199
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->PROGRESS_MARGIN:I

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->HOVER_IMAGE_GAP:I

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->initFloatingWindow()V

    .line 205
    return-void

    .line 186
    nop

    :array_0
    .array-data 4
        0x7f020012
        0x7f020011
        0x7f020014
        0x7f020013
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->openMVPlaylist()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->openPlayerList()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverDuration:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverDuration:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBottomPadding:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextHeight:I

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/mv/player/view/VideoBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    return p1
.end method

.method static synthetic access$3302(Lcom/sec/android/app/mv/player/view/VideoBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextWidth:I

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/mv/player/view/VideoBtnController;IZ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->updateProgressbarPreviewView(IZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTopPadding:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/view/VideoBtnController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbResume:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    return p1
.end method

.method private changeControllerLayout()V
    .locals 12

    .prologue
    const v11, 0x7f0d0070

    const v10, 0x7f0d0069

    const v9, 0x7f080111

    const/16 v8, 0xb

    const/16 v7, 0x9

    .line 339
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v5, :cond_0

    sget-boolean v5, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v5, :cond_0

    .line 340
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 342
    .local v0, "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d0071

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 343
    .local v2, "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 345
    .local v1, "LP_fit_to_src_btn":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v3, -0x1

    .line 346
    .local v3, "controller_layout_width":I
    const/4 v4, 0x3

    .line 348
    .local v4, "gravity":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080112

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 349
    const/16 v4, 0x11

    .line 351
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 352
    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 353
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 354
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080128

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 358
    const/16 v5, 0x51

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 359
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08010f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 365
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 366
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d0071

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 370
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v5, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->changePlayerListBtn()V

    .line 374
    .end local v0    # "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .end local v1    # "LP_fit_to_src_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "controller_layout_width":I
    .end local v4    # "gravity":I
    :cond_0
    return-void

    .line 363
    .restart local v0    # "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v1    # "LP_fit_to_src_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v2    # "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v3    # "controller_layout_width":I
    .restart local v4    # "gravity":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto :goto_0
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xbb8

    .line 1910
    const/4 v0, 0x0

    .line 1911
    .local v0, "retVal":Z
    packed-switch p1, :pswitch_data_0

    .line 1935
    :goto_0
    return v0

    .line 1916
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 1928
    :goto_1
    const/4 v0, 0x1

    .line 1929
    goto :goto_0

    .line 1918
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 1922
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 1911
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1916
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getFitToScnMode()I
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToScnMode(I)I

    move-result v0

    return v0
.end method

.method private getFitToScnMode(I)I
    .locals 2
    .param p1, "screenMode"    # I

    .prologue
    .line 985
    const/4 v0, 0x0

    .line 987
    .local v0, "retVal":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 988
    move v0, p1

    .line 990
    :cond_0
    return v0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 311
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->initProgress(Landroid/view/View;)V

    .line 312
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->initCtrlButton(Landroid/view/View;)V

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 315
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatter:Ljava/util/Formatter;

    .line 317
    const v0, 0x7f0d006f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeEnd(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v0, :cond_2

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08010b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTopPadding:I

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBottomPadding:I

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextWidth:I

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextHeight:I

    .line 335
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->changeControllerLayout()V

    .line 336
    return-void
.end method

.method private initCtrlButton(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f0a0057

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 438
    const v1, 0x7f0d0070

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 441
    const/4 v0, 0x0

    .line 442
    .local v0, "screenMode":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v1

    const-string v2, "screen_mode"

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewModeRes:[I

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToScnMode(I)I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 446
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v1, :cond_0

    .line 447
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 448
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/16 v2, 0x3135

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 453
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 454
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 457
    .end local v0    # "screenMode":I
    :cond_1
    const v1, 0x7f0d0072

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 458
    const v1, 0x7f0d0073

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    .line 459
    const v1, 0x7f0d0074

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    .line 461
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_c

    .line 462
    const v1, 0x7f0d0071

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    .line 467
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 469
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 471
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v1, :cond_2

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseBtnHoverPopupListener:Landroid/widget/HoverPopupWindow$HoverPopupListener;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 475
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 476
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->updatePausePlayBtn()V

    .line 479
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_5

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_4

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 492
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoBtnController$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 508
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 511
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_7

    .line 512
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0095

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 519
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 520
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_6

    .line 523
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoBtnController$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$3;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 540
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 543
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_9

    .line 544
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoBtnController$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$4;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 553
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v1, :cond_8

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 556
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 559
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_b

    .line 560
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 562
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/mv/player/view/VideoBtnController$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$5;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 569
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v1, :cond_a

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 572
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 574
    :cond_b
    return-void

    .line 464
    :cond_c
    const v1, 0x7f0d0075

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMVPlaylistButton:Landroid/widget/ImageButton;

    goto/16 :goto_0
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 226
    const-string v2, "VideoPlayerBtnController"

    const-string v3, "VideoBtnController - initFloatingWindow"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 229
    .local v0, "mDecor":Landroid/view/View;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 230
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    :cond_0
    return-void
.end method

.method private initProgress(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 399
    const v0, 0x7f0d0066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 404
    new-instance v0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 413
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v0, :cond_0

    .line 414
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 415
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 433
    :cond_0
    :goto_0
    const v0, 0x7f0d006e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    .line 434
    const v0, 0x7f0d006d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 435
    return-void

    .line 421
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 425
    new-instance v0, Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->setDaemon(Z)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->start()V

    goto :goto_0
.end method

.method private isProgressZoomPossible()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 385
    const-string v1, "VideoPlayerBtnController"

    const-string v2, "isProgressZoomPossible"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v0

    .line 391
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private openMVPlaylist()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1939
    const-string v1, "VideoPlayerBtnController"

    const-string v2, "openMVPlaylist E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1940
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1941
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "ListType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1942
    const-string v1, "FromMoviePlayer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1943
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1944
    return-void
.end method

.method private openPlayerList()V
    .locals 2

    .prologue
    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->togglePlayerListState()V

    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setPlayerListType(I)V

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showPlayerList()V

    .line 1952
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->changeControllerLayout()V

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1959
    :goto_0
    return-void

    .line 1955
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->hidePlayerList()V

    .line 1956
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->changeControllerLayout()V

    .line 1957
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0
.end method

.method private setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "l"    # Landroid/widget/HoverPopupWindow$HoverPopupListener;

    .prologue
    .line 1962
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1963
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 1964
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1965
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 1966
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 1967
    return-void
.end method

.method private stringForTimeCur(I)Ljava/lang/String;
    .locals 9
    .param p1, "timeMs"    # I

    .prologue
    const/4 v8, 0x0

    .line 1047
    if-gez p1, :cond_0

    .line 1048
    const/4 p1, 0x0

    .line 1051
    :cond_0
    div-int/lit16 v3, p1, 0x3e8

    .line 1052
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 1053
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 1054
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 1056
    .local v0, "hours":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1057
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private stringForTimeEnd(I)Ljava/lang/String;
    .locals 9
    .param p1, "timeMs"    # I

    .prologue
    const/4 v8, 0x0

    .line 1061
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1062
    :cond_0
    const-string v4, "--:--:--"

    .line 1070
    :goto_0
    return-object v4

    .line 1064
    :cond_1
    div-int/lit16 v3, p1, 0x3e8

    .line 1065
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 1066
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 1067
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 1069
    .local v0, "hours":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1070
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFormatter:Ljava/util/Formatter;

    const-string v5, " %02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private updateProgressbarPreviewView(IZ)I
    .locals 6
    .param p1, "progress"    # I
    .param p2, "isPortrait"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1450
    const-string v2, "VideoPlayerBtnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateProgressbarPreviewView progress : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " orientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    const/4 v1, 0x0

    .line 1452
    .local v1, "x":I
    const/4 p2, 0x1

    .line 1454
    const/4 v0, 0x0

    .line 1455
    .local v0, "progressBarWidth":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v2, :cond_0

    .line 1456
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getWidth()I

    move-result v0

    .line 1459
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v2, :cond_1

    .line 1460
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 1461
    if-eqz p2, :cond_4

    .line 1462
    int-to-float v2, p1

    const v3, 0x47c35000    # 100000.0f

    div-float/2addr v2, v3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 1464
    iget v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->PROGRESS_MARGIN:I

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 1465
    iget v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->HOVER_IMAGE_GAP:I

    sub-int v1, v2, v3

    .line 1466
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    .line 1480
    :cond_1
    :goto_0
    return v1

    .line 1467
    :cond_2
    iget v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->PROGRESS_MARGIN:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_3

    .line 1468
    iget v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->HOVER_IMAGE_GAP:I

    sub-int/2addr v2, v3

    neg-int v1, v2

    .line 1469
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 1471
    :cond_3
    const/4 v1, 0x0

    .line 1472
    iput v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 1475
    :cond_4
    iput v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0
.end method


# virtual methods
.method public changePlayerListBtn()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 382
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayerListButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public dismissAllHoveringPopup()V
    .locals 1

    .prologue
    .line 1693
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v0, :cond_2

    .line 1695
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    .line 1698
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1699
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    .line 1701
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1702
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    .line 1704
    :cond_2
    return-void
.end method

.method public dpToPx(J)I
    .locals 3
    .param p1, "dp"    # J

    .prologue
    .line 1896
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 1897
    .local v0, "density":F
    long-to-float v1, p1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public forceHide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 879
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    if-nez v0, :cond_1

    .line 880
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibility(I)V

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->moveSubtitleDown()V

    .line 883
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setInvisibleAllViews()V

    .line 885
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 886
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z

    .line 888
    :cond_1
    return-void
.end method

.method public getFitToSrcBtn()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 845
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    if-nez v1, :cond_0

    .line 846
    const/4 v0, 0x0

    .line 847
    .local v0, "translateOff":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 848
    .restart local v0    # "translateOff":Landroid/view/animation/Animation;
    const-string v1, "VideoPlayerBtnController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getHeight():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 850
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 851
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 852
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibility(I)V

    .line 853
    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$10;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 870
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 872
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    if-eqz v1, :cond_0

    .line 873
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 876
    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public hide(Z)V
    .locals 2
    .param p1, "hide"    # Z

    .prologue
    const/4 v1, 0x0

    .line 891
    if-eqz p1, :cond_1

    .line 892
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    if-eqz v0, :cond_0

    .line 893
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 895
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibility(I)V

    .line 896
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 897
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z

    .line 899
    :cond_1
    return-void
.end method

.method public isProgressDraging()Z
    .locals 1

    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 765
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 293
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 294
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setPadding()V

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->initControllerView(Landroid/view/View;)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 994
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "VideoBtnController - onDetachedFromWindow"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 998
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 999
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 1000
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->initControllerView(Landroid/view/View;)V

    .line 211
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 822
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbResume:Z

    .line 823
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v1, :cond_0

    .line 824
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v1, :cond_1

    .line 836
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 837
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_2

    .line 838
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 842
    :goto_1
    return-void

    .line 827
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 829
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 830
    sget-object v1, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eq v1, v3, :cond_0

    .line 831
    sget-object v1, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 840
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 803
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbResume:Z

    .line 805
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v0, :cond_0

    .line 806
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 817
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 819
    return-void

    .line 809
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 810
    new-instance v0, Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    .line 811
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->setDaemon(Z)V

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->start()V

    goto :goto_0
.end method

.method public playerStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1440
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeEnd(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1442
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1443
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1445
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1447
    :cond_2
    return-void
.end method

.method public refreshFitToScrBtn()V
    .locals 3

    .prologue
    .line 971
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    if-nez v0, :cond_1

    .line 972
    :cond_0
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "refreshFitToScrBtn() return"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :goto_0
    return-void

    .line 976
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mViewModeRes:[I

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToScnMode()I

    move-result v2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 977
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1902
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 1903
    return-void
.end method

.method public removeHandler()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-wide/16 v0, 0x0

    .line 214
    const-string v2, "VideoPlayerBtnController"

    const-string v3, "removeHandler"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mIsSearch:Z

    if-eqz v2, :cond_1

    move-wide v2, v0

    move v6, v5

    .line 217
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    if-eqz v2, :cond_0

    .line 219
    iput-boolean v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    .line 220
    iget v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    iget v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public removeHoverPopup()V
    .locals 1

    .prologue
    .line 1685
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_0

    .line 1686
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    if-eqz v0, :cond_0

    .line 1687
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 1690
    :cond_0
    return-void
.end method

.method public removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1011
    return-void
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1003
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1005
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1006
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1007
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->removeAllViews()V

    .line 257
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 260
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 261
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 264
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 266
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 269
    :cond_0
    const v3, 0x7f0d0069

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/mv/player/view/VideoBtnController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 290
    return-void
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1892
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 1893
    return-void
.end method

.method public setDisable(Z)V
    .locals 7
    .param p1, "fromLocalVideoPlayer"    # Z

    .prologue
    const v6, 0x7f0d0072

    const v5, 0x7f0d0071

    const/4 v4, 0x0

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v2, 0x0

    .line 577
    if-nez p1, :cond_1

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 580
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v0, :cond_0

    .line 581
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 596
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 619
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_2

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 627
    :cond_2
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1413
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 1436
    :goto_0
    return-void

    .line 1417
    :cond_0
    const-string v0, "VideoPlayerBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1422
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1424
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1427
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1430
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_4

    .line 1431
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1434
    :cond_4
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mButtonEnable:Z

    .line 1435
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setFitBtnDisable()V
    .locals 3

    .prologue
    const v2, 0x7f0d0070

    .line 630
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 633
    return-void
.end method

.method public setFitBtnEnable()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const v2, 0x7f0d0070

    .line 636
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 637
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 640
    return-void
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1907
    return-void
.end method

.method protected setInvisibleAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0069

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0072

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0074

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 911
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0076

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 914
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d006c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 915
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d006d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d006e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 917
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0070

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 918
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 923
    :goto_0
    return-void

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPadding()V
    .locals 3

    .prologue
    const/16 v2, 0x53

    const/4 v1, 0x0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setProgress()I
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 1079
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v10, :cond_0

    iget-boolean v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z

    if-eqz v10, :cond_2

    :cond_0
    move v5, v9

    .line 1128
    :cond_1
    :goto_0
    return v5

    .line 1083
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v5

    .line 1084
    .local v5, "position":I
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v2

    .line 1086
    .local v2, "duration":I
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v10, :cond_7

    .line 1087
    const/16 v10, 0x3e8

    if-le v2, v10, :cond_6

    .line 1088
    const-wide/32 v10, 0x186a0

    int-to-long v12, v5

    mul-long/2addr v10, v12

    int-to-long v12, v2

    div-long v6, v10, v12

    .line 1089
    .local v6, "pos":J
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1090
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v11, v6

    invoke-virtual {v10, v11}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1102
    .end local v6    # "pos":J
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isInitialized()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1103
    if-gtz v2, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1104
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1108
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeEnd(I)Ljava/lang/String;

    move-result-object v3

    .line 1109
    .local v3, "endnewTime":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1111
    .local v1, "currentnowTime":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v9, :cond_4

    .line 1112
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1113
    .local v4, "endnowTime":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v9, :cond_4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1114
    if-lez v2, :cond_4

    .line 1115
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1119
    .end local v4    # "endnowTime":Ljava/lang/String;
    :cond_4
    move v8, v5

    .line 1120
    .local v8, "time":I
    invoke-direct {p0, v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;

    move-result-object v0

    .line 1122
    .local v0, "currentnewTime":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v9, :cond_1

    .line 1123
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1124
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 1125
    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1092
    .end local v0    # "currentnewTime":Ljava/lang/String;
    .end local v1    # "currentnowTime":Ljava/lang/String;
    .end local v3    # "endnewTime":Ljava/lang/String;
    .end local v8    # "time":I
    .restart local v6    # "pos":J
    :cond_5
    const-string v10, "VideoPlayerBtnController"

    const-string v11, "setProgress: mServiceUtil.isPauseEnable() is true."

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1095
    .end local v6    # "pos":J
    :cond_6
    const-string v10, "VideoPlayerBtnController"

    const-string v11, "setProgress: duration is less than zero"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v10, v9}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    :cond_7
    move v5, v9

    .line 1099
    goto/16 :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1076
    return-void
.end method

.method public setProgressMax()V
    .locals 2

    .prologue
    .line 1132
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "setProgressMax()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1136
    return-void
.end method

.method public setUpdate()V
    .locals 6

    .prologue
    const v5, 0x7f0d0074

    const v4, 0x7f0d0073

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->updatePausePlayBtn()V

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 243
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CHINA:Z

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 252
    :cond_1
    :goto_0
    return-void

    .line 248
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method protected setVisibleAllViews()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x1

    const v5, 0x7f0d0074

    const v4, 0x7f0d0073

    const/4 v2, 0x0

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0069

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d006f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0072

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 931
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_4

    .line 932
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0075

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 933
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0071

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 938
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0076

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 939
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0077

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 940
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0066

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 941
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d006c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 942
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d006d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 943
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d006e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 944
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0070

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 946
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CHINA:Z

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_5

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 956
    :cond_0
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isFromLocalVideoPlayer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 957
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isFromLocalVideoPlayer()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setDisable(Z)V

    .line 959
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v0, :cond_2

    .line 960
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 961
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setTooltipHoverPopup(Landroid/view/View;Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 965
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 966
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setFitBtnDisable()V

    .line 968
    :cond_3
    return-void

    .line 935
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0071

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 936
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0075

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 951
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 957
    goto :goto_2
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 769
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    if-nez v1, :cond_0

    .line 770
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibleAllViews()V

    .line 771
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->refreshFitToScrBtn()V

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgress()I

    .line 773
    const/4 v0, 0x0

    .line 774
    .local v0, "translateOn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 775
    .restart local v0    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 776
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 778
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 779
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibility(I)V

    .line 781
    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoBtnController$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController$9;-><init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 795
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z

    .line 798
    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 800
    return-void
.end method

.method public updatePausePlayBtn()V
    .locals 4

    .prologue
    const v3, 0x7f0a009f

    const v2, 0x7f0a009e

    const v1, 0x7f020004

    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 1148
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1152
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1156
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1157
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
