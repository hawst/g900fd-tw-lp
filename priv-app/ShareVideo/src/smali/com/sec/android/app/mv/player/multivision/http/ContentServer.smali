.class public Lcom/sec/android/app/mv/player/multivision/http/ContentServer;
.super Ljava/lang/Object;
.source "ContentServer.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;


# static fields
.field public static final DEFAULT_HTTP_PORT:I = 0x1632

.field private static final STREAMING_PORT:I = 0x11d3

.field private static final TAG:Ljava/lang/String; = "ContentServer"


# instance fields
.field private mArrayKey:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mItemHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mItemHashMutext:Ljava/lang/Object;

.field private mItemInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

.field private mServerPort:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    .line 50
    const/16 v0, 0x1632

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    .line 291
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    .line 293
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    .line 297
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHashMutext:Ljava/lang/Object;

    .line 54
    return-void
.end method

.method private buildResponse(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .locals 19
    .param p1, "httpreq"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 197
    const-string v15, "ContentServer"

    const-string v18, "buildResponse E"

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    new-instance v8, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    invoke-direct {v8}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;-><init>()V

    .line 202
    .local v8, "httpRes":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/net/Uri;

    .line 203
    .local v12, "item":Landroid/net/Uri;
    if-nez v12, :cond_1

    .line 204
    const/16 v15, 0x194

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 205
    const-string v15, "ContentServer"

    const-string v18, "httpRequestRecieved-item NOT_FOUND"

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    :goto_0
    return-object v8

    .line 208
    :cond_1
    invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 210
    .local v6, "filePath":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 211
    const-string v15, "ContentServer"

    const-string v18, "httpRequestRecieved-filePath NOT_FOUND"

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const/16 v15, 0x194

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0

    .line 216
    :cond_2
    const/16 v15, 0xc8

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 221
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 227
    .local v7, "filestream":Ljava/io/FileInputStream;
    const-wide/16 v16, 0x0

    .line 229
    .local v16, "size":J
    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-direct {v9, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 230
    .local v9, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 232
    .local v11, "info":Ljava/lang/String;
    const-string v15, "http://"

    invoke-virtual {v11, v15}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 233
    .local v10, "index":I
    if-lez v10, :cond_5

    .line 234
    const/4 v15, 0x0

    const-string v18, "http://"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v11, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 235
    .local v14, "mimetype":Ljava/lang/String;
    invoke-virtual {v8, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 252
    .end local v14    # "mimetype":Ljava/lang/String;
    :cond_3
    :goto_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v5, "file":Ljava/io/File;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_6

    .line 254
    :cond_4
    const/16 v15, 0x194

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 255
    if-eqz v7, :cond_0

    .line 257
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 258
    :catch_0
    move-exception v2

    .line 259
    .local v2, "e":Ljava/io/IOException;
    const-string v15, "ContentServer"

    const-string v18, "filestream can not be closed."

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 222
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "file":Ljava/io/File;
    .end local v7    # "filestream":Ljava/io/FileInputStream;
    .end local v9    # "in":Ljava/io/InputStream;
    .end local v10    # "index":I
    .end local v11    # "info":Ljava/lang/String;
    .end local v16    # "size":J
    :catch_1
    move-exception v3

    .line 223
    .local v3, "e1":Ljava/io/FileNotFoundException;
    const-string v15, "ContentServer"

    const-string v18, "buildResponse-buildResponse FileNotFoundException"

    move-object/from16 v0, v18

    invoke-static {v15, v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 224
    const/16 v15, 0x194

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0

    .line 237
    .end local v3    # "e1":Ljava/io/FileNotFoundException;
    .restart local v7    # "filestream":Ljava/io/FileInputStream;
    .restart local v9    # "in":Ljava/io/InputStream;
    .restart local v10    # "index":I
    .restart local v11    # "info":Ljava/lang/String;
    .restart local v16    # "size":J
    :cond_5
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v13

    .line 238
    .local v13, "mMap":Landroid/webkit/MimeTypeMap;
    invoke-static {v6}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 239
    .local v4, "extension":Ljava/lang/String;
    const-string v15, "text/plain"

    invoke-virtual {v8, v15}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 240
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_3

    .line 241
    invoke-virtual {v13, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 242
    .restart local v14    # "mimetype":Ljava/lang/String;
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_3

    .line 243
    invoke-virtual {v8, v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    goto :goto_1

    .line 264
    .end local v4    # "extension":Ljava/lang/String;
    .end local v13    # "mMap":Landroid/webkit/MimeTypeMap;
    .end local v14    # "mimetype":Ljava/lang/String;
    .restart local v5    # "file":Ljava/io/File;
    :cond_6
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 268
    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentLength(J)V

    .line 269
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isHeadRequest()Z

    move-result v15

    if-nez v15, :cond_0

    .line 270
    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    goto/16 :goto_0
.end method

.method public static getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 12
    .param p0, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v10, 0x0

    .line 138
    if-nez p0, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object v10

    .line 142
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    .line 143
    .local v2, "clientBytes":[B
    if-eqz v2, :cond_0

    array-length v11, v2

    if-eqz v11, :cond_0

    .line 146
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v8

    .line 147
    .local v8, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    if-eqz v8, :cond_0

    .line 150
    :cond_2
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 151
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/NetworkInterface;

    .line 152
    .local v7, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v7}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    .line 153
    .local v1, "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InterfaceAddress;

    .line 154
    .local v0, "addr":Ljava/net/InterfaceAddress;
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v5

    .line 155
    .local v5, "iAddr":Ljava/net/InetAddress;
    if-eqz v5, :cond_3

    .line 158
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    move-result v11

    div-int/lit8 v6, v11, 0x8

    .line 159
    .local v6, "len":I
    invoke-virtual {v5}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v9

    .line 161
    .local v9, "serverBytes":[B
    if-eqz v9, :cond_3

    .line 164
    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 165
    .local v3, "clientOffset":[B
    invoke-static {v9, v6}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v9

    .line 167
    invoke-static {v9, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 168
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    goto :goto_0

    .line 171
    .end local v0    # "addr":Ljava/net/InterfaceAddress;
    .end local v1    # "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v2    # "clientBytes":[B
    .end local v3    # "clientOffset":[B
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "iAddr":Ljava/net/InetAddress;
    .end local v6    # "len":I
    .end local v7    # "ni":Ljava/net/NetworkInterface;
    .end local v8    # "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v9    # "serverBytes":[B
    :catch_0
    move-exception v11

    goto :goto_0
.end method


# virtual methods
.method public dispatch(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V
    .locals 5
    .param p1, "httpReq"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .prologue
    .line 185
    const-string v3, "ContentServer"

    const-string v4, "dispatch E"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "url":Ljava/lang/String;
    move-object v1, v2

    .line 192
    .local v1, "id":Ljava/lang/String;
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->buildResponse(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    move-result-object v0

    .line 193
    .local v0, "httpRes":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    invoke-virtual {p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)Z

    .line 194
    return-void
.end method

.method public getBoundIPAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 1
    .param p1, "clientAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 177
    invoke-static {p1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    return v0
.end method

.method public getRegisteredContents()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getRegisteredContentsInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public httpRequestRecieved(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .prologue
    .line 101
    const-string v1, "ContentServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "httpRequestRecieved. request string : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isHeadRequest()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isGetRequest()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->dispatch(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V

    .line 116
    :goto_0
    return-void

    .line 112
    :cond_1
    const-string v1, "ContentServer"

    const-string v2, "httpRequestRecieved-setStatusCode BAD_REQUEST"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;-><init>()V

    .line 114
    .local v0, "httpRes":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 115
    invoke-virtual {p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method public isOpened()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->isOpened()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRegisteredContent(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public registerContent(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "item"    # Landroid/net/Uri;
    .param p3, "info"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 304
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 319
    :cond_1
    :goto_0
    return v0

    .line 306
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHashMutext:Ljava/lang/Object;

    monitor-enter v1

    .line 310
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    const/16 v3, 0x3e7

    if-le v2, v3, :cond_3

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 315
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public start()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 63
    const-string v1, "ContentServer"

    const-string v2, "start E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/16 v1, 0x11d3

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->isOpened()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 67
    const-string v1, "ContentServer"

    const-string v2, "start() server is already started"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_0
    return v0

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->open(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 72
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    .line 74
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    add-int/lit16 v1, v1, -0x1632

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 75
    const-string v0, "ContentServer"

    const-string v1, "start() fail to start a simple media server"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_1
    const-string v1, "ContentServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start() local content media server starts!!! - Port: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServerPort:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->addRequestListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->start()Z

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 87
    const-string v0, "ContentServer"

    const-string v1, "stop E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->stop()Z

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->close()Z

    .line 91
    return-void
.end method

.method public unregisterAllContent()V
    .locals 2

    .prologue
    .line 337
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHashMutext:Ljava/lang/Object;

    monitor-enter v1

    .line 338
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 341
    monitor-exit v1

    .line 342
    return-void

    .line 341
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterContent(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 323
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHashMutext:Ljava/lang/Object;

    monitor-enter v1

    .line 324
    if-eqz p1, :cond_0

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mArrayKey:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/ContentServer;->mItemInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :cond_0
    monitor-exit v1

    .line 330
    return-void

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
