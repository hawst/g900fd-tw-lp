.class Lcom/sec/android/app/mv/player/view/VideoFlickView$3;
.super Ljava/lang/Object;
.source "VideoFlickView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/view/VideoFlickView;->rightFlickView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 183
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$500(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoFlickView$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView$3$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoFlickView$3;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 181
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 167
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$500(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$500(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$002(Lcom/sec/android/app/mv/player/view/VideoFlickView;I)I

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;->this$0:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 164
    return-void
.end method
