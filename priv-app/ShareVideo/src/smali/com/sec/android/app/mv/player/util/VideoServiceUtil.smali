.class public Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;,
        Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;,
        Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;,
        Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;
    }
.end annotation


# static fields
.field public static final BUFFERING_CHECK_DELAY:I = 0x12c

.field public static final CMD_BOTTOM:I = 0x12

.field public static final CMD_FFLONGSEEK:I = 0x6

.field public static final CMD_FFSHORTSEEK:I = 0x4

.field public static final CMD_NEXT:I = 0x8

.field public static final CMD_NEXT_BY_KEYPAD:I = 0xc

.field public static final CMD_PLAYSHORT:I = 0x3

.field public static final CMD_PREV:I = 0x9

.field public static final CMD_PREV_BY_KEYPAD:I = 0xd

.field public static final CMD_RESET:I = 0xb

.field public static final CMD_RESETSEEK:I = 0xa

.field public static final CMD_REWLONGSEEK:I = 0x7

.field public static final CMD_REWSHORTSEEK:I = 0x5

.field public static final CMD_TOP:I = 0x11

.field private static final LONGSEEKMSG:I = 0x3

.field public static final LONG_PRESS_TIME:J = 0x1f4L

.field public static final LOW_BATTERY_THRESHOLD:I = 0x1

.field public static final MAXPLAYINGCHECKTIME:I = 0xc350

.field public static final NOTI_FINISH:I = 0x0

.field public static final NOTI_PLAY_CHANGE:I = 0x2

.field public static final NOTI_PLAY_STOP:I = 0x1

.field public static final NOTI_PROGRESS_BAR:I = 0x5

.field public static final NOTI_SHOW_CONTROL:I = 0x4

.field public static final NO_TIMEOUT:I = 0x36ee80

.field public static final PLAYER_PAUSE:I = 0x1

.field public static final PLAYER_PLAY:I = 0x0

.field public static final PLAYER_PREPARED:I = 0x4

.field public static final PLAYER_PREPARING:I = 0x3

.field public static final PLAYER_STOP:I = 0x2

.field public static final PLAYING_CHECK_DELAY:I = 0x12c

.field public static final SAVE_BEFORE:I = 0x1388

.field public static final SEEK_MODE_FF:I = 0x1

.field public static final SEEK_MODE_NORMAL:I = 0x0

.field public static final SEEK_MODE_REW:I = 0x2

.field private static final STARTPLAYBACK:I = 0x1

.field private static SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VideoServiceUtil"

.field private static mPausedByUser:Z

.field private static sConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mAudioOnlyClip:Z

.field private mBucketId:I

.field public mBufferingChecker:Ljava/lang/Runnable;

.field private mContext:Landroid/content/Context;

.field private mCurIdx:I

.field private mCurPlayingPath:Ljava/lang/String;

.field private mCurPlayingUri:Landroid/net/Uri;

.field private mCurVideoDbId:J

.field private mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

.field private mHandler:Landroid/os/Handler;

.field private mIsPauseEnable:Z

.field private mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

.field private mLastPos:J

.field private mLongKeyCnt:I

.field private mLongSeekMode:I

.field public mMoviePlayerOnResume:Z

.field private mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

.field public mOnSvcNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;

.field private mPauseSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mPausedOnPlaying:Z

.field private mPlayerState:I

.field private mPlayingBeforePalm:Z

.field private mPlayingCheckTime:I

.field public mPlayingChecker:Ljava/lang/Runnable;

.field private mResumePosition:J

.field private mSeekPos:I

.field private mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

.field private mVideoOnlyClip:Z

.field private sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sConnectionMap:Ljava/util/HashMap;

    .line 150
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedByUser:Z

    .line 194
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 6
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 137
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 143
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 148
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mIsPauseEnable:Z

    .line 154
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mMoviePlayerOnResume:Z

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedOnPlaying:Z

    .line 158
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    .line 160
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoOnlyClip:Z

    .line 162
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mAudioOnlyClip:Z

    .line 164
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 166
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBucketId:I

    .line 168
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayerState:I

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurIdx:I

    .line 172
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 174
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 176
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 178
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    .line 180
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 182
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurVideoDbId:J

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    .line 188
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    .line 192
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 942
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$1;-><init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    .line 1047
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;-><init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    .line 1693
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;-><init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    .line 197
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    .line 198
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 205
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    return v0
.end method

.method static synthetic access$020(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .param p1, "x1"    # I

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->checkLockScreenOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->ffSeek()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->rewSeek()V

    return-void
.end method

.method private checkLockScreenOn()Z
    .locals 3

    .prologue
    .line 998
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1000
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1001
    const-string v1, "VideoServiceUtil"

    const-string v2, "checkLockScreenOn() - isKeyguardLocked TRUE. Lock Screen ON"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    const/4 v1, 0x1

    .line 1005
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private ffSeek()V
    .locals 9

    .prologue
    const/16 v5, 0xc

    const/16 v8, 0xa

    const/4 v4, 0x2

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 477
    const-string v2, "VideoServiceUtil"

    const-string v3, "ffSeek E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v1, 0x0

    .line 482
    .local v1, "scale":I
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-le v2, v5, :cond_0

    .line 483
    iput v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 488
    :cond_0
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    if-nez v2, :cond_1

    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 492
    :cond_1
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-lt v2, v4, :cond_4

    .line 493
    const/4 v1, 0x1

    .line 501
    :goto_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 503
    .local v0, "powVal":I
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    mul-int/lit16 v3, v1, 0x3e8

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 504
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 506
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 507
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 508
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 512
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 513
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 518
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const/4 v3, 0x5

    invoke-interface {v2, v3, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 520
    if-ne v1, v7, :cond_3

    .line 521
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    if-gtz v2, :cond_3

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const/4 v3, 0x4

    invoke-interface {v2, v3, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 523
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 527
    :cond_3
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 528
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sendMessage(IJ)V

    .line 529
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 533
    :goto_2
    return-void

    .line 494
    .end local v0    # "powVal":I
    :cond_4
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-lt v2, v7, :cond_5

    .line 495
    const/4 v1, 0x1

    .line 496
    iput v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    goto :goto_0

    .line 498
    :cond_5
    const/4 v1, -0x1

    goto :goto_0

    .line 515
    .restart local v0    # "powVal":I
    :cond_6
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_1

    .line 531
    :cond_7
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2
.end method

.method public static getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I
    .locals 6

    .prologue
    .line 1861
    sget v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    if-gez v3, :cond_0

    .line 1863
    :try_start_0
    const-class v3, Landroid/view/View;

    const-string v4, "SYSTEM_UI_FLAG_HIDE_NAVIGATION"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1864
    .local v1, "field":Ljava/lang/reflect/Field;
    if-nez v1, :cond_1

    .line 1865
    const/4 v3, 0x2

    sput v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1875
    :cond_0
    :goto_0
    sget v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    return v3

    .line 1867
    :cond_1
    :try_start_1
    const-string v3, "SYSTEM_UI_FLAG_HIDE_NAVIGATION"

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 1868
    .local v2, "systemUiVisibility":I
    sput v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1870
    .end local v2    # "systemUiVisibility":I
    :catch_0
    move-exception v0

    .line 1871
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setRemoveSystemUI not in build package : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isBrowser(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 1738
    const-string v0, "http"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rtsp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sshttp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1739
    :cond_0
    const/4 v0, 0x1

    .line 1741
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPausedByUser()Z
    .locals 1

    .prologue
    .line 1887
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedByUser:Z

    return v0
.end method

.method public static isResolutionQVGA(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x140

    const/16 v3, 0xf0

    .line 1907
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1908
    .local v1, "wm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1909
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    if-eq v2, v4, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    if-ne v2, v4, :cond_2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 1911
    :cond_1
    const/4 v2, 0x1

    .line 1913
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isShortSeekDisalbedCase()Z
    .locals 3

    .prologue
    .line 889
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isNextAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 890
    :cond_0
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isShortSeekDisalbedCase() - getPlayerState():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    const/4 v0, 0x1

    .line 893
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeStopPlayingCallbacks()V
    .locals 2

    .prologue
    .line 935
    const-string v0, "VideoServiceUtil"

    const-string v1, "removeStopPlayingCallbacks() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 939
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 940
    return-void
.end method

.method private resetLongPressCounter()V
    .locals 1

    .prologue
    .line 885
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 886
    return-void
.end method

.method public static resetPausedByUserFlag()V
    .locals 1

    .prologue
    .line 1895
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedByUser:Z

    .line 1896
    return-void
.end method

.method private rewSeek()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v5, -0x2

    const/16 v4, -0xc

    const/4 v6, 0x0

    .line 537
    const-string v2, "VideoServiceUtil"

    const-string v3, "rewSeek E."

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const/4 v1, 0x0

    .line 542
    .local v1, "scale":I
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-ge v2, v4, :cond_0

    .line 543
    iput v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 546
    :cond_0
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    if-nez v2, :cond_1

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 550
    :cond_1
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-gt v2, v5, :cond_4

    .line 551
    const/4 v1, -0x1

    .line 559
    :goto_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 560
    .local v0, "powVal":I
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    mul-int/lit16 v3, v1, 0x3e8

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 561
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 563
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    if-gtz v2, :cond_2

    .line 564
    iput v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 567
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 568
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 573
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const/4 v3, 0x5

    invoke-interface {v2, v3, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 575
    if-ne v1, v7, :cond_3

    .line 576
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 577
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 581
    :cond_3
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    if-lez v2, :cond_7

    .line 584
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v8, v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sendMessage(IJ)V

    .line 585
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 593
    :goto_2
    return-void

    .line 552
    .end local v0    # "powVal":I
    :cond_4
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    if-gt v2, v7, :cond_5

    .line 553
    const/4 v1, -0x1

    .line 554
    iput v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongKeyCnt:I

    goto :goto_0

    .line 556
    :cond_5
    const/4 v1, 0x1

    goto :goto_0

    .line 570
    .restart local v0    # "powVal":I
    :cond_6
    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_1

    .line 587
    :cond_7
    const-string v2, "rewSeek"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 589
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const/4 v3, 0x4

    invoke-interface {v2, v3, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 590
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removeMessage(I)V

    .line 591
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetLongPressCounter()V

    goto :goto_2
.end method

.method public static setPausedByUser()V
    .locals 1

    .prologue
    .line 1891
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedByUser:Z

    .line 1892
    return-void
.end method

.method private shiftContent(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Z
    .locals 8
    .param p1, "direction"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 413
    const-string v5, "VideoServiceUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shiftContent() - direction:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    sget-boolean v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    if-eqz v5, :cond_0

    .line 416
    const-string v4, "VideoServiceUtil"

    const-string v5, "shiftContent() - mCompletedListenrCall is true"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :goto_0
    return v3

    .line 420
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    .line 421
    .local v1, "pl":Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    if-nez v1, :cond_1

    .line 422
    const-string v3, "VideoServiceUtil"

    const-string v5, "shiftContent() : mMVPlaylist is null"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 423
    goto :goto_0

    .line 426
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v0

    .line 427
    .local v0, "idx":I
    const-string v5, "VideoServiceUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shiftContent() : idx is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 431
    sget-object v4, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 433
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_4

    .line 434
    const/4 v0, 0x0

    .line 447
    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 450
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFPS()I

    move-result v4

    const/16 v5, 0x1e

    if-le v4, v5, :cond_3

    .line 451
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->dropLCDfps(Z)Z

    .line 454
    :cond_3
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 455
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 458
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 459
    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getResumePos(I)I

    move-result v4

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    .line 464
    :goto_2
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 466
    const-string v4, "VideoServiceUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "shiftContent() ---- mCurIdx = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  mMVPlaylist.size() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v4, "VideoServiceUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "shiftContent() ---- uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mResumePosition = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetTrackInfo()V

    .line 470
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startNextPrevPlayback()V

    .line 471
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v4}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->initSubtitle()V

    goto/16 :goto_0

    .line 436
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 438
    :cond_5
    sget-object v4, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 440
    if-gtz v0, :cond_6

    .line 441
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    goto/16 :goto_1

    .line 443
    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_1

    .line 461
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_7
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    goto :goto_2
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1441
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1442
    const-string v1, "VideoServiceUtil"

    const-string v2, "addOutbandSubTitle() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    :goto_0
    return-void

    .line 1447
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->addOutbandSubTitle(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1448
    :catch_0
    move-exception v0

    .line 1449
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addOutbandSubTitle() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bindToService(Landroid/content/ServiceConnection;)Z
    .locals 5
    .param p1, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 1619
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1620
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;-><init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Landroid/content/ServiceConnection;)V

    .line 1621
    .local v0, "sb":Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;
    sget-object v1, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sConnectionMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1622
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setServiceContext()V

    .line 1623
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    return v1
.end method

.method public bottom()Z
    .locals 1

    .prologue
    .line 405
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->BOTTOM:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public bufferstate()V
    .locals 4

    .prologue
    .line 1678
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removeStopPlayingCallbacks()V

    .line 1680
    const-string v0, "VideoServiceUtil"

    const-string v1, "bufferstate"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1682
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1684
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestLayout()V

    .line 1691
    :cond_0
    :goto_0
    return-void

    .line 1688
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1689
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public controlRequest(I)V
    .locals 11
    .param p1, "command"    # I

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x2

    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 603
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() command = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    packed-switch p1, :pswitch_data_0

    .line 617
    :cond_0
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    .line 882
    :cond_1
    :goto_0
    :pswitch_1
    return-void

    .line 607
    :pswitch_2
    const-string v3, "VideoServiceUtil"

    const-string v4, "not supported in share video. unpausable"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 611
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 612
    const-string v3, "VideoServiceUtil"

    const-string v4, "not supported in share video client."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 619
    :pswitch_4
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mIsPauseEnable:Z

    if-eqz v3, :cond_3

    .line 620
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 621
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 622
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    goto :goto_0

    .line 624
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    .line 625
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetPausedByUserFlag()V

    goto :goto_0

    .line 628
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 629
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stop()V

    .line 630
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    goto :goto_0

    .line 632
    :cond_4
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetPausedByUserFlag()V

    goto :goto_0

    .line 639
    :pswitch_5
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_PREV/NEXT = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v3, :cond_5

    .line 641
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 644
    :cond_5
    const/16 v3, 0x8

    if-ne p1, v3, :cond_6

    .line 645
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->next()Z

    move-result v2

    .line 649
    .local v2, "result":Z
    :goto_1
    if-eqz v2, :cond_7

    .line 650
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v6, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto :goto_0

    .line 647
    .end local v2    # "result":Z
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->prev()Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_1

    .line 652
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v8, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto :goto_0

    .line 656
    .end local v2    # "result":Z
    :pswitch_6
    const-string v3, "VideoServiceUtil"

    const-string v4, "controlRequest() - CMD_NEXT_BY_KEYPAD"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v3, :cond_8

    .line 658
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 660
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v10, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 661
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v3, :cond_1

    .line 662
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    .line 663
    .local v1, "pos":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v3

    add-int/lit16 v3, v3, -0x3a98

    if-lt v1, v3, :cond_9

    .line 664
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->next()Z

    goto/16 :goto_0

    .line 666
    :cond_9
    add-int/lit16 v3, v1, 0x3a98

    invoke-virtual {p0, v3, v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_0

    .line 672
    .end local v1    # "pos":I
    :pswitch_7
    const-string v3, "VideoServiceUtil"

    const-string v4, "controlRequest() - CMD_PREV_BY_KEYPAD"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v3, :cond_a

    .line 675
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 678
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v10, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 680
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v3, :cond_1

    .line 681
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    .line 682
    .restart local v1    # "pos":I
    const/16 v3, 0xbb8

    if-gt v1, v3, :cond_b

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->prev()Z

    goto/16 :goto_0

    .line 684
    :cond_b
    const/16 v3, 0x3a98

    if-gt v1, v3, :cond_c

    .line 685
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_0

    .line 687
    :cond_c
    add-int/lit16 v3, v1, -0x3a98

    invoke-virtual {p0, v3, v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_0

    .line 693
    .end local v1    # "pos":I
    :pswitch_8
    const-string v3, "VideoServiceUtil"

    const-string v4, "controlRequest() - CMD_FFSHORTSEEK"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isShortSeekDisalbedCase()Z

    move-result v3

    if-nez v3, :cond_1

    .line 699
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v3, :cond_d

    .line 700
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 711
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->next()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 712
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v6, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 714
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v8, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 722
    :pswitch_9
    const-string v3, "VideoServiceUtil"

    const-string v4, "controlRequest() - CMD_REWSHORTSEEK"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isShortSeekDisalbedCase()Z

    move-result v3

    if-nez v3, :cond_1

    .line 728
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v3, :cond_f

    .line 729
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 732
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    .line 733
    .local v0, "cpos":I
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest CMD_REWSHORTSEEK. cpos = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const/16 v3, 0xbb8

    if-le v0, v3, :cond_10

    .line 750
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_0

    .line 751
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->prev()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 752
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v6, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 754
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v8, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 763
    .end local v0    # "cpos":I
    :pswitch_a
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_FFLONGSEEK state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v3

    if-eq v3, v9, :cond_1

    .line 769
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v3, :cond_12

    .line 775
    iput v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 776
    iput v9, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 777
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 778
    const/4 v3, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 780
    :cond_12
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v3, v9, :cond_13

    .line 781
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 783
    :cond_13
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v3, v6, :cond_1

    .line 784
    iput v9, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 785
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 791
    :pswitch_b
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_REWLONGSEEK state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v3

    if-eq v3, v9, :cond_1

    .line 797
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v3, :cond_14

    .line 805
    iput v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mSeekPos:I

    .line 806
    iput v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 807
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 808
    const/4 v3, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 810
    :cond_14
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v3, v6, :cond_15

    .line 811
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 813
    :cond_15
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v3, v9, :cond_1

    .line 814
    iput v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 815
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const v4, 0x36ee80

    invoke-interface {v3, v10, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 821
    :pswitch_c
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_RESETSEEK state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v3, :cond_1

    .line 826
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removeMessage(I)V

    .line 829
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v3

    int-to-long v4, v3

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 831
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_16

    .line 832
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 834
    :cond_16
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 835
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_17

    .line 836
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    long-to-int v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 850
    :cond_17
    :goto_2
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    .line 852
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v10, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    .line 853
    iput v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 854
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetLongPressCounter()V

    goto/16 :goto_0

    .line 839
    :cond_18
    const-string v3, "VideoServiceUtil"

    const-string v4, "Real seek called!!!!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    long-to-int v3, v4

    invoke-virtual {p0, v3, v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_2

    .line 858
    :pswitch_d
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_RESET state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removeMessage(I)V

    .line 861
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v3, :cond_19

    .line 862
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 864
    :cond_19
    iput v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 865
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetLongPressCounter()V

    goto/16 :goto_0

    .line 870
    :pswitch_e
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "controlRequest() - CMD_BOTTOM/TOP = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    const/16 v3, 0x11

    if-ne p1, v3, :cond_1a

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->top()Z

    move-result v2

    .line 876
    .restart local v2    # "result":Z
    :goto_3
    if-eqz v2, :cond_1b

    .line 877
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v6, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 875
    .end local v2    # "result":Z
    :cond_1a
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->bottom()Z

    move-result v2

    .restart local v2    # "result":Z
    goto :goto_3

    .line 879
    :cond_1b
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v8, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto/16 :goto_0

    .line 604
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 617
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_5
        :pswitch_5
        :pswitch_c
        :pswitch_d
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_e
        :pswitch_e
    .end packed-switch
.end method

.method public getBucketID()I
    .locals 1

    .prologue
    .line 1026
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBucketId:I

    return v0
.end method

.method public getBufferPercentage()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1387
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1394
    :goto_0
    return v1

    .line 1391
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getBufferPercentage()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1392
    :catch_0
    move-exception v0

    .line 1393
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBufferPercentage() RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCurIdx()I
    .locals 1

    .prologue
    .line 1014
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurIdx:I

    return v0
.end method

.method public getCurPlayingPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1311
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1317
    :goto_0
    return v1

    .line 1314
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->position()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 1315
    :catch_0
    move-exception v0

    .line 1316
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentPosition() RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDuration()I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 1300
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1306
    :goto_0
    return v1

    .line 1303
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->duration()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 1304
    :catch_0
    move-exception v0

    .line 1305
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDuration() RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getFPS()I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 1563
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1570
    :goto_0
    return v1

    .line 1567
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getFPS()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1568
    :catch_0
    move-exception v0

    .line 1569
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFPS() - RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1757
    const-string v0, " "

    .line 1758
    .local v0, "displayName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    .line 1759
    .local v6, "uri":Landroid/net/Uri;
    const-string v7, "VideoServiceUtil"

    const-string v8, "getDisplayName "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    if-eqz v6, :cond_3

    .line 1763
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isBrowser(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1764
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1765
    .local v2, "filePath":Ljava/lang/String;
    const-string v7, "VideoServiceUtil"

    const-string v8, "getDispalyName(): streaming type."

    invoke-static {v7, v8}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    const/16 v7, 0x2f

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1768
    .local v3, "lastIndex1":I
    const/16 v7, 0x2e

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 1770
    .local v4, "lastIndex2":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v4, v7, :cond_1

    .line 1772
    add-int/lit8 v7, v3, 0x1

    :try_start_0
    invoke-virtual {v2, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1789
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "lastIndex1":I
    .end local v4    # "lastIndex2":I
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1790
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content://mms"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content://gmail-ls"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1792
    invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v7

    .line 1796
    :goto_1
    return-object v7

    .line 1773
    .restart local v2    # "filePath":Ljava/lang/String;
    .restart local v3    # "lastIndex1":I
    .restart local v4    # "lastIndex2":I
    :catch_0
    move-exception v1

    .line 1774
    .local v1, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string v0, " "

    .line 1775
    goto :goto_0

    .line 1777
    .end local v1    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_1
    const-string v0, " "

    goto :goto_0

    .line 1780
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "lastIndex1":I
    .end local v4    # "lastIndex2":I
    :cond_2
    move-object v5, v6

    .line 1782
    .local v5, "newUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/mv/player/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1784
    if-nez v0, :cond_0

    .line 1785
    const-string v0, " "

    goto :goto_0

    .end local v5    # "newUri":Landroid/net/Uri;
    :cond_3
    move-object v7, v0

    .line 1796
    goto :goto_1
.end method

.method public getFileName(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;
    .locals 6
    .param p1, "direction"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 1808
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    .line 1809
    .local v1, "pl":Lcom/sec/android/app/mv/player/common/PlaylistUtils;
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v0

    .line 1811
    .local v0, "idx":I
    sget-object v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    if-ne p1, v3, :cond_1

    .line 1813
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_0

    .line 1814
    const/4 v0, 0x0

    .line 1829
    :goto_0
    sget-object v3, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1830
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .end local v2    # "uri":Landroid/net/Uri;
    :goto_1
    return-object v3

    .line 1816
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1818
    :cond_1
    sget-object v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    if-ne p1, v3, :cond_3

    .line 1820
    if-gtz v0, :cond_2

    .line 1821
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    goto :goto_0

    .line 1823
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1826
    :cond_3
    const-string v3, ""

    goto :goto_1
.end method

.method public getMVMembersNumber()I
    .locals 1

    .prologue
    .line 1593
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getMVMembersNumber()I

    move-result v0

    return v0
.end method

.method public getMeasuredHeightRatio()I
    .locals 1

    .prologue
    .line 1597
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getMeasuredHeightRatio()I

    move-result v0

    return v0
.end method

.method public getNextFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1800
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFileName(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerState()I
    .locals 1

    .prologue
    .line 1834
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayerState:I

    return v0
.end method

.method public getPrevFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1804
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFileName(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperPathToOpen()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1035
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    .line 1037
    .local v0, "currentUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 1038
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1039
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1044
    .end local v1    # "uri":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public getResumePosition()J
    .locals 2

    .prologue
    .line 1899
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    return-wide v0
.end method

.method public getServerTimestampInfo()[J
    .locals 4

    .prologue
    .line 2009
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getServerTimestampInfo()[J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2012
    :goto_0
    return-object v1

    .line 2010
    :catch_0
    move-exception v0

    .line 2011
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getServerTimestampInfo() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2012
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSubtitlPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    const/4 v1, 0x0

    .line 210
    .local v1, "subtitlePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    .line 213
    .local v0, "subtitle":Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    .line 218
    .end local v0    # "subtitle":Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    :cond_0
    return-object v1
.end method

.method public getTrackInfoUtil()Lcom/sec/android/app/mv/player/util/TrackInfoUtil;
    .locals 1

    .prologue
    .line 1921
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    return-object v0
.end method

.method public getVideoDbId()J
    .locals 2

    .prologue
    .line 1010
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurVideoDbId:J

    return-wide v0
.end method

.method public getVideoHeight()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1551
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1558
    :goto_0
    return v1

    .line 1555
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getVideoHeight()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1556
    :catch_0
    move-exception v0

    .line 1557
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoHeight() - RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getVideoUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getVideoWidth()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1539
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1546
    :goto_0
    return v1

    .line 1543
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getVideoWidth()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1544
    :catch_0
    move-exception v0

    .line 1545
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoWidth() - RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initSelectSubtitleTrack()V
    .locals 4

    .prologue
    .line 1483
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1484
    const-string v1, "VideoServiceUtil"

    const-string v2, "initSelectSubtitleTrack() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    :goto_0
    return-void

    .line 1489
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->initSelectSubtitleTrack()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1490
    :catch_0
    move-exception v0

    .line 1491
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSelectSubtitleTrack() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1455
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1456
    const-string v1, "VideoServiceUtil"

    const-string v2, "initSubtitle() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    :goto_0
    return-void

    .line 1461
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->initSubtitle(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1462
    :catch_0
    move-exception v0

    .line 1463
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitle() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public is1080PEquivalent()Z
    .locals 3

    .prologue
    .line 1991
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 1992
    .local v0, "videoRes":I
    const v1, 0x11cc40

    if-ge v0, v1, :cond_1

    const v1, 0x7e900

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x3c

    if-ge v1, v2, :cond_1

    :cond_0
    const v1, 0x38400

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x78

    if-lt v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public is1088pEquivalent()Z
    .locals 4

    .prologue
    const/16 v3, 0x780

    const/16 v2, 0x440

    .line 1983
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v0

    .line 1984
    .local v0, "videoHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v1

    .line 1986
    .local v1, "videoWidth":I
    if-lt v1, v0, :cond_0

    if-gt v1, v3, :cond_1

    if-gt v0, v2, :cond_1

    :cond_0
    if-ge v1, v0, :cond_2

    if-gt v1, v2, :cond_1

    if-le v0, v3, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public is720PEquivalent()Z
    .locals 3

    .prologue
    .line 1997
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 1998
    .local v0, "videoRes":I
    const v1, 0x7e900

    if-ge v0, v1, :cond_0

    const v1, 0x38400

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x3c

    if-lt v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAudioOnlyClip()Z
    .locals 1

    .prologue
    .line 1938
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mAudioOnlyClip:Z

    return v0
.end method

.method public isHttpBrowser()Z
    .locals 2

    .prologue
    .line 1746
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    .line 1748
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 1749
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isBrowser(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1750
    const/4 v1, 0x1

    .line 1753
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1375
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1382
    :goto_0
    return v1

    .line 1379
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->isInitialized()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1380
    :catch_0
    move-exception v0

    .line 1381
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isInitialized() RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isLongSeekMode()Z
    .locals 1

    .prologue
    .line 1846
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v0, :cond_0

    .line 1847
    const/4 v0, 0x1

    .line 1849
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMVServerType()Z
    .locals 1

    .prologue
    .line 1609
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    return v0
.end method

.method public isPauseEnable()Z
    .locals 1

    .prologue
    .line 1879
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mIsPauseEnable:Z

    return v0
.end method

.method public isPausedByTransientLossOfFocus()Z
    .locals 2

    .prologue
    .line 1972
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v1, :cond_0

    .line 1974
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->isPausedByTransientLossOfFocus()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1979
    :goto_0
    return v1

    .line 1975
    :catch_0
    move-exception v0

    .line 1976
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1979
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1363
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1370
    :goto_0
    return v1

    .line 1367
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1368
    :catch_0
    move-exception v0

    .line 1369
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPlaying() RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isPlayingBeforePalm()Z
    .locals 1

    .prologue
    .line 1853
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    return v0
.end method

.method public isQcifOrLowerResClip()Z
    .locals 3

    .prologue
    .line 2002
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 2003
    .local v0, "videoRes":I
    const/16 v1, 0x7900

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1575
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v2, :cond_0

    .line 1582
    :goto_0
    return v1

    .line 1579
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->isSecVideo()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1580
    :catch_0
    move-exception v0

    .line 1581
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSecVideo() - RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isShareVideoMode()Z
    .locals 1

    .prologue
    .line 1601
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    return v0
.end method

.method public isSingleVisionMode()Z
    .locals 1

    .prologue
    .line 1605
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    return v0
.end method

.method public isVideoOnlyClip()Z
    .locals 1

    .prologue
    .line 1930
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoOnlyClip:Z

    return v0
.end method

.method public muteDevice()V
    .locals 4

    .prologue
    .line 1427
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1438
    :goto_0
    return-void

    .line 1432
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->muteDevice()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1433
    :catch_0
    move-exception v0

    .line 1434
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1435
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1436
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - NullPointerException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public next()Z
    .locals 1

    .prologue
    .line 397
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 1157
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v0, :cond_1

    .line 1167
    :cond_0
    :goto_0
    return-void

    .line 1162
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1164
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1165
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_0
.end method

.method public pauseBy(Ljava/lang/Object;)V
    .locals 3
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1228
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseBy. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1230
    const-string v0, "VideoServiceUtil"

    const-string v1, "pauseBy do nothing in RTSP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    :goto_0
    return-void

    .line 1234
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1235
    const-string v0, "VideoServiceUtil"

    const-string v1, "MV_SERVER pauseBy() skip code"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1239
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1240
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1241
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 1242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedOnPlaying:Z

    .line 1248
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1244
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedOnPlaying:Z

    goto :goto_1
.end method

.method public pauseOrStopPlaying()V
    .locals 4

    .prologue
    .line 1206
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    .line 1207
    .local v0, "isPlaying":Z
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pauseOrStopPlaying. isPlaying = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pauseEnable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    if-eqz v0, :cond_0

    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 1214
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_0

    .line 1215
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1217
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1225
    :cond_0
    :goto_0
    return-void

    .line 1222
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stop()V

    goto :goto_0
.end method

.method public pauseS()Z
    .locals 4

    .prologue
    .line 1171
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1177
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1173
    :catch_0
    move-exception v0

    .line 1174
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public playS()Z
    .locals 4

    .prologue
    .line 1148
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->play()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1153
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1149
    :catch_0
    move-exception v0

    .line 1150
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public prev()Z
    .locals 1

    .prologue
    .line 401
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public removeHandlerMessage()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 266
    return-void
.end method

.method public removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 905
    return-void
.end method

.method public removePauseSet(Ljava/lang/Object;)V
    .locals 3
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1277
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removePauseSet. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1279
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1282
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 1285
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reset() sService = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1297
    :goto_0
    return-void

    .line 1290
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->stop()V

    .line 1291
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->reset()V

    .line 1292
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1293
    :catch_0
    move-exception v0

    .line 1294
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reset() RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resetMediaPlayerOnInfoSetting()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1942
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsVideoOnlyClip(Z)V

    .line 1943
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsAudioOnlyClip(Z)V

    .line 1944
    return-void
.end method

.method public resetSubtitle()V
    .locals 4

    .prologue
    .line 1497
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1498
    const-string v1, "VideoServiceUtil"

    const-string v2, "resetSubtitle() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1508
    :goto_0
    return-void

    .line 1503
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->resetSubtitle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1504
    :catch_0
    move-exception v0

    .line 1505
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetSubtitle() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resetTrackInfo()V
    .locals 4

    .prologue
    .line 1957
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1958
    const-string v1, "VideoServiceUtil"

    const-string v2, "resetTrackInfo() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1969
    :goto_0
    return-void

    .line 1963
    :cond_0
    :try_start_0
    const-string v1, "VideoServiceUtil"

    const-string v2, "resetTrackInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1964
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->resetTrackInfo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1965
    :catch_0
    move-exception v0

    .line 1966
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetTrackInfo() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resumeBy(Ljava/lang/Object;)V
    .locals 3
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1252
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeBy. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1255
    const-string v0, "VideoServiceUtil"

    const-string v1, "resumeBy do nothing in RTSP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1274
    :cond_0
    :goto_0
    return-void

    .line 1259
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1261
    const-string v0, "VideoServiceUtil"

    const-string v1, "MV_SERVER resumeBy() skip code"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1265
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1267
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedOnPlaying:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mMoviePlayerOnResume:Z

    if-eqz v0, :cond_3

    .line 1268
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    .line 1271
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedOnPlaying:Z

    goto :goto_0
.end method

.method public saveResumePosition(ZZ)V
    .locals 10
    .param p1, "pauseActivity"    # Z
    .param p2, "rewind"    # Z

    .prologue
    .line 336
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveResumePosition E. mPlayerState = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const/4 v5, 0x0

    .line 341
    .local v5, "isDrm":Z
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 342
    const/4 v6, -0x1

    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 343
    const-string v6, "VideoServiceUtil"

    const-string v7, "saveResumePosition() is DRM file"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v5, 0x1

    .line 347
    :cond_0
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateResumePos() - isDrm = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_1
    const-wide/16 v0, 0x0

    .line 351
    .local v0, "curPos":J
    const-wide/16 v2, 0x0

    .line 353
    .local v2, "duration":J
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    .line 354
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveResumePosition skip saving position due to PlayerStatus = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :goto_0
    return-void

    .line 359
    :cond_2
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v6}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->position()J

    move-result-wide v0

    .line 360
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v6}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->duration()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 366
    :goto_1
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveResumePosition. curPos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", duration = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    if-eqz p2, :cond_8

    .line 369
    const-wide/16 v6, 0x1388

    cmp-long v6, v0, v6

    if-gez v6, :cond_6

    .line 370
    const-wide/16 v0, 0x0

    .line 384
    :cond_3
    :goto_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    invoke-virtual {v6, v7, v0, v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 386
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setResumePos(J)V

    .line 389
    :cond_4
    if-eqz p1, :cond_5

    .line 390
    iput-wide v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    .line 393
    :cond_5
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "saveResumePosition X. mPausedByUser :  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-boolean v8, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPausedByUser:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mResumePosition : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 361
    :catch_0
    move-exception v4

    .line 362
    .local v4, "e":Landroid/os/RemoteException;
    invoke-virtual {v4}, Landroid/os/RemoteException;->printStackTrace()V

    .line 363
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RemoteException occured  1 :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 371
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_6
    sub-long v6, v2, v0

    const-wide/16 v8, 0x1388

    cmp-long v6, v6, v8

    if-gtz v6, :cond_7

    .line 372
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 374
    :cond_7
    const-wide/16 v6, 0x1388

    sub-long/2addr v0, v6

    goto :goto_2

    .line 377
    :cond_8
    const-wide/16 v6, 0x3e8

    cmp-long v6, v0, v6

    if-gez v6, :cond_9

    .line 378
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 379
    :cond_9
    sub-long v6, v2, v0

    const-wide/16 v8, 0x3e8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    .line 380
    const-wide/16 v0, 0x0

    goto/16 :goto_2
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "msec"    # I

    .prologue
    .line 1322
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1331
    :goto_0
    return-void

    .line 1326
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    int-to-long v2, p1

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->seek(J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1327
    :catch_0
    move-exception v0

    .line 1328
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seekTo() RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public seekTo(II)V
    .locals 4
    .param p1, "msec"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 1334
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1360
    :goto_0
    return-void

    .line 1337
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v1

    if-gtz v1, :cond_1

    .line 1338
    const-string v1, "VideoServiceUtil"

    const-string v2, "seekTo() - visual Seek - live streaming. don\'t seek."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1342
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1343
    const-string v1, "VideoServiceUtil"

    const-string v2, "seekTo() - visual Seek - streaming/download tab. call seek()."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    int-to-long v2, p1

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->seek(J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1347
    :catch_0
    move-exception v0

    .line 1348
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1355
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->realSeek(II)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1356
    :catch_1
    move-exception v0

    .line 1357
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seekTo(int,int) RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMasterClock(JJJJJ)V
    .locals 13
    .param p1, "currentTime"    # J
    .param p3, "clockDelta"    # J
    .param p5, "videoTime"    # J
    .param p7, "currentSecMediaClock"    # J
    .param p9, "currentAudioTimestamp"    # J

    .prologue
    .line 2026
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-wide v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-interface/range {v1 .. v11}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->sendMasterClock(JJJJJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2030
    :goto_0
    return-void

    .line 2027
    :catch_0
    move-exception v0

    .line 2028
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMasterClock() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 897
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 900
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 901
    return-void
.end method

.method public setAudioChannel(I)V
    .locals 4
    .param p1, "audioChannel"    # I

    .prologue
    .line 2018
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setAudioChannel(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2022
    :goto_0
    return-void

    .line 2019
    :catch_0
    move-exception v0

    .line 2020
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLeftAudioTrack() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setBucketID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 1022
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBucketId:I

    .line 1023
    return-void
.end method

.method public setCurIdx(I)V
    .locals 0
    .param p1, "curIdx"    # I

    .prologue
    .line 1018
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurIdx:I

    .line 1019
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 4

    .prologue
    .line 1469
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1470
    const-string v1, "VideoServiceUtil"

    const-string v2, "setInbandSubtitle() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    :goto_0
    return-void

    .line 1475
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setInbandSubtitle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1476
    :catch_0
    move-exception v0

    .line 1477
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInbandSubtitle() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setIsAudioOnlyClip(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 1934
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mAudioOnlyClip:Z

    .line 1935
    return-void
.end method

.method public setIsPauseEnable(Z)V
    .locals 0
    .param p1, "mIsPauseEnable"    # Z

    .prologue
    .line 1883
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mIsPauseEnable:Z

    .line 1884
    return-void
.end method

.method public setIsVideoOnlyClip(Z)V
    .locals 0
    .param p1, "set"    # Z

    .prologue
    .line 1926
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoOnlyClip:Z

    .line 1927
    return-void
.end method

.method public setLongSeekMode(I)V
    .locals 0
    .param p1, "LongSeekMode"    # I

    .prologue
    .line 1842
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1843
    return-void
.end method

.method public setMovieVolume(F)V
    .locals 4
    .param p1, "volume"    # F

    .prologue
    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1410
    :goto_0
    return-void

    .line 1404
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setVolume(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1405
    :catch_0
    move-exception v0

    .line 1406
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1407
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1408
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - NullPointerException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setOnNotificationListener(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    .prologue
    .line 1666
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    .line 1667
    return-void
.end method

.method public setOnSvcNotificationListener(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;

    .prologue
    .line 1674
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;

    .line 1675
    return-void
.end method

.method public setPlayerState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 1838
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayerState:I

    .line 1839
    return-void
.end method

.method public setPlayingBeforePalm(Z)V
    .locals 0
    .param p1, "playingBeforePalm"    # Z

    .prologue
    .line 1857
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    .line 1858
    return-void
.end method

.method public setPlayingFileInfo(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 222
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    .line 224
    if-eqz p1, :cond_5

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 226
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurVideoDbId:J

    .line 228
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurVideoDbId:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurVideoDbId:J

    .line 231
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    .line 233
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 234
    :cond_1
    const-string v3, "VideoServiceUtil"

    const-string v4, "setPlayingFileInfo. path is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v2, "file://"

    .line 237
    .local v2, "preFix":Ljava/lang/String;
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 238
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 239
    .local v0, "len":I
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 245
    .end local v0    # "len":I
    .end local v2    # "preFix":Ljava/lang/String;
    :cond_2
    :goto_0
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    .line 246
    const-string v3, "VideoServiceUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPlayingFileInfo. mCurPlayingUri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCurPlayingPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    .end local v1    # "path":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 241
    .restart local v1    # "path":Ljava/lang/String;
    .restart local v2    # "preFix":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 249
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "preFix":Ljava/lang/String;
    :cond_5
    const-string v3, "VideoServiceUtil"

    const-string v4, "setPlayingFileInfo() - uri is null. cannot play! -> finish!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    if-eqz v3, :cond_3

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v5, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto :goto_1
.end method

.method public setResumePosition(J)V
    .locals 1
    .param p1, "mResumePosition"    # J

    .prologue
    .line 1903
    iput-wide p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    .line 1904
    return-void
.end method

.method public setService(Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)V
    .locals 1
    .param p1, "service"    # Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v0, :cond_0

    .line 1614
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 1616
    :cond_0
    return-void
.end method

.method public setServiceContext()V
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    sput-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    .line 1588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    sput-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 1589
    sput-object p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 1590
    return-void
.end method

.method public setSubtitleLanguageIndex(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 1525
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1526
    const-string v1, "VideoServiceUtil"

    const-string v2, "setSubtitleLanguageIndex() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    :goto_0
    return-void

    .line 1531
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setSubtitleLanguage(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1532
    :catch_0
    move-exception v0

    .line 1533
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitle() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSubtitleSyncTime(I)V
    .locals 4
    .param p1, "synctime"    # I

    .prologue
    .line 1511
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1512
    const-string v1, "VideoServiceUtil"

    const-string v2, "setSubtitleSyncTime() - service is not ready."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    :goto_0
    return-void

    .line 1517
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1, p1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setSubtitleSyncTime(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1518
    :catch_0
    move-exception v0

    .line 1519
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleSyncTime() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setTitleName()V
    .locals 1

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setTitleName()V

    .line 1032
    :cond_0
    return-void
.end method

.method public setTrackInfoUtil(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V
    .locals 0
    .param p1, "trackInfoUtil"    # Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .prologue
    .line 1917
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1918
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1949
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setVideoCrop(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1954
    :cond_0
    :goto_0
    return-void

    .line 1950
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 1107
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v0, :cond_1

    .line 1134
    :cond_0
    :goto_0
    return-void

    .line 1112
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1114
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1115
    :cond_2
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start E. playing mPLAYERSTATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayerState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1119
    :cond_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1121
    const-string v0, "VideoServiceUtil"

    const-string v1, "MV_SERVER start() do not call stopPlayingChecker()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 1127
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayS()Z

    goto :goto_0

    .line 1123
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    goto :goto_1

    .line 1128
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->playS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_0
.end method

.method public startNextPrevPlayback()V
    .locals 4

    .prologue
    .line 312
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 314
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setKeepAudioFocus(Z)V

    .line 324
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->setKeepAudioFocus(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :cond_1
    :goto_0
    return-void

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 331
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException occured startNextPrevPlayback :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startPlayS()Z
    .locals 4

    .prologue
    .line 1138
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->startPlay()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1143
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1139
    :catch_0
    move-exception v0

    .line 1140
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startPlayback()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 269
    const-string v4, "VideoServiceUtil"

    const-string v5, "startPlayback() start"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v4, :cond_0

    .line 280
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->stop()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 289
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 290
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 293
    const-string v3, "VideoServiceUtil"

    const-string v4, "startPlayback() - file path to play is not valid."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v3, 0x0

    .line 308
    .end local v1    # "file":Ljava/io/File;
    :goto_1
    return v3

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 283
    const-string v4, "VideoServiceUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RemoteException occured  1 :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayingChecker()V

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTitleName()V

    .line 302
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 305
    .local v2, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 306
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1
.end method

.method public startPlayingChecker()V
    .locals 4

    .prologue
    .line 908
    const-string v0, "VideoServiceUtil"

    const-string v1, "startPlayingChecker() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestLayout()V

    .line 916
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 917
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 918
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 1181
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1183
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v0, :cond_1

    .line 1193
    :cond_0
    :goto_0
    return-void

    .line 1186
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1188
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1189
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mResumePosition:J

    .line 1190
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto :goto_0
.end method

.method public stopBufferingChecker()V
    .locals 2

    .prologue
    .line 1726
    const-string v0, "VideoServiceUtil"

    const-string v1, "stopPlayingChecker() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1729
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideStateView()V

    .line 1731
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 1732
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1734
    :cond_1
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 1735
    return-void
.end method

.method public stopPlayingChecker()V
    .locals 2

    .prologue
    .line 921
    const-string v0, "VideoServiceUtil"

    const-string v1, "stopPlayingChecker() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideStateView()V

    .line 929
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 930
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 931
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 932
    return-void
.end method

.method public stopS()Z
    .locals 4

    .prologue
    .line 1197
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1202
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public top()Z
    .locals 1

    .prologue
    .line 409
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;->TOP:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public unMuteDevice()V
    .locals 4

    .prologue
    .line 1413
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-nez v1, :cond_0

    .line 1424
    :goto_0
    return-void

    .line 1418
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->unMuteDevice()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1419
    :catch_0
    move-exception v0

    .line 1420
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1421
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1422
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMovieVolume() - NullPointerException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unbindFromService()V
    .locals 4

    .prologue
    .line 1628
    sget-object v1, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sConnectionMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;

    .line 1630
    .local v0, "sb":Lcom/sec/android/app/mv/player/util/VideoServiceUtil$ServiceBinder;
    if-nez v0, :cond_0

    .line 1637
    :goto_0
    return-void

    .line 1634
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1635
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 1636
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unbindFromService sConnectionMap.isEmpty = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateProgress()V
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setControllerProgress()V

    .line 597
    return-void
.end method
