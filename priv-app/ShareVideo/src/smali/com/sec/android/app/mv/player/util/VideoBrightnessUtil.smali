.class public Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
.super Ljava/lang/Object;
.source "VideoBrightnessUtil.java"


# static fields
.field private static final AUTO_BRIGHTNESS_FLAG:I = 0x0

.field private static final AUTO_BRIGHTNESS_LEVEL:I = 0x1

.field private static final AUTO_BRIGHT_DETAIL_LEVEL_STEP_VAL:I = 0x14

.field private static final MANUAL_BRIGHTNESS_LEVEL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VideoBrightnessUtil"

.field private static mAutoBrightSupported:I


# instance fields
.field private mAutoBrightDetailLevel:I

.field private mBrightnessRange:I

.field private mContext:Landroid/content/Context;

.field private mHalfLevel:I

.field private mMaxAutoBrightness:I

.field private mMaxBrightness:I

.field private mMinBrightness:I

.field private mPrevBrightnessVal:[I

.field private mSystemAutoBrightness:Z

.field private mSystemBrightnessLevel:I

.field private mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    return-void
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/content/Context;)V
    .locals 3
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 31
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 33
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 35
    const/16 v0, 0xeb

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 37
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 51
    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getMaximumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getMinimumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxBrightness:I

    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 55
    sput v2, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    .line 56
    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 57
    return-void

    .line 39
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 171
    const/4 v0, -0x1

    .line 172
    .local v0, "autobright":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightnessSupported()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 174
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness_mode"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 180
    :cond_0
    :goto_0
    if-ne v0, v3, :cond_1

    :goto_1
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 183
    :try_start_1
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_2

    .line 184
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 190
    :goto_2
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSystemBrightenessLevel. mSystemBrightnessLevel = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 180
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 186
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 187
    :catch_1
    move-exception v2

    .line 188
    .local v2, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method private getWindowBrightnessValue(I)F
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int/2addr v0, p1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxBrightness:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static resetWindowBrightness(Landroid/view/Window;)V
    .locals 2
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 89
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 90
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 92
    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 93
    return-void
.end method

.method private setWindowBrightness()V
    .locals 6

    .prologue
    .line 124
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 129
    .local v1, "config":Landroid/content/res/Configuration;
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowBrightness. mSystemBrightnessLevel = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v3, :cond_2

    iget v3, v1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_2

    .line 132
    const-string v3, "VideoBrightnessUtil"

    const-string v4, "Folder closed!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getWindowBrightnessValue(I)F

    move-result v0

    .line 136
    .local v0, "brightVal":F
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 137
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 140
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_0

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness_mode"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 143
    .end local v0    # "brightVal":F
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_3

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 146
    :cond_3
    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getWindowBrightnessValue(I)F

    move-result v0

    .line 148
    .restart local v0    # "brightVal":F
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 149
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 152
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowBrightness. brightVal = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public brightnessValueLocale()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getAutoBrightDetailTextString()Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "brightnessLocale":Ljava/lang/String;
    const/4 v1, 0x0

    .line 285
    .local v1, "showBrightness":Ljava/lang/String;
    const-string v2, "+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 302
    :goto_0
    return-object v1

    .line 289
    :cond_0
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 291
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 293
    :cond_1
    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 295
    const-string v2, "%d"

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "0"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 299
    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public getAutoBrightDetailLevelInSystemValue()I
    .locals 3

    .prologue
    .line 326
    const/4 v0, 0x0

    .line 327
    .local v0, "retVal":I
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    if-nez v1, :cond_0

    .line 328
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    .line 334
    :goto_0
    return v0

    .line 329
    :cond_0
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    if-lez v1, :cond_1

    .line 330
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    mul-int/lit8 v2, v2, 0x14

    add-int v0, v1, v2

    goto :goto_0

    .line 332
    :cond_1
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    mul-int/lit8 v1, v1, 0x14

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    add-int v0, v1, v2

    goto :goto_0
.end method

.method public getAutoBrightDetailTextString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 307
    .local v0, "text":Ljava/lang/StringBuilder;
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getMaxBrightness()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    .line 310
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    if-le v1, v2, :cond_2

    .line 311
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x14

    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    .line 316
    :goto_0
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    if-lez v1, :cond_0

    .line 317
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 319
    :cond_0
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 322
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 313
    :cond_2
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mHalfLevel:I

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x14

    mul-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    goto :goto_0
.end method

.method public getBrightnessRange()I
    .locals 3

    .prologue
    .line 226
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    .line 227
    const-string v0, "VideoBrightnessUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBrightnessRange = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 231
    :goto_0
    return v0

    .line 230
    :cond_0
    const-string v0, "VideoBrightnessUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBrightnessRange = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    goto :goto_0
.end method

.method public getDefaultScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 266
    const/16 v1, 0x6e

    .line 267
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 269
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    move-result v1

    .line 272
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getMaxBrightness()I
    .locals 1

    .prologue
    .line 96
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    .line 97
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 99
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    goto :goto_0
.end method

.method public getMaximumScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 246
    const/16 v1, 0xff

    .line 247
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 249
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v1

    .line 252
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getMinimumScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 256
    const/16 v1, 0x28

    .line 257
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 259
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    move-result v1

    .line 262
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getSystemBrightnessLevel()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 167
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v0
.end method

.method public isAutoBrightness()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 162
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    return v0
.end method

.method public isAutoBrightnessSupported()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 75
    sget v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isLightSensorAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sput v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    .line 79
    :cond_0
    sget v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    if-ne v0, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 76
    goto :goto_0

    :cond_2
    move v1, v2

    .line 79
    goto :goto_1
.end method

.method public isLightSensorAvailable(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "available":Z
    const-string v6, "sensor"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    .line 62
    .local v3, "sensorMgr":Landroid/hardware/SensorManager;
    const/4 v6, -0x1

    invoke-virtual {v3, v6}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    .line 63
    .local v2, "sensorList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 65
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_1

    .line 66
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/Sensor;

    invoke-virtual {v6}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    .line 67
    .local v4, "sensorType":I
    const/4 v6, 0x5

    if-ne v4, v6, :cond_0

    .line 68
    const/4 v0, 0x1

    .line 65
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v4    # "sensorType":I
    :cond_1
    return v0
.end method

.method public resetWindowBrightness()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 86
    :cond_0
    return-void
.end method

.method public revertBrightness(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 355
    const-string v0, "VideoBrightnessUtil"

    const-string v1, "revertBrightness E:"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    if-nez p1, :cond_1

    .line 357
    const-string v0, "VideoBrightnessUtil"

    const-string v1, "revertBrightness Context is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_2

    .line 362
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_brightness_detail"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v2, v2, v4

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 364
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v2, v2, v6

    iget v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v5

    if-ne v0, v4, :cond_3

    .line 367
    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 368
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setBrightness(I)I

    goto :goto_0

    .line 372
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setBrightness(I)I

    goto :goto_0
.end method

.method public savePrevSystemBrightnessValuesInLocal(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 338
    if-nez p1, :cond_0

    .line 339
    const-string v1, "VideoBrightnessUtil"

    const-string v2, "savePrevSystemBrightnessValuesInLocal Context is NULL!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :goto_0
    return-void

    .line 343
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    aput v1, v3, v2

    .line 345
    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_1

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    .line 348
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v3, v4

    aput v3, v1, v2
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .end local v0    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    move v1, v2

    .line 343
    goto :goto_1
.end method

.method public saveSystemBrightnessLevelValueinLocal(I)I
    .locals 2
    .param p1, "val"    # I

    .prologue
    .line 236
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v0

    .line 238
    .local v0, "maxBrightness":I
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-gez v1, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 239
    :cond_0
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-le v1, v0, :cond_1

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 240
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setWindowBrightness()V

    .line 242
    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v1
.end method

.method public setAutoBrightness(Z)V
    .locals 3
    .param p1, "setAuto"    # Z

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 279
    return-void

    .line 277
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBrightness(I)I
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 107
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-gez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 110
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_2

    .line 111
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    if-le v0, v1, :cond_1

    .line 112
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 118
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setWindowBrightness()V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setSystemBrightnessLevel()V

    .line 120
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v0

    .line 114
    :cond_2
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v1, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    if-le v0, v1, :cond_1

    .line 115
    iget v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mBrightnessRange:I

    iput v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    goto :goto_0
.end method

.method public setSystemBrightnessLevel()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setSystemBrightnessLevel(Landroid/content/Context;)V

    .line 196
    :cond_0
    return-void
.end method

.method public setSystemBrightnessLevel(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 199
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v4, :cond_0

    .line 201
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "auto_brightness_detail"

    iget v6, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    return-void

    .line 202
    :catch_0
    move-exception v1

    .line 203
    .local v1, "doe":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v1    # "doe":Ljava/lang/SecurityException;
    :cond_0
    :try_start_1
    const-string v4, "power"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 208
    .local v2, "power":Landroid/os/PowerManager;
    iget v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int v0, v4, v5

    .line 209
    .local v0, "brightness":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 211
    .local v3, "resolver":Landroid/content/ContentResolver;
    if-eqz v2, :cond_1

    .line 212
    invoke-virtual {v2, v0}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 214
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v4, :cond_2

    .line 215
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness_mode"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 217
    :cond_2
    const-string v4, "screen_brightness"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 221
    .end local v0    # "brightness":I
    .end local v2    # "power":Landroid/os/PowerManager;
    .end local v3    # "resolver":Landroid/content/ContentResolver;
    :goto_1
    const-string v4, "VideoBrightnessUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setSystemBrightnessLevel : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 218
    :catch_1
    move-exception v1

    .line 219
    .restart local v1    # "doe":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1
.end method
