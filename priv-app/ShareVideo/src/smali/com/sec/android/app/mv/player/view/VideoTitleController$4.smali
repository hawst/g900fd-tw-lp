.class Lcom/sec/android/app/mv/player/view/VideoTitleController$4;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0

    .prologue
    .line 603
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    const/16 v2, 0xbb8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    move-result v1

    if-nez v1, :cond_0

    .line 609
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$200(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$300(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeSame()V

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 613
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeSVMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupSVVolbar()V

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeMVBarPopup()V

    goto :goto_0

    .line 617
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$200(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 619
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget v1, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 621
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupVolbar(Z)V

    .line 622
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 623
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setFocusableInTouchMode(Z)V

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->requestFocus()Z

    .line 628
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_0

    .line 626
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupVolbar(Z)V

    goto :goto_1
.end method
