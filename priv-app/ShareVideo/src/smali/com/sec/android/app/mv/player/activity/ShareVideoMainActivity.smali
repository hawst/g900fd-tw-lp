.class public Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
.super Lcom/samsung/groupcast/ExternalActivity;
.source "ShareVideoMainActivity.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
    }
.end annotation


# static fields
.field private static final CLIENT_HANDLER:I = 0x3e9

.field private static final ERROR:I = -0x1

.field private static final MAX_PARICIPANTS:I = 0x12f

.field private static final NO_ENTERING_HANDLER:I = 0x3eb

.field private static final PLAYLIST_TYPE_PICKER:I = 0x0

.field private static final SERVER_HANDLER:I = 0x3e8

.field private static final SERVER_IP:I = 0x12c

.field private static final SERVER_TITLE:I = 0x12d

.field private static final TAG:Ljava/lang/String; = "ShareVideoMainActivity"

.field private static final TIMEOUT:I = 0x3ea


# instance fields
.field private deviceName:Ljava/lang/String;

.field private mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mFromLocalVideoPlayer:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsServiceBound:Z

.field private mNetInterface:I

.field private mServerIP:Ljava/lang/String;

.field private mServerTitle:Ljava/lang/String;

.field private mTimeout:Z

.field private mUri:Landroid/net/Uri;

.field private mloadingDialog:Landroid/app/ProgressDialog;

.field private thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/samsung/groupcast/ExternalActivity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 46
    iput v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->deviceName:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerTitle:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mUri:Landroid/net/Uri;

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mFromLocalVideoPlayer:Z

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mTimeout:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mIsServiceBound:Z

    .line 80
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mContext:Landroid/content/Context;

    .line 82
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    .line 84
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mErrorPopup:Landroid/app/AlertDialog;

    .line 164
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$1;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    .line 285
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$2;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mTimeout:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getHandlerMsg(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mFromLocalVideoPlayer:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callMoviePlayerAsServer()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callPlaylist(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->callMoviePlayerAsClient()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mIsServiceBound:Z

    return p1
.end method

.method private callMoviePlayerAsClient()V
    .locals 3

    .prologue
    .line 250
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "callMoviePlayerOfServer E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "ServerTitle"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    const-string v1, "ServerIP"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    const-string v1, "rtsp://multivision"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 257
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 258
    const-string v1, "DeviceName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 261
    return-void
.end method

.method private callMoviePlayerAsServer()V
    .locals 3

    .prologue
    .line 264
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "callMoviePlayerOfClient E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 267
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "uri"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    const-string v1, "DeviceName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    const-string v1, "From"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 271
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 272
    return-void
.end method

.method private callPlaylist(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 275
    const-string v1, "ShareVideoMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callPlaylist E. type :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 278
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "ListType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 279
    const-string v1, "DeviceName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 282
    const/high16 v1, 0x10a0000

    const v2, 0x10a0001

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->overridePendingTransition(II)V

    .line 283
    return-void
.end method

.method private createErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 536
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "createErrorDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 540
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$4;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$3;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 553
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mErrorPopup:Landroid/app/AlertDialog;

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 555
    return-void
.end method

.method private getHandlerMsg(I)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 383
    const-string v0, "None"

    .line 385
    .local v0, "msgString":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 418
    :goto_0
    return-object v0

    .line 387
    :sswitch_0
    const-string v0, "MAX_PARICIPANTS"

    .line 388
    goto :goto_0

    .line 391
    :sswitch_1
    const-string v0, "SERVER_TITLE"

    .line 392
    goto :goto_0

    .line 395
    :sswitch_2
    const-string v0, "SERVER_IP"

    .line 396
    goto :goto_0

    .line 399
    :sswitch_3
    const-string v0, "SERVER_HANDLER"

    .line 400
    goto :goto_0

    .line 403
    :sswitch_4
    const-string v0, "CLIENT_HANDLER"

    .line 404
    goto :goto_0

    .line 407
    :sswitch_5
    const-string v0, "TIMEOUT"

    .line 408
    goto :goto_0

    .line 411
    :sswitch_6
    const-string v0, "NO_ENTERING_HANDLER"

    .line 412
    goto :goto_0

    .line 385
    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_2
        0x12d -> :sswitch_1
        0x12f -> :sswitch_0
        0x3e8 -> :sswitch_3
        0x3e9 -> :sswitch_4
        0x3ea -> :sswitch_5
        0x3eb -> :sswitch_6
    .end sparse-switch
.end method

.method private isShareVideoSupported()Z
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x1

    .line 569
    .local v0, "ret":Z
    return v0
.end method

.method private startService()V
    .locals 3

    .prologue
    .line 358
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "startService()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 361
    return-void
.end method


# virtual methods
.method public bindChordService()V
    .locals 3

    .prologue
    .line 339
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "bindChordService()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-nez v1, :cond_0

    .line 342
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 345
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mContext:Landroid/content/Context;

    .line 380
    return-void
.end method

.method public onConnectivityChanged()V
    .locals 2

    .prologue
    .line 502
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onConnectivityChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 89
    const-string v3, "ShareVideoMainActivity"

    const-string v4, "onCreate() - start"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    iput-object p0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mContext:Landroid/content/Context;

    .line 95
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->requestWindowFeature(I)Z

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x480

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->isShareVideoSupported()Z

    move-result v3

    if-nez v3, :cond_0

    .line 99
    const v3, 0x7f0a0084

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "str1":Ljava/lang/String;
    const v3, 0x7f0a0056

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "str2":Ljava/lang/String;
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->createErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .end local v1    # "str1":Ljava/lang/String;
    .end local v2    # "str2":Ljava/lang/String;
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->startService()V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->bindChordService()V

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "DeviceName"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->deviceName:Ljava/lang/String;

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "netIF"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    .line 111
    const-string v3, "ShareVideoMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate. mNetInterface : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "From"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mFromLocalVideoPlayer:Z

    .line 114
    const-string v3, "ShareVideoMainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate. mFromLocalVideoPlayer : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mFromLocalVideoPlayer:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mUri:Landroid/net/Uri;

    .line 118
    new-instance v0, Landroid/text/SpannableString;

    const v3, 0x7f0a0070

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 120
    .local v0, "loadString":Landroid/text/SpannableString;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    if-nez v3, :cond_1

    .line 121
    new-instance v3, Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 125
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070024

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    invoke-virtual {v0, v3, v6, v4, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 130
    :cond_1
    new-instance v3, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->thread:Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity$ListenChordServiceThread;->start()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 234
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onDestroy()V

    .line 235
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onDestroy() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 239
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mloadingDialog:Landroid/app/ProgressDialog;

    .line 242
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mTimeout:Z

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->clear()V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 245
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    .line 246
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 247
    return-void
.end method

.method public onFileCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "exchangeId"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;

    .prologue
    .line 520
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFileCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    return-void
.end method

.method public onFileProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "bSend"    # Z
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "progress"    # I
    .param p5, "exchangeId"    # Ljava/lang/String;

    .prologue
    .line 526
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFileProgress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    return-void
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 532
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFileReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    return-void
.end method

.method public onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "index"    # I

    .prologue
    .line 479
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFilesCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-void
.end method

.method public onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;

    .prologue
    .line 483
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFilesFinish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    return-void
.end method

.method public onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 2
    .param p1, "bSend"    # Z
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "progress"    # I
    .param p5, "trId"    # Ljava/lang/String;
    .param p6, "index"    # I
    .param p7, "bMulti"    # Z

    .prologue
    .line 474
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onFilesProgress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    return-void
.end method

.method public onFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 6
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;
    .param p5, "totalCount"    # I
    .param p6, "bMulti"    # Z

    .prologue
    .line 465
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "onFilesWillReceive"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 468
    .local v0, "mChannel":Lcom/samsung/android/sdk/chord/SchordChannel;
    const/16 v2, 0x7530

    const/4 v3, 0x2

    const-wide/32 v4, 0x4b000

    move-object v1, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->acceptFile(Ljava/lang/String;IIJ)V

    .line 469
    const-string v1, "ShareVideoMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShareVideoMain onFilesWillReceive"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    return-void
.end method

.method public onNetworkDisconnected()V
    .locals 2

    .prologue
    .line 493
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onNetworkDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    return-void
.end method

.method public onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "bJoined"    # Z

    .prologue
    .line 488
    const-string v0, "ShareVideoMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNodeEvent node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", channel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bJoined : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 219
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onPause() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onPause()V

    .line 221
    return-void
.end method

.method public onReceiveMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x3e9

    const/16 v6, 0x3e8

    const/4 v5, 0x1

    .line 423
    const/4 v1, 0x0

    .line 424
    .local v1, "unit":[Ljava/lang/String;
    const/4 v0, -0x1

    .line 426
    .local v0, "type":I
    const-string v2, "%"

    invoke-virtual {p3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 427
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 428
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceiveMessage. message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->getHandlerMsg(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    packed-switch v0, :pswitch_data_0

    .line 458
    :pswitch_0
    const-string v2, "ShareVideoMainActivity"

    const-string v3, "There\'s no case"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 432
    :pswitch_1
    aget-object v2, v1, v5

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 433
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3eb

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 439
    :pswitch_2
    aget-object v2, v1, v5

    if-eqz v2, :cond_1

    .line 440
    aget-object v2, v1, v5

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerTitle:Ljava/lang/String;

    .line 443
    :cond_1
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceiveMessage. Server mServerTitle is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 447
    :pswitch_3
    aget-object v2, v1, v5

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    .line 448
    aget-object v2, v1, v5

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 450
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 451
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 454
    :cond_2
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceiveMessage. Server IP is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mServerIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 430
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 224
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onResume() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onResume()V

    .line 226
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 229
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onStop() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onStop()V

    .line 231
    return-void
.end method

.method public onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;

    .prologue
    .line 507
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onUpdateNodeInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    return-void
.end method

.method public onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "portNum"    # I

    .prologue
    .line 497
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "onUpdateNodeInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    return-void
.end method

.method public sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p2, "strFilePath"    # Ljava/lang/String;
    .param p3, "toNode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 513
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "sendFile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/4 v0, 0x0

    return-object v0
.end method

.method public startChord()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 311
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startChord E. mNetInterface : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getAvailableInterfaceTypes()Ljava/util/List;

    move-result-object v1

    .line 315
    .local v1, "interface_list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 316
    const-string v2, "ShareVideoMainActivity"

    const-string v3, "startChord. interface_list is empty"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const v2, 0x7f0a002e

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 336
    :goto_0
    return-void

    .line 321
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 322
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    .line 325
    :cond_1
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startChord. mNetInterface : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mNetInterface:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->start(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/chord/InvalidInterfaceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to start -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 331
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 332
    .local v0, "e":Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
    const-string v2, "ShareVideoMainActivity"

    const-string v3, "There is no such a connection."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 333
    .end local v0    # "e":Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
    :catch_2
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ShareVideoMainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to start -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopChord()V
    .locals 3

    .prologue
    .line 370
    const-string v0, "ShareVideoMainActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopChord mChordService =>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->leaveChannel()V

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->stop()V

    .line 376
    :cond_0
    return-void
.end method

.method protected stopChordService()V
    .locals 0

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopChord()V

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->unbindChordService()V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopService()V

    .line 216
    return-void
.end method

.method public stopService()V
    .locals 3

    .prologue
    .line 364
    const-string v1, "ShareVideoMainActivity"

    const-string v2, "stopService()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 366
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->stopService(Landroid/content/Intent;)Z

    .line 367
    return-void
.end method

.method public unbindChordService()V
    .locals 2

    .prologue
    .line 348
    const-string v0, "ShareVideoMainActivity"

    const-string v1, "unbindChordService()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mIsServiceBound:Z

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 354
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideoMainActivity;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 355
    return-void
.end method
