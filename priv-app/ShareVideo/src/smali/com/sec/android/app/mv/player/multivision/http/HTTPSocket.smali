.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
.super Ljava/lang/Object;
.source "HTTPSocket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;
    }
.end annotation


# instance fields
.field private mOutStream:Ljava/io/OutputStream;

.field private mSockIn:Ljava/io/InputStream;

.field private mSockOut:Ljava/io/BufferedOutputStream;

.field private mSocket:Ljava/net/Socket;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/net/Socket;)V
    .locals 2
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    .line 117
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->setSocket(Ljava/net/Socket;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->open()Z

    .line 74
    return-void
.end method

.method private getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    return-object v0
.end method

.method private post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z
    .locals 21
    .param p1, "httpRes"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "contentOffset"    # J
    .param p5, "contentLength"    # J
    .param p7, "isOnlyHeader"    # Z

    .prologue
    .line 248
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 249
    :cond_0
    const/16 v16, 0x0

    .line 325
    :goto_0
    return v16

    .line 250
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setDate(Ljava/util/Calendar;)V

    .line 251
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    .line 253
    .local v9, "out":Ljava/io/OutputStream;
    const-wide/16 v12, 0x0

    .line 255
    .local v12, "readCnt":J
    :try_start_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentLength(J)V

    .line 256
    const-string v16, "\r\n"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 258
    .local v5, "crlf":[B
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getHeader()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/io/OutputStream;->write([B)V

    .line 259
    invoke-virtual {v9, v5}, Ljava/io/OutputStream;->write([B)V

    .line 261
    const/16 v16, 0x1

    move/from16 v0, p7

    move/from16 v1, v16

    if-ne v0, v1, :cond_2

    .line 262
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    const/16 v16, 0x1

    .line 318
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 320
    :catch_0
    move-exception v6

    .line 321
    .local v6, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "post -in.close - IOException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 266
    .end local v6    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->isChunked()Z

    move-result v8

    .line 275
    .local v8, "isChunkedResponse":Z
    const-wide/16 v16, 0x0

    cmp-long v16, p3, v16

    if-lez v16, :cond_3

    .line 276
    invoke-virtual/range {p2 .. p4}, Ljava/io/InputStream;->skip(J)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v16

    cmp-long v16, v16, p3

    if-eqz v16, :cond_3

    .line 277
    const/16 v16, 0x0

    .line 318
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 320
    :catch_1
    move-exception v6

    .line 321
    .restart local v6    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "post -in.close - IOException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 280
    .end local v6    # "e":Ljava/io/IOException;
    :cond_3
    const v4, 0x4b000

    .line 281
    .local v4, "chunkSize":I
    :try_start_4
    new-array v10, v4, [B

    .line 282
    .local v10, "readBuf":[B
    const-wide/16 v12, 0x0

    .line 283
    int-to-long v0, v4

    move-wide/from16 v16, v0

    cmp-long v16, v16, p5

    if-gez v16, :cond_6

    int-to-long v14, v4

    .line 284
    .local v14, "readSize":J
    :goto_1
    const/16 v16, 0x0

    long-to-int v0, v14

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v10, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    .line 285
    .local v11, "readLen":I
    :goto_2
    if-lez v11, :cond_8

    cmp-long v16, v12, p5

    if-gez v16, :cond_8

    .line 286
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v8, v0, :cond_4

    .line 288
    int-to-long v0, v11

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/io/OutputStream;->write([B)V

    .line 289
    invoke-virtual {v9, v5}, Ljava/io/OutputStream;->write([B)V

    .line 291
    :cond_4
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v9, v10, v0, v11}, Ljava/io/OutputStream;->write([BII)V

    .line 292
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V

    .line 294
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v8, v0, :cond_5

    .line 295
    invoke-virtual {v9, v5}, Ljava/io/OutputStream;->write([B)V

    .line 296
    :cond_5
    int-to-long v0, v11

    move-wide/from16 v16, v0

    add-long v12, v12, v16

    .line 297
    int-to-long v0, v4

    move-wide/from16 v16, v0

    sub-long v18, p5, v12

    cmp-long v16, v16, v18

    if-gez v16, :cond_7

    int-to-long v14, v4

    .line 299
    :goto_3
    const/16 v16, 0x0

    long-to-int v0, v14

    move/from16 v17, v0

    move-object/from16 v0, p2

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v10, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v11

    goto :goto_2

    .end local v11    # "readLen":I
    .end local v14    # "readSize":J
    :cond_6
    move-wide/from16 v14, p5

    .line 283
    goto :goto_1

    .line 297
    .restart local v11    # "readLen":I
    .restart local v14    # "readSize":J
    :cond_7
    sub-long v14, p5, v12

    goto :goto_3

    .line 301
    :cond_8
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v8, v0, :cond_9

    .line 302
    const-string v16, "0"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/io/OutputStream;->write([B)V

    .line 303
    invoke-virtual {v9, v5}, Ljava/io/OutputStream;->write([B)V

    .line 305
    :cond_9
    invoke-virtual {v9}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 318
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 325
    .end local v4    # "chunkSize":I
    .end local v5    # "crlf":[B
    .end local v8    # "isChunkedResponse":Z
    .end local v10    # "readBuf":[B
    .end local v11    # "readLen":I
    .end local v14    # "readSize":J
    :goto_4
    const/16 v16, 0x1

    goto/16 :goto_0

    .line 320
    .restart local v4    # "chunkSize":I
    .restart local v5    # "crlf":[B
    .restart local v8    # "isChunkedResponse":Z
    .restart local v10    # "readBuf":[B
    .restart local v11    # "readLen":I
    .restart local v14    # "readSize":J
    :catch_2
    move-exception v6

    .line 321
    .restart local v6    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "post -in.close - IOException"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 306
    .end local v4    # "chunkSize":I
    .end local v5    # "crlf":[B
    .end local v6    # "e":Ljava/io/IOException;
    .end local v8    # "isChunkedResponse":Z
    .end local v10    # "readBuf":[B
    .end local v11    # "readLen":I
    .end local v14    # "readSize":J
    :catch_3
    move-exception v6

    .line 307
    .local v6, "e":Ljava/lang/Exception;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "post  - Exception"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 309
    :try_start_7
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 313
    :goto_5
    :try_start_8
    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_a

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    const-string v17, "broken pipe"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v16

    if-eqz v16, :cond_a

    .line 314
    const/16 v16, 0x0

    .line 318
    :try_start_9
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 320
    :catch_4
    move-exception v6

    .line 321
    .local v6, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "post -in.close - IOException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 310
    .local v6, "e":Ljava/lang/Exception;
    :catch_5
    move-exception v7

    .line 311
    .local v7, "e1":Ljava/io/IOException;
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "post - out.close - IOException"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_5

    .line 317
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v16

    .line 318
    :try_start_b
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 322
    :goto_6
    throw v16

    .line 318
    .restart local v6    # "e":Ljava/lang/Exception;
    :cond_a
    :try_start_c
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    goto :goto_4

    .line 320
    :catch_6
    move-exception v6

    .line 321
    .local v6, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string v17, "post -in.close - IOException"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 320
    .end local v6    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v6

    .line 321
    .restart local v6    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "post -in.close - IOException"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6
.end method

.method private setSocket(Ljava/net/Socket;)V
    .locals 0
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    .line 91
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 213
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->setSocket(Ljava/net/Socket;)V

    .line 214
    const/4 v1, 0x1

    .line 240
    :goto_0
    return v1

    .line 217
    :cond_1
    const/4 v1, 0x1

    .line 220
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v2

    if-nez v2, :cond_2

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownOutput()V

    .line 222
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v2

    if-nez v2, :cond_3

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->shutdownInput()V

    .line 225
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    if-eqz v2, :cond_4

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 227
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    if-eqz v2, :cond_5

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 229
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    if-eqz v2, :cond_6

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 232
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->close()V

    .line 233
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->setSocket(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v3, "close Exception shutdownOutput : "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 237
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    return-object v0
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSocket:Ljava/net/Socket;

    return-object v0
.end method

.method public open()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v3

    .line 147
    .local v3, "sock":Ljava/net/Socket;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/net/Socket;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v4

    .line 150
    :cond_1
    :try_start_0
    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    .line 151
    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    .line 152
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    if-eqz v5, :cond_0

    .line 154
    new-instance v5, Ljava/io/BufferedOutputStream;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/net/Socket;->getSendBufferSize()I

    move-result v7

    invoke-direct {v5, v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    .line 158
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v5, :cond_0

    .line 208
    const/4 v4, 0x1

    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "open Exception fail to open socket "

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    if-eqz v3, :cond_2

    .line 166
    :try_start_1
    invoke-virtual {v3}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 172
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 173
    :try_start_2
    invoke-virtual {v3}, Ljava/net/Socket;->shutdownInput()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 179
    :cond_3
    :goto_2
    if-eqz v3, :cond_4

    .line 180
    :try_start_3
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 186
    :cond_4
    :goto_3
    :try_start_4
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    if-eqz v5, :cond_5

    .line 187
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockIn:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 193
    :cond_5
    :goto_4
    :try_start_5
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    if-eqz v5, :cond_6

    .line 194
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mOutStream:Ljava/io/OutputStream;

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 200
    :cond_6
    :goto_5
    :try_start_6
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    if-eqz v5, :cond_0

    .line 201
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mSockOut:Ljava/io/BufferedOutputStream;

    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    .line 202
    :catch_1
    move-exception v1

    .line 203
    .local v1, "e1":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "fail to close buffered outputstream - IOException "

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 167
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 168
    .local v2, "e2":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "  open - fail to shutdownOutput socket - IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 174
    .end local v2    # "e2":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 175
    .restart local v2    # "e2":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "  open - fail to shutdownInput socket - IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 181
    .end local v2    # "e2":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 182
    .restart local v1    # "e1":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "  open - fail to close socket - IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 188
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 189
    .restart local v1    # "e1":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "fail to close inputstream -IOException "

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 195
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 196
    .restart local v1    # "e1":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v6, "fail to close outputstream - IOException "

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;JJZ)Z
    .locals 18
    .param p1, "httpRes"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .param p2, "contentOffset"    # J
    .param p4, "contentLength"    # J
    .param p6, "isOnlyHeader"    # Z

    .prologue
    .line 373
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->hasContentInputStream()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 377
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v5

    .local v5, "is":Ljava/io/InputStream;
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move/from16 v10, p6

    .line 378
    invoke-direct/range {v3 .. v10}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z

    move-result v15

    .line 379
    .local v15, "result":Z
    if-eqz v5, :cond_0

    .line 381
    :try_start_0
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    move/from16 v16, v15

    .line 398
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v15    # "result":Z
    .local v16, "result":Z
    :goto_1
    return v16

    .line 382
    .end local v16    # "result":Z
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v15    # "result":Z
    :catch_0
    move-exception v2

    .line 383
    .local v2, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v4, "post - IOException "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 389
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v15    # "result":Z
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContent()Ljava/io/InputStream;

    move-result-object v9

    .local v9, "in":Ljava/io/InputStream;
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-wide/from16 v10, p2

    move-wide/from16 v12, p4

    move/from16 v14, p6

    .line 390
    invoke-direct/range {v7 .. v14}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->post(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;Ljava/io/InputStream;JJZ)Z

    move-result v15

    .line 392
    .restart local v15    # "result":Z
    if-eqz v9, :cond_2

    .line 393
    :try_start_1
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    move/from16 v16, v15

    .line 398
    .end local v15    # "result":Z
    .restart local v16    # "result":Z
    goto :goto_1

    .line 395
    .end local v16    # "result":Z
    .restart local v15    # "result":Z
    :catch_1
    move-exception v2

    .line 396
    .restart local v2    # "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->mTag:Ljava/lang/String;

    const-string v4, "post - IOException "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
