.class Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSettingsPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removePauseSet(Ljava/lang/Object;)V

    .line 345
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$202(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Z)Z

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$302(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$402(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$502(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 361
    :cond_3
    return-void

    .line 343
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSettingsPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_0
.end method
