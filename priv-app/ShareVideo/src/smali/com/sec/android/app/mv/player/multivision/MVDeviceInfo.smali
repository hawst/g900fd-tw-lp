.class public Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
.super Ljava/lang/Object;
.source "MVDeviceInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1fL


# instance fields
.field private mDeviceID:I

.field private mDeviceName:Ljava/lang/String;

.field private mDevicePos:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 11
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 13
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 16
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 18
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 19
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 11
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 13
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 22
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 24
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 25
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 13
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 28
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 30
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 31
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "pos"    # I

    .prologue
    const/4 v1, -0x1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 13
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 34
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 36
    iput p3, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 37
    return-void
.end method


# virtual methods
.method public getDeviceID()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDevicePos()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    return v0
.end method

.method public setDeviceID(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceID:I

    .line 53
    return-void
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setDevicePos(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->mDevicePos:I

    .line 61
    return-void
.end method
