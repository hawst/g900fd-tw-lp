.class public Lcom/sec/android/app/mv/player/util/VideoUtility;
.super Ljava/lang/Object;
.source "VideoUtility.java"


# instance fields
.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 7
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 10
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {v0, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 11
    new-instance v0, Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 12
    return-void
.end method


# virtual methods
.method public getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method public getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoUtility;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    return-object v0
.end method
