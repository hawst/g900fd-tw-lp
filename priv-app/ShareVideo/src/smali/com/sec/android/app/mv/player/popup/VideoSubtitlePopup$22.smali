.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1065
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x1

    .line 1067
    const/4 v1, 0x0

    .line 1068
    .local v1, "selectedFile":Ljava/io/File;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1069
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "selectedFile":Ljava/io/File;
    check-cast v1, Ljava/io/File;

    .line 1071
    .restart local v1    # "selectedFile":Ljava/io/File;
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1072
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1073
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "callSelectSubtitlePopup : onClick INBAND Subtitle"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    .line 1075
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setInbandSubtitle()V

    .line 1090
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v2

    if-nez v2, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a008d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1092
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 1093
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2800(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 1094
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a003f

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1102
    :goto_1
    return-void

    .line 1076
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->existFile(Ljava/lang/String;)Z
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2600(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1078
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleLang()V

    .line 1079
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2502(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Ljava/lang/String;)Ljava/lang/String;

    .line 1080
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 1081
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->selectOutbandSubtitle()Z

    .line 1082
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1085
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "callSelectSubtitlePopup() - iputFilePath has an exception."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1096
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z

    .line 1097
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1098
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V

    goto :goto_1

    .line 1100
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "VideoSubtitlePopup"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_1
.end method
