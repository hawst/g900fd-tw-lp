.class Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;
.super Ljava/lang/Object;
.source "ChordService.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 3
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "payloadType"    # Ljava/lang/String;
    .param p4, "payload"    # [[B

    .prologue
    .line 378
    const-string v1, "MVChordService"

    const-string v2, "MVChordServiceClass : onDataReceived()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-string v1, "CHORD_APITEST_MESSAGE_TYPE"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    const/4 v1, 0x0

    aget-object v0, p4, v1

    .line 384
    .local v0, "buf":[B
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v1, p1, p2, v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onReceiveMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 9
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J

    .prologue
    .line 451
    const-string v0, "MVChordService"

    const-string v1, "onFileChunkReceived E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 454
    const-wide/16 v0, 0x64

    mul-long v0, v0, p9

    div-long v0, v0, p7

    long-to-int v4, v0

    .line 455
    .local v4, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p6

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 457
    .end local v4    # "progress":I
    :cond_0
    return-void
.end method

.method public onFileChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 9
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J
    .param p11, "chunkSize"    # J

    .prologue
    .line 523
    const-string v0, "MVChordService"

    const-string v1, "onFileChunkSent E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 526
    const-wide/16 v0, 0x64

    mul-long v0, v0, p9

    div-long v0, v0, p7

    long-to-int v4, v0

    .line 527
    .local v4, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p6

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 529
    .end local v4    # "progress":I
    :cond_0
    return-void
.end method

.method public onFileFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "exchangeId"    # Ljava/lang/String;
    .param p6, "reason"    # I

    .prologue
    const/4 v6, 0x1

    .line 567
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFileFailed E. reason = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    packed-switch p6, :pswitch_data_0

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p3

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 571
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x3

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p3

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 578
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x2

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p3

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 569
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 14
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 474
    const-string v1, "MVChordService"

    const-string v2, "onFileReceived E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    move-object/from16 v6, p3

    .line 478
    .local v6, "savedName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 479
    .local v10, "index":I
    const-string v1, "."

    invoke-virtual {v6, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 481
    .local v9, "i":I
    const/4 v1, 0x0

    invoke-virtual {v6, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 482
    .local v11, "name":Ljava/lang/String;
    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 484
    .local v8, "ext":Ljava/lang/String;
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : onFileReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ext : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    new-instance v13, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v13, v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    .local v13, "targetFile":Ljava/io/File;
    :goto_0
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 490
    new-instance v13, Ljava/io/File;

    .end local v13    # "targetFile":Ljava/io/File;
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v13, v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    .restart local v13    # "targetFile":Ljava/io/File;
    add-int/lit8 v10, v10, 0x1

    .line 494
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : onFileReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 497
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # setter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mRecivedFileName:Ljava/lang/String;
    invoke-static {v1, v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$402(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;Ljava/lang/String;)Ljava/lang/String;

    .line 499
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p9

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    .local v12, "srcFile":Ljava/io/File;
    invoke-virtual {v12, v13}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 502
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 503
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v7, 0x1

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p6

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 506
    :cond_1
    return-void
.end method

.method public onFileSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;

    .prologue
    .line 543
    const-string v0, "MVChordService"

    const-string v1, "onFileSent E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v5, p3

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 549
    :cond_0
    return-void
.end method

.method public onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 19
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J

    .prologue
    .line 402
    const-string v2, "MVChordService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MVChordServiceClass : onFileWillReceive E. [originalName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " exchangeId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fileSize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    new-instance v14, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 407
    .local v14, "targetdir":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 408
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    .line 414
    :cond_0
    new-instance v9, Landroid/os/StatFs;

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v9, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 416
    .local v9, "stat":Landroid/os/StatFs;
    invoke-virtual {v9}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v12, v2

    .line 417
    .local v12, "blockSize":J
    invoke-virtual {v9}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v16, v0

    .line 418
    .local v16, "totalBlocks":J
    mul-long v10, v12, v16

    .line 420
    .local v10, "availableMemory":J
    cmp-long v2, v10, p7

    if-gez v2, :cond_2

    .line 421
    const-string v2, "MVChordService"

    const-string v3, "onFileWillReceive. available memory size is not enough. so reject file."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->rejectFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 425
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v8, 0x1

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p6

    move-object/from16 v7, p3

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 434
    :cond_1
    :goto_0
    return-void

    .line 431
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public onMultiFilesChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
    .locals 9
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J

    .prologue
    .line 653
    const-string v0, "MVChordService"

    const-string v1, "onMultiFilesChunkReceived E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 656
    const-wide/16 v0, 0x64

    mul-long v0, v0, p9

    div-long v0, v0, p7

    long-to-int v4, v0

    .line 657
    .local v4, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v7, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 659
    .end local v4    # "progress":I
    :cond_0
    return-void
.end method

.method public onMultiFilesChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJJ)V
    .locals 9
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "offset"    # J
    .param p11, "chunkSize"    # J

    .prologue
    .line 722
    const-string v0, "MVChordService"

    const-string v1, "onMultiFilesChunkSent E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 725
    const-wide/16 v0, 0x64

    mul-long v0, v0, p9

    div-long v0, v0, p7

    long-to-int v4, v0

    .line 726
    .local v4, "progress":I
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v7, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 728
    .end local v4    # "progress":I
    :cond_0
    return-void
.end method

.method public onMultiFilesFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 7
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "reason"    # I

    .prologue
    .line 766
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMultiFilesFailed E. reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    packed-switch p6, :pswitch_data_0

    .line 791
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onFileFailed() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 800
    :cond_0
    :goto_0
    return-void

    .line 770
    :pswitch_0
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onFileFailed() - REJECTED Node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x3

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 780
    :pswitch_1
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onFileFailed() - CANCELED Node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x2

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 768
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onMultiFilesFinished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "taskId"    # Ljava/lang/String;
    .param p4, "reason"    # I

    .prologue
    .line 811
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMultiFilesFinished E. reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    packed-switch p4, :pswitch_data_0

    .line 840
    :pswitch_0
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onFileFailed() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 815
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 821
    :pswitch_2
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onMultiFilesFinished() - REJECTED Node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 830
    :pswitch_3
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onFileFailed() - CANCELED Node : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 813
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onMultiFilesReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
    .locals 14
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 676
    const-string v1, "MVChordService"

    const-string v2, "onMultiFilesReceived E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    move-object/from16 v6, p3

    .line 679
    .local v6, "savedName":Ljava/lang/String;
    const-string v1, "."

    invoke-virtual {v6, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 680
    .local v9, "i":I
    const/4 v1, 0x0

    invoke-virtual {v6, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 681
    .local v11, "name":Ljava/lang/String;
    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 684
    .local v8, "ext":Ljava/lang/String;
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMultiFilesReceived E. fileName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ext : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    new-instance v13, Ljava/io/File;

    sget-object v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v13, v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    .local v13, "targetFile":Ljava/io/File;
    const/4 v10, 0x0

    .line 689
    .local v10, "i2":I
    :goto_0
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 690
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 691
    new-instance v13, Ljava/io/File;

    .end local v13    # "targetFile":Ljava/io/File;
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v13, v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    .restart local v13    # "targetFile":Ljava/io/File;
    add-int/lit8 v10, v10, 0x1

    .line 695
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : onFileReceived : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 698
    :cond_0
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p9

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 699
    .local v12, "srcFile":Ljava/io/File;
    invoke-virtual {v12, v13}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 701
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    const/4 v2, 0x1

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    move/from16 v7, p5

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 705
    :cond_1
    return-void
.end method

.method public onMultiFilesSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "toNode"    # Ljava/lang/String;
    .param p2, "toChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "fileType"    # Ljava/lang/String;

    .prologue
    .line 742
    const-string v0, "MVChordService"

    const-string v1, "onMultiFilesSent E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 748
    :cond_0
    return-void
.end method

.method public onMultiFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 19
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "taskId"    # Ljava/lang/String;
    .param p5, "count"    # I
    .param p6, "fileType"    # Ljava/lang/String;
    .param p7, "fileSize"    # J

    .prologue
    .line 608
    const-string v2, "MVChordService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MVChordServiceClass : onMultiFilesWillReceive. [originalName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " taskId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fileSize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    new-instance v14, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 612
    .local v14, "targetdir":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 613
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    .line 619
    :cond_0
    new-instance v9, Landroid/os/StatFs;

    sget-object v2, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->chordFilePath:Ljava/lang/String;

    invoke-direct {v9, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 620
    .local v9, "stat":Landroid/os/StatFs;
    invoke-virtual {v9}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v12, v2

    .line 621
    .local v12, "blockSize":J
    invoke-virtual {v9}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v16, v0

    .line 622
    .local v16, "totalBlocks":J
    mul-long v10, v12, v16

    .line 624
    .local v10, "availableMemory":J
    cmp-long v2, v10, p7

    if-gez v2, :cond_2

    .line 625
    const-string v2, "MVChordService"

    const-string v3, "onMultiFilesWillReceive. available memory size is small. reject file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->rejectFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    const/4 v3, 0x4

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p3

    move/from16 v8, p5

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 636
    :cond_1
    :goto_0
    return-void

    .line 634
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 635
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v2

    const/4 v8, 0x1

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    invoke-interface/range {v2 .. v8}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public onNodeJoined(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 339
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : onNodeJoined(), fromNode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", fromChannel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mChord:Lcom/samsung/android/sdk/chord/SchordManager;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$200(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/samsung/android/sdk/chord/SchordManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 344
    .local v0, "channel":Lcom/samsung/android/sdk/chord/SchordChannel;
    if-eqz v0, :cond_0

    .line 346
    const-string v1, "MVChordService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MVChordServiceClass : onNodeJoined() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/chord/SchordChannel;->getNodeIpAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, p1, p2, v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 351
    :cond_1
    return-void
.end method

.method public onNodeLeft(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;

    .prologue
    .line 362
    const-string v0, "MVChordService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVChordServiceClass : onNodeLeft(), fromNode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromChannel : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$3;->this$0:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    # getter for: Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->mListener:Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->access$000(Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;->onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 366
    :cond_0
    return-void
.end method
