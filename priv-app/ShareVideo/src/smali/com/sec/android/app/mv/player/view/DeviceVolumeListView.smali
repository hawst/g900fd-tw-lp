.class public Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.super Lcom/sec/android/app/mv/player/view/ParentDeviceListView;
.source "DeviceVolumeListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$PosCompare;,
        Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;,
        Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;,
        Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    }
.end annotation


# static fields
.field private static final SPK_MODE_LEFT:I = 0x1

.field private static final SPK_MODE_MASTER:I = 0x0

.field private static final SPK_MODE_RIGHT:I = 0x2

.field public static final TAG:Ljava/lang/String;


# instance fields
.field keyListener:Landroid/view/View$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field public mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

.field private mDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIndex:I

.field private mLRspk:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;

.field private mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/ParentDeviceListView;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I

    .line 49
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->keyListener:Landroid/view/View$OnKeyListener;

    .line 113
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;

    .line 114
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    .line 116
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mLRspk:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mLRspk:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->getLR()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;

    .line 119
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$2;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->keyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->getDeviceName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->getSpkStrPos(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSpkStrPos(I)Ljava/lang/String;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 141
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0089

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146
    :goto_0
    return-object v0

    .line 143
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 146
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
