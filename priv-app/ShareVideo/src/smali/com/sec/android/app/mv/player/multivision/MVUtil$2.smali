.class Lcom/sec/android/app/mv/player/multivision/MVUtil$2;
.super Ljava/lang/Object;
.source "MVUtil.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0

    .prologue
    .line 1911
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBuffering(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1948
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnBufferingListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1950
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->pause()V

    .line 1951
    return-void
.end method

.method public onComplete(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1955
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: onCompleteListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1957
    return-void
.end method

.method public onPaused(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1927
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnPausedListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1928
    return-void
.end method

.method public onPlayed(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1922
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnPlayedListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1923
    return-void
.end method

.method public onPrepared(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1914
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnPreparedListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1915
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->prepared(I)V

    .line 1917
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyDeviceChangedToActivity()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3200(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    .line 1918
    return-void
.end method

.method public onReady(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1942
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnReadyListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1943
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->ready(I)V

    .line 1944
    return-void
.end method

.method public onResumed(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1937
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnResumedListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1938
    return-void
.end method

.method public onStopped(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1932
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVSC: OnStoppedListener() id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1933
    return-void
.end method
