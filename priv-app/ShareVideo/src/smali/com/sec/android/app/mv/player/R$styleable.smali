.class public final Lcom/sec/android/app/mv/player/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final TwProgressBar:[I

.field public static final TwProgressBar_progress:I = 0x5

.field public static final TwProgressBar_secondaryProgress:I = 0x6

.field public static final TwProgressBar_twBackgroundColor:I = 0xa

.field public static final TwProgressBar_twBackgroundDrawable:I = 0x7

.field public static final TwProgressBar_twIndicatorThickness:I = 0xd

.field public static final TwProgressBar_twMax:I = 0x4

.field public static final TwProgressBar_twMaxHeight:I = 0x3

.field public static final TwProgressBar_twMaxWidth:I = 0x1

.field public static final TwProgressBar_twMinHeight:I = 0x2

.field public static final TwProgressBar_twMinWidth:I = 0x0

.field public static final TwProgressBar_twProgressColor:I = 0xb

.field public static final TwProgressBar_twProgressDrawable:I = 0x8

.field public static final TwProgressBar_twProgressOrientation:I = 0xe

.field public static final TwProgressBar_twSecondaryColor:I = 0xc

.field public static final TwProgressBar_twSecondaryDrawable:I = 0x9

.field public static final TwSeekBar:[I

.field public static final TwSeekBar_twSeekBarDisableAlpha:I = 0x3

.field public static final TwSeekBar_twSeekBarIncrement:I = 0x2

.field public static final TwSeekBar_twSeekBarSeekable:I = 0x4

.field public static final TwSeekBar_twSeekBarThumb:I = 0x0

.field public static final TwSeekBar_twSeekBarThumbOffset:I = 0x1

.field public static final TwSeekBar_twSeekThumbFontColor:I = 0x5

.field public static final TwSeekBar_twSeekThumbFontEnable:I = 0x7

.field public static final TwSeekBar_twSeekThumbFontSize:I = 0x6

.field public static final TwTabHost:[I

.field public static final TwTabHost_twTabIndicatorType:I = 0x0

.field public static final TwTheme:[I

.field public static final TwTheme_twAlertDialogStyle:I = 0x2

.field public static final TwTheme_twColorPickerStyle:I = 0x4

.field public static final TwTheme_twProgressBarStyle:I = 0x5

.field public static final TwTheme_twRadioButtonStyle:I = 0x1

.field public static final TwTheme_twSeekBarStyle:I = 0x6

.field public static final TwTheme_twSoftkeyItemStyle:I = 0x3

.field public static final TwTheme_twTabWidgetStyle:I = 0x0

.field public static final TwTouchPunchView:[I

.field public static final TwTouchPunchView_punchshape:I = 0x0

.field public static final TwTouchPunchView_punchshow:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1812
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/mv/player/R$styleable;->TwProgressBar:[I

    .line 2041
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/mv/player/R$styleable;->TwSeekBar:[I

    .line 2163
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010017

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/mv/player/R$styleable;->TwTabHost:[I

    .line 2205
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/mv/player/R$styleable;->TwTheme:[I

    .line 2291
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/mv/player/R$styleable;->TwTouchPunchView:[I

    return-void

    .line 1812
    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
    .end array-data

    .line 2041
    :array_1
    .array-data 4
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
    .end array-data

    .line 2205
    :array_2
    .array-data 4
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
        0x7f01001d
        0x7f01001e
    .end array-data

    .line 2291
    :array_3
    .array-data 4
        0x7f01001f
        0x7f010020
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
