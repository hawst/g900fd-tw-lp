.class public Lcom/sec/android/app/mv/player/view/DeviceListView;
.super Lcom/sec/android/app/mv/player/view/ParentDeviceListView;
.source "DeviceListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/view/DeviceListView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/ParentDeviceListView;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;-><init>(Lcom/sec/android/app/mv/player/view/DeviceListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/DeviceListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 30
    return-void
.end method


# virtual methods
.method public update()V
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/mv/player/view/DeviceListView;->TAG:Ljava/lang/String;

    const-string v1, "---- update DeviceListView"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->notifyDataSetChanged()V

    .line 24
    return-void
.end method
