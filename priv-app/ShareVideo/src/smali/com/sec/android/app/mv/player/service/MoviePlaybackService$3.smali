.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 333
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 334
    .local v0, "action":Ljava/lang/String;
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mBroadcastListener action "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    new-instance v1, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-direct {v1, p1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    .line 338
    .local v1, "au":Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getRingerMode()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$3;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    const-wide/16 v4, 0x14

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 342
    :cond_0
    return-void
.end method
