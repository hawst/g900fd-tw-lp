.class public Lcom/sec/android/app/mv/player/multivision/MVUtil;
.super Ljava/lang/Object;
.source "MVUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;
    }
.end annotation


# static fields
.field public static final ALL:I = -0x1

.field private static final EXPIRED_TIME:I = 0x7d0

.field private static final KEEP_ALIVE_TIME:I = 0x1b58

.field private static final MAXIMUM_DEVICE_CONNECT_NUM:I = 0x4

.field private static final MSG_ADD_CLIENT:I = 0x17

.field private static final MSG_CHANGE_TO_MULTI_VISION:I = 0x1c

.field private static final MSG_CHANGE_TO_SINGLE_VISION:I = 0x1b

.field private static final MSG_DELETE_CLIENT:I = 0x18

.field private static final MSG_EXIT_MV_GROUP:I = 0x16

.field private static final MSG_GET_PLAYER_STATE:I = 0x8

.field private static final MSG_GET_PLAY_POSITION:I = 0x7

.field private static final MSG_HIDE:I = 0x1e

.field private static final MSG_JOIN_MV_GROUP:I = 0x15

.field private static final MSG_KEEP_ALIVE:I = 0x20

.field private static final MSG_PAUSE:I = 0x2

.field private static final MSG_PREPARED:I = 0x28

.field private static final MSG_READY:I = 0x29

.field private static final MSG_REQUEST_MV_JOIN_FROM_TOUCH_MOTION:I = 0x14

.field private static final MSG_RESP_EXPIRED:I = 0x21

.field private static final MSG_SEEKTO:I = 0x5

.field private static final MSG_SEND_MASTER_CLOCK:I = 0x1d

.field private static final MSG_SET_DATA_SOURCE:I = 0x1

.field private static final MSG_SET_PLAY_POSITION:I = 0x6

.field private static final MSG_SET_VOLUME_VALUE:I = 0x9

.field private static final MSG_SHOW:I = 0x1f

.field private static final MSG_START:I = 0x3

.field private static final MSG_START_SHARE_PLAY:I = 0x19

.field private static final MSG_STOP:I = 0x4

.field private static final MSG_STOP_SHARE_PLAY:I = 0x1a

.field public static PORT:I = 0x0

.field public static SOCKET_TIMEOUT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MVUtil"

.field private static mConnectionThread:Ljava/lang/Thread;

.field private static mCpuCoreBooster:Landroid/os/DVFSHelper;

.field private static mServerSocket:Ljava/net/ServerSocket;


# instance fields
.field private TIME_SYNC_COUNT:I

.field private mAdjustLeftCropRatio:F

.field private mAdjustRightCropRatio:F

.field private mBottom:I

.field private mChangedRotation:Z

.field private mClientIdMaker:I

.field private mContext:Landroid/content/Context;

.field private mCurrentURL:Ljava/lang/String;

.field private mCurrentUri:Landroid/net/Uri;

.field private mDeviceInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mInitialStart:Z

.field private mIsRequestMultiVision:Z

.field private mLeft:I

.field private mLeftVolume:I

.field private mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

.field private mMVPlayerServerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;",
            ">;"
        }
    .end annotation
.end field

.field private mMVServerPosition:I

.field private mMVSupport:Z

.field private mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

.field private mMVUtilHandlerThread:Landroid/os/HandlerThread;

.field private mMasterVolume:I

.field private mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

.field private mMeasuredHeightRatio:I

.field private final mMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

.field private mMyDeviceName:Ljava/lang/String;

.field private mNumOfMVMembers:I

.field private mRight:I

.field private mRightVolume:I

.field private mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

.field private mServerIP:Ljava/lang/String;

.field private mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

.field private mShareVideoMode:Z

.field private mSingleVisionMode:Z

.field private final mStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

.field private mSystemTimeBase:J

.field private mTop:I

.field private mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

.field private mXdpi:I

.field private mYdpi:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    sput-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCpuCoreBooster:Landroid/os/DVFSHelper;

    .line 1808
    const/16 v0, 0x1b9e

    sput v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->PORT:I

    .line 1810
    const/16 v0, 0x1388

    sput v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->SOCKET_TIMEOUT:I

    .line 1812
    sput-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mConnectionThread:Ljava/lang/Thread;

    .line 1814
    sput-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "devicename"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentUri:Landroid/net/Uri;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mChangedRotation:Z

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    .line 79
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mClientIdMaker:I

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 83
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->TIME_SYNC_COUNT:I

    .line 85
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeft:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRight:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTop:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mBottom:I

    .line 87
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 97
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    .line 100
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    .line 102
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    .line 104
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mXdpi:I

    .line 106
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mYdpi:I

    .line 108
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasuredHeightRatio:I

    .line 110
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustLeftCropRatio:F

    .line 112
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustRightCropRatio:F

    .line 1911
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    .line 1960
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    .line 170
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    .line 172
    if-nez p2, :cond_1

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 176
    const-string v0, "MVUtil"

    const-string v1, "device_name is null in Setting, use Build.MODEL name"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 184
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->init()V

    .line 185
    return-void

    .line 180
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "devicename"    # Ljava/lang/String;
    .param p3, "serverIP"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentUri:Landroid/net/Uri;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mChangedRotation:Z

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    .line 79
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mClientIdMaker:I

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 83
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->TIME_SYNC_COUNT:I

    .line 85
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeft:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRight:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTop:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mBottom:I

    .line 87
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 97
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    .line 100
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    .line 102
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    .line 104
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mXdpi:I

    .line 106
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mYdpi:I

    .line 108
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasuredHeightRatio:I

    .line 110
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustLeftCropRatio:F

    .line 112
    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustRightCropRatio:F

    .line 1911
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$2;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    .line 1960
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    .line 188
    iput-object p3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    .line 189
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    .line 191
    if-nez p2, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 195
    const-string v0, "MVUtil"

    const-string v1, "device_name is null in Setting, use Build.MODEL name"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    .line 202
    :cond_0
    :goto_0
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVUtil for client. mMyDeviceName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->init()V

    .line 204
    return-void

    .line 199
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMessage(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/multivision/MVUtil;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/multivision/MVUtil;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleRequestMVJoinFromTouchMotion(III)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleJoinMVGroup(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleExitMVGroup(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/multivision/MVUtil;Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleAddClient(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleDeleteClient(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStartSharePlay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStopSharePlay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/mv/player/multivision/MVUtil;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetDataSource()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSendMasterClock()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handlePrepared(I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleReady(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleHide(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleShow(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleKeepAlive()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleRespExpired()V

    return-void
.end method

.method static synthetic access$2700()Ljava/net/ServerSocket;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;

    return-object v0
.end method

.method static synthetic access$2702(Ljava/net/ServerSocket;)Ljava/net/ServerSocket;
    .locals 0
    .param p0, "x0"    # Ljava/net/ServerSocket;

    .prologue
    .line 32
    sput-object p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;

    return-object p0
.end method

.method static synthetic access$2808(Lcom/sec/android/app/mv/player/multivision/MVUtil;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mClientIdMaker:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mClientIdMaker:I

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handlePause()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyDeviceChangedToActivity()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->joinMVGroup_l(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup_l(I)V

    return-void
.end method

.method static synthetic access$3500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    return-wide v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/mv/player/multivision/MVUtil;Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->tryClientTimeSync(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->hide_l(I)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->show_l(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStart()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getType(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStop()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSeekTo(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetPlayPosition(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleGetPlayPosition()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleGetPlayerState()V

    return-void
.end method

.method private addMVClientPosition(II)V
    .locals 6
    .param p1, "addedPosition"    # I
    .param p2, "add_id"    # I

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    .line 642
    .local v2, "mvJoined":I
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addMVClientPosition mvJoined : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", addedPosition : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", add_id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    if-lt v3, p1, :cond_0

    .line 646
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 649
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 651
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v3

    if-nez v3, :cond_1

    .line 652
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    .line 653
    .local v1, "mvClientPosition":I
    if-lt v1, p1, :cond_1

    .line 654
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 659
    .end local v1    # "mvClientPosition":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v3

    if-ne v3, p2, :cond_2

    .line 660
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 661
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setGroupMember(Z)V

    .line 663
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateNumOfMVMembers()V

    .line 649
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 666
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->showMVMemberPosition()V

    .line 667
    return-void
.end method

.method private deleteMVClientPosition(I)V
    .locals 6
    .param p1, "deletePosition"    # I

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    .line 618
    .local v2, "mvJoined":I
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteMVClientPosition mvJoined : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", deletePosition : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    if-ge p1, v3, :cond_0

    .line 622
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 625
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 627
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v3

    if-nez v3, :cond_1

    .line 628
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    .line 629
    .local v1, "mvClientPosition":I
    if-ge p1, v1, :cond_1

    .line 630
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 625
    .end local v1    # "mvClientPosition":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 635
    :cond_2
    const/4 v3, 0x2

    if-le v2, v3, :cond_3

    .line 636
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->showMVMemberPosition()V

    .line 638
    :cond_3
    return-void
.end method

.method private determineAllClientPosition()V
    .locals 6

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    .line 699
    .local v2, "mvNumber":I
    const/4 v3, 0x1

    .line 700
    .local v3, "num":I
    const/4 v0, 0x1

    .line 702
    .local v0, "changeFlag":I
    add-int/lit8 v4, v2, 0x1

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 704
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 705
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v4

    if-nez v4, :cond_0

    .line 707
    rem-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_1

    .line 708
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    add-int/2addr v5, v3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 713
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 704
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 710
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    sub-int/2addr v5, v3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 711
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 717
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->showMVMemberPosition()V

    .line 718
    return-void
.end method

.method private determineDisableRequestMultiVision()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 680
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    if-eqz v2, :cond_2

    .line 681
    const/4 v1, 0x0

    .line 682
    .local v1, "isSupportMV":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 683
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSupportMultiVision()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 684
    const/4 v1, 0x1

    .line 689
    :cond_0
    if-nez v1, :cond_2

    .line 690
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    .line 691
    const/4 v2, 0x1

    .line 694
    .end local v0    # "i":I
    .end local v1    # "isSupportMV":Z
    :goto_1
    return v2

    .line 682
    .restart local v0    # "i":I
    .restart local v1    # "isSupportMV":Z
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    .end local v1    # "isSupportMV":Z
    :cond_2
    move v2, v3

    .line 694
    goto :goto_1
.end method

.method private determineEnableRequestMultiVision(Z)Z
    .locals 2
    .param p1, "isSupportMV"    # Z

    .prologue
    const/4 v0, 0x1

    .line 670
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    if-nez v1, :cond_0

    .line 671
    if-eqz p1, :cond_0

    .line 672
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    .line 676
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private exitMVGroup_l(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 911
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 913
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 918
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 916
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->exitMVGroup()V

    goto :goto_0
.end method

.method private getDisplayInfo()V
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getScreenOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    .line 219
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDisplayInfo portrait. width : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mXdpi:I

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mYdpi:I

    .line 228
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDisplayInfo xdpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mXdpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ydpi : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mYdpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    .line 223
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDisplayInfo landscape. width : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private getMessage(I)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 1670
    packed-switch p1, :pswitch_data_0

    .line 1680
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1671
    :pswitch_0
    const-string v0, "MSG_SET_DATA_SOURCE"

    goto :goto_0

    .line 1672
    :pswitch_1
    const-string v0, "MSG_PAUSE"

    goto :goto_0

    .line 1673
    :pswitch_2
    const-string v0, "MSG_START"

    goto :goto_0

    .line 1674
    :pswitch_3
    const-string v0, "MSG_STOP"

    goto :goto_0

    .line 1675
    :pswitch_4
    const-string v0, "MSG_SEEKTO"

    goto :goto_0

    .line 1676
    :pswitch_5
    const-string v0, "MSG_SET_PLAY_POSITION"

    goto :goto_0

    .line 1677
    :pswitch_6
    const-string v0, "MSG_GET_PLAY_POSITION"

    goto :goto_0

    .line 1678
    :pswitch_7
    const-string v0, "MSG_GET_PLAYER_STATE"

    goto :goto_0

    .line 1679
    :pswitch_8
    const-string v0, "MSG_SET_VOLUME_VALUE"

    goto :goto_0

    .line 1670
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getType(I)I
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 2183
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2184
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 2185
    .local v1, "listSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 2186
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    iget v2, v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->mID:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getClientDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 2190
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2191
    const/16 v2, 0x12d

    .line 2198
    .end local v0    # "i":I
    .end local v1    # "listSize":I
    :goto_1
    return v2

    .line 2192
    .restart local v0    # "i":I
    .restart local v1    # "listSize":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 2193
    const/16 v2, 0x12e

    goto :goto_1

    .line 2185
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2198
    .end local v0    # "i":I
    .end local v1    # "listSize":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private handleAddClient(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 7
    .param p1, "player"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    const/16 v6, 0x20

    const/4 v5, 0x4

    .line 1217
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleAddClient id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 1220
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleAddClient connected device : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", MAXIMUM_DEVICE_CONNECT_NUM : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    const/16 v2, 0xbb9

    invoke-virtual {p1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->endConnection(I)V

    .line 1241
    :goto_0
    return-void

    .line 1225
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendContentURL(Ljava/lang/String;)V

    .line 1226
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1229
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSupportMultiVision()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->determineEnableRequestMultiVision(Z)Z

    .line 1230
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(I)V

    .line 1231
    .local v0, "deviceInfo":Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getClientDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDeviceName(Ljava/lang/String;)V

    .line 1232
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    .line 1236
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->removeMessages(I)V

    .line 1237
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->removeMessages(I)V

    .line 1239
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1240
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private handleChangeToMultiVision(Z)V
    .locals 10
    .param p1, "whole"    # Z

    .prologue
    const/16 v5, 0x6d

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1417
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleChangeToMultiVision whole : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1419
    if-ne p1, v8, :cond_3

    .line 1420
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1421
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSupportMultiVision()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1422
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1423
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setGroupMember(Z)V

    .line 1420
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1427
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateNumOfMVMembers()V

    .line 1429
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v2, v8, :cond_2

    .line 1471
    :goto_1
    return-void

    .line 1432
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->determineAllClientPosition()V

    .line 1435
    .end local v0    # "i":I
    :cond_3
    const/4 v1, 0x0

    .line 1436
    .local v1, "save":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1437
    const/4 v1, 0x1

    .line 1438
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getVolumeValue()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 1439
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mLeftVolume, mRightVolume = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->getVolumeValue()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1442
    :cond_4
    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setSingleVisionMode(Z)V

    .line 1443
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAudioChannel()V

    .line 1445
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->measureMultiVisionSize(Ljava/util/List;)V

    .line 1447
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getChangedRotation()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1448
    const/16 v2, 0x130

    invoke-virtual {p0, v5, v2, v7, v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1453
    :goto_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeft:I

    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTop:I

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRight:I

    iget v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mBottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/mv/player/common/SUtils;->setVideoCrop(IIII)Z

    .line 1454
    const/16 v2, 0x66

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {p0, v2, v3, v1, v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1456
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 1457
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v2

    if-ne v2, v8, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1459
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setClientMultiVisionMode()V

    .line 1456
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1450
    .end local v0    # "i":I
    :cond_6
    const/16 v2, 0x12f

    invoke-virtual {p0, v5, v2, v7, v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    goto :goto_2

    .line 1463
    .restart local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1464
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDevicePos(I)V

    .line 1463
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1467
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDevicePos(I)V

    .line 1469
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 1470
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    goto/16 :goto_1
.end method

.method private handleChangeToSingleVision(I)V
    .locals 6
    .param p1, "clientIndex"    # I

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 1382
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleChangeToSingleVision clientIndex : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    if-ne p1, v5, :cond_0

    .line 1384
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1385
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setSVCropSize()V

    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setGroupMember(Z)V

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 1388
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAudioChannel(I)V

    .line 1389
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setVolumeOn()V

    .line 1390
    const-string v1, "MVUtil"

    const-string v2, "Call Client - setSVCropSize() "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1393
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setGroupMember(Z)V

    .line 1394
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setSVCropSize()V

    .line 1395
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVClientPosition(I)V

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAudioChannel(I)V

    .line 1397
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setVolumeOn()V

    .line 1398
    const-string v1, "MVUtil"

    const-string v2, "handleChangeToSingleVision _ Client"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateNumOfMVMembers()V

    .line 1402
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    if-ne p1, v5, :cond_3

    .line 1403
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setServerSingleVision()V

    .line 1406
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 1407
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDevicePos(I)V

    .line 1406
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1410
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDevicePos(I)V

    .line 1412
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 1413
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    .line 1414
    return-void
.end method

.method private handleDeleteClient(I)V
    .locals 11
    .param p1, "id"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1270
    const-string v5, "MVUtil"

    const-string v8, "handleDeleteClient"

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    const/4 v2, -0x1

    .line 1273
    .local v2, "index":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1274
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 1275
    move v2, v1

    .line 1279
    :cond_0
    const-string v5, "MVUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleDeleteClient mvPlayerServer id : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", index : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    if-ne v2, v10, :cond_3

    .line 1281
    const-string v5, "MVUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleDeleteClient client id("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") is already deleted. ignore it."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1334
    :cond_1
    :goto_1
    return-void

    .line 1273
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1285
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/16 v8, 0xbb8

    invoke-virtual {v5, v8}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->endConnection(I)V

    .line 1286
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v0

    .line 1287
    .local v0, "changedPosition":I
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v5

    if-nez v5, :cond_5

    move v3, v6

    .line 1289
    .local v3, "member":Z
    :goto_2
    if-eqz v3, :cond_6

    .line 1290
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getClientDeviceName()Ljava/lang/String;

    move-result-object v4

    .line 1291
    .local v4, "name":Ljava/lang/String;
    const/16 v5, 0x69

    invoke-virtual {p0, v5, p1, v0, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1292
    const/4 v1, 0x0

    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_6

    .line 1293
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1294
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5, p1, v0, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendMultiVisionDeviceDisconnected(IILjava/lang/String;)V

    .line 1292
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v3    # "member":Z
    .end local v4    # "name":Ljava/lang/String;
    :cond_5
    move v3, v7

    .line 1287
    goto :goto_2

    .line 1299
    .restart local v3    # "member":Z
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1300
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->determineDisableRequestMultiVision()Z

    .line 1301
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateNumOfMVMembers()V

    .line 1303
    const/4 v1, 0x0

    :goto_4
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_7

    .line 1304
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceID()I

    move-result v5

    if-ne v5, p1, :cond_8

    .line 1305
    move v2, v1

    .line 1309
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1311
    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    if-ne v5, v6, :cond_9

    if-eqz v3, :cond_9

    .line 1312
    const-string v5, "MVUtil"

    const-string v6, "handleDeleteClient, server have to change singlevision mode"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setServerSingleVision()V

    .line 1315
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v5, v10}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->setDevicePos(I)V

    .line 1317
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 1318
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    goto/16 :goto_1

    .line 1303
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1323
    :cond_9
    if-ne v3, v6, :cond_a

    .line 1324
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->deleteMVClientPosition(I)V

    .line 1325
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->measureMultiVisionSize(Ljava/util/List;)V

    .line 1326
    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    if-nez v5, :cond_1

    .line 1327
    invoke-direct {p0, v7}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    goto/16 :goto_1

    .line 1331
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 1332
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    goto/16 :goto_1
.end method

.method private handleExitMVGroup(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 1182
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleExitMVGroup id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    const/4 v2, -0x1

    .line 1185
    .local v2, "index":I
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 1186
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V

    .line 1214
    :cond_0
    :goto_0
    return-void

    .line 1189
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 1193
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1194
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v3

    if-ne v3, p1, :cond_4

    .line 1195
    move v2, v1

    .line 1200
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_3

    if-ltz v2, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1201
    :cond_3
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleExitMVGroup id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", already not mv group"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1193
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1205
    :cond_5
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleExitMVGroup - client id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", index : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v0

    .line 1208
    .local v0, "changedPosition":I
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V

    .line 1209
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->deleteMVClientPosition(I)V

    .line 1211
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 1212
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    goto/16 :goto_0
.end method

.method private handleGetPlayPosition()V
    .locals 3

    .prologue
    .line 1064
    const-string v1, "MVUtil"

    const-string v2, "handleGetPlayPosition"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1066
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getPlayPosition()V

    .line 1065
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1068
    :cond_0
    return-void
.end method

.method private handleGetPlayerState()V
    .locals 3

    .prologue
    .line 1071
    const-string v1, "MVUtil"

    const-string v2, "handleGetPlayerState"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1073
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getPlayerState()V

    .line 1072
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1075
    :cond_0
    return-void
.end method

.method private handleHide(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1570
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1571
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v1

    if-ne v1, p1, :cond_2

    .line 1572
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1573
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup_l(I)V

    .line 1575
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setHide(Z)V

    .line 1576
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->determineDisableRequestMultiVision()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1577
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyDeviceChangedToActivity()V

    .line 1582
    :cond_1
    return-void

    .line 1570
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private handleJoinMVGroup(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1173
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleJoinMVGroup id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1175
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    .line 1179
    :goto_0
    return-void

    .line 1177
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleRequestMVJoinFromSwitch(I)V

    goto :goto_0
.end method

.method private handleKeepAlive()V
    .locals 6

    .prologue
    .line 1642
    const-string v3, "MVUtil"

    const-string v4, "handleKeepAlive()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1643
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1644
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendKeepAlive()V

    .line 1643
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1646
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1647
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1648
    .local v1, "msg1":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const-wide/16 v4, 0x1b58

    invoke-virtual {v3, v1, v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1649
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v4, 0x21

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1650
    .local v2, "msg2":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v3, v2, v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1652
    .end local v1    # "msg1":Landroid/os/Message;
    .end local v2    # "msg2":Landroid/os/Message;
    :cond_1
    return-void
.end method

.method private handlePause()V
    .locals 3

    .prologue
    .line 1008
    const-string v1, "MVUtil"

    const-string v2, "handlePause"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1011
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->pause()V

    .line 1010
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1013
    :cond_0
    return-void
.end method

.method private handlePrepared(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 1516
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handlePrepared id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 1520
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getCurrentPosition()I

    move-result v1

    .line 1521
    .local v1, "position":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1522
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 1523
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->seekTo(I)V

    .line 1524
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handlePrepared id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", seek server\'s current position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1521
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1528
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->seekTo(I)V

    .line 1529
    return-void
.end method

.method private handleReady(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 1532
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleReady id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1533
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1534
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getBufferStatus()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 1536
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleReady another device not ready, id : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    :goto_1
    return-void

    .line 1533
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1543
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    if-nez v1, :cond_4

    .line 1544
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1545
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1546
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1547
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V

    .line 1545
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1550
    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    .line 1555
    :goto_3
    const-string v1, "MVUtil"

    const-string v2, "handleReady all client ready to start"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1556
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 1559
    :cond_4
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1560
    const-string v1, "MVUtil"

    const-string v2, "handleReady not paused by user, start playing"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->start()V

    goto :goto_1

    .line 1552
    :cond_5
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V

    goto :goto_3

    .line 1563
    :cond_6
    const-string v1, "MVUtil"

    const-string v2, "handleReady paused by user, stay pause!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->pause()V

    goto :goto_1
.end method

.method private handleRequestMVJoinFromSwitch(I)V
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x1

    .line 1078
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 1079
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v5

    if-ne v5, p1, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v5

    if-ne v5, v7, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1082
    const-string v5, "MVUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleRequestMVJoinFromSwitch, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " already MVGroup member"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    :goto_1
    return-void

    .line 1078
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1086
    :cond_1
    const/4 v1, 0x0

    .line 1087
    .local v1, "leftNum":I
    const/4 v4, 0x0

    .line 1088
    .local v4, "rightNum":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    .line 1089
    .local v2, "mvNum":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVServerPosition()I

    move-result v3

    .line 1091
    .local v3, "mvServerPosition":I
    if-le v2, v7, :cond_2

    .line 1092
    sub-int v4, v2, v3

    .line 1093
    add-int/lit8 v5, v4, 0x1

    sub-int v1, v2, v5

    .line 1096
    :cond_2
    if-eq v4, v1, :cond_3

    if-ge v4, v1, :cond_6

    .line 1097
    :cond_3
    const-string v5, "MVUtil"

    const-string v6, "handleRequestMVJoinFromSwitch_add client right position"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1099
    iput v7, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 1101
    :cond_4
    add-int/lit8 v5, v2, 0x1

    invoke-direct {p0, v5, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    .line 1106
    :cond_5
    :goto_2
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    goto :goto_1

    .line 1102
    :cond_6
    if-le v4, v1, :cond_5

    .line 1103
    const-string v5, "MVUtil"

    const-string v6, "handleRequestMVJoinFromSwitch_add client left position"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    invoke-direct {p0, v7, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    goto :goto_2
.end method

.method private handleRequestMVJoinFromTouchMotion(III)V
    .locals 7
    .param p1, "id_1"    # I
    .param p2, "id_2"    # I
    .param p3, "direction"    # I

    .prologue
    const/4 v6, 0x1

    .line 1110
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleRequestMVJoinFromTouchMotion id_1(first touch) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", id_2(second touch) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", direction : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    const/4 v1, 0x0

    .line 1112
    .local v1, "mvPlayer_1":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    const/4 v2, 0x0

    .line 1114
    .local v2, "mvPlayer_2":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1115
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 1116
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "mvPlayer_1":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 1114
    .restart local v1    # "mvPlayer_1":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1117
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v3

    if-ne v3, p2, :cond_0

    .line 1118
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mvPlayer_2":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .restart local v2    # "mvPlayer_2":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    goto :goto_1

    .line 1123
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_a

    .line 1124
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1125
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion, first client is already MultiVision Group member"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1169
    :goto_2
    return-void

    .line 1127
    :cond_4
    if-nez p2, :cond_7

    if-nez p3, :cond_7

    .line 1128
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1129
    iput v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 1131
    :cond_5
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    .line 1167
    :cond_6
    :goto_3
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V

    goto :goto_2

    .line 1132
    :cond_7
    if-nez p2, :cond_9

    if-ne p3, v6, :cond_9

    .line 1133
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1134
    iput v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 1136
    :cond_8
    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    goto :goto_3

    .line 1138
    :cond_9
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion, couldn\'t join master and client (direction problem)"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1144
    :cond_a
    if-eqz v1, :cond_b

    if-nez v2, :cond_c

    .line 1145
    :cond_b
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion we couldn\'t find mvPlayerServer instance from mMVPlayerList"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1149
    :cond_c
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1150
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion two client are MultiVision Group member"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1152
    :cond_d
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-nez v3, :cond_e

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-nez v3, :cond_e

    .line 1153
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion two client are not MultiVision Group member"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1155
    :cond_e
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v3

    if-nez v3, :cond_f

    .line 1156
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion first client is mv group member, second client is not mv group member. (direction problem)"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1159
    :cond_f
    const-string v3, "MVUtil"

    const-string v4, "handleRequestMVJoinFromTouchMotion second client is mv group member, first client is not mv group member, first client try to join mv group....."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1160
    if-nez p3, :cond_10

    .line 1161
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    goto :goto_3

    .line 1162
    :cond_10
    if-ne p3, v6, :cond_6

    .line 1163
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addMVClientPosition(II)V

    goto/16 :goto_3
.end method

.method private handleRespExpired()V
    .locals 4

    .prologue
    .line 1655
    const-string v1, "MVUtil"

    const-string v2, "handleRespExpired()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1656
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1657
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isRespExpired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1658
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getNumOfExpired()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1659
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleRespExpired()_deleteClient ID = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleDeleteClient(I)V

    .line 1656
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1663
    :cond_1
    const-string v1, "MVUtil"

    const-string v2, "handleRespExpired()_Wait next Resp"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1667
    :cond_2
    return-void
.end method

.method private handleSeekTo(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 1049
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSeekTo pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1052
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->seekTo(I)V

    .line 1051
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1054
    :cond_0
    return-void
.end method

.method private handleSendMasterClock()V
    .locals 27

    .prologue
    .line 1474
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSendMasterClock E. mInitialStart : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1476
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    if-eqz v3, :cond_0

    .line 1477
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1478
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    .line 1479
    .local v24, "apiTimeBase":J
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->getServerTimestampInfo()[J

    move-result-object v26

    .line 1481
    .local v26, "timeinfo":[J
    if-eqz v26, :cond_1

    .line 1482
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v4, v24

    const-wide/16 v14, 0x2

    div-long/2addr v4, v14

    const-wide/16 v14, 0x3e8

    mul-long v22, v4, v14

    .line 1483
    .local v22, "apiGap":J
    const/4 v3, 0x0

    aget-wide v4, v26, v3

    add-long v6, v4, v22

    .line 1484
    .local v6, "videoTime":J
    const/4 v3, 0x1

    aget-wide v4, v26, v3

    add-long v8, v4, v22

    .line 1485
    .local v8, "currentSecMediaClock":J
    const/4 v3, 0x2

    aget-wide v4, v26, v3

    add-long v10, v4, v22

    .line 1487
    .local v10, "currentAudioTimestamp":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    .line 1489
    const-string v3, "MVUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSendMasterClock. apiGap : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", videoTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", currentSecMediaClock : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", currentAudioTimestamp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1494
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    sub-long/2addr v4, v14

    invoke-virtual/range {v3 .. v11}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendMasterClock(JJJJ)V

    .line 1497
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v4, v24

    const-wide/16 v14, 0x3e8

    mul-long v22, v4, v14

    .line 1498
    add-long v6, v6, v22

    .line 1499
    add-long v8, v8, v22

    .line 1500
    add-long v10, v10, v22

    .line 1493
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1505
    .end local v2    # "i":I
    .end local v6    # "videoTime":J
    .end local v8    # "currentSecMediaClock":J
    .end local v10    # "currentAudioTimestamp":J
    .end local v22    # "apiGap":J
    .end local v24    # "apiTimeBase":J
    .end local v26    # "timeinfo":[J
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1506
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    sub-long v14, v4, v14

    const-wide/16 v16, -0x1

    const-wide/16 v18, -0x1

    const-wide/16 v20, -0x1

    invoke-virtual/range {v13 .. v21}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendMasterClock(JJJJ)V

    .line 1505
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1511
    .end local v2    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v4, 0x1d

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v12

    .line 1512
    .local v12, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v12, v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1513
    return-void
.end method

.method private handleSetDataSource()V
    .locals 6

    .prologue
    const/16 v5, 0x1d

    const/4 v4, 0x0

    .line 984
    const-string v2, "MVUtil"

    const-string v3, "handleSetDataSource"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setChangedRotation(Z)V

    .line 987
    invoke-virtual {p0, v4, v4, v4, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMeasuredServerCropSize(IIII)V

    .line 989
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->unregisterContent(Landroid/net/Uri;)V

    .line 990
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->createContentURL()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    .line 991
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleSetDataSource mCurrentURL = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 994
    const-string v2, "MVUtil"

    const-string v3, "handleSetDataSource failed : contentURL is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    :cond_0
    return-void

    .line 998
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->removeMessages(I)V

    .line 999
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1000
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1002
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1003
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setDataSource(Ljava/lang/String;)V

    .line 1002
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private handleSetPlayPosition(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 1057
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSetPlayPosition pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1059
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setPlayPosition(I)V

    .line 1058
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1061
    :cond_0
    return-void
.end method

.method private handleSetVolumeValue(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x6c

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1595
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSetVolumeValue() type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    const/16 v1, 0x12c

    if-ne p1, v1, :cond_4

    .line 1598
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1599
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    if-eq v1, v4, :cond_0

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1600
    :cond_0
    invoke-virtual {p0, v6, p2, v5, v7}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1602
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 1603
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1604
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    if-eq v1, v4, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 1606
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendVolumeValue(I)V

    .line 1602
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1611
    .end local v0    # "i":I
    :cond_4
    const/16 v1, 0x12d

    if-ne p1, v1, :cond_7

    .line 1612
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1613
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    if-ne v1, v4, :cond_5

    .line 1614
    invoke-virtual {p0, v6, p2, v5, v7}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1617
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 1618
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1619
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 1620
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendVolumeValue(I)V

    .line 1617
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1625
    .end local v0    # "i":I
    :cond_7
    const/16 v1, 0x12e

    if-ne p1, v1, :cond_a

    .line 1626
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1627
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_8

    .line 1628
    invoke-virtual {p0, v6, p2, v5, v7}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 1630
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 1631
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1632
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_9

    .line 1633
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendVolumeValue(I)V

    .line 1630
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1637
    .end local v0    # "i":I
    :cond_a
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is Invalid value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    :cond_b
    return-void
.end method

.method private handleShow(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1585
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1586
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 1587
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setHide(Z)V

    .line 1588
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSupportMultiVision()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->determineEnableRequestMultiVision(Z)Z

    .line 1592
    :cond_0
    return-void

    .line 1585
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private handleStart()V
    .locals 3

    .prologue
    .line 1016
    const-string v1, "MVUtil"

    const-string v2, "handleStart"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1019
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getBufferStatus()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    .line 1021
    const-string v1, "MVUtil"

    const-string v2, "handleStart client is not ready, server have to pause"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->pause()V

    .line 1035
    :cond_0
    :goto_1
    return-void

    .line 1018
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1027
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSeeking()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1028
    const-string v1, "MVUtil"

    const-string v2, "handleStart client is seeking, server can\'t start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1032
    :cond_3
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1033
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->resume()V

    .line 1032
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private handleStartSharePlay()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1337
    const-string v2, "MVUtil"

    const-string v3, "handleStartSharePlay E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    const/4 v4, -0x1

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(ILjava/lang/String;I)V

    .line 1341
    .local v0, "deviceInfo":Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 1344
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendDeviceInfo()V

    .line 1346
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setShareVideoMode(Z)V

    .line 1347
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setSingleVisionMode(Z)V

    .line 1349
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->waitClientConnectionWithSetup()V

    .line 1351
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    .line 1352
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1353
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1355
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->startMVShakeMotion()V

    .line 1356
    return v5
.end method

.method private handleStop()V
    .locals 3

    .prologue
    .line 1038
    const-string v1, "MVUtil"

    const-string v2, "handleStop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 1041
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x1d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->removeMessages(I)V

    .line 1043
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1044
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->stop()V

    .line 1043
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1046
    :cond_0
    return-void
.end method

.method private handleStopSharePlay()Z
    .locals 2

    .prologue
    .line 1360
    const-string v0, "MVUtil"

    const-string v1, "handleStopSharePlay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->resetMVPlayer()V

    .line 1363
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopConnectionThread()V

    .line 1365
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setShareVideoMode(Z)V

    .line 1366
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopMVShakeMotion()V

    .line 1369
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->stopServer()V

    .line 1370
    const-string v0, "MVUtil"

    const-string v1, "handleStopSharePlay server finished"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1373
    const-string v0, "MVUtil"

    const-string v1, "exit mMVUtilHandlerThread"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 1375
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    .line 1378
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private hide_l(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 796
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 797
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 799
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 232
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    .line 233
    const-string v0, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init. mMVSupport : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    if-eqz v0, :cond_0

    .line 236
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayInfo()V

    .line 237
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;-><init>(Landroid/content/Context;Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    .line 238
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;II)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    .line 241
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 242
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    if-eqz v0, :cond_1

    .line 243
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasureSize:Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;

    .line 247
    :cond_1
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->startServer()Z

    .line 250
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MVUtilHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 253
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    .line 263
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->enableCpuCore()V

    .line 264
    return-void

    .line 232
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    :cond_3
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;-><init>(Landroid/content/Context;Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->startConnection(Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setClientMultiVisionSupport(Z)V

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->startMVShakeMotion()V

    .line 260
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setShareVideoMode(Z)V

    goto :goto_1
.end method

.method private isSeeking()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 448
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 449
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isSeeking()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    const/4 v1, 0x1

    .line 456
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 449
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 454
    goto :goto_1

    .end local v0    # "i":I
    :cond_2
    move v1, v2

    .line 456
    goto :goto_1
.end method

.method private joinMVGroup_l(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 897
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 899
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 900
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 904
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 902
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->joinMVGroup()V

    goto :goto_0
.end method

.method private notifyDeviceChangedToActivity()V
    .locals 1

    .prologue
    .line 2124
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyDeviceChangedToActivity()V

    .line 2125
    return-void
.end method

.method private printDeviceInfo()V
    .locals 4

    .prologue
    .line 1244
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 1245
    const-string v1, "MVUtil"

    const-string v2, "-----------------------------------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1247
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceInfo["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] : id = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceID()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceInfo["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] : name = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeviceInfo["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] : pos = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    const-string v1, "MVUtil"

    const-string v2, "-----------------------------------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1253
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private resetShareVideoMode()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 588
    const-string v0, "MVUtil"

    const-string v1, "resetShareVideoMode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 598
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setShareVideoMode(Z)V

    .line 600
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mInitialStart:Z

    .line 601
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    .line 602
    return-void
.end method

.method private setAudioChannel()V
    .locals 9

    .prologue
    const/16 v8, 0x6b

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 722
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    if-ne v1, v5, :cond_1

    .line 723
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server is located on the Left, mMVServerPosition = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-virtual {p0, v8, v4, v4, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 725
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->setAudioChannel(I)V

    .line 726
    const/16 v1, 0x12d

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V

    .line 738
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 739
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 741
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client is located on the Left, MVClientPosition : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setVolumeOn()V

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAudioChannel(I)V

    .line 745
    const/16 v1, 0x12d

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V

    .line 738
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 727
    .end local v0    # "i":I
    :cond_1
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 728
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server is located on the Right, mMVServerPosition = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MVMembersNumber = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    invoke-virtual {p0, v8, v4, v4, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 730
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/sec/android/app/mv/player/common/SUtils;->setAudioChannel(I)V

    .line 731
    const/16 v1, 0x12e

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V

    goto/16 :goto_0

    .line 733
    :cond_2
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server is located in the Middle, mMVServerPosition = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const/16 v1, 0x6a

    invoke-virtual {p0, v1, v4, v4, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    goto/16 :goto_0

    .line 746
    .restart local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 747
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client is located on the Right, MVClientPosition : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", MVMembersNumber = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setVolumeOn()V

    .line 751
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAudioChannel(I)V

    .line 752
    const/16 v1, 0x12e

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V

    goto/16 :goto_2

    .line 754
    :cond_4
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client is located in the Middle, MVClientPosition : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setVolumeMute()V

    goto/16 :goto_2

    .line 761
    :cond_5
    return-void
.end method

.method private setServerSingleVision()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 2128
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getVideoWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getVideoHeight()I

    move-result v2

    invoke-virtual {p0, v3, v3, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMeasuredServerCropSize(IIII)V

    .line 2130
    const/4 v0, 0x0

    .line 2131
    .local v0, "save":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2132
    const/4 v0, 0x1

    .line 2134
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setSingleVisionMode(Z)V

    .line 2135
    iput v7, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    .line 2137
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getChangedRotation()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2138
    const/16 v1, 0x6d

    const/16 v2, 0x12f

    invoke-virtual {p0, v1, v2, v3, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 2139
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setChangedRotation(Z)V

    .line 2141
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/mv/player/common/SUtils;->setAudioChannel(I)V

    .line 2142
    const/16 v1, 0x6b

    invoke-virtual {p0, v1, v3, v3, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 2144
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeft:I

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTop:I

    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRight:I

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mBottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->setVideoCrop(IIII)Z

    .line 2145
    const/16 v1, 0x66

    invoke-virtual {p0, v1, v7, v0, v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyMessage(IIILjava/lang/String;)V

    .line 2146
    return-void
.end method

.method private showMVMemberPosition()V
    .locals 4

    .prologue
    .line 605
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showMVMemberPosition [Master : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v1

    if-nez v1, :cond_0

    .line 609
    const-string v2, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showMVMemberPosition [Client : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 613
    :cond_1
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showMVMemberPosition MV Joined Member : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    return-void
.end method

.method private show_l(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 812
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 813
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 814
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 815
    return-void
.end method

.method private tryClientTimeSync(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 11
    .param p1, "player"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    const-wide/16 v4, -0x1

    .line 1896
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tryClientTimeSync id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1898
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->TIME_SYNC_COUNT:I

    if-ge v10, v1, :cond_0

    .line 1899
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    sub-long/2addr v2, v6

    move-object v1, p1

    move-wide v6, v4

    move-wide v8, v4

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendMasterClock(JJJJ)V

    .line 1902
    const-wide/16 v2, 0x14

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1898
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1903
    :catch_0
    move-exception v0

    .line 1904
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 1908
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method

.method private updateNumOfMVMembers()V
    .locals 4

    .prologue
    .line 511
    const/4 v1, 0x1

    .line 513
    .local v1, "num":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 514
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v2

    if-nez v2, :cond_0

    .line 516
    add-int/lit8 v1, v1, 0x1

    .line 513
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 520
    :cond_1
    iput v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    .line 521
    return-void
.end method


# virtual methods
.method public addClient(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 3
    .param p1, "player"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    .line 921
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x17

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 922
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 923
    return-void
.end method

.method public createContentURL()Ljava/lang/String;
    .locals 8

    .prologue
    .line 549
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 550
    .local v0, "fileInfo":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoDbId()J

    move-result-wide v4

    .line 551
    .local v4, "videoId":J
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 553
    .local v2, "uri":Landroid/net/Uri;
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 554
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 557
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentUri:Landroid/net/Uri;

    .line 558
    const-string v3, "MVUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createContentURL. video id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", uri = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServer:Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/multivision/MVHTTPServer;->registerContent(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 563
    .local v1, "httpURL":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 564
    :cond_1
    const-string v3, "MVUtil"

    const-string v6, "createContentURL. httpURL is null or empty. register failed..."

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    :goto_0
    return-object v1

    .line 566
    :cond_2
    const-string v3, "http://ipaddr:"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sshttp://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 567
    const-string v3, "MVUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createContentURL. http url = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteClient(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 926
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 927
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 928
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 929
    return-void
.end method

.method public disableCpuCore()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public enableCpuCore()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public exitMVGroup()V
    .locals 1

    .prologue
    .line 907
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup_l(I)V

    .line 908
    return-void
.end method

.method public getAdjustLeftCropRatio()F
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustLeftCropRatio:F

    return v0
.end method

.method public getAdjustRightCropRatio()F
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustRightCropRatio:F

    return v0
.end method

.method public getChangedRotation()Z
    .locals 1

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mChangedRotation:Z

    return v0
.end method

.method public getClientVolumeValue(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 769
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 770
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    .line 775
    :goto_0
    return v0

    .line 771
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 772
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    goto :goto_0

    .line 774
    :cond_1
    const-string v0, "MVUtil"

    const-string v1, "Type Error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    const/4 v0, 0x0

    .line 431
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->contentURL()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 2111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    if-lt v0, v1, :cond_0

    .line 2112
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    .line 2115
    :goto_0
    return-object v0

    .line 2114
    :cond_0
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "out of index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2115
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMyDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceSize()I
    .locals 1

    .prologue
    .line 2120
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDisplayHeight()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayHeight:I

    return v0
.end method

.method public getDisplayWidth()I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDisplayWidth:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 340
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    const/4 v0, 0x0

    .line 343
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->getId()I

    move-result v0

    goto :goto_0
.end method

.method public getMVMembersNumber()I
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mNumOfMVMembers:I

    .line 527
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->getMVMembersNumber()I

    move-result v0

    goto :goto_0
.end method

.method public getMVServerPosition()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    return v0
.end method

.method public getMasterVolumeValue()I
    .locals 3

    .prologue
    .line 764
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMasterVolumeValue mMasterVolume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    return v0
.end method

.method public getMeasuredHeightRatio()I
    .locals 1

    .prologue
    .line 353
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasuredHeightRatio:I

    .line 356
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->getMeasuredHeightRatio()I

    move-result v0

    goto :goto_0
.end method

.method public getPlayPosition()V
    .locals 3

    .prologue
    .line 851
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 852
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 853
    return-void
.end method

.method public getPlayerState()V
    .locals 3

    .prologue
    .line 856
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 857
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 858
    return-void
.end method

.method public getScreenOrientation()I
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public getSystemClockDelta()J
    .locals 2

    .prologue
    .line 497
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    const-wide/16 v0, 0x0

    .line 500
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->getSystemClockDelta()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getSystemTimeBase()J
    .locals 2

    .prologue
    .line 489
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J

    .line 492
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->getSystemTimeBase()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 385
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoHeight()I

    move-result v0

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 381
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoWidth()I

    move-result v0

    return v0
.end method

.method public getXdpi()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mXdpi:I

    return v0
.end method

.method public getYdpi()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mYdpi:I

    return v0
.end method

.method public hide()Z
    .locals 1

    .prologue
    .line 786
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->disableCpuCore()V

    .line 788
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->hide()Z

    move-result v0

    .line 791
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isMultiVisionMode()Z
    .locals 1

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNextAvailable()Z
    .locals 4

    .prologue
    .line 477
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->checkHide()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getBufferStatus()I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 480
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNextAvailable false, i = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const/4 v1, 0x0

    .line 485
    :goto_1
    return v1

    .line 477
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 484
    :cond_1
    const-string v1, "MVUtil"

    const-string v2, "isNextAvailable true"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isRequestMultiVision()Z
    .locals 1

    .prologue
    .line 316
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mIsRequestMultiVision:Z

    .line 319
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->isSupportMultiVision()Z

    move-result v0

    goto :goto_0
.end method

.method public isShareVideoMode()Z
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    return v0
.end method

.method public isSingleVisionMode()Z
    .locals 1

    .prologue
    .line 465
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    .line 468
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->isSingleVisionMode()Z

    move-result v0

    goto :goto_0
.end method

.method public isSupportMultiVision()Z
    .locals 1

    .prologue
    .line 308
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z

    .line 311
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->isSupportMultiVision()Z

    move-result v0

    goto :goto_0
.end method

.method public joinMVGroup()V
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSupportMultiVision()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->joinMVGroup_l(I)V

    .line 894
    :cond_0
    return-void
.end method

.method public notifyMessage(IIILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # Ljava/lang/String;

    .prologue
    .line 2107
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 2108
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 303
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 824
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 825
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 826
    return-void
.end method

.method public prepared(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 972
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 973
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 974
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 975
    return-void
.end method

.method public ready(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 978
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 979
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 980
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 981
    return-void
.end method

.method public requestMVJoinFromTouchMotion(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;)V
    .locals 3
    .param p1, "requestJoinInfo"    # Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;

    .prologue
    .line 885
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x14

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 887
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 888
    return-void
.end method

.method public resetMVPlayer()V
    .locals 3

    .prologue
    .line 574
    const-string v1, "MVUtil"

    const-string v2, "resetMVPlayer"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 577
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 578
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/16 v2, 0xbb8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->endConnection(I)V

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 581
    :cond_0
    const-string v1, "MVUtil"

    const-string v2, "deleted mMVPlayerList"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    .end local v0    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->resetShareVideoMode()V

    .line 585
    return-void
.end method

.method public seekTo(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 839
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 840
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 842
    return-void
.end method

.method public sendDeviceInfo()V
    .locals 5

    .prologue
    .line 1256
    const-string v3, "MVUtil"

    const-string v4, "sendDeviceInfo E"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    new-instance v2, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;

    invoke-direct {v2}, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;-><init>()V

    .line 1259
    .local v2, "list":Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 1260
    .local v0, "Cnt":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;->setDeviceInfoList(Ljava/util/List;)V

    .line 1262
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1263
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerServerList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V

    .line 1262
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1266
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyDeviceChangedToActivity()V

    .line 1267
    return-void
.end method

.method public sendPauseToServer()V
    .locals 1

    .prologue
    .line 939
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendPauseToServer()V

    .line 942
    :cond_0
    return-void
.end method

.method public sendPlayToServer()V
    .locals 1

    .prologue
    .line 946
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendPlayToServer()V

    .line 949
    :cond_0
    return-void
.end method

.method public sendPlayerState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 780
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendPlayerState(I)V

    .line 783
    :cond_0
    return-void
.end method

.method public sendTouchInfo(IJ)V
    .locals 2
    .param p1, "direction"    # I
    .param p2, "touchTime"    # J

    .prologue
    .line 505
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendClientMVRequest(IJ)V

    .line 508
    :cond_0
    return-void
.end method

.method public setAdjustLeftCropRatio(F)V
    .locals 0
    .param p1, "adjustLeftCropRatio"    # F

    .prologue
    .line 361
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustLeftCropRatio:F

    .line 362
    return-void
.end method

.method public setAdjustRightCropRatio(F)V
    .locals 0
    .param p1, "adjustRightCropRatio"    # F

    .prologue
    .line 369
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mAdjustRightCropRatio:F

    .line 370
    return-void
.end method

.method public setChangedRotation(Z)V
    .locals 0
    .param p1, "changedRotation"    # Z

    .prologue
    .line 404
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mChangedRotation:Z

    .line 405
    return-void
.end method

.method public setClientVolumeValue(II)Z
    .locals 4
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    const/16 v3, 0x12e

    const/16 v2, 0x12d

    .line 2149
    const/4 v0, 0x1

    .line 2150
    .local v0, "changeRequired":Z
    if-ne p1, v2, :cond_2

    .line 2151
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    if-ne v1, p2, :cond_0

    .line 2152
    const/4 v0, 0x0

    .line 2154
    :cond_0
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    .line 2165
    :cond_1
    :goto_0
    return v0

    .line 2155
    :cond_2
    const/16 v1, 0x12c

    if-ne p1, v1, :cond_3

    .line 2156
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 2157
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setVolumeValue(II)V

    .line 2158
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setVolumeValue(II)V

    goto :goto_0

    .line 2159
    :cond_3
    if-ne p1, v3, :cond_1

    .line 2160
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    if-ne v1, p2, :cond_4

    .line 2161
    const/4 v0, 0x0

    .line 2163
    :cond_4
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    goto :goto_0
.end method

.method public setDataSource()V
    .locals 3

    .prologue
    .line 818
    const-string v1, "MVUtil"

    const-string v2, "setDataSource E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 820
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 821
    return-void
.end method

.method public setMasterVolumeValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 861
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMasterVolumeValue value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 863
    return-void
.end method

.method public setMeasuredHeightRatio(I)V
    .locals 3
    .param p1, "measuredHeightRatio"    # I

    .prologue
    .line 348
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasuredHeightRatio:I

    .line 349
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Server] setMeasuredHeightRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMeasuredHeightRatio:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    return-void
.end method

.method public setMeasuredServerCropSize(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 389
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeft:I

    .line 390
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTop:I

    .line 391
    iput p3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRight:I

    .line 392
    iput p4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mBottom:I

    .line 393
    return-void
.end method

.method public setPlayPosition(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 845
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 846
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 847
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 848
    return-void
.end method

.method public setServerIP(Ljava/lang/String;)V
    .locals 2
    .param p1, "serverip"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerIP:Ljava/lang/String;

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->createContentURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mCurrentURL:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 211
    const-string v0, "MVUtil"

    const-string v1, "setServerIP failed : contentURL is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    return-void
.end method

.method public setShareVideoMode(Z)V
    .locals 0
    .param p1, "isShareVideo"    # Z

    .prologue
    .line 296
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z

    .line 297
    return-void
.end method

.method public setSingleVisionMode(Z)V
    .locals 1
    .param p1, "singleVisionMode"    # Z

    .prologue
    .line 436
    if-eqz p1, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopMVShakeMotion()V

    .line 442
    :goto_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSingleVisionMode:Z

    .line 445
    :cond_0
    return-void

    .line 439
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->startMVShakeMotion()V

    goto :goto_0
.end method

.method public setVideoCrop(IIII)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 532
    const-string v0, "MVUtil"

    const-string v1, "setVideoCrop"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/common/SUtils;->setVideoCrop(IIII)Z

    .line 534
    return-void
.end method

.method public setVolumeValue(II)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 866
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 867
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 868
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 869
    const/16 v1, 0x12c

    if-ne p1, v1, :cond_0

    .line 870
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    .line 878
    :goto_0
    const-string v1, "MVUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVolumeValue type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMasterVolume : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMasterVolume:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mLeftVolume : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mRightVolume : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 882
    return-void

    .line 871
    :cond_0
    const/16 v1, 0x12d

    if-ne p1, v1, :cond_1

    .line 872
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mLeftVolume:I

    goto :goto_0

    .line 873
    :cond_1
    const/16 v1, 0x12e

    if-ne p1, v1, :cond_2

    .line 874
    iput p2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mRightVolume:I

    goto :goto_0

    .line 876
    :cond_2
    const-string v1, "MVUtil"

    const-string v2, "Type Error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public show()Z
    .locals 1

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->enableCpuCore()V

    .line 804
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->show()Z

    move-result v0

    .line 807
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 829
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 830
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 831
    return-void
.end method

.method public startMVShakeMotion()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->start()V

    .line 419
    :cond_0
    return-void
.end method

.method public startSharePlay()Z
    .locals 3

    .prologue
    .line 932
    const-string v1, "MVUtil"

    const-string v2, "startSharePlay E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x19

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 934
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 935
    const/4 v1, 0x1

    return v1
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 834
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 835
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 836
    return-void
.end method

.method public stopConnectionThread()V
    .locals 3

    .prologue
    .line 1817
    const-string v1, "MVUtil"

    const-string v2, "stopConnectionThread E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1819
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mConnectionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1820
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mConnectionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1823
    :try_start_0
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1828
    :cond_0
    :goto_0
    return-void

    .line 1824
    :catch_0
    move-exception v0

    .line 1825
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopMVShakeMotion()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShakeMotion:Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->stop()V

    .line 425
    :cond_0
    return-void
.end method

.method public stopSharePlay()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 952
    const-string v1, "MVUtil"

    const-string v2, "stopSharePlay E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->disableCpuCore()V

    .line 955
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 956
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    const/16 v2, 0x1a

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 957
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVUtilHandler:Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->sendMessage(Landroid/os/Message;)Z

    .line 967
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return v3

    .line 960
    :cond_0
    const-string v1, "MVUtil"

    const-string v2, "stopSharePlay client finished"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setShareVideoMode(Z)V

    .line 962
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopMVShakeMotion()V

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->endConnection()V

    goto :goto_0
.end method

.method public updateDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V
    .locals 3
    .param p1, "list"    # Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;

    .prologue
    .line 537
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    const-string v1, "MVUtil"

    const-string v2, "updateDeviceInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;->getDeviceInfoList()Ljava/util/List;

    move-result-object v0

    .line 540
    .local v0, "infolist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;>;"
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 541
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mDeviceInfoList:Ljava/util/List;

    .line 543
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->notifyDeviceChangedToActivity()V

    .line 544
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->printDeviceInfo()V

    .line 546
    .end local v0    # "infolist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;>;"
    :cond_0
    return-void
.end method

.method public updateVolume(I)V
    .locals 2
    .param p1, "currentVolume"    # I

    .prologue
    .line 2169
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2170
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2171
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/16 v1, 0x12d

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->onVolumeChanged(II)V

    .line 2180
    :cond_0
    :goto_0
    return-void

    .line 2172
    :cond_1
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVServerPosition:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2173
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/16 v1, 0x12e

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->onVolumeChanged(II)V

    goto :goto_0

    .line 2176
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    if-eqz v0, :cond_0

    .line 2177
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendVolumetoMasterById(I)V

    goto :goto_0
.end method

.method public waitClientConnectionWithSetup()V
    .locals 2

    .prologue
    .line 1831
    const-string v0, "MVUtil"

    const-string v1, "waitClientConnectionWithSetup E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;-><init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mConnectionThread:Ljava/lang/Thread;

    .line 1892
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;->mConnectionThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1893
    return-void
.end method
