.class Lcom/sec/android/app/mv/player/activity/ShareVideo$35;
.super Landroid/database/ContentObserver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 3232
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 3234
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3243
    :cond_0
    :goto_0
    return-void

    .line 3235
    :cond_1
    const-string v0, "ShareVideo"

    const-string v1, "registerContentObserver mObserverMWTray onChange()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3236
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3237
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMWTrayOpen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3238
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    goto :goto_0

    .line 3240
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto :goto_0
.end method
