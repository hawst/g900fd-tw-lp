.class public Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefaceSortByName;
.super Ljava/lang/Object;
.source "TypefaceFinder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TypefaceSortByName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/mv/player/flipfont/Typeface;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/mv/player/flipfont/Typeface;Lcom/sec/android/app/mv/player/flipfont/Typeface;)I
    .locals 2
    .param p1, "o1"    # Lcom/sec/android/app/mv/player/flipfont/Typeface;
    .param p2, "o2"    # Lcom/sec/android/app/mv/player/flipfont/Typeface;

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 124
    check-cast p1, Lcom/sec/android/app/mv/player/flipfont/Typeface;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/mv/player/flipfont/Typeface;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefaceSortByName;->compare(Lcom/sec/android/app/mv/player/flipfont/Typeface;Lcom/sec/android/app/mv/player/flipfont/Typeface;)I

    move-result v0

    return v0
.end method
