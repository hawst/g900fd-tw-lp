.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;
.super Landroid/os/AsyncTask;
.source "VideoSubtitlePopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MakeSubtitleListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1205
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;

    .prologue
    .line 1205
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->makeSubtitleFilesList()Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1210
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1205
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "i"    # Ljava/lang/Boolean;

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 1217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2800(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 1218
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1205
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
