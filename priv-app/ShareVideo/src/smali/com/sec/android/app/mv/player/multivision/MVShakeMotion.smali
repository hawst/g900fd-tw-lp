.class public Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;
.super Ljava/lang/Object;
.source "MVShakeMotion.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final DATA_X:I = 0x0

.field private static final DATA_Y:I = 0x1

.field private static final DATA_Z:I = 0x2

.field private static final SHAKE_THRESHOLD:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "MVShakeMotion"


# instance fields
.field private accelerormeterSensor:Landroid/hardware/Sensor;

.field private lastTime:J

.field private lastX:F

.field private lastY:F

.field private lastZ:F

.field private mContext:Landroid/content/Context;

.field mIsRegistered:Z

.field private mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

.field private sensorManager:Landroid/hardware/SensorManager;

.field private speed:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "util"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    .line 47
    const-string v0, "MVShakeMotion"

    const-string v1, "[Server] MVShakeMotion sensor is create.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->init()V

    .line 53
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->sensorManager:Landroid/hardware/SensorManager;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->accelerormeterSensor:Landroid/hardware/Sensor;

    .line 59
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 80
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 84
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v4}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 86
    .local v0, "currentTime":J
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastTime:J

    sub-long v2, v0, v4

    .line 88
    .local v2, "gabOfTime":J
    const-wide/16 v4, 0x96

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 89
    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastTime:J

    .line 91
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v7

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->x:F

    .line 92
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->y:F

    .line 93
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->z:F

    .line 95
    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->x:F

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->y:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->z:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastX:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastY:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastZ:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    long-to-float v5, v2

    div-float/2addr v4, v5

    const v5, 0x461c4000    # 10000.0f

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->speed:F

    .line 97
    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->speed:F

    const/high16 v5, 0x447a0000    # 1000.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup()V

    .line 104
    :cond_0
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v7

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastX:F

    .line 105
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v6

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastY:F

    .line 106
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v8

    iput v4, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->lastZ:F

    .line 109
    .end local v0    # "currentTime":J
    .end local v2    # "gabOfTime":J
    :cond_1
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->accelerormeterSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    if-nez v0, :cond_0

    .line 63
    const-string v0, "MVShakeMotion"

    const-string v1, "MVShakeMotion sensor is started.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->accelerormeterSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 67
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->sensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 71
    const-string v0, "MVShakeMotion"

    const-string v1, "MVShakeMotion sensor is stopped.."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->mIsRegistered:Z

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVShakeMotion;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 75
    :cond_0
    return-void
.end method
