.class Lcom/sec/android/app/mv/player/activity/ShareVideo$8;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1635
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1637
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1638
    const-string v3, "ShareVideo"

    const-string v4, "mMVPlaylistShowInfoReceiver. This device is client side. so do not need process"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1641
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1642
    .local v0, "action":Ljava/lang/String;
    const-string v3, "command"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1643
    .local v1, "cmd":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1645
    .local v2, "message":Ljava/lang/String;
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMVPlaylistShowInfoReceiver - onReceive. cmd : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1647
    const-string v3, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST_SHOW_INFO"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1648
    const-string v3, "mv_enter_list"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1649
    const-string v2, "302%mv_enter_list%"

    .line 1655
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v3

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 1656
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 1657
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mMVPlaylistShowInfoReceiver. Send infomation to all client : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1660
    :cond_2
    return-void

    .line 1650
    :cond_3
    const-string v3, "mv_exit_list"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1651
    const-string v2, "302%mv_exit_list%"

    goto :goto_0
.end method
