.class public Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
.super Ljava/lang/Object;
.source "VideoLockCtrl.java"


# static fields
.field private static final HIDE_LOCK_ICON:I

.field private static mbLockMode:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mLockBtn:Landroid/widget/ImageButton;

.field private mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mLockCtrlView:Landroid/widget/RelativeLayout;

.field private mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field private mParentView:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mbLockMode:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    .line 50
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 89
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    .line 31
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 32
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/VideoLockCtrl;)Lcom/sec/android/app/mv/player/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    return-object v0
.end method

.method public static getLockState()Z
    .locals 1

    .prologue
    .line 106
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mbLockMode:Z

    return v0
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0092

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 48
    return-void
.end method

.method public static setLockState(Z)V
    .locals 0
    .param p0, "mode"    # Z

    .prologue
    .line 102
    sput-boolean p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mbLockMode:Z

    .line 103
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03001a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->initViews()V

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 42
    return-void
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 124
    :cond_1
    return-void
.end method

.method public hideLockIcon()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 87
    return-void
.end method

.method public isLockBtnVisible()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mParentView:Landroid/widget/RelativeLayout;

    .line 113
    return-void
.end method

.method public showLockIcon()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 73
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->getHeight()I

    move-result v0

    invoke-virtual {v1, v4, v0, v4, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 82
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->mLockCtrlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    goto :goto_0
.end method
