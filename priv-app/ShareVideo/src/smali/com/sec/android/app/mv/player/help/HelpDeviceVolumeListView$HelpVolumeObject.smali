.class Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
.super Ljava/lang/Object;
.source "HelpVolumeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelpVolumeObject"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)V

    return-void
.end method


# virtual methods
.method getCount()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$300(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getVolume(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 236
    const/4 v0, 0x0

    .line 238
    .local v0, "volume":I
    packed-switch p1, :pswitch_data_0

    .line 248
    const/4 v0, 0x5

    .line 252
    :goto_0
    return v0

    .line 240
    :pswitch_0
    const/4 v0, 0x5

    .line 241
    goto :goto_0

    .line 244
    :pswitch_1
    const/4 v0, 0x5

    .line 245
    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setVolume(II)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "volume"    # I

    .prologue
    .line 276
    return-void
.end method

.method setVolumeProgress(II)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "volume"    # I

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->getCount()I

    move-result v0

    .line 258
    .local v0, "count":I
    if-nez p1, :cond_1

    .line 260
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    .line 262
    .local v2, "seekBar":Landroid/widget/SeekBar;
    if-eqz v2, :cond_0

    .line 263
    invoke-virtual {v2, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 260
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    .end local v1    # "i":I
    .end local v2    # "seekBar":Landroid/widget/SeekBar;
    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_3

    .line 273
    :cond_2
    :goto_1
    return-void

    .line 268
    :cond_3
    const/4 v3, 0x2

    if-ne p1, v3, :cond_2

    goto :goto_1
.end method
