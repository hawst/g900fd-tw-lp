.class Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;
.super Landroid/widget/ArrayAdapter;
.source "PlaylistActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initCounterAdapters()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # [Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 151
    if-nez p2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 153
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000e

    invoke-virtual {v0, v2, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 155
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0d002d

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 156
    .local v1, "tv":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0082

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getSelectedListSize()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    return-object p2
.end method
