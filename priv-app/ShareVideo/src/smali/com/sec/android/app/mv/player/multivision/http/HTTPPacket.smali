.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
.super Ljava/lang/Object;
.source "HTTPPacket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;
    }
.end annotation


# instance fields
.field private mContent:[B

.field private mContentInput:Ljava/io/InputStream;

.field private mFileContent:Ljava/io/File;

.field private mFirstLine:Ljava/lang/String;

.field private mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

.field private mMBFileContent:Z

.field private mRand:Ljava/util/Random;

.field private mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

.field private final mTag:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    .line 163
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    .line 278
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mRand:Ljava/util/Random;

    .line 411
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 441
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    .line 533
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    .line 562
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 727
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    .line 841
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    .line 131
    const-string v0, "1.1"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setVersion(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 133
    return-void
.end method

.method public static isIPv6Address(Ljava/lang/String;)Z
    .locals 1
    .param p0, "host"    # Ljava/lang/String;

    .prologue
    .line 1064
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1065
    :cond_0
    const/4 v0, 0x0

    .line 1066
    :goto_0
    return v0

    :cond_1
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method private setFirstLine(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    .line 537
    return-void
.end method

.method public static final toLong(Ljava/lang/String;)J
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 701
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 705
    :goto_0
    return-wide v0

    .line 702
    :catch_0
    move-exception v0

    .line 705
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addHeader(Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;)V
    .locals 1
    .param p1, "header"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 573
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    return-void
.end method

.method public clearHeaders()V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 602
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 603
    return-void
.end method

.method public getConnection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 927
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 800
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 801
    const/4 v1, 0x0

    .line 805
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFileContent()Ljava/io/File;

    move-result-object v2

    .line 806
    .local v2, "tempFile":Ljava/io/File;
    if-nez v2, :cond_0

    move-object v1, v3

    .line 815
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :goto_0
    return-object v1

    .line 808
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "tempFile":Ljava/io/File;
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 809
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 810
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 811
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v5, "getContent  - Exception"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v3

    .line 812
    goto :goto_0

    .line 815
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method

.method public getContentInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    return-object v0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 902
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getLongHeaderValue(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getContentRange()[J
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 966
    const/4 v7, 0x3

    new-array v4, v7, [J

    .line 967
    .local v4, "range":[J
    const-wide/16 v8, 0x0

    aput-wide v8, v4, v12

    aput-wide v8, v4, v11

    aput-wide v8, v4, v10

    .line 968
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasContentRange()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1019
    :cond_0
    :goto_0
    return-object v4

    .line 970
    :cond_1
    const-string v7, "Content-Range"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 972
    .local v5, "rangeLine":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_2

    .line 973
    const-string v7, "Range"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 974
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 978
    const-string v7, "bytes"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 979
    const-string v7, "-1"

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v10

    goto :goto_0

    .line 984
    :cond_3
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, " ="

    invoke-direct {v6, v5, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    .local v6, "strToken":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 988
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 991
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 994
    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 995
    .local v1, "firstPosStr":Ljava/lang/String;
    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "="

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 996
    :cond_4
    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 999
    :cond_5
    const/4 v7, 0x0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1003
    :goto_1
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1005
    const-string v7, "-/"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1007
    .local v2, "lastPosStr":Ljava/lang/String;
    const/4 v7, 0x1

    :try_start_1
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1011
    :goto_2
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1013
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1015
    .local v3, "lengthStr":Ljava/lang/String;
    const/4 v7, 0x2

    :try_start_2
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 1016
    :catch_0
    move-exception v0

    .line 1017
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange  - numberFormatException cached(3)"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1000
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "lastPosStr":Ljava/lang/String;
    .end local v3    # "lengthStr":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1001
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange  - numberFormatException cached(1)...."

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1008
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "lastPosStr":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 1009
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange  - numberFormatException cached(2)"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public getContentRangeFirstPosition()J
    .locals 4

    .prologue
    .line 1023
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 1024
    .local v0, "range":[J
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    return-wide v2
.end method

.method public getContentRangeLastPosition()J
    .locals 4

    .prologue
    .line 1028
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 1029
    .local v0, "range":[J
    const/4 v1, 0x1

    aget-wide v2, v0, v1

    return-wide v2
.end method

.method getContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 822
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 823
    const-string v0, "This Packet Contains a file content, so can not be converted to String"

    .line 825
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public getCurrentReceviedContentLength()J
    .locals 2

    .prologue
    .line 906
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFileContent()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 911
    :goto_0
    return-wide v0

    .line 908
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    if-eqz v0, :cond_1

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    array-length v0, v0

    int-to-long v0, v0

    goto :goto_0

    .line 911
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getFileContent()Ljava/io/File;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    return-object v0
.end method

.method protected getFirstLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    return-object v0
.end method

.method protected getFirstLineToken(I)Ljava/lang/String;
    .locals 5
    .param p1, "num"    # I

    .prologue
    .line 544
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    const-string v4, " "

    invoke-direct {v2, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    .local v2, "st":Ljava/util/StringTokenizer;
    const-string v0, ""

    .line 546
    .local v0, "lastToken":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-gt v1, p1, :cond_0

    .line 547
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_1

    .line 548
    const-string v0, ""

    .line 551
    .end local v0    # "lastToken":Ljava/lang/String;
    :cond_0
    return-object v0

    .line 549
    .restart local v0    # "lastToken":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 546
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getHeader(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    .locals 5
    .param p1, "n"    # I

    .prologue
    .line 578
    const/4 v2, 0x0

    .line 580
    .local v2, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 584
    :goto_0
    return-object v2

    .line 581
    :catch_0
    move-exception v1

    .line 582
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v4, "getHeader - ArrayIndexOutOfBoundsException :"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getNHeaders()I

    move-result v3

    .line 589
    .local v3, "nHeaders":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 590
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 591
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-nez v0, :cond_1

    .line 589
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 593
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v1

    .line 594
    .local v1, "headerName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 597
    .end local v0    # "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    .end local v1    # "headerName":Ljava/lang/String;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getHeaderString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 713
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 714
    .local v3, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 715
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 716
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 717
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 718
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 715
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 720
    .end local v0    # "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 635
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 636
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 637
    const-string v1, ""

    .line 638
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHttpServer()Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    return-object v0
.end method

.method public getLongHeaderValue(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 693
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 694
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 695
    const-wide/16 v2, 0x0

    .line 696
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->toLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getNHeaders()I
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 672
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "startWidth"    # Ljava/lang/String;
    .param p3, "endWidth"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 661
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 662
    .local v0, "headerValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 663
    const/4 v1, 0x0

    .line 668
    :goto_0
    return-object v1

    .line 664
    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 665
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 666
    :cond_1
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 667
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    move-object v1, v0

    .line 668
    goto :goto_0
.end method

.method public getTransferEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method handleExpect100Continue()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 256
    const-string v3, "Expect"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 257
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "headerValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 260
    const-string v3, "100-continue"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentLength()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasEnoughStorage(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 262
    check-cast p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .end local p0    # "this":Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
    const/16 v2, 0x1a1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    .line 263
    const/4 v2, 0x0

    .line 271
    .end local v1    # "headerValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 265
    .restart local v1    # "headerValue":Ljava/lang/String;
    .restart local p0    # "this":Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
    :cond_1
    check-cast p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .end local p0    # "this":Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
    const/16 v3, 0x64

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->returnResponse(I)Z

    goto :goto_0
.end method

.method public hasConnection()Z
    .locals 1

    .prologue
    .line 919
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasContent()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 829
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 834
    :cond_0
    :goto_0
    return v0

    .line 831
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    if-eqz v2, :cond_2

    .line 832
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    array-length v2, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 834
    goto :goto_0
.end method

.method public hasContentInputStream()Z
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContentRange()Z
    .locals 1

    .prologue
    .line 953
    const-string v0, "Content-Range"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Range"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEnoughStorage(J)Z
    .locals 9
    .param p1, "size"    # J

    .prologue
    .line 881
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->getCacheDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 882
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v6, v3

    mul-long v0, v4, v6

    .line 883
    .local v0, "space":J
    cmp-long v3, v0, p1

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public hasFileContent()Z
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    return v0
.end method

.method public hasFirstLine()Z
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeader(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 606
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransferEncoding()Z
    .locals 1

    .prologue
    .line 1100
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 140
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->clearHeaders()V

    .line 142
    const/4 v0, 0x0

    new-array v0, v0, [B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 144
    return-void
.end method

.method public isChunked()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1113
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasTransferEncoding()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1118
    :cond_0
    :goto_0
    return v1

    .line 1115
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getTransferEncoding()Ljava/lang/String;

    move-result-object v0

    .line 1116
    .local v0, "transEnc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1118
    const-string v1, "Chunked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCloseConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 931
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 936
    :cond_0
    :goto_0
    return v1

    .line 933
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 934
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 936
    const-string v1, "close"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isFileStreamNeeded()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 870
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Android/data/com.sec.android.wimp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 872
    .local v0, "sdcard":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 876
    :cond_0
    :goto_0
    return v1

    .line 874
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->isChunked()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentLength()J

    move-result-wide v2

    const-wide/32 v4, 0x300000

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 875
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isKeepAliveConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 940
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 945
    :cond_0
    :goto_0
    return v1

    .line 942
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 943
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 945
    const-string v1, "Keep-Alive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHttpServer()Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHttpServer()Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->performReceivedListener(Ljava/lang/String;)V

    .line 409
    :cond_0
    return-void
.end method

.method public read(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 521
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 523
    :cond_0
    const/4 v0, 0x0

    .line 526
    :goto_0
    return v0

    .line 525
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->init()V

    .line 526
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->set(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)Z

    move-result v0

    goto :goto_0
.end method

.method readChunkLength(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)J
    .locals 4
    .param p1, "reader"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 361
    .local v0, "contentLen":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 364
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 368
    :cond_1
    return-wide v0
.end method

.method readContentInFile(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)V
    .locals 26
    .param p1, "reader"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->isChunked()Z

    move-result v13

    .line 282
    .local v13, "isChunkedRequest":Z
    const-wide/16 v8, 0x0

    .line 283
    .local v8, "contentLen":J
    if-nez p1, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_2

    .line 287
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->readChunkLength(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)J

    move-result-wide v8

    .line 292
    :goto_1
    const-wide/16 v24, 0x0

    cmp-long v23, v8, v24

    if-nez v23, :cond_3

    .line 293
    long-to-int v0, v8

    move/from16 v23, v0

    move/from16 v0, v23

    new-array v7, v0, [B

    .line 294
    .local v7, "content":[B
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 295
    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    goto :goto_0

    .line 290
    .end local v7    # "content":[B
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentLength()J

    move-result-wide v8

    goto :goto_1

    .line 303
    :cond_3
    const/4 v11, 0x0

    .line 306
    .local v11, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/http/HTTP;->getCacheDirectory()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mRand:Ljava/util/Random;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Random;->nextLong()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ".tmp"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 308
    .local v22, "tempFile":Ljava/io/File;
    new-instance v12, Ljava/io/FileOutputStream;

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .local v12, "fos":Ljava/io/FileOutputStream;
    const/16 v23, 0x1

    :try_start_1
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 311
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    .line 313
    const v6, 0x4b000

    .line 315
    .local v6, "chunkSize":I
    new-array v14, v6, [B

    .line 316
    .local v14, "readBuf":[B
    const-wide/16 v16, 0x0

    .line 318
    .local v16, "readCnt":J
    :goto_2
    const-wide/16 v24, 0x0

    cmp-long v23, v24, v8

    if-gez v23, :cond_a

    .line 319
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-static {v14, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 320
    const-wide/16 v16, 0x0

    .line 321
    :goto_3
    cmp-long v23, v16, v8

    if-gez v23, :cond_5

    .line 322
    sub-long v4, v8, v16

    .line 323
    .local v4, "bufReadLen":J
    int-to-long v0, v6

    move-wide/from16 v24, v0

    cmp-long v23, v24, v4

    if-gez v23, :cond_4

    .line 324
    int-to-long v4, v6

    .line 325
    :cond_4
    const/16 v23, 0x0

    long-to-int v0, v4

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v14, v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->read([BII)I

    move-result v15

    .line 326
    .local v15, "readLen":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 327
    if-gez v15, :cond_7

    .line 334
    .end local v4    # "bufReadLen":J
    .end local v15    # "readLen":I
    :cond_5
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_9

    .line 336
    const-wide/16 v20, 0x0

    .line 339
    .local v20, "skipLen":J
    :cond_6
    const-string v23, "\r\n"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    sub-long v24, v24, v20

    move-object/from16 v0, p1

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->skip(J)J

    move-result-wide v18

    .line 340
    .local v18, "skipCnt":J
    const-wide/16 v24, 0x0

    cmp-long v23, v18, v24

    if-gez v23, :cond_8

    .line 344
    :goto_4
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->readChunkLength(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)J

    move-result-wide v8

    .line 347
    goto :goto_2

    .line 329
    .end local v18    # "skipCnt":J
    .end local v20    # "skipLen":J
    .restart local v4    # "bufReadLen":J
    .restart local v15    # "readLen":I
    :cond_7
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v12, v14, v0, v15}, Ljava/io/FileOutputStream;->write([BII)V

    .line 330
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->flush()V

    .line 331
    int-to-long v0, v15

    move-wide/from16 v24, v0

    add-long v16, v16, v24

    .line 332
    goto :goto_3

    .line 342
    .end local v4    # "bufReadLen":J
    .end local v15    # "readLen":I
    .restart local v18    # "skipCnt":J
    .restart local v20    # "skipLen":J
    :cond_8
    add-long v20, v20, v18

    .line 343
    const-string v23, "\r\n"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    cmp-long v23, v20, v24

    if-ltz v23, :cond_6

    goto :goto_4

    .line 348
    .end local v18    # "skipCnt":J
    .end local v20    # "skipLen":J
    :cond_9
    const-wide/16 v8, 0x0

    goto/16 :goto_2

    .line 353
    :cond_a
    if-eqz v12, :cond_0

    .line 354
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 350
    .end local v6    # "chunkSize":I
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "readBuf":[B
    .end local v16    # "readCnt":J
    .end local v22    # "tempFile":Ljava/io/File;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v10

    .line 351
    .local v10, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_2
    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 353
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v23

    :goto_6
    if-eqz v11, :cond_b

    .line 354
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    :cond_b
    throw v23

    .line 353
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "tempFile":Ljava/io/File;
    :catchall_1
    move-exception v23

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 350
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v10

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5
.end method

.method readContentInMemory(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)V
    .locals 13
    .param p1, "reader"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getContentLength()J

    move-result-wide v4

    .line 376
    .local v4, "contentLen":J
    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-gez v9, :cond_0

    .line 377
    new-instance v9, Ljava/lang/NegativeArraySizeException;

    const-string v10, "Fail to create content buffer"

    invoke-direct {v9, v10}, Ljava/lang/NegativeArraySizeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 378
    :cond_0
    long-to-int v9, v4

    new-array v2, v9, [B

    .line 381
    .local v2, "content":[B
    const-wide/16 v6, 0x0

    .line 382
    .local v6, "readCnt":J
    :goto_0
    cmp-long v9, v6, v4

    if-gez v9, :cond_1

    .line 384
    sub-long v0, v4, v6

    .line 385
    .local v0, "bufReadLen":J
    long-to-int v9, v6

    long-to-int v10, v0

    :try_start_0
    invoke-virtual {p1, v2, v9, v10}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->read([BII)I

    move-result v8

    .line 386
    .local v8, "readLen":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 387
    if-gez v8, :cond_2

    .line 399
    .end local v0    # "bufReadLen":J
    .end local v8    # "readLen":I
    :cond_1
    :goto_1
    cmp-long v9, v4, v6

    if-eqz v9, :cond_3

    .line 400
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Fail to get All contents"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 389
    .restart local v0    # "bufReadLen":J
    .restart local v8    # "readLen":I
    :cond_2
    int-to-long v10, v8

    add-long/2addr v6, v10

    goto :goto_0

    .line 390
    .end local v8    # "readLen":I
    :catch_0
    move-exception v3

    .line 392
    .local v3, "e":Ljava/net/SocketTimeoutException;
    goto :goto_1

    .line 393
    .end local v3    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v3

    .line 394
    .local v3, "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v10, "readContentInMemory  Exception :"

    invoke-static {v9, v10, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 402
    .end local v0    # "bufReadLen":J
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_3
    iput-boolean v12, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 403
    invoke-virtual {p0, v2, v12}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    .line 404
    return-void
.end method

.method readHeaders(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)Z
    .locals 6
    .param p1, "reader"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 226
    if-nez p1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v3

    .line 228
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "firstLineOfPacket":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 232
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    .line 235
    .local v2, "headerLine":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 237
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 238
    new-instance v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    invoke-direct {v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;-><init>(Ljava/lang/String;)V

    .line 239
    .local v1, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->hasName()Z

    move-result v3

    if-ne v3, v4, :cond_2

    .line 240
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;)V

    .line 241
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    .line 242
    goto :goto_1

    .end local v1    # "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    :cond_3
    move v3, v4

    .line 244
    goto :goto_0
.end method

.method public removeContent()V
    .locals 2

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 425
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->removeTempFile()V

    .line 428
    :goto_0
    return-void

    .line 427
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    goto :goto_0
.end method

.method public removeTempFile()V
    .locals 3

    .prologue
    .line 431
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 434
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "fail to removeTempFile"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected set(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;)V
    .locals 4
    .param p1, "httpPacket"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;

    .prologue
    .line 502
    if-nez p1, :cond_0

    .line 513
    :goto_0
    return-void

    .line 504
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->clearHeaders()V

    .line 507
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 508
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 509
    invoke-virtual {p1, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 510
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->addHeader(Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;)V

    .line 508
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 512
    .end local v0    # "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;)V

    goto :goto_0
.end method

.method protected set(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 498
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->set(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 491
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->set(Ljava/io/InputStream;Z)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;Z)Z
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "onlyHeaders"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 445
    if-nez p1, :cond_1

    .line 487
    :cond_0
    :goto_0
    return v1

    .line 447
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    if-nez v3, :cond_2

    .line 448
    new-instance v3, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;-><init>(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    .line 452
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->readHeaders(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 455
    if-ne p2, v2, :cond_3

    .line 456
    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    move v1, v2

    .line 457
    goto :goto_0

    .line 450
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v4, "Something Left. Read input stream again!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 472
    :catch_0
    move-exception v0

    .line 474
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->removeTempFile()V

    .line 475
    new-instance v1, Ljava/net/SocketTimeoutException;

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 461
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->handleExpect100Continue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->isFileStreamNeeded()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 469
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->readContentInFile(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)V

    :goto_2
    move v1, v2

    .line 487
    goto :goto_0

    .line 471
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mReader:Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->readContentInMemory(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket$DualReader;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 476
    :catch_1
    move-exception v0

    .line 478
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v3, "set  -IOException "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->removeTempFile()V

    goto :goto_0

    .line 481
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 484
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->removeTempFile()V

    goto :goto_0
.end method

.method public setCacheControl(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1038
    const-string v0, "max-age"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setCacheControl(Ljava/lang/String;I)V

    .line 1039
    return-void
.end method

.method public setCacheControl(Ljava/lang/String;I)V
    .locals 3
    .param p1, "directive"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1033
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1034
    .local v0, "strVal":Ljava/lang/String;
    const-string v1, "Cache-Control"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    return-void
.end method

.method public setConnection(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 923
    const-string v0, "Connection"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method public setContent(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;)V
    .locals 2
    .param p1, "packet"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;

    .prologue
    .line 737
    if-nez p1, :cond_0

    .line 743
    :goto_0
    return-void

    .line 739
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 740
    iget-object v0, p1, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    goto :goto_0

    .line 742
    :cond_1
    iget-object v0, p1, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent(Ljava/io/File;)V

    goto :goto_0
.end method

.method public setContent(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 750
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->removeTempFile()V

    .line 751
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mFileContent:Ljava/io/File;

    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 753
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    .line 754
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 772
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    .line 773
    return-void
.end method

.method public setContent(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 757
    if-eqz p1, :cond_0

    .line 760
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 769
    :cond_0
    :goto_0
    return-void

    .line 761
    :catch_0
    move-exception v0

    .line 762
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    .line 763
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "setContent  UnsupportedEncodingException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 765
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 766
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "setContent  NullPointerException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setContent([B)V
    .locals 1
    .param p1, "content"    # [B

    .prologue
    .line 746
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContent([BZ)V

    .line 747
    return-void
.end method

.method public setContent([BZ)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 730
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContent:[B

    .line 731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mMBFileContent:Z

    .line 732
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_0

    .line 733
    array-length v0, p1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setContentLength(J)V

    .line 734
    :cond_0
    return-void
.end method

.method public setContentInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 844
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    .line 845
    return-void
.end method

.method public setContentLength(J)V
    .locals 1
    .param p1, "len"    # J

    .prologue
    .line 898
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setLongHeader(Ljava/lang/String;J)V

    .line 899
    return-void
.end method

.method public setContentRange(JJJ)V
    .locals 7
    .param p1, "firstPos"    # J
    .param p3, "lastPos"    # J
    .param p5, "length"    # J

    .prologue
    .line 957
    const-string v0, ""

    .line 958
    .local v0, "rangeStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bytes "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 959
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 960
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 961
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v4, 0x0

    cmp-long v1, v4, p5

    if-gez v1, :cond_0

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 962
    const-string v1, "Content-Range"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    return-void

    .line 961
    :cond_0
    const-string v1, "*"

    goto :goto_0
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 860
    const-string v0, "Content-Type"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 3
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 1086
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/Date;

    invoke-direct {v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/Date;-><init>(Ljava/util/Calendar;)V

    .line 1087
    .local v0, "date":Lcom/sec/android/app/mv/player/multivision/http/Date;
    const-string v1, "Date"

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/Date;->getDateString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    return-void
.end method

.method public setHeader(Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;)V
    .locals 2
    .param p1, "header"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    .prologue
    .line 629
    if-nez p1, :cond_0

    .line 632
    :goto_0
    return-void

    .line 631
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 610
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 611
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 612
    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    .line 616
    :goto_0
    return-void

    .line 615
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;I)V
    .locals 4
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1070
    move-object v0, p1

    .line 1071
    .local v0, "hostAddr":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->isIPv6Address(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1072
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1073
    :cond_0
    const-string v1, "HOST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    return-void
.end method

.method public setHttpServer(Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;)V
    .locals 0
    .param p1, "server"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    .line 167
    return-void
.end method

.method public setLongHeader(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 681
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    return-void
.end method

.method public setServer(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1051
    const-string v0, "Server"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 657
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "startWidth"    # Ljava/lang/String;
    .param p4, "endWidth"    # Ljava/lang/String;

    .prologue
    .line 646
    if-nez p2, :cond_0

    .line 654
    :goto_0
    return-void

    .line 648
    :cond_0
    move-object v0, p2

    .line 649
    .local v0, "headerValue":Ljava/lang/String;
    invoke-virtual {v0, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 650
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 651
    :cond_1
    invoke-virtual {v0, p4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 653
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "ver"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;->mVersion:Ljava/lang/String;

    .line 154
    return-void
.end method
