.class public Lcom/sec/android/app/mv/player/common/Const;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/common/Const$GUIMsg;,
        Lcom/sec/android/app/mv/player/common/Const$Type;,
        Lcom/sec/android/app/mv/player/common/Const$Msg;
    }
.end annotation


# static fields
.field public static final CHARACTER_SPACE:Ljava/lang/String; = " "

.field public static final CLIENT_PAUSED:I = 0x3

.field public static final CLIENT_PLAY:I = 0x4

.field public static final FALSE:I = 0x0

.field public static final GAME_ACTION_EXTRA_GAME_NAME:Ljava/lang/String; = "GAME NAME"

.field public static final GAME_ACTION_GAMER_JOINED:Ljava/lang/String; = "GROUPPLAY GAMER JOINED"

.field public static final GAME_ACTION_GAMER_LEFT:Ljava/lang/String; = "GROUPPLAY GAMER LEFT"

.field public static final HOVER_MODE_AUTO:I = 0x2

.field public static final HOVER_MODE_FINGER:I = 0x1

.field public static final HOVER_MODE_PEN:I = 0x0

.field public static final SCREEN_AUTO_BRIGHTNESS_DETAIL:Ljava/lang/String; = "auto_brightness_detail"

.field public static final SERVICE_MINI_PLAYER:Ljava/lang/String; = "com.sec.android.app.videoplayer.miniapp.MiniVideoPlayerService"

.field public static final SYSTEM_UI_FLAG_REMOVE_NAVIGATION:Ljava/lang/String; = "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

.field public static final TRUE:I = 0x1

.field public static final UNDEFINED:I = -0x1

.field public static final VIDEO_PLAYBACK_PROGRESS_BAR_STYLE:I = 0x1010289


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method
