.class Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "HelpVolumeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelpDeviceAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 334
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 288
    iput-object p2, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->mContext:Landroid/content/Context;

    .line 289
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$300(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 298
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 303
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "index"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "group"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x1

    .line 308
    sget-object v5, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ---- getView: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    if-nez p2, :cond_0

    .line 310
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 311
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030003

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 315
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v5, 0x7f0d000a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 317
    .local v1, "deviceVolumeName":Landroid/widget/TextView;
    const v5, 0x7f0d000b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    .line 319
    .local v4, "seekBar":Landroid/widget/SeekBar;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # invokes: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->getDeviceName(I)Ljava/lang/String;
    invoke-static {v5, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$400(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;I)Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "deviceName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # invokes: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->getSpkStrPos(I)Ljava/lang/String;
    invoke-static {v5, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$500(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;I)Ljava/lang/String;

    move-result-object v3

    .line 321
    .local v3, "pos":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 324
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 325
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->getVolume(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 326
    sget-boolean v5, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v5, :cond_1

    sget-boolean v5, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v5, :cond_1

    .line 327
    invoke-virtual {v4, v8}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 328
    invoke-virtual {v4, v8}, Landroid/widget/SeekBar;->setFocusableInTouchMode(Z)V

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020072

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 331
    :cond_1
    return-object p2
.end method
