.class Lcom/sec/android/app/mv/player/view/VideoBtnController$11;
.super Landroid/os/Handler;
.source "VideoBtnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbResume:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1043
    :cond_0
    :goto_0
    return-void

    .line 1018
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1020
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbProgressDragStatus:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutShow:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mbResume:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgress()I

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1024
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 1026
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1031
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->openMVPlaylist()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    goto :goto_0

    .line 1036
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1037
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$11;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->openPlayerList()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V

    goto :goto_0

    .line 1018
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
