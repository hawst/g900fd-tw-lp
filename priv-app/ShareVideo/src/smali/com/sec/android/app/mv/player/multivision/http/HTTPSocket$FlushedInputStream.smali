.class Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;
.super Ljava/io/FilterInputStream;
.source "HTTPSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FlushedInputStream"
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 330
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 331
    return-void
.end method


# virtual methods
.method public skip(J)J
    .locals 15
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    const-wide/16 v8, 0x0

    .line 336
    .local v8, "totalBytesSkipped":J
    const/high16 v1, 0x800000

    .line 337
    .local v1, "bufferSize":I
    new-array v0, v1, [B

    .line 338
    .local v0, "buffer":[B
    :goto_0
    cmp-long v10, v8, p1

    if-gez v10, :cond_3

    .line 339
    const-wide/16 v4, -0x1

    .line 340
    .local v4, "bytesSkipped":J
    const-wide/32 v10, 0x7fffffff

    cmp-long v10, p1, v10

    if-lez v10, :cond_4

    .line 341
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;->in:Ljava/io/InputStream;

    const-wide/32 v12, 0x7fffffff

    invoke-virtual {v10, v12, v13}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v4

    .line 342
    sub-long v10, p1, v4

    int-to-long v12, v1

    div-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long v6, v10, v12

    .line 343
    .local v6, "count":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    int-to-long v10, v3

    cmp-long v10, v10, v6

    if-gez v10, :cond_0

    .line 344
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->read([B)I

    move-result v10

    int-to-long v10, v10

    add-long/2addr v4, v10

    .line 343
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 346
    :cond_0
    const-wide/16 v6, 0x0

    .line 347
    :cond_1
    cmp-long v10, v4, p1

    if-gez v10, :cond_2

    .line 348
    sub-long v10, p1, v4

    long-to-int v10, v10

    new-array v0, v10, [B

    .line 349
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v10, v0}, Ljava/io/InputStream;->read([B)I

    move-result v10

    int-to-long v10, v10

    add-long/2addr v4, v10

    .line 350
    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    .line 351
    const-wide/16 v10, 0x3

    cmp-long v10, v6, v10

    if-lez v10, :cond_1

    .line 357
    .end local v3    # "i":I
    .end local v6    # "count":J
    :cond_2
    :goto_2
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-nez v10, :cond_6

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;->read()I

    move-result v2

    .line 359
    .local v2, "byteOne":I
    if-gez v2, :cond_5

    .line 367
    .end local v2    # "byteOne":I
    .end local v4    # "bytesSkipped":J
    :cond_3
    return-wide v8

    .line 355
    .restart local v4    # "bytesSkipped":J
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket$FlushedInputStream;->in:Ljava/io/InputStream;

    sub-long v12, p1, v8

    invoke-virtual {v10, v12, v13}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v4

    goto :goto_2

    .line 362
    .restart local v2    # "byteOne":I
    :cond_5
    const-wide/16 v4, 0x1

    .line 365
    .end local v2    # "byteOne":I
    :cond_6
    add-long/2addr v8, v4

    .line 366
    goto :goto_0
.end method
