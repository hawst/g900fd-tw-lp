.class Lcom/sec/android/app/mv/player/view/VideoBtnController$21;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1795
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1797
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 1798
    .local v1, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 1800
    .local v2, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1801
    .local v0, "action":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionX:F
    invoke-static {v3, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3202(Lcom/sec/android/app/mv/player/view/VideoBtnController;F)F

    .line 1802
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoBtnSeekBarPosionY:F
    invoke-static {v3, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3302(Lcom/sec/android/app/mv/player/view/VideoBtnController;F)F

    .line 1804
    packed-switch v0, :pswitch_data_0

    .line 1820
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I

    .line 1823
    const/4 v3, 0x0

    return v3

    .line 1807
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 1808
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->showScrubbingSpeedInfoToast()V

    goto :goto_0

    .line 1814
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$21;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->cancelScrubbingSpeedInfoToast()V

    goto :goto_0

    .line 1804
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
