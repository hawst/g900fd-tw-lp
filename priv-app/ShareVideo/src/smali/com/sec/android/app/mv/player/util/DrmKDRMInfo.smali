.class public Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;
.super Ljava/lang/Object;
.source "DrmKDRMInfo.java"


# static fields
.field public static final DRM_TPE_INVALID_TERUTEN:I = 0x4

.field public static final DRM_TYPE_MAX:I = 0x5

.field public static final DRM_TYPE_NED:I = 0x2

.field public static final DRM_TYPE_NOMEDIA:I = 0x3

.field public static final DRM_TYPE_NORMAL:I = 0x0

.field public static final DRM_TYPE_TERUTEN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DrmKDRMInfo"

.field private static bLmsStart:Z


# instance fields
.field public mAvailCount:I

.field private mDisableCapture:Z

.field private mDisableTVOut:Z

.field private mDrmPlaybackStatus:I

.field private mDrmType:I

.field private mTotalCount:I

.field private mWatermark:[B

.field private mWatermarkFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    .line 124
    const-string v0, "kordrm-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->nativeInit()V

    .line 126
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDisableTVOut:Z

    .line 7
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDisableCapture:Z

    .line 8
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mWatermarkFlag:Z

    .line 10
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mWatermark:[B

    .line 11
    iput v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mAvailCount:I

    .line 12
    iput v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mTotalCount:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDrmPlaybackStatus:I

    .line 27
    iput v1, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDrmType:I

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->openFile(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static native DiscountCall(Ljava/lang/String;)V
.end method

.method public static native LMSClose(I)V
.end method

.method public static native LMSOpen()V
.end method

.method public static native LMSPlay(I)V
.end method

.method public static native LMSStop(I)V
.end method

.method public static LogLmsClose(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 80
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LogLmsClose=====================]] ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 83
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LMSClose(I)V

    .line 84
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    .line 86
    :cond_0
    return-void
.end method

.method public static LogLmsOpen()V
    .locals 2

    .prologue
    .line 72
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    .line 74
    const-string v0, "DrmKDRMInfo"

    const-string v1, "LogLmsOpen[[====================="

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LMSOpen()V

    .line 77
    :cond_0
    return-void
.end method

.method public static LogLmsPlay(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 89
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LMSPlay bLmsStart ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 91
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LMSPlay(I)V

    .line 93
    :cond_0
    return-void
.end method

.method public static LogLmsStop(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 96
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LogLmsStop bLmsStart ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 98
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LMSStop(I)V

    .line 100
    :cond_0
    return-void
.end method

.method private static native nativeInit()V
.end method

.method private native openFile(Ljava/lang/String;)V
.end method


# virtual methods
.method public native checkCID()I
.end method

.method public getAvailableCount()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mAvailCount:I

    return v0
.end method

.method public getDisableCapture()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDisableCapture:Z

    return v0
.end method

.method public getDisableTVOut()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDisableTVOut:Z

    return v0
.end method

.method public getDrmPlaybackStatus()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDrmPlaybackStatus:I

    return v0
.end method

.method public getDrmType()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mDrmType:I

    return v0
.end method

.method public native getTestCount(Ljava/lang/String;)I
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mTotalCount:I

    return v0
.end method

.method public getWatermark()[B
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mWatermark:[B

    return-object v0
.end method

.method public getWatermarkFlag()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->mWatermarkFlag:Z

    return v0
.end method
