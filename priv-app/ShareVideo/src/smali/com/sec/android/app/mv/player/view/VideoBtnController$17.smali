.class Lcom/sec/android/app/mv/player/view/VideoBtnController$17;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1355
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v4, 0x0

    .line 1357
    const/4 v1, 0x0

    .line 1358
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1359
    sparse-switch p2, :sswitch_data_0

    .line 1389
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$600(Lcom/sec/android/app/mv/player/view/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1390
    .local v0, "isReturn":Z
    if-eqz v0, :cond_2

    .line 1408
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 1362
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1365
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1366
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1302(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J

    .line 1367
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1370
    :cond_1
    const-string v2, "VideoPlayerBtnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_DOWN : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1374
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mDownKeyPressTime:J
    invoke-static {v2, v4, v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1302(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J

    .line 1376
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 1377
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1378
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1380
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1393
    .restart local v0    # "isReturn":Z
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 1395
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1396
    goto/16 :goto_0

    .line 1399
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$17;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1400
    goto/16 :goto_0

    .line 1359
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 1362
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1393
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
