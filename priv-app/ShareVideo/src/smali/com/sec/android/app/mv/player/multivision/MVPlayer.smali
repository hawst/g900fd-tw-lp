.class public abstract Lcom/sec/android/app/mv/player/multivision/MVPlayer;
.super Ljava/lang/Object;
.source "MVPlayer.java"


# static fields
.field public static final BUFFERING:I = 0x8

.field public static final COMPLETE:I = 0x9

.field public static final ERROR:I = 0xa

.field public static final ERRORMSG:I = -0x1

.field public static final EXIT_MV_GROUP:I = 0x10

.field public static final FINISHED:I = 0x13

.field public static final GET_PLAYER_STATE:I = 0x7

.field public static final GET_PLAY_POSITION:I = 0x6

.field public static final GET_VOLUME_VALUE:I = 0x1d

.field public static final HIDE:I = 0x17

.field public static final IDLE:I = 0x1

.field public static final JOIN_MV_GROUP:I = 0xf

.field public static final LEFT:I = 0x0

.field public static final LEFT_AUDIO:I = 0x1

.field public static final MSG_MAXIMUM_DEVICE_CONNECTED:I = 0xbb9

.field public static final MSG_SHARED_DEVICE_EXIT:I = 0xbb8

.field public static final MVPlayerLog:[Ljava/lang/String;

.field public static final NORMAL_AUDIO:I = 0x0

.field public static final PAUSE:I = 0x1

.field public static final PAUSED:I = 0x4

.field public static final PAUSE_SERVER:I = 0x22

.field public static final PLAY:I = 0x0

.field public static final PLAYED:I = 0x3

.field public static final PLAY_SERVER:I = 0x23

.field public static final PREPARED:I = 0x2

.field public static final READY:I = 0x7

.field public static final RESPONSE:I = 0xe

.field public static final RESUME:I = 0x3

.field public static final RESUMED:I = 0x6

.field public static final RIGHT:I = 0x1

.field public static final RIGHT_AUDIO:I = 0x2

.field public static final SEEK:I = 0x4

.field public static final SEND_CLIENT_DISCONNECT:I = 0xd

.field public static final SEND_CLIENT_INITIAL_INFO:I = 0x16

.field public static final SEND_CLIENT_MVREQ:I = 0x11

.field public static final SEND_CONTENT_URL:I = 0x12

.field public static final SEND_DEVICE_INFO:I = 0x15

.field public static final SEND_KEEP_ALIVE:I = 0x1f

.field public static final SEND_MASTER_CLOCK:I = 0x8

.field public static final SEND_MULTIVISION_DEVICE_DISCONNECTED:I = 0x20

.field public static final SEND_PLAYER_STATE:I = 0x9

.field public static final SEND_VOLUME_VALUE:I = 0x1c

.field public static final SET_AUDIO_CHANNEL:I = 0x19

.field public static final SET_DATA_SOURCE:I = 0x14

.field public static final SET_MULTIVISION_SIZE:I = 0xb

.field public static final SET_PLAY_POSITION:I = 0x5

.field public static final SET_SERVER_INITIAL_INFO:I = 0xa

.field public static final SET_SINGLEVISION_SIZE:I = 0xc

.field public static final SET_VOLUME_MUTE:I = 0x1a

.field public static final SET_VOLUME_ON:I = 0x1b

.field public static final SHOW:I = 0x18

.field public static final STOP:I = 0x2

.field public static final STOPPED:I = 0x5

.field public static final UNKNOWN:I = 0x0

.field public static final UPDATE_VOLUME_BY_ID:I = 0x21


# instance fields
.field protected mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

.field protected mID:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/16 v0, 0x24

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PLAY"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "PAUSE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "STOP"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "RESUME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SEEK"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SET_PLAY_POSITION"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GET_PLAY_POSITION"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GET_PLAYER_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SEND_MASTER_CLOCK"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SEND_PLAYER_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SET_SERVER_INITIAL_INFO"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SET_MULTIVISION_SIZE"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SET_SINGLEVISION_SIZE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SEND_CLIENT_DISCONNECT"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "RESPONSE"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "JOIN_MV_GROUP"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "EXIT_MV_GROUP"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SEND_CLIENT_MVREQ"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "SEND_CONTENT_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "FINISHED"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SET_DATA_SOURCE"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SEND_DEVICE_INFO"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SEND_CLIENT_INITIAL_INFO"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HIDE"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SHOW"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "SET_AUDIO_CHANNEL"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "SET_VOLUME_MUTE"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "SET_VOLUME_ON"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "SEND_VOLUME_VALUE"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "GET_VOLUME_VALUE"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "30"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "SEND_KEEP_ALIVE"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "SEND_MULTIVISION_DEVICE_DISCONNECTED"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "UPDATE_VOLUME_BY_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "PAUSE_SERVER"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "PLAY_SERVER"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/MVPlayer;->MVPlayerLog:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayer;->mID:I

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayer;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    return-void
.end method
