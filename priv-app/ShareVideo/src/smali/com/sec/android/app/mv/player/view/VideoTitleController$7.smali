.class Lcom/sec/android/app/mv/player/view/VideoTitleController$7;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    .line 729
    if-eqz p3, :cond_0

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$300(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->setVolume(I)Z

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeBtnPopup()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$500(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$600(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 734
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 738
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    .line 743
    return-void
.end method
