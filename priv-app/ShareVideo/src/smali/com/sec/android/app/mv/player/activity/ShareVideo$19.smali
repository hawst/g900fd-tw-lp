.class Lcom/sec/android/app/mv/player/activity/ShareVideo$19;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2456
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x3

    .line 2458
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2460
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2461
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_0

    .line 2462
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 2464
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/storage/extSdCard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2465
    const-string v2, "ShareVideo"

    const-string v3, "mMediaReceiver - Intent.ACTION_MEDIA_UNMOUNTED."

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a007b

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2467
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2468
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2472
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method
