.class Lcom/sec/android/app/mv/player/view/VideoTitleController$5;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0

    .prologue
    .line 636
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 638
    const/4 v1, 0x0

    .line 639
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$000(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 642
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$400(Lcom/sec/android/app/mv/player/view/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 643
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 661
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 646
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 648
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 649
    goto :goto_0

    .line 652
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;->this$0:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 653
    goto :goto_0

    .line 646
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
