.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
.super Ljava/lang/Object;
.source "HTTPServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final NAME:Ljava/lang/String; = "WIMPServer"

.field public static final VERSION:Ljava/lang/String; = "1.1"


# instance fields
.field private mAcceptSocketList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/net/Socket;",
            ">;>;"
        }
    .end annotation
.end field

.field private mBindPort:I

.field private mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpServerThread:Ljava/lang/Thread;

.field private mServerSock:Ljava/net/ServerSocket;

.field private final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    .line 211
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 266
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 96
    return-void
.end method

.method public static getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 85
    const-string v2, "os.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "osName":Ljava/lang/String;
    const-string v2, "os.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "osVer":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "WIMPServer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public accept()Ljava/net/Socket;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "accept E"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v2, 0x0

    .line 171
    .local v2, "sock":Ljava/net/Socket;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 172
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "accept. mServerSock is null or closed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :goto_0
    return-object v3

    .line 177
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 178
    const v4, 0x88b8

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 179
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setReceiveBufferSize(I)V

    .line 180
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "accept :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/net/Socket;->getSendBufferSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v2

    .line 183
    goto :goto_0

    .line 184
    :catch_0
    move-exception v1

    .line 185
    .local v1, "e1":Ljava/net/SocketTimeoutException;
    goto :goto_0

    .line 186
    .end local v1    # "e1":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v2, :cond_2

    .line 189
    :try_start_1
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 194
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "accept Exception :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 190
    :catch_2
    move-exception v1

    .line 191
    .local v1, "e1":Ljava/io/IOException;
    move-object v0, v1

    goto :goto_1
.end method

.method public addReceivedListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;

    .prologue
    .line 243
    return-void
.end method

.method public addRequestListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    return-void
.end method

.method public close()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v4, "close E"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-nez v3, :cond_0

    .line 161
    :goto_0
    return v1

    .line 153
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 159
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v3, "Close  - Exception :"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 159
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    move v1, v2

    goto :goto_0

    .line 158
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 159
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    throw v1
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public open(I)Z
    .locals 5
    .param p1, "port"    # I

    .prologue
    const/4 v1, 0x1

    .line 120
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "open E. port : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v3, "open. mServerSock is not null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :goto_0
    return v1

    .line 128
    :cond_0
    :try_start_0
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    .line 132
    new-instance v2, Ljava/net/ServerSocket;

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    const/16 v4, 0x200

    invoke-direct {v2, v3, v4}, Ljava/net/ServerSocket;-><init>(II)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    const v3, 0x88b8

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v2, "Open - IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 137
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 260
    return-void
.end method

.method public performRequestListener(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .prologue
    .line 231
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    .line 232
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;

    .line 234
    .local v0, "listener":Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;
    invoke-interface {v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;->httpRequestRecieved(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V

    .line 232
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 236
    .end local v0    # "listener":Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;
    :cond_0
    return-void
.end method

.method public removeReceivedListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;

    .prologue
    .line 247
    return-void
.end method

.method public removeRequestListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpRequestListenerList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->isOpened()Z

    move-result v4

    if-nez v4, :cond_0

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "mHttpServerThread run. isOpend is false"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    return-void

    .line 277
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 278
    .local v1, "exeutor":Ljava/util/concurrent/ExecutorService;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    .line 281
    .local v3, "thisThread":Ljava/lang/Thread;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "mHttpServerThread-http server started!!!!"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 284
    :cond_1
    :goto_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    if-ne v4, v3, :cond_5

    .line 285
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    const/4 v2, 0x0

    .line 289
    .local v2, "sock":Ljava/net/Socket;
    :goto_2
    :try_start_2
    sget v4, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    const/16 v6, 0x64

    if-le v4, v6, :cond_3

    .line 290
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v2, :cond_2

    .line 314
    :try_start_3
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 318
    :cond_2
    :goto_3
    :try_start_4
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "run - IOException"

    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 322
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "sock":Ljava/net/Socket;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 323
    :catch_1
    move-exception v0

    .line 324
    .restart local v0    # "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "run - Exception on mHttpServerThread"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 327
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 328
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "mHttpServerThread-http server stopped!!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 293
    .restart local v2    # "sock":Ljava/net/Socket;
    :cond_3
    :try_start_6
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 294
    :try_start_7
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 295
    if-eqz v2, :cond_4

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    new-instance v7, Ljava/lang/ref/WeakReference;

    invoke-direct {v7, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v4, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;

    invoke-direct {v4, p0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;-><init>(Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;Ljava/net/Socket;)V

    invoke-interface {v1, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 305
    sget v4, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    .line 307
    :cond_4
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 309
    :try_start_8
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->isOpened()Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 322
    .end local v2    # "sock":Ljava/net/Socket;
    :cond_5
    :try_start_9
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_4

    .line 307
    .restart local v2    # "sock":Ljava/net/Socket;
    :catchall_1
    move-exception v4

    :try_start_a
    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v4
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 315
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v4

    goto :goto_3
.end method

.method public declared-synchronized start()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 333
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v1, "start E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP Accept Thread:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mBindPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    monitor-exit p0

    return v3

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()Z
    .locals 7

    .prologue
    .line 344
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "stop E"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->close()Z

    .line 349
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 354
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 355
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357
    .local v3, "sockRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/net/Socket;>;"
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/Socket;

    .line 358
    .local v2, "sock":Ljava/net/Socket;
    if-eqz v2, :cond_0

    .line 359
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 361
    .end local v2    # "sock":Ljava/net/Socket;
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "Error: socket - IOException"

    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 367
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "sockRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/net/Socket;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 344
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 366
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_5
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 367
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 369
    const/4 v4, 0x1

    monitor-exit p0

    return v4
.end method
