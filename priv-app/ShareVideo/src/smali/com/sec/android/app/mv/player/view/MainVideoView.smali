.class public Lcom/sec/android/app/mv/player/view/MainVideoView;
.super Landroid/widget/RelativeLayout;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    }
.end annotation


# static fields
.field private static final BACK_KEY_EXPIRE_TIMEOUT:I = 0xa28

.field protected static final DEFAULT_TIME_OUT:I = 0xbb8

.field private static final FADE_OUT:I = 0x1

.field public static final FILL_SCREEN_RATIO:I = 0x2

.field public static final FIT_TO_HEIGHT_RATIO:I = 0x1

.field private static final HANDLE_BACK_KEY:I = 0x3

.field private static final HIDE_SYSTEM_UI:I = 0x2

.field public static final KEEP_ASPECT_RATIO:I = 0x0

.field private static final LONG_PRESS_TIME:J = 0x1f4L

.field private static final MSG_FROM_HANDLER:I = 0x0

.field private static final MSG_FROM_USER:I = 0x1

.field public static final ORIGINAL_SIZE:I = 0x3

.field public static final SCREEN_MODE_DEFAULT:I = 0x0

.field public static final SCREEN_MODE_END:I = 0x3

.field public static final SCREEN_MODE_START:I = 0x0

.field private static final SET_MULTIVISION_SCREEN:I = 0xa

.field private static final START_GESTURE:I = 0x4

.field private static final SWIPE_MAX_Y_DISTANCE:I = 0x5dc

.field private static final SWIPE_MIN_X_DISTANCE:I = 0xc8

.field private static final SWIPE_SEEK_VALUE:I = 0x1388

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "MainVideoView"

.field private static final TOGGLE_CONTROLS_VISIBILITY:I = 0x5

.field protected static final TRANSLATE_TIME:I = 0x1f4


# instance fields
.field final gestureDetector:Landroid/view/GestureDetector;

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mBackKeyTimer:I

.field private mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

.field private mBtnControllerStatus:Z

.field private mChangeView:Ljava/lang/Runnable;

.field private mChangeViewDone:Z

.field private mDownTime:J

.field private mDownYPos:I

.field private mFitToScnMode:I

.field public mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

.field private mHandler:Landroid/os/Handler;

.field private mIsVideoGestureStart:Z

.field private mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

.field private mMVGUIHandler:Landroid/os/Handler;

.field private mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

.field private mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

.field private mTitleControllerStatus:Z

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

.field private mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

.field private mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

.field private mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

.field private mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

.field private mWasInvisibleSystemUI:Z

.field private mXTouchPos:I

.field private mYTouchPos:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 4
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 158
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 89
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 95
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mWasInvisibleSystemUI:Z

    .line 101
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 103
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 105
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 107
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 111
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 113
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 117
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    .line 121
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    .line 123
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    .line 125
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I

    .line 127
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I

    .line 129
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I

    .line 133
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownTime:J

    .line 142
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 144
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    .line 266
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/mv/player/view/MainVideoView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$1;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->gestureDetector:Landroid/view/GestureDetector;

    .line 297
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$2;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 1188
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$3;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    .line 1574
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$4;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    .line 1770
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$5;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mMVGUIHandler:Landroid/os/Handler;

    .line 159
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 160
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Landroid/util/AttributeSet;I)V

    .line 164
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 165
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 168
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 89
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 91
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 95
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mWasInvisibleSystemUI:Z

    .line 101
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 103
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 105
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 107
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 111
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 113
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 117
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    .line 121
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    .line 123
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    .line 125
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I

    .line 127
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I

    .line 129
    iput v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I

    .line 133
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownTime:J

    .line 142
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 144
    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    .line 266
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/mv/player/view/MainVideoView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$1;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->gestureDetector:Landroid/view/GestureDetector;

    .line 297
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$2;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 1188
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$3;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    .line 1574
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$4;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    .line 1770
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$5;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mMVGUIHandler:Landroid/os/Handler;

    .line 169
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 170
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/view/MainVideoView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isNotificationAreaTouched(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoSurface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/MainVideoView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/view/MainVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/mv/player/view/MainVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mYTouchPos:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/mv/player/view/MainVideoView;Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mXTouchPos:I

    return p1
.end method

.method private blockHideController()Z
    .locals 2

    .prologue
    .line 1147
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMWTrayOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private blockShowController()Z
    .locals 1

    .prologue
    .line 1153
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    return v0
.end method

.method private getDownTime()J
    .locals 2

    .prologue
    .line 1767
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownTime:J

    return-wide v0
.end method

.method private initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 4
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 173
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 175
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setFocusable(Z)V

    .line 176
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setFocusableInTouchMode(Z)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestFocus()Z

    .line 181
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v0

    .line 185
    .local v0, "prefMgr":Lcom/sec/android/app/mv/player/db/SharedPreference;
    const-string v1, "screen_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setFitToScnMode(I)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v1, :cond_0

    .line 188
    new-instance v1, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 195
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mMVGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/common/SUtils;->setGUIHandler(Landroid/os/Handler;)V

    .line 196
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    .line 197
    return-void
.end method

.method private isNotificationAreaTouched(I)Z
    .locals 3
    .param p1, "y"    # I

    .prologue
    .line 404
    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I

    add-int/lit8 v0, v0, 0xa

    if-le p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownYPos:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08009f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keyUpResetSeek(JI)Z
    .locals 3
    .param p1, "pressTime"    # J
    .param p3, "command"    # I

    .prologue
    .line 964
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 967
    const-wide/16 v0, 0x1f4

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 968
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "short press. pressTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 971
    :cond_0
    const/4 v0, 0x1

    .line 975
    :goto_0
    return v0

    .line 973
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 975
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keyVolumeDown()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 919
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_2

    .line 920
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 921
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeSame()V

    .line 938
    :goto_0
    return v0

    .line 923
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeMute()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 924
    goto :goto_0

    .line 926
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeSVMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupSVVolbar()V

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeMVBarPopup()V

    :cond_2
    :goto_1
    move v0, v1

    .line 938
    goto :goto_0

    .line 930
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupVolbar(Z)V

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeDown()V

    .line 932
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 933
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_1
.end method

.method private keyVolumeUp()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 942
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_2

    .line 943
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 944
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeSame()V

    .line 960
    :goto_0
    return v0

    .line 946
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeMute()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 947
    goto :goto_0

    .line 949
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeSVMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupSVVolbar()V

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeMVBarPopup()V

    :cond_2
    :goto_1
    move v0, v1

    .line 960
    goto :goto_0

    .line 953
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->showPopupVolbar(Z)V

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeUp()V

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_1
.end method

.method private requestSystemKeyEvent(IZ)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "request"    # Z

    .prologue
    .line 1516
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 1519
    .local v1, "windowmanager":Landroid/view/IWindowManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 1520
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1525
    :goto_0
    return v2

    .line 1521
    :catch_0
    move-exception v0

    .line 1522
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1525
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private statusBarSetting(Z)V
    .locals 4
    .param p1, "request"    # Z

    .prologue
    .line 1494
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "statusbar"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 1495
    .local v1, "statusBar":Landroid/app/StatusBarManager;
    if-eqz p1, :cond_0

    .line 1496
    const-string v2, "MainVideoView"

    const-string v3, " statusBarSetting disable statusBar"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    const/high16 v2, 0x1210000

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1500
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Ljava/lang/String;)V

    .line 1501
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_DISABLE_STATUSBAR_OPEN"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Ljava/lang/String;)V

    .line 1502
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->unsetWindowFlag(Ljava/lang/String;)V

    .line 1513
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :goto_0
    return-void

    .line 1504
    .restart local v1    # "statusBar":Landroid/app/StatusBarManager;
    :cond_0
    const-string v2, "MainVideoView"

    const-string v3, " statusBarSetting enable statusBar"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1506
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->unsetWindowFlag(Ljava/lang/String;)V

    .line 1507
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_DISABLE_STATUSBAR_OPEN"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->unsetWindowFlag(Ljava/lang/String;)V

    .line 1508
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    const-string v3, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1510
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :catch_0
    move-exception v0

    .line 1511
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public ChangeView()V
    .locals 1

    .prologue
    .line 1565
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->attachController()V

    .line 1566
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 1567
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestLayout()V

    .line 1568
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setControllerUpdate()V

    .line 1569
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->resetLayout()V

    .line 1570
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTitleName()V

    .line 1571
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->refreshPlayList()V

    .line 1572
    return-void
.end method

.method public attachController()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setAnchorView()V

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibility(I)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setAnchorView()V

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_2

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibility(I)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setAnchorView()V

    .line 497
    :cond_2
    return-void
.end method

.method public attachVideoGestureView()V
    .locals 2

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_0

    .line 1693
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoGestureView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoGestureView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    .line 1694
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->addViewTo(Landroid/view/View;)V

    .line 1696
    :cond_0
    return-void
.end method

.method public changeLockStatus(Z)V
    .locals 10
    .param p1, "lockState"    # Z

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x2

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1404
    const-string v2, "MainVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeLockStatus E. lockState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    const/4 v1, 0x1

    .line 1408
    .local v1, "isCtrlBtnEnable":Z
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1410
    .local v0, "configuration":Landroid/content/res/Configuration;
    if-nez p1, :cond_4

    .line 1412
    const/4 v1, 0x1

    .line 1413
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->setLockState(Z)V

    .line 1414
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 1415
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1417
    :cond_0
    invoke-direct {p0, v7, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1418
    const/16 v2, 0xbb

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1419
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v7, :cond_1

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v8, :cond_1

    .line 1421
    invoke-direct {p0, v9, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1423
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->statusBarSetting(Z)V

    .line 1425
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v2, :cond_2

    .line 1426
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->hideLockIcon()V

    .line 1428
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    if-eqz v2, :cond_3

    .line 1429
    const-string v2, "MainVideoView"

    const-string v3, "changeLockStatus. show controller"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1433
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setRemoveSystemUI(Z)V

    .line 1468
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setEnabled(Z)V

    .line 1469
    const-string v2, "MainVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeLockStatus(). VideoServiceUtil.getLockState() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470
    return-void

    .line 1437
    :cond_4
    const/4 v1, 0x0

    .line 1438
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->setLockState(Z)V

    .line 1440
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_5

    .line 1441
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1444
    :cond_5
    invoke-direct {p0, v7, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1445
    const/16 v2, 0xbb

    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1446
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v7, :cond_6

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v8, :cond_6

    .line 1448
    invoke-direct {p0, v9, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestSystemKeyEvent(IZ)Z

    .line 1450
    :cond_6
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->statusBarSetting(Z)V

    .line 1452
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v2, :cond_7

    .line 1453
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 1455
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->forceBtnRelease()V

    .line 1456
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeHandler()V

    .line 1458
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1459
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 1461
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_9

    .line 1462
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    .line 1465
    :cond_9
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setRemoveSystemUI(Z)V

    goto :goto_0
.end method

.method public changeLockStatusForOnKeyEvent(Landroid/view/KeyEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    .line 1370
    if-nez p1, :cond_1

    .line 1371
    const-string v1, "MainVideoView"

    const-string v4, "changeLockStatusForOnKeyEvent : EVENT is null!!!"

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1401
    :cond_0
    :goto_0
    return-void

    .line 1375
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1376
    .local v0, "configuration":Landroid/content/res/Configuration;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1378
    :pswitch_0
    iget v1, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v1, v9, :cond_0

    iget v1, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v1, v8, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 1381
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownTime:J

    goto :goto_0

    .line 1386
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 1388
    .local v2, "pressTime":J
    iget v1, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v1, v9, :cond_2

    iget v1, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v1, v8, :cond_2

    .line 1390
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 1393
    :cond_2
    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1394
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1395
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 1397
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 1376
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public changeScreen()V
    .locals 4

    .prologue
    .line 1300
    const-string v1, "MainVideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeScreen() -ScreenMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    iget v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    add-int/lit8 v0, v1, 0x1

    .line 1304
    .local v0, "newMode":I
    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 1305
    const/4 v0, 0x0

    .line 1308
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1309
    const/4 v0, 0x2

    .line 1312
    :cond_1
    iput v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    .line 1313
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 1314
    return-void
.end method

.method public checkSurfaceArea(II)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    .line 1605
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isSurfaceExist()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1606
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getX()F

    move-result v1

    float-to-int v1, v1

    if-le p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getX()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getY()F

    move-result v1

    float-to-int v1, v1

    if-le p2, v1, :cond_0

    int-to-float v1, p2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getHeight()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 1610
    const/4 v0, 0x1

    .line 1614
    :cond_0
    return v0
.end method

.method public finishUpVideoGesture()Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x0

    .line 1707
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1708
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1711
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 1713
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1714
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->hide()V

    .line 1716
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    if-ne v0, v1, :cond_1

    .line 1717
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 1720
    :cond_1
    const/4 v0, 0x1

    .line 1724
    :cond_2
    return v0
.end method

.method public forceBtnRelease()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setBtnPress(Z)V

    .line 1346
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_1

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setBtnPress(Z)V

    .line 1348
    :cond_1
    return-void
.end method

.method public forceHideController()V
    .locals 2

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->forceHide()V

    .line 1131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_1

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->forceHide()V

    .line 1135
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_2

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->forceHide()V

    .line 1139
    :cond_2
    const-string v0, "MainVideoView"

    const-string v1, "setSystemUiVisibility - set hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1143
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1144
    :cond_3
    return-void
.end method

.method public getBtnInstance()Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .locals 1

    .prologue
    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1686
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 1688
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFitToScnMode()I
    .locals 1

    .prologue
    .line 1639
    iget v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    return v0
.end method

.method public getLockState()Z
    .locals 1

    .prologue
    .line 1323
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    return v0
.end method

.method public hasNavigationBar()Z
    .locals 1

    .prologue
    .line 1701
    const/4 v0, 0x0

    return v0
.end method

.method public hideController()V
    .locals 2

    .prologue
    .line 1097
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1098
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->blockHideController()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1099
    const-string v0, "MainVideoView"

    const-string v1, "blockHideController - Don\'t Hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    :cond_0
    :goto_0
    return-void

    .line 1104
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_2

    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->hide()V

    .line 1108
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_3

    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hide()V

    .line 1112
    :cond_3
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_6

    .line 1113
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_6

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hide()V

    .line 1118
    :cond_6
    const-string v0, "MainVideoView"

    const-string v1, "setSystemUiVisibility - set hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1120
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public hideStateView()V
    .locals 2

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v0, :cond_0

    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 1620
    :cond_0
    return-void
.end method

.method public isChangeViewDone()Z
    .locals 1

    .prologue
    .line 1647
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    return v0
.end method

.method public isControls()Z
    .locals 1

    .prologue
    .line 986
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isControlsShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1025
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControls()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1029
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isStateViewVisible()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1628
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v1, :cond_0

    .line 1629
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1630
    const/4 v0, 0x1

    .line 1635
    :cond_0
    return v0
.end method

.method public isSurfaceExist()Z
    .locals 1

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceExists()Z

    move-result v0

    .line 1601
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleCtrlShowing()Z
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-nez v0, :cond_0

    .line 1034
    const/4 v0, 0x0

    .line 1037
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public isVolumeCtrlShowing()Z
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-nez v0, :cond_0

    .line 1042
    const/4 v0, 0x0

    .line 1045
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isVolumeBarShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public moveSubtitleDown()V
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 1588
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->moveDown()V

    .line 1590
    :cond_0
    return-void
.end method

.method public moveSubtitleUp()V
    .locals 1

    .prologue
    .line 1581
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->moveUp()V

    .line 1584
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 251
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->isLockBtnVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 255
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 263
    :goto_0
    return v0

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 261
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 263
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v7, 0x6

    const/16 v6, 0xbb8

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 508
    const-string v2, "MainVideoView"

    const-string v3, "onKeyDown"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 512
    .local v0, "configuration":Landroid/content/res/Configuration;
    const/16 v2, 0x1a

    if-eq p1, v2, :cond_1

    const/16 v2, 0x7a

    if-eq p1, v2, :cond_1

    const/16 v2, 0x18

    if-eq p1, v2, :cond_1

    const/16 v2, 0x19

    if-eq p1, v2, :cond_1

    const/16 v2, 0xa4

    if-eq p1, v2, :cond_1

    const/16 v2, 0x5b

    if-eq p1, v2, :cond_1

    const/16 v2, 0x55

    if-eq p1, v2, :cond_1

    const/16 v2, 0x57

    if-eq p1, v2, :cond_1

    const/16 v2, 0x58

    if-eq p1, v2, :cond_1

    const/16 v2, 0x56

    if-eq p1, v2, :cond_1

    const/16 v2, 0x59

    if-eq p1, v2, :cond_1

    const/16 v2, 0x5a

    if-eq p1, v2, :cond_1

    const/16 v2, 0x7e

    if-eq p1, v2, :cond_1

    const/16 v2, 0x7f

    if-eq p1, v2, :cond_1

    const/16 v2, 0x4f

    if-eq p1, v2, :cond_1

    const/16 v2, 0x17

    if-eq p1, v2, :cond_1

    const/16 v2, 0x16

    if-eq p1, v2, :cond_1

    const/16 v2, 0x15

    if-eq p1, v2, :cond_1

    const/16 v2, 0x52

    if-eq p1, v2, :cond_1

    const/16 v2, 0x14

    if-eq p1, v2, :cond_1

    const/16 v2, 0x13

    if-eq p1, v2, :cond_1

    const/16 v2, 0x3e

    if-eq p1, v2, :cond_1

    const/16 v2, 0xa8

    if-eq p1, v2, :cond_1

    const/16 v2, 0xa9

    if-eq p1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 717
    :cond_0
    :goto_0
    :sswitch_0
    return v1

    .line 539
    :cond_1
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_2

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 542
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mDownTime:J

    .line 545
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 717
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 547
    :sswitch_1
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v2, :cond_3

    goto :goto_0

    .line 553
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_4

    .line 554
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 560
    :cond_4
    :sswitch_3
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_6

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_6

    .line 562
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-nez v2, :cond_5

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto :goto_0

    .line 565
    :cond_5
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 566
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 571
    :cond_6
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_7

    .line 572
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 578
    :cond_7
    :sswitch_4
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_8

    .line 579
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 585
    :cond_8
    :sswitch_5
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_a

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_a

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-nez v2, :cond_9

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto :goto_0

    .line 590
    :cond_9
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 591
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 596
    :cond_a
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_b

    .line 597
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 603
    :cond_b
    :sswitch_6
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_0

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 615
    :sswitch_7
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_d

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_d

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-nez v2, :cond_c

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 620
    :cond_c
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 621
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setFocus()V

    .line 622
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 627
    :cond_d
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyVolumeUp()Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 632
    :sswitch_8
    iget v2, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v2, v5, :cond_f

    iget v2, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v2, v4, :cond_f

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-nez v2, :cond_e

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 637
    :cond_e
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 638
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setFocus()V

    .line 639
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 644
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyVolumeDown()Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 650
    :sswitch_9
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyVolumeUp()Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 655
    :sswitch_a
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyVolumeDown()Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 660
    :sswitch_b
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_HIGH_SPEED_PLAY:Z

    if-eqz v2, :cond_0

    .line 662
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v2

    const/16 v3, 0x221

    if-ne v2, v3, :cond_0

    .line 664
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 665
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    .line 673
    :cond_10
    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    .line 674
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    .line 676
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeUp()V

    .line 677
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 666
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_10

    .line 667
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isShowing()Z

    move-result v2

    if-nez v2, :cond_10

    .line 668
    :cond_12
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 669
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    goto :goto_1

    .line 683
    :sswitch_c
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_HIGH_SPEED_PLAY:Z

    if-eqz v2, :cond_0

    .line 685
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v2

    const/16 v3, 0x222

    if-ne v2, v3, :cond_0

    .line 688
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 689
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    .line 697
    :cond_13
    :goto_2
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    .line 698
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    .line 700
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeDown()V

    .line 701
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 690
    :cond_14
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_13

    .line 691
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isShowing()Z

    move-result v2

    if-nez v2, :cond_13

    .line 692
    :cond_15
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 693
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    goto :goto_2

    .line 708
    :sswitch_d
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_0

    .line 709
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->makeToggleMute()V

    goto/16 :goto_0

    .line 545
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_7
        0x14 -> :sswitch_8
        0x15 -> :sswitch_3
        0x16 -> :sswitch_5
        0x17 -> :sswitch_6
        0x18 -> :sswitch_9
        0x19 -> :sswitch_a
        0x52 -> :sswitch_1
        0x59 -> :sswitch_2
        0x5a -> :sswitch_4
        0x5b -> :sswitch_d
        0xa4 -> :sswitch_d
        0xa8 -> :sswitch_b
        0xa9 -> :sswitch_c
        0x117 -> :sswitch_a
        0x118 -> :sswitch_9
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 721
    const-string v4, "MainVideoView"

    const-string v5, "onKeyUp"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v4, :cond_0

    .line 724
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isResumed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 725
    const-string v4, "MainVideoView"

    const-string v5, "onKeyUp() - MoviePlayer isResumed() == false, not handle the KeyEvent"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    const/4 v4, 0x1

    .line 915
    :goto_0
    return v4

    .line 730
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 732
    .local v0, "configuration":Landroid/content/res/Configuration;
    const/16 v4, 0x1a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x18

    if-eq p1, v4, :cond_2

    const/16 v4, 0x19

    if-eq p1, v4, :cond_2

    const/16 v4, 0xa4

    if-eq p1, v4, :cond_2

    const/16 v4, 0x5b

    if-eq p1, v4, :cond_2

    const/16 v4, 0x55

    if-eq p1, v4, :cond_2

    const/16 v4, 0x57

    if-eq p1, v4, :cond_2

    const/16 v4, 0x58

    if-eq p1, v4, :cond_2

    const/16 v4, 0x56

    if-eq p1, v4, :cond_2

    const/16 v4, 0x59

    if-eq p1, v4, :cond_2

    const/16 v4, 0x5a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7e

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7f

    if-eq p1, v4, :cond_2

    const/16 v4, 0x4f

    if-eq p1, v4, :cond_2

    const/16 v4, 0x17

    if-eq p1, v4, :cond_2

    const/16 v4, 0x16

    if-eq p1, v4, :cond_2

    const/16 v4, 0x15

    if-eq p1, v4, :cond_2

    const/16 v4, 0x52

    if-ne p1, v4, :cond_1

    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-nez v4, :cond_2

    :cond_1
    const/16 v4, 0x14

    if-eq p1, v4, :cond_2

    const/16 v4, 0x13

    if-eq p1, v4, :cond_2

    const/16 v4, 0x3e

    if-eq p1, v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 754
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ne p1, v4, :cond_4

    .line 755
    const-string v4, "MainVideoView"

    const-string v5, "KEYCODE_BACK for MVClient"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->finishUpVideoGesture()Z

    .line 768
    const-wide/16 v2, 0x0

    .line 769
    .local v2, "pressTime":J
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 774
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 779
    :goto_1
    sparse-switch p1, :sswitch_data_0

    .line 915
    :cond_3
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v4

    goto/16 :goto_0

    .line 759
    .end local v2    # "pressTime":J
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_5

    .line 760
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 762
    :cond_5
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 776
    .restart local v2    # "pressTime":J
    :cond_6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    goto :goto_1

    .line 782
    :sswitch_0
    const-wide/16 v4, 0x1f4

    cmp-long v4, v2, v4

    if-gez v4, :cond_7

    .line 783
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 784
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    .line 788
    :cond_7
    :goto_3
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 786
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_3

    .line 791
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 792
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 794
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 795
    .local v1, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 797
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 800
    .end local v1    # "msg":Landroid/os/Message;
    :sswitch_2
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v4, :cond_3

    .line 801
    const/16 v4, 0xbb8

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 802
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->showPopupMenu()V

    .line 803
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 809
    :sswitch_3
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_c

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    .line 811
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_b

    .line 812
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 817
    :cond_a
    :goto_4
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 814
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_a

    .line 815
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto :goto_4

    .line 819
    :cond_c
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_d

    .line 820
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 822
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_3

    .line 823
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto/16 :goto_2

    .line 828
    :sswitch_4
    const/16 v4, 0xd

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 829
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 834
    :sswitch_5
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_e

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_e

    .line 836
    const/16 v4, 0x9

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    .line 837
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 839
    :cond_e
    const/16 v4, 0xd

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 840
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 845
    :sswitch_6
    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 846
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 851
    :sswitch_7
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_f

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_f

    .line 853
    const/16 v4, 0x8

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    .line 854
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 856
    :cond_f
    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 857
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 863
    :sswitch_8
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_10

    .line 864
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 865
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 867
    :cond_10
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_3

    .line 868
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto/16 :goto_2

    .line 872
    :sswitch_9
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_11

    .line 873
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 874
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 876
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_3

    .line 877
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto/16 :goto_2

    .line 881
    :sswitch_a
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_12

    .line 882
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 883
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 885
    :cond_12
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_3

    .line 886
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto/16 :goto_2

    .line 890
    :sswitch_b
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-nez v4, :cond_13

    .line 891
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v5, 0x12

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 892
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 894
    :cond_13
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_3

    .line 895
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    goto/16 :goto_2

    .line 901
    :sswitch_c
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 903
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 904
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v4, :cond_14

    .line 905
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->showLockIcon()V

    .line 907
    :cond_14
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 779
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_c
        0x14 -> :sswitch_c
        0x15 -> :sswitch_5
        0x16 -> :sswitch_7
        0x17 -> :sswitch_3
        0x1a -> :sswitch_0
        0x1e -> :sswitch_b
        0x2a -> :sswitch_8
        0x2c -> :sswitch_9
        0x30 -> :sswitch_a
        0x3e -> :sswitch_3
        0x52 -> :sswitch_2
        0x59 -> :sswitch_4
        0x5a -> :sswitch_6
        0x7a -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolMVBar()Z

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->onPause()V

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->finishUpVideoGesture()Z

    .line 437
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 421
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->onResume()V

    .line 426
    :cond_0
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_0

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->toggleControlsVisiblity()V

    .line 504
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public prepareChangeView(I)V
    .locals 5
    .param p1, "orientation"    # I

    .prologue
    const/4 v4, -0x1

    .line 1529
    const-string v2, "MainVideoView"

    const-string v3, "prepareChangeView() : start!"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllInController()V

    .line 1532
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->requestLayout()V

    .line 1534
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1535
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1536
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1540
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 1541
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1542
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1545
    .end local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v2, :cond_2

    .line 1546
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1547
    .local v1, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1548
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1551
    .end local v1    # "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_3

    .line 1552
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 1555
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 1556
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->finishUpVideoGesture()Z

    .line 1557
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->forceBtnRelease()V

    .line 1558
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeHandler()V

    .line 1559
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->moveSubtitleDown()V

    .line 1560
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1561
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1562
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1728
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    if-eqz v0, :cond_0

    .line 1729
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->releaseView()V

    .line 1730
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    .line 1733
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v0, :cond_1

    .line 1734
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->releaseView()V

    .line 1735
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 1738
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 1739
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->releaseView()V

    .line 1740
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 1743
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_3

    .line 1744
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->releaseView()V

    .line 1745
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 1748
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_4

    .line 1749
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->releaseView()V

    .line 1750
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 1753
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_5

    .line 1754
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->releaseView()V

    .line 1755
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 1758
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 1759
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1761
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 1763
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllViews()V

    .line 1764
    return-void
.end method

.method public removeAllInController()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 990
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hide(Z)V

    .line 992
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->removeAllViewsInLayout()V

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_1

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->hide(Z)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->removeAllViewsInLayout()V

    .line 1000
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_2

    .line 1001
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hide(Z)V

    .line 1002
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->removeAllViewsInLayout()V

    .line 1005
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setNullBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$myView;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1009
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 1011
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1012
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->hide()V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    if-ne v0, v1, :cond_3

    .line 1014
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 1019
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_4

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1022
    :cond_4
    return-void
.end method

.method public removeControllerFadeOut()V
    .locals 2

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1158
    return-void
.end method

.method public removeHandler()V
    .locals 1

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1352
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->removeHandler()V

    .line 1353
    :cond_0
    return-void
.end method

.method public reorderViews()V
    .locals 1

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->bringToFront()V

    .line 1658
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_1

    .line 1659
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->bringToFront()V

    .line 1661
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_2

    .line 1662
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->bringToFront()V

    .line 1664
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_3

    .line 1665
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->bringToFront()V

    .line 1667
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_4

    .line 1668
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->bringToFront()V

    .line 1670
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v0, :cond_5

    .line 1671
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->bringToFront()V

    .line 1673
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v0, :cond_6

    .line 1674
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->bringToFront()V

    .line 1676
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    if-eqz v0, :cond_7

    .line 1677
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->bringToFront()V

    .line 1679
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    if-eqz v0, :cond_8

    .line 1680
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->bringToFront()V

    .line 1682
    :cond_8
    return-void
.end method

.method public setChangeViewDone(Z)V
    .locals 0
    .param p1, "mChangeViewDone"    # Z

    .prologue
    .line 1651
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mChangeViewDone:Z

    .line 1652
    return-void
.end method

.method public setChildViewReferences(Lcom/sec/android/app/mv/player/view/VideoSurface;Lcom/sec/android/app/mv/player/view/VideoStateView;Lcom/sec/android/app/mv/player/view/SubtitleView;Lcom/sec/android/app/mv/player/view/VideoLockCtrl;Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;Lcom/sec/android/app/mv/player/view/VideoFlickView;)V
    .locals 0
    .param p1, "surface"    # Lcom/sec/android/app/mv/player/view/VideoSurface;
    .param p2, "state"    # Lcom/sec/android/app/mv/player/view/VideoStateView;
    .param p3, "subtitle"    # Lcom/sec/android/app/mv/player/view/SubtitleView;
    .param p4, "lockCtrl"    # Lcom/sec/android/app/mv/player/view/VideoLockCtrl;
    .param p5, "numbering"    # Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;
    .param p6, "flickView"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 230
    if-eqz p1, :cond_0

    .line 231
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 233
    :cond_0
    if-eqz p2, :cond_1

    .line 234
    iput-object p2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 236
    :cond_1
    if-eqz p3, :cond_2

    .line 237
    iput-object p3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 239
    :cond_2
    if-eqz p4, :cond_3

    .line 240
    iput-object p4, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 242
    :cond_3
    if-eqz p5, :cond_4

    .line 243
    iput-object p5, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 245
    :cond_4
    if-eqz p6, :cond_5

    .line 246
    iput-object p6, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .line 248
    :cond_5
    return-void
.end method

.method public setControllerPlayerStop()V
    .locals 1

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->playerStop()V

    .line 480
    :cond_0
    return-void
.end method

.method public setControllerProgress()V
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setProgress()I

    .line 1595
    :cond_0
    return-void
.end method

.method public setControllerUpdate()V
    .locals 2

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 442
    const-string v0, "MainVideoView"

    const-string v1, "setControllerUpdate - previous status is pause."

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setUpdate()V

    .line 449
    :cond_1
    return-void
.end method

.method public setFitToScnMode(I)V
    .locals 0
    .param p1, "mFitToScnMode"    # I

    .prologue
    .line 1643
    iput p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFitToScnMode:I

    .line 1644
    return-void
.end method

.method public setInvisibleControllers()V
    .locals 1

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1162
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setInvisibleAllViews()V

    .line 1165
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_1

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setInvisibleAllViews()V

    .line 1169
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_2

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setInvisibleAllViews()V

    .line 1172
    :cond_2
    return-void
.end method

.method public setLockState(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    .line 1327
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLockState mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    invoke-static {p1}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->setLockState(Z)V

    .line 1329
    return-void
.end method

.method public setMultiVisionScreen()V
    .locals 4

    .prologue
    .line 1317
    const-string v1, "MainVideoView"

    const-string v2, "setMultiVisionScreen"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1318
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1319
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1320
    return-void
.end method

.method public setRemoveSystemUI(Z)V
    .locals 5
    .param p1, "remove"    # Z

    .prologue
    .line 1474
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1475
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const-string v2, "MainVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSystemUIRemove ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") current systemUiVisibility : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v1

    .line 1479
    .local v1, "systemUiVisibility":I
    if-eqz p1, :cond_1

    .line 1481
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    or-int/2addr v2, v1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 1488
    :cond_0
    :goto_0
    const-string v2, "MainVideoView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setRemoveSystemUI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1490
    return-void

    .line 1484
    :cond_1
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    .line 1485
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    xor-int/2addr v2, v1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_0
.end method

.method public setRootViewReference(Landroid/widget/RelativeLayout;)V
    .locals 0
    .param p1, "rootView"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 223
    if-eqz p1, :cond_0

    .line 224
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 225
    :cond_0
    return-void
.end method

.method public setTitleName()V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setTitleName(Ljava/lang/String;)V

    .line 474
    return-void
.end method

.method public setTitleName(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const v2, 0x7f0a0093

    .line 452
    const/4 v0, 0x0

    .line 454
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFileName()Ljava/lang/String;

    move-result-object v0

    .line 457
    :cond_0
    if-eqz p1, :cond_3

    .line 458
    move-object v0, p1

    .line 465
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getServerFileTitle()Ljava/lang/String;

    move-result-object v0

    .line 469
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->updateTitle(Ljava/lang/String;)V

    .line 470
    return-void

    .line 459
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 461
    :cond_4
    if-nez v0, :cond_1

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setTitleVolumeControl(Z)V
    .locals 2
    .param p1, "isCheck"    # Z

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v0, :cond_0

    .line 1357
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 1359
    :cond_0
    if-eqz p1, :cond_1

    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeUp()V

    .line 1365
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolume()V

    .line 1366
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1367
    return-void

    .line 1362
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeDown()V

    goto :goto_0
.end method

.method public setVideoController(Lcom/sec/android/app/mv/player/view/VideoBtnController;Lcom/sec/android/app/mv/player/view/VideoTitleController;Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V
    .locals 1
    .param p1, "btnctrl"    # Lcom/sec/android/app/mv/player/view/VideoBtnController;
    .param p2, "titlectrl"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;
    .param p3, "actionBar"    # Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 413
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 414
    iput-object p2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 415
    iput-object p3, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->attachController()V

    .line 418
    return-void
.end method

.method public setViewEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1333
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setEnabled(Z)V

    .line 1335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_1

    .line 1336
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setEnabled(Z)V

    .line 1338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_2

    .line 1339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setEnabled(Z)V

    .line 1340
    :cond_2
    return-void
.end method

.method public setVisibleControllers()V
    .locals 1

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setVisibleAllViews()V

    .line 1179
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_1

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibleAllViews()V

    .line 1183
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_2

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setVisibleAllViews()V

    .line 1186
    :cond_2
    return-void
.end method

.method public showController()V
    .locals 1

    .prologue
    .line 1049
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1050
    return-void
.end method

.method public showController(I)V
    .locals 4
    .param p1, "showTime"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1053
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1055
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->blockShowController()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1056
    const-string v0, "MainVideoView"

    const-string v1, "blockShowController - Don\'t Hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    :goto_0
    return-void

    .line 1060
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1061
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$Color;->BLACK:Lcom/sec/android/app/mv/player/common/VUtils$Color;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->changeWindowBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$Color;)V

    .line 1062
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    const-string v1, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->unsetWindowFlag(Ljava/lang/String;)V

    .line 1065
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_2

    .line 1066
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    if-nez v0, :cond_4

    .line 1067
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->show()V

    .line 1072
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_6

    .line 1073
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    if-nez v0, :cond_5

    .line 1074
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->show()V

    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_3

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->show()V

    .line 1078
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->moveSubtitleUp()V

    .line 1085
    :goto_2
    const-string v0, "MainVideoView"

    const-string v1, "setSystemUiVisibility - set show!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1086
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1087
    const/16 v0, 0x200

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1092
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1069
    :cond_4
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mBtnControllerStatus:Z

    goto :goto_1

    .line 1080
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleControllerStatus:Z

    goto :goto_2

    .line 1082
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->moveSubtitleUp()V

    goto :goto_2

    .line 1089
    :cond_7
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    goto :goto_3
.end method

.method public showStateView()V
    .locals 2

    .prologue
    .line 1623
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v0, :cond_0

    .line 1624
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 1625
    :cond_0
    return-void
.end method

.method public toggleControlsVisiblity()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 980
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleControlsVisiblity - isControlsShowing() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 982
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 983
    return-void
.end method
