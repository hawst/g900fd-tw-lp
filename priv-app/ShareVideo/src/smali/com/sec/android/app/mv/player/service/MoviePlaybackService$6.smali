.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 5
    .param p1, "focusChange"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 673
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAudioFocusListener - focusChange : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    packed-switch p1, :pswitch_data_0

    .line 726
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 678
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    .line 680
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    .line 681
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->pause()V

    .line 686
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPauseToServer()V

    goto :goto_0

    .line 684
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stop()V

    goto :goto_1

    .line 693
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->checkIsRinging(Landroid/content/Context;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setVolume(F)V

    .line 698
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    const-string v0, "MoviePlaybackService"

    const-string v1, "mAudioFocusListener. pause by alert sound"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    .line 701
    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mHasAudioFocus:Z
    invoke-static {v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1302(Z)Z

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->pauseOrStopByAlertSound()V

    .line 703
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPauseToServer()V

    goto :goto_0

    .line 710
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    .line 713
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    .line 717
    :goto_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayToServer()V

    goto/16 :goto_0

    .line 716
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$6;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->startPlay()V

    goto :goto_2

    .line 675
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
