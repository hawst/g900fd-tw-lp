.class Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;
.super Ljava/lang/Object;
.source "HelpPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->initializeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$100(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/help/HelpStep;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    const v2, 0x7f0d001c

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    .line 242
    .local v0, "mSwitch":Lcom/sec/android/app/mv/player/widget/ChannelSwitch;
    invoke-virtual {p1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 243
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 244
    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # invokes: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextOfSwitchScreen()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$200(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$2;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # invokes: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->setStepNextFocusOnVolumeBtn()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$300(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V

    .line 248
    .end local v0    # "mSwitch":Lcom/sec/android/app/mv/player/widget/ChannelSwitch;
    :cond_0
    return-void
.end method
