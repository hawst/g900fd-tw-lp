.class Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;
.super Ljava/lang/Object;
.source "DeviceVolumeListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LRSpk"
.end annotation


# instance fields
.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V
    .locals 4

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    .line 296
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    .line 298
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v1

    .line 300
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceInfo(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->print(Ljava/util/List;)V

    .line 306
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    new-instance v3, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$PosCompare;

    invoke-direct {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$PosCompare;-><init>()V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 307
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->print(Ljava/util/List;)V

    .line 308
    :cond_2
    return-void
.end method

.method private print(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p1, "l":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 337
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 338
    sget-object v3, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "------- ID: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceID()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", pos:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", name: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :cond_0
    return-void
.end method


# virtual methods
.method public getLR()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 311
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 314
    .local v1, "mLR":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;>;"
    new-instance v4, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f0a0076

    invoke-virtual {v3, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v6, v3, v6}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(ILjava/lang/String;I)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .line 323
    .local v2, "size":I
    move v0, v2

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v3

    if-eq v3, v6, :cond_2

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->mList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    :cond_0
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v3, :cond_1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$LRSpk;->print(Ljava/util/List;)V

    .line 331
    :cond_1
    return-object v1

    .line 323
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
