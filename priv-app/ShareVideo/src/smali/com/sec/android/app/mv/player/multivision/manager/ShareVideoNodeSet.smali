.class public Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet;
.super Ljava/lang/Object;
.source "ShareVideoNodeSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    }
.end annotation


# static fields
.field private static mNodeItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet;->mNodeItemList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static getNodeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet;->mNodeItemList:Ljava/util/ArrayList;

    return-object v0
.end method
