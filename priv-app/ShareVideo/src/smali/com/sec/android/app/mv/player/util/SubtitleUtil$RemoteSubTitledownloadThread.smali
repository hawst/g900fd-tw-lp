.class Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;
.super Ljava/lang/Thread;
.source "SubtitleUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/SubtitleUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RemoteSubTitledownloadThread"
.end annotation


# instance fields
.field conn:Ljava/net/HttpURLConnection;

.field down:Ljava/net/URL;

.field f:Ljava/io/File;

.field fos:Ljava/io/FileOutputStream;

.field is:Ljava/io/InputStream;

.field mDownloadPath:Ljava/lang/String;

.field mSubTitleUri:Ljava/lang/String;

.field read:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "SubTitleUri"    # Ljava/lang/String;
    .param p2, "DownloadPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 812
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 796
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mSubTitleUri:Ljava/lang/String;

    .line 798
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->read:I

    .line 800
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->down:Ljava/net/URL;

    .line 802
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    .line 804
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    .line 806
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    .line 808
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->f:Ljava/io/File;

    .line 810
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mDownloadPath:Ljava/lang/String;

    .line 813
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mSubTitleUri:Ljava/lang/String;

    .line 814
    iput-object p2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mDownloadPath:Ljava/lang/String;

    .line 815
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 819
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mSubTitleUri:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->down:Ljava/net/URL;

    .line 820
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->down:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    .line 821
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->mDownloadPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->f:Ljava/io/File;

    .line 823
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->f:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 824
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->f:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 826
    :cond_0
    const-string v2, "SubtitleUtil"

    const-string v3, "RemoteSubTitledownloadThread Start!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 828
    .local v0, "buffer":[B
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    .line 829
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->f:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    .line 830
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->read:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 831
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->read:I

    invoke-virtual {v2, v0, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 833
    .end local v0    # "buffer":[B
    :catch_0
    move-exception v1

    .line 834
    .local v1, "e":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 839
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    if-eqz v2, :cond_1

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 841
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    if-eqz v2, :cond_2

    .line 842
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 843
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    if-eqz v2, :cond_3

    .line 844
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 845
    :cond_3
    const-string v2, "SubtitleUtil"

    const-string v3, "RemoteSubTitledownloadThread finish!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 851
    .end local v1    # "e":Ljava/net/MalformedURLException;
    :goto_1
    return-void

    .line 839
    .restart local v0    # "buffer":[B
    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    if-eqz v2, :cond_5

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 841
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    if-eqz v2, :cond_6

    .line 842
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 843
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    if-eqz v2, :cond_7

    .line 844
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 845
    :cond_7
    const-string v2, "SubtitleUtil"

    const-string v3, "RemoteSubTitledownloadThread finish!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 847
    :catch_1
    move-exception v1

    .line 848
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 847
    .end local v0    # "buffer":[B
    .local v1, "e":Ljava/net/MalformedURLException;
    :catch_2
    move-exception v1

    .line 848
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 835
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 836
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 839
    :try_start_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    if-eqz v2, :cond_8

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 841
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    if-eqz v2, :cond_9

    .line 842
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 843
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    if-eqz v2, :cond_a

    .line 844
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 845
    :cond_a
    const-string v2, "SubtitleUtil"

    const-string v3, "RemoteSubTitledownloadThread finish!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 847
    :catch_4
    move-exception v1

    .line 848
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 838
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 839
    :try_start_6
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    if-eqz v3, :cond_b

    .line 840
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->is:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 841
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    if-eqz v3, :cond_c

    .line 842
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 843
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    if-eqz v3, :cond_d

    .line 844
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/SubtitleUtil$RemoteSubTitledownloadThread;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 845
    :cond_d
    const-string v3, "SubtitleUtil"

    const-string v4, "RemoteSubTitledownloadThread finish!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 849
    :goto_2
    throw v2

    .line 847
    :catch_5
    move-exception v1

    .line 848
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
