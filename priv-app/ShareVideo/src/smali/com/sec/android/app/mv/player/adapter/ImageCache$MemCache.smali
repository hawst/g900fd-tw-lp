.class Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/adapter/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemCache"
.end annotation


# static fields
.field private static mLruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createLruCache()Landroid/util/LruCache;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$100()Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    .line 99
    .local v1, "memClass":I
    const/high16 v2, 0x100000

    mul-int/2addr v2, v1

    div-int/lit8 v0, v2, 0x4

    .line 100
    .local v0, "cacheSize":I
    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ImageCache - MemCache - cacheSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$100()Landroid/content/Context;

    move-result-object v4

    int-to-long v6, v0

    # invokes: Lcom/sec/android/app/mv/player/adapter/ImageCache;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;
    invoke-static {v4, v6, v7}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$200(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " memClass :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v2, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache$1;

    invoke-direct {v2, v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache$1;-><init>(I)V

    return-object v2
.end method

.method public static declared-synchronized get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v1, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 74
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-nez v0, :cond_0

    .line 91
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->createLruCache()Landroid/util/LruCache;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    .line 93
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public static declared-synchronized put(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 81
    const-class v1, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :goto_0
    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ImageCache - CURRENT CACHED SIZE:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$100()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v4}, Landroid/util/LruCache;->size()I

    move-result v4

    int-to-long v4, v4

    # invokes: Lcom/sec/android/app/mv/player/adapter/ImageCache;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$200(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit v1

    return-void

    .line 84
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->getCache()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
