.class Lcom/sec/android/app/mv/player/view/MainVideoView$5;
.super Landroid/os/Handler;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 1770
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1772
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1796
    const-string v0, "MainVideoView"

    const-string v1, "mMVGUIHandler: default"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1799
    :cond_0
    :goto_0
    return-void

    .line 1774
    :sswitch_0
    const-string v0, "MainVideoView"

    const-string v1, "mMVGUIHandler: DEVICE_CHANGED"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1775
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1776
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateControllersForSV()V

    .line 1777
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1778
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1300(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->updateActionBarForSV()V

    .line 1779
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyDeviceChange()V

    goto :goto_0

    .line 1783
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1784
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$200(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoTitleController;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolumeDilaogFrag(II)V

    goto :goto_0

    .line 1788
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    goto :goto_0

    .line 1792
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$5;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    goto :goto_0

    .line 1772
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
    .end sparse-switch
.end method
