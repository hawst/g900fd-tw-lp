.class Lcom/sec/android/app/mv/player/view/VideoBtnController$18;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mPositionStatus:I

.field msg:Landroid/os/Message;

.field position:J

.field preProgress:I

.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1483
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private abs(I)I
    .locals 0
    .param p1, "a"    # I

    .prologue
    .line 1492
    if-ltz p1, :cond_0

    .line 1495
    .end local p1    # "a":I
    :goto_0
    return p1

    .restart local p1    # "a":I
    :cond_0
    neg-int p1, p1

    goto :goto_0
.end method

.method private isUpdateNeeded(II)Z
    .locals 5
    .param p1, "preProgress"    # I
    .param p2, "progress"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1499
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1500
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_2

    .line 1501
    if-gez p1, :cond_1

    .line 1514
    :cond_0
    :goto_0
    return v1

    .line 1503
    :cond_1
    sub-int v3, p1, p2

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->abs(I)I

    move-result v3

    const/16 v4, 0xfa0

    if-ge v3, v4, :cond_0

    move v1, v2

    .line 1506
    goto :goto_0

    .line 1509
    :cond_2
    if-ltz p1, :cond_0

    .line 1511
    sub-int v3, p1, p2

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->abs(I)I

    move-result v3

    const/16 v4, 0x7d0

    if-ge v3, v4, :cond_0

    move v1, v2

    .line 1514
    goto :goto_0
.end method


# virtual methods
.method public onHoverChanged(Landroid/widget/SeekBar;IZ)V
    .locals 13
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1520
    const-string v8, "VideoPlayerBtnController"

    const-string v9, "SeekHoverListener onHoverChanged."

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v9

    int-to-long v10, v9

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverDuration:J
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1502(Lcom/sec/android/app/mv/player/view/VideoBtnController;J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    .line 1607
    :cond_0
    :goto_0
    return-void

    .line 1528
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1529
    const-string v8, "VideoPlayerBtnController"

    const-string v9, "SeekHoverListener onHoverChanged - call onStartTrackingHover"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->onStartTrackingHover(Landroid/widget/SeekBar;I)V

    .line 1533
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverDuration:J
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)J

    move-result-wide v8

    int-to-long v10, p2

    mul-long/2addr v8, v10

    const-wide/32 v10, 0x186a0

    div-long/2addr v8, v10

    iput-wide v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->position:J

    .line 1534
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v8

    const/16 v9, 0xbb8

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1536
    sget-boolean v8, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1537
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->position:J

    long-to-int v9, v10

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 1541
    :cond_3
    iget v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->preProgress:I

    invoke-direct {p0, v8, p2}, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->isUpdateNeeded(II)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1542
    const-string v8, "VideoPlayerBtnController"

    const-string v9, "returned"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1546
    :cond_4
    sget-boolean v8, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v8, :cond_5

    .line 1547
    iput p2, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->preProgress:I

    .line 1548
    const-string v8, "VideoPlayerBtnController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onHoverChanged. progress: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1550
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1551
    .local v1, "seekBarRect":Landroid/graphics/Rect;
    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1553
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v9

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v10

    add-int/2addr v9, v10

    sub-int/2addr v8, v9

    mul-int/2addr v8, p2

    int-to-long v8, v8

    const-wide/32 v10, 0x186a0

    div-long/2addr v8, v10

    long-to-int v7, v8

    .line 1555
    .local v7, "x_position":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1556
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v9

    iget v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->mPositionStatus:I

    invoke-virtual {v8, v7, v9, v10, p2}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setRawX(IIII)V

    .line 1557
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 1558
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->position:J

    long-to-int v9, v10

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 1559
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->sendDelayedMoveMessage()V

    .line 1560
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v8

    const/16 v9, 0x384

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->sendDelayedStartMessage(I)V

    goto/16 :goto_0

    .line 1563
    .end local v1    # "seekBarRect":Landroid/graphics/Rect;
    .end local v7    # "x_position":I
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    if-gez v8, :cond_8

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    if-gez v8, :cond_8

    .line 1564
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v5

    .line 1565
    .local v5, "videoWidth":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v4

    .line 1567
    .local v4, "videoHeight":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08010a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 1568
    .local v3, "thumb_width":F
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080109

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 1570
    .local v2, "thumb_height":F
    if-nez v5, :cond_6

    if-eqz v4, :cond_8

    .line 1571
    :cond_6
    if-le v5, v4, :cond_9

    .line 1572
    int-to-float v8, v5

    mul-float/2addr v8, v2

    int-to-float v9, v4

    div-float v3, v8, v9

    .line 1576
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    float-to-int v9, v3

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1802(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I

    .line 1577
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    float-to-int v9, v2

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1902(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I

    .line 1579
    sget-boolean v8, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v8, :cond_7

    .line 1580
    const-string v8, "VideoPlayerBtnController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "videowidth : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " videoheight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const-string v8, "VideoPlayerBtnController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mMeasuredW: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v10}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "mMeasuredH: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v10}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1584
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v9

    add-int v6, v8, v9

    .line 1585
    .local v6, "w":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBottomPadding:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextHeight:I
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v9

    add-int v0, v8, v9

    .line 1587
    .local v0, "h":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v9

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v10, v6, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9, v10}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1592
    .end local v0    # "h":I
    .end local v2    # "thumb_height":F
    .end local v3    # "thumb_width":F
    .end local v4    # "videoHeight":I
    .end local v5    # "videoWidth":I
    .end local v6    # "w":I
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    if-lez v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    if-lez v8, :cond_0

    .line 1593
    iget-wide v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->position:J

    long-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->preProgress:I

    .line 1595
    new-instance v8, Landroid/os/Message;

    invoke-direct {v8}, Landroid/os/Message;-><init>()V

    iput-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    .line 1596
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    const/4 v9, 0x0

    iput v9, v8, Landroid/os/Message;->what:I

    .line 1597
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    iget-wide v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->position:J

    long-to-int v9, v10

    iput v9, v8, Landroid/os/Message;->arg1:I

    .line 1598
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    iput p2, v8, Landroid/os/Message;->arg2:I

    .line 1599
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    new-instance v9, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v10}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v11}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1800(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v12}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1900(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v12

    invoke-direct {v9, v10, v11, v12}, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;-><init>(Ljava/lang/String;II)V

    iput-object v9, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1601
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1602
    sget-object v8, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    iget v9, v9, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_0

    .line 1603
    sget-object v8, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->msg:Landroid/os/Message;

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1574
    .restart local v2    # "thumb_height":F
    .restart local v3    # "thumb_width":F
    .restart local v4    # "videoHeight":I
    .restart local v5    # "videoWidth":I
    :cond_9
    int-to-float v8, v4

    mul-float/2addr v8, v3

    int-to-float v9, v5

    div-float v2, v8, v9

    goto/16 :goto_1
.end method

.method public onStartTrackingHover(Landroid/widget/SeekBar;I)V
    .locals 11
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    .line 1610
    const-string v6, "VideoPlayerBtnController"

    const-string v7, "SeekHoverListener onStartTrackingHover"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1611
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v6, :cond_0

    .line 1612
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-static {}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getInstance()Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v7

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    .line 1615
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoWidth:I
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1802(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I

    .line 1616
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMeasuredVideoHeight:I
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1902(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)I

    .line 1617
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->preProgress:I

    .line 1619
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->isProgressZoomPossible()Z
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1662
    :cond_1
    :goto_0
    return-void

    .line 1623
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    const/4 v7, 0x1

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z

    .line 1625
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v6, :cond_4

    .line 1626
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1627
    .local v1, "mHoverRect":Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1628
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v6

    int-to-long v6, v6

    int-to-long v8, p2

    mul-long/2addr v6, v8

    const-wide/32 v8, 0x186a0

    div-long/2addr v6, v8

    long-to-int v2, v6

    .line 1630
    .local v2, "progress_position":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1631
    .local v3, "seekBarRect":Landroid/graphics/Rect;
    invoke-virtual {p1, v3}, Landroid/widget/SeekBar;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1633
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v7

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    sub-int/2addr v6, v7

    mul-int/2addr v6, p2

    int-to-long v6, v6

    const-wide/32 v8, 0x186a0

    div-long/2addr v6, v8

    long-to-int v5, v6

    .line 1636
    .local v5, "x_position":I
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 1637
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 1638
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v3, v9}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setParam(Landroid/content/Context;ILandroid/graphics/Rect;Landroid/net/Uri;)V

    .line 1639
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->init()V

    .line 1640
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->getVideoViewSize()Z

    .line 1641
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->mPositionStatus:I

    invoke-virtual {v6, v5, v7, v8, p2}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setRawX(IIII)V

    .line 1642
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 1643
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->sendDelayedShowMessage()V

    .line 1659
    .end local v1    # "mHoverRect":Landroid/graphics/Rect;
    .end local v2    # "progress_position":I
    .end local v3    # "seekBarRect":Landroid/graphics/Rect;
    .end local v5    # "x_position":I
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00a6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v10}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getDuration()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1660
    .local v4, "str":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a00a7

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a00a8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1646
    .end local v4    # "str":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1647
    .local v0, "inflate":Landroid/view/LayoutInflater;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 1649
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    const v7, 0x7f03001b

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2402(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/view/View;)Landroid/view/View;

    .line 1650
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1651
    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v6

    const v8, 0x7f0d0094

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v7, v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1652
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    new-instance v7, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1653
    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v6

    const v8, 0x7f0d0093

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v7, v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2702(Lcom/sec/android/app/mv/player/view/VideoBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 1655
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    goto/16 :goto_1
.end method

.method public onStopTrackingHover(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 1665
    const-string v0, "VideoPlayerBtnController"

    const-string v1, "SeekHoverListener onStopTrackingHover"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # setter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mHoverEventStart:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1602(Lcom/sec/android/app/mv/player/view/VideoBtnController;Z)Z

    .line 1668
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mCtrlLayoutState:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1669
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    .line 1671
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->M_SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_2

    .line 1672
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1673
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$18;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mVTPP:Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$1700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 1680
    :cond_1
    :goto_0
    return-void

    .line 1676
    :cond_2
    sget-object v0, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677
    sget-object v0, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method
