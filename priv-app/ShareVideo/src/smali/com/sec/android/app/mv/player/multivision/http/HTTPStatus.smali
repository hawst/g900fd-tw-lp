.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;
.super Ljava/lang/Object;
.source "HTTPStatus.java"


# static fields
.field public static final BAD_REQUEST:I = 0x190

.field public static final CONTINUE:I = 0x64

.field public static final FORBIDDEN:I = 0x193

.field public static final GONE:I = 0x19a

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final INVALID_RANGE:I = 0x1a0

.field public static final NOT_ACCEPTABLE:I = 0x196

.field public static final NOT_FOUND:I = 0x194

.field public static final OK:I = 0xc8

.field public static final PARTIAL_CONTENT:I = 0xce

.field public static final PRECONDITION_FAILED:I = 0x19c

.field public static final REQUEST_TIMEOUT:I = 0x198


# instance fields
.field private mReasonPhrase:Ljava/lang/String;

.field private mStatusCode:I

.field private final mTag:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "lineStr"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mTag:Ljava/lang/String;

    .line 146
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mVersion:Ljava/lang/String;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mStatusCode:I

    .line 150
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mReasonPhrase:Ljava/lang/String;

    .line 139
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->set(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public static final code2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 91
    sparse-switch p0, :sswitch_data_0

    .line 117
    const-string v0, ""

    :goto_0
    return-object v0

    .line 93
    :sswitch_0
    const-string v0, "Continue"

    goto :goto_0

    .line 95
    :sswitch_1
    const-string v0, "OK"

    goto :goto_0

    .line 97
    :sswitch_2
    const-string v0, "Partial Content"

    goto :goto_0

    .line 99
    :sswitch_3
    const-string v0, "Bad Request"

    goto :goto_0

    .line 101
    :sswitch_4
    const-string v0, "Not Found"

    goto :goto_0

    .line 103
    :sswitch_5
    const-string v0, "Precondition Failed"

    goto :goto_0

    .line 105
    :sswitch_6
    const-string v0, "Invalid Range"

    goto :goto_0

    .line 107
    :sswitch_7
    const-string v0, "Internal Server Error"

    goto :goto_0

    .line 109
    :sswitch_8
    const-string v0, "Not Acceptable"

    goto :goto_0

    .line 111
    :sswitch_9
    const-string v0, "Device is Gone"

    goto :goto_0

    .line 113
    :sswitch_a
    const-string v0, "Request Time Out"

    goto :goto_0

    .line 115
    :sswitch_b
    const-string v0, "Forbidden"

    goto :goto_0

    .line 91
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xce -> :sswitch_2
        0x190 -> :sswitch_3
        0x193 -> :sswitch_b
        0x194 -> :sswitch_4
        0x196 -> :sswitch_8
        0x198 -> :sswitch_a
        0x19a -> :sswitch_9
        0x19c -> :sswitch_5
        0x1a0 -> :sswitch_6
        0x1f4 -> :sswitch_7
    .end sparse-switch
.end method

.method public static final isSuccessful(I)Z
    .locals 1
    .param p0, "statCode"    # I

    .prologue
    .line 181
    const/16 v0, 0xc8

    if-gt v0, p0, :cond_0

    const/16 v0, 0x12c

    if-ge p0, v0, :cond_0

    .line 182
    const/4 v0, 0x1

    .line 183
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getReasonPhrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mReasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mStatusCode:I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public set(Ljava/lang/String;)V
    .locals 9
    .param p1, "lineStr"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x1f4

    .line 196
    if-nez p1, :cond_1

    .line 197
    const-string v7, "1.1"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setVersion(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setStatusCode(I)V

    .line 199
    invoke-static {v8}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setReasonPhrase(Ljava/lang/String;)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    :try_start_0
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v7, " "

    invoke-direct {v5, p1, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .local v5, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 208
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 209
    .local v6, "ver":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setVersion(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 213
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 214
    .local v1, "codeStr":Ljava/lang/String;
    const/4 v0, 0x0

    .line 216
    .local v0, "code":I
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 220
    :goto_1
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setStatusCode(I)V

    .line 223
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 225
    .local v4, "reason":Ljava/lang/StringBuffer;
    :goto_2
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 226
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-ltz v7, :cond_2

    .line 227
    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    :cond_2
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 231
    .end local v0    # "code":I
    .end local v1    # "codeStr":Ljava/lang/String;
    .end local v4    # "reason":Ljava/lang/StringBuffer;
    .end local v5    # "st":Ljava/util/StringTokenizer;
    .end local v6    # "ver":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 232
    .local v2, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mTag:Ljava/lang/String;

    const-string v8, "set - Exception(2)"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 217
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "code":I
    .restart local v1    # "codeStr":Ljava/lang/String;
    .restart local v5    # "st":Ljava/util/StringTokenizer;
    .restart local v6    # "ver":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 218
    .local v3, "e1":Ljava/lang/Exception;
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mTag:Ljava/lang/String;

    const-string v8, "set  - Exception(1)"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 230
    .end local v3    # "e1":Ljava/lang/Exception;
    .restart local v4    # "reason":Ljava/lang/StringBuffer;
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->setReasonPhrase(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method public setReasonPhrase(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mReasonPhrase:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mStatusCode:I

    .line 158
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->mVersion:Ljava/lang/String;

    .line 154
    return-void
.end method
