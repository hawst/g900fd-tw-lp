.class public Lcom/sec/android/app/mv/player/view/VideoTitleController;
.super Landroid/widget/RelativeLayout;
.source "VideoTitleController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;,
        Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;
    }
.end annotation


# static fields
.field private static final FADE_OUT_MV_VOLUME_BAR:I = 0xb

.field private static final FADE_OUT_VOLUME_BAR:I = 0xa

.field private static final FADE_OUT_VOLUME_BAR_DELAY:I = 0x7d0

.field private static final HOVER_DETECT_TIME_MS:I = 0x12c

.field private static final LAYOUT_ID:I = 0x12c

.field public static final POPUP_VOLUMEBAR_FOCUSABLE:Z = true

.field public static final POPUP_VOLUMEBAR_UNFOCUSABLE:Z = false

.field private static final TAG:Ljava/lang/String; = "VideoTitleController"

.field public static final mRefreshingDelay:I = 0x3e8

.field private static mShowing:Z


# instance fields
.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonEnable:Z

.field private mContext:Landroid/content/Context;

.field private mDeviceVolumeDialog:Landroid/app/DialogFragment;

.field private mHandler:Landroid/os/Handler;

.field private mLongPressChecker:Ljava/lang/Runnable;

.field private mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field public mPopupVolBar:Landroid/widget/PopupWindow;

.field public mRefreshingCheckTime:I

.field private mRoot:Landroid/view/View;

.field private mRotationBtn:Landroid/widget/ImageButton;

.field private mRotationBtnClickListener:Landroid/view/View$OnClickListener;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mTitleKeyListener:Landroid/view/View$OnKeyListener;

.field private mTitleLayout:Landroid/widget/RelativeLayout;

.field private mTitleText:Landroid/widget/TextView;

.field private mTitleTextLayout:Landroid/widget/RelativeLayout;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVolPopupLayout:Landroid/view/View;

.field public mVolumeBtn:Landroid/widget/ImageButton;

.field private mVolumeBtnClickListener:Landroid/view/View$OnClickListener;

.field private mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

.field public mVolumeSeekBarPopup:Landroid/widget/SeekBar;

.field private mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

.field public mVolumeTextPopup:Landroid/widget/TextView;

.field mWifiLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mButtonEnable:Z

    .line 114
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mWifiLevel:I

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 603
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$4;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtnClickListener:Landroid/view/View$OnClickListener;

    .line 636
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$5;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleKeyListener:Landroid/view/View$OnKeyListener;

    .line 665
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$6;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    .line 726
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$7;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 747
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$8;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 755
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$9;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 881
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$10;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtnClickListener:Landroid/view/View$OnClickListener;

    .line 132
    iput-object p0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    .line 133
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    .line 134
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 135
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->initFloatingWindow()V

    .line 143
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/VideoTitleController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeBtnPopup()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/view/VideoTitleController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->toggleRotateScreen()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->toggleRotateSecondScreen()V

    return-void
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xbb8

    .line 817
    const/4 v0, 0x0

    .line 818
    .local v0, "retVal":Z
    packed-switch p1, :pswitch_data_0

    .line 842
    :goto_0
    return v0

    .line 823
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 835
    :goto_1
    const/4 v0, 0x1

    .line 836
    goto :goto_0

    .line 825
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 829
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_1

    .line 818
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 823
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v8, 0x12c

    const/4 v7, 0x4

    const/4 v4, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 226
    const v2, 0x7f0d00ae

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    .line 228
    const v2, 0x7f0d00b5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 229
    .local v1, "viewGroup":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 232
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030027

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    .line 234
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->changeTitleLayoutForUpgradeModels(Landroid/content/Context;Landroid/view/View;)V

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-nez v2, :cond_6

    .line 237
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    const v3, 0x1030002

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 243
    :goto_0
    const v2, 0x7f0d00b4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    .line 244
    const v2, 0x7f0d00b2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v3, 0x7f0d00ba

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeTextPopup:Landroid/widget/TextView;

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v3, 0x7f0d00b8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v2, :cond_0

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v2, v4}, Landroid/widget/SeekBar;->setMode(I)V

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 256
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v2, :cond_1

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x5153

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 266
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v2

    if-nez v2, :cond_7

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v3, 0x7f02003c

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 272
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 276
    :cond_2
    const v2, 0x7f0d00b3

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 279
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 284
    :cond_3
    const v2, 0x7f0d00b1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_5

    .line 286
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_9

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateAutoRotationBtn()V

    .line 296
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a003d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 299
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_TOOLTIP_HOVERING:Z

    if-eqz v2, :cond_4

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    const/16 v3, 0x5153

    invoke-virtual {v2, v3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 306
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 308
    :cond_5
    return-void

    .line 240
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    goto/16 :goto_0

    .line 269
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v3, 0x7f02003b

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 290
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateSecondScreenAutoRotationBtn()V

    goto :goto_2

    .line 293
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateAutoRotationBtn()V

    goto :goto_2
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 156
    .local v0, "mDecor":Landroid/view/View;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 158
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    :cond_0
    return-void
.end method

.method public static isShowing()Z
    .locals 1

    .prologue
    .line 344
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    return v0
.end method

.method private setReleaseRotateScreen()V
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->releaseRotationScreen()V

    .line 921
    :cond_0
    return-void
.end method

.method private setReleaseRotateSecondScreen()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->releaseRotationScreen()V

    .line 929
    :cond_0
    return-void
.end method

.method private setRotationBtnDisable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0d00b1

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 459
    return-void
.end method

.method private setRotationBtnEnable()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const v2, 0x7f0d00b1

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 451
    return-void
.end method

.method private setVolDisable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0d00b2

    .line 433
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeSVMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    :goto_0
    return-void

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private setVolEnable()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const v2, 0x7f0d00b2

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 430
    return-void
.end method

.method private setVolumeBtnPopup()V
    .locals 4

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 513
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeTextPopup:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a003e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 516
    :cond_0
    return-void

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f02003b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private toggleRotateScreen()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 900
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getScreenOrientation()I

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen(I)V

    .line 903
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getScreenOrientation()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setUserOrientation(I)V

    .line 905
    :cond_1
    return-void
.end method

.method private toggleRotateSecondScreen()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getSecondScreenOrientation()I

    move-result v2

    if-ne v2, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateSecondScreen(I)V

    .line 911
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getSecondScreenOrientation()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setSecondScreenUserOrientation(I)V

    .line 913
    :cond_1
    return-void
.end method


# virtual methods
.method public changeAutoRotationBtn(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 874
    if-eqz p1, :cond_1

    .line 875
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 879
    :cond_0
    :goto_0
    return-void

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public checkShowingAndDismissPopupVolBar()Z
    .locals 3

    .prologue
    .line 473
    const-string v1, "VideoTitleController"

    const-string v2, "checkShowingAndDismissPopupVolBar E:"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 476
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 482
    :goto_0
    const/4 v1, 0x1

    .line 485
    :goto_1
    return v1

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 479
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 480
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 485
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public checkShowingAndDismissPopupVolMVBar()Z
    .locals 1

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isDialogFragmentShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 491
    const/4 v0, 0x1

    .line 493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public forceHide()V
    .locals 1

    .prologue
    .line 378
    sget-boolean v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    if-eqz v0, :cond_0

    .line 384
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setInvisibleAllViews()V

    .line 386
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    .line 388
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 348
    sget-boolean v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    if-eqz v1, :cond_0

    .line 349
    const/4 v0, 0x0

    .line 350
    .local v0, "translateOff":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 351
    .restart local v0    # "translateOff":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 352
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 353
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 354
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 355
    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoTitleController$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 373
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    .line 375
    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public hide(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 391
    if-eqz p1, :cond_0

    .line 397
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 398
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    .line 400
    :cond_0
    return-void
.end method

.method public hideVolumeBarPopup()V
    .locals 4

    .prologue
    const/16 v1, 0xa

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 499
    return-void
.end method

.method public hideVolumeMVBarPopup()V
    .locals 4

    .prologue
    const/16 v1, 0xb

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 504
    return-void
.end method

.method protected isAllSharePlayMode()Z
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public isDialogFragmentShowing()Z
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVolumeBarShowing()Z
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    const/4 v0, 0x1

    .line 798
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    const-string v1, "VideoTitleController"

    const-string v2, "makeControllerView"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 208
    .local v0, "inflate":Landroid/view/LayoutInflater;
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_0

    .line 209
    const v1, 0x7f030026

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    .line 214
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setPadding()V

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->initControllerView(Landroid/view/View;)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    return-object v1

    .line 211
    :cond_0
    const v1, 0x7f030025

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    goto :goto_0
.end method

.method public makeToggleMute()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 802
    invoke-static {}, Landroid/media/AudioManager;->isMediaSilentMode()Z

    move-result v0

    .line 803
    .local v0, "isMute":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Landroid/media/AudioManager;->setMediaSilentMode(Z)V

    .line 804
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2, v2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 805
    return-void

    :cond_0
    move v1, v2

    .line 803
    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->initControllerView(Landroid/view/View;)V

    .line 151
    :cond_0
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    .line 808
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 809
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 810
    return-void
.end method

.method public removeLongPressCallback()V
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mLongPressChecker:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mLongPressChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 848
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mLongPressChecker:Ljava/lang/Runnable;

    .line 850
    :cond_0
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 166
    const-string v3, "VideoTitleController"

    const-string v4, "setAnchorView"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->removeAllViews()V

    .line 169
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 172
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 173
    .local v2, "v":Landroid/view/View;
    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 174
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 177
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 179
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 182
    :cond_0
    const v3, 0x7f0d00af

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/mv/player/view/VideoTitleController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 203
    return-void
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 595
    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 587
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 585
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mButtonEnable:Z

    .line 586
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 814
    return-void
.end method

.method protected setInvisibleAllViews()V
    .locals 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setInvisibleNormalLayout()V

    .line 405
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setNullBackgroundColor(Lcom/sec/android/app/mv/player/common/VUtils$myView;)V

    .line 407
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    const-string v1, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Ljava/lang/String;)V

    .line 410
    :cond_0
    return-void
.end method

.method protected setInvisibleNormalLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 418
    return-void
.end method

.method public setPadding()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const/16 v1, 0x53

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 222
    :cond_0
    return-void
.end method

.method protected setVisibleAllViews()V
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibleAllViews(Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;)V

    .line 422
    return-void
.end method

.method protected setVisibleAllViews(Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;)V
    .locals 3
    .param p1, "layout"    # Lcom/sec/android/app/mv/player/view/VideoTitleController$LayoutMode;

    .prologue
    const/4 v2, 0x0

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 469
    return-void
.end method

.method public setVolumeSeekbarLevel()V
    .locals 2

    .prologue
    .line 778
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 779
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeSame()V

    .line 786
    :cond_0
    :goto_0
    return-void

    .line 782
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 783
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v0

    .line 784
    .local v0, "vol":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 316
    .local v0, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080209

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 322
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 324
    sget-boolean v2, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    if-nez v2, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibleAllViews()V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolume()V

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateControllersForSV()V

    .line 331
    const/4 v1, 0x0

    .line 332
    .local v1, "translateOn":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->getHeight()I

    move-result v2

    mul-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    invoke-direct {v1, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 333
    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 334
    invoke-virtual {v1}, Landroid/view/animation/Animation;->startNow()V

    .line 335
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 336
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVisibility(I)V

    .line 338
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mShowing:Z

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTitleName()V

    .line 341
    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    :cond_0
    return-void

    .line 320
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080208

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    goto :goto_0
.end method

.method public showPopupSVVolbar()V
    .locals 5

    .prologue
    .line 520
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getInstance()Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    .line 522
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "volume_dialog"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    .line 523
    .local v0, "dFragment":Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;
    if-nez v0, :cond_0

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v4, "volume_dialog"

    invoke-virtual {v3, v2, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    .end local v0    # "dFragment":Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;
    :cond_0
    :goto_0
    return-void

    .line 526
    :catch_0
    move-exception v1

    .line 527
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public showPopupVolbar(Z)V
    .locals 6
    .param p1, "focused_state"    # Z

    .prologue
    .line 533
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v4, :cond_2

    .line 534
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 536
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d00b6

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 538
    .local v3, "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 541
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v4, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 542
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    new-instance v5, Lcom/sec/android/app/mv/player/view/VideoTitleController$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController$3;-><init>(Lcom/sec/android/app/mv/player/view/VideoTitleController;)V

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 554
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeBtnPopup()V

    .line 556
    const/16 v0, 0x35

    .line 557
    .local v0, "gravity":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 558
    .local v2, "margin_Y":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08018f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 560
    .local v1, "margin":I
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v4, :cond_0

    .line 561
    const/16 v0, 0x33

    .line 562
    const/4 v1, 0x0

    .line 565
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 567
    const/16 v1, 0x76

    .line 570
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 572
    .end local v0    # "gravity":I
    .end local v1    # "margin":I
    .end local v2    # "margin_Y":I
    .end local v3    # "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void
.end method

.method public updateAutoRotationBtn()V
    .locals 2

    .prologue
    .line 853
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 854
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 857
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateControllersForSV()V
    .locals 3

    .prologue
    .line 683
    const-string v0, "VideoTitleController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateControllersForSV getDeviceSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v0

    if-nez v0, :cond_0

    .line 724
    :goto_0
    return-void

    .line 691
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 692
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_4

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 694
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setReleaseRotateScreen()V

    .line 702
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setRotationBtnEnable()V

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/mv/player/view/VideoBtnController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/mv/player/view/VideoBtnController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setFitBtnEnable()V

    .line 716
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolEnable()V

    .line 718
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isVolumeMute()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 719
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolDisable()V

    .line 722
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolMVBar()Z

    goto :goto_0

    .line 696
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setReleaseRotateSecondScreen()V

    goto :goto_1

    .line 699
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setReleaseRotateScreen()V

    goto :goto_1

    .line 708
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setRotationBtnDisable()V

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/mv/player/view/VideoBtnController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getBtnInstance()Lcom/sec/android/app/mv/player/view/VideoBtnController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->setFitBtnDisable()V

    goto :goto_2
.end method

.method public updateSecondScreenAutoRotationBtn()V
    .locals 2

    .prologue
    .line 863
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenAutoRotation(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 865
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 870
    :cond_0
    :goto_0
    return-void

    .line 867
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mRotationBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 792
    return-void
.end method

.method public updateVolume()V
    .locals 0

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeBtnPopup()V

    .line 599
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeBarPopup()V

    .line 601
    return-void
.end method

.method public updateVolumeDilaogFrag(II)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 979
    const/4 v0, 0x0

    .line 980
    .local v0, "isNotifyRequired":Z
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->setClientVolumeValue(II)Z

    move-result v0

    .line 982
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mDeviceVolumeDialog:Landroid/app/DialogFragment;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 983
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->getInstance()Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;

    move-result-object v1

    # getter for: Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;->access$900(Lcom/sec/android/app/mv/player/view/VideoTitleController$DeviceVolumeDialogFragment;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->notifyDataSetChanged()V

    .line 985
    :cond_0
    return-void
.end method
