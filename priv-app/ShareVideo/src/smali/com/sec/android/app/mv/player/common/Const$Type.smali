.class public final enum Lcom/sec/android/app/mv/player/common/Const$Type;
.super Ljava/lang/Enum;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/common/Const;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/mv/player/common/Const$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/mv/player/common/Const$Type;

.field public static final enum CLIENT:Lcom/sec/android/app/mv/player/common/Const$Type;

.field public static final enum SERVER:Lcom/sec/android/app/mv/player/common/Const$Type;

.field public static final enum UNDEFINED:Lcom/sec/android/app/mv/player/common/Const$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/sec/android/app/mv/player/common/Const$Type;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/mv/player/common/Const$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->SERVER:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 56
    new-instance v0, Lcom/sec/android/app/mv/player/common/Const$Type;

    const-string v1, "CLIENT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/mv/player/common/Const$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->CLIENT:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 57
    new-instance v0, Lcom/sec/android/app/mv/player/common/Const$Type;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/mv/player/common/Const$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->UNDEFINED:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/mv/player/common/Const$Type;

    sget-object v1, Lcom/sec/android/app/mv/player/common/Const$Type;->SERVER:Lcom/sec/android/app/mv/player/common/Const$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/mv/player/common/Const$Type;->CLIENT:Lcom/sec/android/app/mv/player/common/Const$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/mv/player/common/Const$Type;->UNDEFINED:Lcom/sec/android/app/mv/player/common/Const$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->$VALUES:[Lcom/sec/android/app/mv/player/common/Const$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/mv/player/common/Const$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/mv/player/common/Const$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/common/Const$Type;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/mv/player/common/Const$Type;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->$VALUES:[Lcom/sec/android/app/mv/player/common/Const$Type;

    invoke-virtual {v0}, [Lcom/sec/android/app/mv/player/common/Const$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/mv/player/common/Const$Type;

    return-object v0
.end method
