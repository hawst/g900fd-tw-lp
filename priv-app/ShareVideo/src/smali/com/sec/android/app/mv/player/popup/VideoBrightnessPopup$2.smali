.class Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 120
    sparse-switch p2, :sswitch_data_0

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 122
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_1

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->revertBrightness()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 124
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$300(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$300(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatusForOnKeyEvent(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 134
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->revertBrightness()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 136
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 147
    goto :goto_0

    .line 152
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 155
    goto :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
        0xa4 -> :sswitch_3
    .end sparse-switch
.end method
