.class Lcom/sec/android/app/mv/player/view/MainVideoView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v4, 0x44fa0000    # 2000.0f

    const/high16 v3, 0x43480000    # 200.0f

    const/4 v2, 0x1

    .line 268
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x44bb8000    # 1500.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 280
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    add-int/lit16 v1, v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->rightFlickView()V

    goto :goto_0

    .line 289
    :catch_0
    move-exception v0

    goto :goto_0

    .line 284
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    add-int/lit16 v1, v1, -0x1388

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$1;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/view/MainVideoView;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->leftFlickView()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
