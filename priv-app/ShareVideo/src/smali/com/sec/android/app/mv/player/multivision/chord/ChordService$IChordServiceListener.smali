.class public interface abstract Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
.super Ljava/lang/Object;
.source "ChordService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IChordServiceListener"
.end annotation


# static fields
.field public static final CANCELLED:I = 0x2

.field public static final FAILED:I = 0x4

.field public static final FINISH:I = 0x5

.field public static final RECEIVED:I = 0x1

.field public static final REJECTED:I = 0x3

.field public static final SENT:I


# virtual methods
.method public abstract onConnectivityChanged()V
.end method

.method public abstract onFileCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFileProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
.end method

.method public abstract onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V
.end method

.method public abstract onFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
.end method

.method public abstract onNetworkDisconnected()V
.end method

.method public abstract onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract onReceiveMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method
