.class Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;
.super Landroid/os/Handler;
.source "BitmapAtTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/util/BitmapAtTime;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final mhandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/BitmapAtTime;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/util/BitmapAtTime;)V
    .locals 1

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->this$0:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->this$0:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->mMainHandler:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->mhandler:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 28
    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    .line 29
    .local v5, "retmsg":Landroid/os/Message;
    const/4 v0, 0x0

    .line 31
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    .line 72
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 73
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->mhandler:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    .line 74
    .local v2, "h":Landroid/os/Handler;
    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 77
    .end local v2    # "h":Landroid/os/Handler;
    :cond_1
    return-void

    .line 33
    :pswitch_0
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;

    .line 34
    .local v4, "r":Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->getWidth()I

    move-result v8

    .line 35
    .local v8, "width":I
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->getHeight()I

    move-result v3

    .line 37
    .local v3, "height":I
    if-lez v8, :cond_0

    if-lez v3, :cond_0

    .line 38
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->this$0:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    iget v10, p1, Landroid/os/Message;->arg1:I

    int-to-long v10, v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    # setter for: Lcom/sec/android/app/mv/player/util/BitmapAtTime;->pos:J
    invoke-static {v9, v10, v11}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->access$002(Lcom/sec/android/app/mv/player/util/BitmapAtTime;J)J

    .line 39
    new-instance v6, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v6}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 42
    .local v6, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 43
    const/16 v9, 0x18

    invoke-virtual {v6, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    .line 44
    .local v7, "rotation":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-eqz v9, :cond_2

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/16 v10, 0xb4

    if-ne v9, v10, :cond_3

    .line 45
    :cond_2
    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v6, v8, v3, v9, v10}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 50
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/mv/player/util/BitmapAtTime$1;->this$0:Lcom/sec/android/app/mv/player/util/BitmapAtTime;

    # getter for: Lcom/sec/android/app/mv/player/util/BitmapAtTime;->pos:J
    invoke-static {v9}, Lcom/sec/android/app/mv/player/util/BitmapAtTime;->access$000(Lcom/sec/android/app/mv/player/util/BitmapAtTime;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 54
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 57
    .end local v7    # "rotation":Ljava/lang/String;
    :goto_2
    const/4 v9, 0x0

    iput v9, v5, Landroid/os/Message;->what:I

    .line 58
    iput-object v0, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 59
    iget v9, p1, Landroid/os/Message;->arg1:I

    iput v9, v5, Landroid/os/Message;->arg1:I

    .line 60
    iget v9, p1, Landroid/os/Message;->arg2:I

    iput v9, v5, Landroid/os/Message;->arg2:I

    goto :goto_0

    .line 47
    .restart local v7    # "rotation":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x1

    const/4 v10, 0x1

    :try_start_1
    invoke-virtual {v6, v3, v8, v9, v10}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 51
    .end local v7    # "rotation":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 52
    .local v1, "ex":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_2

    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    invoke-virtual {v6}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v9

    .line 65
    .end local v3    # "height":I
    .end local v4    # "r":Lcom/sec/android/app/mv/player/util/BitmapAtTimeMsg;
    .end local v6    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v8    # "width":I
    :pswitch_1
    const-string v9, "BitmapAtTime"

    const-string v10, "thread. quit() "

    invoke-static {v9, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
