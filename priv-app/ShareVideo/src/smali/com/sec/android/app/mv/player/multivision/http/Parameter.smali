.class public Lcom/sec/android/app/mv/player/multivision/http/Parameter;
.super Ljava/lang/Object;
.source "Parameter.java"


# instance fields
.field private mName:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mName:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mValue:Ljava/lang/String;

    .line 31
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->setName(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->setValue(Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/Parameter;->mValue:Ljava/lang/String;

    .line 53
    return-void
.end method
