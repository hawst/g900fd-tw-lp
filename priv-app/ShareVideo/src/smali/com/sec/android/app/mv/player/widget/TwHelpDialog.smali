.class public Lcom/sec/android/app/mv/player/widget/TwHelpDialog;
.super Landroid/app/Dialog;
.source "TwHelpDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/widget/TwHelpDialog$1;,
        Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;
    }
.end annotation


# instance fields
.field private fPunchEvent:Z

.field private mIgnoreHoverEvt:Z

.field private mShowWrongInputToast:Z

.field private mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

.field private mWrongInputToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    const v0, 0x7f0b0008

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 29
    sget-object v0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    .line 33
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 35
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    .line 37
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 59
    sget-object v0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->TRANSPARENT:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    .line 61
    const v0, 0x7f0a003b

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    .line 63
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->setCancelable(Z)V

    .line 42
    invoke-virtual {p0, p3}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 43
    return-void
.end method


# virtual methods
.method public getShowWrongInputToast()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    return v0
.end method

.method public getTouchTransparencyMode()Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    return-object v0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    const/16 v5, 0x9

    const/4 v4, 0x7

    const/4 v3, 0x0

    .line 137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 139
    .local v0, "action":I
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    if-eqz v1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v3

    .line 143
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_2

    .line 144
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    .line 149
    :cond_2
    sget-object v1, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$1;->$SwitchMap$com$sec$android$app$mv$player$widget$TwHelpDialog$TouchMode:[I

    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 178
    :cond_3
    :goto_1
    if-eq v0, v6, :cond_4

    if-eq v0, v5, :cond_0

    .line 179
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    goto :goto_0

    .line 151
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_5

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 154
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_3

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 161
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_7

    if-eq v0, v5, :cond_6

    if-ne v0, v4, :cond_7

    .line 163
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 165
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v6, :cond_3

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 90
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 92
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v1

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 93
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    .line 98
    :cond_0
    sget-object v1, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$1;->$SwitchMap$com$sec$android$app$mv$player$widget$TwHelpDialog$TouchMode:[I

    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 125
    :cond_1
    :goto_0
    if-eq v0, v5, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    if-eq v0, v3, :cond_3

    .line 129
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    .line 131
    :cond_3
    return v4

    .line 100
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_4

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 103
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 110
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->fPunchEvent:Z

    if-eqz v1, :cond_5

    if-eq v0, v3, :cond_5

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 113
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    if-eq v0, v3, :cond_1

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mWrongInputToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setIgnoreHoverEvt(Z)V
    .locals 0
    .param p1, "bIgnoreHoverEvt"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mIgnoreHoverEvt:Z

    .line 85
    return-void
.end method

.method public setShowWrongInputToast(Z)V
    .locals 0
    .param p1, "pShowWrongInputToast"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mShowWrongInputToast:Z

    .line 77
    return-void
.end method

.method public setTouchTransparencyMode(Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;)V
    .locals 0
    .param p1, "touchTransparencyMode"    # Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->mTouchTransparencyMode:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    .line 69
    return-void
.end method
