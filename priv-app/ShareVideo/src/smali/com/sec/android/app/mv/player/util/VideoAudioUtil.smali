.class public Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
.super Ljava/lang/Object;
.source "VideoAudioUtil.java"


# static fields
.field private static final PLAYBACK_TYPE_LOCAL:I = 0x0

.field private static final PLAYBACK_TYPE_REMOTE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoAudioUtil"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mRouteTypes:I

.field private mRouter:Landroid/media/MediaRouter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    .line 32
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    .line 33
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    .line 34
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->setRouteTypes(I)V

    .line 35
    return-void
.end method

.method public static isAllSoundOff(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 203
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "all_sound_off"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 125
    return-void
.end method

.method public dismissVolumePanel()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->dismissVolumePanel()V

    .line 141
    return-void
.end method

.method public gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .locals 3
    .param p1, "listener"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    return v0
.end method

.method public getCurrentVolume()I
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public getImplicitVideoVolume()F
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=7;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getMaxVolume()I
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public getRingerMode()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    return v0
.end method

.method public isAudioPathBT()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit16 v2, v2, 0x380

    if-nez v2, :cond_2

    .line 152
    .local v0, "isBt":Z
    :goto_1
    goto :goto_0

    .line 150
    .end local v0    # "isBt":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isAudioPathBTModeSCO()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 162
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x20

    if-nez v2, :cond_2

    .line 164
    .local v0, "isBt":Z
    :goto_1
    goto :goto_0

    .line 162
    .end local v0    # "isBt":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public isExtraSpeakerDockOn()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isExtraSpeakerDockOn()Z

    move-result v0

    return v0
.end method

.method public isPauseWorkByMotion()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 78
    const/4 v0, 0x1

    .line 79
    .local v0, "bRet":Z
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const-string v4, "audioParam;outDevice"

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/lit16 v3, v3, 0x180

    if-nez v3, :cond_4

    .line 86
    .local v1, "isBt":Z
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v1, :cond_3

    .line 87
    :cond_2
    const/4 v0, 0x0

    :cond_3
    move v1, v0

    .line 90
    goto :goto_0

    .line 84
    .end local v1    # "isBt":Z
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isRecordActive()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v0

    return v0
.end method

.method public isWiredConnected()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method public registerBluetoothButtonReceiver()V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 105
    return-void
.end method

.method public selectRouteInt(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    .line 175
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    if-nez v4, :cond_0

    .line 176
    const-string v4, "VideoAudioUtil"

    const-string v5, "selectRouteInt : mRouter is NULL!!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :goto_0
    return-void

    .line 180
    :cond_0
    const/4 v3, 0x0

    .line 182
    .local v3, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    if-nez p1, :cond_2

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v3

    .line 195
    :cond_1
    if-eqz v3, :cond_4

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouteTypes:I

    invoke-virtual {v4, v5, v3}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    .line 185
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v0

    .line 186
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 187
    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4, v1}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v2

    .line 188
    .local v2, "route":Landroid/media/MediaRouter$RouteInfo;
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouteTypes:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouter:Landroid/media/MediaRouter;

    invoke-virtual {v4}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v4

    if-eq v2, v4, :cond_3

    .line 189
    move-object v3, v2

    .line 190
    const-string v4, "VideoAudioUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "selectRouteInt : Route Selected! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 198
    .end local v0    # "N":I
    .end local v1    # "i":I
    .end local v2    # "route":Landroid/media/MediaRouter$RouteInfo;
    :cond_4
    const-string v4, "VideoAudioUtil"

    const-string v5, "selectRouteInt : routeInfo is NULL!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAudioPathBT()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->selectRouteInt(I)V

    .line 113
    return-void
.end method

.method public setAudioPathDevice()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->selectRouteInt(I)V

    .line 117
    return-void
.end method

.method public setRingerMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 129
    return-void
.end method

.method public setRouteTypes(I)V
    .locals 0
    .param p1, "types"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mRouteTypes:I

    .line 172
    return-void
.end method

.method public setVolume(I)Z
    .locals 3
    .param p1, "level"    # I

    .prologue
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    if-eq p1, v1, :cond_0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unregisterBluetoothButtonReceiver()V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 109
    return-void
.end method

.method public volumeDown()V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 43
    return-void
.end method

.method public volumeSame()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 47
    return-void
.end method

.method public volumeUp()V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 39
    return-void
.end method
