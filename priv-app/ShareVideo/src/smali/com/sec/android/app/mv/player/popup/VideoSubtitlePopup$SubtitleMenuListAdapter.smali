.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;
.super Landroid/widget/SimpleAdapter;
.source "VideoSubtitlePopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleMenuListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V
    .locals 6
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "resource"    # I
    .param p5, "key"    # [Ljava/lang/String;
    .param p6, "values"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 316
    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 317
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 320
    if-nez p2, :cond_0

    .line 321
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 322
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030022

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 325
    .end local v3    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 326
    const-string v4, "VideoSubtitlePopup"

    const-string v5, "SubtitleMenuListAdapter : getView() - getCount() is 0"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :goto_0
    return-object p2

    .line 330
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 331
    .local v0, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_2

    .line 332
    const-string v4, "VideoSubtitlePopup"

    const-string v5, "SubtitleMenuListAdapter : getView() - item is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 336
    :cond_2
    const v4, 0x7f0d00a1

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 337
    .local v1, "menuName":Landroid/widget/TextView;
    const v4, 0x7f0d00a2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 339
    .local v2, "menuValue":Landroid/widget/TextView;
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a0040

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 342
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 343
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->getMultiSelectedSubtitleLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 345
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 347
    :cond_4
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a0030

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedFont()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 349
    :cond_5
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a008f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 350
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getColorValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 351
    :cond_6
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a0031

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 352
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSizeValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 353
    :cond_7
    const-string v4, "MENU_TITLE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a008e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 354
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getColorValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 356
    :cond_8
    const-string v4, "MENU_VALUE"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
