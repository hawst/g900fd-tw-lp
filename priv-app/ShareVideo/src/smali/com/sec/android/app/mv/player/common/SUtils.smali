.class public Lcom/sec/android/app/mv/player/common/SUtils;
.super Ljava/lang/Object;
.source "SUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/common/SUtils$1;,
        Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;,
        Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SUtils"

.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/common/SUtils;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

.field private mGUIHandler:Landroid/os/Handler;

.field private mInitialized:Z

.field private mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

.field private mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mType:Lcom/sec/android/app/mv/player/common/Const$Type;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/android/app/mv/player/common/SUtils;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/common/SUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/common/SUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/SUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->UNDEFINED:Lcom/sec/android/app/mv/player/common/Const$Type;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mType:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mInitialized:Z

    .line 423
    iput-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    .line 42
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/mv/player/common/SUtils;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/mv/player/common/SUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/SUtils;

    return-object v0
.end method


# virtual methods
.method public exitMVGroup()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup()V

    .line 273
    return-void
.end method

.method public exitShareVideo()V
    .locals 3

    .prologue
    .line 513
    const-string v1, "SUtils"

    const-string v2, "exitShareVideo E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 515
    const-string v1, "SUtils"

    const-string v2, "exitShareVideo mContext is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :goto_0
    return-void

    .line 519
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "GROUPPLAY GAMER LEFT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 520
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.groupcast"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 521
    const-string v1, "GAME NAME"

    const-string v2, "ShareVideo"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public getClientVolumeValue(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getClientVolumeValue(I)I

    move-result v0

    return v0
.end method

.method public getContentURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getContentURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurPlayingPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDeviceInfo(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceInfoForListview(I)Ljava/lang/Object;
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "currentDevice":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceID()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->getID()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 288
    move v0, v1

    .line 293
    :cond_0
    if-nez p1, :cond_2

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    move-result-object v2

    .line 300
    :goto_1
    return-object v2

    .line 286
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_2
    if-gt p1, v0, :cond_3

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    move-result-object v2

    goto :goto_1

    .line 300
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceInfo(I)Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    move-result-object v2

    goto :goto_1
.end method

.method public getDeviceSize()I
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceSize()I

    move-result v0

    return v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getId()I

    move-result v0

    return v0
.end method

.method public getMVMembersNumber()I
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v0

    return v0
.end method

.method public getMasterVolumeValue()I
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMasterVolumeValue()I

    move-result v0

    return v0
.end method

.method public getMeasuredHeightRatio()I
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMeasuredHeightRatio()I

    move-result v0

    return v0
.end method

.method public getMediaStreamMaxVolume()I
    .locals 2

    .prologue
    .line 334
    const/4 v0, -0x1

    .line 335
    .local v0, "max":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getMaxVolume()I

    move-result v0

    .line 338
    :cond_0
    return v0
.end method

.method public getServerTimestampInfo()[J
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getServerTimestampInfo()[J

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoDbId()J
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoDbId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getVolumeValue()I
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public hide()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->hide()Z

    move-result v0

    return v0
.end method

.method public isClient()Z
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mType:Lcom/sec/android/app/mv/player/common/Const$Type;

    sget-object v1, Lcom/sec/android/app/mv/player/common/Const$Type;->CLIENT:Lcom/sec/android/app/mv/player/common/Const$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiVisionMode()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isMultiVisionMode()Z

    move-result v0

    return v0
.end method

.method public isNextAvailable()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isNextAvailable()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRequestMultiVision()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isRequestMultiVision()Z

    move-result v0

    return v0
.end method

.method public isServer()Z
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mType:Lcom/sec/android/app/mv/player/common/Const$Type;

    sget-object v1, Lcom/sec/android/app/mv/player/common/Const$Type;->SERVER:Lcom/sec/android/app/mv/player/common/Const$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShareVideoMode()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isShareVideoMode()Z

    move-result v0

    .line 199
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSingleVisionMode()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSingleVisionMode()Z

    move-result v0

    return v0
.end method

.method public isSupportMultiVision()Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSupportMultiVision()Z

    move-result v0

    return v0
.end method

.method public isVolumeMute()Z
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVolumeSVMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceSize()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public joinMVGroup()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->joinMVGroup()V

    .line 277
    return-void
.end method

.method public joinShareVideo()V
    .locals 3

    .prologue
    .line 499
    const-string v1, "SUtils"

    const-string v2, "joinShareVideo E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 502
    const-string v1, "SUtils"

    const-string v2, "joinShareVideo mContext is null!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :goto_0
    return-void

    .line 506
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "GROUPPLAY GAMER JOINED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 507
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.groupcast"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    const-string v1, "GAME NAME"

    const-string v2, "ShareVideo"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 509
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public notifyDeviceChange()V
    .locals 2

    .prologue
    .line 437
    const-string v0, "SUtils"

    const-string v1, "notifyDeviceChange"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;->notifyChange()V

    .line 439
    return-void
.end method

.method public notifyDeviceChangedToActivity()V
    .locals 2

    .prologue
    .line 260
    const-string v0, "SUtils"

    const-string v1, "notifyDeviceChangedToActivity E"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 265
    :cond_0
    return-void
.end method

.method public notifyMessage(IIILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # Ljava/lang/String;

    .prologue
    .line 342
    const-string v0, "SUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyMessage E. type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    if-eqz v0, :cond_0

    .line 345
    packed-switch p1, :pswitch_data_0

    .line 392
    :goto_0
    return-void

    .line 347
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onStartShareVideo()V

    goto :goto_0

    .line 351
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onStopShareVideo(I)V

    goto :goto_0

    .line 355
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p2, p3}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onNeedMultiVisionView(II)V

    goto :goto_0

    .line 359
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p4}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onOpenPath(Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onCurrentContentStop()V

    goto :goto_0

    .line 367
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p2, p3, p4}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onMultiVisionDeviceDisconnected(IILjava/lang/String;)V

    goto :goto_0

    .line 371
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onSetVolumeMute()V

    goto :goto_0

    .line 375
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onSetVolumeOn()V

    goto :goto_0

    .line 379
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onSendVolumeValue(I)V

    goto :goto_0

    .line 383
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;->onSetScreenRotation(I)V

    goto :goto_0

    .line 390
    :cond_0
    const-string v0, "SUtils"

    const-string v1, "mOnMessageListener is null, can\'t send noti message"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 345
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public onClientPause(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 479
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 484
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onClientPlay(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 487
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 489
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 492
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 310
    return-void
.end method

.method public onVolumeChanged(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 470
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 472
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 473
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 476
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseS()Z

    goto :goto_0
.end method

.method public registerDeviceObserver(Lcom/sec/android/app/mv/player/view/ParentDeviceListView;)V
    .locals 1
    .param p1, "listView"    # Lcom/sec/android/app/mv/player/view/ParentDeviceListView;

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;->registerObserver(Ljava/lang/Object;)V

    .line 443
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 101
    :cond_0
    return-void
.end method

.method public sendMasterClock(JJJJJ)V
    .locals 13
    .param p1, "currentTime"    # J
    .param p3, "clockDelta"    # J
    .param p5, "videoTime"    # J
    .param p7, "currentSecMediaClock"    # J
    .param p9, "currentAudioTimestamp"    # J

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-wide v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sendMasterClock(JJJJJ)V

    .line 143
    :cond_0
    return-void
.end method

.method public sendPauseToServer()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendPauseToServer()V

    .line 237
    return-void
.end method

.method public sendPlayToServer()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendPlayToServer()V

    .line 241
    return-void
.end method

.method public sendPlayerState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->sendPlayerState(I)V

    .line 249
    return-void
.end method

.method public sendUpdatedVolumeToMaster(I)V
    .locals 1
    .param p1, "currentVolume"    # I

    .prologue
    .line 495
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateVolume(I)V

    .line 496
    return-void
.end method

.method public setAudioChannel(I)V
    .locals 1
    .param p1, "audioChannel"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setAudioChannel(I)V

    .line 138
    :cond_0
    return-void
.end method

.method public setClientVolumeValue(II)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setClientVolumeValue(II)Z

    move-result v0

    return v0
.end method

.method public setDataSource()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setDataSource()V

    .line 245
    return-void
.end method

.method public setGUIHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mGUIHandler:Landroid/os/Handler;

    .line 75
    return-void
.end method

.method public setMasterVolumeValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMasterVolumeValue(I)V

    .line 319
    return-void
.end method

.method public setOnMessageListener(Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    .line 421
    return-void
.end method

.method public setServerIP(Ljava/lang/String;)V
    .locals 1
    .param p1, "serverip"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setServerIP(Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public setShareVideoData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "devicename"    # Ljava/lang/String;
    .param p3, "serverIP"    # Ljava/lang/String;

    .prologue
    .line 45
    const-string v0, "SUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShareVideoData E. mInitialized : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mInitialized:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/sec/android/app/mv/player/common/SUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/SUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "SUtils"

    const-string v1, "ShareVideoUtil is running. Stop it."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    sget-object v0, Lcom/sec/android/app/mv/player/common/SUtils;->mUniqueInstance:Lcom/sec/android/app/mv/player/common/SUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->stopSharePlay()Z

    .line 52
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mInitialized:Z

    if-nez v0, :cond_1

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    .line 55
    if-nez p3, :cond_2

    .line 56
    sget-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->SERVER:Lcom/sec/android/app/mv/player/common/Const$Type;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mType:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 57
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 63
    :goto_0
    new-instance v0, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;-><init>(Lcom/sec/android/app/mv/player/common/SUtils;Lcom/sec/android/app/mv/player/common/SUtils$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mInitialized:Z

    .line 66
    :cond_1
    return-void

    .line 59
    :cond_2
    sget-object v0, Lcom/sec/android/app/mv/player/common/Const$Type;->CLIENT:Lcom/sec/android/app/mv/player/common/Const$Type;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mType:Lcom/sec/android/app/mv/player/common/Const$Type;

    .line 60
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    goto :goto_0
.end method

.method public setUtils(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Lcom/sec/android/app/mv/player/util/VideoAudioUtil;)V
    .locals 0
    .param p1, "videoServiceUtil"    # Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .param p2, "videoAudioUtil"    # Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 70
    iput-object p2, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 71
    return-void
.end method

.method public setVideoCrop(IIII)Z
    .locals 1
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setVideoCrop(IIII)V

    .line 114
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVolumeValue(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 313
    const-string v0, "SUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVolumeValue type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setVolumeValue(II)V

    .line 315
    return-void
.end method

.method public shareVideoPause()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->pause()V

    .line 174
    return-void
.end method

.method public shareVideoSeekTo(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->pause()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->seekTo(I)V

    .line 181
    return-void
.end method

.method public shareVideoStart()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->start()V

    .line 166
    return-void
.end method

.method public shareVideoStop()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stop()V

    .line 170
    return-void
.end method

.method public show()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->show()Z

    move-result v0

    return v0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->playS()Z

    goto :goto_0
.end method

.method public startSharePlay()Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->startSharePlay()Z

    move-result v0

    return v0
.end method

.method public stopSharePlay()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mInitialized:Z

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopSharePlay()Z

    move-result v0

    .line 232
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unregisterDeviceObserver(Lcom/sec/android/app/mv/player/view/ParentDeviceListView;)V
    .locals 3
    .param p1, "listView"    # Lcom/sec/android/app/mv/player/view/ParentDeviceListView;

    .prologue
    .line 447
    :try_start_0
    const-string v1, "SUtils"

    const-string v2, "unregisterDeviceObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/mv/player/common/SUtils;->mDeviceObserver:Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/common/SUtils$MVDeviceObserver;->unregisterObserver(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 454
    :goto_0
    return-void

    .line 449
    :catch_0
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 451
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
