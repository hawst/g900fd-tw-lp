.class public Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;
.super Landroid/view/View;
.source "AnimatedBrightnessIconView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AnimatedBrightnessIconView"


# instance fields
.field private mBmIcon:Landroid/graphics/Bitmap;

.field private mBrightnessValueMax:I

.field private mBrightnessValueMin:I

.field private mCx:F

.field private mCy:F

.field private mDefaultCircleRadius:F

.field private mDefaultCircleX:F

.field private mDefaultCircleY:F

.field private mDefaultImageHeight:I

.field private mDefaultImageWidth:I

.field private mDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mPaint:Landroid/graphics/Paint;

.field private mRadius:F

.field private mValue:I

.field private objAnimationValue:F

.field private objAnimator:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->init()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->init()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v0, 0x4a

    const/high16 v1, 0x41900000    # 18.0f

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultImageWidth:I

    .line 19
    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultImageHeight:I

    .line 22
    iput v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleRadius:F

    .line 23
    const/high16 v0, 0x41940000    # 18.5f

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleX:F

    .line 24
    iput v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleY:F

    .line 26
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleRadius:F

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    .line 27
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleX:F

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCx:F

    .line 28
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleY:F

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCy:F

    .line 31
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mValue:I

    .line 33
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMin:I

    .line 34
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMax:I

    .line 41
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->objAnimationValue:F

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->init()V

    .line 56
    return-void
.end method

.method private changeValue()V
    .locals 3

    .prologue
    .line 86
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBmIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultImageWidth:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 88
    .local v0, "value":F
    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleRadius:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    .line 89
    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleX:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCx:F

    .line 90
    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleY:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCy:F

    .line 91
    return-void
.end method

.method private getValue()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mValue:I

    int-to-float v0, v0

    return v0
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 59
    const-string v0, "AnimatedBrightnessIconView"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const v0, 0x7f020097

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->setIcon(IZ)V

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mPaint:Landroid/graphics/Paint;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mPaint:Landroid/graphics/Paint;

    const v1, -0x3a1f17

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mValue:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->setValue(I)V

    .line 68
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 76
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBmIcon:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCx:F

    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCy:F

    iget v2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    mul-float/2addr v2, v4

    iget v3, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mValue:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCx:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCy:F

    add-float/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 81
    iget v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCx:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    iget v2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mCy:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mRadius:F

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 82
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 83
    return-void
.end method

.method public setBrightnessValueMax(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 120
    const-string v0, "AnimatedBrightnessIconView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBrightnessValueMax : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iput p1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMax:I

    .line 122
    return-void
.end method

.method public setBrightnessValueMin(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 115
    const-string v0, "AnimatedBrightnessIconView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBrightnessValueMin : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iput p1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMin:I

    .line 117
    return-void
.end method

.method public setIcon(IIIIII)V
    .locals 2
    .param p1, "resId"    # I
    .param p2, "imgWidth"    # I
    .param p3, "imgHeight"    # I
    .param p4, "circleRadius"    # I
    .param p5, "circleX"    # I
    .param p6, "circleY"    # I

    .prologue
    .line 104
    const-string v0, "AnimatedBrightnessIconView"

    const-string v1, "setIcon(6)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iput p2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultImageWidth:I

    .line 106
    iput p3, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultImageHeight:I

    .line 107
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleRadius:F

    .line 108
    int-to-float v0, p5

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleX:F

    .line 109
    int-to-float v0, p6

    iput v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDefaultCircleY:F

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->setIcon(IZ)V

    .line 112
    return-void
.end method

.method public setIcon(IZ)V
    .locals 2
    .param p1, "resId"    # I
    .param p2, "needChange"    # Z

    .prologue
    .line 94
    const-string v0, "AnimatedBrightnessIconView"

    const-string v1, "setIcon(2)"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBmIcon:Landroid/graphics/Bitmap;

    .line 98
    if-eqz p2, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->changeValue()V

    .line 101
    :cond_0
    return-void
.end method

.method public setValue(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    const/16 v3, 0x64

    .line 125
    const-string v0, "AnimatedBrightnessIconView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->objAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->objAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 130
    :cond_0
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMax:I

    iget v2, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mBrightnessValueMin:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 132
    if-le p1, v3, :cond_2

    .line 133
    const/16 p1, 0x64

    .line 139
    :cond_1
    :goto_0
    iput p1, p0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->mValue:I

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->invalidate()V

    .line 141
    return-void

    .line 134
    :cond_2
    const/16 v0, 0x60

    if-lt p1, v0, :cond_3

    if-ge p1, v3, :cond_3

    .line 135
    const/16 p1, 0x60

    goto :goto_0

    .line 136
    :cond_3
    if-gez p1, :cond_1

    .line 137
    const/4 p1, 0x0

    goto :goto_0
.end method
