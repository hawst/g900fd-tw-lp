.class Lcom/sec/android/app/mv/player/activity/ShareVideo$5;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1535
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1568
    :cond_0
    :goto_0
    return-void

    .line 1539
    :cond_1
    const-string v1, "command"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1540
    .local v0, "cmd":Ljava/lang/String;
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBtMediaBtnReceiver - cmd : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1543
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto :goto_0

    .line 1544
    :cond_2
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1545
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stop()V

    goto :goto_0

    .line 1546
    :cond_3
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1548
    :cond_4
    const-string v1, "ShareVideo"

    const-string v2, "KEYCODE_MEDIA_REWIND/KEYCODE_MEDIA_FAST_FORWARD up"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 1550
    :cond_5
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1551
    const-string v1, "ShareVideo"

    const-string v2, "[c] VIDEO_MEDIA_BTN_PLAYPAUSE"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->isDialogPopupShowing()Z
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1553
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 1555
    :cond_6
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1556
    const-string v1, "ShareVideo"

    const-string v2, "MEDIA_BTN_NEXT"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 1558
    :cond_7
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1559
    const-string v1, "ShareVideo"

    const-string v2, "MEDIA_BTN_PREV"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 1561
    :cond_8
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1562
    const-string v1, "ShareVideo"

    const-string v2, "KEYCODE_MEDIA_FAST_FORWARD"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1563
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 1564
    :cond_9
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1565
    const-string v1, "ShareVideo"

    const-string v2, "KEYCODE_MEDIA_REWIND"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0
.end method
