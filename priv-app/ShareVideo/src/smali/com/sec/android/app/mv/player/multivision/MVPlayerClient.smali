.class public Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;
.super Lcom/sec/android/app/mv/player/multivision/MVPlayer;
.source "MVPlayerClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MVPlayerClient"


# instance fields
.field private DEBUG:Z

.field private mClientSupportMultiVision:Z

.field private mCurrentURL:Ljava/lang/String;

.field private mMVClientPosition:I

.field private mMVPlayNumber:I

.field private mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

.field private mMeasuredHeightRatio:I

.field private mPlayerStatus:I

.field private mSendSmallestTimeDelta:Z

.field private mServerSupportMultiVision:Z

.field private mSingleVisionMode:Z

.field private mSystemClockDelta:J

.field private mSystemTimeBase:J

.field private useGroupPlayUrl:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mvUtil"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVPlayer;-><init>()V

    .line 15
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVPlayNumber:I

    .line 17
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVClientPosition:I

    .line 19
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    .line 21
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mClientSupportMultiVision:Z

    .line 23
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mServerSupportMultiVision:Z

    .line 25
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemTimeBase:J

    .line 27
    iput-wide v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSendSmallestTimeDelta:Z

    .line 31
    iput v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mPlayerStatus:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 37
    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMeasuredHeightRatio:I

    .line 39
    const-string v0, "true"

    const-string v1, "secmm.player.gp.url"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->useGroupPlayUrl:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 45
    iput v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    .line 48
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVPlayerClient create useGroupPlayUrl : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->useGroupPlayUrl:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method private resetPlaybackParameter()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVPlayNumber:I

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSendSmallestTimeDelta:Z

    .line 125
    return-void
.end method


# virtual methods
.method public contentURL()Ljava/lang/String;
    .locals 3

    .prologue
    .line 471
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 472
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contentURL() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    return-object v0
.end method

.method public endConnection()V
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_1

    .line 60
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mPlayerStatus:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 65
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .restart local v0    # "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->finished()V

    .line 69
    .end local v0    # "message":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public exitMVGroup()V
    .locals 3

    .prologue
    .line 382
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    if-nez v1, :cond_0

    .line 383
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 386
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public finished(I)V
    .locals 4
    .param p1, "endMsg"    # I

    .prologue
    .line 86
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] finished.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/16 v1, 0x65

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 88
    return-void
.end method

.method public getCommunicator()Lcom/sec/android/app/mv/player/multivision/MVCommunicator;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    return v0
.end method

.method public getMVMembersNumber()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVPlayNumber:I

    return v0
.end method

.method public getMeasuredHeightRatio()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMeasuredHeightRatio:I

    return v0
.end method

.method public getPlayPosition()V
    .locals 4

    .prologue
    .line 390
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 391
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] getPlayPosition"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPlayPosition() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPlayerStatus()I
    .locals 3

    .prologue
    .line 398
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getPlayerStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mPlayerStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mPlayerStatus:I

    return v0
.end method

.method public getSystemClockDelta()J
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    return-wide v0
.end method

.method public getSystemTimeBase()J
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemTimeBase:J

    return-wide v0
.end method

.method public hide()Z
    .locals 3

    .prologue
    .line 209
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 212
    const/4 v1, 0x1

    .line 214
    .end local v0    # "message":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isChangeGroupPlayURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->useGroupPlayUrl:Z

    if-eqz v0, :cond_0

    .line 431
    const-string v0, "sshttp://"

    const-string v1, "groupplay://"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 434
    :cond_0
    return-object p1
.end method

.method public isSingleVisionMode()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    return v0
.end method

.method public isSupportMultiVision()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mServerSupportMultiVision:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mClientSupportMultiVision:Z

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public joinMVGroup()V
    .locals 3

    .prologue
    .line 375
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    if-eqz v1, :cond_0

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 379
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 239
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] pause()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_0
    return-void

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pause() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public play(I)V
    .locals 4
    .param p1, "startTime"    # I

    .prologue
    .line 230
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] play() startTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resume()V
    .locals 4

    .prologue
    .line 257
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 258
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] resume()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 267
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 268
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] seekTo() pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    :goto_0
    return-void

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seekTo() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendClientInitialInfo()V
    .locals 6

    .prologue
    .line 154
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "deviceName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 157
    .local v2, "support_multivision":I
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mClientSupportMultiVision:Z

    if-eqz v3, :cond_0

    .line 158
    const/4 v2, 0x1

    .line 160
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 161
    const-string v3, "MVPlayerClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] sendClientInitialInfo support_multivision = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", DeviceName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v3, :cond_2

    .line 165
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x16

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getXdpi()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getYdpi()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "message":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 171
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public sendClientMVRequest(IJ)V
    .locals 4
    .param p1, "direction"    # I
    .param p2, "touchTime"    # J

    .prologue
    .line 174
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 175
    if-nez p1, :cond_2

    .line 176
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendClientMVRequest LEFT, touchTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_1

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 186
    .end local v0    # "message":Ljava/lang/String;
    :cond_1
    return-void

    .line 178
    :cond_2
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendClientMVRequest RIGHT, touchTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendContentURL(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->isChangeGroupPlayURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 440
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 441
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendContentURL() - url = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    .line 444
    return-void
.end method

.method public sendDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V
    .locals 1
    .param p1, "infoList"    # Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->updateDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V

    .line 448
    return-void
.end method

.method public sendKeepAlive()V
    .locals 4

    .prologue
    .line 463
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 464
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendKeepAlive()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendKeepAlive() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMasterClock(JJJJ)V
    .locals 15
    .param p1, "clock"    # J
    .param p3, "videoTime"    # J
    .param p5, "currentSecMediaClock"    # J
    .param p7, "currentAudioTimestamp"    # J

    .prologue
    .line 404
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemTimeBase:J

    sub-long v4, v8, v10

    .line 405
    .local v4, "currentTime":J
    sub-long v6, v4, p1

    .line 406
    .local v6, "clockDelta":J
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 407
    const-string v3, "MVPlayerClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendMasterClock() - current Delta = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", minClockDelta = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", videoTime = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p3

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", currentSecMediaClock = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p5

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", currentAudioTimestamp = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, p7

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mSendSmallestTimeDelta = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSendSmallestTimeDelta:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_0
    iget-wide v8, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-eqz v3, :cond_1

    iget-wide v8, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-lez v3, :cond_2

    .line 415
    :cond_1
    iput-wide v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    .line 418
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSendSmallestTimeDelta:Z

    if-eqz v3, :cond_3

    .line 419
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    iget-wide v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemClockDelta:J

    .end local v6    # "clockDelta":J
    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    invoke-virtual/range {v3 .. v13}, Lcom/sec/android/app/mv/player/common/SUtils;->sendMasterClock(JJJJJ)V

    .line 420
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSendSmallestTimeDelta:Z

    .line 427
    .end local v4    # "currentTime":J
    :goto_0
    return-void

    .line 422
    .restart local v4    # "currentTime":J
    .restart local v6    # "clockDelta":J
    :cond_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    invoke-virtual/range {v3 .. v13}, Lcom/sec/android/app/mv/player/common/SUtils;->sendMasterClock(JJJJJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 424
    .end local v4    # "currentTime":J
    .end local v6    # "clockDelta":J
    :catch_0
    move-exception v2

    .line 425
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "MVPlayerClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendMasterClock() - MediaPlayer is not ready"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMultiVisionDeviceDisconnected(IILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "position"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/16 v1, 0x69

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 190
    return-void
.end method

.method public sendPauseToServer()V
    .locals 3

    .prologue
    .line 72
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 76
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public sendPlayToServer()V
    .locals 3

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    if-eqz v1, :cond_0

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x23

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 83
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public sendPlayerState(I)V
    .locals 4
    .param p1, "playerState"    # I

    .prologue
    .line 478
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 479
    packed-switch p1, :pswitch_data_0

    .line 510
    :cond_0
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mPlayerStatus:I

    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 513
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 514
    return-void

    .line 481
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_1
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus UNKNOWN"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 484
    :pswitch_2
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus PREPARED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 487
    :pswitch_3
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus PLAYED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 490
    :pswitch_4
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus PAUSED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 493
    :pswitch_5
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus STOPPED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 496
    :pswitch_6
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus RESUMED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 499
    :pswitch_7
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus READY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 502
    :pswitch_8
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus BUFFERING"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 505
    :pswitch_9
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendPlayerStatus ERROR"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public sendVolumeValue(I)V
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 452
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 453
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sendVolumeValue()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/16 v2, 0x6c

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p1, v3, v4}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :goto_0
    return-void

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendVolumeValue() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendVolumetoMasterById(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    .line 518
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 519
    .local v1, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    .end local v1    # "message":Ljava/lang/String;
    :goto_0
    return-void

    .line 520
    :catch_0
    move-exception v0

    .line 521
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MVPlayerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendVolumeValue() - RemoteException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAudioChannel(I)V
    .locals 4
    .param p1, "audioChannel"    # I

    .prologue
    .line 303
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 304
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setAudioChannel(), AudioChannel is = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->setAudioChannel(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioChannel() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setClientID(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 132
    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setClientID id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    .line 134
    return-void
.end method

.method public setClientMultiVisionSupport(Z)V
    .locals 0
    .param p1, "clientSupportMultiVision"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mClientSupportMultiVision:Z

    .line 143
    return-void
.end method

.method public setCrop(IIII)V
    .locals 9
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 277
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoHeight()I

    move-result v2

    .line 278
    .local v2, "videoHegith":I
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoWidth()I

    move-result v3

    .line 279
    .local v3, "videoWidth":I
    if-le v2, v3, :cond_2

    .line 280
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    const/16 v5, 0x6d

    const/16 v6, 0x130

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 285
    :goto_0
    const/4 v1, 0x0

    .line 286
    .local v1, "save":I
    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 287
    const/4 v1, 0x1

    .line 289
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->startMVShakeMotion()V

    .line 290
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    .line 292
    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v4, :cond_1

    const-string v4, "MVPlayerClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] setCrop left : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", top : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", right : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", bottom : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/common/SUtils;->setVideoCrop(IIII)Z

    .line 295
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    const/16 v5, 0x66

    iget v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVClientPosition:I

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v1, v7}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 299
    .end local v1    # "save":I
    .end local v2    # "videoHegith":I
    .end local v3    # "videoWidth":I
    :goto_1
    return-void

    .line 282
    .restart local v2    # "videoHegith":I
    .restart local v3    # "videoWidth":I
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v4

    const/16 v5, 0x6d

    const/16 v6, 0x12f

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 296
    .end local v2    # "videoHegith":I
    .end local v3    # "videoWidth":I
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "MVPlayerClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setVideoCrop() - RemoteException occured :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 6
    .param p1, "curURL"    # Ljava/lang/String;

    .prologue
    .line 194
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->isChangeGroupPlayURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 196
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 197
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setDataSource() URL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->resetPlaybackParameter()V

    .line 201
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    .line 202
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/16 v2, 0x67

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCurrentURL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMVClientPosition(I)V
    .locals 0
    .param p1, "clientPosition"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVClientPosition:I

    .line 108
    return-void
.end method

.method public setMVPlayNumber(I)V
    .locals 0
    .param p1, "mvPlayNumber"    # I

    .prologue
    .line 103
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVPlayNumber:I

    .line 104
    return-void
.end method

.method public setMeasuredHeightRatio(I)V
    .locals 0
    .param p1, "measuredHeightRatio"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMeasuredHeightRatio:I

    .line 96
    return-void
.end method

.method public setPlayPosition(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 333
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 334
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setPlayPosition pos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPlayPosition() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSVCropSize()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 342
    const/4 v2, 0x0

    .line 343
    .local v2, "left":I
    const/4 v5, 0x0

    .line 344
    .local v5, "top":I
    const/4 v3, 0x0

    .line 345
    .local v3, "right":I
    const/4 v0, 0x0

    .line 347
    .local v0, "bottom":I
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoWidth()I

    move-result v3

    .line 348
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->getVideoHeight()I

    move-result v0

    .line 350
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_TABLET:Z

    if-nez v6, :cond_0

    if-le v0, v3, :cond_0

    .line 351
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    const/16 v7, 0x6d

    const/16 v8, 0x12f

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V

    .line 354
    :cond_0
    const-string v6, "MVPlayerClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] setSVCropSize - right : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "bottom : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v4, 0x0

    .line 357
    .local v4, "save":I
    iget-boolean v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    if-ne v6, v11, :cond_1

    .line 358
    const/4 v4, 0x1

    .line 360
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->stopMVShakeMotion()V

    .line 361
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSingleVisionMode:Z

    .line 363
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6, v2, v5, v3, v0}, Lcom/sec/android/app/mv/player/common/SUtils;->setVideoCrop(IIII)Z

    .line 364
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    const/16 v7, 0x66

    const/4 v8, -0x1

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v4, v9}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    .end local v4    # "save":I
    :goto_0
    return-void

    .line 365
    :catch_0
    move-exception v1

    .line 366
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "MVPlayerClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "setSVCropSize() - RemoteException occured :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setServerSupportMultiVision(Z)V
    .locals 3
    .param p1, "serverSupportMultiVision"    # Z

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "MVPlayerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] setServerSupportMultiVision : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mServerSupportMultiVision:Z

    .line 139
    return-void
.end method

.method public setVolumeMute()V
    .locals 6

    .prologue
    .line 313
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 314
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setVolumeMute()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/16 v2, 0x6a

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :goto_0
    return-void

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVolumeMute() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVolumeOn()V
    .locals 6

    .prologue
    .line 323
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 324
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setVolumeOn()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/16 v2, 0x6b

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVolumeOn() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public show()Z
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    .line 222
    const/4 v1, 0x1

    .line 224
    .end local v0    # "message":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startConnection(Ljava/lang/String;)V
    .locals 2
    .param p1, "serverIP"    # Ljava/lang/String;

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;-><init>(Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mCommunicator:Lcom/sec/android/app/mv/player/multivision/MVCommunicator;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->start()V

    .line 55
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mSystemTimeBase:J

    .line 56
    return-void
.end method

.method public stop()V
    .locals 6

    .prologue
    .line 248
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] stop()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    const/16 v2, 0x68

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/common/SUtils;->notifyMessage(IIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MVPlayerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop() - RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
