.class public final Lcom/sec/android/app/mv/player/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final ChapterLine:I = 0x7f070000

.field public static final Line:I = 0x7f070001

.field public static final No_item:I = 0x7f070002

.field public static final Separate_Color:I = 0x7f070003

.field public static final Separate_Text:I = 0x7f070004

.field public static final actionbar_btn_shadow:I = 0x7f070005

.field public static final actionbar_btn_text:I = 0x7f070006

.field public static final actionbar_btn_text_dim:I = 0x7f070007

.field public static final actionbar_text:I = 0x7f070008

.field public static final back_text_color:I = 0x7f070009

.field public static final black:I = 0x7f07000a

.field public static final bookmark_black_background:I = 0x7f07000b

.field public static final brightness_text_color:I = 0x7f07000c

.field public static final brightness_text_level:I = 0x7f07000d

.field public static final button_pressed:I = 0x7f07000e

.field public static final check_videoListTitle:I = 0x7f07000f

.field public static final color2:I = 0x7f070010

.field public static final count_BG:I = 0x7f070011

.field public static final current_time_text:I = 0x7f070012

.field public static final defaultBackground:I = 0x7f070013

.field public static final details_id:I = 0x7f070014

.field public static final details_title:I = 0x7f070015

.field public static final details_value:I = 0x7f070016

.field public static final device_dialog_list_channelpos_text:I = 0x7f070017

.field public static final device_dialog_list_devicename_text:I = 0x7f070018

.field public static final device_dialog_list_devicename_text_master:I = 0x7f070019

.field public static final device_volume_text:I = 0x7f07001a

.field public static final duration_text:I = 0x7f07001b

.field public static final folder_count:I = 0x7f07001c

.field public static final group_count_color:I = 0x7f07001d

.field public static final group_text:I = 0x7f07001e

.field public static final keyboard_transparent_background:I = 0x7f07001f

.field public static final list_divider:I = 0x7f070020

.field public static final list_duration_text:I = 0x7f070021

.field public static final list_elapsed_text:I = 0x7f070022

.field public static final list_end_text:I = 0x7f070023

.field public static final load_color:I = 0x7f070024

.field public static final main_list_text_color:I = 0x7f070025

.field public static final micro_text:I = 0x7f070026

.field public static final mosaic_background:I = 0x7f070027

.field public static final mosaic_black_background:I = 0x7f070028

.field public static final movie_background:I = 0x7f070029

.field public static final no_item:I = 0x7f07002a

.field public static final normal_text:I = 0x7f07002b

.field public static final opaque_black:I = 0x7f07002c

.field public static final personal_border_color:I = 0x7f07002d

.field public static final playerlist_background_color:I = 0x7f07002e

.field public static final playerlist_header_text:I = 0x7f07002f

.field public static final playerlist_spinner_text:I = 0x7f070030

.field public static final playlist_1st_row_color:I = 0x7f070031

.field public static final playlist_1st_row_pressed_color:I = 0x7f070032

.field public static final playlist_2nd_row_color:I = 0x7f070033

.field public static final playlist_2nd_row_pressed_color:I = 0x7f070034

.field public static final playlist_background_color:I = 0x7f070035

.field public static final popup_color:I = 0x7f070036

.field public static final popup_color_dark:I = 0x7f070037

.field public static final popup_color_light:I = 0x7f070038

.field public static final popup_sec_color:I = 0x7f070039

.field public static final popup_sharevia_color:I = 0x7f07003a

.field public static final popup_text_color:I = 0x7f07003b

.field public static final popup_text_color_value:I = 0x7f07003c

.field public static final popup_title:I = 0x7f07003d

.field public static final popup_value_color:I = 0x7f07003e

.field public static final popup_value_color_dark:I = 0x7f07003f

.field public static final popup_value_color_light:I = 0x7f070040

.field public static final poster_title:I = 0x7f070041

.field public static final recent_filename_color:I = 0x7f070042

.field public static final search_result_color:I = 0x7f070043

.field public static final select_all_color:I = 0x7f070044

.field public static final select_all_text_color:I = 0x7f070045

.field public static final selected_item:I = 0x7f070046

.field public static final send_dialog_text_color:I = 0x7f070047

.field public static final sharevideo_explain_text:I = 0x7f070048

.field public static final sharevideo_popup_check_text:I = 0x7f070049

.field public static final split_foler_selected_text:I = 0x7f07004a

.field public static final split_foler_text:I = 0x7f07004b

.field public static final split_left_background:I = 0x7f07004c

.field public static final sub_info_text:I = 0x7f07004d

.field public static final sub_list_text_color:I = 0x7f07004e

.field public static final subtext_color_light:I = 0x7f07004f

.field public static final switchtext:I = 0x7f070050

.field public static final tab_disabled:I = 0x7f070051

.field public static final tab_enable:I = 0x7f070052

.field public static final tab_focus_font_color:I = 0x7f070053

.field public static final tab_font_color:I = 0x7f070054

.field public static final tab_font_shadow_color:I = 0x7f070055

.field public static final text_color_5:I = 0x7f070056

.field public static final text_color_6:I = 0x7f070057

.field public static final text_color_7:I = 0x7f070058

.field public static final text_color_light:I = 0x7f070059

.field public static final text_color_light_disabled:I = 0x7f07005a

.field public static final thumbnail_bkgnd:I = 0x7f07005b

.field public static final thumbnail_duration_text:I = 0x7f07005c

.field public static final thumbnail_elapsed_text:I = 0x7f07005d

.field public static final time_indicator:I = 0x7f07005e

.field public static final title_text_color:I = 0x7f07005f

.field public static final transparency:I = 0x7f070060

.field public static final transparent:I = 0x7f070061

.field public static final videoListNewDuration:I = 0x7f070062

.field public static final video_count:I = 0x7f070063

.field public static final volume_text:I = 0x7f070064

.field public static final white:I = 0x7f070065

.field public static final winset3_whith_selected:I = 0x7f070066


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
