.class Lcom/sec/android/app/mv/player/activity/ShareVideo$24;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->createKDrmPopup(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

.field final synthetic val$PopUptype:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V
    .locals 0

    .prologue
    .line 2613
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iput p2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->val$PopUptype:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2616
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->val$PopUptype:I

    packed-switch v1, :pswitch_data_0

    .line 2636
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2637
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 2638
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4002(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 2640
    :cond_0
    return-void

    .line 2620
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 2627
    :pswitch_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->play()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2628
    :catch_0
    move-exception v0

    .line 2629
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2616
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
