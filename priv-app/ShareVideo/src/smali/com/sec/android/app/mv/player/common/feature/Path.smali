.class public Lcom/sec/android/app/mv/player/common/feature/Path;
.super Ljava/lang/Object;
.source "Path.java"


# static fields
.field public static final CAPTURE_INTERNAL:Ljava/lang/String;

.field public static final EXTERNAL_ICS_ROOT_PATH:Ljava/lang/String; = "/mnt/extSdCard"

.field public static final EXTERNAL_ROOT_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field public static final FILE:Ljava/lang/String; = "file://"

.field public static final INTERNAL_ICS_ROOT_PATH:Ljava/lang/String; = "/mnt/sdcard"

.field public static final INTERNAL_JB_ROOT_PATH:Ljava/lang/String; = "/storage/sdcard0"

.field public static final INTERNAL_ROOT_PATH:Ljava/lang/String;

.field public static final MNT:Ljava/lang/String; = "/mnt"

.field public static final STORAGE:Ljava/lang/String; = "/storage"

.field public static final STORAGE_ICS_USB_DRIVE:Ljava/lang/String; = "/mnt/UsbDrive"

.field public static final STORAGE_USB_DRIVE:Ljava/lang/String; = "/storage/UsbDrive"

.field public static final SYSTEM:Ljava/lang/String; = "/system/"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/mv/player/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Pictures/Screenshots"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Path;->CAPTURE_INTERNAL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ReplaceOldPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 29
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-object v2

    .line 31
    :cond_1
    const-string v0, ""

    .line 32
    .local v0, "newPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 33
    .local v1, "pos":I
    const-string v3, "/mnt/sdcard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 34
    const-string v2, "/mnt/sdcard"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 35
    sget-object v0, Lcom/sec/android/app/mv/player/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    .line 45
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 36
    :cond_2
    const-string v3, "/mnt/extSdCard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 37
    const-string v2, "/mnt/extSdCard"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 38
    const-string v0, "/storage/extSdCard"

    goto :goto_1

    .line 39
    :cond_3
    const-string v3, "/mnt/UsbDrive"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 40
    const-string v2, "/mnt/UsbDrive"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 41
    const-string v0, "/storage/UsbDrive"

    goto :goto_1
.end method
