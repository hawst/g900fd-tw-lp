.class Lcom/sec/android/app/mv/player/activity/ShareVideo$11;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2254
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2256
    if-nez p2, :cond_1

    .line 2272
    :cond_0
    :goto_0
    return-void

    .line 2259
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2260
    .local v0, "action":Ljava/lang/String;
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mVolumeChangeReceiver. intent.getAction() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    const-string v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2263
    const-string v2, "android.media.EXTRA_RINGER_MODE"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2264
    .local v1, "newRingerMode":I
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mVolumeChangeReceiver. newRingerMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2266
    .end local v1    # "newRingerMode":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->hasWindowFocus()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->isDialogFragmentShowing()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2267
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/SUtils;->sendUpdatedVolumeToMaster(I)V

    .line 2269
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v2, :cond_0

    .line 2270
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolume()V

    goto :goto_0
.end method
