.class public Lcom/sec/android/app/mv/player/view/SubtitleView;
.super Landroid/widget/RelativeLayout;
.source "SubtitleView.java"


# static fields
.field public static final SUBTITLE_SIZE_UP_L:I = 0x1c

.field public static final SUBTITLE_SIZE_UP_M:I = 0x15

.field public static final SUBTITLE_SIZE_UP_S:I = 0x12


# instance fields
.field private height_portrait:I

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mContext:Landroid/content/Context;

.field private mSubtitleLayout:Landroid/widget/RelativeLayout;

.field private mSubtitleText:Landroid/widget/TextView;

.field private mSubtitleView:Landroid/view/View;

.field private margin_from_bottom:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 47
    return-void
.end method

.method private initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    .line 50
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mContext:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->removeAllViews()V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->makeView()Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->addView(Landroid/view/View;)V

    .line 57
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setLayout(Z)V

    .line 58
    return-void
.end method

.method private makeView()Landroid/view/View;
    .locals 4

    .prologue
    .line 61
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 62
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03001f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    const v2, 0x7f0d009b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    const v2, 0x7f0d009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v3}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    return-object v1
.end method

.method private resizeTextSize()I
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getSubTitleSize()I

    move-result v0

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    if-ne v0, v1, :cond_0

    .line 137
    const/16 v0, 0x1c

    .line 141
    :goto_0
    return v0

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getSubTitleSize()I

    move-result v0

    sget v1, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    if-ne v0, v1, :cond_1

    .line 139
    const/16 v0, 0x12

    goto :goto_0

    .line 141
    :cond_1
    const/16 v0, 0x15

    goto :goto_0
.end method

.method private setLayout(Z)V
    .locals 7
    .param p1, "isControlShow"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 81
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 82
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "height":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 86
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    if-eqz p1, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 90
    :cond_0
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    add-int/2addr v0, v2

    .line 91
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 132
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    return-void

    .line 93
    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v2

    if-eq v2, v6, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v2

    if-eq v2, v5, :cond_5

    .line 96
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_HIGH_SPEED_PLAY:Z

    if-eqz v2, :cond_4

    .line 97
    if-eqz p1, :cond_3

    .line 98
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    add-int/lit8 v0, v2, 0x32

    .line 99
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 101
    :cond_3
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 102
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    add-int/lit16 v0, v2, 0x226

    .line 103
    invoke-virtual {v1, v4, v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 106
    :cond_4
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->height_portrait:I

    add-int/lit8 v0, v2, 0x32

    .line 107
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 110
    :cond_5
    iget v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    .line 111
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 115
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080110

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 117
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v2

    if-eq v2, v6, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v2

    if-eq v2, v5, :cond_7

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isMultiWindowMode()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 121
    :cond_7
    if-nez p1, :cond_8

    .line 122
    const/4 v0, 0x0

    .line 124
    :cond_8
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    add-int/2addr v0, v2

    .line 127
    :cond_9
    iget v2, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->margin_from_bottom:I

    add-int/2addr v0, v2

    .line 129
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_0
.end method


# virtual methods
.method public getSubTitleSize()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v0

    return v0
.end method

.method public moveDown()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setLayout(Z)V

    .line 78
    return-void
.end method

.method public moveUp()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setLayout(Z)V

    .line 74
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 182
    return-void
.end method

.method public updateSubTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 157
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateSubTitleBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 173
    return-void
.end method

.method public updateSubTitleColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 169
    return-void
.end method

.method public updateSubTitleFont(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 177
    return-void
.end method

.method public updateSubTitleSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 145
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SUBTITLE_RESIZE_MODEL:Z

    if-eqz v0, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->resizeTextSize()I

    move-result v0

    add-int/2addr p1, v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v1, 0x1

    int-to-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 150
    return-void
.end method
