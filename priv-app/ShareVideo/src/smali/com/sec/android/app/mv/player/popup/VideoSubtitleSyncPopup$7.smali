.class Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;
.super Ljava/lang/Object;
.source "VideoSubtitleSyncPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->createPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x2

    .line 166
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 185
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 168
    :pswitch_1
    const-string v0, "VideoSubtitleSyncPopup"

    const-string v1, "mMinusButton. ACTION_DOWN"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    const-wide/16 v2, 0x0

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->sendMessage(IJ)V
    invoke-static {v0, v4, v2, v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;IJ)V

    goto :goto_0

    .line 173
    :pswitch_2
    const-string v0, "VideoSubtitleSyncPopup"

    const-string v1, "mMinusButton. ACTION_UP"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;I)V

    goto :goto_0

    .line 179
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;I)V

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
