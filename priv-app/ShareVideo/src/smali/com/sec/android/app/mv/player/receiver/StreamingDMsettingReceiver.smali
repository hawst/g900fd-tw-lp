.class public Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StreamingDMsettingReceiver.java"


# static fields
.field private static final INTENT_STREAMING_GET_PROFILE:Ljava/lang/String; = "android.intent.action.STREAMING_GET_PROFILE"

.field private static final INTENT_STREAMING_PROFILE:Ljava/lang/String; = "android.intent.action.STREAMING_PROFILE"

.field private static final INTENT_STREAMING_SET_PROFILE:Ljava/lang/String; = "android.intent.action.STREAMING_SET_PROFILE"

.field public static final STREAMING_UDP_PORT_RANGE:Ljava/lang/String; = "UDPportRangeSetup"

.field private static deafaultMaxPort:Ljava/lang/String;

.field private static deafaultMinPort:Ljava/lang/String;

.field private static mMaxUdpPort:Ljava/lang/String;

.field private static mMinUdpPort:Ljava/lang/String;


# instance fields
.field public myUdpRangeContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMaxUdpPort:Ljava/lang/String;

    .line 19
    sput-object v0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMinUdpPort:Ljava/lang/String;

    .line 20
    const-string v0, "65000"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->deafaultMaxPort:Ljava/lang/String;

    .line 21
    const-string v0, "1024"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->deafaultMinPort:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getUdpPortRange(Landroid/content/SharedPreferences;)V
    .locals 5
    .param p1, "pref"    # Landroid/content/SharedPreferences;

    .prologue
    .line 57
    const-string v2, "MaxUdpPort"

    sget-object v3, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->deafaultMaxPort:Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "maxudpport":Ljava/lang/String;
    const-string v2, "Streaming"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get MaxUdpPort, MaxUdpPort = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "MinUdpPort"

    sget-object v3, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->deafaultMinPort:Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "minudpport":Ljava/lang/String;
    const-string v2, "Streaming"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Get MinUdpPort, MinUdpPort = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    sput-object v0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMaxUdpPort:Ljava/lang/String;

    .line 63
    sput-object v1, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMinUdpPort:Ljava/lang/String;

    .line 64
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "action":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->myUdpRangeContext:Landroid/content/Context;

    .line 31
    iget-object v3, p0, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->myUdpRangeContext:Landroid/content/Context;

    const-string v4, "UDPportRangeSetup"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 33
    .local v0, "UdpRangePref":Landroid/content/SharedPreferences;
    const-string v3, "android.intent.action.STREAMING_SET_PROFILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 35
    const-string v3, "Streaming"

    const-string v4, "action: set streaming udp port range"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const-string v3, "MaxUdpPort"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMaxUdpPort:Ljava/lang/String;

    .line 38
    const-string v3, "MinUdpPort"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMinUdpPort:Ljava/lang/String;

    .line 39
    sget-object v3, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMaxUdpPort:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMinUdpPort:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v4}, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->setUdpPortRange(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const-string v3, "android.intent.action.STREAMING_GET_PROFILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44
    const-string v3, "Streaming"

    const-string v4, "action: get streaming udp port range"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->getUdpPortRange(Landroid/content/SharedPreferences;)V

    .line 47
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.STREAMING_PROFILE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 48
    .local v2, "setUdpRangeIntent":Landroid/content/Intent;
    const-string v3, "MaxUdpPort"

    sget-object v4, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMaxUdpPort:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v3, "MinUdpPort"

    sget-object v4, Lcom/sec/android/app/mv/player/receiver/StreamingDMsettingReceiver;->mMinUdpPort:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public setUdpPortRange(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "pref"    # Landroid/content/SharedPreferences;
    .param p2, "maxudpport"    # Ljava/lang/String;
    .param p3, "minudpport"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 70
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MaxUdpPort"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 71
    const-string v1, "Streaming"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set MaxUdpPort, MaxUdpPort = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v1, "MinUdpPort"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 73
    const-string v1, "Streaming"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set MinUdpPort, MinUdpPort = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 76
    return-void
.end method
