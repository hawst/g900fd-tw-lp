.class Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
.super Lcom/sec/android/app/mv/player/view/ParentDeviceListView;
.source "HelpVolumeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;,
        Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    }
.end annotation


# static fields
.field private static final SPK_MODE_LEFT:I = 0x1

.field private static final SPK_MODE_MASTER:I = 0x0

.field private static final SPK_MODE_RIGHT:I = 0x2

.field public static final TAG:Ljava/lang/String;


# instance fields
.field keyListener:Landroid/view/View$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field private mDeviceAdapter:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

.field private mDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

.field private mIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    const-class v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/ParentDeviceListView;-><init>(Landroid/content/Context;)V

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I

    .line 149
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->keyListener:Landroid/view/View$OnKeyListener;

    .line 189
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->makeDummyDeviceList()V

    .line 193
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    .line 194
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceAdapter:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpDeviceAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 196
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$2;-><init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->keyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 209
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->getDeviceName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->getSpkStrPos(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSpkStrPos(I)Ljava/lang/String;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 225
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0089

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 230
    :goto_0
    return-object v0

    .line 227
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    const v1, 0x7f0a008a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 230
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private makeDummyDeviceList()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0076

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v0, v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(ILjava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0038

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v2, v4, v0, v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(ILjava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mDeviceList:Ljava/util/List;

    new-instance v2, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0037

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x2

    invoke-direct {v2, v4, v0, v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;-><init>(ILjava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    return-void
.end method
