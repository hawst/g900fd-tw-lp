.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VideoSubtitlePopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleFontAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 826
    .local p4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .line 827
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 828
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 831
    if-nez p2, :cond_0

    .line 832
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 833
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03001d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 836
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 837
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "SubtitleFontAdapter : getView() - getCount() is 0"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    :goto_0
    return-object p2

    .line 841
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 842
    .local v1, "item":Ljava/lang/String;
    const v3, 0x7f0d0098

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 844
    .local v0, "fontTv":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFont(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 845
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
