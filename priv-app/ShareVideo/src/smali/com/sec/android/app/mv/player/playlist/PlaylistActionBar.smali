.class public Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;
.super Landroid/widget/RelativeLayout;
.source "PlaylistActionBar.java"


# static fields
.field private static final HOVER_DETECT_TIME_MS:I = 0x12c


# instance fields
.field private mCancelBtnClickListener:Landroid/view/View$OnClickListener;

.field private mCloseSelectBtn:Landroid/widget/RelativeLayout;

.field private mCloseSelectClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mCounterSelectAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mCounterSpinner:Landroid/widget/Spinner;

.field private mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDoneBtn:Landroid/widget/RelativeLayout;

.field private mDoneBtnClickListener:Landroid/view/View$OnClickListener;

.field private mRoot:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # I

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    .line 212
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$7;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCloseSelectClickListener:Landroid/view/View$OnClickListener;

    .line 219
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$8;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCancelBtnClickListener:Landroid/view/View$OnClickListener;

    .line 226
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtnClickListener:Landroid/view/View$OnClickListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setView()V

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setViewForDemoPlaylist()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private changeActionLayoutForPlayList()V
    .locals 8

    .prologue
    const v7, 0x7f0d0043

    const v6, 0x7f0d005a

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0045

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0049

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d004c

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d004d

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0050

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0051

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v3, 0x7f0d0053

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 250
    .local v1, "LP_TopActionBar":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 253
    .local v0, "LP_CancelDoneBtn":Landroid/widget/RelativeLayout$LayoutParams;
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_OVERLAY_NOTIFICATION_BAR:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_CHAGALL:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_KLIMT:Z

    if-nez v2, :cond_0

    .line 254
    iget v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080149

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 256
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 257
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 261
    return-void
.end method

.method private initButtonView()V
    .locals 4

    .prologue
    .line 60
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d005b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 61
    .local v0, "cancelBtn":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCancelBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d005d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    :cond_1
    return-void
.end method

.method private initCounterAdapters()V
    .locals 6

    .prologue
    const v5, 0x7f03000e

    const v4, 0x7f03000b

    .line 148
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$4;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 163
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$5;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$5;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 178
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$6;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v5, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$6;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 192
    return-void
.end method

.method private initCounterView()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d004a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCloseSelectBtn:Landroid/widget/RelativeLayout;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCloseSelectBtn:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCloseSelectClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d004b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initCounterAdapters()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$1;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$2;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 124
    return-void
.end method

.method private initCounterViewForDemoPlaylist()V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d004b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    .line 129
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$3;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    const v2, 0x7f03000e

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$3;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f03000b

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 145
    return-void
.end method


# virtual methods
.method public changeDropdownMenu()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterUnSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 209
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 210
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->isAllUnSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAllAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSelectAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method public getDoneBtn()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getView()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public setCounterSpinnerText(I)V
    .locals 7
    .param p1, "size"    # I

    .prologue
    const v6, 0x7f0a0082

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    const v2, 0x7f0d002d

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 196
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method public setDoneBtnEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 268
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d005e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 269
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 274
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mDoneBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 275
    return-void

    .line 272
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setView()V
    .locals 3

    .prologue
    .line 44
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030015

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->changeActionLayoutForPlayList()V

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initCounterView()V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initButtonView()V

    .line 49
    return-void
.end method

.method public setViewForDemoPlaylist()V
    .locals 3

    .prologue
    .line 52
    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 53
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030015

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mRoot:Landroid/widget/RelativeLayout;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->changeActionLayoutForPlayList()V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initCounterViewForDemoPlaylist()V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->initButtonView()V

    .line 57
    return-void
.end method
