.class Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 1693
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    .line 1695
    const-string v1, "VideoServiceUtil"

    const-string v2, "mBufferingChecker Run!"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1699
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->getBufferPercentage()I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 1700
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1722
    :goto_0
    return-void

    .line 1703
    :catch_0
    move-exception v0

    .line 1704
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBufferingChecker run() RemoteException occured :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1707
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1708
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1710
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1711
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showStateView()V

    .line 1713
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1715
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1716
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1718
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1719
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showStateView()V

    .line 1720
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
