.class public Lcom/sec/android/app/mv/player/multivision/MVCommunicator;
.super Ljava/lang/Thread;
.source "MVCommunicator.java"


# static fields
.field private static final PORT:I = 0x1b9e

.field private static final TAG:Ljava/lang/String; = "MVCommunicator"


# instance fields
.field private DEBUG:Z

.field private mConnected:Z

.field private mConnectionLock:Ljava/lang/Object;

.field private mFinished:Z

.field private mID:I

.field private mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

.field private mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

.field private mObjInput:Ljava/io/ObjectInputStream;

.field private mObjOutput:Ljava/io/ObjectOutputStream;

.field private final mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private mServerIP:Ljava/lang/String;

.field private mServerSocket:Ljava/net/ServerSocket;

.field private mSocket:Ljava/net/Socket;


# direct methods
.method public constructor <init>(ILcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "mp"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    .line 42
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    .line 57
    const-string v0, "MVCommunicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVCommunicator id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 60
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;Ljava/lang/String;)V
    .locals 2
    .param p1, "mp"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;
    .param p2, "serverIP"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    .line 42
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    .line 46
    const-string v0, "MVCommunicator"

    const-string v1, "MVCommunicator E"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    .line 52
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerIP:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private cleanCommunicator()V
    .locals 4

    .prologue
    .line 95
    const-string v1, "MVCommunicator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] cleanCommunicator()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerSocket:Ljava/net/ServerSocket;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->close()V

    .line 100
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerSocket:Ljava/net/ServerSocket;

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 105
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;

    if-eqz v1, :cond_2

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 110
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    if-eqz v1, :cond_3

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_3
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 124
    const-string v5, "MVCommunicator"

    const-string v6, "init"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 127
    const-string v5, "MVCommunicator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mServerIP at communicator : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerIP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v1, v5, :cond_0

    .line 131
    :try_start_0
    new-instance v4, Ljava/net/InetSocketAddress;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mServerIP:Ljava/lang/String;

    const/16 v6, 0x1b9e

    invoke-direct {v4, v5, v6}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 132
    .local v4, "socketAddress":Ljava/net/SocketAddress;
    new-instance v5, Ljava/net/Socket;

    invoke-direct {v5}, Ljava/net/Socket;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    const/16 v6, 0x12c

    invoke-virtual {v5, v4, v6}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 135
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v4    # "socketAddress":Ljava/net/SocketAddress;
    :cond_0
    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    if-nez v5, :cond_1

    .line 145
    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 150
    .end local v1    # "i":I
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 151
    .local v2, "input":Ljava/io/InputStream;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    .line 153
    .local v3, "output":Ljava/io/OutputStream;
    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    .line 154
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    .end local v2    # "input":Ljava/io/InputStream;
    .end local v3    # "output":Ljava/io/OutputStream;
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    monitor-enter v6

    .line 168
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 169
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 171
    :cond_2
    return-void

    .line 138
    .restart local v1    # "i":I
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "MVCommunicator"

    const-string v6, "connect IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "i":I
    :catch_1
    move-exception v0

    .line 157
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v5, "MVCommunicator"

    const-string v6, "init IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 162
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    throw v5

    .line 159
    :catch_2
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v5, "MVCommunicator"

    const-string v6, "init Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 169
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v5

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5
.end method

.method private receiveMessage()V
    .locals 51

    .prologue
    .line 230
    const-string v5, "MVCommunicator"

    const-string v49, "receiveMessage E"

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/16 v26, 0x0

    .line 233
    .local v26, "message":Ljava/lang/String;
    const/16 v43, 0x0

    .line 234
    .local v43, "unit":[Ljava/lang/String;
    const/16 v42, -0x1

    .line 237
    .local v42, "type":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;

    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v30

    .line 238
    .local v30, "obj":Ljava/lang/Object;
    if-nez v30, :cond_1

    .line 554
    .end local v30    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 240
    .restart local v30    # "obj":Ljava/lang/Object;
    :cond_1
    move-object/from16 v0, v30

    check-cast v0, Ljava/lang/String;

    move-object/from16 v26, v0

    .line 241
    const-string v5, "%"

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v43

    .line 243
    move-object/from16 v0, v43

    array-length v5, v0

    const/16 v49, 0x1

    move/from16 v0, v49

    if-gt v5, v0, :cond_4

    .line 244
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. message is "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 531
    .end local v30    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v20

    .line 532
    .local v20, "e":Ljava/io/EOFException;
    const-string v5, "MVCommunicator"

    const-string v49, "EOFException happend"

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_25

    .line 535
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    const/16 v49, 0xd

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    .line 536
    :cond_2
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 537
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    .line 550
    .end local v20    # "e":Ljava/io/EOFException;
    :cond_3
    :goto_1
    const/4 v5, -0x1

    move/from16 v0, v42

    if-eq v0, v5, :cond_0

    .line 551
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v49, v0

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v49, "%"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v49, 0xe

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v49, "%"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v49, "%"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 552
    .local v32, "resp":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->sendMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    .end local v32    # "resp":Ljava/lang/String;
    .restart local v30    # "obj":Ljava/lang/Object;
    :cond_4
    const/4 v5, 0x1

    :try_start_1
    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v42

    .line 250
    packed-switch v42, :pswitch_data_0

    .line 528
    :pswitch_0
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] ERROR - unknown message"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 543
    .end local v30    # "obj":Ljava/lang/Object;
    :catch_1
    move-exception v20

    .line 544
    .local v20, "e":Ljava/io/IOException;
    invoke-virtual/range {v20 .. v20}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 252
    .end local v20    # "e":Ljava/io/IOException;
    .restart local v30    # "obj":Ljava/lang/Object;
    :pswitch_1
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_5

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_DATA_SOURCE, URL ="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :cond_5
    const/4 v5, 0x2

    aget-object v16, v43, v5

    .line 254
    .local v16, "contentUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setDataSource(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 545
    .end local v16    # "contentUrl":Ljava/lang/String;
    .end local v30    # "obj":Ljava/lang/Object;
    :catch_2
    move-exception v20

    .line 546
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 258
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v30    # "obj":Ljava/lang/Object;
    :pswitch_2
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_6

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. PLAY time="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_6
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v36

    .line 260
    .local v36, "startTime":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v36

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->play(I)V

    goto/16 :goto_1

    .line 264
    .end local v36    # "startTime":I
    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_7

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. PAUSE"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->pause()V

    goto/16 :goto_1

    .line 269
    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_8

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. STOP"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->stop()V

    goto/16 :goto_1

    .line 274
    :pswitch_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_9

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. RESUME"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->resume()V

    goto/16 :goto_1

    .line 279
    :pswitch_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_a

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEEK pos="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_a
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 281
    .local v27, "msec":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->seekTo(I)V

    goto/16 :goto_1

    .line 285
    .end local v27    # "msec":I
    :pswitch_7
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_3

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_PLAY_POSITION pos="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 289
    :pswitch_8
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_3

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. GET_PLAY_POSITION"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 293
    :pswitch_9
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_3

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. GET_PLAYER_STATE"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 297
    :pswitch_a
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_b

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_MASTER_CLOCK master_clock="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", master_videoTime="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x3

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", master_currentSecMediaClock="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x4

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", master_currentAudioTimestamp="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x5

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_b
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 300
    .local v6, "clock":J
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 301
    .local v8, "videoTime":J
    const/4 v5, 0x4

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 302
    .local v10, "currentSecMediaClock":J
    const/4 v5, 0x5

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 303
    .local v12, "currentAudioTimestamp":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual/range {v5 .. v13}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendMasterClock(JJJJ)V

    goto/16 :goto_0

    .line 307
    .end local v6    # "clock":J
    .end local v8    # "videoTime":J
    .end local v10    # "currentSecMediaClock":J
    .end local v12    # "currentAudioTimestamp":J
    :pswitch_b
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_c

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_PLAYER_STATE state="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :cond_c
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 311
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v37

    .line 312
    .local v37, "state":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v37

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyStatusChanged(I)V

    goto/16 :goto_0

    .line 317
    .end local v37    # "state":I
    :pswitch_c
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "receiveMessage. SET_SERVER_INITIAL_INFO id="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    .line 319
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v35

    .line 320
    .local v35, "server_support_multivision":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v49, v0

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setClientID(I)V

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move-object/from16 v49, v0

    if-eqz v35, :cond_d

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setServerSupportMultiVision(Z)V

    .line 322
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendClientInitialInfo()V

    goto/16 :goto_0

    .line 321
    :cond_d
    const/4 v5, 0x0

    goto :goto_2

    .line 326
    .end local v35    # "server_support_multivision":I
    :pswitch_d
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_e

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_MULTIVISION_SIZE (left,top,right,bottom)=("

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ","

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x3

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ","

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x4

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ","

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x5

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ")"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", MVPlayNumber=("

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x6

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ")"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", measuredHeightRatio=("

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x7

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ")"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ", mMVClientPosition=("

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x8

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ")"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_e
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 329
    .local v23, "left":I
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v39

    .line 330
    .local v39, "top":I
    const/4 v5, 0x4

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v34

    .line 331
    .local v34, "right":I
    const/4 v5, 0x5

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 332
    .local v14, "bottom":I
    const/4 v5, 0x6

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v28

    .line 333
    .local v28, "mvPlayNumber":I
    const/4 v5, 0x7

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .line 334
    .local v25, "measuredHeightRatio":I
    const/16 v5, 0x8

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 336
    .local v15, "clientPosition":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setMVPlayNumber(I)V

    .line 337
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setMeasuredHeightRatio(I)V

    .line 338
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5, v15}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setMVClientPosition(I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v23

    move/from16 v1, v39

    move/from16 v2, v34

    invoke-virtual {v5, v0, v1, v2, v14}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setCrop(IIII)V

    goto/16 :goto_1

    .line 343
    .end local v14    # "bottom":I
    .end local v15    # "clientPosition":I
    .end local v23    # "left":I
    .end local v25    # "measuredHeightRatio":I
    .end local v28    # "mvPlayNumber":I
    .end local v34    # "right":I
    .end local v39    # "top":I
    :pswitch_e
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_f

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_SINGLEVISION_SIZE"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    const/16 v49, 0x1

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setMVPlayNumber(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setSVCropSize()V

    goto/16 :goto_1

    .line 349
    :pswitch_f
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_10

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_CLIENT_DISCONNECT"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_10
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 351
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    .line 353
    :cond_11
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 354
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    goto/16 :goto_0

    .line 358
    :pswitch_10
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_12

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. PAUSE_SERVER"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_12
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 360
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 365
    :pswitch_11
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_13

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. PLAY_SERVER"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_13
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 367
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 372
    :pswitch_12
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_14

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_PLAYER_STATE returnType="

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_14
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 376
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v33

    .line 377
    .local v33, "returnType":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 382
    .end local v33    # "returnType":I
    :pswitch_13
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_15

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. JOIN_MV_GROUP"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_15
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 384
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 389
    :pswitch_14
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_16

    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. EXIT_MV_GROUP"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_16
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 391
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 396
    :pswitch_15
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 397
    .local v17, "direction":I
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v40

    .line 398
    .local v40, "touchTime":J
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_17

    .line 399
    if-nez v17, :cond_18

    .line 400
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_CLIENT_MVREQ LEFT touchTime : "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_17
    :goto_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 406
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setRequestDirection(I)V

    .line 407
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move-wide/from16 v0, v40

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setRequestTouchTime(J)V

    .line 408
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 402
    :cond_18
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_CLIENT_MVREQ RIGHT touchTime : "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 413
    .end local v17    # "direction":I
    .end local v40    # "touchTime":J
    :pswitch_16
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_19

    .line 414
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_CONTENT_URL"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_19
    const/4 v5, 0x2

    aget-object v44, v43, v5

    .line 416
    .local v44, "url":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move-object/from16 v0, v44

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendContentURL(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 421
    .end local v44    # "url":Ljava/lang/String;
    :pswitch_17
    const/16 v21, 0xbb8

    .line 423
    .local v21, "endMsg":I
    const/4 v5, 0x2

    :try_start_4
    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-result v21

    .line 431
    :goto_4
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_1a

    .line 432
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. FINISHED endMsg : "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->finished(I)V

    .line 435
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 436
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    goto/16 :goto_0

    .line 424
    :catch_3
    move-exception v20

    .line 425
    .local v20, "e":Ljava/lang/NullPointerException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_4

    .line 426
    .end local v20    # "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v20

    .line 427
    .local v20, "e":Ljava/lang/RuntimeException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_4

    .line 428
    .end local v20    # "e":Ljava/lang/RuntimeException;
    :catch_5
    move-exception v20

    .line 429
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 441
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "endMsg":I
    :pswitch_18
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_1b

    .line 442
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_DEVICE_INFO"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjInput:Ljava/io/ObjectInputStream;

    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v30

    .line 446
    :try_start_6
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_DEVICE_INFO obj is available"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    check-cast v30, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;

    .end local v30    # "obj":Ljava/lang/Object;
    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendDeviceInfo(Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/EOFException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 448
    :catch_6
    move-exception v20

    .line 449
    .restart local v20    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 455
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v30    # "obj":Ljava/lang/Object;
    :pswitch_19
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 456
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_1c

    .line 457
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SEND_CLIENT_DISPLAY_INFO"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    :cond_1c
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 460
    .local v19, "displayWidth":I
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 461
    .local v18, "displayHeight":I
    const/4 v5, 0x4

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v47

    .line 462
    .local v47, "xdpi":I
    const/4 v5, 0x5

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v48

    .line 463
    .local v48, "ydpi":I
    const/4 v5, 0x6

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v38

    .line 464
    .local v38, "support_multivision":I
    const/4 v5, 0x7

    aget-object v29, v43, v5

    .line 466
    .local v29, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v47

    move/from16 v3, v48

    invoke-virtual {v5, v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setClientDisplayInfo(IIII)V

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move-object/from16 v49, v0

    if-eqz v38, :cond_1d

    const/4 v5, 0x1

    :goto_5
    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setSupportMultiVision(Z)V

    .line 468
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setClientDeviceName(Ljava/lang/String;)V

    .line 469
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 467
    :cond_1d
    const/4 v5, 0x0

    goto :goto_5

    .line 476
    .end local v18    # "displayHeight":I
    .end local v19    # "displayWidth":I
    .end local v29    # "name":Ljava/lang/String;
    .end local v38    # "support_multivision":I
    .end local v47    # "xdpi":I
    .end local v48    # "ydpi":I
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->notifyMessageResponse(I)V

    goto/16 :goto_0

    .line 479
    :pswitch_1b
    const/4 v5, 0x0

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 480
    .local v24, "mMid":I
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v45

    .line 481
    .local v45, "vVol":I
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_1e

    .line 482
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] UPDATE_VOLUME_BY_ID, mId Value is = "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x0

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "Volume  is = "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_1e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerServer:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    move/from16 v0, v24

    move/from16 v1, v45

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->sendVolumeValueUpdateById(II)V

    goto/16 :goto_0

    .line 486
    .end local v24    # "mMid":I
    .end local v45    # "vVol":I
    :pswitch_1c
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_1f

    .line 487
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] receiveMessage. SET_AUDIO_CHANNEL, Audio Channel is = "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_1f
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 489
    .local v4, "audioChannel":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setAudioChannel(I)V

    goto/16 :goto_0

    .line 493
    .end local v4    # "audioChannel":I
    :pswitch_1d
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_20

    .line 494
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] SET_VOLUME_MUTE"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    :cond_20
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setVolumeMute()V

    goto/16 :goto_0

    .line 499
    :pswitch_1e
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_21

    .line 500
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] SET_VOLUME_ON"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->setVolumeOn()V

    goto/16 :goto_0

    .line 505
    :pswitch_1f
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_22

    .line 506
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] SEND_VOLUME_VALUE, Volume Value is = "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const/16 v50, 0x2

    aget-object v50, v43, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_22
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v46

    .line 508
    .local v46, "value":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v46

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendVolumeValue(I)V

    goto/16 :goto_0

    .line 512
    .end local v46    # "value":I
    :pswitch_20
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_23

    .line 513
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] SEND_KEEP_ALIVE"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :cond_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendKeepAlive()V

    goto/16 :goto_1

    .line 518
    :pswitch_21
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->DEBUG:Z

    if-eqz v5, :cond_24

    .line 519
    const-string v5, "MVCommunicator"

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "["

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    move/from16 v50, v0

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, "] SEND_MULTIVISION_DEVICE_DISCONNECTED"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :cond_24
    const/4 v5, 0x2

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 522
    .local v22, "id":I
    const/4 v5, 0x3

    aget-object v5, v43, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v31

    .line 523
    .local v31, "position":I
    const/4 v5, 0x4

    aget-object v29, v43, v5

    .line 524
    .restart local v29    # "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    move/from16 v0, v22

    move/from16 v1, v31

    move-object/from16 v2, v29

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->sendMultiVisionDeviceDisconnected(IILjava/lang/String;)V
    :try_end_7
    .catch Ljava/io/EOFException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    .line 539
    .end local v22    # "id":I
    .end local v29    # "name":Ljava/lang/String;
    .end local v30    # "obj":Ljava/lang/Object;
    .end local v31    # "position":I
    .local v20, "e":Ljava/io/EOFException;
    :cond_25
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    if-eqz v5, :cond_26

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mMVPlayerClient:Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;

    const/16 v49, 0xbb8

    move/from16 v0, v49

    invoke-virtual {v5, v0}, Lcom/sec/android/app/mv/player/multivision/MVPlayerClient;->finished(I)V

    .line 540
    :cond_26
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 541
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    goto/16 :goto_1

    .line 250
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_1
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1a
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_0
        :pswitch_20
        :pswitch_21
        :pswitch_1b
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public finished()V
    .locals 3

    .prologue
    .line 89
    const-string v0, "MVCommunicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] finished"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    .line 92
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    return v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->init()V

    .line 558
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->setPriority(I)V

    .line 560
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mFinished:Z

    if-nez v0, :cond_0

    .line 561
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->receiveMessage()V

    goto :goto_0

    .line 564
    :cond_0
    const-string v0, "MVCommunicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mFinished is true, break condition.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->cleanCommunicator()V

    .line 566
    return-void
.end method

.method public sendMessage(Ljava/lang/String;)V
    .locals 6
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 175
    const/4 v1, 0x0

    .line 176
    .local v1, "type":I
    if-eqz p1, :cond_0

    .line 177
    const/4 v2, 0x0

    .line 178
    .local v2, "unit":[Ljava/lang/String;
    const-string v3, "%"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 179
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 183
    .end local v2    # "unit":[Ljava/lang/String;
    :cond_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    .line 184
    const-string v3, "MVCommunicator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendMessage(msg) E. mConnected : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type: ERRORMSG , msg: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    if-eqz v3, :cond_1

    .line 192
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v3, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 204
    :cond_1
    :goto_1
    return-void

    .line 186
    :cond_2
    const-string v3, "MVCommunicator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendMessage(msg) E. mConnected : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/mv/player/multivision/MVPlayer;->MVPlayerLog:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", msg: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    .line 198
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 207
    const-string v1, "MVCommunicator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessage(msg, obj) E. mConnected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", msg : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    if-eqz v1, :cond_0

    .line 211
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mObjOutput:Ljava/io/ObjectOutputStream;

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 221
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSendLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public setSocket(Ljava/net/Socket;)V
    .locals 4
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 65
    const-string v1, "MVCommunicator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] setSocket"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mSocket:Ljava/net/Socket;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->start()V

    .line 69
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    monitor-enter v2

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnectionLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 73
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/multivision/MVCommunicator;->mConnected:Z

    .line 74
    const-string v1, "MVCommunicator"

    const-string v3, "setSocket ready to server init function, mConnected is true"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :cond_0
    const-string v1, "MVCommunicator"

    const-string v2, "setSocket out"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "MVCommunicator"

    const-string v3, "setSocket failed to receive mConnectionLock signal"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
