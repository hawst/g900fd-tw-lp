.class public Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
.super Ljava/lang/Object;
.source "ShareVideoNodeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NodeItems"
.end annotation


# instance fields
.field public deltaTime:D

.field public isChangeToClient:Z

.field public isMVSupport:Z

.field public nodeDeviceName:Ljava/lang/String;

.field public nodeIP:Ljava/lang/String;

.field public nodeName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeDeviceName:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    .line 16
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    .line 18
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isChangeToClient:Z

    .line 20
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isMVSupport:Z

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getDeltatime()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    return-wide v0
.end method

.method public getNodeIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    return-object v0
.end method

.method public getNodeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    return-object v0
.end method
