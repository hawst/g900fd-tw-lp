.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;
.super Ljava/lang/Thread;
.source "HTTPServerThread.java"


# static fields
.field static final MAX_SERVER_SOCKET:I = 0x64

.field static usedServerSocket:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

.field private mSocket:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;Ljava/net/Socket;)V
    .locals 2
    .param p1, "httpServer"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    .param p2, "sock"    # Ljava/net/Socket;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->TAG:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->TAG:Ljava/lang/String;

    const-string v1, "HTTPServerThread E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->mSocket:Ljava/net/Socket;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP Thread for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/net/Socket;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->setName(Ljava/lang/String;)V

    .line 46
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 57
    new-instance v2, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->mSocket:Ljava/net/Socket;

    invoke-direct {v2, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;-><init>(Ljava/net/Socket;)V

    .line 58
    .local v2, "httpSock":Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;
    new-instance v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    invoke-direct {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;-><init>()V

    .line 59
    .local v1, "httpReq":Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;
    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->setSocket(Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;)V

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->setHttpServer(Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;)V

    .line 63
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->readData()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->mHttpServer:Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->performRequestListener(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)V

    .line 66
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->isKeepAlive()Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_0

    .line 77
    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->hasFileContent()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 78
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->removeTempFile()V

    .line 81
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPSocket;->close()Z

    .line 83
    sget v3, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v3, v3, -0x1

    sput v3, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->usedServerSocket:I

    .line 84
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->TAG:Ljava/lang/String;

    const-string v4, "run Exception"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 73
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Error;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerThread;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "run Error : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/lang/Error;
    :catch_2
    move-exception v3

    goto :goto_0
.end method
