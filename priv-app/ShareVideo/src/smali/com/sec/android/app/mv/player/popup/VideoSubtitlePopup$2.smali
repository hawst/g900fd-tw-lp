.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 221
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->sendSubtitle()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitleValues()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    .line 224
    return-void
.end method
