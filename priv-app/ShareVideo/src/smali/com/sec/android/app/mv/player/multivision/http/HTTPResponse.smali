.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
.super Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;
.source "HTTPResponse.java"


# static fields
.field static final CONTENT_TYPE:Ljava/lang/String; = "text/html; charset=\"utf-8\""


# instance fields
.field private mStatusCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    .line 54
    const-string v0, "text/html; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setServer(Ljava/lang/String;)V

    .line 56
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setContent(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;)V
    .locals 1
    .param p1, "httpRes"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    .line 60
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->set(Lcom/sec/android/app/mv/player/multivision/http/HTTPPacket;)V

    .line 64
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getStatusCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setStatusCode(I)V

    .line 65
    return-void
.end method

.method public static buildHTTPResponse(Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;)Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    .locals 4
    .param p0, "req"    # Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;

    .prologue
    .line 68
    new-instance v1, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;

    invoke-direct {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;-><init>()V

    .line 69
    .local v1, "resp":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    if-nez p0, :cond_1

    .line 70
    const/4 v1, 0x0

    .line 77
    .end local v1    # "resp":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    :cond_0
    :goto_0
    return-object v1

    .line 73
    .restart local v1    # "resp":Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;
    :cond_1
    const-string v2, "Accept-Language"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPRequest;->getHeader(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 74
    .local v0, "acceptLanguage":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 75
    const-string v2, "Content-Language"

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 125
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getHeaderString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 108
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 109
    .local v3, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getNHeaders()I

    move-result v2

    .line 110
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 111
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getHeader(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;

    move-result-object v0

    .line 112
    .local v0, "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 113
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 110
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "header":Lcom/sec/android/app/mv/player/multivision/http/HTTPHeader;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getStatusCode()I
    .locals 2

    .prologue
    .line 91
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    if-eqz v1, :cond_0

    .line 92
    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    .line 94
    :goto_0
    return v1

    .line 93
    :cond_0
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getFirstLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "httpStatus":Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;
    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->getStatusCode()I

    move-result v1

    goto :goto_0
.end method

.method public getStatusLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getStatusCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    invoke-static {v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPStatus;->isSuccessful(I)Z

    move-result v0

    return v0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->mStatusCode:I

    .line 88
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 139
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPResponse;->getContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
