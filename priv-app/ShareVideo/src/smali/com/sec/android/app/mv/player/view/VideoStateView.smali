.class public Lcom/sec/android/app/mv/player/view/VideoStateView;
.super Landroid/widget/ProgressBar;
.source "VideoStateView.java"


# instance fields
.field private mParentView:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 18
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x2

    .line 24
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 25
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 26
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 28
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 40
    return-void
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 31
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 32
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 33
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->requestLayout()V

    .line 35
    return-void
.end method
