.class public Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;
.super Lcom/sec/android/app/mv/player/playlist/PlaylistView;
.source "PlaylistPhoneView.java"


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mPlaylistType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "attr"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;-><init>(Landroid/content/Context;II)V

    .line 25
    iput p2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mPlaylistType:I

    .line 26
    return-void
.end method


# virtual methods
.method public PlaylistListViewInit(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 2
    .param p1, "mRoot"    # Landroid/view/View;
    .param p2, "mOnItemClickListener"    # Landroid/widget/AdapterView$OnItemClickListener;
    .param p3, "mOnItemLongClickListener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 52
    const v0, 0x7f0d0034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    const v1, 0x7f020035

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(I)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_T:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->isSelectableListType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 74
    :cond_0
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 80
    return-void
.end method

.method public saveState()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "firstVisibleItem"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 44
    return-void
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;)V
    .locals 1
    .param p1, "mAdapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 30
    return-void
.end method

.method public setEmptyView()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->emptyData:Landroid/widget/RelativeLayout;

    if-eq v0, v1, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->emptyData:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 40
    :cond_0
    return-void
.end method

.method public setNumberOfColumns()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "selectIndex"    # I

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 34
    return-void
.end method
