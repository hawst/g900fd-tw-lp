.class Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;
.super Landroid/os/Handler;
.source "MVTouchMotion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 20
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 87
    move-object/from16 v0, p1

    iget v14, v0, Landroid/os/Message;->what:I

    packed-switch v14, :pswitch_data_0

    .line 171
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "handleMessage: unexpected code: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :goto_0
    return-void

    .line 89
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 90
    .local v4, "joinTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MSG_REQUEST_MV_JOIN TouchInfo id : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", direction : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", touchtime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$000(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Lcom/sec/android/app/mv/player/multivision/MVUtil;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getSystemTimeBase()J

    move-result-wide v16

    sub-long v10, v14, v16

    .line 94
    .local v10, "systemTime":J
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    sub-long v14, v10, v14

    const-wide/16 v16, 0x3e8

    cmp-long v14, v14, v16

    if-lez v14, :cond_0

    .line 95
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MSG_REQUEST_MV_JOIN TouchInfo was expired time, (systemTime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " - touchTime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ") > EXPIRED_TIME : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x3e8

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 100
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 101
    .local v9, "touchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    invoke-virtual {v14, v4, v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->isMVJoinableTouchInfo(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 102
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$000(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Lcom/sec/android/app/mv/player/multivision/MVUtil;

    move-result-object v14

    new-instance v15, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    move-object/from16 v16, v0

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v17

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v18

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v19

    invoke-direct/range {v15 .. v19}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;-><init>(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;III)V

    invoke-virtual {v14, v15}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->requestMVJoinFromTouchMotion(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;)V

    .line 103
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v9}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 109
    .end local v9    # "touchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    const/4 v14, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->hasMessages(I)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 113
    const-string v14, "MVTouchMotion"

    const-string v15, "MSG_REQUEST_MV_JOIN hasMessages(MSG_TOUCH_EXPIRED_TIME)"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 115
    :cond_3
    const/4 v14, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    .line 116
    .local v7, "newMsg":Landroid/os/Message;
    const-wide/16 v14, 0x3e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v14, v15}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 122
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "joinTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    .end local v7    # "newMsg":Landroid/os/Message;
    .end local v10    # "systemTime":J
    :pswitch_1
    const-string v14, "MVTouchMotion"

    const-string v15, "MSG_CANCEL_TOUCHINFO"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 127
    :pswitch_2
    const-string v14, "MVTouchMotion"

    const-string v15, "MSG_TOUCH_EXPIRED_TIME"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 131
    .local v8, "removeTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 132
    .restart local v9    # "touchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v14

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v15

    if-ne v14, v15, :cond_4

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-nez v14, :cond_4

    .line 133
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MSG_TOUCH_EXPIRED_TIME remove TouchInfo id : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getId()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", direction : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getDirection()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", touchTime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v9}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 140
    .end local v9    # "touchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$000(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Lcom/sec/android/app/mv/player/multivision/MVUtil;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getSystemTimeBase()J

    move-result-wide v16

    sub-long v12, v14, v16

    .line 141
    .local v12, "systemtime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-nez v14, :cond_6

    .line 142
    const-string v14, "MVTouchMotion"

    const-string v15, "MSG_TOUCH_EXPIRED_TIME TouchInfoList is empty"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 143
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_8

    .line 144
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    invoke-virtual {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    sub-long v14, v12, v14

    long-to-int v14, v14

    rsub-int v5, v14, 0x3e8

    .line 145
    .local v5, "msgDelayTime":I
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MSG_TOUCH_EXPIRED_TIME TouchInfoList has one instance, msgDelayTime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    if-gez v5, :cond_7

    .line 147
    const/4 v5, 0x0

    .line 149
    :cond_7
    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v15}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    .line 150
    .restart local v7    # "newMsg":Landroid/os/Message;
    int-to-long v14, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v14, v15}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 152
    .end local v5    # "msgDelayTime":I
    .end local v7    # "newMsg":Landroid/os/Message;
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    invoke-virtual {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    sub-long v14, v12, v14

    long-to-int v14, v14

    rsub-int v5, v14, 0x3e8

    .line 153
    .restart local v5    # "msgDelayTime":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 154
    .local v6, "msgTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-ge v2, v14, :cond_a

    .line 155
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    invoke-virtual {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    sub-long v14, v12, v14

    long-to-int v14, v14

    rsub-int v14, v14, 0x3e8

    if-ge v14, v5, :cond_9

    .line 156
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    invoke-virtual {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;->getTouchTime()J

    move-result-wide v14

    sub-long v14, v12, v14

    long-to-int v14, v14

    rsub-int v5, v14, 0x3e8

    .line 157
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->mTouchInfoList:Ljava/util/List;
    invoke-static {v14}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->access$100(Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "msgTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    check-cast v6, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;

    .line 154
    .restart local v6    # "msgTouchInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$TouchInfo;
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 160
    :cond_a
    if-gez v5, :cond_b

    .line 161
    const/4 v5, 0x0

    .line 163
    :cond_b
    const-string v14, "MVTouchMotion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MSG_TOUCH_EXPIRED_TIME TouchInfoList found nearest TouchInfo instance, msgDelayTime : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/4 v14, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v6}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    .line 165
    .restart local v7    # "newMsg":Landroid/os/Message;
    int-to-long v14, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v14, v15}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
