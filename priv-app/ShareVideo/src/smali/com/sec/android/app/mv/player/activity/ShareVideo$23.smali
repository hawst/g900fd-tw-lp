.class Lcom/sec/android/app/mv/player/activity/ShareVideo$23;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->createKDrmPopup(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

.field final synthetic val$PopUptype:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V
    .locals 0

    .prologue
    .line 2578
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iput p2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->val$PopUptype:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v4, 0xc8

    .line 2580
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createKDRMPopup() - OK type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->val$PopUptype:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2582
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->val$PopUptype:I

    packed-switch v0, :pswitch_data_0

    .line 2609
    :goto_0
    return-void

    .line 2584
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2585
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2586
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    goto :goto_0

    .line 2590
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    goto :goto_0

    .line 2595
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 2599
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->next()Z

    goto :goto_0

    .line 2603
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->prev()Z

    goto :goto_0

    .line 2582
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
