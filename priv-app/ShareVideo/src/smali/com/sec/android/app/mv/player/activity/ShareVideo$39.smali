.class Lcom/sec/android/app/mv/player/activity/ShareVideo$39;
.super Landroid/os/AsyncTask;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 3836
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3836
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 6
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 3844
    const/4 v1, 0x0

    .line 3848
    .local v1, "nodeIP":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3853
    :goto_1
    if-eqz v1, :cond_2

    .line 3854
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNodeEvent. node IP is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3866
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    move-result-object v2

    aget-object v3, p1, v5

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setNodeIP(Ljava/lang/String;Ljava/lang/String;)V

    .line 3868
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3871
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 3877
    :cond_1
    :goto_2
    const/4 v2, 0x0

    return-object v2

    .line 3849
    :catch_0
    move-exception v0

    .line 3850
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3859
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-wide/16 v2, 0xc8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 3860
    :catch_1
    move-exception v0

    .line 3861
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 3872
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v0

    .line 3873
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 3836
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 3839
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 3840
    return-void
.end method
