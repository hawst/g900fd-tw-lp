.class Lcom/sec/android/app/mv/player/adapter/ImageCache$1;
.super Ljava/lang/Object;
.source "ImageCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/adapter/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/adapter/ImageCache;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/adapter/ImageCache;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;->this$0:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;->this$0:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    # invokes: Lcom/sec/android/app/mv/player/adapter/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v1, p1}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$400(Lcom/sec/android/app/mv/player/adapter/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 161
    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 141
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 154
    :cond_0
    return-void

    .line 143
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$Queue;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 144
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache$Queue;->get()Ljava/lang/String;

    move-result-object v1

    .line 146
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_1

    .line 148
    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 149
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 150
    invoke-static {v1, v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;->this$0:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$300(Lcom/sec/android/app/mv/player/adapter/ImageCache;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;->this$0:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    # getter for: Lcom/sec/android/app/mv/player/adapter/ImageCache;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->access$300(Lcom/sec/android/app/mv/player/adapter/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
