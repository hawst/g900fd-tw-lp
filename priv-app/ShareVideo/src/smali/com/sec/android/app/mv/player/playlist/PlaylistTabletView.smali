.class public Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;
.super Lcom/sec/android/app/mv/player/playlist/PlaylistView;
.source "PlaylistTabletView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "attr"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;-><init>(Landroid/content/Context;II)V

    .line 22
    return-void
.end method


# virtual methods
.method public PlaylistListViewInit(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 4
    .param p1, "mRoot"    # Landroid/view/View;
    .param p2, "mOnItemClickListener"    # Landroid/widget/AdapterView$OnItemClickListener;
    .param p3, "mOnItemLongClickListener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    const v3, 0x7f020035

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 78
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    if-ne v0, v2, :cond_2

    .line 79
    const v0, 0x7f0d0034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSelector(I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    const/high16 v1, 0x3000000

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 84
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 87
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_T:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isSelectableListType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView$1;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 133
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    const v0, 0x7f0d0035

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setSelector(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p3}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 113
    :cond_3
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_T:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->isSelectableListType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView$2;-><init>(Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setEnableDragBlock(Z)V

    .line 131
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->setNumberOfColumns()V

    goto :goto_0
.end method

.method public getAdapter()Landroid/widget/BaseAdapter;
    .locals 2

    .prologue
    .line 69
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 72
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 2

    .prologue
    .line 61
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    .line 64
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 142
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mOrientation:I

    if-eq v0, v1, :cond_0

    .line 143
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mOrientation:I

    .line 144
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->setNumberOfColumns()V

    goto :goto_0
.end method

.method public saveState()V
    .locals 3

    .prologue
    .line 53
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "firstVisibleItem"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPref:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "firstVisibleItem"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;)V
    .locals 2
    .param p1, "mAdapter"    # Landroid/widget/BaseAdapter;

    .prologue
    .line 25
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 30
    :goto_0
    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public setEmptyView()V
    .locals 2

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->emptyData:Landroid/widget/RelativeLayout;

    if-eq v0, v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->emptyData:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->emptyData:Landroid/widget/RelativeLayout;

    if-eq v0, v1, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->emptyData:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setNumberOfColumns()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->getColumnsCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 139
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 2
    .param p1, "selectIndex"    # I

    .prologue
    .line 33
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mPlaylistAttr:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_0
.end method
