.class public Lcom/sec/android/app/mv/player/util/TrackInfoUtil;
.super Ljava/lang/Object;
.source "TrackInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TrackInfoUtil"

.field private static final mTrackTypes:[Ljava/lang/String;


# instance fields
.field private mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

.field private mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

.field private mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

.field private mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MEDIA_TRACK_TYPE_UNKNOWN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MEDIA_TRACK_TYPE_VIDEO"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MEDIA_TRACK_TYPE_AUDIO"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MEDIA_TRACK_TYPE_TIMEDTEXT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([Landroid/media/MediaPlayer$TrackInfo;)V
    .locals 1
    .param p1, "tinfo"    # [Landroid/media/MediaPlayer$TrackInfo;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->createTrackInfoForVideoPlayer(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>([Landroid/media/MediaPlayer$TrackInfo;Ljava/lang/String;)V
    .locals 1
    .param p1, "tinfo"    # [Landroid/media/MediaPlayer$TrackInfo;
    .param p2, "timedTextSrc"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    .line 31
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->createTrackInfoForVideoPlayer(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method private saveTrackInfo(Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)V
    .locals 4
    .param p1, "info"    # Landroid/media/MediaPlayer$TrackInfo;
    .param p2, "index"    # I
    .param p3, "OutBandSrc"    # Ljava/lang/String;

    .prologue
    .line 70
    if-nez p1, :cond_0

    .line 71
    const-string v1, "TrackInfoUtil"

    const-string v2, "saveTrackInfo : TrackInfo is NULL!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_0
    :pswitch_0
    return-void

    .line 75
    :cond_0
    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v0

    .line 77
    .local v0, "trackType":I
    if-ltz v0, :cond_1

    sget-object v1, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 78
    const-string v1, "TrackInfoUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveTrackInfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 106
    const-string v1, "TrackInfoUtil"

    const-string v2, "saveTrackInfo : TrackType is NOT in the list Please Check the list with MMFW!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_2

    .line 86
    new-instance v1, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    goto :goto_0

    .line 92
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_3

    .line 93
    new-instance v1, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 95
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    goto :goto_0

    .line 99
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_4

    .line 100
    new-instance v1, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 102
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2, p3}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public clearTracks()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->clear()V

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->clear()V

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->clear()V

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 126
    :cond_2
    return-void
.end method

.method public createTrackInfoForVideoPlayer(Ljava/lang/String;)V
    .locals 3
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    if-nez v2, :cond_1

    .line 67
    :cond_0
    return-void

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    array-length v0, v2

    .line 64
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    aget-object v2, v2, v1

    invoke-direct {p0, v2, v1, p1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->saveTrackInfo(Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)V

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getAudioTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public getVideoTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public hasSavedTrackInfo()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTrackInfo:[Landroid/media/MediaPlayer$TrackInfo;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 53
    :cond_0
    return v0
.end method

.method public initTracks()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_1

    .line 44
    new-instance v0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_2

    .line 47
    new-instance v0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 48
    :cond_2
    return-void
.end method

.method public isSameSrc(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 158
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->isSameSrc(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isTimedTextSourceExist()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->isOutBandTimedText()Z

    move-result v0

    goto :goto_0
.end method

.method public setTimedTextSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->mTimedTracks:Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->setOutBandSource(Ljava/lang/String;)V

    .line 146
    return-void
.end method
