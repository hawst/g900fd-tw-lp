.class Lcom/sec/android/app/mv/player/activity/ShareVideo$22;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2487
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;
    .param p2, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 2489
    const-string v0, "ShareVideo"

    const-string v1, "ServiceConnection - onServiceConnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-static {p2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 2491
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setService(Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)V

    .line 2492
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;

    .prologue
    .line 2495
    const-string v0, "ShareVideo"

    const-string v1, "ServiceConnection - onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 2497
    return-void
.end method
