.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 1822
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/16 v5, 0x68

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1824
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TouchPlayer :: mErrorListener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<<<<<<<<<<<<"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1826
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Z)Z

    .line 1828
    sparse-switch p2, :sswitch_data_0

    .line 1857
    const-string v0, "MoviePlaybackService"

    const-string v2, ">>>>>>>>PLAYBACK_UNKNOWNERROR<<<<<<<<<<<<"

    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    :goto_0
    move v0, v1

    .line 1863
    :goto_1
    return v0

    .line 1830
    :sswitch_0
    const-string v2, "MoviePlaybackService"

    const-string v3, ">>>>>>>>MEDIA_ERROR_SERVER_DIED<<<<<<<<<<<<"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1831
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V
    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Z)V

    .line 1832
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 1833
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 1834
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2302(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 1835
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->access$2300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 1836
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v1, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_1

    .line 1842
    :sswitch_1
    const-string v0, "MoviePlaybackService"

    const-string v2, ">>>>>>>>MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK<<<<<<<<<<<<"

    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x67

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 1847
    :sswitch_2
    const-string v0, "MoviePlaybackService"

    const-string v2, ">>>>>>>>MEDIA_ERROR_NOTSUPPORT<<<<<<<<<<<<"

    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x69

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 1852
    :sswitch_3
    const-string v0, "MoviePlaybackService"

    const-string v2, ">>>>>>>>MEDIA_ERROR_CURRUPT<<<<<<<<<<<<"

    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1853
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;->this$1:Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v2, 0x6a

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 1828
    :sswitch_data_0
    .sparse-switch
        -0xa -> :sswitch_3
        -0x4 -> :sswitch_2
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
