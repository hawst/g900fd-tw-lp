.class public Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
.super Ljava/lang/Object;
.source "TrackInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/TrackInfoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Tracks"
.end annotation


# static fields
.field private static final MAX_INDEX:I = 0x64


# instance fields
.field public Index:[I

.field public Languages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mTimedTextOutBandSrc:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x64

    .line 170
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->this$0:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    .line 166
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    .line 168
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    .line 172
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    .line 174
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;I)V
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 177
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 178
    .local v0, "i":I
    const-string v1, "TrackInfoUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    aput p2, v1, v0

    .line 182
    return-void
.end method

.method public add(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "OutBandSrc"    # Ljava/lang/String;

    .prologue
    .line 186
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    .line 187
    iput-object p3, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    .line 188
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 196
    const/16 v0, 0x64

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    .line 198
    return-void
.end method

.method public isOutBandTimedText()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSameSrc(Ljava/lang/String;)Z
    .locals 1
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->isOutBandTimedText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 215
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOutBandSource(Ljava/lang/String;)V
    .locals 0
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->mTimedTextOutBandSrc:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
