.class Lcom/sec/android/app/mv/player/activity/ShareVideo$2;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCurrentContentStop()V
    .locals 3

    .prologue
    .line 705
    const-string v1, "ShareVideo"

    const-string v2, "onCurrentContentStop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 707
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 708
    return-void
.end method

.method public onMultiVisionDeviceDisconnected(IILjava/lang/String;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "position"    # I
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 712
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMultiVisionDeviceDisconnected deleted client id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", device_name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 715
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 716
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 717
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 718
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 719
    return-void
.end method

.method public onNeedMultiVisionView(II)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "save"    # I

    .prologue
    const/16 v8, 0x19

    const/16 v7, 0x18

    const/16 v6, 0x13

    const/16 v5, 0x12

    .line 661
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNeedMultiVisionView multivision mode position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", save : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 664
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveFontState()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    .line 667
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 668
    const/4 v2, -0x1

    if-eq p1, v2, :cond_1

    .line 669
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 670
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 671
    .local v1, "msg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 672
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 675
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 676
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 677
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 678
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 691
    .end local v0    # "message":Landroid/os/Message;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_2

    .line 692
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setMultiVisionScreen()V

    .line 694
    :cond_2
    return-void

    .line 681
    :cond_3
    const-string v2, "ShareVideo"

    const-string v3, "onNeedMultiVisionView singlevision mode"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 683
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 685
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 686
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 687
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onOpenPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 698
    const-string v1, "ShareVideo"

    const-string v2, "onOpenPath"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 700
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 701
    return-void
.end method

.method public onSendVolumeValue(I)V
    .locals 5
    .param p1, "value"    # I

    .prologue
    .line 737
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSendVolumeValue, Volume value = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    move v1, p1

    .line 739
    .local v1, "volumeValue":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x16

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 740
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 741
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->setVolume(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 742
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/common/SUtils;->sendUpdatedVolumeToMaster(I)V

    .line 744
    :cond_0
    return-void
.end method

.method public onSetScreenRotation(I)V
    .locals 3
    .param p1, "screenMode"    # I

    .prologue
    .line 748
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetScreenRotation, screenMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    const/16 v0, 0x12f

    if-ne p1, v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->forceRotateScreen(I)V

    .line 757
    :goto_0
    return-void

    .line 752
    :cond_0
    const/16 v0, 0x130

    if-ne p1, v0, :cond_1

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->forceRotateScreen(I)V

    goto :goto_0

    .line 755
    :cond_1
    const-string v0, "ShareVideo"

    const-string v1, "screenMode is wrong"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSetVolumeMute()V
    .locals 3

    .prologue
    .line 723
    const-string v1, "ShareVideo"

    const-string v2, "onSetVolumeMute"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 725
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 726
    return-void
.end method

.method public onSetVolumeOn()V
    .locals 3

    .prologue
    .line 730
    const-string v1, "ShareVideo"

    const-string v2, "onSetVolumeOn"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 732
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 733
    return-void
.end method

.method public onStartShareVideo()V
    .locals 2

    .prologue
    .line 640
    const-string v0, "ShareVideo"

    const-string v1, "onStartShareVideo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    return-void
.end method

.method public onStopShareVideo(I)V
    .locals 5
    .param p1, "endMsg"    # I

    .prologue
    const/16 v4, 0x1c

    const/4 v3, 0x3

    .line 645
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStopShareVideo endMsg : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    const/16 v0, 0xbb8

    if-ne p1, v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 657
    :goto_0
    return-void

    .line 650
    :cond_0
    const/16 v0, 0xbb9

    if-ne p1, v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 655
    :cond_1
    const-string v0, "ShareVideo"

    const-string v1, "onStopShareVideo unknown endMsg"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
