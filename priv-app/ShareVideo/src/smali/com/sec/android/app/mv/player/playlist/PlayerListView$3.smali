.class Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;
.super Ljava/lang/Object;
.source "PlayerListView.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setImageCacheListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdated()V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->access$000(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlayerListView$3;->this$0:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlayerListView;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->access$000(Lcom/sec/android/app/mv/player/playlist/PlayerListView;)Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;->notifyDataSetChanged()V

    .line 340
    const-string v0, "VideoPlayerListView"

    const-string v1, "setImageCacheListener() - update UI issued by ImageCache"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    return-void
.end method
