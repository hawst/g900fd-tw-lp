.class Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;
.super Ljava/lang/Object;
.source "PlaylistActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeCallback"
.end annotation


# instance fields
.field mCustomView:Landroid/view/View;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V
    .locals 0

    .prologue
    .line 840
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
    .param p2, "x1"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;

    .prologue
    .line 840
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 903
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "onActionItemClicked"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 907
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 908
    .local v0, "selectedArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    .line 910
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .end local v0    # "selectedArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    :cond_0
    move v2, v3

    .line 942
    :goto_0
    return v2

    .line 913
    .restart local v0    # "selectedArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # invokes: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V
    invoke-static {v4, v2, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$800(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;ILjava/util/ArrayList;)V

    .line 915
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 916
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->removeDeletedIdsFromSelectedList()V

    .line 917
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->removeDeletedIdsFromPlayList(Landroid/content/Context;)V

    .line 920
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->notifyDataSetChanged()V

    .line 922
    const/4 v1, 0x0

    .line 923
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v2, :cond_3

    .line 924
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const v5, 0x7f0a006b

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 929
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-static {v4, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 931
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 932
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 935
    :cond_2
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto :goto_0

    .line 926
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const v5, 0x7f0a006d

    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 910
    :pswitch_data_0
    .packed-switch 0x7f0d00bf
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 11
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v10, 0x7f0d00bf

    const/4 v9, 0x3

    const v8, 0x7f0d00c0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 845
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "onCreateActionMode"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # setter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v3, p1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$202(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 848
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/high16 v6, 0x7f030000

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->mCustomView:Landroid/view/View;

    .line 849
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$200(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/view/ActionMode;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->mCustomView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 850
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->mCustomView:Landroid/view/View;

    const v7, 0x7f0d0006

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v6, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    .line 852
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # setter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v3, v9}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$602(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;I)I

    .line 853
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v6

    invoke-virtual {v3, v6, v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setListType(II)V

    .line 854
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v6

    invoke-virtual {v3, v6, v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setAdapterListType(II)V

    .line 856
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v3

    if-ne v3, v9, :cond_1

    .line 857
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 858
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const v6, 0x7f0d0030

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 859
    .local v1, "selectAll":Landroid/widget/RelativeLayout;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const v6, 0x7f0d002f

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 860
    .local v2, "selectAllLayout":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mOnSelectAllClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$700(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 861
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 863
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 864
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f0c0003

    invoke-virtual {v0, v3, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 866
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-boolean v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v3, :cond_0

    .line 867
    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 868
    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 869
    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 876
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->notifyDataSetChanged()V

    .line 878
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->mCustomView:Landroid/view/View;

    const v6, 0x7f0d0004

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v6, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback$1;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 888
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0082

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v9}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v4

    .line 892
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    .end local v1    # "selectAll":Landroid/widget/RelativeLayout;
    .end local v2    # "selectAllLayout":Landroid/widget/LinearLayout;
    :goto_1
    return v3

    .line 871
    .restart local v0    # "inflater":Landroid/view/MenuInflater;
    .restart local v1    # "selectAll":Landroid/widget/RelativeLayout;
    .restart local v2    # "selectAllLayout":Landroid/widget/LinearLayout;
    :cond_0
    invoke-interface {p2, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 872
    invoke-interface {p2, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .end local v0    # "inflater":Landroid/view/MenuInflater;
    .end local v1    # "selectAll":Landroid/widget/RelativeLayout;
    .end local v2    # "selectAllLayout":Landroid/widget/LinearLayout;
    :cond_1
    move v3, v5

    .line 892
    goto :goto_1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 995
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDestroyActionMode"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    if-eqz v1, :cond_1

    .line 1000
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iput-boolean v4, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    .line 1004
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    iput-boolean v3, v1, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    .line 1007
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$202(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 1008
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # setter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v1, v4}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$602(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;I)I

    .line 1009
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setListType(II)V

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->setAdapterListType(II)V

    .line 1012
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1013
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1016
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1017
    .local v0, "selectAllLayout":Landroid/widget/LinearLayout;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1018
    return-void

    .line 1002
    .end local v0    # "selectAllLayout":Landroid/widget/LinearLayout;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    goto :goto_0
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 8
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "id"    # J
    .param p5, "checked"    # Z

    .prologue
    const v7, 0x7f0d00c0

    const v5, 0x7f0d00bf

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 948
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onItemCheckedStateChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    if-eqz p5, :cond_1

    .line 950
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    invoke-direct {v3, p2, v1}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 959
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemCheckedStateChanged mPlayListView.getSelectedList().size(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->notifyDataSetChanged()V

    .line 963
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v1, :cond_3

    .line 965
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 966
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 989
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0082

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 990
    return-void

    .line 952
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v1

    if-ne v1, p2, :cond_2

    .line 954
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 952
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 968
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 969
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 971
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    .line 972
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v1, :cond_5

    .line 973
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 974
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 976
    :cond_5
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 980
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v1, :cond_7

    .line 981
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 982
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 984
    :cond_7
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 985
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 898
    const/4 v0, 0x1

    return v0
.end method
