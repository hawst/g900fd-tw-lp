.class public Lcom/sec/android/app/mv/player/common/feature/Vintent;
.super Ljava/lang/Object;
.source "Vintent.java"


# static fields
.field public static final ACTION_CLEAR_COVER_OPEN:Ljava/lang/String; = "com.samsung.cover.OPEN"

.field public static final ACTION_GLANCEVIEW_EVENT_INFO:Ljava/lang/String; = "com.android.internal.policy.impl.sec.glanceview.eventinfo"

.field public static final ACTION_HDMI_PLUG:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field public static final ACTION_LINE_OUT:Ljava/lang/String; = "android.intent.action.USB_ANLG_HEADSET_PLUG"

.field public static final ACTION_MULTIWINDOW:Ljava/lang/String; = "com.sec.android.action.NOTIFY_SPLIT_WINDOWS"

.field public static final ACTION_MV_PLAYLIST_SHOW_INFO:Ljava/lang/String; = "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST_SHOW_INFO"

.field public static final ACTION_MV_PLAYLIST_URI_UPDATE:Ljava/lang/String; = "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST"

.field public static ACTION_PAUSE_BY_SPEN:Ljava/lang/String; = null

.field public static ACTION_PLAY_BY_SPEN:Ljava/lang/String; = null

.field public static final ACTION_VIDEOPLAYER_AUTO_OFF_COMPLETE:Ljava/lang/String; = "android.intent.action.VIDEOPLAYER_AUTO_OFF_COMPLETE"

.field public static final ACTIVITY_SAMSUNG_APPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final ACTIVITY_SAMSUNG_APPS_DOWNLOAD_LINK:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static FROM_ASP_SOURCE_DEVICE:Ljava/lang/String; = null

.field public static FROM_GALLERY:Ljava/lang/String; = null

.field public static FROM_SETUP_WIZARD:Ljava/lang/String; = null

.field public static FROM_WIDGET:Ljava/lang/String; = null

.field public static final INTENT_STOP_APP_IN_APP:Ljava/lang/String; = "intent.stop.app-in-app"

.field public static final INTENT_STOP_APP_IN_APP_SEND_APP:Ljava/lang/String; = "intent.stop.app-in-app.send_app"

.field public static final MV_PALYLIST_CALL_PLAYER:Ljava/lang/String; = "mv_call_player"

.field public static final MV_PALYLIST_UPDATE_URI:Ljava/lang/String; = "mv_update_uri"

.field public static final MV_PLAYLIST_ENTER:Ljava/lang/String; = "mv_enter_list"

.field public static final MV_PLAYLIST_EXIT:Ljava/lang/String; = "mv_exit_list"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "com.sec.android.app.mv.player.VIDEOPLAYER_PAUSE"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    .line 12
    const-string v0, "com.sec.android.app.mv.player.VIDEOPLAYER_PLAY"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    .line 15
    const-string v0, "from_gallery_to_videoplayer"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->FROM_GALLERY:Ljava/lang/String;

    .line 16
    const-string v0, "from-sw"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->FROM_SETUP_WIZARD:Ljava/lang/String;

    .line 17
    const-string v0, "isFromWidget"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->FROM_WIDGET:Ljava/lang/String;

    .line 18
    const-string v0, "sourceDeviceTypeFromASP"

    sput-object v0, Lcom/sec/android/app/mv/player/common/feature/Vintent;->FROM_ASP_SOURCE_DEVICE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
