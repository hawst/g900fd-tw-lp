.class public Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "PlaylistCursorAdapter.java"


# static fields
.field private static final PROGRESS_RESOLUTION:J = 0x3e8L


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLastPlayedID:J

.field private mPlaylistAttr:I

.field private mPlaylistType:I

.field mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mSelectedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation
.end field

.field mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;IILcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "type"    # I
    .param p5, "attr"    # I
    .param p6, "v"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 61
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/mv/player/playlist/PlaylistView;)V

    .line 62
    iput p5, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistAttr:I

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "type"    # I
    .param p5, "v"    # Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 32
    const-class v0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->TAG:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    .line 52
    iput p4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    .line 53
    invoke-virtual {p5}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mSelectedList:Ljava/util/ArrayList;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v2, "lastPlayedItem"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mLastPlayedID:J

    .line 58
    return-void
.end method

.method private getPlaylistItem(Landroid/database/Cursor;)Lcom/sec/android/app/mv/player/type/PlaylistItem;
    .locals 19
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 262
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 263
    .local v14, "idIndex":I
    const-string v2, "title"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 264
    .local v18, "titleIndex":I
    const-string v2, "_data"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 265
    .local v11, "dataIndex":I
    const-string v2, "duration"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 266
    .local v13, "durationIndex":I
    const-string v2, "resumePos"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 267
    .local v15, "resumePosIndex":I
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 268
    .local v3, "_id":J
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 269
    .local v5, "title":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 270
    .local v8, "path":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 271
    .local v12, "durText":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 273
    .local v6, "duration":J
    if-eqz v12, :cond_0

    .line 274
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 277
    :cond_0
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 279
    .local v16, "resumePos":J
    new-instance v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    const/4 v9, 0x0

    move-wide/from16 v0, v16

    long-to-int v10, v0

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/app/mv/player/type/PlaylistItem;-><init>(JLjava/lang/String;JLjava/lang/String;II)V

    return-object v2
.end method

.method private isSelected(J)Z
    .locals 7
    .param p1, "vID"    # J

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mSelectedList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mSelectedList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getItem()Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v4

    cmp-long v1, v4, p1

    if-nez v1, :cond_0

    .line 74
    const/4 v1, 0x1

    .line 79
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 72
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 77
    goto :goto_1

    .end local v0    # "i":I
    :cond_2
    move v1, v2

    .line 79
    goto :goto_1
.end method

.method private showCheckbox(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 133
    const v1, 0x7f0d002b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 135
    .local v0, "cb":Landroid/widget/CheckBox;
    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-ne v1, v5, :cond_2

    .line 139
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 141
    :cond_2
    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 144
    :cond_3
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->isSelected(J)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 145
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 149
    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 147
    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method private showDuration(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v9, 0x0

    .line 183
    const v6, 0x7f0d002a

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 184
    .local v4, "tv":Landroid/widget/TextView;
    const v6, 0x7f0d002b

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 185
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v4, :cond_0

    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistAttr:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 190
    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    iget v6, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    .line 191
    :cond_2
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 192
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v6, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v7, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080168

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    mul-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 193
    .local v5, "width":I
    invoke-virtual {v4, v9, v9, v5, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 199
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "width":I
    :cond_3
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "durString":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v6

    int-to-long v6, v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v3

    .line 201
    .local v3, "resumePosString":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 195
    .end local v1    # "durString":Ljava/lang/String;
    .end local v3    # "resumePosString":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4, v9, v9, v9, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private showFooter(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 205
    const v4, 0x7f0d0019

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 206
    .local v2, "layoutCount":Landroid/widget/RelativeLayout;
    const v4, 0x7f0d002c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 208
    .local v0, "bottomLine":Landroid/widget/LinearLayout;
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 209
    .local v1, "count":I
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    add-int/lit8 v5, v1, -0x1

    if-ne v4, v5, :cond_3

    .line 210
    const v4, 0x7f0d001a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 211
    .local v3, "textCount":Landroid/widget/TextView;
    if-ne v1, v9, :cond_2

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0a001d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :goto_0
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 217
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 223
    .end local v3    # "textCount":Landroid/widget/TextView;
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_1

    .line 224
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistAttr:I

    if-ne v4, v9, :cond_0

    .line 225
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 227
    :cond_0
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 229
    :cond_1
    return-void

    .line 214
    .restart local v3    # "textCount":Landroid/widget/TextView;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0a001e

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 219
    .end local v3    # "textCount":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v2, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 220
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private showProgress(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 165
    const v4, 0x7f0d0026

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 166
    .local v1, "pb":Landroid/widget/ProgressBar;
    if-nez v1, :cond_0

    .line 180
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getResumePos()I

    move-result v3

    .line 170
    .local v3, "resumePos":I
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getDuration()J

    move-result-wide v4

    long-to-int v0, v4

    .line 171
    .local v0, "duration":I
    const/4 v2, 0x0

    .line 173
    .local v2, "progress":I
    if-eqz v0, :cond_1

    .line 174
    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    int-to-long v6, v0

    div-long/2addr v4, v6

    long-to-int v2, v4

    .line 177
    :cond_1
    const/16 v4, 0x3e8

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 178
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 179
    if-nez v2, :cond_2

    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private showRecentlyPlayMark(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "mItem"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    .line 155
    const v1, 0x7f0d0027

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 157
    .local v0, "ivLastPlay":Landroid/widget/ImageView;
    iget v1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mLastPlayedID:J

    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getVideoID()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 158
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showThumbnail(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "item"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v4, 0x0

    .line 232
    const v2, 0x7f0d0025

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 234
    .local v1, "videoThumb":Landroid/widget/ImageView;
    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->TAG:Ljava/lang/String;

    const-string v3, "updateThumb() E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 242
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 243
    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 245
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 248
    :cond_2
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private showTitle(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "mItem"    # Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 101
    const v4, 0x7f0d0029

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 102
    .local v2, "tv":Landroid/widget/TextView;
    const v4, 0x7f0d002b

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 104
    .local v0, "cb":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v4, :cond_5

    .line 107
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-ne v4, v8, :cond_3

    .line 108
    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 128
    :cond_2
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v2, v8, v4}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 129
    invoke-virtual {p2}, Lcom/sec/android/app/mv/player/type/PlaylistItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 110
    :cond_3
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-ne v4, v6, :cond_2

    .line 113
    :cond_4
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 114
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v2, v7, v7, v4, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 118
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistAttr:I

    if-ne v4, v8, :cond_2

    .line 119
    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-eq v4, v5, :cond_6

    iget v4, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    if-ne v4, v6, :cond_7

    .line 120
    :cond_6
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 121
    .restart local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v5, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080168

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    mul-int/lit8 v5, v5, 0x2

    add-int v3, v4, v5

    .line 122
    .local v3, "width":I
    invoke-virtual {v2, v7, v7, v3, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 124
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v3    # "width":I
    :cond_7
    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 13
    .param p1, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 254
    const-wide/16 v8, 0x3e8

    div-long v6, p1, v8

    .line 255
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 256
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 257
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 258
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 85
    if-eqz p3, :cond_0

    .line 86
    invoke-direct {p0, p3}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->getPlaylistItem(Landroid/database/Cursor;)Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v0

    .line 88
    .local v0, "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showTitle(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 89
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showCheckbox(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 90
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showRecentlyPlayMark(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 91
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showProgress(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 92
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showDuration(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 93
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showFooter(Landroid/view/View;Landroid/database/Cursor;)V

    .line 94
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->showThumbnail(Landroid/view/View;Lcom/sec/android/app/mv/player/type/PlaylistItem;)V

    .line 96
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 98
    .end local v0    # "mItem":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :cond_0
    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 284
    invoke-super {p0}, Landroid/widget/ResourceCursorAdapter;->notifyDataSetChanged()V

    .line 285
    return-void
.end method

.method public setListType(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "attr"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistType:I

    .line 67
    iput p2, p0, Lcom/sec/android/app/mv/player/adapter/PlaylistCursorAdapter;->mPlaylistAttr:I

    .line 68
    return-void
.end method
