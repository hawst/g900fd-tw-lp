.class public Lcom/sec/android/app/mv/player/flipfont/FontList;
.super Ljava/lang/Object;
.source "FontList.java"


# static fields
.field private static final APPLE_MINT:Ljava/lang/String; = "Apple mint"

.field private static final CHOCO:Ljava/lang/String; = "Choco"

.field private static final CHOCO_COOKY:Ljava/lang/String; = "Choco cooky"

.field private static final COOL:Ljava/lang/String; = "Cool"

.field private static final COOL_JAZZ:Ljava/lang/String; = "Cool jazz"

.field private static final FONT_DIRECTORY:Ljava/lang/String; = "fonts/"

.field private static final FONT_DROID_SANS:Ljava/lang/String; = "/system/fonts/DroidSans.ttf"

.field private static final FONT_EXTENSION:Ljava/lang/String; = ".ttf"

.field private static final FONT_PACKAGE:Ljava/lang/String; = "com.monotype.android.font."

.field private static final KAITI:Ljava/lang/String; = "Kaiti"

.field private static final MARUBERI:Ljava/lang/String; = "Maruberi"

.field private static final MIAO:Ljava/lang/String; = "Miao"

.field private static final MINCHO:Ljava/lang/String; = "Mincho"

.field private static final ROSE:Ljava/lang/String; = "Rose"

.field private static final ROSE_MARY:Ljava/lang/String; = "Rosemary"

.field private static final SHAONV:Ljava/lang/String; = "Shao nv"

.field private static final TAG:Ljava/lang/String; = "FontList"

.field private static final TINKER_BELL:Ljava/lang/String; = "Tinker bell"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDroidSansFont:Landroid/graphics/Typeface;

.field private mFontAssetManager:Landroid/content/res/AssetManager;

.field private mFontNames:Ljava/util/Vector;

.field private mFontPackageNames:Ljava/util/Vector;

.field private mInstalledApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mTypefaceFiles:Ljava/util/Vector;

.field private mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

.field private mTypefaces:Ljava/util/Vector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    .line 42
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 48
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    .line 51
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    .line 54
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    .line 57
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    .line 60
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    .line 63
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    .line 77
    iput-object v2, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 108
    iput-object p1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    .line 109
    new-instance v1, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    invoke-direct {v1, p1}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 112
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/fonts/DroidSans.ttf"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const-string v1, "FontList"

    const-string v2, "fontListInit() :: ttf exists"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v1, "/system/fonts/DroidSans.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 117
    :cond_0
    return-void
.end method


# virtual methods
.method public fontListInit()V
    .locals 10

    .prologue
    .line 124
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v5, :cond_0

    .line 125
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    .line 127
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    .line 129
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->clearTypfaceArray()V

    .line 131
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v6, 0x80

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    .line 133
    const/4 v2, 0x0

    .line 134
    .local v2, "fontPackageName":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ApplicationInfo;

    iget-object v2, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 136
    const-string v5, "com.monotype.android.font."

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v6, 0x80

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 138
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v5, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 139
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v4

    .line 141
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 142
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v5, v6, v2}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    .line 134
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "res":Landroid/content/res/Resources;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 145
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v5, :cond_3

    .line 146
    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->getSansEntries(Landroid/content/pm/PackageManager;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v2    # "fontPackageName":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_3
    :goto_1
    return-void

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "FontList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "font package not found, just use default font, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFont(I)Landroid/graphics/Typeface;
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 295
    :goto_0
    return-object v0

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-nez v1, :cond_1

    .line 291
    const-string v1, "FontList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFont() :: input = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 294
    :cond_1
    const-string v1, "FontList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFont() :: input = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 9
    .param p1, "typeface"    # Ljava/lang/String;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 261
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "default"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 263
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 284
    :goto_0
    return-object v6

    .line 266
    :cond_1
    const/16 v6, 0x2f

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 267
    .local v5, "start_pos":I
    const/16 v6, 0x2e

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 268
    .local v2, "end_pos":I
    if-gez v2, :cond_2

    .line 269
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 271
    :cond_2
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 272
    .local v3, "loadTypeface":Ljava/lang/String;
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 274
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x80

    invoke-virtual {v6, p2, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 275
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v6, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 276
    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v4

    .line 278
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "res":Landroid/content/res/Resources;
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fonts/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".ttf"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefacesClass;->get(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    goto :goto_0

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "FontList"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "font package not found, just use default font, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getFontName(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 168
    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 170
    .local v0, "tmpString":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 172
    .end local v0    # "tmpString":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "fontStringName"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0a0051

    const v2, 0x7f0a004b

    const v1, 0x7f0a004a

    .line 177
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    :goto_0
    return-object v0

    .line 181
    :cond_1
    const-string v0, "Cool"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    :cond_2
    :goto_1
    move-object v0, p1

    .line 207
    goto :goto_0

    .line 183
    :cond_3
    const-string v0, "Rose"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 185
    :cond_4
    const-string v0, "Choco"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 187
    :cond_5
    const-string v0, "Rosemary"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 189
    :cond_6
    const-string v0, "Choco cooky"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 191
    :cond_7
    const-string v0, "Cool jazz"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 193
    :cond_8
    const-string v0, "Apple mint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 195
    :cond_9
    const-string v0, "Tinker bell"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 197
    :cond_a
    const-string v0, "Shao nv"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 199
    :cond_b
    const-string v0, "Kaiti"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 201
    :cond_c
    const-string v0, "Miao"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 203
    :cond_d
    const-string v0, "Maruberi"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 205
    :cond_e
    const-string v0, "Mincho"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method public getFontPackageName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFontStringName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTypefaceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadTypefaces()V
    .locals 6

    .prologue
    .line 238
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-nez v4, :cond_1

    .line 254
    :cond_0
    return-void

    .line 241
    :cond_1
    const/4 v1, 0x0

    .line 242
    .local v1, "fontfile":Ljava/lang/String;
    const/4 v0, 0x0

    .line 243
    .local v0, "fontPackageName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 244
    .local v3, "newTypeface":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 245
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 246
    const/4 v2, 0x1

    .line 247
    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/mv/player/flipfont/FontList;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 251
    iget-object v4, p0, Lcom/sec/android/app/mv/player/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 252
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
