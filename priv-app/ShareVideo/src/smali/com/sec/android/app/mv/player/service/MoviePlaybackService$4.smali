.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/common/manager/MotionManager$OnMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 3
    .param p1, "event"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 388
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "motion : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 391
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->pause()V

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    goto :goto_0

    .line 389
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
