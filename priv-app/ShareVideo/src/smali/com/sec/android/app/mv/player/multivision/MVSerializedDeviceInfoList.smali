.class public Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;
.super Ljava/lang/Object;
.source "MVSerializedDeviceInfoList.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x20L


# instance fields
.field private mDeviceInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;->mDeviceInfoList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getDeviceInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;->mDeviceInfoList:Ljava/util/List;

    return-object v0
.end method

.method public setDeviceInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "deviceInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;>;"
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVSerializedDeviceInfoList;->mDeviceInfoList:Ljava/util/List;

    .line 19
    return-void
.end method
