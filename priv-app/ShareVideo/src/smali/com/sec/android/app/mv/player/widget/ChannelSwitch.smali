.class public Lcom/sec/android/app/mv/player/widget/ChannelSwitch;
.super Landroid/widget/CompoundButton;
.source "ChannelSwitch.java"


# static fields
.field private static final CHECKED_STATE_SET:[I

.field private static final MONOSPACE:I = 0x3

.field private static final SANS:I = 0x1

.field private static final SERIF:I = 0x2

.field public static final TAG:Ljava/lang/String;

.field private static final TOUCH_MODE_DOWN:I = 0x1

.field private static final TOUCH_MODE_DRAGGING:I = 0x2

.field private static final TOUCH_MODE_IDLE:I


# instance fields
.field mContext:Landroid/content/Context;

.field private mIsDeviceDefaultTheme:Z

.field mIsKorean:Ljava/lang/Boolean;

.field private mMinFlingVelocity:I

.field private mOffLayout:Landroid/text/Layout;

.field private mOnLayout:Landroid/text/Layout;

.field private mSwitchBottom:I

.field private mSwitchHeight:I

.field private mSwitchLeft:I

.field private mSwitchMinWidth:I

.field private mSwitchPadding:I

.field private mSwitchRight:I

.field private mSwitchTop:I

.field private mSwitchWidth:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTextColors:Landroid/content/res/ColorStateList;

.field mTextColorsOff:Landroid/content/res/ColorStateList;

.field mTextColorsOn:Landroid/content/res/ColorStateList;

.field private mTextOff:Ljava/lang/CharSequence;

.field private mTextOn:Ljava/lang/CharSequence;

.field private mTextPaint:Landroid/text/TextPaint;

.field private mThumbDrawable:Landroid/graphics/drawable/Drawable;

.field private mThumbPosition:F

.field private mThumbTextPadding:I

.field private mThumbWidth:I

.field private mTouchMode:I

.field private mTouchSlop:I

.field private mTouchX:F

.field private mTouchY:F

.field private mTrackDrawable:Landroid/graphics/drawable/Drawable;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field final switchOff:Ljava/lang/CharSequence;

.field final switchOn:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->TAG:Ljava/lang/String;

    .line 127
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "state_checked"

    const-string v4, "attr"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 146
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 158
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "switchStyle"

    const-string v2, "attr"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    .line 160
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, 0x0

    .line 174
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 118
    const-string v5, "ON"

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->switchOn:Ljava/lang/CharSequence;

    .line 120
    const-string v5, "OFF"

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->switchOff:Ljava/lang/CharSequence;

    .line 125
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    .line 131
    iput-boolean v7, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    .line 175
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    .line 177
    new-instance v5, Landroid/text/TextPaint;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 179
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    iput v6, v5, Landroid/text/TextPaint;->density:F

    .line 182
    const-string v5, "Switch"

    invoke-static {v5}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableIntArray(Ljava/lang/String;)[I

    move-result-object v5

    invoke-virtual {p1, p2, v5, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 185
    .local v0, "a":Landroid/content/res/TypedArray;
    :try_start_0
    const-string v5, "Switch_thumb"

    invoke-static {v5}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    .line 186
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    .line 187
    const-string v5, ""

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    .line 188
    const-string v5, ""

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    .line 189
    const-string v5, "Switch_thumbTextPadding"

    invoke-static {v5}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbTextPadding:I

    .line 190
    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08001e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchMinWidth:I

    .line 191
    const-string v5, "Switch_switchPadding"

    invoke-static {v5}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchPadding:I

    .line 193
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsKorean:Ljava/lang/Boolean;

    .line 195
    const-string v5, "Switch_switchTextAppearance"

    invoke-static {v5}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 197
    .local v1, "appearance":I
    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setSwitchTextAppearance(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 205
    const/4 v0, 0x0

    .line 208
    .end local v1    # "appearance":I
    :goto_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    .line 209
    .local v2, "config":Landroid/view/ViewConfiguration;
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    .line 210
    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mMinFlingVelocity:I

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->refreshDrawableState()V

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    .line 216
    return-void

    .line 200
    .end local v2    # "config":Landroid/view/ViewConfiguration;
    :catch_0
    move-exception v3

    .line 201
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v5, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->TAG:Ljava/lang/String;

    const-string v6, "ChannelSwitch tryCatch"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 205
    const/4 v0, 0x0

    .line 206
    goto :goto_0

    .line 204
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 205
    const/4 v0, 0x0

    throw v5
.end method

.method private animateThumbToCheckedState(Z)V
    .locals 0
    .param p1, "newCheckedState"    # Z

    .prologue
    .line 693
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setChecked(Z)V

    .line 694
    return-void
.end method

.method private cancelSuperTouch(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 654
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 655
    .local v0, "cancel":Landroid/view/MotionEvent;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 656
    invoke-super {p0, v0}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 657
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 658
    return-void
.end method

.method private getTargetCheckedState()Z
    .locals 3

    .prologue
    .line 697
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getThumbScrollRange()I
    .locals 2

    .prologue
    .line 818
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 819
    const/4 v0, 0x0

    .line 822
    :goto_0
    return v0

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 822
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchWidth:I

    iget v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbWidth:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private hitThumb(FF)Z
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 577
    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 578
    iget v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchTop:I

    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    sub-int v3, v4, v5

    .line 579
    .local v3, "thumbTop":I
    iget v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchLeft:I

    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v5, v6

    float-to-int v5, v5

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    sub-int v1, v4, v5

    .line 580
    .local v1, "thumbLeft":I
    iget v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbWidth:I

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    add-int v2, v4, v5

    .line 582
    .local v2, "thumbRight":I
    iget v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchBottom:I

    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    add-int v0, v4, v5

    .line 583
    .local v0, "thumbBottom":I
    int-to-float v4, v1

    cmpl-float v4, p1, v4

    if-lez v4, :cond_0

    int-to-float v4, v2

    cmpg-float v4, p1, v4

    if-gez v4, :cond_0

    int-to-float v4, v3

    cmpl-float v4, p2, v4

    if-lez v4, :cond_0

    int-to-float v4, v0

    cmpg-float v4, p2, v4

    if-gez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 561
    move-object v1, p1

    .line 565
    .local v1, "transformed":Ljava/lang/CharSequence;
    if-nez v1, :cond_0

    .line 566
    move-object v1, p1

    .line 569
    :cond_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method

.method private setSwitchTypefaceByIndex(II)V
    .locals 1
    .param p1, "typefaceIndex"    # I
    .param p2, "styleIndex"    # I

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "tf":Landroid/graphics/Typeface;
    packed-switch p1, :pswitch_data_0

    .line 282
    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setSwitchTypeface(Landroid/graphics/Typeface;I)V

    .line 283
    return-void

    .line 270
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 271
    goto :goto_0

    .line 274
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    .line 275
    goto :goto_0

    .line 278
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    goto :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private stopDrag(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 667
    iput v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    .line 670
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 672
    .local v0, "commitChange":Z
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->cancelSuperTouch(Landroid/view/MotionEvent;)V

    .line 674
    if-eqz v0, :cond_3

    .line 676
    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 677
    iget-object v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    .line 678
    .local v2, "xvel":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mMinFlingVelocity:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 679
    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_1

    move v1, v3

    .line 683
    .local v1, "newState":Z
    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->animateThumbToCheckedState(Z)V

    .line 687
    .end local v1    # "newState":Z
    .end local v2    # "xvel":F
    :goto_2
    return-void

    .end local v0    # "commitChange":Z
    :cond_0
    move v0, v4

    .line 670
    goto :goto_0

    .restart local v0    # "commitChange":Z
    .restart local v2    # "xvel":F
    :cond_1
    move v1, v4

    .line 679
    goto :goto_1

    .line 681
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getTargetCheckedState()Z

    move-result v1

    .restart local v1    # "newState":Z
    goto :goto_1

    .line 685
    .end local v1    # "newState":Z
    .end local v2    # "xvel":F
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->animateThumbToCheckedState(Z)V

    goto :goto_2
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 836
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    .line 838
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getDrawableState()[I

    move-result-object v0

    .line 843
    .local v0, "myDrawableState":[I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 844
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 845
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 846
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 848
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->invalidate()V

    .line 849
    return-void
.end method

.method public getCompoundPaddingRight()I
    .locals 3

    .prologue
    .line 810
    invoke-super {p0}, Landroid/widget/CompoundButton;->getCompoundPaddingRight()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchWidth:I

    add-int v0, v1, v2

    .line 811
    .local v0, "padding":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 812
    iget v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchPadding:I

    add-int/2addr v0, v1

    .line 814
    :cond_0
    return v0
.end method

.method public getSwitchMinWidth()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchMinWidth:I

    return v0
.end method

.method public getSwitchPadding()I
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchPadding:I

    return v0
.end method

.method public getTextOff()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextOn()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getThumbDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getThumbTextPadding()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbTextPadding:I

    return v0
.end method

.method public getTrackDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 858
    invoke-super {p0}, Landroid/widget/CompoundButton;->jumpDrawablesToCurrentState()V

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 861
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 827
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 828
    .local v0, "drawableState":[I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 829
    sget-object v1, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mergeDrawableStates([I[I)[I

    .line 831
    :cond_0
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 759
    invoke-super/range {p0 .. p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 762
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchLeft:I

    .line 763
    .local v6, "switchLeft":I
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchTop:I

    .line 764
    .local v9, "switchTop":I
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchRight:I

    .line 765
    .local v7, "switchRight":I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchBottom:I

    .line 767
    .local v1, "switchBottom":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v6, v9, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 768
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 770
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 772
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v14}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 773
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    add-int v3, v6, v13

    .line 774
    .local v3, "switchInnerLeft":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    add-int v5, v9, v13

    .line 775
    .local v5, "switchInnerTop":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    sub-int v4, v7, v13

    .line 776
    .local v4, "switchInnerRight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v1, v13

    .line 777
    .local v2, "switchInnerBottom":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 779
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v14}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 780
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    const/high16 v14, 0x3f000000    # 0.5f

    add-float/2addr v13, v14

    float-to-int v11, v13

    .line 781
    .local v11, "thumbPos":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    sub-int v13, v3, v13

    add-int v10, v13, v11

    .line 782
    .local v10, "thumbLeft":I
    add-int v13, v3, v11

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbWidth:I

    add-int/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    add-int v12, v13, v14

    .line 784
    .local v12, "thumbRight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v13, v10, v9, v12, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 785
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 787
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070050

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/text/TextPaint;->setColor(I)V

    .line 788
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getDrawableState()[I

    move-result-object v14

    iput-object v14, v13, Landroid/text/TextPaint;->drawableState:[I

    .line 790
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    if-eqz v13, :cond_2

    .line 791
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsKorean:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 792
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getTargetCheckedState()Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    .line 793
    .local v8, "switchText":Landroid/text/Layout;
    :goto_0
    add-int v13, v10, v12

    div-int/lit8 v13, v13, 0x2

    invoke-virtual {v8}, Landroid/text/Layout;->getWidth()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    sub-int/2addr v13, v14

    int-to-float v13, v13

    add-int v14, v5, v2

    div-int/lit8 v14, v14, 0x2

    invoke-virtual {v8}, Landroid/text/Layout;->getHeight()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    sub-int/2addr v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    .line 796
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 805
    .end local v8    # "switchText":Landroid/text/Layout;
    :cond_0
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 806
    return-void

    .line 792
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    goto :goto_0

    .line 799
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getTargetCheckedState()Z

    move-result v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    .line 800
    .restart local v8    # "switchText":Landroid/text/Layout;
    :goto_2
    add-int v13, v10, v12

    div-int/lit8 v13, v13, 0x2

    invoke-virtual {v8}, Landroid/text/Layout;->getWidth()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    sub-int/2addr v13, v14

    int-to-float v13, v13

    add-int v14, v5, v2

    div-int/lit8 v14, v14, 0x2

    invoke-virtual {v8}, Landroid/text/Layout;->getHeight()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    sub-int/2addr v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    .line 803
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 799
    .end local v8    # "switchText":Landroid/text/Layout;
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    goto :goto_2
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 865
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 866
    const-class v0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 867
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 5
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 871
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 872
    const-class v3, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    .line 874
    .local v2, "switchText":Ljava/lang/CharSequence;
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 875
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 876
    .local v1, "oldText":Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 877
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 884
    .end local v1    # "oldText":Ljava/lang/CharSequence;
    :cond_0
    :goto_1
    return-void

    .line 873
    .end local v2    # "switchText":Ljava/lang/CharSequence;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    goto :goto_0

    .line 879
    .restart local v1    # "oldText":Ljava/lang/CharSequence;
    .restart local v2    # "switchText":Ljava/lang/CharSequence;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 880
    .local v0, "newText":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 881
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 709
    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getThumbScrollRange()I

    move-result v5

    int-to-float v5, v5

    :goto_0
    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    .line 713
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getPaddingRight()I

    move-result v6

    sub-int v3, v5, v6

    .line 714
    .local v3, "switchRight":I
    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchWidth:I

    sub-int v2, v3, v5

    .line 715
    .local v2, "switchLeft":I
    const/4 v4, 0x0

    .line 716
    .local v4, "switchTop":I
    const/4 v1, 0x0

    .line 717
    .local v1, "switchBottom":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getGravity()I

    move-result v5

    and-int/lit8 v5, v5, 0x70

    sparse-switch v5, :sswitch_data_0

    .line 720
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getPaddingTop()I

    move-result v4

    .line 721
    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    add-int v1, v4, v5

    .line 736
    :goto_1
    iput v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchLeft:I

    .line 737
    iput v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchTop:I

    .line 738
    iput v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchBottom:I

    .line 739
    iput v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchRight:I

    .line 748
    const/4 v0, 0x0

    .line 750
    .local v0, "isDeviceDefault":Z
    if-eqz v0, :cond_1

    .line 751
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    .line 755
    :goto_2
    return-void

    .line 711
    .end local v0    # "isDeviceDefault":Z
    .end local v1    # "switchBottom":I
    .end local v2    # "switchLeft":I
    .end local v3    # "switchRight":I
    .end local v4    # "switchTop":I
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 725
    .restart local v1    # "switchBottom":I
    .restart local v2    # "switchLeft":I
    .restart local v3    # "switchRight":I
    .restart local v4    # "switchTop":I
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    div-int/lit8 v6, v6, 0x2

    sub-int v4, v5, v6

    .line 727
    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    add-int v1, v4, v5

    .line 728
    goto :goto_1

    .line 731
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getPaddingBottom()I

    move-result v6

    sub-int v1, v5, v6

    .line 732
    iget v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    sub-int v4, v1, v5

    goto :goto_1

    .line 753
    .restart local v0    # "isDeviceDefault":Z
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    goto :goto_2

    .line 717
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 505
    const/4 v1, 0x0

    .line 506
    .local v1, "isDeviceDefault":Z
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    .line 507
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsDeviceDefaultTheme:Z

    if-eqz v3, :cond_5

    .line 508
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mIsKorean:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 509
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    if-nez v3, :cond_0

    .line 510
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    .line 512
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    if-nez v3, :cond_1

    .line 513
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    .line 533
    :cond_1
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbWidth:I

    .line 535
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchWidth:I

    .line 536
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    .line 545
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getMeasuredHeight()I

    move-result v2

    .line 546
    .local v2, "measuredHeight":I
    iget v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    if-ge v2, v3, :cond_2

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getMeasuredWidthAndState()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchHeight:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setMeasuredDimension(II)V

    .line 549
    :cond_2
    return-void

    .line 516
    .end local v2    # "measuredHeight":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    if-nez v3, :cond_4

    .line 517
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->switchOn:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    .line 519
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    if-nez v3, :cond_1

    .line 520
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->switchOff:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    goto :goto_0

    .line 524
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    if-nez v3, :cond_6

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    .line 527
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    if-nez v3, :cond_1

    .line 528
    iget-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->makeLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    goto :goto_0

    .line 537
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->TAG:Ljava/lang/String;

    const-string v4, " onMeasure"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 541
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    throw v3
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 553
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 554
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOnLayout:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 555
    .local v0, "text":Ljava/lang/CharSequence;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 556
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 558
    :cond_0
    return-void

    .line 554
    .end local v0    # "text":Ljava/lang/CharSequence;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mOffLayout:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x1

    .line 588
    iget-object v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 589
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 590
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 649
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    .line 650
    :cond_1
    :goto_1
    return v3

    .line 592
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 593
    .local v4, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 594
    .local v5, "y":F
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->hitThumb(FF)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 595
    iput v3, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    .line 596
    iput v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchX:F

    .line 597
    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchY:F

    goto :goto_0

    .line 603
    .end local v4    # "x":F
    .end local v5    # "y":F
    :pswitch_2
    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    packed-switch v6, :pswitch_data_1

    goto :goto_0

    .line 609
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 610
    .restart local v4    # "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 611
    .restart local v5    # "y":F
    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchX:F

    sub-float v6, v4, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_2

    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchY:F

    sub-float v6, v5, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchSlop:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    .line 613
    :cond_2
    iput v8, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    .line 614
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 615
    iput v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchX:F

    .line 616
    iput v5, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchY:F

    goto :goto_1

    .line 623
    .end local v4    # "x":F
    .end local v5    # "y":F
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 624
    .restart local v4    # "x":F
    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchX:F

    sub-float v1, v4, v6

    .line 625
    .local v1, "dx":F
    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    add-float/2addr v7, v1

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getThumbScrollRange()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 627
    .local v2, "newPos":F
    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    cmpl-float v6, v2, v6

    if-eqz v6, :cond_1

    .line 628
    iput v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    .line 629
    iput v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchX:F

    .line 630
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->invalidate()V

    goto :goto_1

    .line 640
    .end local v1    # "dx":F
    .end local v2    # "newPos":F
    .end local v4    # "x":F
    :pswitch_5
    iget v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    if-ne v6, v8, :cond_3

    .line 641
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->stopDrag(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 644
    :cond_3
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTouchMode:I

    .line 645
    iget-object v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    .line 590
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 603
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 702
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 703
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->getThumbScrollRange()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    iput v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbPosition:F

    .line 704
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->invalidate()V

    .line 705
    return-void

    .line 703
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSwitchMinWidth(I)V
    .locals 0
    .param p1, "pixels"    # I

    .prologue
    .line 363
    iput p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchMinWidth:I

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 365
    return-void
.end method

.method public setSwitchPadding(I)V
    .locals 0
    .param p1, "pixels"    # I

    .prologue
    .line 339
    iput p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mSwitchPadding:I

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 341
    return-void
.end method

.method public setSwitchTextAppearance(Landroid/content/Context;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resid"    # I

    .prologue
    .line 225
    const-string v6, "TextAppearance"

    invoke-static {v6}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableIntArray(Ljava/lang/String;)[I

    move-result-object v6

    invoke-virtual {p1, p2, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 230
    .local v0, "appearance":Landroid/content/res/TypedArray;
    const/4 v1, -0x1

    .line 231
    .local v1, "color":I
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextColors:Landroid/content/res/ColorStateList;

    .line 234
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08001b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 235
    .local v4, "ts":I
    if-eqz v4, :cond_0

    .line 236
    int-to-float v6, v4

    iget-object v7, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->getTextSize()F

    move-result v7

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_0

    .line 237
    iget-object v6, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    int-to-float v7, v4

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 243
    :cond_0
    const-string v6, "TextAppearance_typeface"

    invoke-static {v6}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 245
    .local v5, "typefaceIndex":I
    const-string v6, "TextAppearance_textStyle"

    invoke-static {v6}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 247
    .local v3, "styleIndex":I
    or-int/lit8 v6, v3, 0x1

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setSwitchTypefaceByIndex(II)V

    .line 249
    const-string v6, "TextAppearance_textAllCaps"

    invoke-static {v6}, Lcom/sec/android/app/mv/player/util/Utils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 264
    .end local v3    # "styleIndex":I
    .end local v4    # "ts":I
    .end local v5    # "typefaceIndex":I
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v2

    .line 258
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v6, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->TAG:Ljava/lang/String;

    const-string v7, "setSwitchTextAppearance"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v6
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->invalidate()V

    .line 329
    :cond_0
    return-void
.end method

.method public setSwitchTypeface(Landroid/graphics/Typeface;I)V
    .locals 6
    .param p1, "tf"    # Landroid/graphics/Typeface;
    .param p2, "style"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 292
    if-lez p2, :cond_4

    .line 293
    if-nez p1, :cond_1

    .line 294
    invoke-static {p2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object p1

    .line 299
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    .line 301
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v1

    .line 302
    .local v1, "typefaceStyle":I
    :goto_1
    xor-int/lit8 v4, v1, -0x1

    and-int v0, p2, v4

    .line 303
    .local v0, "need":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    and-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_3

    const/high16 v2, -0x41800000    # -0.25f

    :goto_2
    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 310
    .end local v0    # "need":I
    .end local v1    # "typefaceStyle":I
    :goto_3
    return-void

    .line 296
    :cond_1
    invoke-static {p1, p2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object p1

    goto :goto_0

    :cond_2
    move v1, v2

    .line 301
    goto :goto_1

    .restart local v0    # "need":I
    .restart local v1    # "typefaceStyle":I
    :cond_3
    move v2, v3

    .line 304
    goto :goto_2

    .line 306
    .end local v0    # "need":I
    .end local v1    # "typefaceStyle":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 308
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setSwitchTypeface(Landroid/graphics/Typeface;)V

    goto :goto_3
.end method

.method public setTextOff(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "textOff"    # Ljava/lang/CharSequence;

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOff:Ljava/lang/CharSequence;

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 501
    return-void
.end method

.method public setTextOn(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "textOn"    # Ljava/lang/CharSequence;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTextOn:Ljava/lang/CharSequence;

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 482
    return-void
.end method

.method public setThumbDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "thumb"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 439
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 441
    return-void
.end method

.method public setThumbResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 452
    return-void
.end method

.method public setThumbTextPadding(I)V
    .locals 0
    .param p1, "pixels"    # I

    .prologue
    .line 386
    iput p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbTextPadding:I

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 388
    return-void
.end method

.method public setTrackDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "track"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->requestLayout()V

    .line 409
    return-void
.end method

.method public setTrackResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 419
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 853
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mThumbDrawable:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/widget/ChannelSwitch;->mTrackDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
