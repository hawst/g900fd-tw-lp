.class public final Lcom/sec/android/app/mv/player/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_switch_multi_vision:I = 0x7f020000

.field public static final actionbar_switch_single_vision:I = 0x7f020001

.field public static final btn_control_next:I = 0x7f020002

.field public static final btn_control_pause:I = 0x7f020003

.field public static final btn_control_play:I = 0x7f020004

.field public static final btn_control_prev:I = 0x7f020005

.field public static final btn_control_stop:I = 0x7f020006

.field public static final btn_lock:I = 0x7f020007

.field public static final btn_more_option:I = 0x7f020008

.field public static final btn_playerlist:I = 0x7f020009

.field public static final btn_playerlist_on:I = 0x7f02000a

.field public static final btn_playspeed_minus:I = 0x7f02000b

.field public static final btn_playspeed_plus:I = 0x7f02000c

.field public static final btn_rotate_selector:I = 0x7f02000d

.field public static final btn_toggle_02:I = 0x7f02000e

.field public static final btn_toggle_02_pressed:I = 0x7f02000f

.field public static final btn_toggle_02_selected:I = 0x7f020010

.field public static final btn_view_mode_fill:I = 0x7f020011

.field public static final btn_view_mode_fit_height:I = 0x7f020012

.field public static final btn_view_mode_keep_aspect:I = 0x7f020013

.field public static final btn_view_mode_orig:I = 0x7f020014

.field public static final check_icon_selector:I = 0x7f020015

.field public static final dark_no_item_v:I = 0x7f020016

.field public static final fairview_arrow:I = 0x7f020017

.field public static final fairview_hover_video_bg_c:I = 0x7f020018

.field public static final groupcast_action_add:I = 0x7f020019

.field public static final groupcast_action_add_dim:I = 0x7f02001a

.field public static final groupcast_action_delete:I = 0x7f02001b

.field public static final groupcast_action_delete_dim:I = 0x7f02001c

.field public static final groupcast_action_group:I = 0x7f02001d

.field public static final groupcast_action_more:I = 0x7f02001e

.field public static final groupcast_add_selector:I = 0x7f02001f

.field public static final groupcast_delete_selector:I = 0x7f020020

.field public static final groupcast_main_video:I = 0x7f020021

.field public static final groupcast_mute_focus:I = 0x7f020022

.field public static final groupcast_mute_normal:I = 0x7f020023

.field public static final groupcast_mute_press:I = 0x7f020024

.field public static final groupcast_volume_focus:I = 0x7f020025

.field public static final groupcast_volume_normal:I = 0x7f020026

.field public static final groupcast_volume_press:I = 0x7f020027

.field public static final help_list_thumb_view_bg_1:I = 0x7f020028

.field public static final help_list_thumb_view_bg_2:I = 0x7f020029

.field public static final help_list_thumb_view_bg_3:I = 0x7f02002a

.field public static final help_list_thumb_view_bg_4:I = 0x7f02002b

.field public static final help_popup_picker_bg_w_01:I = 0x7f02002c

.field public static final help_popup_picker_t_c:I = 0x7f02002d

.field public static final help_tap_1_default:I = 0x7f02002e

.field public static final hover_popup_black_bg:I = 0x7f02002f

.field public static final hover_popup_white_bg:I = 0x7f020030

.field public static final hover_video_bg_c:I = 0x7f020031

.field public static final hover_video_bg_l:I = 0x7f020032

.field public static final hover_video_bg_r:I = 0x7f020033

.field public static final list_divider_holo_dark:I = 0x7f020034

.field public static final list_selector:I = 0x7f020035

.field public static final list_selector_item:I = 0x7f020036

.field public static final lock_focused:I = 0x7f020037

.field public static final lock_normal:I = 0x7f020038

.field public static final lock_pressed:I = 0x7f020039

.field public static final music_fullplayer_progress_secondary:I = 0x7f02003a

.field public static final mv_btn_volume:I = 0x7f02003b

.field public static final mv_btn_volume_mute:I = 0x7f02003c

.field public static final seek_thumb:I = 0x7f02003d

.field public static final selector_dropdown:I = 0x7f02003e

.field public static final selector_dropdown_arrow_btn:I = 0x7f02003f

.field public static final selector_dropdown_text:I = 0x7f020040

.field public static final selector_playlist_1st_row:I = 0x7f020041

.field public static final selector_playlist_2nd_row:I = 0x7f020042

.field public static final setmode_btn_toggle_bg:I = 0x7f020043

.field public static final share_video_help:I = 0x7f020044

.field public static final share_video_icon_list:I = 0x7f020045

.field public static final share_video_icon_list_press:I = 0x7f020046

.field public static final share_video_list_footer_bg:I = 0x7f020047

.field public static final switch_toggle_button_02:I = 0x7f020048

.field public static final thumbnail_progress:I = 0x7f020049

.field public static final thumbnail_progress_bar:I = 0x7f02004a

.field public static final thumbnail_progress_bg:I = 0x7f02004b

.field public static final tw_ab_solid_shadow_holo_dark:I = 0x7f02004c

.field public static final tw_ab_spinner_list_focused_holo_light:I = 0x7f02004d

.field public static final tw_ab_spinner_list_pressed_holo_light:I = 0x7f02004e

.field public static final tw_ab_transparent_holo:I = 0x7f02004f

.field public static final tw_action_bar_icon_setting_holo_dark:I = 0x7f020050

.field public static final tw_action_item_background:I = 0x7f020051

.field public static final tw_action_item_background_focused_holo_dark:I = 0x7f020052

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f020053

.field public static final tw_action_item_background_selected_holo_dark:I = 0x7f020054

.field public static final tw_btn_check_off_holo_dark:I = 0x7f020055

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f020056

.field public static final tw_btn_check_on_holo_dark:I = 0x7f020057

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f020058

.field public static final tw_cab_background_top_holo_dark:I = 0x7f020059

.field public static final tw_dialog_full_holo_dark:I = 0x7f02005a

.field public static final tw_dialog_title_holo_dark:I = 0x7f02005b

.field public static final tw_divider_ab_holo:I = 0x7f02005c

.field public static final tw_ic_ab_back_holo_light:I = 0x7f02005d

.field public static final tw_list_section_divider_focused_holo_dark:I = 0x7f02005e

.field public static final tw_list_section_divider_pressed_holo_dark:I = 0x7f02005f

.field public static final tw_menu_ab_dropdown_panel_holo_dark:I = 0x7f020060

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f020061

.field public static final tw_preference_list_section_divider_holo_light:I = 0x7f020062

.field public static final tw_seekbar_ani_thumb_holo_light_00:I = 0x7f020063

.field public static final tw_seekbar_ani_thumb_holo_light_01:I = 0x7f020064

.field public static final tw_seekbar_ani_thumb_holo_light_02:I = 0x7f020065

.field public static final tw_seekbar_ani_thumb_holo_light_03:I = 0x7f020066

.field public static final tw_seekbar_ani_thumb_holo_light_04:I = 0x7f020067

.field public static final tw_seekbar_ani_thumb_holo_light_05:I = 0x7f020068

.field public static final tw_seekbar_ani_thumb_holo_light_06:I = 0x7f020069

.field public static final tw_seekbar_ani_thumb_holo_light_07:I = 0x7f02006a

.field public static final tw_spinner_ab_default_holo:I = 0x7f02006b

.field public static final tw_spinner_ab_disabled_holo:I = 0x7f02006c

.field public static final tw_spinner_ab_focused_holo:I = 0x7f02006d

.field public static final tw_spinner_ab_pressed_holo:I = 0x7f02006e

.field public static final tw_spinner_ab_selected_holo:I = 0x7f02006f

.field public static final tw_sub_action_bar_bg_holo_dark:I = 0x7f020070

.field public static final video:I = 0x7f020071

.field public static final video_brightness_tw_seek_thumb:I = 0x7f020072

.field public static final video_btn_list_dim:I = 0x7f020073

.field public static final video_btn_list_focus:I = 0x7f020074

.field public static final video_btn_list_normal:I = 0x7f020075

.field public static final video_btn_list_on:I = 0x7f020076

.field public static final video_btn_list_press:I = 0x7f020077

.field public static final video_btn_multivison:I = 0x7f020078

.field public static final video_btn_multivison_pressed:I = 0x7f020079

.field public static final video_btn_multivison_selected:I = 0x7f02007a

.field public static final video_btn_playing:I = 0x7f02007b

.field public static final video_btn_playing_pressed:I = 0x7f02007c

.field public static final video_btn_playing_selected:I = 0x7f02007d

.field public static final video_btn_press:I = 0x7f02007e

.field public static final video_control_ff_f:I = 0x7f02007f

.field public static final video_control_ff_n:I = 0x7f020080

.field public static final video_control_ff_p:I = 0x7f020081

.field public static final video_control_next_f:I = 0x7f020082

.field public static final video_control_next_n:I = 0x7f020083

.field public static final video_control_next_p:I = 0x7f020084

.field public static final video_control_pause_f:I = 0x7f020085

.field public static final video_control_pause_n:I = 0x7f020086

.field public static final video_control_pause_p:I = 0x7f020087

.field public static final video_control_play_f:I = 0x7f020088

.field public static final video_control_play_n:I = 0x7f020089

.field public static final video_control_play_p:I = 0x7f02008a

.field public static final video_control_prev_f:I = 0x7f02008b

.field public static final video_control_prev_n:I = 0x7f02008c

.field public static final video_control_prev_p:I = 0x7f02008d

.field public static final video_control_rew_f:I = 0x7f02008e

.field public static final video_control_rew_n:I = 0x7f02008f

.field public static final video_control_rew_p:I = 0x7f020090

.field public static final video_control_stop_f:I = 0x7f020091

.field public static final video_control_stop_n:I = 0x7f020092

.field public static final video_control_stop_p:I = 0x7f020093

.field public static final video_default_thumbnail_image_vertical:I = 0x7f020094

.field public static final video_gesture_controller_bg:I = 0x7f020095

.field public static final video_gesture_icon_brightness:I = 0x7f020096

.field public static final video_gesture_icon_brightness_empty:I = 0x7f020097

.field public static final video_gesture_icon_mute:I = 0x7f020098

.field public static final video_gesture_icon_volume:I = 0x7f020099

.field public static final video_gesture_progress_controller_f:I = 0x7f02009a

.field public static final video_gesture_progress_controller_n:I = 0x7f02009b

.field public static final video_gesture_progress_controller_n2:I = 0x7f02009c

.field public static final video_list_thumb_bg:I = 0x7f02009d

.field public static final video_no_video:I = 0x7f02009e

.field public static final video_player_arrow_left_01:I = 0x7f02009f

.field public static final video_player_arrow_left_02:I = 0x7f0200a0

.field public static final video_player_arrow_right_01:I = 0x7f0200a1

.field public static final video_player_arrow_right_02:I = 0x7f0200a2

.field public static final video_player_background:I = 0x7f0200a3

.field public static final video_player_control_vol_dn_icon:I = 0x7f0200a4

.field public static final video_player_control_vol_dn_icon_d:I = 0x7f0200a5

.field public static final video_player_control_vol_dn_icon_f:I = 0x7f0200a6

.field public static final video_player_control_vol_dn_icon_p:I = 0x7f0200a7

.field public static final video_player_control_vol_mute_icon:I = 0x7f0200a8

.field public static final video_player_control_vol_mute_icon_d:I = 0x7f0200a9

.field public static final video_player_control_vol_mute_icon_f:I = 0x7f0200aa

.field public static final video_player_control_vol_mute_icon_p:I = 0x7f0200ab

.field public static final video_player_control_vol_up_icon:I = 0x7f0200ac

.field public static final video_player_control_vol_up_icon_d:I = 0x7f0200ad

.field public static final video_player_control_vol_up_icon_f:I = 0x7f0200ae

.field public static final video_player_control_vol_up_icon_p:I = 0x7f0200af

.field public static final video_player_icon_rotate:I = 0x7f0200b0

.field public static final video_player_icon_rotate_f:I = 0x7f0200b1

.field public static final video_player_icon_rotate_p:I = 0x7f0200b2

.field public static final video_popup_icon_mute_n:I = 0x7f0200b3

.field public static final video_popup_icon_no_lightness_00_n:I = 0x7f0200b4

.field public static final video_popup_icon_no_lightness_01_n:I = 0x7f0200b5

.field public static final video_popup_icon_no_lightness_02_n:I = 0x7f0200b6

.field public static final video_popup_icon_no_lightness_03_n:I = 0x7f0200b7

.field public static final video_popup_icon_no_lightness_04_n:I = 0x7f0200b8

.field public static final video_popup_icon_no_lightness_05_n:I = 0x7f0200b9

.field public static final video_popup_icon_no_lightness_06_n:I = 0x7f0200ba

.field public static final video_popup_icon_no_lightness_07_n:I = 0x7f0200bb

.field public static final video_popup_icon_no_lightness_08_n:I = 0x7f0200bc

.field public static final video_popup_icon_no_lightness_09_n:I = 0x7f0200bd

.field public static final video_popup_icon_no_lightness_10_n:I = 0x7f0200be

.field public static final video_popup_icon_volume_n:I = 0x7f0200bf

.field public static final video_popup_sync_minus_f:I = 0x7f0200c0

.field public static final video_popup_sync_minus_n:I = 0x7f0200c1

.field public static final video_popup_sync_minus_p:I = 0x7f0200c2

.field public static final video_popup_sync_plus_f:I = 0x7f0200c3

.field public static final video_popup_sync_plus_n:I = 0x7f0200c4

.field public static final video_popup_sync_plus_p:I = 0x7f0200c5

.field public static final video_progress_bar:I = 0x7f0200c6

.field public static final video_progress_bg:I = 0x7f0200c7

.field public static final video_progress_point:I = 0x7f0200c8

.field public static final video_progress_point_press:I = 0x7f0200c9

.field public static final video_progressive_focus:I = 0x7f0200ca

.field public static final video_progressive_ing:I = 0x7f0200cb

.field public static final video_progressive_press:I = 0x7f0200cc

.field public static final video_recently_played_l:I = 0x7f0200cd

.field public static final video_seek_thumb_pressed_custom:I = 0x7f0200ce

.field public static final video_view_mode_01_f:I = 0x7f0200cf

.field public static final video_view_mode_01_n:I = 0x7f0200d0

.field public static final video_view_mode_01_p:I = 0x7f0200d1

.field public static final video_view_mode_02_f:I = 0x7f0200d2

.field public static final video_view_mode_02_n:I = 0x7f0200d3

.field public static final video_view_mode_02_p:I = 0x7f0200d4

.field public static final video_view_mode_03_f:I = 0x7f0200d5

.field public static final video_view_mode_03_n:I = 0x7f0200d6

.field public static final video_view_mode_03_p:I = 0x7f0200d7

.field public static final video_view_mode_04_f:I = 0x7f0200d8

.field public static final video_view_mode_04_n:I = 0x7f0200d9

.field public static final video_view_mode_04_p:I = 0x7f0200da

.field public static final video_volume_popup_bg:I = 0x7f0200db

.field public static final videoplayer_progressbar:I = 0x7f0200dc

.field public static final videoplayer_seek_thumb:I = 0x7f0200dd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
