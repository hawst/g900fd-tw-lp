.class public Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;
.super Landroid/widget/RelativeLayout;
.source "ShareVideoNumberingView.java"


# instance fields
.field private height_portrait:I

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mContext:Landroid/content/Context;

.field private mNumberLayout:Landroid/widget/RelativeLayout;

.field private mNumberText:Landroid/widget/TextView;

.field private mNumberingView:Landroid/view/View;

.field private margin_from_bottom:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mContext:Landroid/content/Context;

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberText:Landroid/widget/TextView;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberLayout:Landroid/widget/RelativeLayout;

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->margin_from_bottom:I

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->height_portrait:I

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 30
    return-void
.end method

.method private initView(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    .line 33
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mContext:Landroid/content/Context;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->removeAllViews()V

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->makeView()Landroid/view/View;

    move-result-object v0

    .line 38
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->addView(Landroid/view/View;)V

    .line 40
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->setLayout(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->invalidate()V

    .line 42
    return-void
.end method

.method private makeView()Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 55
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030012

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    const v2, 0x7f0d003d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberText:Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    const v2, 0x7f0d003c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberLayout:Landroid/widget/RelativeLayout;

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    return-object v1
.end method

.method private setLayout(Z)V
    .locals 5
    .param p1, "b"    # Z

    .prologue
    const/4 v4, 0x0

    .line 45
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 46
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 47
    const/16 v0, 0x100

    .line 48
    .local v0, "height":I
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 50
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    return-void
.end method


# virtual methods
.method public releaseView()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberingView:Landroid/view/View;

    .line 80
    return-void
.end method

.method public updateSubTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 66
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->mNumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
