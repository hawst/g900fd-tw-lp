.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchPlayer"
.end annotation


# static fields
.field public static final MAX_MULTI_SUBTITLE:I = 0x32


# instance fields
.field private final mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mDuration:J

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mHandler:Landroid/os/Handler;

.field private final mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsInitialized:Z

.field private mIsOutbandSubtitle:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private final mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSubtitleFile:Ljava/lang/String;

.field private mSubtitleTempText:[Ljava/lang/String;

.field private mSubtitleText:Ljava/lang/String;

.field private final mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 958
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 947
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 949
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 950
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 951
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 952
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 954
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;

    .line 955
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;

    .line 1649
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$1;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 1674
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$2;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 1764
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$3;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 1822
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$4;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 1867
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$5;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 1887
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$6;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 1916
    new-instance v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer$7;-><init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    .line 958
    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isLiveStreaming()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 946
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 946
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 946
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 946
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;

    .prologue
    .line 946
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 946
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleText:Ljava/lang/String;

    return-object p1
.end method

.method private isLiveStreaming()Z
    .locals 2

    .prologue
    .line 2232
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    const/16 v1, 0x3ea

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAIAContext(Z)V
    .locals 4
    .param p1, "set"    # Z

    .prologue
    .line 2325
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 2326
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2333
    :cond_0
    :goto_0
    return-void

    .line 2328
    :catch_0
    move-exception v0

    .line 2329
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAIAContext: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2330
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 2331
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAIAContext: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "mode"    # Z

    .prologue
    .line 1020
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnableSoundAlive E. mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1025
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    new-instance v1, Landroid/media/audiofx/SoundAlive;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1502(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1036
    :cond_1
    :goto_0
    return-void

    .line 1030
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1502(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;

    goto :goto_0
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;)V
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1107
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    .line 1108
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle E: "

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    if-eqz v2, :cond_2

    .line 1110
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1112
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1113
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V

    .line 1114
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v2, :cond_0

    .line 1115
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    .line 1118
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    new-instance v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;-><init>([Landroid/media/MediaPlayer$TrackInfo;)V

    iput-object v3, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1119
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 1120
    .local v1, "inbandTrack":Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    if-lez v2, :cond_1

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v2, :cond_1

    .line 1121
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TouchPlayer :: addOutbandSubTitle() track.size() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", getSubtitleLanguageIndex() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setInbandSubtitleType()V

    .line 1124
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 1125
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1127
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    .line 1129
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v3, "application/x-subrip"

    invoke-virtual {v2, p1, v3}, Landroid/media/MediaPlayer;->addTimedTextSourceSEC(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    new-instance v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;-><init>([Landroid/media/MediaPlayer$TrackInfo;Ljava/lang/String;)V

    iput-object v3, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1131
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTrackInfoUtil(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    .line 1132
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 1149
    .end local v1    # "inbandTrack":Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    :cond_2
    :goto_0
    return-void

    .line 1134
    :cond_3
    const-string v2, "MoviePlaybackService"

    const-string v3, "addOutbandSubTitle : MediaPlayer is NOT initialized!!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 1137
    :catch_0
    move-exception v0

    .line 1138
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1139
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1140
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1141
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1142
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1143
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1144
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1039
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: addOutbandSubTitle()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 1042
    iput-boolean p2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 1044
    return-void
.end method

.method public deselectSubtitleTrack()V
    .locals 6

    .prologue
    .line 1207
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_1

    .line 1209
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 1211
    .local v2, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 1212
    aget-object v3, v2, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1213
    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v1}, Landroid/media/MediaPlayer;->deselectTrack(I)V

    .line 1214
    const-string v3, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deselectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1211
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1217
    .end local v1    # "index":I
    .end local v2    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 1218
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v3, "MoviePlaybackService"

    const-string v4, "deselectSubtitleTrack : RuntimeException"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_1
    return-void
.end method

.method public duration()J
    .locals 5

    .prologue
    const/16 v4, 0x3ea

    const-wide/16 v2, 0x0

    .line 1967
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1968
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 1972
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1973
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1974
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x3e9

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1978
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1982
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1600(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 1983
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsPauseEnable(Z)V

    .line 1988
    :goto_0
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_3

    .line 1989
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_3

    .line 1990
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->updatePausePlayBtn()V

    .line 1994
    :cond_3
    iget-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    return-wide v0

    .line 1985
    :cond_4
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsPauseEnable(Z)V

    goto :goto_0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2317
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2318
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2320
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFPS()I
    .locals 4

    .prologue
    .line 2287
    const/4 v0, -0x1

    .line 2289
    .local v0, "fps":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 2290
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5e1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v0

    .line 2293
    :cond_0
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFPS() - fps:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2294
    return v0
.end method

.method public getServerTimestampInfo()[J
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 2143
    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v9, :cond_1

    .line 2145
    :try_start_0
    const-string v6, "android.media.MediaPlayer"

    .line 2149
    .local v6, "sClassName":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 2153
    .local v2, "mediaplayerClass":Ljava/lang/Class;
    const-string v9, "invoke"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    const-class v12, Landroid/os/Parcel;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-class v12, Landroid/os/Parcel;

    aput-object v12, v10, v11

    invoke-virtual {v2, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2155
    .local v1, "invoke_method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 2158
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 2159
    .local v5, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 2160
    .local v4, "reply":Landroid/os/Parcel;
    const-string v9, "android.media.IMediaPlayer"

    invoke-virtual {v5, v9}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2163
    const/16 v9, 0x303f

    invoke-virtual {v5, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 2166
    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-virtual {v1, v9, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2167
    .local v3, "obj":Ljava/lang/Object;
    const-string v9, "MoviePlaybackService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invoke Result : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2169
    const/4 v9, 0x3

    new-array v7, v9, [J

    .line 2170
    .local v7, "timeinfo":[J
    const/4 v9, 0x0

    invoke-virtual {v4}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    aput-wide v10, v7, v9

    .line 2171
    const/4 v9, 0x1

    invoke-virtual {v4}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    aput-wide v10, v7, v9

    .line 2172
    const/4 v9, 0x2

    invoke-virtual {v4}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    aput-wide v10, v7, v9
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2191
    .end local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v2    # "mediaplayerClass":Ljava/lang/Class;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "reply":Landroid/os/Parcel;
    .end local v5    # "request":Landroid/os/Parcel;
    .end local v6    # "sClassName":Ljava/lang/String;
    .end local v7    # "timeinfo":[J
    :goto_0
    return-object v7

    .restart local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .restart local v2    # "mediaplayerClass":Ljava/lang/Class;
    .restart local v6    # "sClassName":Ljava/lang/String;
    :cond_0
    move-object v7, v8

    .line 2176
    goto :goto_0

    .line 2178
    .end local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v2    # "mediaplayerClass":Ljava/lang/Class;
    .end local v6    # "sClassName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2179
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    move-object v7, v8

    .line 2180
    goto :goto_0

    .line 2181
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 2183
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v7, v8

    .line 2184
    goto :goto_0

    .line 2185
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v0

    .line 2187
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v7, v8

    .line 2188
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    move-object v7, v8

    .line 2191
    goto :goto_0
.end method

.method public initSelectSubtitleTrack()V
    .locals 6

    .prologue
    .line 1166
    const-string v4, "MoviePlaybackService"

    const-string v5, "initSelectSubtitleTrack()"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-nez v4, :cond_1

    .line 1187
    :cond_0
    :goto_0
    return-void

    .line 1171
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    move-result-object v3

    .line 1172
    .local v3, "track":Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 1173
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->deselectSubtitleTrack()V

    .line 1174
    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1175
    const-string v4, "MoviePlaybackService"

    const-string v5, "initSelectSubtitleTrack. subtitle language is multiple"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 1177
    sget-object v4, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v4, v4, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    aget v2, v4, v0

    .line 1178
    .local v2, "selectedIndex":I
    iget-object v4, v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    aget v1, v4, v2

    .line 1179
    .local v1, "idx":I
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    .line 1176
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1181
    .end local v1    # "idx":I
    .end local v2    # "selectedIndex":I
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setTracksAndGetLangIndex(Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;)I

    goto :goto_0

    .line 1183
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setTracksAndGetLangIndex(Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;)I

    move-result v0

    .line 1184
    .restart local v0    # "i":I
    iget-object v4, v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->Index:[I

    aget v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    goto :goto_0

    .line 1186
    .end local v0    # "i":I
    :cond_4
    const-string v4, "MoviePlaybackService"

    const-string v5, "initSelectSubtitleTrack : There is NO TIMED TEXT in track!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1047
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: initSubtitle()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 1050
    iput-boolean p2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 1051
    return-void
.end method

.method public initTrackInfoUtil()V
    .locals 4

    .prologue
    .line 1152
    const-string v1, "MoviePlaybackService"

    const-string v2, "initTrackInfoUtil()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    new-instance v2, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;-><init>([Landroid/media/MediaPlayer$TrackInfo;)V

    iput-object v2, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1154
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTrackInfoUtil(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    .line 1156
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    move-result-object v0

    .line 1158
    .local v0, "inbandTrack":Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1159
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 1160
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initInbandSubtitle()V

    .line 1161
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    .line 1163
    :cond_0
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1428
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    return v0
.end method

.method public isMuteDevice()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2213
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1612
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2298
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_HIGH_SPEED_PLAY:Z

    if-eqz v2, :cond_1

    .line 2299
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 2300
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2301
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0x5e2

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->getIntParameter(I)I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 2302
    const-string v1, "MoviePlaybackService"

    const-string v2, "isSECVideo() - true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2313
    :goto_0
    return v0

    .line 2306
    :cond_0
    const-string v0, "MoviePlaybackService"

    const-string v2, "isSECVideo() StreamingType - false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2307
    goto :goto_0

    .line 2312
    :cond_1
    const-string v0, "MoviePlaybackService"

    const-string v2, "isSECVideo() - false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2313
    goto :goto_0
.end method

.method public muteDevice()V
    .locals 2

    .prologue
    .line 2217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 2218
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2219
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 1543
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1545
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1546
    const-string v0, "MoviePlaybackService"

    const-string v1, "MV_SERVER pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1547
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoPause()V

    .line 1550
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-eqz v0, :cond_3

    .line 1551
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1552
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: pause() isWiredHeadsetOn()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1553
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    .line 1554
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCurrentVolume:F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 1557
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1558
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/manager/MotionManager;->unregisterMotionListener()V

    .line 1565
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1566
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer called mediaplayer.pause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 1568
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1571
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1572
    const-string v0, "MoviePlaybackService"

    const-string v1, "pause(), send to PAUSED status to multivision server"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1573
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayerState(I)V

    .line 1581
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 1582
    return-void

    .line 1577
    :cond_4
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer isPlaying() == false, not called mediaplayer.pause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public position()J
    .locals 2

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public realSeek(II)J
    .locals 5
    .param p1, "whereto"    # I
    .param p2, "seekMode"    # I

    .prologue
    const/4 v4, 0x1

    .line 2019
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2021
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2022
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: seek() pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seekMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2023
    if-ne p2, v4, :cond_2

    .line 2024
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoSeekTo(I)V

    .line 2025
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2902(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    .line 2032
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->seekTo(II)V

    .line 2037
    :cond_1
    int-to-long v0, p1

    return-wide v0

    .line 2026
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    .line 2027
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: shareVideoPause call"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2028
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoPause()V

    .line 2029
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isIFrameSeekChecker:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$2902(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1585
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1587
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1592
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1593
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1595
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1596
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 1599
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v0, :cond_1

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 1601
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput-object v3, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1604
    :cond_1
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_2

    .line 1605
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetMediaPlayerOnInfoSetting()V

    .line 1608
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1609
    return-void
.end method

.method public resetPlayer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1620
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer::resetPlayer() - start."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1622
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1624
    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1625
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1627
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 1628
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1633
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1635
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v1, :cond_0

    .line 1636
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 1637
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput-object v4, v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1641
    :cond_0
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_1

    .line 1642
    sget-object v1, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetMediaPlayerOnInfoSetting()V

    .line 1645
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 1646
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1647
    return-void

    .line 1629
    :catch_0
    move-exception v0

    .line 1631
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public resetSubtitle()V
    .locals 3

    .prologue
    .line 1054
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 1056
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 1058
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-nez v1, :cond_1

    .line 1059
    :cond_0
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle() - mMediaplayer is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    :goto_0
    return-void

    .line 1064
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->deselectSubtitleTrack()V

    .line 1065
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    goto :goto_0

    .line 1066
    :catch_0
    move-exception v0

    .line 1067
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "MoviePlaybackService"

    const-string v2, "TouchPlayer :: resetSubtitle() - RuntimeException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public seek(J)J
    .locals 3
    .param p1, "whereto"    # J

    .prologue
    .line 2002
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2004
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MV_SERVER seekTo() whereto : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2005
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoSeekTo(I)V

    .line 2008
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 2009
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: seek() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 2013
    :cond_1
    return-wide p1
.end method

.method public selectSubtitleTrack(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 1190
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 1192
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    .line 1194
    .local v1, "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    array-length v2, v1

    if-ge p1, v2, :cond_1

    if-ltz p1, :cond_1

    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 1195
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : mMediaPlayer.selectTrack : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V

    .line 1204
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_0
    :goto_0
    return-void

    .line 1198
    .restart local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :cond_1
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectSubtitleTrack : Subtitle NOT Selected! : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1200
    .end local v1    # "trackInfo":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 1201
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "MoviePlaybackService"

    const-string v3, "selectSubtitleTrack : RuntimeException"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendMasterClock(JJJJJ)V
    .locals 13
    .param p1, "currentTime"    # J
    .param p3, "clockdelta"    # J
    .param p5, "videoTime"    # J
    .param p7, "currentSecMediaClock"    # J
    .param p9, "currentAudioTimestamp"    # J

    .prologue
    .line 2042
    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v9, :cond_2

    .line 2044
    :try_start_0
    const-string v8, "android.media.MediaPlayer"
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2046
    .local v8, "sClassName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2047
    .local v7, "request":Landroid/os/Parcel;
    const/4 v6, 0x0

    .line 2050
    .local v6, "reply":Landroid/os/Parcel;
    :try_start_1
    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 2054
    .local v4, "mediaplayerClass":Ljava/lang/Class;
    const-string v9, "invoke"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    const-class v12, Landroid/os/Parcel;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-class v12, Landroid/os/Parcel;

    aput-object v12, v10, v11

    invoke-virtual {v4, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 2056
    .local v3, "invoke_method":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 2058
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    .line 2059
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    .line 2060
    const-string v9, "android.media.IMediaPlayer"

    invoke-virtual {v7, v9}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2062
    const/16 v9, 0x303a

    invoke-virtual {v7, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 2064
    invoke-virtual {v7, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 2065
    move-wide/from16 v0, p3

    invoke-virtual {v7, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2066
    move-wide/from16 v0, p5

    invoke-virtual {v7, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2067
    move-wide/from16 v0, p7

    invoke-virtual {v7, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068
    move-wide/from16 v0, p9

    invoke-virtual {v7, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2071
    iget-object v9, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v6, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2072
    .local v5, "obj":Ljava/lang/Object;
    const-string v9, "MoviePlaybackService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invoke Result : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2081
    .end local v5    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz v7, :cond_1

    .line 2082
    :try_start_2
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 2084
    :cond_1
    if-eqz v6, :cond_2

    .line 2085
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2092
    .end local v3    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v4    # "mediaplayerClass":Ljava/lang/Class;
    .end local v6    # "reply":Landroid/os/Parcel;
    .end local v7    # "request":Landroid/os/Parcel;
    .end local v8    # "sClassName":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 2074
    .restart local v6    # "reply":Landroid/os/Parcel;
    .restart local v7    # "request":Landroid/os/Parcel;
    .restart local v8    # "sClassName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 2076
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2081
    if-eqz v7, :cond_3

    .line 2082
    :try_start_4
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 2084
    :cond_3
    if-eqz v6, :cond_2

    .line 2085
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 2088
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .end local v6    # "reply":Landroid/os/Parcel;
    .end local v7    # "request":Landroid/os/Parcel;
    .end local v8    # "sClassName":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 2089
    .local v2, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v2}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 2077
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .restart local v6    # "reply":Landroid/os/Parcel;
    .restart local v7    # "request":Landroid/os/Parcel;
    .restart local v8    # "sClassName":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 2079
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2081
    if-eqz v7, :cond_4

    .line 2082
    :try_start_6
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 2084
    :cond_4
    if-eqz v6, :cond_2

    .line 2085
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 2081
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    if-eqz v7, :cond_5

    .line 2082
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 2084
    :cond_5
    if-eqz v6, :cond_6

    .line 2085
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    :cond_6
    throw v9
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
.end method

.method public setAudioChannel(I)V
    .locals 11
    .param p1, "audioChannel"    # I

    .prologue
    .line 2095
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "TouchPlayer_setAudioChannel(), audioChannel is"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2097
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v7, :cond_0

    .line 2099
    :try_start_0
    const-string v6, "android.media.MediaPlayer"

    .line 2100
    .local v6, "sClassName":Ljava/lang/String;
    const-string v7, "MoviePlaybackService"

    const-string v8, "TouchPlayer :: setAudioChannel()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2103
    :try_start_1
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 2107
    .local v2, "mediaplayerClass":Ljava/lang/Class;
    const-string v7, "invoke"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    invoke-virtual {v2, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2110
    .local v1, "invoke_method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 2112
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 2115
    .local v5, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 2116
    .local v4, "reply":Landroid/os/Parcel;
    const-string v7, "android.media.IMediaPlayer"

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2118
    const/16 v7, 0x303e

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 2119
    const-string v7, "MoviePlaybackService"

    const-string v8, "setAudioChannel(adj)"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2121
    invoke-virtual {v5, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2124
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2125
    .local v3, "obj":Ljava/lang/Object;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invoke Result : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2140
    .end local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v2    # "mediaplayerClass":Ljava/lang/Class;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "reply":Landroid/os/Parcel;
    .end local v5    # "request":Landroid/os/Parcel;
    .end local v6    # "sClassName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2128
    .restart local v6    # "sClassName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2130
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2136
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    .end local v6    # "sClassName":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 2137
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 2131
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v6    # "sClassName":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 2133
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1323
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1324
    .local v3, "path":Ljava/lang/String;
    const-string v5, "MoviePlaybackService"

    const-string v6, "TouchPlayer :: setDataSource()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isStateViewVisible()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1327
    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showStateView()V

    .line 1330
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v5, :cond_1

    .line 1331
    invoke-direct {p0, v8}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1332
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->reset()V

    .line 1333
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->release()V

    .line 1334
    iput-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1335
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v5, v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v5, :cond_1

    .line 1336
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v5, v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 1337
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput-object v7, v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1342
    :cond_1
    const/4 v0, 0x0

    .line 1344
    .local v0, "dtcp_path":Ljava/lang/String;
    if-eqz v3, :cond_2

    const-string v5, "sshttp://"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v5

    if-eqz v5, :cond_2

    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v5, :cond_2

    .line 1345
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1800(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/mv/player/db/VideoDB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 1346
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v5, "application/x-dtcp1"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1347
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "??"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1353
    .end local v2    # "mimeType":Ljava/lang/String;
    :cond_2
    const-wide/16 v6, 0x0

    :try_start_0
    iput-wide v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 1354
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v6, 0x3e8

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1356
    new-instance v5, Landroid/media/MediaPlayer;

    invoke-direct {v5}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1359
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1360
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 1361
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1362
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1363
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 1364
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 1365
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 1366
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1368
    if-eqz v3, :cond_5

    const-string v5, "content://"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1369
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v5, v6, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1376
    :goto_0
    sget-boolean v5, Lcom/sec/android/app/mv/player/common/feature/Feature;->X_SLSI_JA_CHIP:Z

    if-nez v5, :cond_7

    .line 1381
    :goto_1
    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v5, v5, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    .line 1382
    .local v4, "sh":Landroid/view/SurfaceHolder;
    if-eqz v4, :cond_8

    .line 1383
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v4}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 1389
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1391
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 1393
    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1395
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1396
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v6, 0x645

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 1401
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 1402
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 1425
    .end local v4    # "sh":Landroid/view/SurfaceHolder;
    :cond_4
    :goto_3
    return-void

    .line 1370
    :cond_5
    if-eqz v0, :cond_6

    .line 1371
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 1404
    :catch_0
    move-exception v1

    .line 1405
    .local v1, "ex":Ljava/io/IOException;
    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1406
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException occured  9 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v6, 0x68

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_3

    .line 1373
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_6
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 1409
    :catch_1
    move-exception v1

    .line 1410
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1411
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IllegalArgumentException occured  10 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v5, :cond_4

    .line 1413
    sget-object v5, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_3

    .line 1377
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_7
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/VUtils;->releaseCpuBooster()V

    .line 1378
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/VUtils;->releaseBusBooster()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_1

    .line 1415
    :catch_2
    move-exception v1

    .line 1416
    .local v1, "ex":Ljava/lang/IllegalStateException;
    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1417
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IllegalStateException occured  11 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1385
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .restart local v4    # "sh":Landroid/view/SurfaceHolder;
    :cond_8
    :try_start_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_3

    .line 1420
    .end local v4    # "sh":Landroid/view/SurfaceHolder;
    :catch_3
    move-exception v1

    .line 1421
    .local v1, "ex":Ljava/lang/SecurityException;
    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1422
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SecurityException occured  12 : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1397
    .end local v1    # "ex":Ljava/lang/SecurityException;
    .restart local v4    # "sh":Landroid/view/SurfaceHolder;
    :cond_9
    :try_start_4
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1398
    iget-object v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v6, 0x645

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/media/MediaPlayer;->setParameter(II)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_2
.end method

.method public setDataSourceAsync(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1254
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setDataSourceAsync()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    const-string v2, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1257
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v3, 0x3e8

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1262
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 1263
    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1264
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    .line 1265
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 1266
    iput-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1267
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v2, :cond_0

    .line 1268
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 1269
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput-object v4, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1274
    :cond_0
    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mDuration:J

    .line 1276
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1277
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1278
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 1279
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1280
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1281
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 1282
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 1283
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mOnTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 1284
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2, v3, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1285
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1287
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    .line 1289
    .local v1, "sh":Landroid/view/SurfaceHolder;
    if-eqz v1, :cond_3

    .line 1290
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 1296
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 1298
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 1300
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1302
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1303
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0x645

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 1308
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 1309
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1320
    .end local v1    # "sh":Landroid/view/SurfaceHolder;
    :goto_2
    return-void

    .line 1259
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v3, 0x3e9

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPlayType:I
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1602(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    goto/16 :goto_0

    .line 1292
    .restart local v1    # "sh":Landroid/view/SurfaceHolder;
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 1310
    .end local v1    # "sh":Landroid/view/SurfaceHolder;
    :catch_0
    move-exception v0

    .line 1311
    .local v0, "ex":Ljava/io/IOException;
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1312
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException occured  7 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v3, 0x68

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    goto :goto_2

    .line 1304
    .end local v0    # "ex":Ljava/io/IOException;
    .restart local v1    # "sh":Landroid/view/SurfaceHolder;
    :cond_4
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1305
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0x645

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(II)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1315
    .end local v1    # "sh":Landroid/view/SurfaceHolder;
    :catch_1
    move-exception v0

    .line 1316
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1317
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException occured  8 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public setDataSourcePrepare(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 961
    if-nez p1, :cond_0

    .line 984
    :goto_0
    return-void

    .line 964
    :cond_0
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: setDataSourcePrepare() : start."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 968
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->setDataSource()V

    .line 971
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x82

    # invokes: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$300(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)V

    .line 973
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_2

    .line 974
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetMediaPlayerOnInfoSetting()V

    .line 977
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 979
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 980
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setDataSourceAsync(Landroid/net/Uri;)V

    goto :goto_0

    .line 982
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setDataSource(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1616
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mHandler:Landroid/os/Handler;

    .line 1617
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 5

    .prologue
    .line 1074
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setInbandSubtitle()"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    const/4 v1, 0x0

    .line 1077
    .local v1, "track":Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_1

    .line 1078
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setInbandSubtitle() - mMediaplayer is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    :cond_0
    :goto_0
    return-void

    .line 1083
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->removeOutbandTimedTextSource()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1088
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    .line 1089
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    new-instance v3, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;-><init>([Landroid/media/MediaPlayer$TrackInfo;)V

    iput-object v3, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1090
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTrackInfoUtil(Lcom/sec/android/app/mv/player/util/TrackInfoUtil;)V

    .line 1092
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v2, :cond_2

    .line 1093
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->getTimedTracks()Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 1096
    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    if-lez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v2, :cond_0

    .line 1097
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TouchPlayer :: setInbandSubtitle() track.size() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 1099
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initInbandSubtitle()V

    .line 1100
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 1101
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 1102
    sget-object v2, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v2, v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    goto :goto_0

    .line 1084
    :catch_0
    move-exception v0

    .line 1085
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1
.end method

.method public setMoviePlayerVolume()V
    .locals 2

    .prologue
    .line 2204
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->isMuteDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205
    const-string v0, "MoviePlaybackService"

    const-string v1, "MultiVision Mode! this is mute device don\'t touch the volume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2206
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->muteDevice()V

    .line 2210
    :goto_0
    return-void

    .line 2208
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->unMuteDevice()V

    goto :goto_0
.end method

.method public setSoundAliveMode()V
    .locals 4

    .prologue
    .line 987
    const-string v2, "MoviePlaybackService"

    const-string v3, "TouchPlayer :: setSoundAliveMode E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    const/4 v1, 0x0

    .line 992
    .local v1, "mode":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setEnableSoundAlive(Landroid/media/MediaPlayer;Z)V

    .line 994
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 995
    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1500(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v2

    int-to-short v3, v1

    invoke-virtual {v2, v3}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1017
    :cond_0
    :goto_0
    return-void

    .line 1008
    :catch_0
    move-exception v0

    .line 1009
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1010
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1011
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1012
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 1013
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0

    .line 1014
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_3
    move-exception v0

    .line 1015
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSubtitleLanguage(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1224
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleLanguage() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 4
    .param p1, "synctime"    # I

    .prologue
    .line 1245
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: setSubtitleSyncTime() - synctime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1248
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x5dd

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setParameter(II)Z

    move-result v0

    .line 1249
    .local v0, "result":Z
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TouchPlayer :: setSubtitleSyncTime setParameter : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    .end local v0    # "result":Z
    :cond_0
    return-void
.end method

.method public setTracksAndGetLangIndex(Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;)I
    .locals 4
    .param p1, "track"    # Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;

    .prologue
    const/4 v1, 0x0

    .line 1235
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_0

    .line 1236
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTimedTextTrack(Lcom/sec/android/app/mv/player/util/TrackInfoUtil$Tracks;)V

    .line 1237
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v1

    .line 1241
    :goto_0
    return v1

    .line 1240
    :cond_0
    const-string v2, "MoviePlaybackService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSubtitleLanguage : mSubtitleUtil is NULL!!!"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public setVideoCrop(IIII)V
    .locals 11
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2239
    const-string v6, "android.media.MediaPlayer"

    .line 2240
    .local v6, "sClassName":Ljava/lang/String;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setWindowCrop l:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " t:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " r:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " b:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2242
    if-ltz p1, :cond_1

    if-ltz p2, :cond_1

    if-lez p3, :cond_1

    if-lez p4, :cond_1

    .line 2245
    :try_start_0
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 2249
    .local v2, "mediaplayerClass":Ljava/lang/Class;
    const-string v7, "invoke"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Landroid/os/Parcel;

    aput-object v10, v8, v9

    invoke-virtual {v2, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2251
    .local v1, "invoke_method":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 2253
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    .line 2256
    .local v5, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 2257
    .local v4, "reply":Landroid/os/Parcel;
    const-string v7, "android.media.IMediaPlayer"

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2260
    const/16 v7, 0x3039

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 2261
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setWindowCrop(adj) l:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " t:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " r:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " b:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2263
    invoke-virtual {v5, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2264
    invoke-virtual {v5, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2265
    invoke-virtual {v5, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2266
    invoke-virtual {v5, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 2269
    iget-object v7, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2270
    .local v3, "obj":Ljava/lang/Object;
    const-string v7, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invoke Result : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2284
    .end local v1    # "invoke_method":Ljava/lang/reflect/Method;
    .end local v2    # "mediaplayerClass":Ljava/lang/Class;
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "reply":Landroid/os/Parcel;
    .end local v5    # "request":Landroid/os/Parcel;
    :cond_0
    :goto_0
    return-void

    .line 2274
    :catch_0
    move-exception v0

    .line 2276
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2277
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 2279
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2282
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v7, "MoviePlaybackService"

    const-string v8, "SetWindowCrop ----- IGNORING INVALID PARAM --------"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVolume(F)V
    .locals 3
    .param p1, "vol"    # F

    .prologue
    .line 2197
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2198
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: setVolume() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2199
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2201
    :cond_0
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1432
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: start() : start!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    if-eqz v0, :cond_5

    .line 1435
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setMoviePlayerVolume()V

    .line 1437
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1439
    const/16 v0, 0x32

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleTempText:[Ljava/lang/String;

    .line 1442
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/16 v1, 0x5de

    invoke-virtual {v0, v1, v4}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 1445
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->hasSavedTrackInfo()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1446
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    if-eqz v0, :cond_6

    .line 1447
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: start. mIsOutbandSubtitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 1457
    :goto_0
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setSubtitleSyncTime(I)V

    .line 1458
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 1460
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1461
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/manager/MotionManager;->registerMotionListener()V

    .line 1464
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;Z)Z

    .line 1465
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1467
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1468
    const-string v0, "MoviePlaybackService"

    const-string v1, "start(), send to PLAYED status to multivision server"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayerState(I)V

    .line 1472
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 1478
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1480
    const-string v0, "MoviePlaybackService"

    const-string v1, "MV_SERVER start()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1481
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoStart()V

    .line 1484
    :cond_5
    return-void

    .line 1450
    :cond_6
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: start. mTrackInfoUtil is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initTrackInfoUtil()V

    goto :goto_0

    .line 1454
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1487
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1489
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1491
    const-string v0, "MoviePlaybackService"

    const-string v1, "MV_SERVER stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1492
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->shareVideoStop()V

    .line 1495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1496
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/mv/player/common/manager/MotionManager;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/common/manager/MotionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/manager/MotionManager;->unregisterMotionListener()V

    .line 1503
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_4

    .line 1505
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setAIAContext(Z)V

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 1507
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1508
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1510
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1511
    const-string v0, "MoviePlaybackService"

    const-string v1, "stop(), send to STOPPED status to multivision server"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/SUtils;->sendPlayerState(I)V

    .line 1515
    :cond_2
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1517
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    if-eqz v0, :cond_3

    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/TrackInfoUtil;->clearTracks()V

    .line 1519
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    iput-object v3, v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mTrackInfoUtil:Lcom/sec/android/app/mv/player/util/TrackInfoUtil;

    .line 1522
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 1525
    :cond_4
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsInitialized:Z

    .line 1527
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_5

    .line 1528
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetMediaPlayerOnInfoSetting()V

    .line 1532
    :cond_5
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_6

    .line 1533
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    .line 1534
    sget-object v0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 1537
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z

    .line 1538
    iput-object v3, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->mSubtitleFile:Ljava/lang/String;

    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mBufferPercentage:I
    invoke-static {v0, v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1702(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 1540
    return-void
.end method

.method public unMuteDevice()V
    .locals 2

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2223
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 2225
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mIsMuteDevice:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3002(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2226
    return-void
.end method
