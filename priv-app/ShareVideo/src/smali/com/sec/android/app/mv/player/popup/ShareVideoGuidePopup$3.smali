.class Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;
.super Ljava/lang/Object;
.source "ShareVideoGuidePopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v2

    if-nez v2, :cond_0

    .line 97
    :goto_0
    return v0

    .line 76
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v1

    .line 97
    goto :goto_0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 81
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v4, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 82
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_1

    .line 88
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setPressed(Z)V

    .line 89
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpl-float v0, v4, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;->this$0:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    goto/16 :goto_1

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
