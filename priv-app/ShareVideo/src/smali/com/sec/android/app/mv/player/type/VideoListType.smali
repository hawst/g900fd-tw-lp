.class public Lcom/sec/android/app/mv/player/type/VideoListType;
.super Ljava/lang/Object;
.source "VideoListType.java"


# static fields
.field public static final SHAREVIDEO_LIST_ATTR_NORMAL:I = 0x0

.field public static final SHAREVIDEO_LIST_ATTR_PLAYERLIST:I = 0x1

.field public static final SHAREVIDEO_LIST_TYPE_ADD_LIST:I = 0x2

.field public static final SHAREVIDEO_LIST_TYPE_PICKER:I = 0x0

.field public static final SHAREVIDEO_LIST_TYPE_PLAYLIST:I = 0x1

.field public static final SHAREVIDEO_LIST_TYPE_REMOVE_LIST:I = 0x3

.field public static final SORTBY_DATE:I = 0x1

.field public static final SORTBY_NAME:I = 0x0

.field public static final SORTBY_SIZE:I = 0x2

.field public static final SORTBY_TYPE:I = 0x3

.field public static final VIDEO_DB_COLUMN_ISPLAYED:Ljava/lang/String; = "isPlayed"

.field public static final VIDEO_DB_COLUMN_RESUME_POS:Ljava/lang/String; = "resumePos"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultVideoColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 23
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "isPlayed"

    aput-object v2, v0, v1

    .line 30
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method

.method public static getFolderColumns()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "distinct bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1 as _id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    .line 39
    .local v0, "returnValue":[Ljava/lang/String;
    return-object v0
.end method
