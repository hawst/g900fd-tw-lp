.class public Lcom/sec/android/app/mv/player/view/VideoSurface;
.super Landroid/view/SurfaceView;
.source "VideoSurface.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoSurface"

.field private static mSurfaceCount:I


# instance fields
.field private mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

.field private mContext:Landroid/content/Context;

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSurfaceExists:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 195
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoSurface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoSurface$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->initVideoView()V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/mv/player/view/VideoSurface;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->initVideoView()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 195
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoSurface$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoSurface$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoSurface;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mContext:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->initVideoView()V

    .line 39
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/mv/player/view/VideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoSurface;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 14
    sget v0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$108()I
    .locals 2

    .prologue
    .line 14
    sget v0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$110()I
    .locals 2

    .prologue
    .line 14
    sget v0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/VideoSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoSurface;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceExists:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/mv/player/view/VideoSurface;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoSurface;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceExists:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/VideoSurface;)Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoSurface;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    return-object v0
.end method

.method private initVideoView()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 177
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 179
    .local v0, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 180
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 185
    return-void
.end method


# virtual methods
.method public getSurfaceExists()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceExists:Z

    return v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v11, 0x78

    .line 42
    const/4 v5, 0x1

    .line 43
    .local v5, "surfaceWidth":I
    const/4 v4, 0x1

    .line 45
    .local v4, "surfaceHeight":I
    const/4 v7, 0x0

    .line 46
    .local v7, "videoWidth":I
    const/4 v6, 0x0

    .line 48
    .local v6, "videoHeight":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v8, :cond_0

    .line 49
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v7

    .line 50
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoHeight()I

    move-result v6

    .line 53
    :cond_0
    if-nez v7, :cond_1

    if-nez v6, :cond_1

    .line 54
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - videoWidth is 0 && videoHeight is 0 "

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - caculated size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, v5, v4}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    .line 148
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual {p0, v7, p1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    .line 60
    invoke-virtual {p0, v6, p2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v4

    .line 63
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - param size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " x "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - real size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " x "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - surfaceWidth : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", surfaceHeight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isShareVideoMode()Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isSingleVisionMode()Z

    move-result v8

    if-nez v8, :cond_9

    .line 68
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getMVMembersNumber()I

    move-result v0

    .line 69
    .local v0, "multivisionNumber":I
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - Measure for ShareVideo multivisionNumber : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    if-nez v0, :cond_3

    .line 71
    const/4 v5, 0x1

    .line 72
    const/4 v4, 0x1

    .line 146
    .end local v0    # "multivisionNumber":I
    :cond_2
    :goto_1
    const-string v8, "VideoSurface"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onMeasure() - caculated size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-super {p0, v5, v4}, Landroid/view/SurfaceView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 74
    .restart local v0    # "multivisionNumber":I
    :cond_3
    mul-int v8, v7, v4

    mul-int v9, v6, v5

    if-lt v8, v9, :cond_6

    .line 76
    move v1, v4

    .line 77
    .local v1, "prevSurfaceHeight":I
    if-le v6, v7, :cond_5

    .line 78
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - Please change Landscape Mode"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :goto_2
    if-le v4, v1, :cond_4

    .line 84
    move v4, v1

    .line 86
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getMeasuredHeightRatio()I

    move-result v8

    mul-int/2addr v8, v4

    div-int/lit16 v4, v8, 0x3e8

    .line 87
    goto :goto_1

    .line 80
    :cond_5
    mul-int v8, v5, v6

    div-int/2addr v8, v7

    mul-int v4, v8, v0

    goto :goto_2

    .line 88
    .end local v1    # "prevSurfaceHeight":I
    :cond_6
    move v2, v5

    .line 89
    .local v2, "prevSurfaceWidth":I
    if-le v6, v7, :cond_8

    .line 90
    mul-int v8, v4, v7

    div-int/2addr v8, v6

    mul-int v5, v8, v0

    .line 95
    :goto_3
    if-le v5, v2, :cond_7

    .line 96
    move v5, v2

    .line 98
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getMeasuredHeightRatio()I

    move-result v8

    mul-int/2addr v8, v5

    div-int/lit16 v5, v8, 0x3e8

    goto :goto_1

    .line 92
    :cond_8
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - Please change Portrait Mode"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 102
    .end local v0    # "multivisionNumber":I
    .end local v2    # "prevSurfaceWidth":I
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoSurface;->mApp:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    goto :goto_1

    .line 104
    :pswitch_0
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - VideoServiceUtil.KEEP_ASPECT_RATIO"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    mul-int v8, v7, v4

    mul-int v9, v6, v5

    if-lt v8, v9, :cond_a

    .line 106
    mul-int v8, v5, v6

    div-int v4, v8, v7

    goto/16 :goto_1

    .line 108
    :cond_a
    mul-int v8, v4, v7

    div-int v5, v8, v6

    .line 109
    goto/16 :goto_1

    .line 112
    :pswitch_1
    if-lez v7, :cond_2

    if-lez v6, :cond_2

    .line 113
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - VideoServiceUtil.ORIGINAL_SIZE"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    if-gt v7, v5, :cond_b

    if-le v6, v4, :cond_d

    .line 115
    :cond_b
    mul-int v8, v7, v4

    mul-int v9, v6, v5

    if-lt v8, v9, :cond_c

    .line 116
    mul-int v8, v5, v6

    div-int v4, v8, v7

    goto/16 :goto_1

    .line 118
    :cond_c
    mul-int v8, v4, v7

    div-int v5, v8, v6

    goto/16 :goto_1

    .line 120
    :cond_d
    move v5, v7

    .line 121
    move v4, v6

    goto/16 :goto_1

    .line 127
    :pswitch_2
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - VideoServiceUtil.FIT_TO_HEIGHT_RATIO"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0, v6, p2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v3

    .line 129
    .local v3, "screenHeight":I
    mul-int v8, v3, v7

    div-int v5, v8, v6

    .line 130
    move v4, v3

    .line 131
    goto/16 :goto_1

    .line 134
    .end local v3    # "screenHeight":I
    :pswitch_3
    if-lez v7, :cond_2

    if-lez v6, :cond_2

    .line 135
    const-string v8, "VideoSurface"

    const-string v9, "onMeasure() - VideoServiceUtil.FULL_SCREEN_RATIO"

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0, v7, p1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v5

    .line 137
    invoke-virtual {p0, v6, p2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->resolveAdjustedSize(II)I

    move-result v4

    goto/16 :goto_1

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 151
    move v0, p1

    .line 153
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 154
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 156
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 173
    :goto_0
    return v0

    .line 158
    :sswitch_0
    move v0, p1

    .line 159
    goto :goto_0

    .line 162
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 163
    goto :goto_0

    .line 166
    :sswitch_2
    move v0, v2

    .line 167
    goto :goto_0

    .line 156
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method
