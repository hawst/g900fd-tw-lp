.class Lcom/sec/android/app/mv/player/multivision/MVUtil$1;
.super Ljava/lang/Object;
.source "MVUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/multivision/MVUtil;->waitClientConnectionWithSetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0

    .prologue
    .line 1833
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1836
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1837
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->isClosed()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1839
    :try_start_0
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1845
    :cond_0
    :goto_0
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2702(Ljava/net/ServerSocket;)Ljava/net/ServerSocket;

    .line 1849
    :cond_1
    :try_start_1
    new-instance v5, Ljava/net/ServerSocket;

    sget v6, Lcom/sec/android/app/mv/player/multivision/MVUtil;->PORT:I

    invoke-direct {v5, v6}, Ljava/net/ServerSocket;-><init>(I)V

    # setter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2702(Ljava/net/ServerSocket;)Ljava/net/ServerSocket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1857
    :cond_2
    :try_start_2
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    sget v6, Lcom/sec/android/app/mv/player/multivision/MVUtil;->SOCKET_TIMEOUT:I

    invoke-virtual {v5, v6}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    .line 1858
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v4

    .line 1860
    .local v4, "socket":Ljava/net/Socket;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1861
    invoke-virtual {v4}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    .line 1862
    .local v2, "ip":Ljava/lang/String;
    const-string v5, "MVUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "waitClientConnectionWithSetup connected clientIP : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1864
    new-instance v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # operator++ for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mClientIdMaker:I
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2808(Lcom/sec/android/app/mv/player/multivision/MVUtil;)I

    move-result v5

    invoke-direct {v3, v5, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;-><init>(ILjava/lang/String;)V

    .line 1865
    .local v3, "mvPlayer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mStatusChangedListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2900(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setOnStatusChangedListener(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnStatusChangedListener;)V

    .line 1866
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMessageResponseListener:Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setOnMessageResponseListener(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;)V

    .line 1867
    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setSocket(Ljava/net/Socket;)V

    .line 1868
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mMVSupport:Z
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3100(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setServerInitialInfo(Z)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1877
    .end local v2    # "ip":Ljava/lang/String;
    .end local v3    # "mvPlayer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1878
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup is finished.."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    :try_start_3
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 1889
    .end local v4    # "socket":Ljava/net/Socket;
    :goto_1
    return-void

    .line 1840
    :catch_0
    move-exception v0

    .line 1841
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1850
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1851
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup. ServerSocket is not created..."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1882
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "socket":Ljava/net/Socket;
    :catch_2
    move-exception v1

    .line 1883
    .local v1, "ignore":Ljava/lang/Exception;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1870
    .end local v1    # "ignore":Ljava/lang/Exception;
    .end local v4    # "socket":Ljava/net/Socket;
    :catch_3
    move-exception v5

    .line 1877
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1878
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup is finished.."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    :try_start_4
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_1

    .line 1882
    :catch_4
    move-exception v1

    .line 1883
    .restart local v1    # "ignore":Ljava/lang/Exception;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1872
    .end local v1    # "ignore":Ljava/lang/Exception;
    :catch_5
    move-exception v5

    .line 1877
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1878
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup is finished.."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    :try_start_5
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_1

    .line 1882
    :catch_6
    move-exception v1

    .line 1883
    .restart local v1    # "ignore":Ljava/lang/Exception;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1874
    .end local v1    # "ignore":Ljava/lang/Exception;
    :catch_7
    move-exception v5

    .line 1877
    iget-object v5, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1878
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup is finished.."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    :try_start_6
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8

    goto :goto_1

    .line 1882
    :catch_8
    move-exception v1

    .line 1883
    .restart local v1    # "ignore":Ljava/lang/Exception;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1877
    .end local v1    # "ignore":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$1;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v6}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1878
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup is finished.."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    :try_start_7
    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mServerSocket:Ljava/net/ServerSocket;
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2700()Ljava/net/ServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/ServerSocket;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9

    goto/16 :goto_1

    .line 1882
    :catch_9
    move-exception v1

    .line 1883
    .restart local v1    # "ignore":Ljava/lang/Exception;
    const-string v5, "MVUtil"

    const-string v6, "waitClientConnectionWithSetup Exception"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1885
    .end local v1    # "ignore":Ljava/lang/Exception;
    :cond_4
    throw v5
.end method
