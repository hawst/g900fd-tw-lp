.class Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;
.super Landroid/os/Handler;
.source "MVUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MVUtilHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;Landroid/os/Looper;)V
    .locals 0
    .param p2, "l"    # Landroid/os/Looper;

    .prologue
    .line 1685
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 1686
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1687
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1691
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mShareVideoMode:Z
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x19

    if-eq v2, v3, :cond_0

    .line 1692
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage do not handle message msg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1805
    :goto_0
    return-void

    .line 1696
    :cond_0
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage. msg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v5, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMessage(I)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$100(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 1803
    :pswitch_0
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MVUtilHandler handleMessage : unexpected code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1700
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetDataSource()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$200(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1704
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handlePause()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$300(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1708
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStart()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$400(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1712
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStop()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1716
    :pswitch_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSeekTo(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$600(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto :goto_0

    .line 1720
    :pswitch_6
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetPlayPosition(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$700(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto :goto_0

    .line 1724
    :pswitch_7
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleGetPlayPosition()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$800(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1728
    :pswitch_8
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleGetPlayerState()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$900(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto :goto_0

    .line 1732
    :pswitch_9
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSetVolumeValue(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1000(Lcom/sec/android/app/mv/player/multivision/MVUtil;II)V

    goto :goto_0

    .line 1736
    :pswitch_a
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;

    .line 1737
    .local v1, "requestJoinInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;->getId_1()I

    move-result v3

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;->getId_2()I

    move-result v4

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;->getDirection()I

    move-result v5

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleRequestMVJoinFromTouchMotion(III)V
    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1100(Lcom/sec/android/app/mv/player/multivision/MVUtil;III)V

    goto/16 :goto_0

    .line 1742
    .end local v1    # "requestJoinInfo":Lcom/sec/android/app/mv/player/multivision/MVTouchMotion$RequestJoinInfo;
    :pswitch_b
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleJoinMVGroup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1200(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1746
    :pswitch_c
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleExitMVGroup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1750
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 1751
    .local v0, "player":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleAddClient(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1400(Lcom/sec/android/app/mv/player/multivision/MVUtil;Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    goto/16 :goto_0

    .line 1755
    .end local v0    # "player":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :pswitch_e
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleDeleteClient(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1500(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1759
    :pswitch_f
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStartSharePlay()Z
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    goto/16 :goto_0

    .line 1763
    :pswitch_10
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleStopSharePlay()Z
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1700(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Z

    goto/16 :goto_0

    .line 1767
    :pswitch_11
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToSingleVision(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1800(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1771
    :pswitch_12
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleChangeToMultiVision(Z)V
    invoke-static {v3, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$1900(Lcom/sec/android/app/mv/player/multivision/MVUtil;Z)V

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1775
    :pswitch_13
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleSendMasterClock()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2000(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto/16 :goto_0

    .line 1779
    :pswitch_14
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handlePrepared(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2100(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1783
    :pswitch_15
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleReady(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2200(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1787
    :pswitch_16
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleHide(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1791
    :pswitch_17
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    iget v3, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleShow(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2400(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    goto/16 :goto_0

    .line 1795
    :pswitch_18
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleKeepAlive()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto/16 :goto_0

    .line 1799
    :pswitch_19
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$MVUtilHandler;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->handleRespExpired()V
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$2600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V

    goto/16 :goto_0

    .line 1698
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method
