.class public Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "DeviceVolumeListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DeviceAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 264
    new-instance v0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter$1;-><init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 220
    iput-object p2, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->mContext:Landroid/content/Context;

    .line 221
    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mDeviceList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$400(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 230
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 235
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "index"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "group"    # Landroid/view/ViewGroup;

    .prologue
    .line 240
    sget-object v6, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " ---- getView: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    if-nez p2, :cond_0

    .line 243
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 244
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030003

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 248
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v6, 0x7f0d000a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 249
    .local v1, "deviceVolumeName":Landroid/widget/TextView;
    const v6, 0x7f0d000b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/SeekBar;

    .line 251
    .local v5, "seekBar":Landroid/widget/SeekBar;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # invokes: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->getDeviceName(I)Ljava/lang/String;
    invoke-static {v6, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$500(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "deviceName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # invokes: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->getSpkStrPos(I)Ljava/lang/String;
    invoke-static {v6, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$600(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)Ljava/lang/String;

    move-result-object v4

    .line 253
    .local v4, "pos":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 256
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setTag(Ljava/lang/Object;)V

    .line 257
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->getMediaStreamMaxVolume()I

    move-result v3

    .line 258
    .local v3, "max":I
    const/4 v6, -0x1

    if-eq v3, v6, :cond_1

    invoke-virtual {v5, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 259
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->getVolume(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 261
    return-object p2
.end method
