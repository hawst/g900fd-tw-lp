.class public Lcom/sec/android/app/mv/player/adapter/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;,
        Lcom/sec/android/app/mv/player/adapter/ImageCache$Queue;,
        Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;
    }
.end annotation


# static fields
.field private static final MAX_THREADS:I = 0x8

.field private static TAG:Ljava/lang/String; = null

.field private static final THUMB_ITEM_HEIGHT:I = 0x9b

.field private static final THUMB_ITEM_WIDTH:I = 0xdb

.field private static mContext:Landroid/content/Context;

.field private static mOnUpdatedListener:Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;

.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/adapter/ImageCache;


# instance fields
.field private mBitmapWorker:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mThreadPool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$1;-><init>(Lcom/sec/android/app/mv/player/adapter/ImageCache;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    .line 224
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/ImageCache$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$2;-><init>(Lcom/sec/android/app/mv/player/adapter/ImageCache;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mHandler:Landroid/os/Handler;

    .line 32
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # J

    .prologue
    .line 19
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/adapter/ImageCache;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/adapter/ImageCache;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/adapter/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/adapter/ImageCache;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500()Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mOnUpdatedListener:Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;

    return-object v0
.end method

.method private static formatFileSize(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "number"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x44800000    # 1024.0f

    const/high16 v4, 0x44610000    # 900.0f

    .line 243
    if-nez p0, :cond_0

    .line 244
    const-string v3, ""

    .line 278
    :goto_0
    return-object v3

    .line 247
    :cond_0
    long-to-float v0, p1

    .line 248
    .local v0, "result":F
    const-string v1, "B"

    .line 249
    .local v1, "suffix":Ljava/lang/String;
    cmpl-float v3, v0, v4

    if-lez v3, :cond_1

    .line 250
    const-string v1, "KB"

    .line 251
    div-float/2addr v0, v5

    .line 253
    :cond_1
    cmpl-float v3, v0, v4

    if-lez v3, :cond_2

    .line 254
    const-string v1, "MB"

    .line 255
    div-float/2addr v0, v5

    .line 257
    :cond_2
    cmpl-float v3, v0, v4

    if-lez v3, :cond_3

    .line 258
    const-string v1, "GB"

    .line 259
    div-float/2addr v0, v5

    .line 261
    :cond_3
    cmpl-float v3, v0, v4

    if-lez v3, :cond_4

    .line 262
    const-string v1, "TB"

    .line 263
    div-float/2addr v0, v5

    .line 265
    :cond_4
    cmpl-float v3, v0, v4

    if-lez v3, :cond_5

    .line 266
    const-string v1, "PB"

    .line 267
    div-float/2addr v0, v5

    .line 271
    :cond_5
    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_6

    .line 272
    const-string v3, "%.2f"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 278
    .local v2, "value":Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 273
    .end local v2    # "value":Ljava/lang/String;
    :cond_6
    const/high16 v3, 0x41200000    # 10.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_7

    .line 274
    const-string v3, "%.1f"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1

    .line 276
    .end local v2    # "value":Ljava/lang/String;
    :cond_7
    const-string v3, "%.0f"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_1
.end method

.method public static getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    if-nez p0, :cond_1

    move-object v0, v1

    .line 57
    :cond_0
    :goto_0
    return-object v0

    .line 51
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 52
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 55
    invoke-static {p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache$Queue;->put(Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->getInstance()Lcom/sec/android/app/mv/player/adapter/ImageCache;

    move-result-object v2

    invoke-direct {v2}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->startWorkerThread()V

    move-object v0, v1

    .line 57
    goto :goto_0
.end method

.method private getDefaultBmp()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 201
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02009d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized getInstance()Lcom/sec/android/app/mv/player/adapter/ImageCache;
    .locals 2

    .prologue
    .line 35
    const-class v1, Lcom/sec/android/app/mv/player/adapter/ImageCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mUniqueInstance:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mUniqueInstance:Lcom/sec/android/app/mv/player/adapter/ImageCache;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mUniqueInstance:Lcom/sec/android/app/mv/player/adapter/ImageCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 166
    const/4 v5, -0x1

    .line 167
    .local v5, "time":I
    const-wide/16 v2, -0x1

    .line 168
    .local v2, "duration":J
    const/4 v0, 0x0

    .line 169
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 171
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 173
    const/16 v6, 0x9

    invoke-virtual {v4, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 175
    const-wide/16 v6, 0x3a98

    cmp-long v6, v2, v6

    if-ltz v6, :cond_2

    .line 176
    const v5, 0xe4e1c0

    .line 181
    :cond_0
    :goto_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setVideoReSize(Landroid/media/MediaMetadataRetriever;)V

    .line 183
    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 191
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 194
    :goto_1
    if-nez v0, :cond_1

    .line 195
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->getDefaultBmp()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 197
    :cond_1
    return-object v0

    .line 177
    :cond_2
    const-wide/16 v6, 0x3e8

    cmp-long v6, v2, v6

    if-ltz v6, :cond_0

    .line 178
    const v5, 0xf4240

    goto :goto_0

    .line 184
    :catch_0
    move-exception v1

    .line 185
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 186
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 187
    .local v1, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .line 188
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v6
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 44
    sput-object p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mContext:Landroid/content/Context;

    .line 46
    :cond_0
    return-void
.end method

.method public static setOnUpdatedListener(Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;)V
    .locals 0
    .param p0, "l"    # Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;

    .prologue
    .line 238
    sput-object p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mOnUpdatedListener:Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;

    .line 239
    return-void
.end method

.method private setVideoReSize(Landroid/media/MediaMetadataRetriever;)V
    .locals 6
    .param p1, "retriever"    # Landroid/media/MediaMetadataRetriever;

    .prologue
    const/16 v5, 0xdb

    const/16 v4, 0x9b

    const/4 v3, 0x1

    .line 205
    const/4 v0, -0x1

    .line 206
    .local v0, "height":I
    const/16 v2, 0x12

    invoke-virtual {p1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 207
    .local v1, "width":I
    const/16 v2, 0x13

    invoke-virtual {p1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 209
    if-lt v5, v1, :cond_0

    if-ge v4, v0, :cond_2

    .line 210
    :cond_0
    if-le v1, v5, :cond_1

    .line 211
    mul-int/lit16 v2, v0, 0xdb

    div-int v0, v2, v1

    .line 212
    const/16 v1, 0xdb

    .line 215
    :cond_1
    if-le v0, v4, :cond_2

    .line 216
    mul-int/lit16 v2, v1, 0x9b

    div-int v1, v2, v0

    .line 217
    const/16 v0, 0x9b

    .line 221
    :cond_2
    invoke-virtual {p1, v1, v0, v3, v3}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 222
    return-void
.end method

.method private startWorkerThread()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 63
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/adapter/ImageCache;->mBitmapWorker:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 67
    return-void
.end method
