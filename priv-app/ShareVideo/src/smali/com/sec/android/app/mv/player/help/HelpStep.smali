.class public Lcom/sec/android/app/mv/player/help/HelpStep;
.super Ljava/lang/Object;
.source "HelpStep.java"


# static fields
.field public static final HELP_STEP_ADJUST_VOLUME:I = 0x4

.field public static final HELP_STEP_END:I = 0x5

.field public static final HELP_STEP_NONE:I = -0x1

.field public static final HELP_STEP_SELECT_ALL:I = 0x1

.field public static final HELP_STEP_SELECT_OK:I = 0x2

.field public static final HELP_STEP_SWITCH_SCREEN:I = 0x3

.field private static mUniqueInstance:Lcom/sec/android/app/mv/player/help/HelpStep;


# instance fields
.field private mCurrentStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpStep;->mUniqueInstance:Lcom/sec/android/app/mv/player/help/HelpStep;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/mv/player/help/HelpStep;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpStep;->mUniqueInstance:Lcom/sec/android/app/mv/player/help/HelpStep;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpStep;->mUniqueInstance:Lcom/sec/android/app/mv/player/help/HelpStep;

    .line 25
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpStep;->mUniqueInstance:Lcom/sec/android/app/mv/player/help/HelpStep;

    return-object v0
.end method


# virtual methods
.method public getCurrentStep()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/mv/player/help/HelpStep;->mCurrentStep:I

    return v0
.end method

.method public setCurrentStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/mv/player/help/HelpStep;->mCurrentStep:I

    .line 34
    return-void
.end method

.method public setFirstStep()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/help/HelpStep;->mCurrentStep:I

    .line 30
    return-void
.end method
