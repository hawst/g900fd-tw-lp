.class public Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;
.super Landroid/app/Activity;
.source "HelpPlaylistActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;,
        Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;
    }
.end annotation


# static fields
.field private static final CURRENT_STEP:Ljava/lang/String; = "current_step"

.field private static final EXAMPLE_THUMBNAIL:[I

.field static final EXAMPLE_TIME:[Ljava/lang/String;

.field static final EXAMPLE_TITLE:[Ljava/lang/String;

.field private static final ITEM_SIZE:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCheckBoxSelectAll:Landroid/widget/CheckBox;

.field private mExampleAdapter:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

.field private mExampleVideoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;",
            ">;"
        }
    .end annotation
.end field

.field protected mGridView:Landroid/widget/GridView;

.field private mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

.field private mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

.field private mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

.field private mIsPlayerStarted:Z

.field protected mListView:Landroid/widget/ListView;

.field protected mOrientation:I

.field public mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 41
    const-class v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    .line 43
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "A day of railway travel"

    aput-object v1, v0, v3

    const-string v1, "Cool Breeze"

    aput-object v1, v0, v4

    const-string v1, "Delusion"

    aput-object v1, v0, v5

    const-string v1, "Mother earth"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_TITLE:[Ljava/lang/String;

    .line 51
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "00:03:00 / 00:06:00"

    aput-object v1, v0, v3

    const-string v1, "00:03:00 / 00:06:00"

    aput-object v1, v0, v4

    const-string v1, "00:03:00 / 00:06:00"

    aput-object v1, v0, v5

    const-string v1, "00:03:00 / 00:06:00"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_TIME:[Ljava/lang/String;

    .line 58
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_THUMBNAIL:[I

    .line 65
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_TITLE:[Ljava/lang/String;

    array-length v0, v0

    sput v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->ITEM_SIZE:I

    return-void

    .line 58
    :array_0
    .array-data 4
        0x7f020028
        0x7f020029
        0x7f02002a
        0x7f02002b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mIsPlayerStarted:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    .line 422
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;)Lcom/sec/android/app/mv/player/help/HelpStep;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectOk()V

    return-void
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->ITEM_SIZE:I

    return v0
.end method

.method private initActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 205
    .local v0, "action":Landroid/app/ActionBar;
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 206
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 208
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v2, :cond_0

    .line 209
    const v2, 0x7f070060

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setLogo(I)V

    .line 210
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 213
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_1

    .line 214
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 215
    new-instance v2, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-direct {v2, p0, v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    .line 217
    const v2, 0x7f0d0033

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 218
    .local v1, "layout":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getDoneBtn()Landroid/widget/RelativeLayout;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setDoneBtnEnabled(Z)V

    .line 226
    .end local v1    # "layout":Landroid/widget/RelativeLayout;
    :cond_1
    return-void
.end method

.method private initializeView()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 279
    sget-object v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v7, "initializeView()"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleVideoList:Ljava/util/ArrayList;

    .line 282
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->ITEM_SIZE:I

    if-ge v1, v6, :cond_0

    .line 283
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    invoke-direct {v0, v9}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$1;)V

    .line 284
    .local v0, "data":Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;
    sget-object v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v1

    iput-object v6, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->title:Ljava/lang/String;

    .line 285
    sget-object v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_TIME:[Ljava/lang/String;

    aget-object v6, v6, v1

    iput-object v6, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->time:Ljava/lang/String;

    .line 286
    sget-object v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->EXAMPLE_THUMBNAIL:[I

    aget v6, v6, v1

    iput v6, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->thumbNail:I

    .line 287
    iput-boolean v10, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->isChecked:Z

    .line 288
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleVideoList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 291
    .end local v0    # "data":Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->updateOrientation()V

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030010

    invoke-virtual {v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 295
    .local v3, "mPlayListView":Landroid/view/View;
    const v6, 0x7f0d0021

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 296
    .local v2, "layout":Landroid/widget/RelativeLayout;
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    new-instance v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f030014

    iget-object v9, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleVideoList:Ljava/util/ArrayList;

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleAdapter:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    .line 301
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 302
    const v6, 0x7f0d0035

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/GridView;

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    .line 303
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v6, v10}, Landroid/widget/GridView;->setVisibility(I)V

    .line 304
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleAdapter:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 305
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v6, v10}, Landroid/widget/GridView;->setClickable(Z)V

    .line 306
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v6, v10}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setNumberOfColumns()V

    .line 316
    :goto_1
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v6, :cond_3

    .line 317
    const v6, 0x7f0d0031

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    .line 320
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v6, v10}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 321
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v6, :cond_1

    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v6, :cond_1

    .line 322
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v6, v11}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 323
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v6, v11}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 324
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->requestFocus()Z

    .line 326
    :cond_1
    const v6, 0x7f0d0030

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 327
    .local v4, "selectAll":Landroid/view/View;
    new-instance v6, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    .end local v4    # "selectAll":Landroid/view/View;
    :goto_2
    new-instance v6, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v6, p0, p0, v11}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 341
    new-instance v6, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    const/4 v7, 0x2

    invoke-direct {v6, p0, p0, v7}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 342
    return-void

    .line 309
    :cond_2
    const v6, 0x7f0d0034

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mListView:Landroid/widget/ListView;

    .line 310
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 311
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mListView:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleAdapter:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 312
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setClickable(Z)V

    .line 313
    iget-object v6, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, v10}, Landroid/widget/ListView;->setFocusable(Z)V

    goto :goto_1

    .line 336
    :cond_3
    const v6, 0x7f0d002f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 337
    .local v5, "selectAllLayout":Landroid/widget/LinearLayout;
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private setStepSelectAll()V
    .locals 2

    .prologue
    .line 246
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "setStepSelectAll()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->show()V

    .line 250
    return-void
.end method

.method private setStepSelectOk()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 256
    sget-object v1, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v2, "setStepSelectOk()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->show()V

    .line 266
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v1, :cond_2

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 269
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v1, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->ITEM_SIZE:I

    if-ge v0, v1, :cond_1

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleVideoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    iput-boolean v3, v1, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->isChecked:Z

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mExampleAdapter:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->notifyDataSetChanged()V

    .line 275
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->invalidateOptionsMenu()V

    .line 276
    return-void
.end method

.method private declared-synchronized startHelpPlayer()V
    .locals 3

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startHelpActivity() E: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mIsPlayerStarted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mIsPlayerStarted:Z

    if-nez v0, :cond_1

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mIsPlayerStarted:Z

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 237
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->startActivity(Landroid/content/Intent;)V

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :cond_1
    monitor-exit p0

    return-void

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateOrientation()V
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mOrientation:I

    .line 431
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 478
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 502
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :sswitch_0
    return v0

    .line 487
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_1

    .line 489
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 491
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectOk()V

    goto :goto_0

    .line 492
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 493
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->startHelpPlayer()V

    goto :goto_0

    .line 495
    :cond_3
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "invalid step"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 478
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_1
        0x42 -> :sswitch_1
    .end sparse-switch
.end method

.method protected getColumnsCount()I
    .locals 3

    .prologue
    .line 434
    const/4 v0, 0x2

    .line 435
    .local v0, "n":I
    iget v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mOrientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 436
    const/4 v0, 0x3

    .line 439
    :cond_0
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 449
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 450
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mOrientation:I

    if-eq v0, v1, :cond_2

    .line 451
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mOrientation:I

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setNumberOfColumns()V

    .line 454
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 457
    iput-object v4, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 461
    iput-object v4, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 464
    :cond_1
    new-instance v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0, p0, v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 465
    new-instance v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-direct {v0, p0, p0, v3}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;-><init>(Landroid/content/Context;Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 467
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectAll()V

    .line 475
    :cond_2
    :goto_0
    return-void

    .line 468
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 469
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectOk()V

    goto :goto_0

    .line 471
    :cond_4
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "invalid step "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setContentView(I)V

    .line 97
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setRequestedOrientation(I)V

    .line 101
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->initActionBar()V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->initializeView()V

    .line 104
    invoke-static {}, Lcom/sec/android/app/mv/player/help/HelpStep;->getInstance()Lcom/sec/android/app/mv/player/help/HelpStep;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    .line 107
    if-nez p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->setFirstStep()V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    const-string v1, "current_step"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->setCurrentStep(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 167
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onCreateOptionsMenu()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 169
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onOptionsItemSelected()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 198
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 192
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->startHelpPlayer()V

    .line 196
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d00bb
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectAll:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpDialogSelectOk:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 153
    :cond_1
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 173
    sget-object v1, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v2, "onPrepareOptionsMenu()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const v1, 0x7f0d00bb

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 176
    .local v0, "mMenuDone":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 177
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mCheckBoxSelectAll:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 184
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mIsPlayerStarted:Z

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 136
    :goto_0
    return-void

    .line 128
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectAll()V

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->setStepSelectOk()V

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 160
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const-string v0, "current_step"

    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 163
    return-void
.end method

.method public openPlayerDemo()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 507
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->startHelpPlayer()V

    .line 509
    :cond_0
    return-void
.end method

.method public setNumberOfColumns()V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getColumnsCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 446
    :cond_0
    return-void
.end method

.method public updateActionBarBtn()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    if-nez v0, :cond_0

    .line 521
    :goto_0
    return-void

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getDoneBtn()Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setDoneBtnEnabled(Z)V

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setCounterSpinnerText(I)V

    goto :goto_0
.end method
