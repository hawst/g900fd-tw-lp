.class public Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
.super Ljava/lang/Object;
.source "ShareVideoManager.java"


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;


# instance fields
.field private mNodeItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
    .locals 2

    .prologue
    .line 17
    const-class v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mInstance:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mInstance:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    .line 21
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mInstance:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    .locals 6
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 65
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getNodeInfo : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v4, "getNodeInfo mNodeItems is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 79
    :goto_0
    return-object v1

    .line 72
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    .line 73
    .local v1, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    iget-object v3, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNodeInfo node IP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " node Name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " delta Time :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v1    # "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    :cond_2
    move-object v1, v2

    .line 79
    goto :goto_0
.end method


# virtual methods
.method public addNode(Ljava/lang/String;)I
    .locals 6
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 41
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 43
    .local v2, "prevSize":I
    new-instance v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    invoke-direct {v1, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;-><init>(Ljava/lang/String;)V

    .line 44
    .local v1, "nodeInfo":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 47
    .local v0, "afterSize":I
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addNode. getNodeItems nodeInfo"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", previous Node size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", aftter Node size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return v0
.end method

.method public checkNodeTypeChangeToClient(Ljava/lang/String;)Z
    .locals 5
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 194
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v4, "getNodeTypeChangeToClient"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 198
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_1

    .line 199
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v3, "getNodeTypeChangeToClient : info is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_0
    :goto_0
    return v1

    .line 204
    :cond_1
    iget-boolean v3, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isChangeToClient:Z

    if-ne v3, v2, :cond_0

    .line 205
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Node : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is changed from server to client ??: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isChangeToClient:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 206
    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getMVSupportCount()I
    .locals 6

    .prologue
    .line 178
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v4, "getMVSupportCount"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const/4 v0, 0x0

    .line 182
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    .line 184
    .local v2, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    iget-boolean v3, v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isMVSupport:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 185
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MV support node name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " node ip : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    .end local v2    # "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    :cond_1
    return v0
.end method

.method public getNodeIP(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 100
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNodeIP, nodeName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 v1, 0x0

    .line 103
    .local v1, "ip":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 105
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-eqz v0, :cond_0

    .line 106
    sget-object v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "info.nodeIP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v1, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    .line 110
    :cond_0
    return-object v1
.end method

.method public getNodeItems()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v1, "getNodeItems"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRealMasterIP()Ljava/lang/String;
    .locals 8

    .prologue
    .line 159
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v6, "getRealMasterIP"

    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v2, 0x0

    .line 162
    .local v2, "mServerIP":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 164
    .local v4, "max":D
    iget-object v3, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    .line 166
    .local v1, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    iget-wide v6, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    cmpl-double v3, v6, v4

    if-lez v3, :cond_0

    .line 167
    iget-wide v4, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    .line 168
    iget-object v2, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    .line 169
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setElapseTimeFromEnter max : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " max info ip : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    .end local v1    # "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    :cond_1
    sget-object v3, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found real master ServerIP : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-object v2
.end method

.method public initializeShareVideoManager()V
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet;->getNodeList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    .line 32
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v1, "initializeShareVideoManager"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mInstance:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    .line 27
    sget-object v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v1, "release() instance is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public removeNode(Ljava/lang/String;)V
    .locals 3
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 56
    .local v0, "nodeInfo":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_0

    .line 57
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v2, "removeNode nodeInfo is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->mNodeItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setElapseTimeFromEnter(Ljava/lang/String;D)V
    .locals 4
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "delta"    # D

    .prologue
    .line 114
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setElapseTimeFromEnter : nodeName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " elapsed time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 118
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_0

    .line 119
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v2, "setElapseTimeFromEnter : info is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-wide p2, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->deltaTime:D

    goto :goto_0
.end method

.method public setMVSupport(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "bSupport"    # Z

    .prologue
    .line 127
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMVSupport : nodeName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " support MV : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 131
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_0

    .line 132
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v2, "setMVSupport : info is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    iput-boolean p2, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isMVSupport:Z

    goto :goto_0
.end method

.method public setNodeIP(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;

    .prologue
    .line 83
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setNodeIP : nodeName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 91
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_2

    .line 92
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v2, "setNodeIP : info is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    iput-object p2, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->nodeIP:Ljava/lang/String;

    goto :goto_0
.end method

.method public setNodeTypeChangeToClient(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "bSupport"    # Z

    .prologue
    .line 140
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setNodeTypeChangeToClient : nodeName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bSupport : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeInfo(Ljava/lang/String;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    move-result-object v0

    .line 144
    .local v0, "info":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    if-nez v0, :cond_0

    .line 145
    sget-object v1, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->TAG:Ljava/lang/String;

    const-string v2, "setNodeTypeChangeToClient : info is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_0
    iput-boolean p2, v0, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->isChangeToClient:Z

    goto :goto_0
.end method
