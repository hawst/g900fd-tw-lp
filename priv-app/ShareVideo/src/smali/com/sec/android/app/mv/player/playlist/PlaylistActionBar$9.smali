.class Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;
.super Ljava/lang/Object;
.source "PlaylistActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 229
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->openPlayerDemo()V

    .line 235
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateListandCallPlayer()V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar$9;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0
.end method
