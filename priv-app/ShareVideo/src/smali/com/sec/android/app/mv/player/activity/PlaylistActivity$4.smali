.class Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;
.super Ljava/lang/Object;
.source "PlaylistActivity.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->setImageCacheListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdated()V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->notifyDataSetChanged()V

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;->this$0:Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    # getter for: Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "setImageCacheListener() - update UI issued by ImageCache"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_1
    return-void
.end method
