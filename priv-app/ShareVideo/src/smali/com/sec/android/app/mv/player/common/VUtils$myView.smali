.class public final enum Lcom/sec/android/app/mv/player/common/VUtils$myView;
.super Ljava/lang/Enum;
.source "VUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/common/VUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "myView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/mv/player/common/VUtils$myView;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/mv/player/common/VUtils$myView;

.field public static final enum CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

.field public static final enum SUBTITLE_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 677
    new-instance v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;

    const-string v1, "CONTROLLER_VIEW"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/mv/player/common/VUtils$myView;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    .line 678
    new-instance v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;

    const-string v1, "SUBTITLE_VIEW"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/mv/player/common/VUtils$myView;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    .line 676
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/mv/player/common/VUtils$myView;

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/mv/player/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/mv/player/common/VUtils$myView;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;->$VALUES:[Lcom/sec/android/app/mv/player/common/VUtils$myView;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/mv/player/common/VUtils$myView;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 676
    const-class v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/mv/player/common/VUtils$myView;
    .locals 1

    .prologue
    .line 676
    sget-object v0, Lcom/sec/android/app/mv/player/common/VUtils$myView;->$VALUES:[Lcom/sec/android/app/mv/player/common/VUtils$myView;

    invoke-virtual {v0}, [Lcom/sec/android/app/mv/player/common/VUtils$myView;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/mv/player/common/VUtils$myView;

    return-object v0
.end method
