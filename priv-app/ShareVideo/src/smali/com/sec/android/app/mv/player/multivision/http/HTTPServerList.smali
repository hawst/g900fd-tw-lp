.class public Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "HTTPServerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 51
    return-void
.end method


# virtual methods
.method public addReceivedListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->size()I

    move-result v1

    .line 67
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v2

    .line 69
    .local v2, "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->addReceivedListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPReceivedListener;)V

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    .end local v2    # "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public addRequestListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->size()I

    move-result v1

    .line 59
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 60
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v2

    .line 61
    .local v2, "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->addRequestListener(Lcom/sec/android/app/mv/player/multivision/http/IHTTPRequestListener;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    .end local v2    # "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public close()Z
    .locals 5

    .prologue
    .line 82
    const/4 v0, 0x1

    .line 83
    .local v0, "isClosed":Z
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->size()I

    move-result v2

    .line 84
    .local v2, "nServers":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 85
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v3

    .line 86
    .local v3, "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->close()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    .line 84
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    .end local v3    # "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    :cond_1
    return v0
.end method

.method public getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    return-object v0
.end method

.method public open(I)Z
    .locals 2
    .param p1, "port"    # I

    .prologue
    .line 94
    new-instance v0, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;-><init>()V

    .line 95
    .local v0, "httpServer":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->open(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->close()Z

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->clear()V

    .line 98
    const/4 v1, 0x0

    .line 102
    :goto_0
    return v1

    .line 100
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->add(Ljava/lang/Object;)Z

    .line 102
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->size()I

    move-result v1

    .line 111
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 112
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v2

    .line 113
    .local v2, "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->start()Z

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    .end local v2    # "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->size()I

    move-result v1

    .line 119
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 120
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServerList;->getHTTPServer(I)Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;

    move-result-object v2

    .line 121
    .local v2, "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;->stop()Z

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    .end local v2    # "server":Lcom/sec/android/app/mv/player/multivision/http/HTTPServer;
    :cond_0
    return-void
.end method
