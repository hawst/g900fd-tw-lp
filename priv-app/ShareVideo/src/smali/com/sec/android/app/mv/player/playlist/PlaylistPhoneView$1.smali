.class Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;
.super Ljava/lang/Object;
.source "PlaylistPhoneView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->PlaylistListViewInit(Landroid/view/View;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemLongClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    .line 65
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 1
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->notifyDataSetChanged()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->listRefreshForCheck()V

    .line 70
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 1
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPenpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView$1;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;->addOrRemoveSelection(Landroid/view/View;I)V

    .line 62
    return-void
.end method
