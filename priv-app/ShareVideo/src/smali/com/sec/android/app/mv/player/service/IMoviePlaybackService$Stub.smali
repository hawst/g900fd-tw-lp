.class public abstract Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;
.super Landroid/os/Binder;
.source "IMoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.mv.player.service.IMoviePlaybackService"

.field static final TRANSACTION_addOutbandSubTitle:I = 0x15

.field static final TRANSACTION_checkDRMFile:I = 0x14

.field static final TRANSACTION_duration:I = 0x5

.field static final TRANSACTION_getBufferPercentage:I = 0x1

.field static final TRANSACTION_getCurrentFrame:I = 0x1e

.field static final TRANSACTION_getFPS:I = 0x1c

.field static final TRANSACTION_getServerTimestampInfo:I = 0x24

.field static final TRANSACTION_getVideoHeight:I = 0x3

.field static final TRANSACTION_getVideoWidth:I = 0x2

.field static final TRANSACTION_initSelectSubtitleTrack:I = 0x17

.field static final TRANSACTION_initSubtitle:I = 0x21

.field static final TRANSACTION_isInitialized:I = 0x9

.field static final TRANSACTION_isPausedByTransientLossOfFocus:I = 0x1f

.field static final TRANSACTION_isPlaying:I = 0x8

.field static final TRANSACTION_isSecVideo:I = 0x1d

.field static final TRANSACTION_muteDevice:I = 0x11

.field static final TRANSACTION_openPath:I = 0xf

.field static final TRANSACTION_pause:I = 0xb

.field static final TRANSACTION_play:I = 0xc

.field static final TRANSACTION_position:I = 0x4

.field static final TRANSACTION_realSeek:I = 0x7

.field static final TRANSACTION_reset:I = 0xd

.field static final TRANSACTION_resetSubtitle:I = 0x18

.field static final TRANSACTION_resetTrackInfo:I = 0x1b

.field static final TRANSACTION_seek:I = 0x6

.field static final TRANSACTION_sendMasterClock:I = 0x22

.field static final TRANSACTION_setAudioChannel:I = 0x23

.field static final TRANSACTION_setInbandSubtitle:I = 0x16

.field static final TRANSACTION_setKeepAudioFocus:I = 0xe

.field static final TRANSACTION_setSubtitleLanguage:I = 0x1a

.field static final TRANSACTION_setSubtitleSyncTime:I = 0x19

.field static final TRANSACTION_setVideoCrop:I = 0x20

.field static final TRANSACTION_setVolume:I = 0x10

.field static final TRANSACTION_startPlay:I = 0x13

.field static final TRANSACTION_stop:I = 0xa

.field static final TRANSACTION_unMuteDevice:I = 0x12


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 16
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 366
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 42
    :sswitch_0
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    const/4 v2, 0x1

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getBufferPercentage()I

    move-result v14

    .line 49
    .local v14, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    const/4 v2, 0x1

    goto :goto_0

    .line 55
    .end local v14    # "_result":I
    :sswitch_2
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getVideoWidth()I

    move-result v14

    .line 57
    .restart local v14    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 58
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    const/4 v2, 0x1

    goto :goto_0

    .line 63
    .end local v14    # "_result":I
    :sswitch_3
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getVideoHeight()I

    move-result v14

    .line 65
    .restart local v14    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    const/4 v2, 0x1

    goto :goto_0

    .line 71
    .end local v14    # "_result":I
    :sswitch_4
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->position()J

    move-result-wide v14

    .line 73
    .local v14, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Landroid/os/Parcel;->writeLong(J)V

    .line 75
    const/4 v2, 0x1

    goto :goto_0

    .line 79
    .end local v14    # "_result":J
    :sswitch_5
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->duration()J

    move-result-wide v14

    .line 81
    .restart local v14    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Landroid/os/Parcel;->writeLong(J)V

    .line 83
    const/4 v2, 0x1

    goto :goto_0

    .line 87
    .end local v14    # "_result":J
    :sswitch_6
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 90
    .local v4, "_arg0":J
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->seek(J)J

    move-result-wide v14

    .line 91
    .restart local v14    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Landroid/os/Parcel;->writeLong(J)V

    .line 93
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 97
    .end local v4    # "_arg0":J
    .end local v14    # "_result":J
    :sswitch_7
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 101
    .local v4, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 102
    .local v6, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->realSeek(II)J

    move-result-wide v14

    .line 103
    .restart local v14    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 104
    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Landroid/os/Parcel;->writeLong(J)V

    .line 105
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 109
    .end local v4    # "_arg0":I
    .end local v6    # "_arg1":I
    .end local v14    # "_result":J
    :sswitch_8
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->isPlaying()Z

    move-result v14

    .line 111
    .local v14, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    if-eqz v14, :cond_0

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 112
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 117
    .end local v14    # "_result":Z
    :sswitch_9
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->isInitialized()Z

    move-result v14

    .line 119
    .restart local v14    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    if-eqz v14, :cond_1

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 120
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 125
    .end local v14    # "_result":Z
    :sswitch_a
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->stop()V

    .line 127
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 128
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 132
    :sswitch_b
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->pause()V

    .line 134
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 139
    :sswitch_c
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->play()V

    .line 141
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 142
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 146
    :sswitch_d
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->reset()V

    .line 148
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 149
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 153
    :sswitch_e
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x1

    .line 156
    .local v4, "_arg0":Z
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setKeepAudioFocus(Z)V

    .line 157
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 155
    .end local v4    # "_arg0":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 162
    :sswitch_f
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 165
    .local v4, "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->openPath(Ljava/lang/String;)Z

    move-result v14

    .line 166
    .restart local v14    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    if-eqz v14, :cond_3

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 167
    :cond_3
    const/4 v2, 0x0

    goto :goto_4

    .line 172
    .end local v4    # "_arg0":Ljava/lang/String;
    .end local v14    # "_result":Z
    :sswitch_10
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 175
    .local v4, "_arg0":F
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setVolume(F)V

    .line 176
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 181
    .end local v4    # "_arg0":F
    :sswitch_11
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->muteDevice()V

    .line 183
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 188
    :sswitch_12
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->unMuteDevice()V

    .line 190
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 191
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 195
    :sswitch_13
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->startPlay()V

    .line 197
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 198
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 202
    :sswitch_14
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 204
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->checkDRMFile(Ljava/lang/String;)Z

    move-result v14

    .line 206
    .restart local v14    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 207
    if-eqz v14, :cond_4

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 207
    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    .line 212
    .end local v4    # "_arg0":Ljava/lang/String;
    .end local v14    # "_result":Z
    :sswitch_15
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 216
    .restart local v4    # "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5

    const/4 v6, 0x1

    .line 217
    .local v6, "_arg1":Z
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 218
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 219
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 216
    .end local v6    # "_arg1":Z
    :cond_5
    const/4 v6, 0x0

    goto :goto_6

    .line 223
    .end local v4    # "_arg0":Ljava/lang/String;
    :sswitch_16
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setInbandSubtitle()V

    .line 225
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 226
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 230
    :sswitch_17
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->initSelectSubtitleTrack()V

    .line 232
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 233
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 237
    :sswitch_18
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->resetSubtitle()V

    .line 239
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 240
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 244
    :sswitch_19
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 246
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 247
    .local v4, "_arg0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setSubtitleSyncTime(I)V

    .line 248
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 249
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 253
    .end local v4    # "_arg0":I
    :sswitch_1a
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 256
    .restart local v4    # "_arg0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setSubtitleLanguage(I)V

    .line 257
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 258
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 262
    .end local v4    # "_arg0":I
    :sswitch_1b
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->resetTrackInfo()V

    .line 264
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 265
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 269
    :sswitch_1c
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 270
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getFPS()I

    move-result v14

    .line 271
    .local v14, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 272
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeInt(I)V

    .line 273
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 277
    .end local v14    # "_result":I
    :sswitch_1d
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->isSecVideo()Z

    move-result v14

    .line 279
    .local v14, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 280
    if-eqz v14, :cond_6

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 281
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 280
    :cond_6
    const/4 v2, 0x0

    goto :goto_7

    .line 285
    .end local v14    # "_result":Z
    :sswitch_1e
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v14

    .line 287
    .local v14, "_result":Landroid/graphics/Bitmap;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 288
    if-eqz v14, :cond_7

    .line 289
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v14, v0, v2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 295
    :goto_8
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 293
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 299
    .end local v14    # "_result":Landroid/graphics/Bitmap;
    :sswitch_1f
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->isPausedByTransientLossOfFocus()Z

    move-result v14

    .line 301
    .local v14, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 302
    if-eqz v14, :cond_8

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 302
    :cond_8
    const/4 v2, 0x0

    goto :goto_9

    .line 307
    .end local v14    # "_result":Z
    :sswitch_20
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 309
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 311
    .restart local v4    # "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 313
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 315
    .local v8, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 316
    .local v10, "_arg3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6, v8, v10}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setVideoCrop(IIII)V

    .line 317
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 318
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 322
    .end local v4    # "_arg0":I
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    :sswitch_21
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 324
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 326
    .local v4, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9

    const/4 v6, 0x1

    .line 327
    .local v6, "_arg1":Z
    :goto_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->initSubtitle(Ljava/lang/String;Z)V

    .line 328
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 329
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 326
    .end local v6    # "_arg1":Z
    :cond_9
    const/4 v6, 0x0

    goto :goto_a

    .line 333
    .end local v4    # "_arg0":Ljava/lang/String;
    :sswitch_22
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 335
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 337
    .local v4, "_arg0":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 339
    .local v6, "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 341
    .local v8, "_arg2":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    .line 343
    .local v10, "_arg3":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v12

    .local v12, "_arg4":J
    move-object/from16 v3, p0

    .line 344
    invoke-virtual/range {v3 .. v13}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->sendMasterClock(JJJJJ)V

    .line 345
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 346
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 350
    .end local v4    # "_arg0":J
    .end local v6    # "_arg1":J
    .end local v8    # "_arg2":J
    .end local v10    # "_arg3":J
    .end local v12    # "_arg4":J
    :sswitch_23
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 352
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 353
    .local v4, "_arg0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->setAudioChannel(I)V

    .line 354
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 355
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 359
    .end local v4    # "_arg0":I
    :sswitch_24
    const-string v2, "com.sec.android.app.mv.player.service.IMoviePlaybackService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 360
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;->getServerTimestampInfo()[J

    move-result-object v14

    .line 361
    .local v14, "_result":[J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 362
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 363
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
