.class Lcom/sec/android/app/mv/player/activity/ShareVideo$18;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->popupLowBattery(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 2422
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2424
    sparse-switch p2, :sswitch_data_0

    .line 2446
    :cond_0
    :goto_0
    return v2

    .line 2427
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 2428
    const-string v0, "ShareVideo"

    const-string v1, "VideoDetails - KEYCODE_POWER_ACTION_UP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2429
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2430
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3702(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    .line 2432
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 2438
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 2439
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2440
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 2441
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$3702(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z

    goto :goto_0

    .line 2424
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x1a -> :sswitch_0
        0x54 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
