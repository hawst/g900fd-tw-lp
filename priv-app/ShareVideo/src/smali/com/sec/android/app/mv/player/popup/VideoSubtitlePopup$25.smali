.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->makeSubtitleFilesList()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0

    .prologue
    .line 1225
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 5
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 1228
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1229
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFileFilter:Ljava/io/FilenameFilter;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$3200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/io/FilenameFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    .line 1235
    :cond_0
    :goto_0
    const/4 v1, 0x1

    .line 1238
    :goto_1
    return v1

    .line 1230
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".smi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".srt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".sub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1233
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1236
    :catch_0
    move-exception v0

    .line 1237
    .local v0, "ex":Ljava/lang/StackOverflowError;
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "makeSubtitleFilesList :: StackOverflowError ex"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    const/4 v1, 0x0

    goto :goto_1
.end method
