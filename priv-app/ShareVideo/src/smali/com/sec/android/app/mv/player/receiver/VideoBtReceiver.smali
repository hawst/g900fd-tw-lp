.class public Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VideoBtReceiver.java"


# static fields
.field public static VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

.field public static VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

.field public static VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

.field public static VIDEO_MEDIA_UPDATE_VOLUME:Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "VideoBTReceiver.VIDEO_MEDIA_BTN_CMD"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    .line 17
    const-string v0, "stop"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    .line 18
    const-string v0, "play_pause"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    .line 19
    const-string v0, "next"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    .line 20
    const-string v0, "prev"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    .line 21
    const-string v0, "ff_up"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

    .line 22
    const-string v0, "rew_up"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

    .line 23
    const-string v0, "ff_down"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

    .line 24
    const-string v0, "rew_down"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

    .line 25
    const-string v0, "pause"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    .line 26
    const-string v0, "updateVol"

    sput-object v0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_UPDATE_VOLUME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    const-string v0, "VideoBTReceiver"

    iput-object v0, p0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 32
    .local v4, "intentAction":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive() - Action : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v8, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 35
    const-string v8, "android.bluetooth.profile.extra.STATE"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 36
    .local v7, "sinkState":I
    const-string v8, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 38
    .local v6, "previousSinkState":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->TAG:Ljava/lang/String;

    const-string v9, "ACTION_SINK_STATE_CHANGED %d->%d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    if-nez v7, :cond_1

    const/16 v8, 0xa

    if-ne v6, v8, :cond_1

    .line 41
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    .line 43
    .local v1, "command":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 44
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 45
    .local v3, "i":Landroid/content/Intent;
    sget-object v8, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const-string v8, "command"

    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 117
    .end local v1    # "command":Ljava/lang/String;
    .end local v3    # "i":Landroid/content/Intent;
    .end local v6    # "previousSinkState":I
    .end local v7    # "sinkState":I
    :cond_0
    :goto_0
    return-void

    .line 49
    .restart local v6    # "previousSinkState":I
    .restart local v7    # "sinkState":I
    :cond_1
    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 50
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->TAG:Ljava/lang/String;

    const-string v9, "BT headset connected."

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_UPDATE_VOLUME:Ljava/lang/String;

    .line 53
    .restart local v1    # "command":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 54
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 55
    .restart local v3    # "i":Landroid/content/Intent;
    sget-object v8, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v8, "command"

    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 60
    .end local v1    # "command":Ljava/lang/String;
    .end local v3    # "i":Landroid/content/Intent;
    .end local v6    # "previousSinkState":I
    .end local v7    # "sinkState":I
    :cond_3
    const-string v8, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 61
    const-string v8, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/view/KeyEvent;

    .line 63
    .local v2, "event":Landroid/view/KeyEvent;
    if-eqz v2, :cond_0

    .line 65
    const/4 v1, 0x0

    .line 66
    .restart local v1    # "command":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    .line 67
    .local v5, "keycode":I
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 69
    .local v0, "action":I
    iget-object v8, p0, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_MEDIA_BUTTON case - keycode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " action : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const/4 v8, 0x1

    if-ne v0, v8, :cond_5

    .line 72
    packed-switch v5, :pswitch_data_0

    .line 110
    :cond_4
    :goto_1
    :pswitch_0
    if-eqz v1, :cond_0

    .line 111
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 112
    .restart local v3    # "i":Landroid/content/Intent;
    sget-object v8, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v8, "command"

    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 74
    .end local v3    # "i":Landroid/content/Intent;
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    .line 75
    goto :goto_1

    .line 77
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

    .line 78
    goto :goto_1

    .line 80
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

    goto :goto_1

    .line 83
    :cond_5
    if-nez v0, :cond_4

    .line 84
    sparse-switch v5, :sswitch_data_0

    goto :goto_1

    .line 89
    :sswitch_0
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v8

    if-nez v8, :cond_4

    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    goto :goto_1

    .line 93
    :sswitch_1
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    .line 94
    goto :goto_1

    .line 97
    :sswitch_2
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    .line 98
    goto :goto_1

    .line 101
    :sswitch_3
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

    .line 102
    goto :goto_1

    .line 105
    :sswitch_4
    sget-object v1, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

    goto :goto_1

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x56
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x57 -> :sswitch_1
        0x58 -> :sswitch_2
        0x59 -> :sswitch_4
        0x5a -> :sswitch_3
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
    .end sparse-switch
.end method
