.class public Lcom/sec/android/app/mv/player/activity/ShareVideo;
.super Lcom/samsung/groupcast/ExternalActivity;
.source "ShareVideo.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/activity/IVideoApp;
.implements Lcom/sec/android/app/mv/player/multivision/chord/ChordService$IChordServiceListener;
.implements Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;
.implements Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;,
        Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;,
        Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    }
.end annotation


# static fields
.field private static final DRM_AVAILABLE_DISCOUNT:I = 0xc8

.field private static final ELAPSETIME_FROM_ENTERING:I = 0x131

.field private static final ERROR:I = -0x1

.field private static final EXIT_MOVIE_PLAYER:I = 0x3

.field private static final HANDLE_SET_SUBTITLE_SYNC_TIME:I = 0x5

.field protected static final INIT_TIME:I = 0x0

.field private static final INVALID_URI_ERROR:I = 0x0

.field private static final MENU_GROUP:I = 0x0

.field private static final MENU_SETTINGS:I = 0x6b

.field private static final MENU_VIEW_PARTICIPANTS:I = 0x6c

.field private static final MOVIEPLAYER_EXIT_ON_ERRORPOPUP:I = 0x9

.field private static final QUIT:I = 0x2

.field private static final REFRESH:I = 0x1

.field private static final RESET_SUBTITLE_SYNC_TIME:I = 0x0

.field private static final SAVE_SUBTITLE_SYNC_TIME:I = 0x1

.field private static final SERVER_IP:I = 0x12c

.field private static final SERVER_SUBTITLE_ACTIVATED_STATE:I = 0x133

.field private static final SET_CONTROLLER_UPDATE:I = 0x7

.field private static final SET_DNIE_SETTINGS:I = 0x4

.field private static final SHARE_VIDEO_CURRENT_CONTENT_STOP:I = 0x11

.field private static final SHARE_VIDEO_DETERMINE_FIRST_MASTER:I = 0x1a

.field private static final SHARE_VIDEO_HIDE_NUMBER:I = 0x13

.field private static final SHARE_VIDEO_MSG_MAXIMUM_DEVICE_CONNECTED:I = 0x1c

.field private static final SHARE_VIDEO_MULTIVISION_DEVICE_DISCONNECTED:I = 0x1b

.field private static final SHARE_VIDEO_OPEN_PATH:I = 0x10

.field private static final SHARE_VIDEO_SEND_VOLUME_VALUE:I = 0x16

.field private static final SHARE_VIDEO_SET_SCREEN_ROTATION:I = 0x17

.field private static final SHARE_VIDEO_SET_VOLUME_MUTE:I = 0x14

.field private static final SHARE_VIDEO_SET_VOLUME_ON:I = 0x15

.field private static final SHARE_VIDEO_SHOW_NUMBER:I = 0x12

.field private static final SHARE_VIDEO_SHOW_SUBTITLE_MULTI_VISION:I = 0x19

.field private static final SHARE_VIDEO_SHOW_SUBTITLE_SINGLE_VISION:I = 0x18

.field private static final SHOW_MVLIST:I = 0x12e

.field private static final SHOW_SHARE_VIDEO_POPUP:I = 0xa

.field private static final SUBTITLE_EXTENSION:I = 0x130

.field private static final SUPPORT_MULTIVISION:I = 0x132

.field private static final TAG:Ljava/lang/String; = "ShareVideo"

.field private static final TITLE:I = 0x12d

.field private static final VOLUME_UPDATE:I = 0x8

.field public static mHasFocus:Z

.field private static mInitialHeadsetAction:Z

.field public static mVideoOnresumeForDelay:Z


# instance fields
.field private deviceName:Ljava/lang/String;

.field private enteringTime:J

.field private isTransferToDeviceSelected:Z

.field private mAlertmessage:Z

.field private final mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mBattLevel:I

.field private mBattScale:I

.field private mBattStatus:I

.field private mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mBroadcastReceiverExRegistered:Z

.field private final mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

.field public mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

.field private mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

.field private mClientSubtitlePath:Ljava/lang/String;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mDockReceiver:Landroid/content/BroadcastReceiver;

.field private mElapseTime:D

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mErrorType:I

.field private mExit:Z

.field public mExitPlayer:Ljava/lang/Runnable;

.field public mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

.field private mFromLocalVideoPlayer:Z

.field private final mHandler:Landroid/os/Handler;

.field private mItemID:I

.field private mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

.field private mKdrmPopup:Landroid/app/AlertDialog;

.field public mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

.field private mLowBattery:Ljava/lang/Runnable;

.field private mLowBtteryPopup:Landroid/app/AlertDialog;

.field private final mMVPlaylistReceiver:Landroid/content/BroadcastReceiver;

.field private final mMVPlaylistShowInfoReceiver:Landroid/content/BroadcastReceiver;

.field public mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMenu:Landroid/view/Menu;

.field private mMenuVisibilityListener:Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;

.field private final mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private mNetInterface:I

.field public mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

.field private mObserverAutoBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightnessMode:Landroid/database/ContentObserver;

.field private mObserverMWTray:Landroid/database/ContentObserver;

.field private mOldFolderState:I

.field private final mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

.field private mOwnNodeName:Ljava/lang/String;

.field private mPlayerListState:Z

.field public mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

.field private mPopupLowBattShow:Z

.field private mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

.field private mRootLayout:Landroid/widget/RelativeLayout;

.field private mRotationObserver:Landroid/database/ContentObserver;

.field private final mSPenReceiver:Landroid/content/BroadcastReceiver;

.field private mSVPopup:Landroid/app/AlertDialog;

.field private mServerIP:Ljava/lang/String;

.field private mServerTitle:Ljava/lang/String;

.field private mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

.field private final mServiceConnection:Landroid/content/ServiceConnection;

.field public mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

.field private mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

.field private mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

.field private mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

.field private mShowInfo:Z

.field private mShowShareVideoGuidePopup:Z

.field private final mStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mStopped:Z

.field private mSubTitleExtension:Ljava/lang/String;

.field private mSubtitleFile:Ljava/lang/String;

.field public mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

.field public mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

.field public mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

.field private mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

.field public mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

.field private mVideoActivityCreated:Z

.field private mVideoActivityOnResume:Z

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

.field public mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

.field public mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

.field public mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

.field private mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mWifi:Landroid/net/wifi/WifiManager;

.field private mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 257
    sput-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z

    .line 258
    sput-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHasFocus:Z

    .line 259
    sput-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoOnresumeForDelay:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 134
    invoke-direct {p0}, Lcom/samsung/groupcast/ExternalActivity;-><init>()V

    .line 138
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->deviceName:Ljava/lang/String;

    .line 140
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    .line 144
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    .line 146
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    .line 148
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .line 153
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .line 154
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 156
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    .line 159
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 160
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 161
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 162
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 163
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 164
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    .line 165
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 166
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 167
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 168
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 170
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 171
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

    .line 172
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 178
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    .line 181
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 182
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    .line 185
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .line 188
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->enteringTime:J

    .line 189
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mElapseTime:D

    .line 191
    iput v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    .line 224
    iput v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorType:I

    .line 225
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mItemID:I

    .line 229
    iput v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I

    .line 230
    iput v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I

    .line 231
    iput v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I

    .line 246
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityCreated:Z

    .line 248
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mExit:Z

    .line 249
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    .line 250
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    .line 251
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBroadcastReceiverExRegistered:Z

    .line 252
    iput-boolean v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowInfo:Z

    .line 253
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z

    .line 254
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z

    .line 255
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    .line 256
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFromLocalVideoPlayer:Z

    .line 264
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .line 637
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$2;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    .line 1335
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$3;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mDockReceiver:Landroid/content/BroadcastReceiver;

    .line 1343
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$4;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    .line 1533
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$5;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    .line 1571
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    .line 1599
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$7;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistReceiver:Landroid/content/BroadcastReceiver;

    .line 1635
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$8;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistShowInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 1671
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$9;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    .line 2103
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$10;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 2254
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$11;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 2456
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$19;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 2475
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$20;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mExitPlayer:Ljava/lang/Runnable;

    .line 2481
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$21;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBattery:Ljava/lang/Runnable;

    .line 2487
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$22;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 2659
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$26;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 2908
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$27;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 3003
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$30;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 3056
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    .line 3057
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 3058
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 3059
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 3060
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverMWTray:Landroid/database/ContentObserver;

    .line 3417
    new-instance v0, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$36;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/activity/ShareVideo;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->queueNextRefresh(IJ)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/popup/IVideoPopup;)Lcom/sec/android/app/mv/player/popup/IVideoPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/SharedPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showDeviceNumber(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowInfo:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowInfo:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->dismissDeviceNumber()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageDetermineMasterNode()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattScale:I

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->popupLowBattery(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveFontState()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    return-object v0
.end method

.method static synthetic access$3200()Z
    .locals 1

    .prologue
    .line 134
    sget-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z

    return v0
.end method

.method static synthetic access$3202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 134
    sput-boolean p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z

    return p0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isTransferToDeviceSelected:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/mv/player/activity/ShareVideo;IZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorType:I

    return v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/sec/android/app/mv/player/activity/ShareVideo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothA2dp;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifi:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isDialogPopupShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/multivision/chord/ChordService;)Lcom/sec/android/app/mv/player/multivision/chord/ChordService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/ShareVideo;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    return-object p1
.end method

.method private checkLockScreenOn()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2865
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2867
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    .line 2868
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2869
    const-string v1, "ShareVideo"

    const-string v2, "checkLockScreenOn() - isKeyguardLocked TRUE. Lock Screen ON"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2870
    const/4 v1, 0x1

    .line 2874
    :cond_0
    return v1
.end method

.method private createErrorDialog(ILjava/lang/String;)V
    .locals 6
    .param p1, "action"    # I
    .param p2, "uriStr"    # Ljava/lang/String;

    .prologue
    const v5, 0x104000a

    .line 2276
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createErrorDialog(action, intent). action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2278
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 2279
    iput p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorType:I

    .line 2281
    const/4 v1, 0x0

    .line 2283
    .local v1, "resId":I
    sparse-switch p1, :sswitch_data_0

    .line 2394
    :goto_0
    return-void

    .line 2285
    :sswitch_0
    const v1, 0x7f0a006a

    .line 2308
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V

    .line 2310
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    if-eqz v2, :cond_0

    .line 2311
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->dismiss()V

    .line 2312
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .line 2315
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2316
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2317
    const v2, 0x7f0a0017

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2319
    const/16 v2, 0x68

    if-eq p1, v2, :cond_1

    const/16 v2, 0x67

    if-ne p1, v2, :cond_2

    .line 2320
    :cond_1
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$12;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2342
    :goto_2
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$14;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2381
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$15;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2390
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    .line 2391
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2392
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 2393
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    goto :goto_0

    .line 2289
    .end local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :sswitch_1
    const v1, 0x7f0a0068

    .line 2290
    goto :goto_1

    .line 2293
    :sswitch_2
    const v1, 0x7f0a0069

    .line 2294
    goto :goto_1

    .line 2297
    :sswitch_3
    const v1, 0x7f0a0067

    .line 2298
    goto :goto_1

    .line 2301
    :sswitch_4
    const v1, 0x7f0a006a

    .line 2302
    goto :goto_1

    .line 2328
    .restart local v0    # "dialog":Landroid/app/AlertDialog$Builder;
    :cond_2
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$13;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 2283
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x67 -> :sswitch_1
        0x68 -> :sswitch_0
        0x69 -> :sswitch_2
        0x6a -> :sswitch_3
    .end sparse-switch
.end method

.method private dismissDeviceNumber()V
    .locals 2

    .prologue
    .line 3528
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 3529
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->setVisibility(I)V

    .line 3531
    :cond_0
    return-void
.end method

.method private dismissDialogPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2666
    const-string v0, "ShareVideo"

    const-string v1, "dismissDialogPopup E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2668
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2669
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2672
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2673
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2676
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    if-eqz v0, :cond_2

    .line 2677
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismiss()V

    .line 2680
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v0, :cond_3

    .line 2681
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 2684
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    .line 2685
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    .line 2686
    return-void
.end method

.method private getActivityPreferences()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "screen_mode"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setFitToScnMode(I)V

    .line 370
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v4, "subtitle_activation"

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_USA:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_CANADA:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActivation(Z)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "subtitle_size"

    sget v4, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextSizeForPreference(I)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "subtitle_font_color"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextColor(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "subtitle_font_backgroundcolor"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setBackgroundColor(I)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v2, "subtitle_font"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFontNameForPreference(Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v2, "subtitle_font_pakcagename"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFontPackageNameForPreference(Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v2, "subtitle_font_string"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFontStringNameForPreference(Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFont(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v2, "subtitle_path"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFilePath(Ljava/lang/String;)V

    .line 386
    :cond_1
    return-void

    .line 370
    :cond_2
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private getTitleFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2839
    const-string v2, "ShareVideo"

    const-string v3, "getTitleFileName()"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2841
    const/4 v1, 0x0

    .line 2843
    .local v1, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    if-eqz v2, :cond_0

    .line 2844
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 2847
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2848
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 2850
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 2851
    const-string v2, "file://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2852
    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "filePath":Ljava/lang/String;
    :cond_2
    :goto_0
    move-object v2, v1

    .line 2861
    :goto_1
    return-object v2

    .line 2854
    .restart local v0    # "filePath":Ljava/lang/String;
    :cond_3
    move-object v1, v0

    goto :goto_0

    .line 2857
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private init(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 416
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    .line 418
    .local v8, "uri":Landroid/net/Uri;
    const-string v10, "DeviceName"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 419
    .local v3, "deviceName":Ljava/lang/String;
    const-string v10, "ServerTitle"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    .line 420
    const-string v10, "ServerIP"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 422
    .local v7, "serverIP":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 424
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - init : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", current deviceName = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - mServerTitle = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", serverIP = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOnMessageListener:Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/common/SUtils;->setOnMessageListener(Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;)V

    .line 429
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_7

    .line 430
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 431
    .local v0, "MVUri":Ljava/lang/String;
    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    .line 433
    .local v6, "scheme":Ljava/lang/String;
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - MVUri = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", scheme : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v10, "rtsp://multivision"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 436
    const-string v10, "ShareVideo"

    const-string v11, "init() - make ShareVideoUtil for client"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v11, v3, v7}, Lcom/sec/android/app/mv/player/common/SUtils;->setShareVideoData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const/4 v2, 0x0

    .line 440
    .local v2, "currentURL":Ljava/lang/String;
    const/16 v1, 0x32

    .line 442
    .local v1, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_0

    .line 443
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/common/SUtils;->getContentURL()Ljava/lang/String;

    move-result-object v2

    .line 446
    if-eqz v2, :cond_5

    .line 456
    :cond_0
    if-nez v2, :cond_6

    .line 457
    const-string v10, "ShareVideo"

    const-string v11, "init(). contentURL is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 464
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-wide/16 v12, -0x1

    invoke-virtual {v10, v12, v13}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setResumePosition(J)V

    .line 465
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 467
    if-eqz v0, :cond_1

    .line 468
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 503
    .end local v0    # "MVUri":Ljava/lang/String;
    .end local v1    # "count":I
    .end local v2    # "currentURL":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "scheme":Ljava/lang/String;
    :cond_1
    :goto_2
    const/4 v10, 0x0

    invoke-direct {p0, v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveSubtitleSyncTime(I)V

    .line 504
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleSettings()V

    .line 507
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v10, :cond_2

    .line 508
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - getID : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoDbId()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - mResumePosition = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 514
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v10, :cond_3

    .line 515
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 518
    :cond_3
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/common/SUtils;->startSharePlay()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 519
    const-string v10, "ShareVideo"

    const-string v11, "init() - startSharePlay "

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v10, :cond_4

    .line 521
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 528
    :cond_4
    :goto_3
    return-void

    .line 449
    .restart local v0    # "MVUri":Ljava/lang/String;
    .restart local v1    # "count":I
    .restart local v2    # "currentURL":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v6    # "scheme":Ljava/lang/String;
    :cond_5
    const-wide/16 v10, 0x64

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 442
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 451
    :catch_0
    move-exception v4

    .line 452
    .local v4, "e":Ljava/lang/InterruptedException;
    const-string v10, "ShareVideo"

    const-string v11, "init(). InterruptedException."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 460
    .end local v4    # "e":Ljava/lang/InterruptedException;
    :cond_6
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 461
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - final url = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 472
    .end local v0    # "MVUri":Ljava/lang/String;
    .end local v1    # "count":I
    .end local v2    # "currentURL":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "scheme":Ljava/lang/String;
    :cond_7
    const-string v10, "ShareVideo"

    const-string v11, "init() - make ShareVideoUtil for server"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v3, v12}, Lcom/sec/android/app/mv/player/common/SUtils;->setShareVideoData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/4 v9, 0x0

    .line 479
    .local v9, "videoUri":Landroid/net/Uri;
    :try_start_1
    const-string v10, "uri"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v9

    .line 486
    :goto_5
    if-nez v9, :cond_8

    .line 487
    const-string v10, "ShareVideo"

    const-string v11, "init() - uri is invalid."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->createErrorDialog(ILjava/lang/String;)V

    goto :goto_3

    .line 480
    :catch_1
    move-exception v4

    .line 481
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_5

    .line 482
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v4

    .line 483
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 492
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 493
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v10, v9}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    .line 494
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v11, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-virtual {v11, v9}, Lcom/sec/android/app/mv/player/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setResumePosition(J)V

    .line 496
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v10, :cond_9

    .line 497
    iget-object v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v11, "showsharevideopopup"

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z

    .line 500
    :cond_9
    const-string v10, "ShareVideo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "init() - fileUri = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 525
    .end local v9    # "videoUri":Landroid/net/Uri;
    :cond_a
    const-string v10, "ShareVideo"

    const-string v11, "init() - startSharePlay failed.."

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method private initActionBarView()V
    .locals 2

    .prologue
    .line 604
    const-string v0, "ShareVideo"

    const-string v1, "initActionBarView"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->removeAllViews()V

    .line 609
    :cond_0
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 610
    return-void
.end method

.method private initMainView()V
    .locals 9

    .prologue
    const/4 v3, -0x1

    const/16 v2, 0x8

    .line 531
    const-string v0, "ShareVideo"

    const-string v1, "initMainView"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    const/4 v7, 0x0

    .line 535
    .local v7, "attr":Landroid/util/AttributeSet;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllInController()V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllViews()V

    .line 540
    :cond_0
    new-instance v0, Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 542
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 544
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoStateView;

    const v1, 0x101007a

    invoke-direct {v0, p0, v7, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 546
    new-instance v0, Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/SubtitleView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/SubtitleView;->setVisibility(I)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleView(Lcom/sec/android/app/mv/player/view/SubtitleView;)V

    .line 549
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 550
    new-instance v0, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->setVisibility(I)V

    .line 553
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .line 555
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_2

    .line 556
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    .line 557
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v8, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 558
    .local v8, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 560
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 565
    .end local v8    # "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->addViewTo(Landroid/view/View;)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 572
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 573
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 574
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->addViewTo(Landroid/view/View;)V

    .line 575
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initActionBarView()V

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setRootViewReference(Landroid/widget/RelativeLayout;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setVideoController(Lcom/sec/android/app/mv/player/view/VideoBtnController;Lcom/sec/android/app/mv/player/view/VideoTitleController;Lcom/sec/android/app/mv/player/view/VideoActionBarView;)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFlickView:Lcom/sec/android/app/mv/player/view/VideoFlickView;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setChildViewReferences(Lcom/sec/android/app/mv/player/view/VideoSurface;Lcom/sec/android/app/mv/player/view/VideoStateView;Lcom/sec/android/app/mv/player/view/SubtitleView;Lcom/sec/android/app/mv/player/view/VideoLockCtrl;Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;Lcom/sec/android/app/mv/player/view/VideoFlickView;)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setImportantForAccessibility(I)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 583
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setClientVolume()V

    .line 586
    :cond_1
    return-void

    .line 562
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private initMoviePlayer()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 593
    const-string v0, "ShareVideo"

    const-string v1, "initMoviePlayer"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initMainView()V

    .line 595
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 596
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initPlayerListView(I)V

    .line 598
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 599
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->resetColor()V

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setWindowBackgroundColor()V

    .line 601
    return-void
.end method

.method private initSettingsPopup()V
    .locals 2

    .prologue
    .line 341
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    new-instance v1, Lcom/sec/android/app/mv/player/activity/ShareVideo$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$1;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->setOnPopupCreatedListener(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;)V

    .line 347
    return-void
.end method

.method private isDialogPopupShowing()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2727
    const-string v1, "ShareVideo"

    const-string v2, "isDialogPopupShowing()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2729
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    if-eqz v1, :cond_1

    .line 2730
    :cond_0
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDialogPopupShowing - mPopupLowBattShow:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mAlertmessage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2739
    :goto_0
    return v0

    .line 2734
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->isPopupShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2735
    const-string v1, "ShareVideo"

    const-string v2, "isDialogPopupShowing - mSettingsPopup true"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2739
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pauseOrStopPlaying()V
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseOrStopPlaying()V

    .line 844
    :cond_0
    return-void
.end method

.method private popupLowBattery(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2397
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "popupLowBattery E. mPopupLowBattShow = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    if-nez v1, :cond_0

    .line 2400
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V

    .line 2401
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    .line 2403
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2405
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a0043

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2406
    const v1, 0x7f0a0042

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2408
    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$16;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2415
    new-instance v1, Lcom/sec/android/app/mv/player/activity/ShareVideo$17;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$17;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2422
    new-instance v1, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$18;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2451
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    .line 2452
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2454
    .end local v0    # "popup":Landroid/app/AlertDialog$Builder;
    :cond_0
    return-void
.end method

.method private queueNextRefresh(IJ)V
    .locals 4
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1664
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    if-nez v1, :cond_0

    .line 1665
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1666
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1667
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1669
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private registerBroadcastReciever()V
    .locals 10

    .prologue
    .line 1288
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 1289
    .local v4, "intentFilter1":Landroid/content/IntentFilter;
    const-string v8, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1290
    const-string v8, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1291
    const-string v8, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1293
    const-string v8, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1294
    const-string v8, "android.intent.action.PALM_DOWN"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1295
    const-string v8, "android.intent.action.PALM_UP"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1296
    const-string v8, "android.intent.action.USER_PRESENT"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1297
    const-string v8, "com.samsung.cover.OPEN"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1298
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9, v4}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1300
    new-instance v5, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v5, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1301
    .local v5, "intentFilter2":Landroid/content/IntentFilter;
    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1302
    const-string v8, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1303
    const-string v8, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1304
    const-string v8, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1305
    const-string v8, "file"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1306
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1308
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 1309
    .local v6, "intentFilter3":Landroid/content/IntentFilter;
    sget-object v8, Lcom/sec/android/app/mv/player/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1310
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1313
    new-instance v0, Landroid/content/IntentFilter;

    const-string v8, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1314
    .local v0, "intent6":Landroid/content/IntentFilter;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    const-string v9, "wifi"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    iput-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifi:Landroid/net/wifi/WifiManager;

    .line 1315
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1318
    new-instance v1, Landroid/content/IntentFilter;

    const-string v8, "android.net.wifi.RSSI_CHANGED"

    invoke-direct {v1, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1319
    .local v1, "intent7":Landroid/content/IntentFilter;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1321
    new-instance v2, Landroid/content/IntentFilter;

    const-string v8, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v2, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1322
    .local v2, "intent8":Landroid/content/IntentFilter;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1324
    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    .line 1325
    .local v7, "intentFilterDock":Landroid/content/IntentFilter;
    sget-object v8, Landroid/app/UiModeManager;->ACTION_ENTER_CAR_MODE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1326
    sget-object v8, Landroid/app/UiModeManager;->ACTION_ENTER_DESK_MODE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1327
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mDockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1329
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 1330
    .local v3, "intent9":Landroid/content/IntentFilter;
    sget-object v8, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1331
    sget-object v8, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1332
    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1333
    return-void
.end method

.method private registerContentObserver()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3145
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver E"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3147
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    if-nez v4, :cond_2

    .line 3148
    new-instance v4, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    .line 3164
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3165
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v4, "accelerometer_rotation"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3166
    .local v2, "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3168
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v4, :cond_0

    .line 3169
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3170
    .local v1, "cr2":Landroid/content/ContentResolver;
    const-string v4, "accelerometer_rotation_second"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3171
    .local v3, "tmp2":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3173
    .end local v1    # "cr2":Landroid/content/ContentResolver;
    .end local v3    # "tmp2":Landroid/net/Uri;
    :cond_0
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mRotationObserver is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3179
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-nez v4, :cond_3

    .line 3180
    new-instance v4, Lcom/sec/android/app/mv/player/activity/ShareVideo$32;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo$32;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 3188
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3189
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "screen_brightness_mode"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3190
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3191
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverBrightnessMode is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3196
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    if-nez v4, :cond_4

    .line 3197
    new-instance v4, Lcom/sec/android/app/mv/player/activity/ShareVideo$33;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo$33;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 3205
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3206
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "screen_brightness"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3207
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3208
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverBrightness is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3212
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-nez v4, :cond_5

    .line 3213
    new-instance v4, Lcom/sec/android/app/mv/player/activity/ShareVideo$34;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo$34;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 3221
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3222
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "auto_brightness_detail"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3223
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3224
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverBrightness is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3230
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_3
    sget-boolean v4, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_MULTIWINDOW_TRAY:Z

    if-eqz v4, :cond_1

    .line 3231
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverMWTray:Landroid/database/ContentObserver;

    if-nez v4, :cond_6

    .line 3232
    new-instance v4, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo$35;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverMWTray:Landroid/database/ContentObserver;

    .line 3246
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3247
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "multi_window_expanded"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3248
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverMWTray:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 3249
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverMWTray is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3254
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :cond_1
    :goto_4
    return-void

    .line 3175
    :cond_2
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mRotationObserver already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3193
    :cond_3
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverBrightnessMode already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3210
    :cond_4
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverBrightness already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 3226
    :cond_5
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverAutoBrightness already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 3251
    :cond_6
    const-string v4, "ShareVideo"

    const-string v5, "registerContentObserver - mObserverMWTray already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private releaseMainView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1230
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 1231
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->releaseView()V

    .line 1232
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-eqz v0, :cond_1

    .line 1236
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoStateView;->releaseView()V

    .line 1237
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 1240
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 1241
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->releaseView()V

    .line 1242
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLockCtrlView:Lcom/sec/android/app/mv/player/view/VideoLockCtrl;

    .line 1245
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_3

    .line 1246
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->releaseView()V

    .line 1247
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    .line 1250
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_4

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->releaseView()V

    .line 1252
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    .line 1255
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v0, :cond_5

    .line 1258
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    .line 1261
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    if-eqz v0, :cond_6

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/SubtitleView;->releaseView()V

    .line 1263
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleView:Lcom/sec/android/app/mv/player/view/SubtitleView;

    .line 1266
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    if-eqz v0, :cond_7

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->releaseView()V

    .line 1268
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    .line 1271
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_8

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->releaseView()V

    .line 1273
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    .line 1276
    :cond_8
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_9

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    if-eqz v0, :cond_9

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->releaseView()V

    .line 1279
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    .line 1283
    :cond_9
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1284
    const-string v0, "ShareVideo"

    const-string v1, "releaseMainView() finished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    return-void
.end method

.method private removeAllPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2689
    const-string v0, "ShareVideo"

    const-string v1, "removeAllPopup E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2691
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    if-eqz v0, :cond_0

    .line 2692
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->dismiss()V

    .line 2693
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoGuidePopup:Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .line 2696
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 2697
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2698
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    .line 2701
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 2702
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2703
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBtteryPopup:Landroid/app/AlertDialog;

    .line 2706
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v0, :cond_3

    .line 2707
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 2708
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    .line 2711
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    if-eqz v0, :cond_4

    .line 2712
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismiss()V

    .line 2713
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->removeAllPopup()V

    .line 2714
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .line 2717
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 2718
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2719
    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    .line 2722
    :cond_5
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    .line 2723
    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    .line 2724
    return-void
.end method

.method private removeAnimationEffects()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3376
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->overridePendingTransition(II)V

    .line 3377
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v2, "FLAG_PRIVATE_NO_ANIMATION_WHEN_RESIZE_INCLUDE_CHILD"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Landroid/view/Window;Ljava/lang/String;)V

    .line 3378
    return-void
.end method

.method private requestSystemKeyEvent(IZ)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "request"    # Z

    .prologue
    .line 2897
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 2900
    .local v1, "windowmanager":Landroid/view/IWindowManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2905
    :goto_0
    return v2

    .line 2901
    :catch_0
    move-exception v0

    .line 2902
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2905
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private resetSubtitleIfContentChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1038
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v5, "lastPlayedItem"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v2

    .line 1039
    .local v2, "lastPlayedID":J
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getIndexOf(J)I

    move-result v0

    .line 1041
    .local v0, "lastPlayIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1042
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    iput-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    .line 1044
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v4, "subtitle_path"

    invoke-virtual {v1, v4, v6}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    :cond_0
    return-void
.end method

.method private saveFontState()V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_activation"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_size"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_font_color"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_font_backgroundcolor"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getBackgroundColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_font"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_font_pakcagename"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_font_string"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontStringName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_path"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    return-void
.end method

.method private saveSubtitleSyncTime(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x1

    .line 3398
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveSubtitleSyncTime() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->getID()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 3401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 3404
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v0, :cond_2

    .line 3405
    if-nez p1, :cond_1

    .line 3406
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_synctime"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 3407
    :cond_1
    if-ne p1, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_2

    .line 3408
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "subtitle_synctime"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 3410
    :cond_2
    return-void
.end method

.method private sendMessageDetermineMasterNode()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3632
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-nez v1, :cond_1

    .line 3686
    :cond_0
    :goto_0
    return-void

    .line 3636
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getRealMasterIP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    .line 3637
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageDetermineMasterNode mServerIP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Own IP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3640
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3642
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3643
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->stopSharePlay()Z

    .line 3644
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChord()V

    .line 3645
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unbindChordService()V

    .line 3646
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChordService()V

    .line 3649
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_3

    .line 3650
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 3651
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 3654
    :cond_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3655
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3656
    const v1, 0x7f0a0071

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3657
    const v1, 0x7f0a0060

    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3671
    const v1, 0x7f0a0016

    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$38;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$38;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3678
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    .line 3679
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3680
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 3682
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3683
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0
.end method

.method private sendMessageElapseTime(D)V
    .locals 5
    .param p1, "elapseTime"    # D

    .prologue
    .line 3608
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    .line 3609
    .local v1, "strElapse":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "305%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3610
    .local v0, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v2, :cond_0

    .line 3611
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3612
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageElapseTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3614
    :cond_0
    return-void
.end method

.method private sendMessageMVsupport()V
    .locals 5

    .prologue
    .line 3617
    const-string v2, "ShareVideo"

    const-string v3, "sendMessageMVsupport"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3619
    const/4 v0, 0x0

    .line 3620
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3622
    .local v1, "supportMV":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    .line 3623
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "306%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3625
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 3626
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3627
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageMVsupport : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3629
    :cond_0
    return-void
.end method

.method private sendMessageServerIP()V
    .locals 4

    .prologue
    .line 3592
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v1, :cond_0

    .line 3593
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "300%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3594
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3595
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageServerIP. Server Own IP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3597
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendMessageServerTitle()V
    .locals 4

    .prologue
    .line 3600
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 3601
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "301%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3602
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3603
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageServerTitle. File Name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3605
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendMessageSubTitleExtMessage()V
    .locals 4

    .prologue
    .line 3572
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSubtitlPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubTitleExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubTitleExtension:Ljava/lang/String;

    .line 3573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "304%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubTitleExtension:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3575
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v1, :cond_0

    .line 3576
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3577
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageSubTitleExtMessage. message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3579
    :cond_0
    return-void
.end method

.method private sendTitleToClient(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 3534
    const-string v1, "ShareVideo"

    const-string v2, "sendTitleToClient E."

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3536
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3537
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "301%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3539
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v1, :cond_0

    .line 3540
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3541
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendTitleToClient. message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3544
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setClientVolume()V
    .locals 1

    .prologue
    .line 3508
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateVolume()V

    .line 3509
    return-void
.end method

.method private showDeviceNumber(I)V
    .locals 6
    .param p1, "mDeviceNumber"    # I

    .prologue
    const/16 v3, 0x13

    .line 3513
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 3515
    .local v0, "number":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->setVisibility(I)V

    .line 3516
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNumberingView:Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/view/ShareVideoNumberingView;->updateSubTitle(Ljava/lang/String;)V

    .line 3518
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 3519
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->invalidate()V

    .line 3521
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 3522
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 3524
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showDeviceNumber : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3525
    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 3413
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 3414
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/mv/player/common/VUtils;->showToast(Landroid/content/Context;I)V

    .line 3415
    :cond_0
    return-void
.end method

.method public static sleep(I)V
    .locals 4
    .param p0, "ms"    # I

    .prologue
    .line 2248
    int-to-long v2, p0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2252
    :goto_0
    return-void

    .line 2249
    :catch_0
    move-exception v0

    .line 2250
    .local v0, "ie":Ljava/lang/InterruptedException;
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "sleep interrupted"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private statusBarSetting(Z)V
    .locals 4
    .param p1, "request"    # Z

    .prologue
    .line 2881
    :try_start_0
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 2882
    .local v1, "statusBar":Landroid/app/StatusBarManager;
    if-eqz p1, :cond_0

    .line 2883
    const-string v2, "ShareVideo"

    const-string v3, " statusBarSetting disable statusBar"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2884
    const/high16 v2, 0x1230000

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 2894
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :goto_0
    return-void

    .line 2888
    .restart local v1    # "statusBar":Landroid/app/StatusBarManager;
    :cond_0
    const-string v2, "ShareVideo"

    const-string v3, " statusBarSetting enable statusBar"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2889
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2891
    .end local v1    # "statusBar":Landroid/app/StatusBarManager;
    :catch_0
    move-exception v0

    .line 2892
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopAppInApp()V
    .locals 3

    .prologue
    .line 1064
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.stop.app-in-app"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1065
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "intent.stop.app-in-app.send_app"

    const-string v2, "VideoPlayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendBroadcast(Landroid/content/Intent;)V

    .line 1067
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3257
    const-string v1, "ShareVideo"

    const-string v2, "unregisterContentObserver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3258
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3260
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 3261
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 3262
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRotationObserver:Landroid/database/ContentObserver;

    .line 3263
    const-string v1, "ShareVideo"

    const-string v2, "unregisterContentObserver - mObserver is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3267
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    .line 3268
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 3269
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 3270
    const-string v1, "ShareVideo"

    const-string v2, "unregisterContentObserver - mObserverBrightnessMode is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3273
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_2

    .line 3274
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 3275
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 3276
    const-string v1, "ShareVideo"

    const-string v2, "unregisterContentObserver - mObserverBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3279
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_3

    .line 3280
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 3281
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 3282
    const-string v1, "ShareVideo"

    const-string v2, "unregisterContentObserver - mObserverAutoBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3285
    :cond_3
    return-void
.end method

.method private updateScreenRotationBtn()V
    .locals 1

    .prologue
    .line 3133
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 3134
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateAutoRotationBtn()V

    .line 3136
    :cond_0
    return-void
.end method

.method private updateSecondScreenRotationBtn()V
    .locals 1

    .prologue
    .line 3139
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_0

    .line 3140
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateSecondScreenAutoRotationBtn()V

    .line 3142
    :cond_0
    return-void
.end method


# virtual methods
.method public bindChordService()V
    .locals 3

    .prologue
    .line 3467
    const-string v1, "ShareVideo"

    const-string v2, "bindChordService()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3468
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-nez v1, :cond_0

    .line 3469
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3470
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 3472
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public changeLockStatus(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 3391
    invoke-static {p1}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->setLockState(Z)V

    .line 3392
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 3393
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatus(Z)V

    .line 3395
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 3503
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    .line 3504
    return-void
.end method

.method public createKDrmPopup(I)V
    .locals 12
    .param p1, "type"    # I

    .prologue
    .line 2502
    const-string v9, "ShareVideo"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createKdrmPopup() type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2504
    const/4 v3, 0x0

    .line 2505
    .local v3, "leftBtn":I
    const/4 v7, 0x0

    .line 2506
    .local v7, "rightBtn":I
    const/4 v1, 0x1

    .line 2507
    .local v1, "btnEnable":Z
    const/4 v5, 0x0

    .line 2508
    .local v5, "popupStr":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2509
    .local v6, "popupTitleStr":Ljava/lang/String;
    move v0, p1

    .line 2511
    .local v0, "PopUptype":I
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getDrmPlaybackStatus()I

    move-result v8

    .line 2512
    .local v8, "status":I
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getAvailableCount()I

    move-result v2

    .line 2514
    .local v2, "cnt":I
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->checkMTPConnected()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2515
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 2657
    :goto_0
    return-void

    .line 2519
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    if-eqz v9, :cond_1

    .line 2520
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->dismiss()V

    .line 2521
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 2524
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 2573
    :goto_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-direct {v4, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2575
    .local v4, "popup":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2576
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2578
    new-instance v9, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$23;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V

    invoke-virtual {v4, v3, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2612
    if-eqz v1, :cond_2

    .line 2613
    new-instance v9, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$24;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;I)V

    invoke-virtual {v4, v7, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2644
    :cond_2
    new-instance v9, Lcom/sec/android/app/mv/player/activity/ShareVideo$25;

    invoke-direct {v9, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$25;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v4, v9}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2654
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 2655
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKdrmPopup:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 2656
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    goto :goto_0

    .line 2526
    .end local v4    # "popup":Landroid/app/AlertDialog$Builder;
    :pswitch_0
    iget-object v9, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    goto :goto_0

    .line 2530
    :pswitch_1
    const v3, 0x104000a

    .line 2531
    const/4 v1, 0x0

    .line 2532
    const-string v6, "DRM Expired"

    .line 2534
    const v9, -0xfffdffa

    if-ne v8, v9, :cond_3

    .line 2535
    const v9, 0x7f0a0026

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 2536
    :cond_3
    const v9, -0xfffdffc

    if-ne v8, v9, :cond_4

    .line 2537
    const v9, 0x7f0a0027

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 2539
    :cond_4
    const v9, 0x7f0a006a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2541
    goto :goto_1

    .line 2545
    :pswitch_2
    const-string v6, "DRM Check"

    .line 2546
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const v10, 0x7f0a002a

    invoke-virtual {p0, v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0a002b

    invoke-virtual {p0, v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0a002c

    invoke-virtual {p0, v10}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2548
    const v3, 0x1040013

    .line 2549
    const v7, 0x1040009

    .line 2550
    const/4 v1, 0x1

    .line 2551
    goto/16 :goto_1

    .line 2556
    :pswitch_3
    const v9, 0x7f0a0028

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2557
    const v3, 0x1040013

    .line 2558
    const v7, 0x1040009

    .line 2559
    const/4 v1, 0x1

    .line 2560
    goto/16 :goto_1

    .line 2563
    :pswitch_4
    const v9, 0x7f0a0029

    invoke-virtual {p0, v9}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2564
    const v3, 0x1040013

    .line 2565
    const v7, 0x1040009

    .line 2566
    const/4 v1, 0x1

    .line 2567
    goto/16 :goto_1

    .line 2524
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public exitApp()V
    .locals 2

    .prologue
    .line 1495
    const-string v0, "ShareVideo"

    const-string v1, "exitApp E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1498
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->stopSharePlay()Z

    .line 1499
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChord()V

    .line 1500
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unbindChordService()V

    .line 1501
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChordService()V

    .line 1504
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v0, :cond_1

    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v0, :cond_1

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->dismissAllHoveringPopup()V

    .line 1510
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityCreated:Z

    if-nez v0, :cond_2

    .line 1511
    const-string v0, "ShareVideo"

    const-string v1, "exitApp. Video Activity not created."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1531
    :goto_0
    return-void

    .line 1515
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    .line 1517
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_3

    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->DeleteClientSubTitleFile()V

    .line 1521
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_4

    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 1523
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 1526
    :cond_4
    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1527
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->changeLockStatus(Z)V

    .line 1530
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->finish()V

    goto :goto_0
.end method

.method public finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1937
    const-string v0, "ShareVideo"

    const-string v1, "finish E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 1940
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 1943
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 1945
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mExit:Z

    if-eqz v0, :cond_1

    .line 1952
    :goto_0
    return-void

    .line 1948
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mExit:Z

    .line 1949
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    .line 1951
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->finish()V

    goto :goto_0
.end method

.method public forceRotateScreen(I)V
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    .line 3091
    move v0, p1

    .line 3093
    .local v0, "mode":I
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    .line 3095
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setScreenOrientation(I)V

    .line 3096
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setRequestedOrientation(I)V

    .line 3100
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->updateScreenRotationBtn()V

    .line 3101
    return-void

    .line 3098
    :cond_1
    const-string v1, "ShareVideo"

    const-string v2, "rotateScreen : invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 3308
    return-object p0
.end method

.method public getAppWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 3288
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public getAudioUtil()Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
    .locals 1

    .prologue
    .line 3312
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    return-object v0
.end method

.method public getKDrmUtil()Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;
    .locals 1

    .prologue
    .line 3316
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    if-nez v0, :cond_0

    .line 3317
    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 3319
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mKDrmUtil:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    return-object v0
.end method

.method public getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;
    .locals 1

    .prologue
    .line 3300
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    return-object v0
.end method

.method public getPlayerListState()Z
    .locals 1

    .prologue
    .line 4009
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    return v0
.end method

.method public getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;
    .locals 1

    .prologue
    .line 3304
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    return-object v0
.end method

.method public getRootLayout()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getServerFileTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3937
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getSettingsSubPopup()Lcom/sec/android/app/mv/player/popup/IVideoPopup;
    .locals 1

    .prologue
    .line 3387
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    return-object v0
.end method

.method public getSurfaceView()Lcom/sec/android/app/mv/player/view/VideoSurface;
    .locals 1

    .prologue
    .line 3296
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    return-object v0
.end method

.method public getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;
    .locals 1

    .prologue
    .line 3292
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

    return-object v0
.end method

.method public getWindowHeight()I
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 767
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getWindowWidth()I
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 762
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public hidePlayerList()V
    .locals 1

    .prologue
    .line 3996
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->hideList()V

    .line 3997
    return-void
.end method

.method public initPlayerListView(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 613
    const-string v0, "ShareVideo"

    const-string v1, "initPlayerListView"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->removeAllViews()V

    .line 619
    :cond_0
    new-instance v0, Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->addViewTo(Landroid/view/View;)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setPlayerList()V

    .line 622
    return-void
.end method

.method public initSubtitle()V
    .locals 4

    .prologue
    .line 2789
    const-string v1, "ShareVideo"

    const-string v2, "initSubtitle"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2791
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-nez v1, :cond_1

    .line 2792
    :cond_0
    const-string v1, "ShareVideo"

    const-string v2, "initSubtitle return :  Null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2821
    :goto_0
    return-void

    .line 2797
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleSettings()V

    .line 2798
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->GetRemoteSubtitleUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2799
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->RemoteSubTitledownloadThreadCheck()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2800
    const-string v1, "ShareVideo"

    const-string v2, "DownloadCompelet!!!!"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2802
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->GetRemoteSubtitlePath()Ljava/lang/String;

    move-result-object v0

    .line 2803
    .local v0, "mRemoteSubtitlePath":Ljava/lang/String;
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRemoteSubtitlePath  :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 2806
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initSubtitle()Z

    .line 2807
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    goto :goto_0

    .line 2809
    .end local v0    # "mRemoteSubtitlePath":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->checkExistWatchNowSubtitle()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2810
    const-string v1, "ShareVideo"

    const-string v2, "initSubtitle: WatchNow subtitle"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2811
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getWatchNowSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 2812
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initSubtitle()Z

    .line 2813
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    goto :goto_0

    .line 2814
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getTitleFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->checkExistOutbandSubtitle(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2815
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initSubtitle()Z

    .line 2816
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    goto/16 :goto_0

    .line 2818
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    goto/16 :goto_0
.end method

.method public isBluetoothDeviceConnected()Z
    .locals 2

    .prologue
    .line 3323
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 3324
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 3325
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3326
    const/4 v1, 0x1

    .line 3330
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBluetoothOn()Z
    .locals 1

    .prologue
    .line 3334
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 3335
    const/4 v0, 0x0

    .line 3338
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isFromLocalVideoPlayer()Z
    .locals 1

    .prologue
    .line 4013
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFromLocalVideoPlayer:Z

    return v0
.end method

.method public isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceName"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x64

    .line 1050
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1051
    const-string v3, "activity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1052
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1053
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1054
    .local v2, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v3, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1055
    const/4 v3, 0x1

    .line 1060
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1439
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1441
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConfigurationChanged. orientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1443
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_4

    .line 1444
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1445
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen(I)V

    .line 1456
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_1

    .line 1457
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOldFolderState:I

    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v0, v2, :cond_1

    .line 1458
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOldFolderState:I

    .line 1459
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sub_lcd_auto_lock"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 1461
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V

    .line 1462
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    .line 1468
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-nez v0, :cond_6

    .line 1469
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 1481
    :goto_1
    return-void

    .line 1448
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateSecondScreen(I)V

    goto :goto_0

    .line 1452
    :cond_4
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_5

    move v0, v1

    :cond_5
    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setScreenOrientation(I)V

    goto :goto_0

    .line 1471
    :cond_6
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_8

    .line 1472
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v5, :cond_7

    .line 1473
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getScreenOrientation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->prepareChangeView(I)V

    goto :goto_1

    .line 1475
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getSecondScreenOrientation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->prepareChangeView(I)V

    goto :goto_1

    .line 1478
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getScreenOrientation()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->prepareChangeView(I)V

    goto :goto_1
.end method

.method public onConnectivityChanged()V
    .locals 2

    .prologue
    .line 3926
    const-string v0, "ShareVideo"

    const-string v1, "onConnectivityChanged E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3927
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 272
    const-string v2, "ShareVideo"

    const-string v3, "onCreate() - start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onCreate(Landroid/os/Bundle;)V

    .line 275
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->enteringTime:J

    .line 276
    iput-object p0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    .line 278
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setVolumeControlStream(I)V

    .line 280
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestWindowFeature(I)Z

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 283
    new-instance v2, Lcom/sec/android/app/mv/player/util/VideoUtility;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoUtility;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mUtil:Lcom/sec/android/app/mv/player/util/VideoUtility;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 286
    new-instance v2, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 288
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/mv/player/common/VUtils;->setVUtilsData(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    .line 289
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/common/SUtils;->setUtils(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;Lcom/sec/android/app/mv/player/util/VideoAudioUtil;)V

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->startChordService()V

    .line 292
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->bindChordService()V

    .line 294
    invoke-static {}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getInstance()Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->initializeShareVideoManager()V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "netIF"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    .line 298
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate. mNetInterface : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "From"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFromLocalVideoPlayer:Z

    .line 301
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate. mFromLocalVideoPlayer : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mFromLocalVideoPlayer:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-static {p0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 310
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 311
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 312
    .local v0, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setContentView(Landroid/view/View;)V

    .line 315
    iput-boolean v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityCreated:Z

    .line 316
    iput-boolean v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    .line 317
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    .line 318
    iput-boolean v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mExit:Z

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOldFolderState:I

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenuVisibilityListener:Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;

    if-nez v2, :cond_0

    .line 322
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$1;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenuVisibilityListener:Lcom/sec/android/app/mv/player/activity/ShareVideo$MenuVisibilityListener;

    .line 325
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setOnNotificationListener(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;)V

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setOnSvcNotificationListener(Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;)V

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->bindToService(Landroid/content/ServiceConnection;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 331
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 333
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->init(Landroid/content/Intent;)V

    .line 334
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initMoviePlayer()V

    .line 335
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerBroadcastRecieverExt(Z)V

    .line 337
    const-string v2, "ShareVideo"

    const-string v3, "onCreate() - end"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f020050

    const/4 v2, 0x0

    .line 771
    const-string v0, "ShareVideo"

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    .line 774
    const/16 v0, 0x6b

    const v1, 0x7f0a0083

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 777
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v0, :cond_0

    .line 778
    const/16 v0, 0x6c

    const v1, 0x7f0a0047

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 782
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1173
    const-string v0, "ShareVideo"

    const-string v1, "onDestroy() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->checkNodeTypeChangeToClient(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1176
    const-string v0, "ShareVideo"

    const-string v1, "onDestroy() - chord service doesn\'t stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->stopSharePlay()Z

    .line 1178
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChord()V

    .line 1179
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unbindChordService()V

    .line 1180
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopChordService()V

    .line 1183
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_1

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllInController()V

    .line 1185
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeAllViews()V

    .line 1188
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoOnresumeForDelay:Z

    if-nez v0, :cond_2

    .line 1189
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->getVolumeValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->setVolume(I)Z

    .line 1190
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    .line 1193
    :cond_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoOnresumeForDelay:Z

    if-nez v0, :cond_3

    .line 1194
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->stopSharePlay()Z

    .line 1197
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->release()V

    .line 1198
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    .line 1201
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    .line 1208
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->statusBarSetting(Z)V

    .line 1209
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->removeAllPopup()V

    .line 1211
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->unbindFromService()V

    .line 1213
    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerBroadcastRecieverExt(Z)V

    .line 1215
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityCreated:Z

    .line 1216
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_4

    .line 1219
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 1222
    :cond_4
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->destroyToast()V

    .line 1223
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->releaseMainView()V

    .line 1225
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onDestroy()V

    .line 1226
    const-string v0, "ShareVideo"

    const-string v1, "onDestroy() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    return-void
.end method

.method public onFileCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "exchangeId"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;

    .prologue
    .line 3975
    const-string v0, "ShareVideo"

    const-string v1, "onFileCompleted E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3976
    return-void
.end method

.method public onFileProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "bSend"    # Z
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "progress"    # I
    .param p5, "exchangeId"    # Ljava/lang/String;

    .prologue
    .line 3981
    const-string v0, "ShareVideo"

    const-string v1, "onFileProgress E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3982
    return-void
.end method

.method public onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "fromNode"    # Ljava/lang/String;
    .param p2, "fromChannel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "hash"    # Ljava/lang/String;
    .param p5, "fileType"    # Ljava/lang/String;
    .param p6, "exchangeId"    # Ljava/lang/String;
    .param p7, "fileSize"    # J
    .param p9, "tmpFilePath"    # Ljava/lang/String;

    .prologue
    .line 3933
    const-string v0, "ShareVideo"

    const-string v1, "onFileReceived E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3934
    return-void
.end method

.method public onFilesCompleted(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "index"    # I

    .prologue
    const/16 v4, 0x19

    const/16 v3, 0x18

    .line 3792
    const-string v0, "ShareVideo"

    const-string v1, "onFilesCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3794
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3796
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getRecievedFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    .line 3797
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFilesCompleted. mClientSubtitlePath  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mClientSubtitlePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3799
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 3800
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 3802
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isMultiVisionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3803
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3808
    :cond_0
    :goto_0
    return-void

    .line 3805
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onFilesFinish(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # I
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;

    .prologue
    .line 3811
    const-string v0, "ShareVideo"

    const-string v1, "onFilesFinish E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3812
    return-void
.end method

.method public onFilesProgress(ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 2
    .param p1, "bSend"    # Z
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "channel"    # Ljava/lang/String;
    .param p4, "progress"    # I
    .param p5, "trId"    # Ljava/lang/String;
    .param p6, "index"    # I
    .param p7, "bMulti"    # Z

    .prologue
    .line 3788
    const-string v0, "ShareVideo"

    const-string v1, "onFilesProgress E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3789
    return-void
.end method

.method public onFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 6
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "trId"    # Ljava/lang/String;
    .param p5, "totalCount"    # I
    .param p6, "bMulti"    # Z

    .prologue
    .line 3944
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v1, :cond_0

    .line 3945
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getChannel()Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    .line 3946
    .local v0, "mChannel":Lcom/samsung/android/sdk/chord/SchordChannel;
    const/16 v2, 0x7530

    const/4 v3, 0x2

    const-wide/32 v4, 0x4b000

    move-object v1, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/chord/SchordChannel;->acceptFile(Ljava/lang/String;IIJ)V

    .line 3948
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFilesWillReceive"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3950
    .end local v0    # "mChannel":Lcom/samsung/android/sdk/chord/SchordChannel;
    :cond_0
    return-void
.end method

.method public onNetworkDisconnected()V
    .locals 2

    .prologue
    .line 3921
    const-string v0, "ShareVideo"

    const-string v1, "onNetworkDisconnected E"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3922
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 350
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 351
    const-string v0, "ShareVideo"

    const-string v1, "onNewIntent()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/mv/player/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_PLAYING_VZW:Z

    if-nez v0, :cond_0

    .line 354
    const-string v0, "ShareVideo"

    const-string v1, "onNewIntent() - call connect. Do not play video."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 363
    :goto_0
    return-void

    .line 360
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->init(Landroid/content/Intent;)V

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->invalidateOptionsMenu()V

    .line 362
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initMoviePlayer()V

    goto :goto_0
.end method

.method public onNodeEvent(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "bJoined"    # Z

    .prologue
    const/4 v6, 0x3

    .line 3816
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    if-nez v3, :cond_1

    .line 3917
    :cond_0
    :goto_0
    return-void

    .line 3820
    :cond_1
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNodeEvent node :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "channel :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "bJoined :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3822
    if-eqz p3, :cond_4

    .line 3823
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3824
    const-string v3, "ShareVideo"

    const-string v4, "onNodeEvent. getMVChannel is equal"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3826
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->addNode(Ljava/lang/String;)I

    .line 3827
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageMVsupport()V

    .line 3829
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3830
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->enteringTime:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mElapseTime:D

    .line 3833
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    iget-wide v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mElapseTime:D

    invoke-virtual {v3, v4, v6, v7}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setElapseTimeFromEnter(Ljava/lang/String;D)V

    .line 3834
    iget-wide v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mElapseTime:D

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageElapseTime(D)V

    .line 3836
    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$39;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    .line 3881
    .local v2, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Ljava/lang/Void;>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 3884
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageServerTitle()V

    .line 3885
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageServerIP()V

    .line 3886
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageSubTitleExtMessage()V

    .line 3887
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageServerSubtitleActivatedState()V

    goto/16 :goto_0

    .line 3889
    .end local v2    # "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/String;Ljava/lang/Void;Ljava/lang/Void;>;"
    :cond_2
    const/4 v1, 0x0

    .line 3892
    .local v1, "nodeIP":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    sget-object v4, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->CHORD_OPEN_CHANNEL:Ljava/lang/String;

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3896
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v3, p1, v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setNodeIP(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3893
    :catch_0
    move-exception v0

    .line 3894
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3899
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "nodeIP":Ljava/lang/String;
    :cond_3
    const-string v3, "ShareVideo"

    const-string v4, "onNodeEvent. getMVChannel is not equal."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3902
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3903
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNodeEvent. Leave MVChannel : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3905
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3906
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3907
    const-string v3, "ShareVideo"

    const-string v4, "onNodeEvent. Master node left channel"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3909
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 3910
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3914
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->removeNode(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onNotification(II)V
    .locals 5
    .param p1, "notification"    # I
    .param p2, "param"    # I

    .prologue
    const/4 v4, 0x0

    .line 2928
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    .line 3001
    :cond_0
    :goto_0
    return-void

    .line 2932
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2934
    :pswitch_1
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveSubtitleSyncTime(I)V

    .line 2935
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2936
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2937
    const v1, 0x7f0a006f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 2938
    new-instance v1, Lcom/sec/android/app/mv/player/activity/ShareVideo$28;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$28;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2954
    const v1, 0x7f0a0060

    new-instance v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$29;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo$29;-><init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0016

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2960
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    .line 2961
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2962
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSVPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2964
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 2965
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeHandler()V

    .line 2966
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto :goto_0

    .line 2971
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v1, :cond_2

    .line 2972
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->playerStop()V

    .line 2974
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_3

    .line 2975
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 2977
    :cond_3
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveSubtitleSyncTime(I)V

    goto :goto_0

    .line 2981
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->updateSubtitle(Ljava/lang/String;)V

    .line 2982
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSubtitleSettings()V

    .line 2985
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_4

    .line 2986
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 2988
    :cond_4
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveSubtitleSyncTime(I)V

    goto/16 :goto_0

    .line 2992
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 2993
    if-nez p2, :cond_5

    .line 2994
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 2996
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 2932
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 812
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 813
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mItemID:I

    .line 815
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mItemID:I

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    .line 818
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mItemID:I

    packed-switch v0, :pswitch_data_0

    .line 832
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onOptionsItemSelected(). getItemId(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 820
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    if-eqz v0, :cond_1

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->showPopup()V

    goto :goto_0

    .line 826
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_1

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->showGrouplist()V

    goto :goto_0

    .line 818
    nop

    :pswitch_data_0
    .packed-switch 0x6b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 9

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 1070
    const-string v2, "ShareVideo"

    const-string v3, "onPause() - start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onPause()V

    .line 1073
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/mv/player/common/VUtils;->dropLCDfps(Z)Z

    .line 1075
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1076
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->close()V

    .line 1079
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_1

    .line 1080
    iput-boolean v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    .line 1081
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->hidePlayerList()V

    .line 1084
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1085
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    .line 1088
    :cond_2
    sput-boolean v7, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 1089
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1091
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->releaseCpuBooster()V

    .line 1092
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->releaseBusBooster()V

    .line 1094
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_3

    .line 1095
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->removeHandlerMessage()V

    .line 1098
    :cond_3
    const/16 v2, 0x1a

    invoke-direct {p0, v2, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z

    .line 1099
    const/16 v2, 0xbb

    invoke-direct {p0, v2, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z

    .line 1100
    invoke-direct {p0, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->statusBarSetting(Z)V

    .line 1102
    const-string v2, "video"

    invoke-static {v2, v7}, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 1103
    iput-boolean v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    .line 1104
    sput-boolean v7, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoOnresumeForDelay:Z

    .line 1106
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_4

    .line 1107
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iput-boolean v7, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mMoviePlayerOnResume:Z

    .line 1116
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_5

    .line 1117
    const-string v2, "ShareVideo"

    const-string v3, "onPause. VideoList launch mode. save last played item information"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 1120
    .local v8, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v3, "lastPlayedItem"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    .end local v8    # "path":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v2, :cond_a

    .line 1124
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_6

    .line 1125
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onPause()V

    .line 1128
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->getSurfaceExists()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v2, :cond_7

    .line 1129
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v4, :cond_9

    .line 1130
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, v4, v7}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1135
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->reset()V

    .line 1143
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v2, :cond_8

    .line 1144
    new-instance v2, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 1146
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->unregisterBluetoothButtonReceiver()V

    move-wide v2, v0

    move v6, v5

    .line 1148
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1149
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->dismissDialogPopup()V

    .line 1150
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveActivityPreferences()V

    .line 1152
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->exitShareVideo()V

    .line 1154
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->hide()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1155
    const-string v0, "ShareVideo"

    const-string v1, "onPause() - hide() failed, client was disconnected from the master, exitApp() call"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 1170
    :goto_1
    return-void

    .line 1132
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2, v4, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    goto :goto_0

    .line 1138
    :cond_a
    const-string v0, "ShareVideo"

    const-string v1, "onPause() - does not load VideoSurface"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1139
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_1

    .line 1160
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 1163
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterContentObserver()V

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mLowBattery:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1167
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->removeAnimationEffects()V

    .line 1169
    const-string v0, "ShareVideo"

    const-string v1, "onPause() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x0

    .line 786
    const-string v1, "ShareVideo"

    const-string v2, "onPrepareOptionsMenu E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    if-nez v1, :cond_0

    .line 789
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initSettingsPopup()V

    .line 792
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    if-eqz v1, :cond_1

    .line 793
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->removeHandler()V

    .line 796
    :cond_1
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/sec/android/app/mv/player/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 797
    :cond_2
    const-string v1, "ShareVideo"

    const-string v2, "onPrepareOptionsMenu. menu is null or Lock State or from help screen"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    :goto_0
    return v0

    .line 801
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsPopup:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->setAllVisible()V

    .line 802
    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 804
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    if-eqz v0, :cond_4

    .line 805
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->hideGrouplistPopup()Z

    .line 808
    :cond_4
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onReceiveMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0x1a

    const/4 v9, 0x1

    .line 3690
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveMessage : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3692
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3693
    :cond_0
    const-string v6, "ShareVideo"

    const-string v7, "onReceiveMessage. message is NULL"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3784
    :cond_1
    :goto_0
    return-void

    .line 3697
    :cond_2
    const/4 v5, 0x0

    .line 3698
    .local v5, "unit":[Ljava/lang/String;
    const/4 v4, -0x1

    .line 3700
    .local v4, "type":I
    const-string v6, "%"

    invoke-virtual {p3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 3701
    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 3703
    packed-switch v4, :pswitch_data_0

    .line 3781
    :pswitch_0
    const-string v6, "ShareVideo"

    const-string v7, "There\'s no case"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3705
    :pswitch_1
    aget-object v6, v5, v9

    if-eqz v6, :cond_3

    .line 3706
    aget-object v6, v5, v9

    iput-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    .line 3709
    :cond_3
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveMessage. movie player Server IP is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3713
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3714
    aget-object v6, v5, v9

    if-eqz v6, :cond_4

    .line 3715
    aget-object v6, v5, v9

    iput-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    .line 3718
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->updateTitle(Ljava/lang/String;)V

    .line 3720
    :cond_5
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveMessage. title is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3724
    :pswitch_3
    aget-object v6, v5, v9

    if-eqz v6, :cond_1

    aget-object v6, v5, v9

    const-string v7, "mv_enter_list"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3725
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v6, :cond_6

    .line 3726
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 3729
    :cond_6
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceiveMessage. SHOW_MVLIST : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3734
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 3735
    aget-object v6, v5, v9

    if-eqz v6, :cond_7

    .line 3736
    aget-object v6, v5, v9

    iput-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubTitleExtension:Ljava/lang/String;

    .line 3738
    :cond_7
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SubTitle Extension is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubTitleExtension:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3742
    :pswitch_5
    const-wide/16 v2, 0x0

    .line 3744
    .local v2, "elapseTime":D
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3745
    aget-object v6, v5, v9

    if-eqz v6, :cond_8

    .line 3746
    aget-object v6, v5, v9

    invoke-static {v6}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 3747
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ELAPSETIME_FROM_ENTERING elapseTime : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3750
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v6, p1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setElapseTimeFromEnter(Ljava/lang/String;D)V

    .line 3751
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ELAPSETIME_FROM_ENTERING : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3753
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 3754
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    const-wide/16 v8, 0xbb8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 3759
    .end local v2    # "elapseTime":D
    :pswitch_6
    const/4 v1, 0x0

    .line 3761
    .local v1, "supportMV":Z
    aget-object v6, v5, v9

    if-eqz v6, :cond_1

    .line 3762
    aget-object v6, v5, v9

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 3764
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    if-eqz v6, :cond_1

    .line 3765
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v6, p1, v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setMVSupport(Ljava/lang/String;Z)V

    .line 3766
    const-string v6, "ShareVideo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SUPPORT_MULTIVISION supportMV : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3772
    .end local v1    # "supportMV":Z
    :pswitch_7
    aget-object v6, v5, v9

    if-eqz v6, :cond_1

    .line 3773
    aget-object v6, v5, v9

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 3775
    .local v0, "activated":Z
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v6, :cond_1

    .line 3776
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setServerSubtitleActivationState(Z)V

    goto/16 :goto_0

    .line 3703
    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/16 v5, 0x1a

    const/4 v6, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 909
    const-string v2, "ShareVideo"

    const-string v3, "onResume() - start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onResume()V

    .line 912
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.videoplayer.miniapp.MiniVideoPlayerService"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 913
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopAppInApp()V

    .line 915
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->joinShareVideo()V

    .line 917
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->show()Z

    move-result v2

    if-nez v2, :cond_1

    .line 918
    const-string v0, "ShareVideo"

    const-string v1, "onResume() - show() failed, client was disconnected from the master, exitApp() call"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    .line 1035
    :goto_0
    return-void

    .line 923
    :cond_1
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->dropLCDfps(Z)Z

    .line 925
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->removeAnimationEffects()V

    .line 926
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v3

    const-string v4, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowFlag(Landroid/view/Window;Ljava/lang/String;)V

    .line 927
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setWindowBackgroundColor()V

    .line 928
    sput-boolean v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mInitialHeadsetAction:Z

    .line 930
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->resetSubtitleIfContentChanged()V

    .line 931
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getActivityPreferences()V

    .line 933
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/mv/player/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_PLAYING_VZW:Z

    if-nez v2, :cond_2

    .line 934
    const-string v1, "ShareVideo"

    const-string v2, "onResume() - call connect. Do not play video."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    .line 937
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 941
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setServiceContext()V

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-nez v2, :cond_3

    .line 944
    const-string v0, "ShareVideo"

    const-string v1, "onResume() - does not load VideoSurface"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto :goto_0

    .line 948
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 951
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->checkLockScreenOn()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 952
    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z

    .line 957
    :goto_1
    invoke-static {}, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->resetMode()V

    .line 958
    const-string v2, "video"

    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo$SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 960
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 962
    iput-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    .line 963
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iput-boolean v1, v2, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mMoviePlayerOnResume:Z

    .line 964
    sput-boolean v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoOnresumeForDelay:Z

    .line 966
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v2, :cond_4

    .line 967
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setChangeViewDone(Z)V

    .line 969
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v2, :cond_5

    .line 970
    new-instance v2, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 972
    :cond_5
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_e

    .line 973
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 974
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen(I)V

    .line 982
    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerContentObserver()V

    .line 985
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->registerBluetoothButtonReceiver()V

    .line 986
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->dismissVolumePanel()V

    .line 988
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattLevel:I

    if-gt v0, v1, :cond_10

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBattStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    .line 990
    invoke-direct {p0, p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->popupLowBattery(Landroid/app/Activity;)V

    .line 1016
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_7

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->onResume()V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setControllerUpdate()V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setInvisibleControllers()V

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->reorderViews()V

    .line 1022
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setVisibleControllers()V

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1028
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_8

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    .line 1031
    :cond_8
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_9

    .line 1032
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->removeDeletedIdsFromPlayList(Landroid/content/Context;)V

    .line 1034
    :cond_9
    const-string v0, "ShareVideo"

    const-string v1, "onResume() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 954
    :cond_a
    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->requestSystemKeyEvent(IZ)Z

    goto/16 :goto_1

    :cond_b
    move v0, v1

    .line 974
    goto :goto_2

    .line 976
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_d

    :goto_4
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateSecondScreen(I)V

    goto :goto_3

    :cond_d
    move v0, v1

    goto :goto_4

    .line 979
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_f

    :goto_5
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen(I)V

    goto/16 :goto_3

    :cond_f
    move v0, v1

    goto :goto_5

    .line 992
    :cond_10
    const-string v0, "ShareVideo"

    const-string v1, "onResume() - startPlayback()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->checkLockScreenOn()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1005
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1006
    const-string v0, "ShareVideo"

    const-string v1, "onResume() - file path to play is not valid."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayingFileInfo(Landroid/net/Uri;)V

    .line 1008
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    .line 1009
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->exitApp()V

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 847
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onStart()V

    .line 848
    const-string v0, "ShareVideo"

    const-string v1, "onStart() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerBroadcastReciever()V

    .line 851
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->isBluetoothOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 864
    const-string v0, "ShareVideo"

    const-string v1, "onStart. Getting Headset Proxy failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    :cond_0
    const/4 v0, 0x1

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->queueNextRefresh(IJ)V

    .line 869
    const-string v0, "ShareVideo"

    const-string v1, "onStart() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 873
    const-string v0, "ShareVideo"

    const-string v1, "onStop() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 879
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 884
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 885
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mWifiConnetionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 887
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_4

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 890
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mDockReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_5

    .line 891
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mDockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 893
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_6

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 896
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_8

    .line 897
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_7

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 899
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 901
    :cond_7
    iput-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    .line 904
    :cond_8
    invoke-super {p0}, Lcom/samsung/groupcast/ExternalActivity;->onStop()V

    .line 905
    const-string v0, "ShareVideo"

    const-string v1, "onStop() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    return-void
.end method

.method public onSvcNotification(ILjava/lang/String;)V
    .locals 9
    .param p1, "action"    # I
    .param p2, "uriStr"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x7

    const/4 v4, 0x6

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1955
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSvcNotification - action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1957
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActivityOnResume:Z

    if-nez v1, :cond_1

    .line 2101
    :cond_0
    :goto_0
    return-void

    .line 1960
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2005
    :goto_1
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setResumePosition(J)V

    .line 2006
    sput-boolean v6, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 2008
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSvcNotification(). PLAYBACK_COMPLETE - mAlertmessage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    if-nez v1, :cond_0

    .line 2011
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_8

    .line 2012
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v1, :cond_2

    .line 2013
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 2016
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v1, :cond_3

    .line 2017
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->dismissAllHoveringPopup()V

    .line 2020
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 2021
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 1962
    :sswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mStopped:Z

    if-nez v1, :cond_0

    .line 1965
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1966
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShowShareVideoGuidePopup:Z

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1967
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1968
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    const-wide/16 v4, 0xfa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1972
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPopupLowBattShow:Z

    if-eqz v1, :cond_5

    .line 1973
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V

    .line 1975
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1976
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1977
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1979
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->checkLockScreenOn()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1980
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pause()V

    .line 1983
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1987
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setChangeViewDone(Z)V

    .line 1989
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 1990
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_0

    .line 1991
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/sec/android/app/mv/player/common/VUtils;->dropLCDfps(Z)Z

    goto/16 :goto_0

    .line 1998
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getVideoWidth()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1999
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    .line 2000
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setIsAudioOnlyClip(Z)V

    goto/16 :goto_1

    .line 2023
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->seekTo(I)V

    .line 2024
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPausedByUser()V

    .line 2025
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 2035
    :sswitch_4
    iget-boolean v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mAlertmessage:Z

    if-nez v1, :cond_0

    .line 2037
    const-string v0, "  "

    .line 2039
    .local v0, "play_next_toast":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    if-eqz v1, :cond_9

    .line 2040
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    .line 2042
    :cond_9
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_c

    .line 2044
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    const v2, 0x7f0a005b

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getFileName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2045
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v1, :cond_a

    .line 2046
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSettingsSubPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 2049
    :cond_a
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_HOVERING:Z

    if-eqz v1, :cond_b

    .line 2050
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBtnController:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->dismissAllHoveringPopup()V

    .line 2053
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 2054
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1, v8}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 2067
    :goto_2
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2068
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2058
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_d

    .line 2059
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 2060
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 2061
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setPlayerState(I)V

    .line 2064
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    const v2, 0x7f0a005c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2075
    .end local v0    # "play_next_toast":Ljava/lang/String;
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 2076
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->restartSubtitle()V

    .line 2081
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendSubtitleFiles()V

    .line 2082
    const-string v1, "ShareVideo"

    const-string v2, "onSvcNotification - action : 130 out"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2078
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->initSubtitle()V

    goto :goto_3

    .line 2086
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendSubtitleFiles()V

    .line 2087
    const-string v1, "ShareVideo"

    const-string v2, "onSvcNotification - action : 131 out"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2091
    :sswitch_7
    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    goto/16 :goto_0

    .line 2095
    :sswitch_8
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showToast(I)V

    goto/16 :goto_0

    .line 1960
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x66 -> :sswitch_1
        0x67 -> :sswitch_4
        0x68 -> :sswitch_4
        0x69 -> :sswitch_4
        0x6a -> :sswitch_4
        0x82 -> :sswitch_5
        0x83 -> :sswitch_6
        0x85 -> :sswitch_2
        0x8d -> :sswitch_3
        0x3b6 -> :sswitch_8
        0x3b7 -> :sswitch_7
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isChangeViewDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1433
    const/4 v0, 0x1

    .line 1435
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/groupcast/ExternalActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;

    .prologue
    .line 3969
    const-string v0, "ShareVideo"

    const-string v1, "onUpdateNodeInfo E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3970
    return-void
.end method

.method public onUpdateNodeInfo(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "nodeName"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "portNum"    # I

    .prologue
    .line 3955
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    .line 3957
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    if-eqz v0, :cond_0

    .line 3958
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->addNode(Ljava/lang/String;)I

    .line 3959
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setNodeIP(Ljava/lang/String;Ljava/lang/String;)V

    .line 3960
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setMVSupport(Ljava/lang/String;Z)V

    .line 3961
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateNodeInfo Node Name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " support Multivision : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isSupportMultiVision()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3964
    :cond_0
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateNodeInfo My node Name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Own IP : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getOwnIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3965
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 6
    .param p1, "hasFocus"    # Z

    .prologue
    .line 3348
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onWindowFocusChanged() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3350
    if-nez p1, :cond_2

    .line 3352
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 3353
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3354
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3360
    :cond_0
    :goto_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHasFocus:Z

    .line 3373
    :cond_1
    :goto_1
    return-void

    .line 3357
    :catch_0
    move-exception v0

    .line 3358
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "ShareVideo"

    const-string v2, "onWindowFocusChanged - IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3362
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHasFocus:Z

    .line 3364
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_DUAL_LCD:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_1

    .line 3365
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3366
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isShareVideoMode()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3367
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1
.end method

.method public refreshPlayList()V
    .locals 1

    .prologue
    .line 1484
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 1485
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    if-eqz v0, :cond_1

    .line 1486
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController()V

    .line 1487
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->showPlayerList()V

    .line 1492
    :cond_0
    :goto_0
    return-void

    .line 1489
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->hidePlayerList()V

    goto :goto_0
.end method

.method public registerBroadcastRecieverExt(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 1402
    if-eqz p1, :cond_2

    .line 1403
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBroadcastReceiverExRegistered:Z

    if-eqz v3, :cond_1

    .line 1429
    :cond_0
    :goto_0
    return-void

    .line 1406
    :cond_1
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1407
    .local v2, "intentFilterVolume":Landroid/content/IntentFilter;
    const-string v3, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1408
    const-string v3, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1409
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1412
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1413
    .local v0, "intentFilterPlaylist":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1414
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1416
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1417
    .local v1, "intentFilterPlaylistInfo":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST_SHOW_INFO"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1418
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistShowInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1420
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBroadcastReceiverExRegistered:Z

    goto :goto_0

    .line 1422
    .end local v0    # "intentFilterPlaylist":Landroid/content/IntentFilter;
    .end local v1    # "intentFilterPlaylistInfo":Landroid/content/IntentFilter;
    .end local v2    # "intentFilterVolume":Landroid/content/IntentFilter;
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBroadcastReceiverExRegistered:Z

    if-eqz v3, :cond_0

    .line 1424
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1425
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1426
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMVPlaylistShowInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1427
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mBroadcastReceiverExRegistered:Z

    goto :goto_0
.end method

.method public releaseRotationScreen()V
    .locals 1

    .prologue
    .line 3104
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setRequestedOrientation(I)V

    .line 3105
    return-void
.end method

.method public restartSubtitle()V
    .locals 4

    .prologue
    .line 2824
    const-string v0, "ShareVideo"

    const-string v1, "restartSubtitle E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2826
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-nez v0, :cond_0

    .line 2827
    const-string v0, "ShareVideo"

    const-string v1, "restartSubtitle return :  Null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2836
    :goto_0
    return-void

    .line 2831
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2833
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initSubtitle()Z

    .line 2834
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V

    .line 2835
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    goto :goto_0
.end method

.method public rotateScreen()V
    .locals 1

    .prologue
    .line 3063
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getScreenOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen(I)V

    .line 3064
    return-void
.end method

.method public rotateScreen(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 3067
    move v0, p1

    .line 3069
    .local v0, "mode":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3070
    const/4 v0, 0x4

    .line 3073
    :cond_0
    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    if-ne v0, v3, :cond_4

    .line 3076
    :cond_1
    if-ne v0, v3, :cond_3

    .line 3077
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setScreenOrientation(I)V

    .line 3082
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setRequestedOrientation(I)V

    .line 3086
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->updateScreenRotationBtn()V

    .line 3088
    return-void

    .line 3079
    :cond_3
    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setScreenOrientation(I)V

    goto :goto_0

    .line 3084
    :cond_4
    const-string v1, "ShareVideo"

    const-string v2, "rotateScreen : invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public rotateSecondScreen()V
    .locals 1

    .prologue
    .line 3108
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getSecondScreenOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateSecondScreen(I)V

    .line 3109
    return-void
.end method

.method public rotateSecondScreen(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 3112
    move v0, p1

    .line 3114
    .local v0, "mode":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenAutoRotation(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3115
    const/4 v0, 0x4

    .line 3118
    :cond_0
    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    if-ne v0, v3, :cond_4

    .line 3119
    :cond_1
    if-ne v0, v3, :cond_3

    .line 3120
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setSecondScreenOrientation(I)V

    .line 3125
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->setRequestedOrientation(I)V

    .line 3129
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->updateSecondScreenRotationBtn()V

    .line 3130
    return-void

    .line 3122
    :cond_3
    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->setSecondScreenOrientation(I)V

    goto :goto_0

    .line 3127
    :cond_4
    const-string v1, "ShareVideo"

    const-string v2, "rotateSecondScreen : invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected saveActivityPreferences()V
    .locals 3

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v1, "screen_mode"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getFitToScnMode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 391
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->saveFontState()V

    .line 393
    :cond_0
    return-void
.end method

.method public sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "toChannel"    # Ljava/lang/String;
    .param p2, "strFilePath"    # Ljava/lang/String;
    .param p3, "toNode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 3987
    const-string v0, "ShareVideo"

    const-string v1, "sendFile E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3988
    const/4 v0, 0x0

    return-object v0
.end method

.method public sendMessageServerSubtitleActivatedState()V
    .locals 5

    .prologue
    .line 3582
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3583
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v0

    .line 3584
    .local v0, "activated":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "307%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3586
    .local v1, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendDataToAll(Ljava/lang/String;[B)Z

    .line 3587
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageServerSubtitleActivation - message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3589
    .end local v0    # "activated":Z
    .end local v1    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public sendSubtitleFiles()V
    .locals 7

    .prologue
    .line 3547
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3568
    :goto_0
    return-void

    .line 3551
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->getNodeItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3553
    .local v1, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3554
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;

    .line 3556
    .local v2, "node":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Every Nodes for sending subtiles : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3559
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getMVChannel()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 3560
    const-string v3, "ShareVideo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendSubtitleFiles. subtitlePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSubtitlPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3561
    :catch_0
    move-exception v0

    .line 3563
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3567
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "node":Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoNodeSet$NodeItems;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageSubTitleExtMessage()V

    goto :goto_0
.end method

.method public setRemoveSystemUI(Z)V
    .locals 5
    .param p1, "remove"    # Z

    .prologue
    .line 2743
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2744
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSystemUIRemove ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") current systemUiVisibility : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2746
    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v1

    .line 2748
    .local v1, "systemUiVisibility":I
    if-eqz p1, :cond_1

    .line 2750
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    or-int/2addr v2, v1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 2757
    :cond_0
    :goto_0
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setRemoveSystemUI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2758
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2759
    return-void

    .line 2753
    :cond_1
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    and-int/2addr v2, v1

    if-eqz v2, :cond_0

    .line 2754
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    xor-int/2addr v2, v1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_0
.end method

.method public setWindowBackgroundColor()V
    .locals 2

    .prologue
    .line 3383
    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->getInstance()Lcom/sec/android/app/mv/player/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    .line 3384
    return-void
.end method

.method public showPlayerList()V
    .locals 2

    .prologue
    .line 3992
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListView:Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->showList(Z)V

    .line 3993
    return-void
.end method

.method public startChord()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 3440
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startChord , mNetInterface ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3442
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->getAvailableInterfaceTypes()Ljava/util/List;

    move-result-object v1

    .line 3444
    .local v1, "interfaceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3445
    const-string v2, "ShareVideo"

    const-string v3, "startChord. interfacelist is empty"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3446
    const v2, 0x7f0a002e

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3464
    :goto_0
    return-void

    .line 3450
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 3451
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    .line 3455
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    iget v3, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mNetInterface:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->start(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/chord/InvalidInterfaceException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 3456
    :catch_0
    move-exception v0

    .line 3457
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to start -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3458
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 3459
    .local v0, "e":Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
    const-string v2, "ShareVideo"

    const-string v3, "There is no such a connection."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3460
    .end local v0    # "e":Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
    :catch_2
    move-exception v0

    .line 3461
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ShareVideo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to start -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startChordService()V
    .locals 3

    .prologue
    .line 3483
    const-string v1, "ShareVideo"

    const-string v2, "startChordService()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3484
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3485
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 3486
    return-void
.end method

.method public stopChord()V
    .locals 3

    .prologue
    .line 3495
    const-string v0, "ShareVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopChord mChordService =>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3496
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v0, :cond_0

    .line 3497
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->leaveChannel()V

    .line 3498
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;->stop()V

    .line 3500
    :cond_0
    return-void
.end method

.method public stopChordService()V
    .locals 3

    .prologue
    .line 3489
    const-string v1, "ShareVideo"

    const-string v2, "stopChordService()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3490
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3491
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->stopService(Landroid/content/Intent;)Z

    .line 3492
    return-void
.end method

.method public stopSubtitle()V
    .locals 1

    .prologue
    .line 3342
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v0, :cond_0

    .line 3343
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    .line 3345
    :cond_0
    return-void
.end method

.method public togglePlayerListState()V
    .locals 1

    .prologue
    .line 4000
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    if-eqz v0, :cond_0

    .line 4001
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    .line 4006
    :goto_0
    return-void

    .line 4004
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPlayerListState:Z

    goto :goto_0
.end method

.method public unbindChordService()V
    .locals 2

    .prologue
    .line 3475
    const-string v0, "ShareVideo"

    const-string v1, "unbindChordService()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3476
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    if-eqz v0, :cond_0

    .line 3477
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->unbindService(Landroid/content/ServiceConnection;)V

    .line 3479
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mChordService:Lcom/sec/android/app/mv/player/multivision/chord/ChordService;

    .line 3480
    return-void
.end method

.method public updateTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2762
    const-string v0, "ShareVideo"

    const-string v1, "updateTitle()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2764
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isServer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2765
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendTitleToClient(Ljava/lang/String;)V

    .line 2768
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    if-eqz v0, :cond_1

    .line 2769
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->updateTitle(Ljava/lang/String;)V

    .line 2771
    :cond_1
    return-void
.end method
