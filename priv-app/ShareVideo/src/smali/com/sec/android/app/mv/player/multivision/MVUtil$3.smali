.class Lcom/sec/android/app/mv/player/multivision/MVUtil$3;
.super Ljava/lang/Object;
.source "MVUtil.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/multivision/MVPlayerServer$OnMessageResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/multivision/MVUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0

    .prologue
    .line 1960
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangeToMultiVision(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2018
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onChangeToMultiVision id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2019
    return-void
.end method

.method public onChangeToSingleVision(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2023
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onChangeToSingleVision id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2024
    return-void
.end method

.method public onClientPause(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 2096
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->onClientPause(I)V

    .line 2097
    return-void
.end method

.method public onClientPlay(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 2102
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->onClientPlay(I)V

    .line 2103
    return-void
.end method

.method public onExitMVGroup(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2042
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onExitMVGroup id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2043
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->exitMVGroup_l(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3400(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    .line 2044
    return-void
.end method

.method public onGetPlayPosition(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1998
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onGetPlayPosition id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1999
    return-void
.end method

.method public onGetPlayerState(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2003
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onGetPlayerState id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2004
    return-void
.end method

.method public onHide(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2078
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onHide id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2079
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->hide_l(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3800(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    .line 2080
    return-void
.end method

.method public onJoinMVGroup(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2034
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onJoinMVGroup id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2035
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSupportMultiVision()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2036
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->joinMVGroup_l(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3300(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    .line 2038
    :cond_0
    return-void
.end method

.method public onPause(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1973
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onPause id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1974
    return-void
.end method

.method public onPlay(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1968
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onPlay id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1969
    return-void
.end method

.method public onResume(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1983
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onResume id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1984
    return-void
.end method

.method public onSeek(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1988
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSeek id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    return-void
.end method

.method public onSendClientDisconnect(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2028
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSendClientDisconnect id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2029
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->deleteClient(I)V

    .line 2030
    return-void
.end method

.method public onSendClientInitialInfo(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    .locals 3
    .param p1, "player"    # Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .prologue
    .line 2068
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSendClientInitialInfo id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", DeviceName : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getClientDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2070
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->tryClientTimeSync(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3700(Lcom/sec/android/app/mv/player/multivision/MVUtil;Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    .line 2071
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->addClient(Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;)V

    .line 2073
    return-void
.end method

.method public onSendClientMVRequest(IIJ)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "direction"    # I
    .param p3, "touchtime"    # J

    .prologue
    .line 2048
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->isSupportMultiVision()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2050
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mSystemTimeBase:J
    invoke-static {v4}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3500(Lcom/sec/android/app/mv/player/multivision/MVUtil;)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 2052
    .local v0, "serverTime":J
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MVMR: onSendClientMVRequest id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", direction : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", TouchTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ServerTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2054
    const-string v2, "MVUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MVMR: onSendClientMVRequest gap result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v4, v0, p3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2056
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # getter for: Lcom/sec/android/app/mv/player/multivision/MVUtil;->mTouchMotion:Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3600(Lcom/sec/android/app/mv/player/multivision/MVUtil;)Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/multivision/MVTouchMotion;->sendTouchInfo(IIJ)V

    .line 2059
    .end local v0    # "serverTime":J
    :cond_0
    return-void
.end method

.method public onSendContentURL(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2063
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSendContentURL id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    return-void
.end method

.method public onSendMasterClock(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2008
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSendMasterClock id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2009
    return-void
.end method

.method public onSendPlayerState(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2013
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSendPlayerState id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    return-void
.end method

.method public onSetDataSource(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1963
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSetDataSource id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1964
    return-void
.end method

.method public onSetPlayPosition(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1993
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSetPlayPosition id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1994
    return-void
.end method

.method public onShow(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2084
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onSHow id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2085
    iget-object v0, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->show_l(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$3900(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)V

    .line 2086
    return-void
.end method

.method public onStop(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 1978
    const-string v0, "MVUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MVMR: onStop id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1979
    return-void
.end method

.method public sendVolumeValueUpdateById(II)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "volume"    # I

    .prologue
    .line 2090
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/multivision/MVUtil$3;->this$0:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    # invokes: Lcom/sec/android/app/mv/player/multivision/MVUtil;->getType(I)I
    invoke-static {v1, p1}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->access$4000(Lcom/sec/android/app/mv/player/multivision/MVUtil;I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/mv/player/common/SUtils;->onVolumeChanged(II)V

    .line 2091
    return-void
.end method
