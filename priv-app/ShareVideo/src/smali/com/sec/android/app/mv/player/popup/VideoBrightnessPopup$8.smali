.class Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seek"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 352
    if-eqz p3, :cond_1

    .line 353
    const-string v0, "VideoBrightnessDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChanged - brightness : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 358
    :cond_1
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seek"    # Landroid/widget/SeekBar;

    .prologue
    .line 361
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seek"    # Landroid/widget/SeekBar;

    .prologue
    .line 364
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getAutoBrightDetailLevelInSystemValue()I

    move-result v0

    .line 366
    .local v0, "saveVal":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 367
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 368
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 370
    .end local v0    # "saveVal":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->resetWindowBrightness()V

    .line 373
    :cond_1
    return-void
.end method
