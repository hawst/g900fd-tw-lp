.class Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "DeviceListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/DeviceListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceListView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/mv/player/view/DeviceListView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Device List : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceSize()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 54
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceInfo(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 59
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "index"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "group"    # Landroid/view/ViewGroup;

    .prologue
    .line 64
    sget-object v6, Lcom/sec/android/app/mv/player/view/DeviceListView;->TAG:Ljava/lang/String;

    const-string v7, " getView : index"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/sec/android/app/mv/player/common/SUtils;->getDeviceInfoForListview(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;

    .line 67
    .local v3, "mMVDevice":Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 68
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030001

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 70
    const v6, 0x7f0d0007

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    .local v0, "deviceName":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDeviceID()I

    move-result v6

    if-nez v6, :cond_0

    .line 75
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->this$0:Lcom/sec/android/app/mv/player/view/DeviceListView;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/view/DeviceListView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 78
    :cond_0
    const v6, 0x7f0d0008

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 79
    .local v1, "devicePos":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVDeviceInfo;->getDevicePos()I

    move-result v4

    .line 80
    .local v4, "mPos":I
    const/4 v6, -0x1

    if-ne v4, v6, :cond_1

    .line 81
    const v6, 0x7f0a0086

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 87
    :goto_0
    return-object p2

    .line 83
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceListView$DeviceAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0054

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 84
    .local v5, "str":Ljava/lang/String;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/mv/player/view/DeviceListView;->TAG:Ljava/lang/String;

    const-string v1, "---- notifyDataSetChanged"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 44
    return-void
.end method
