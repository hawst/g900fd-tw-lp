.class public Lcom/sec/android/app/mv/player/view/VideoGestureView;
.super Ljava/lang/Object;
.source "VideoGestureView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoGestureView"


# instance fields
.field private GESTURE_MAX_PROGRESS:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

.field private mBrightnessAnimIcon:Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

.field private mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

.field private mBrightnessImage:Landroid/widget/ImageView;

.field private mBrightnessPopup:Landroid/widget/PopupWindow;

.field private final mBrightnessRes:[I

.field private mBrightnessSeekBar:Landroid/widget/SeekBar;

.field private mBrightnessText:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mGestureView:Landroid/view/View;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

.field private mVolumeGestureLayout:Landroid/widget/RelativeLayout;

.field private mVolumeImage:Landroid/widget/ImageView;

.field private mVolumePopup:Landroid/widget/PopupWindow;

.field private mVolumeSeekBar:Landroid/widget/SeekBar;

.field private mVolumeText:Landroid/widget/TextView;

.field private mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mWindow:Landroid/view/Window;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessAnimIcon:Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    .line 55
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessRes:[I

    .line 125
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 70
    const-string v0, "VideoGestureView"

    const-string v1, "VideoGestureView"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    .line 72
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mWindow:Landroid/view/Window;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 75
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    .line 76
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mWindow:Landroid/view/Window;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;-><init>(Landroid/view/Window;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 77
    return-void

    .line 55
    :array_0
    .array-data 4
        0x7f0200b4
        0x7f0200b5
        0x7f0200b6
        0x7f0200b7
        0x7f0200b8
        0x7f0200b9
        0x7f0200ba
        0x7f0200bb
        0x7f0200bc
        0x7f0200bd
        0x7f0200be
    .end array-data
.end method

.method private initGestureView()V
    .locals 4

    .prologue
    const v3, 0x1030004

    const/4 v2, 0x3

    .line 210
    const-string v0, "VideoGestureView"

    const-string v1, "initGestureView"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 212
    const-string v0, "VideoGestureView"

    const-string v1, "initGestureView : mGestureView is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 218
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0083

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    .line 226
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ANIMATED_BRIGHTNESS_ICON:Z

    if-eqz v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessAnimIcon:Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

    .line 232
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0085

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method private setBrightnessBarGone()V
    .locals 4

    .prologue
    const v3, 0x7f0d0085

    const/16 v2, 0x8

    .line 327
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBarGone"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0083

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0084

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0087

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 344
    :cond_1
    return-void
.end method

.method private setBrightnessBarUI(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 252
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBar"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v0, :cond_0

    .line 254
    const-string v0, "VideoGestureView"

    const-string v1, "setBrightnessBar : mVideoAudioUtil is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :goto_0
    return-void

    .line 258
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    if-le p1, v0, :cond_2

    .line 259
    iget p1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    .line 263
    :cond_1
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessText(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 260
    :cond_2
    if-gez p1, :cond_1

    .line 261
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private setBrightnessBarVisible()V
    .locals 9

    .prologue
    const v8, 0x7f0d0086

    const v7, 0x7f0d0085

    const v6, 0x7f0d0083

    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 290
    const-string v2, "VideoGestureView"

    const-string v3, "setBrightnessBarVisible"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 295
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolumeBarGone()V

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 300
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d0084

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 301
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ANIMATED_BRIGHTNESS_ICON:Z

    if-eqz v2, :cond_3

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 308
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d0087

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d0088

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 312
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 315
    .local v1, "paramBrightness":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 318
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 320
    const/16 v0, 0x13

    .line 322
    .local v0, "gravity":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3, v0, v5, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 324
    .end local v0    # "gravity":I
    .end local v1    # "paramBrightness":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    return-void

    .line 305
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setBrightnessText(I)V
    .locals 8
    .param p1, "level"    # I

    .prologue
    const/4 v7, 0x0

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    int-to-float v3, p1

    iget v4, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 274
    .local v2, "percent":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 275
    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->brightnessValueLocale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    :cond_2
    :goto_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->updateBrightnessIcon(I)V

    goto :goto_0

    .line 279
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    .local v0, "mFormatBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 281
    .local v1, "mFormatter":Ljava/util/Formatter;
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessText:Landroid/widget/TextView;

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v1, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    invoke-virtual {v1}, Ljava/util/Formatter;->close()V

    goto :goto_1
.end method

.method private setVolumeBarGone()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 382
    const-string v0, "VideoGestureView"

    const-string v1, "setVolumeBarGone"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0090

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v1, 0x7f0d008f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 397
    :cond_1
    return-void
.end method

.method private setVolumeBarVisible()V
    .locals 6

    .prologue
    const v4, 0x7f0d008b

    const/4 v5, 0x0

    .line 347
    const-string v2, "VideoGestureView"

    const-string v3, "setVolumeBarVisible"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-eqz v2, :cond_0

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->dismissVolumePanel()V

    .line 351
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 354
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarGone()V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeGestureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d008c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d008d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d0090

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d0091

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 363
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d008e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    const v3, 0x7f0d008f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 367
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 368
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 370
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 375
    const/16 v0, 0x15

    .line 377
    .local v0, "gravity":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3, v0, v5, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 379
    .end local v0    # "gravity":I
    .end local v1    # "rp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    return-void
.end method

.method private setVolumeUI(II)V
    .locals 4
    .param p1, "vol"    # I
    .param p2, "level"    # I

    .prologue
    .line 128
    const-string v1, "VideoGestureView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVolumeUI : currentLevel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vol = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getMaxVolume()I

    move-result v0

    .line 131
    .local v0, "maxVol":I
    if-gez p1, :cond_2

    .line 132
    const/4 p1, 0x0

    .line 137
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 139
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 142
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v1, :cond_4

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-nez p1, :cond_3

    const v1, 0x7f020098

    :goto_1
    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 147
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeText:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->setVolume(I)Z

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolumeBarVisible()V

    .line 153
    return-void

    .line 133
    :cond_2
    if-le p1, v0, :cond_0

    .line 134
    move p1, v0

    goto :goto_0

    .line 143
    :cond_3
    const v1, 0x7f020099

    goto :goto_1

    .line 145
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-nez p1, :cond_5

    const v1, 0x7f0200b3

    :goto_3
    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_5
    const v1, 0x7f0200bf

    goto :goto_3
.end method

.method private updateBrightnessIcon(I)V
    .locals 4
    .param p1, "percent"    # I

    .prologue
    .line 416
    const-string v1, "VideoGestureView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateBrightnessIcon :: percent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 418
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 420
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 423
    :cond_0
    div-int/lit8 v0, p1, 0xa

    .line 425
    .local v0, "id":I
    if-gez v0, :cond_3

    .line 426
    const/4 v0, 0x0

    .line 430
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessRes:[I

    aget v2, v2, v0

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 433
    .end local v0    # "id":I
    :cond_2
    return-void

    .line 427
    .restart local v0    # "id":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessRes:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_4

    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    .line 428
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessRes:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method private updateIcon(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 407
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ANIMATED_BRIGHTNESS_ICON:Z

    if-eqz v0, :cond_0

    .line 408
    const-string v0, "VideoGestureView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateIcon :: value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessAnimIcon:Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessAnimIcon:Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/view/AnimatedBrightnessIconView;->setValue(I)V

    .line 413
    :cond_0
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 80
    const-string v1, "VideoGestureView"

    const-string v2, "addViewTo"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 82
    const-string v1, "VideoGestureView"

    const-string v2, "addViewTo : context is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .end local p1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 86
    .restart local p1    # "view":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 87
    .local v0, "inflate":Landroid/view/LayoutInflater;
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 88
    const v1, 0x7f030019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->initGestureView()V

    goto :goto_0
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolumeBarGone()V

    .line 206
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarGone()V

    .line 207
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumePopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    :cond_1
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 402
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mGestureView:Landroid/view/View;

    .line 403
    iput-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mParentView:Landroid/widget/RelativeLayout;

    .line 404
    return-void
.end method

.method public setBrightness(I)V
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 169
    const-string v3, "VideoGestureView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setBrightness E : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v3, :cond_0

    .line 184
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightnessSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 175
    .local v0, "currentLevel":I
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarVisible()V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 178
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    .line 179
    .local v1, "maxVol":I
    add-int/2addr v0, p1

    .line 180
    int-to-float v3, v1

    int-to-float v4, v0

    iget v5, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float v2, v3, v4

    .line 181
    .local v2, "val":F
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    float-to-int v4, v2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setBrightness(I)I

    .line 183
    .end local v1    # "maxVol":I
    .end local v2    # "val":F
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarUI(I)V

    goto :goto_0
.end method

.method public setVolume(I)V
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 102
    const-string v3, "VideoGestureView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setVolume : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    if-nez v3, :cond_0

    .line 105
    const-string v3, "VideoGestureView"

    const-string v4, "setVolume : mVideoAudioUtil is null"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->volumeSame()V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v3

    int-to-float v2, v3

    .line 115
    .local v2, "vol":F
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getMaxVolume()I

    move-result v1

    .line 116
    .local v1, "maxVol":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 118
    .local v0, "currentLevel":I
    add-int/2addr v0, p1

    .line 120
    int-to-float v3, v1

    int-to-float v4, v0

    iget v5, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float v2, v3, v4

    .line 122
    float-to-int v3, v2

    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolumeUI(II)V

    goto :goto_0
.end method

.method public showBrightness()V
    .locals 6

    .prologue
    .line 156
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    .line 157
    .local v1, "level":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    .line 158
    .local v2, "maxLevel":I
    int-to-float v3, v1

    iget v4, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    mul-float v0, v3, v4

    .line 160
    .local v0, "currentLevel":F
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 164
    :cond_0
    float-to-int v3, v0

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarUI(I)V

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setBrightnessBarVisible()V

    .line 166
    return-void
.end method

.method public showVolume()V
    .locals 5

    .prologue
    .line 94
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v2

    .line 95
    .local v2, "vol":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mVideoAudioUtil:Lcom/sec/android/app/mv/player/util/VideoAudioUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/VideoAudioUtil;->getMaxVolume()I

    move-result v1

    .line 96
    .local v1, "maxVol":I
    int-to-float v3, v2

    iget v4, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->GESTURE_MAX_PROGRESS:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    int-to-float v4, v1

    div-float v0, v3, v4

    .line 98
    .local v0, "currentLevel":F
    float-to-int v3, v0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->setVolumeUI(II)V

    .line 99
    return-void
.end method

.method public syncBrightnessWithSystemLevel()V
    .locals 2

    .prologue
    .line 187
    const-string v0, "VideoGestureView"

    const-string v1, "syncBrightnessWithSystemLevel"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v0, :cond_0

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setSystemBrightnessLevel()V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoGestureView;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->resetWindowBrightness()V

    goto :goto_0
.end method
