.class public Lcom/sec/android/app/mv/player/db/VideoDB;
.super Ljava/lang/Object;
.source "VideoDB.java"


# static fields
.field public static final COLUMN_FLAG_COUNT:Ljava/lang/String; = "-100"

.field private static final COUNT_DISPLAY_THRESHOLD:I = 0xa

.field private static final DEFAULT_WHERE_CLAUSE:Ljava/lang/String; = "length(trim(_data)) > 0"

.field private static final DEFAULT_WHERE_CLAUSE_FOLDER_VIEW:Ljava/lang/String; = "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

.field public static final EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field public static final INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

.field public static final SEARCH_ESCAPE_STRING:Ljava/lang/String; = " escape \'!\'"

.field private static final TAG:Ljava/lang/String; = "VideoDB"

.field private static final TITLE_ASCENDING_ORDER:Ljava/lang/String; = " COLLATE LOCALIZED ASC"

.field private static mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 28
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    .line 53
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const-class v1, Lcom/sec/android/app/mv/player/db/VideoDB;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/db/VideoDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/mv/player/db/VideoDB;->mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 22
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 246
    const/4 v4, 0x0

    .line 295
    :goto_0
    return-object v4

    .line 248
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-nez v19, :cond_2

    .line 249
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 250
    const/4 v4, 0x0

    goto :goto_0

    .line 253
    :cond_2
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    new-array v12, v0, [I

    .line 254
    .local v12, "noDRMIndex":[I
    const/4 v2, 0x0

    .line 255
    .local v2, "Index":I
    const/4 v10, 0x0

    .line 256
    .local v10, "noDRMCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoDRMUtil;

    move-result-object v6

    .line 259
    .local v6, "drmUtil":Lcom/sec/android/app/mv/player/util/VideoDRMUtil;
    :cond_3
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 261
    .local v5, "data":Ljava/lang/String;
    invoke-virtual {v6, v5}, Lcom/sec/android/app/mv/player/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 262
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "noDRMCount":I
    .local v11, "noDRMCount":I
    aput v2, v12, v10

    move v10, v11

    .line 264
    .end local v11    # "noDRMCount":I
    .restart local v10    # "noDRMCount":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 265
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-nez v19, :cond_3

    .line 267
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    move/from16 v0, v19

    if-ne v10, v0, :cond_5

    .line 268
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-object/from16 v4, p1

    .line 269
    goto :goto_0

    .line 270
    :cond_5
    if-nez v10, :cond_6

    .line 271
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 272
    const/16 p1, 0x0

    .line 273
    const/4 v4, 0x0

    goto :goto_0

    .line 276
    :cond_6
    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "cols":[Ljava/lang/String;
    new-instance v4, Landroid/database/MatrixCursor;

    invoke-direct {v4, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 279
    .local v4, "cursorToReturn":Landroid/database/MatrixCursor;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v10, :cond_7

    .line 280
    aget v19, v12, v8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 281
    const-string v19, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 282
    .local v18, "videoId":Ljava/lang/String;
    const-string v19, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 283
    .local v17, "title":Ljava/lang/String;
    const-string v19, "duration"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 284
    .local v7, "duration":Ljava/lang/String;
    const-string v19, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 285
    const-string v19, "isPlayed"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 286
    .local v13, "played":I
    const-string v19, "resumePos"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 287
    .local v14, "resumePos":J
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    .line 288
    .local v16, "resumePosition":Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 289
    .local v9, "isPlayed":Ljava/lang/String;
    const-string v19, "VideoDB"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "isPlayed = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", resumePosition : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/16 v19, 0x6

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v18, v19, v20

    const/16 v20, 0x1

    aput-object v17, v19, v20

    const/16 v20, 0x2

    aput-object v7, v19, v20

    const/16 v20, 0x3

    aput-object v5, v19, v20

    const/16 v20, 0x4

    aput-object v16, v19, v20

    const/16 v20, 0x5

    aput-object v9, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 279
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 293
    .end local v7    # "duration":Ljava/lang/String;
    .end local v9    # "isPlayed":Ljava/lang/String;
    .end local v13    # "played":I
    .end local v14    # "resumePos":J
    .end local v16    # "resumePosition":Ljava/lang/String;
    .end local v17    # "title":Ljava/lang/String;
    .end local v18    # "videoId":Ljava/lang/String;
    :cond_7
    const-string v19, "VideoDB"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "excludeDRMFiles. cursorToReturn count = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Landroid/database/MatrixCursor;->getCount()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->moveToFirst()Z

    goto/16 :goto_0
.end method

.method private fetchInt(Landroid/net/Uri;Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, -0x1

    .line 639
    if-nez p1, :cond_1

    move v0, v8

    .line 659
    :cond_0
    :goto_0
    return v0

    .line 642
    :cond_1
    const/4 v6, 0x0

    .line 643
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 647
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 648
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 649
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 655
    if-eqz v6, :cond_0

    .line 656
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 655
    :cond_2
    if-eqz v6, :cond_3

    .line 656
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move v0, v8

    .line 659
    goto :goto_0

    .line 652
    :catch_0
    move-exception v7

    .line 653
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnLong() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    if-eqz v6, :cond_3

    .line 656
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 655
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 656
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private fetchLong(Landroid/net/Uri;Ljava/lang/String;)J
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    .line 663
    if-nez p1, :cond_1

    move-wide v0, v8

    .line 683
    :cond_0
    :goto_0
    return-wide v0

    .line 666
    :cond_1
    const/4 v6, 0x0

    .line 667
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 671
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 672
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 673
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 674
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 679
    if-eqz v6, :cond_0

    .line 680
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 679
    :cond_2
    if-eqz v6, :cond_3

    .line 680
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-wide v0, v8

    .line 683
    goto :goto_0

    .line 676
    :catch_0
    move-exception v7

    .line 677
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnLong() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 679
    if-eqz v6, :cond_3

    .line 680
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 679
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 680
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/mv/player/db/VideoDB;
    .locals 2

    .prologue
    .line 64
    const-class v0, Lcom/sec/android/app/mv/player/db/VideoDB;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->mInstance:Lcom/sec/android/app/mv/player/db/VideoDB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 234
    if-nez p0, :cond_0

    .line 235
    const-string p0, "_"

    .line 241
    :goto_0
    return-object p0

    .line 237
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 238
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 239
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 240
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 241
    goto :goto_0
.end method

.method private getRowFolderCountString(I)Ljava/lang/String;
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRowItemCountString(I)Ljava/lang/String;
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 687
    if-nez p1, :cond_1

    move-object v0, v8

    .line 707
    :cond_0
    :goto_0
    return-object v0

    .line 690
    :cond_1
    const/4 v6, 0x0

    .line 691
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 695
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 696
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 697
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 698
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 703
    if-eqz v6, :cond_0

    .line 704
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 703
    :cond_2
    if-eqz v6, :cond_3

    .line 704
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v8

    .line 707
    goto :goto_0

    .line 700
    :catch_0
    move-exception v7

    .line 701
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returnString() uri:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " column:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " e:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 703
    if-eqz v6, :cond_3

    .line 704
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 703
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 704
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getBucketID(Landroid/net/Uri;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 351
    const-string v0, "bucket_id"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchInt(Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getBucketIdbyPath(Ljava/lang/String;)I
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 474
    const/4 v7, -0x1

    .line 475
    .local v7, "id":I
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "bucket_id"

    aput-object v0, v2, v1

    .line 478
    .local v2, "cols":[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 480
    .local v9, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data = ?"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    new-array v4, v3, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 484
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 486
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 489
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 490
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 492
    .local v8, "sBucketId":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 496
    .end local v8    # "sBucketId":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 497
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 499
    :cond_1
    return v7

    .line 496
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 497
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getDateTaken(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 359
    const-string v0, "date_modified"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDisplayCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 307
    if-nez p1, :cond_1

    .line 308
    const/4 p1, 0x0

    .line 321
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 309
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_0

    .line 312
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 314
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    .line 315
    .local v0, "addString":[Ljava/lang/String;
    const-string v2, "-100"

    aput-object v2, v0, v4

    .line 316
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getRowItemCountString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 318
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 319
    .local v1, "cursorToAdd":Landroid/database/MatrixCursor;
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 321
    new-instance v2, Landroid/database/MergeCursor;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/database/Cursor;

    aput-object p1, v3, v4

    aput-object v1, v3, v5

    invoke-direct {v2, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object p1, v2

    goto :goto_0
.end method

.method public getDisplayFolderCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 325
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gtz v4, :cond_2

    .line 326
    :cond_0
    const/4 p1, 0x0

    .line 347
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-object p1

    .line 327
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-lt v4, v5, :cond_1

    .line 330
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    .line 332
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    .line 333
    .local v0, "addString":[Ljava/lang/String;
    const-string v4, "-100"

    aput-object v4, v0, v6

    .line 334
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getRowFolderCountString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v7

    .line 336
    const/4 v3, 0x0

    .line 338
    .local v3, "retCursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 339
    .local v1, "cursorToAdd":Landroid/database/MatrixCursor;
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 341
    new-instance v3, Landroid/database/MergeCursor;

    .end local v3    # "retCursor":Landroid/database/Cursor;
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/database/Cursor;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-direct {v3, v4}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v3    # "retCursor":Landroid/database/Cursor;
    move-object p1, v3

    .line 347
    goto :goto_0

    .line 342
    .end local v1    # "cursorToAdd":Landroid/database/MatrixCursor;
    .end local v3    # "retCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 343
    .local v2, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 383
    const-string v0, "_display_name"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDurationTime(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 363
    const-string v0, "duration"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileId(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 367
    const-string v0, "_id"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileIdByPath(Ljava/lang/String;)J
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 526
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-wide v0, v2

    .line 534
    :cond_1
    :goto_0
    return-wide v0

    .line 529
    :cond_2
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 531
    .local v0, "id":J
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 532
    sget-object v2, Lcom/sec/android/app/mv/player/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 615
    const-wide/16 v8, -0x1

    .line 616
    .local v8, "id":J
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 619
    .local v2, "cols":[Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 620
    .local v7, "where":Ljava/lang/StringBuilder;
    const-string v0, "_data LIKE \'%\' || ? || \'%\'"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const/4 v6, 0x0

    .line 623
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 626
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 627
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 631
    :cond_0
    if-eqz v6, :cond_1

    .line 632
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_1
    return-wide v8

    .line 631
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 632
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFileNameByUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 591
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v2, v1

    .line 594
    .local v2, "cols":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 595
    .local v8, "name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 598
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 599
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 600
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 601
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 606
    :cond_0
    if-eqz v6, :cond_1

    .line 607
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 611
    :cond_1
    :goto_0
    return-object v8

    .line 603
    :catch_0
    move-exception v7

    .line 604
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFilePathbyId() "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606
    if-eqz v6, :cond_1

    .line 607
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 606
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 607
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 375
    const-string v0, "_data"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileSize(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 399
    const-string v0, "_size"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFolderName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 395
    const-string v0, "bucket_display_name"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFolderNameByBucketID(I)Ljava/lang/String;
    .locals 9
    .param p1, "bucketid"    # I

    .prologue
    const/4 v1, 0x0

    .line 403
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "bucket_display_name"

    aput-object v0, v2, v1

    .line 406
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 408
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 409
    .local v8, "foldername":Ljava/lang/String;
    const/4 v6, 0x0

    .line 412
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 413
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 414
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 420
    :cond_0
    if-eqz v6, :cond_1

    .line 421
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 424
    :cond_1
    :goto_0
    return-object v8

    .line 417
    :catch_0
    move-exception v7

    .line 418
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileNameByBucketID() "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    if-eqz v6, :cond_1

    .line 421
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 420
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 421
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getFolderPathByBucketID(I)Ljava/lang/String;
    .locals 9
    .param p1, "bucketid"    # I

    .prologue
    const/4 v1, 0x0

    .line 428
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 431
    .local v2, "cols":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 433
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 434
    .local v8, "foldername":Ljava/lang/String;
    const/4 v6, 0x0

    .line 437
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 438
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 439
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 445
    :cond_0
    if-eqz v6, :cond_1

    .line 446
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 449
    :cond_1
    :goto_0
    return-object v8

    .line 442
    :catch_0
    move-exception v7

    .line 443
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileNameByBucketID() "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    if-eqz v6, :cond_1

    .line 446
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 445
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 446
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 387
    const-string v0, "mime_type"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResolution(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 391
    const-string v0, "resolution"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResumePosition(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 371
    const-string v0, "resumePos"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSize(Landroid/net/Uri;)J
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 355
    const-string v0, "_size"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchLong(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSortOrderString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    iget-object v2, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v2

    const-string v3, "sortorder"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 69
    .local v1, "sortOrder":I
    const-string v2, "VideoDB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSortOrderString() order:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    packed-switch v1, :pswitch_data_0

    .line 89
    const-string v2, "title COLLATE LOCALIZED ASC"

    :goto_0
    return-object v2

    .line 73
    :pswitch_0
    const-string v2, "title COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 76
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 83
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "(_size * 1) DESC"

    goto :goto_0

    .line 86
    :pswitch_3
    const-string v2, "mime_type ASC"

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getTitleName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 379
    const-string v0, "title"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/mv/player/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUriByPath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const-wide/16 v6, -0x1

    .line 503
    if-eqz p1, :cond_0

    const-string v4, ""

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v2, v3

    .line 522
    :cond_1
    :goto_0
    return-object v2

    .line 506
    :cond_2
    const/4 v2, 0x0

    .line 507
    .local v2, "uri":Landroid/net/Uri;
    const-wide/16 v0, -0x1

    .line 509
    .local v0, "id":J
    cmp-long v4, v0, v6

    if-nez v4, :cond_3

    .line 510
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 511
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 514
    :cond_3
    cmp-long v4, v0, v6

    if-nez v4, :cond_4

    .line 515
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    .line 516
    sget-object v4, Lcom/sec/android/app/mv/player/db/VideoDB;->INTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 519
    :cond_4
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    move-object v2, v3

    .line 520
    goto :goto_0
.end method

.method public getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;
    .locals 9
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z

    .prologue
    const/4 v6, 0x0

    .line 98
    const/4 v5, -0x1

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v7, v6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;
    .locals 9
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z
    .param p5, "listType"    # I

    .prologue
    const/4 v6, 0x0

    .line 102
    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v7, v6

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(IZLjava/lang/String;ZIZZ)Landroid/database/Cursor;
    .locals 9
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z
    .param p5, "listType"    # I
    .param p6, "excludeOnlineFiles"    # Z
    .param p7, "excludeLocalFiles"    # Z

    .prologue
    .line 110
    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z
    .param p5, "listType"    # I
    .param p6, "excludeOnlineFiles"    # Z
    .param p7, "excludeLocalFiles"    # Z
    .param p8, "limitCount"    # Ljava/lang/String;

    .prologue
    .line 114
    sget-object v2, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    .line 115
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/db/VideoDB;->getSortOrderString()Ljava/lang/String;

    move-result-object v6

    .line 116
    .local v6, "orderByClause":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v3

    .line 117
    .local v3, "cols":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 119
    .local v7, "cursorVideo":Landroid/database/Cursor;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .local v10, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    if-nez p2, :cond_0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " AND bucket_id="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " AND title like \'%"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Lcom/sec/android/app/mv/player/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " escape \'!\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    if-eqz p8, :cond_2

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 135
    :cond_2
    if-eqz p7, :cond_3

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " AND _data NOT LIKE \""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 149
    if-eqz p4, :cond_4

    .line 150
    invoke-direct {p0, v7}, Lcom/sec/android/app/mv/player/db/VideoDB;->excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v7

    .line 152
    :cond_4
    if-nez v7, :cond_5

    .line 153
    const/4 v1, 0x0

    .line 164
    :goto_0
    return-object v1

    .line 141
    :catch_0
    move-exception v8

    .line 142
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVideoCursor - SQLiteException :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x0

    goto :goto_0

    .line 144
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v9

    .line 145
    .local v9, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v1, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVideoCursor - UnsupportedOperationException :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v1, 0x0

    goto :goto_0

    .line 154
    .end local v9    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_6

    .line 155
    const-string v1, "VideoDB"

    const-string v4, "cursorVideo.getCount() <= 0"

    invoke-static {v1, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    .line 159
    goto :goto_0

    .line 162
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v1, v7

    .line 164
    goto :goto_0
.end method

.method public getVideoCursor(IZLjava/lang/String;ZLjava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "strFilter"    # Ljava/lang/String;
    .param p4, "excludeDRM"    # Z
    .param p5, "limitCount"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 106
    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v7, v6

    move-object v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursor(IZZ)Landroid/database/Cursor;
    .locals 9
    .param p1, "bucketId"    # I
    .param p2, "allBucket"    # Z
    .param p3, "excludeDRM"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 94
    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move v7, v6

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZIZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoCursorALL()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 711
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "length(trim(_data)) > 0"

    const/4 v4, 0x0

    const-string v5, "title COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 715
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/db/VideoDB;->excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v6

    .line 716
    return-object v6
.end method

.method public getVideoCursorExcept(Lcom/sec/android/app/mv/player/common/PlaylistUtils;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pl"    # Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    .prologue
    .line 720
    const-string v0, "VideoDB"

    const-string v1, "getVideoCursorExcept E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const/4 v6, 0x0

    .line 722
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v9

    .line 724
    .local v9, "playlistSize":I
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    .line 725
    .local v10, "where":Ljava/lang/StringBuffer;
    const-string v0, "(length(trim(_data)) > 0) and ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 727
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v9, :cond_0

    .line 728
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id <> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v8}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 727
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 730
    :cond_0
    const-string v0, "1<>2 )"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 733
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoCursorExcept. where : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "title COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 744
    :goto_1
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/db/VideoDB;->excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v6

    .line 746
    return-object v6

    .line 739
    :catch_0
    move-exception v7

    .line 740
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getVideoCursorFolder()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 168
    const/4 v6, 0x0

    .line 170
    .local v6, "cursorVideo":Landroid/database/Cursor;
    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC, bucket_id COLLATE LOCALIZED ASC"

    .line 174
    .local v5, "orderByClause":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getFolderColumns()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "length(trim(_data)) > 0 AND _data not like \'/storage/sdcard0/cloudagent/cache%\'"

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 184
    if-nez v6, :cond_0

    move-object v0, v9

    .line 192
    :goto_0
    return-object v0

    .line 176
    :catch_0
    move-exception v7

    .line 177
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoCursorFolder - SQLiteException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v9

    .line 178
    goto :goto_0

    .line 179
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 180
    .local v8, "ex":Ljava/lang/UnsupportedOperationException;
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoCursorFolder - UnsupportedOperationException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v9

    .line 181
    goto :goto_0

    .line 186
    .end local v8    # "ex":Ljava/lang/UnsupportedOperationException;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 187
    const-string v0, "VideoDB"

    const-string v1, "getVideoCursorFolder - cursorVideo.getCount() <= 0"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 188
    goto :goto_0

    .line 191
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v6

    .line 192
    goto :goto_0
.end method

.method public getVideoCursorFolderByBucketId(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 10
    .param p1, "cursorFolder"    # Landroid/database/Cursor;

    .prologue
    .line 197
    const/4 v7, 0x0

    .line 198
    .local v7, "retCursor":Landroid/database/Cursor;
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 200
    :cond_0
    const-string v0, "bucket_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 201
    .local v1, "bucketId":I
    const/4 v9, 0x0

    .line 204
    .local v9, "tempCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, " limit 1"

    .line 205
    .local v5, "limitCount":Ljava/lang/String;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/mv/player/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 207
    if-eqz v9, :cond_1

    .line 208
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 209
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    if-nez v7, :cond_3

    .line 212
    move-object v7, v9

    .line 226
    .end local v5    # "limitCount":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    .end local v1    # "bucketId":I
    .end local v9    # "tempCursor":Landroid/database/Cursor;
    :cond_2
    return-object v7

    .line 214
    .restart local v1    # "bucketId":I
    .restart local v5    # "limitCount":Ljava/lang/String;
    .restart local v9    # "tempCursor":Landroid/database/Cursor;
    :cond_3
    :try_start_1
    new-instance v8, Landroid/database/MergeCursor;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v7, v0, v2

    const/4 v2, 0x1

    aput-object v9, v0, v2

    invoke-direct {v8, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .end local v7    # "retCursor":Landroid/database/Cursor;
    .local v8, "retCursor":Landroid/database/Cursor;
    move-object v7, v8

    .end local v8    # "retCursor":Landroid/database/Cursor;
    .restart local v7    # "retCursor":Landroid/database/Cursor;
    goto :goto_0

    .line 217
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 220
    .end local v5    # "limitCount":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 221
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 222
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    throw v0
.end method

.method public getVideoCursorIncludes(Lcom/sec/android/app/mv/player/common/PlaylistUtils;)Landroid/database/Cursor;
    .locals 11
    .param p1, "pl"    # Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    .prologue
    .line 750
    const-string v0, "VideoDB"

    const-string v1, "getVideoCursorIncludes E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const/4 v6, 0x0

    .line 752
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v9

    .line 754
    .local v9, "playlistSize":I
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    .line 755
    .local v10, "where":Ljava/lang/StringBuffer;
    const-string v0, "(length(trim(_data)) > 0) and ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 757
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v9, :cond_0

    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v8}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " or "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 757
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 760
    :cond_0
    const-string v0, "1 == 2 )"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 762
    const-string v0, "VideoDB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoCursorIncludes. where : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/app/mv/player/type/VideoListType;->getDefaultVideoColumns()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "title COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 773
    :goto_1
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/db/VideoDB;->excludeDRMFiles(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v6

    .line 775
    if-nez v6, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 777
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->removeDeletedIdsFromPlayList(Landroid/content/Context;)V

    .line 780
    :cond_3
    return-object v6

    .line 768
    :catch_0
    move-exception v7

    .line 769
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public isMediaFolder(I)Z
    .locals 8
    .param p1, "bucketID"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 453
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move v0, v7

    .line 469
    :goto_0
    return v0

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 459
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 460
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 461
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 462
    const/4 v6, 0x0

    .line 463
    const/4 v0, 0x1

    goto :goto_0

    .line 465
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 466
    const/4 v6, 0x0

    :cond_2
    move v0, v7

    .line 469
    goto :goto_0
.end method

.method public setContentDuration(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 554
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setContentDuration() - pos : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const/4 v0, 0x0

    .line 557
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 558
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "duration"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 561
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 566
    :goto_0
    return v0

    .line 562
    :catch_0
    move-exception v1

    .line 563
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition - Exception occured:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setResumePosition(Landroid/net/Uri;J)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "pos"    # J

    .prologue
    .line 538
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition() - pos : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const/4 v0, 0x0

    .line 541
    .local v0, "UpdateCnt":I
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 542
    .local v2, "newValues":Landroid/content/ContentValues;
    const-string v3, "resumePos"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 545
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 550
    :goto_0
    return v0

    .line 546
    :catch_0
    move-exception v1

    .line 547
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "VideoDB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setResumePosition - Exception occured:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updatePlayedState(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 570
    const/4 v0, 0x0

    .line 571
    .local v0, "UpdateCnt":I
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 572
    .local v3, "newValues":Landroid/content/ContentValues;
    const-string v5, "isPlayed"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 575
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 576
    .local v4, "path":Ljava/lang/String;
    const-string v2, ""

    .line 577
    .local v2, "newPath":Ljava/lang/String;
    const-string v5, "ss"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 578
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 581
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/db/VideoDB;->mResolver:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, p1, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 586
    :goto_0
    const-string v5, "VideoDB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updatePlayedState. return = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    return-void

    .line 582
    :catch_0
    move-exception v1

    .line 583
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "VideoDB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updatePlayedState - Exception occured:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
