.class public final Lcom/sec/android/app/mv/player/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Details_ID:I = 0x7f0d00a1

.field public static final Details_Value:I = 0x7f0d00a2

.field public static final EmptyText:I = 0x7f0d0039

.field public static final No_Item_Icon:I = 0x7f0d0038

.field public static final ProgressBar:I = 0x7f0d0024

.field public static final ResultEmpty:I = 0x7f0d0036

.field public static final TitleLayout:I = 0x7f0d0014

.field public static final VideoView:I = 0x7f0d00ab

.field public static final actionbar_layout:I = 0x7f0d0044

.field public static final addBtn:I = 0x7f0d0055

.field public static final addBtnImage:I = 0x7f0d0056

.field public static final animatedbrightness_icon:I = 0x7f0d0086

.field public static final auto_brightness_checkbox:I = 0x7f0d0060

.field public static final auto_brightness_text:I = 0x7f0d0061

.field public static final auto_brightness_text_level:I = 0x7f0d0064

.field public static final back_action:I = 0x7f0d0045

.field public static final back_arrow:I = 0x7f0d0046

.field public static final back_text:I = 0x7f0d0048

.field public static final background:I = 0x7f0d0017

.field public static final bottom_line:I = 0x7f0d002c

.field public static final brightness_gesture_img:I = 0x7f0d0085

.field public static final brightness_gesture_img_layout:I = 0x7f0d0084

.field public static final brightness_gesture_seekbar:I = 0x7f0d0088

.field public static final brightness_gesture_seekbar_layout:I = 0x7f0d0087

.field public static final brightness_gesture_text:I = 0x7f0d008a

.field public static final brightness_gesture_text_layout:I = 0x7f0d0089

.field public static final brightness_gesture_vertical:I = 0x7f0d0083

.field public static final brightness_level:I = 0x7f0d0062

.field public static final brightness_text:I = 0x7f0d0063

.field public static final cancelBtn:I = 0x7f0d005b

.field public static final cancelBtnText:I = 0x7f0d005c

.field public static final cbSubtitle:I = 0x7f0d00a0

.field public static final close_select:I = 0x7f0d004a

.field public static final controller_layout:I = 0x7f0d0069

.field public static final controller_layout_background:I = 0x7f0d006a

.field public static final counter_spinner:I = 0x7f0d004b

.field public static final ctrl_button:I = 0x7f0d006f

.field public static final ctrl_checkbox:I = 0x7f0d005f

.field public static final ctrl_playtime_and_buttons:I = 0x7f0d006b

.field public static final ctrl_playtime_text:I = 0x7f0d006c

.field public static final ctrl_progress:I = 0x7f0d0077

.field public static final ctrl_progressbar:I = 0x7f0d0076

.field public static final ctrl_vol_seekbar_layout_popup:I = 0x7f0d00b7

.field public static final ctrl_vol_text_layout2:I = 0x7f0d00b9

.field public static final ctrl_vol_vertical_popup:I = 0x7f0d00b6

.field public static final custom_done:I = 0x7f0d0005

.field public static final custom_layout:I = 0x7f0d0004

.field public static final custom_title:I = 0x7f0d0006

.field public static final deleteBtn:I = 0x7f0d0058

.field public static final deleteBtnImage:I = 0x7f0d0059

.field public static final device_list_popup:I = 0x7f0d0018

.field public static final device_volume_list:I = 0x7f0d0009

.field public static final device_volume_name:I = 0x7f0d000a

.field public static final device_volume_seekbar:I = 0x7f0d000b

.field public static final devicename:I = 0x7f0d0007

.field public static final devicepos:I = 0x7f0d0008

.field public static final doneBtn:I = 0x7f0d005d

.field public static final doneBtnText:I = 0x7f0d005e

.field public static final dropdown_text:I = 0x7f0d001e

.field public static final first_row_text:I = 0x7f0d0029

.field public static final fit_to_src_btn:I = 0x7f0d0070

.field public static final font_list:I = 0x7f0d0098

.field public static final font_listview:I = 0x7f0d0099

.field public static final gesture_layout:I = 0x7f0d0082

.field public static final gridview:I = 0x7f0d0035

.field public static final groupDivider:I = 0x7f0d0050

.field public static final group_count:I = 0x7f0d004f

.field public static final group_icon:I = 0x7f0d004e

.field public static final grouplayout:I = 0x7f0d004d

.field public static final help_end_text:I = 0x7f0d0011

.field public static final help_punch_view:I = 0x7f0d000c

.field public static final help_punch_view_image:I = 0x7f0d000d

.field public static final help_summary_arrow:I = 0x7f0d0010

.field public static final help_summary_text:I = 0x7f0d000f

.field public static final help_summary_view:I = 0x7f0d000e

.field public static final horizontal:I = 0x7f0d0000

.field public static final icon:I = 0x7f0d0047

.field public static final lastPlayedItem:I = 0x7f0d0027

.field public static final left_flick_1:I = 0x7f0d0079

.field public static final left_flick_2:I = 0x7f0d007a

.field public static final left_flick_3:I = 0x7f0d007b

.field public static final left_flick_layout:I = 0x7f0d0078

.field public static final left_sec:I = 0x7f0d007c

.field public static final list_counter:I = 0x7f0d001f

.field public static final list_counter_text:I = 0x7f0d0020

.field public static final list_end:I = 0x7f0d0019

.field public static final list_end_text:I = 0x7f0d001a

.field public static final list_select_layout:I = 0x7f0d0049

.field public static final listcheckbox:I = 0x7f0d002b

.field public static final listview:I = 0x7f0d0034

.field public static final lock_btn:I = 0x7f0d0092

.field public static final main_actionBar:I = 0x7f0d0013

.field public static final main_controller:I = 0x7f0d0016

.field public static final main_fragment_setting:I = 0x7f0d0015

.field public static final main_root:I = 0x7f0d0012

.field public static final menu_choice_add:I = 0x7f0d00bd

.field public static final menu_choice_cancel:I = 0x7f0d00bc

.field public static final menu_choice_delete_done:I = 0x7f0d00c0

.field public static final menu_choice_done:I = 0x7f0d00bb

.field public static final menu_choice_remove:I = 0x7f0d00be

.field public static final menu_selection_remove:I = 0x7f0d00bf

.field public static final minus:I = 0x7f0d00a4

.field public static final moreBtn:I = 0x7f0d0052

.field public static final moreDivider:I = 0x7f0d0053

.field public static final morelayout:I = 0x7f0d0051

.field public static final no_item_bg:I = 0x7f0d0037

.field public static final numbering:I = 0x7f0d003d

.field public static final numbering_portrait:I = 0x7f0d003c

.field public static final playerlist_add_layout:I = 0x7f0d0054

.field public static final playerlist_cancel_done_layout:I = 0x7f0d005a

.field public static final playerlist_delete_layout:I = 0x7f0d0057

.field public static final playlist_actionbar_layout:I = 0x7f0d0033

.field public static final playlist_relative_layout:I = 0x7f0d0021

.field public static final plus:I = 0x7f0d00a6

.field public static final popup:I = 0x7f0d00b5

.field public static final preview_arrow:I = 0x7f0d00ad

.field public static final preview_backgound:I = 0x7f0d00a9

.field public static final preview_current_time:I = 0x7f0d00ac

.field public static final progressBar_preview_seeked_bitmap:I = 0x7f0d0093

.field public static final progressBar_preview_show_time:I = 0x7f0d0094

.field public static final progressview:I = 0x7f0d0026

.field public static final relative_progress_preview:I = 0x7f0d00a7

.field public static final right_flick_1:I = 0x7f0d007e

.field public static final right_flick_2:I = 0x7f0d007f

.field public static final right_flick_3:I = 0x7f0d0080

.field public static final right_flick_layout:I = 0x7f0d007d

.field public static final right_sec:I = 0x7f0d0081

.field public static final root_view:I = 0x7f0d002e

.field public static final rotate_ctrl_icon:I = 0x7f0d00b1

.field public static final rotate_ctrl_layout:I = 0x7f0d00b0

.field public static final row_main_layout:I = 0x7f0d0022

.field public static final row_main_layout_list_item:I = 0x7f0d0023

.field public static final row_text_layout:I = 0x7f0d0028

.field public static final second_layout:I = 0x7f0d00af

.field public static final second_row_text:I = 0x7f0d002a

.field public static final seekbar:I = 0x7f0d00a5

.field public static final select_all_button:I = 0x7f0d0030

.field public static final select_all_button_layout:I = 0x7f0d002f

.field public static final select_all_check:I = 0x7f0d0031

.field public static final selected_subtitle_check:I = 0x7f0d0097

.field public static final setting_multivision_button2:I = 0x7f0d001d

.field public static final setting_playing_switch:I = 0x7f0d001c

.field public static final setting_singlevision_button2:I = 0x7f0d001b

.field public static final sharevideoPopupChecktext:I = 0x7f0d0042

.field public static final sharevideoexplain:I = 0x7f0d003e

.field public static final sharevideoimage:I = 0x7f0d003f

.field public static final sharevideopopupcheck:I = 0x7f0d0041

.field public static final sharevideotextlayout:I = 0x7f0d0040

.field public static final spinner_text:I = 0x7f0d002d

.field public static final subtitle:I = 0x7f0d009b

.field public static final subtitle_menu_list:I = 0x7f0d009e

.field public static final subtitle_name:I = 0x7f0d0095

.field public static final subtitle_onoff:I = 0x7f0d009f

.field public static final subtitle_path:I = 0x7f0d0096

.field public static final subtitle_portrait:I = 0x7f0d009a

.field public static final subtitle_preview:I = 0x7f0d009c

.field public static final subtitle_top_divider:I = 0x7f0d009d

.field public static final switchlayout:I = 0x7f0d004c

.field public static final synctext:I = 0x7f0d00a3

.field public static final text:I = 0x7f0d0002

.field public static final textView1:I = 0x7f0d0032

.field public static final text_and_image:I = 0x7f0d0003

.field public static final thumbnail:I = 0x7f0d0025

.field public static final time_current:I = 0x7f0d006d

.field public static final time_total:I = 0x7f0d006e

.field public static final title:I = 0x7f0d003a

.field public static final title_layout:I = 0x7f0d00ae

.field public static final title_text:I = 0x7f0d00b4

.field public static final title_text_layout:I = 0x7f0d00b3

.field public static final top_actionbar:I = 0x7f0d0043

.field public static final value:I = 0x7f0d003b

.field public static final vertical:I = 0x7f0d0001

.field public static final videocontroller_layout:I = 0x7f0d0065

.field public static final videocontroller_layout_sub:I = 0x7f0d0067

.field public static final videocontroller_progress:I = 0x7f0d0066

.field public static final videocontroller_progress_auto:I = 0x7f0d0068

.field public static final videoplayer_btn_ff:I = 0x7f0d0074

.field public static final videoplayer_btn_mv_playlist:I = 0x7f0d0075

.field public static final videoplayer_btn_pause:I = 0x7f0d0072

.field public static final videoplayer_btn_playerlist:I = 0x7f0d0071

.field public static final videoplayer_btn_rew:I = 0x7f0d0073

.field public static final vol_seekbar_popup:I = 0x7f0d00b8

.field public static final vol_text_popup:I = 0x7f0d00ba

.field public static final volume_btn:I = 0x7f0d00b2

.field public static final volume_gesture_img:I = 0x7f0d008d

.field public static final volume_gesture_img_layout:I = 0x7f0d008c

.field public static final volume_gesture_seekbar:I = 0x7f0d008f

.field public static final volume_gesture_seekbar_layout:I = 0x7f0d008e

.field public static final volume_gesture_text:I = 0x7f0d0091

.field public static final volume_gesture_text_layout:I = 0x7f0d0090

.field public static final volume_gesture_vertical:I = 0x7f0d008b

.field public static final vppp_content_area:I = 0x7f0d00a8

.field public static final vppp_surface_and_timetext:I = 0x7f0d00aa


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
