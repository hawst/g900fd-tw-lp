.class public Lcom/sec/android/app/mv/player/util/VideoGateUtil;
.super Ljava/lang/Object;
.source "VideoGateUtil.java"


# static fields
.field private static mGateEnabled:Z

.field private static mGateLcdtextEnabled:Z

.field private static mGateTraceTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    sput-boolean v1, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateEnabled:Z

    .line 7
    const-string v0, "GATE"

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateTraceTag:Ljava/lang/String;

    .line 9
    sput-boolean v1, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateLcdtextEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGateEnabled()Z
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateEnabled:Z

    return v0
.end method

.method public static getGateLcdtextEnabled()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateLcdtextEnabled:Z

    return v0
.end method

.method public static getGateTraceTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateTraceTag:Ljava/lang/String;

    return-object v0
.end method

.method public static setGateEnabled(Z)V
    .locals 0
    .param p0, "gateEnabled"    # Z

    .prologue
    .line 12
    sput-boolean p0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateEnabled:Z

    .line 13
    return-void
.end method

.method public static setGateLcdtextEnabled(Z)V
    .locals 0
    .param p0, "gateLcdtextEnabled"    # Z

    .prologue
    .line 20
    sput-boolean p0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateLcdtextEnabled:Z

    .line 21
    return-void
.end method

.method public static setGateTraceTag(Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 16
    sput-object p0, Lcom/sec/android/app/mv/player/util/VideoGateUtil;->mGateTraceTag:Ljava/lang/String;

    .line 17
    return-void
.end method
