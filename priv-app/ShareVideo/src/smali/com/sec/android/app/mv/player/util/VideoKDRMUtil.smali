.class public Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;
.super Ljava/lang/Object;
.source "VideoKDRMUtil.java"


# static fields
.field public static final DRM_BEFORE_DATE:I = -0xfffdff9

.field public static final DRM_CHECK_COUNT_POPUP:I = 0xb

.field public static final DRM_DATE_FILE:I = 0x2

.field public static final DRM_ERROR:I = -0x1

.field public static final DRM_EXIT_POPUP:I = 0xd

.field public static final DRM_EXPIRE_POPUP:I = 0xe

.field public static final DRM_INVALID_FLASH:I = -0xfffdff3

.field public static final DRM_LIMIT_DATE:I = -0xfffdffc

.field public static final DRM_MON_FILE:I = 0x1

.field public static final DRM_NEXT_POPUP:I = 0xf

.field public static final DRM_NONE_POPUP:I = 0xa

.field public static final DRM_NOT_EXIST_FILE:I = -0xfffdfff

.field public static final DRM_OUT_OF_COUNT:I = -0xfffdffa

.field public static final DRM_PARSER_ERROR:I = -0xfffdffe

.field public static final DRM_PREV_POPUP:I = 0x10

.field public static final DRM_REPLAY_POPUP:I = 0xc

.field public static final DRM_TYPE_INVALID_TERUTEN:I = 0x4

.field public static final DRM_TYPE_MAX:I = 0x5

.field public static final DRM_TYPE_NED:I = 0x2

.field public static final DRM_TYPE_NOMEDIA:I = 0x3

.field public static final DRM_TYPE_NORMAL:I = 0x0

.field public static final DRM_TYPE_TERUTEN:I = 0x1

.field public static final KDRM_SUCCESS:I

.field private static mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private static mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

.field private static mInstance:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "VideoKDRMUtil"

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method public static LmsLogClose(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 151
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 152
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LogLmsClose(I)V

    .line 154
    :cond_0
    return-void
.end method

.method public static LmsLogOpen()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 146
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-static {}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LogLmsOpen()V

    .line 148
    :cond_0
    return-void
.end method

.method public static LmsLogPlay(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 157
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 158
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LogLmsPlay(I)V

    .line 160
    :cond_0
    return-void
.end method

.method public static LmsLogStop(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 163
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 164
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-static {p0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->LogLmsStop(I)V

    .line 166
    :cond_0
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const-class v1, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;

    .line 59
    check-cast p0, Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .end local p0    # "context":Landroid/content/Context;
    sput-object p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 61
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mInstance:Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public checkKdrmFile()I
    .locals 6

    .prologue
    .line 76
    const/16 v0, 0xa

    .line 78
    .local v0, "retValue":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getTotalCount()I

    move-result v3

    .line 79
    .local v3, "totalCnt":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getDrmPlaybackStatus()I

    move-result v2

    .line 80
    .local v2, "status":I
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getDrmType()I

    move-result v4

    .line 82
    .local v4, "type":I
    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    move v1, v0

    .line 99
    .end local v0    # "retValue":I
    .local v1, "retValue":I
    :goto_0
    return v1

    .line 86
    .end local v1    # "retValue":I
    .restart local v0    # "retValue":I
    :cond_0
    const v5, -0xfffdffa

    if-eq v2, v5, :cond_1

    const v5, -0xfffdffc

    if-eq v2, v5, :cond_1

    const v5, -0xfffdfff

    if-eq v2, v5, :cond_1

    const v5, -0xfffdffe

    if-eq v2, v5, :cond_1

    const v5, -0xfffdff9

    if-eq v2, v5, :cond_1

    const v5, -0xfffdff3

    if-eq v2, v5, :cond_1

    const/4 v5, -0x1

    if-ne v2, v5, :cond_3

    .line 94
    :cond_1
    const/16 v0, 0xe

    :cond_2
    :goto_1
    move v1, v0

    .line 99
    .end local v0    # "retValue":I
    .restart local v1    # "retValue":I
    goto :goto_0

    .line 95
    .end local v1    # "retValue":I
    .restart local v0    # "retValue":I
    :cond_3
    if-lez v3, :cond_2

    .line 96
    const/16 v0, 0xb

    goto :goto_1
.end method

.method public checkMTPConnected()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getDrmType()I

    move-result v0

    .line 66
    .local v0, "type":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "mtp_running_status"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    if-ne v0, v1, :cond_1

    .line 68
    :cond_0
    const-string v3, "TAG"

    const-string v4, "MTP Connected"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0025

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 72
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public discountKDRM(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-static {p1}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->DiscountCall(Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method public displayLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 169
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "##################################"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM File path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDrmPlaybackStatus()= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :goto_0
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDRmType()          = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getAvailableCount     = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getAvailableCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getTotalCount         = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getTotalCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDisableCapture     = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDisableCapture()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDisableTVOut       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDisableTVOut()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getwatermarkFlag      = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getWatermarkFlag()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getWatermark().length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getWatermark()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getWaterMarkStr       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->getWaterMarkString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDRmType()          = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDrmPlaybackStatus()= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "##################################"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    return-void

    .line 175
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_OUT_OF_COUNT"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 178
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_LIMIT_DATE"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 181
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_NOT_EXIST_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 184
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_PARSER_ERROR"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_BEFORE_DATE"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_INVALID_FLASH"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 193
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_ERROR"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_MON_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 199
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_DATE_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NORMAL"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 212
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_TERUTEN"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 215
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NED"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 218
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NOMEDIA"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 221
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_INVALID_TERUTEN"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 224
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_MAX"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 173
    nop

    :sswitch_data_0
    .sparse-switch
        -0xfffdfff -> :sswitch_2
        -0xfffdffe -> :sswitch_3
        -0xfffdffc -> :sswitch_1
        -0xfffdffa -> :sswitch_0
        -0xfffdff9 -> :sswitch_4
        -0xfffdff3 -> :sswitch_5
        -0x1 -> :sswitch_6
        0x1 -> :sswitch_7
        0x2 -> :sswitch_8
    .end sparse-switch

    .line 207
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getAvailableCount()I
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getAvailableCount()I

    move-result v0

    return v0
.end method

.method public getDisableCapture()Z
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDisableCapture()Z

    move-result v0

    return v0
.end method

.method public getDisableTVOut()Z
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDisableTVOut()Z

    move-result v0

    return v0
.end method

.method public getDrmPlaybackStatus()I
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v0

    return v0
.end method

.method public getDrmType()I
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getTotalCount()I

    move-result v0

    return v0
.end method

.method public getWaterMarkString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "waterMarkStr":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getWatermarkFlag()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    new-instance v0, Ljava/lang/String;

    .end local v0    # "waterMarkStr":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;->getWatermark()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 140
    .restart local v0    # "waterMarkStr":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public roadKDRMInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->mDrmInfo:Lcom/sec/android/app/mv/player/util/DrmKDRMInfo;

    .line 104
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;->displayLog(Ljava/lang/String;)V

    .line 105
    return-void
.end method
