.class public Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;
.super Ljava/lang/Object;
.source "TypefaceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefacesClass;,
        Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefaceSortByName;
    }
.end annotation


# static fields
.field public static final DEFAULT_FONT_VALUE:Ljava/lang/String; = "default"

.field private static final FONT_ASSET_DIR:Ljava/lang/String; = "xml"

.field private static final FONT_DIRECTORY:Ljava/lang/String; = "fonts/"

.field private static final FONT_EXTENSION:Ljava/lang/String; = ".ttf"

.field private static final TAG:Ljava/lang/String; = "TypefaceFinder"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mTypefaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/flipfont/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    .line 68
    return-void
.end method


# virtual methods
.method public clearTypfaceArray()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 121
    :cond_0
    return-void
.end method

.method public findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 7
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 75
    const/4 v3, 0x0

    .line 77
    .local v3, "xmlfiles":[Ljava/lang/String;
    :try_start_0
    const-string v4, "xml"

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 82
    const/4 v1, 0x0

    .line 83
    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 85
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xml/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 86
    .local v2, "in":Ljava/io/InputStream;
    aget-object v4, v3, v1

    invoke-virtual {p0, v4, v2, p2}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 91
    .end local v2    # "in":Ljava/io/InputStream;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 79
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_2
    return v4

    .line 87
    .restart local v1    # "i":I
    :catch_1
    move-exception v0

    .line 89
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v4, "TypefaceFinder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Not possible to open, continue to next file, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 93
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_0
    const/4 v4, 0x1

    goto :goto_2
.end method

.method public getSansEntries(Landroid/content/pm/PackageManager;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 16
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "entries"    # Ljava/util/Vector;
    .param p3, "entryValues"    # Ljava/util/Vector;
    .param p4, "fontPackageName"    # Ljava/util/Vector;

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0048

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 138
    const-string v13, "default"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v13, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v13}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 141
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    new-instance v14, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefaceSortByName;

    invoke-direct {v14}, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder$TypefaceSortByName;-><init>()V

    invoke-static {v13, v14}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 142
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-ge v7, v13, :cond_5

    .line 143
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/mv/player/flipfont/Typeface;

    invoke-virtual {v13}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->getSansName()Ljava/lang/String;

    move-result-object v11

    .line 144
    .local v11, "s":Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 147
    .local v1, "aManager":Landroid/content/res/AssetManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/mv/player/flipfont/Typeface;

    invoke-virtual {v13}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->getTypefaceFilename()Ljava/lang/String;

    move-result-object v6

    .line 148
    .local v6, "fontName":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 149
    const-string v13, "TypefaceFinder"

    const-string v14, "getSansEntries :: fontName is null"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v1    # "aManager":Landroid/content/res/AssetManager;
    .end local v6    # "fontName":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 153
    .restart local v1    # "aManager":Landroid/content/res/AssetManager;
    .restart local v6    # "fontName":Ljava/lang/String;
    :cond_1
    const/16 v13, 0x2f

    invoke-virtual {v6, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    .line 154
    .local v12, "start_pos":I
    const/16 v13, 0x2e

    invoke-virtual {v6, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 155
    .local v4, "end_pos":I
    if-gez v4, :cond_2

    .line 156
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    .line 158
    :cond_2
    add-int/lit8 v13, v12, 0x1

    invoke-virtual {v6, v13, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 159
    .local v8, "loadTypeface":Ljava/lang/String;
    const-string v13, " "

    const-string v14, ""

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 161
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/mv/player/flipfont/Typeface;

    invoke-virtual {v13}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->getFontPackageName()Ljava/lang/String;

    move-result-object v9

    .line 162
    .local v9, "packageName":Ljava/lang/String;
    if-nez v9, :cond_3

    .line 163
    const-string v13, "TypefaceFinder"

    const-string v14, "getSansEntries :: packageName is null"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 168
    :cond_3
    const/16 v13, 0x80

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 169
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v13, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v13, v2, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 170
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v10

    .line 172
    .local v10, "res":Landroid/content/res/Resources;
    invoke-virtual {v10}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    .line 179
    .local v5, "fontAssetManager":Landroid/content/res/AssetManager;
    const-string v13, "com.monotype.android.font.droidserifitalic"

    invoke-virtual {v9, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 183
    sget-boolean v13, Lcom/sec/android/app/mv/player/common/feature/Feature;->C_KOREA:Z

    if-eqz v13, :cond_4

    .line 184
    const-string v13, "com.monotype.android.font.cooljazz"

    invoke-virtual {v9, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    .line 189
    :cond_4
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 190
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 191
    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 192
    .end local v2    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "fontAssetManager":Landroid/content/res/AssetManager;
    .end local v10    # "res":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 193
    .local v3, "e":Ljava/lang/Exception;
    const-string v13, "TypefaceFinder"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSansEntries - Typeface.createFromAsset caused an exception for - fonts/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".ttf"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 200
    .end local v1    # "aManager":Landroid/content/res/AssetManager;
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "end_pos":I
    .end local v6    # "fontName":Ljava/lang/String;
    .end local v8    # "loadTypeface":Ljava/lang/String;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v11    # "s":Ljava/lang/String;
    .end local v12    # "start_pos":I
    :cond_5
    return-void
.end method

.method public parseTypefaceXml(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 9
    .param p1, "xmlFilename"    # Ljava/lang/String;
    .param p2, "inStream"    # Ljava/io/InputStream;
    .param p3, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 101
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v4

    .line 102
    .local v4, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 103
    .local v3, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v5

    .line 104
    .local v5, "xr":Lorg/xml/sax/XMLReader;
    new-instance v1, Lcom/sec/android/app/mv/player/flipfont/TypefaceParser;

    invoke-direct {v1}, Lcom/sec/android/app/mv/player/flipfont/TypefaceParser;-><init>()V

    .line 105
    .local v1, "fontParser":Lcom/sec/android/app/mv/player/flipfont/TypefaceParser;
    invoke-interface {v5, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 106
    new-instance v6, Lorg/xml/sax/InputSource;

    invoke-direct {v6, p2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v5, v6}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 107
    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/flipfont/TypefaceParser;->getParsedData()Lcom/sec/android/app/mv/player/flipfont/Typeface;

    move-result-object v2

    .line 108
    .local v2, "newTypeface":Lcom/sec/android/app/mv/player/flipfont/Typeface;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->setTypefaceFilename(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v2, p3}, Lcom/sec/android/app/mv/player/flipfont/Typeface;->setFontPackageName(Ljava/lang/String;)V

    .line 110
    iget-object v6, p0, Lcom/sec/android/app/mv/player/flipfont/TypefaceFinder;->mTypefaces:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v1    # "fontParser":Lcom/sec/android/app/mv/player/flipfont/TypefaceParser;
    .end local v2    # "newTypeface":Lcom/sec/android/app/mv/player/flipfont/Typeface;
    .end local v3    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v4    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v5    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "TypefaceFinder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File parsing is not possible, omit this typeface, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
