.class Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;
.super Ljava/lang/Object;
.source "PlaylistView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/playlist/PlaylistView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x3

    const/4 v4, 0x1

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistType:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$000(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)I

    move-result v3

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    iget v3, v3, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mPlaylistAttr:I

    if-ne v3, v4, :cond_4

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v3

    const/4 v5, 0x2

    if-ge v3, v5, :cond_0

    .line 159
    const/4 v3, 0x0

    .line 180
    :goto_0
    return v3

    .line 160
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistItem;

    .line 161
    .local v2, "item":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListView()Lcom/sec/android/app/mv/player/playlist/PlayerListView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/playlist/PlayerListView;->setPlayerListType(I)V

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->showPlayerListBtn(I)V

    .line 164
    const v3, 0x7f0d002b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 165
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 167
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_2

    .line 168
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v3

    if-ne v3, p3, :cond_1

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 168
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 174
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-direct {v5, p3, v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;-><init>(ILcom/sec/android/app/mv/player/type/PlaylistItem;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$300(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mVideoActionBar:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    iget-object v5, p0, Lcom/sec/android/app/mv/player/playlist/PlaylistView$2;->this$0:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    # getter for: Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mSelectedList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->access$400(Lcom/sec/android/app/mv/player/playlist/PlaylistView;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->setCounterSpinnerText(I)V

    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v2    # "item":Lcom/sec/android/app/mv/player/type/PlaylistItem;
    :cond_4
    move v3, v4

    .line 180
    goto/16 :goto_0
.end method
