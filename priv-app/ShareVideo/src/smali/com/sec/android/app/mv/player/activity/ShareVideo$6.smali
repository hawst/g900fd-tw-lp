.class Lcom/sec/android/app/mv/player/activity/ShareVideo$6;
.super Landroid/content/BroadcastReceiver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 1571
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1573
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1574
    .local v0, "action":Ljava/lang/String;
    const-string v1, "ShareVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSPenReceiver. action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1576
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-nez v1, :cond_1

    .line 1596
    :cond_0
    :goto_0
    return-void

    .line 1580
    :cond_1
    sget-object v1, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1581
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1582
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PLAY:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    .line 1586
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    goto :goto_0

    .line 1584
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PAUSE:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    goto :goto_1

    .line 1587
    :cond_3
    sget-object v1, Lcom/sec/android/app/mv/player/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1588
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PLAY:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    if-ne v1, v2, :cond_4

    .line 1589
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->start()V

    .line 1590
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PLAY:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    goto :goto_0

    .line 1591
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$800(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PAUSE:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    if-ne v1, v2, :cond_0

    .line 1592
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # invokes: Lcom/sec/android/app/mv/player/activity/ShareVideo;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V

    .line 1593
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$6;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    sget-object v2, Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;->PAUSE:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    # setter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mPrevPlayerState:Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$802(Lcom/sec/android/app/mv/player/activity/ShareVideo;Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;)Lcom/sec/android/app/mv/player/activity/ShareVideo$ePlayerStateType;

    goto :goto_0
.end method
