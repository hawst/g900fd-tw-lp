.class public interface abstract Lcom/sec/android/app/mv/player/common/SUtils$OnMessageListener;
.super Ljava/lang/Object;
.source "SUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/common/SUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnMessageListener"
.end annotation


# virtual methods
.method public abstract onCurrentContentStop()V
.end method

.method public abstract onMultiVisionDeviceDisconnected(IILjava/lang/String;)V
.end method

.method public abstract onNeedMultiVisionView(II)V
.end method

.method public abstract onOpenPath(Ljava/lang/String;)V
.end method

.method public abstract onSendVolumeValue(I)V
.end method

.method public abstract onSetScreenRotation(I)V
.end method

.method public abstract onSetVolumeMute()V
.end method

.method public abstract onSetVolumeOn()V
.end method

.method public abstract onStartShareVideo()V
.end method

.method public abstract onStopShareVideo(I)V
.end method
