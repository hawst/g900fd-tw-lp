.class public Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;
.super Ljava/lang/Object;
.source "VideoProgressBarScrubbing.java"


# static fields
.field public static final HALF_SPEED_SCRUBBING:I = 0x2

.field public static final HI_SPEED_SCRUBBING:I = 0x1

.field public static final QUARTER_SPEED_SCRUBBING:I = 0x4

.field private static mScrubbingSpeedInfoToast:Landroid/widget/Toast;


# instance fields
.field private final HALF_SPEED_SCRUBBING_HEIGHT:F

.field private mComparedProgress:I

.field private mContext:Landroid/content/Context;

.field private mSavedProgress:I

.field private mSavedScrubbingSpeed:I

.field private mScrubbingSpeed:I

.field private mStartedProgress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v1, -0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v3, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    .line 20
    iput v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    .line 21
    iput v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    .line 22
    iput v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 29
    const/high16 v1, 0x42a00000    # 80.0f

    iput v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->HALF_SPEED_SCRUBBING_HEIGHT:F

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    .line 33
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 34
    .local v0, "height":I
    iget-object v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    const v2, 0x7f0a003c

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    .line 35
    sget-object v1, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    const/16 v2, 0x51

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 36
    return-void
.end method


# virtual methods
.method public cancelScrubbingSpeedInfoToast()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 44
    return-void
.end method

.method public reprocessProgress(I)I
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    iget v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    if-eq v0, v1, :cond_0

    .line 83
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    iput v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    .line 84
    iput p1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    .line 86
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    iput v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 87
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    packed-switch v0, :pswitch_data_0

    .line 97
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    .line 98
    return p1

    .line 89
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    add-int p1, v0, v1

    .line 90
    goto :goto_0

    .line 92
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x4

    add-int p1, v0, v1

    .line 93
    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 48
    iget-object v4, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v3, v4, Landroid/util/DisplayMetrics;->density:F

    .line 49
    .local v3, "scale":F
    const/high16 v4, 0x42a00000    # 80.0f

    mul-float/2addr v4, v3

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 51
    .local v0, "heightOfHalf":I
    sget-object v4, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    const v5, 0x7f0a003c

    invoke-virtual {v4, v5}, Landroid/widget/Toast;->setText(I)V

    .line 53
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    .line 54
    .local v1, "nTouchPointY":I
    const/4 v2, 0x1

    .line 56
    .local v2, "returnValue":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int v4, v1, v4

    if-lez v4, :cond_1

    .line 76
    :cond_0
    :goto_0
    iput v2, p0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    .line 78
    return v2

    .line 66
    :cond_1
    if-gez v1, :cond_0

    .line 68
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v0, :cond_2

    .line 69
    sget-object v4, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    const v5, 0x7f0a0077

    invoke-virtual {v4, v5}, Landroid/widget/Toast;->setText(I)V

    .line 70
    const/4 v2, 0x4

    goto :goto_0

    .line 72
    :cond_2
    sget-object v4, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    const v5, 0x7f0a0033

    invoke-virtual {v4, v5}, Landroid/widget/Toast;->setText(I)V

    .line 73
    const/4 v2, 0x2

    goto :goto_0
.end method

.method public showScrubbingSpeedInfoToast()V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/mv/player/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 40
    return-void
.end method
