.class Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 291
    const-string v0, "VideoBrightnessDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNormalDialog() - isChecked:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    if-eqz p2, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Z)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->toggleBrightProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1000(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 304
    :goto_0
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$100(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getDialogFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    .line 316
    return-void

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Z)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1100(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;->this$0:Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->toggleBrightProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->access$1000(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    goto/16 :goto_0
.end method
