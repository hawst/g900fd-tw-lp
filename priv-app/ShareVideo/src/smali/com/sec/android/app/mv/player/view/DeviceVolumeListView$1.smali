.class Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;
.super Ljava/lang/Object;
.source "DeviceVolumeListView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "dialog"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)I

    move-result v3

    const/4 v6, -0x1

    if-ne v3, v6, :cond_0

    .line 53
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # setter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I
    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$002(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;I)I

    .line 56
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    .line 57
    .local v1, "seekBar":Landroid/widget/SeekBar;
    const/4 v2, 0x0

    .line 58
    .local v2, "volume":I
    if-eqz v1, :cond_1

    .line 59
    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 63
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    iget-object v3, v3, Lcom/sec/android/app/mv/player/activity/ShareVideo;->mTitleController:Lcom/sec/android/app/mv/player/view/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/view/VideoTitleController;->hideVolumeMVBarPopup()V

    .line 65
    sparse-switch p2, :sswitch_data_0

    move v3, v5

    .line 106
    .end local v1    # "seekBar":Landroid/widget/SeekBar;
    .end local v2    # "volume":I
    :goto_0
    return v3

    .line 67
    .restart local v1    # "seekBar":Landroid/widget/SeekBar;
    .restart local v2    # "volume":I
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V

    .line 69
    if-eqz v1, :cond_2

    .line 70
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_2
    move v3, v4

    .line 73
    goto :goto_0

    .line 76
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_3

    .line 77
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mIndex:I
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)I

    move-result v5

    add-int/lit8 v6, v2, -0x1

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V

    .line 78
    if-eqz v1, :cond_3

    .line 79
    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_3
    move v3, v4

    .line 82
    goto :goto_0

    .line 87
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_4

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v3

    const/4 v5, 0x0

    add-int/lit8 v6, v2, 0x1

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V

    :cond_4
    move v3, v4

    .line 90
    goto :goto_0

    .line 95
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_5

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->mVolumeObject:Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->access$200(Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;)Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;

    move-result-object v3

    const/4 v5, 0x0

    add-int/lit8 v6, v2, -0x1

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView$VolumeObject;->setVolumeProgress(IIZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    move v3, v4

    .line 98
    goto :goto_0

    .line 103
    .end local v1    # "seekBar":Landroid/widget/SeekBar;
    .end local v2    # "volume":I
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/app/mv/player/view/DeviceVolumeListView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 106
    goto/16 :goto_0

    .line 65
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x18 -> :sswitch_2
        0x19 -> :sswitch_3
        0x117 -> :sswitch_3
        0x118 -> :sswitch_2
    .end sparse-switch
.end method
