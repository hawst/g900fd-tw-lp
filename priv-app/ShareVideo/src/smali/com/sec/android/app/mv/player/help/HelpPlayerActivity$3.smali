.class Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;
.super Ljava/lang/Object;
.source "HelpPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->initializeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 288
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpStep:Lcom/sec/android/app/mv/player/help/HelpStep;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$100(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/help/HelpStep;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/help/HelpStep;->getCurrentStep()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$400(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogAdjustVolume:Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$400(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->dismiss()V

    .line 293
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "help_volume_dialog"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    .line 294
    .local v0, "dFragment":Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
    if-nez v0, :cond_1

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->mHelpDialogFragmentVolume:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->access$500(Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DialogFragment;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity$3;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "help_volume_dialog"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v0    # "dFragment":Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
    :cond_1
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
