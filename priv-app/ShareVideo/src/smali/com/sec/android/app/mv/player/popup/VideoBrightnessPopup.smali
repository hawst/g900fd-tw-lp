.class public Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Lcom/sec/android/app/mv/player/popup/IVideoPopup;


# static fields
.field private static final ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VideoBrightnessDialog"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mAutoBrightnessText:Landroid/widget/TextView;

.field private mAutoCheckbox:Landroid/widget/CheckBox;

.field mAutoValText:Landroid/widget/TextView;

.field private mBrightProgressBar:Landroid/widget/SeekBar;

.field private mBrightProgressBarAuto:Landroid/widget/SeekBar;

.field private mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

.field private mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

.field mBrightnessText:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mObserverBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightnessMode:Landroid/database/ContentObserver;

.field mRoot:Landroid/view/View;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mWindow:Landroid/view/Window;

.field private mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .param p2, "bUtil"    # Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    .line 108
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 118
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 350
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$8;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 376
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$9;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 433
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$10;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 59
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    .line 60
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 62
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;

    .line 63
    iput-object p2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->show()V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->toggleBrightProgressBar()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->revertBrightness()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    return-object v0
.end method

.method private registerContentObserver()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 164
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver E"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-nez v2, :cond_0

    .line 166
    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$3;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 193
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "screen_brightness_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 194
    .local v1, "tmp":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 195
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightnessMode is now registered"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "tmp":Landroid/net/Uri;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    if-nez v2, :cond_1

    .line 201
    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$4;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$4;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 208
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v2, "screen_brightness"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 209
    .restart local v1    # "tmp":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 210
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightness is now registered"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "tmp":Landroid/net/Uri;
    :goto_1
    return-void

    .line 197
    :cond_0
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightnessMode already exists"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_1
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightness already exists"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private revertBrightness()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->revertBrightness(Landroid/content/Context;)V

    .line 402
    :cond_0
    return-void
.end method

.method private setAutoBrightnessDetailLevelText()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getAutoBrightDetailTextString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    :cond_0
    return-void
.end method

.method private setBrightProgressBarEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 410
    move v0, p1

    .line 411
    .local v0, "val":Z
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_0

    .line 412
    const/4 v0, 0x1

    .line 414
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 415
    return-void
.end method

.method private setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V
    .locals 6
    .param p1, "dialogBuilder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 236
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0061

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0064

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0066

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0060

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    .line 243
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0062

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0063

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    .line 246
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_4

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 253
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightnessSupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_8

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->savePrevSystemBrightnessValuesInLocal(Landroid/content/Context;)V

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 263
    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 264
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_2

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 267
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v1, :cond_2

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a0014

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 283
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a000c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 284
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$5;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_7

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v1, :cond_6

    .line 348
    :cond_3
    :goto_2
    return-void

    .line 249
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 272
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 273
    invoke-direct {p0, v5}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 274
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 277
    sget-boolean v1, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a0015

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 321
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 328
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_8

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-eqz v1, :cond_3

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 332
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 336
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 338
    const v1, 0x7f0a0023

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$6;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 343
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup$7;-><init>(Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2
.end method

.method private show()V
    .locals 3

    .prologue
    .line 68
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "show()!"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v2, "VideoBrightnessDialog"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 71
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    const v1, 0x7f0a0013

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 75
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->registerContentObserver()V

    .line 77
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 81
    return-void
.end method

.method private toggleBrightProgressBar()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 418
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->G_SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 421
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v1, 0x7f0a0014

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 427
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v1, 0x7f0a0015

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private unregisterContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 217
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 220
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 222
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 223
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver - mObserverBrightnessMode is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 228
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 229
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver - mObserverBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_1
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 94
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->unregisterContentObserver()V

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->revertBrightness()V

    .line 89
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
