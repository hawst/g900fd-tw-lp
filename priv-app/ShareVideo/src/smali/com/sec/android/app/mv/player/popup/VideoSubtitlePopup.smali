.class public Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.super Ljava/lang/Object;
.source "VideoSubtitlePopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/sec/android/app/mv/player/popup/IVideoPopup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;,
        Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;,
        Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;,
        Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeFontListTask;,
        Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;
    }
.end annotation


# static fields
.field private static final ID:I = 0x1

.field private static final MENU_TITLE:Ljava/lang/String; = "MENU_TITLE"

.field private static final MENU_VALUE:Ljava/lang/String; = "MENU_VALUE"

.field private static final TAG:Ljava/lang/String; = "VideoSubtitlePopup"

.field private static final TEXT_LARGE:I = 0x0

.field private static final TEXT_MEDEUM:I = 0x1

.field private static final TEXT_SMALL:I = 0x2

.field private static final VIDEO_SUBTITLE_MENU_ACTIVATION:I = 0x0

.field private static final VIDEO_SUBTITLE_MENU_BACKGROUND_COLOUR:I = 0x7

.field public static final VIDEO_SUBTITLE_MENU_FONT_COLOUR:I = 0x5


# instance fields
.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mContext:Landroid/content/Context;

.field private mFileFilter:Ljava/io/FilenameFilter;

.field private mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

.field private mInvaildSubtitle:Z

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mSelectSubtitleDialog:Landroid/app/AlertDialog;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mShowingSubtitleFontPopup:Z

.field private mSubDialogs:Landroid/app/AlertDialog;

.field private mSubFilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mSubtitleActive:Z

.field private mSubtitleBGColor:I

.field private mSubtitleFile:Ljava/lang/String;

.field private mSubtitleFilePath:Ljava/lang/String;

.field private mSubtitleFont:Ljava/lang/String;

.field private mSubtitleFontColor:I

.field private mSubtitleFontDialog:Landroid/app/AlertDialog;

.field private mSubtitleFontSize:I

.field private mSubtitleLangArray:[Z

.field private mSubtitleLangDialog:Landroid/app/AlertDialog;

.field private mSubtitleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

.field private mSubtitleMenuDialog:Landroid/app/AlertDialog;

.field private mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

.field private mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

.field private mSubtitleSyncTime:I

.field private mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

.field private mSubtitleView:Landroid/view/View;

.field private mToastSelectAtLeastOne:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 3
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleActive:Z

    .line 94
    iput v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncTime:I

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFont:Ljava/lang/String;

    .line 98
    iput v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontColor:I

    .line 99
    iput v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontSize:I

    .line 100
    iput v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleBGColor:I

    .line 101
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    .line 103
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mToastSelectAtLeastOne:Landroid/widget/Toast;

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 117
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    .line 1338
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$26;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 1377
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$27;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 121
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "VideoSubtitlePopup"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    .line 123
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitleValues()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createShowInvalidSubtitle()V

    .line 138
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleMenu()V

    goto :goto_0

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSelectSubtitlePopup()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->revertSubtitleSettings()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleTextColorPopup()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleSizePopup()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleBackgroundColorPopup()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Lcom/sec/android/app/mv/player/view/VideoStateView;)Lcom/sec/android/app/mv/player/view/VideoStateView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Lcom/sec/android/app/mv/player/view/VideoStateView;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->refreshSubtitleMenu()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->makeFontList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->sendSubtitle()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSubtitleFontPopup()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;[Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # [Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->setSelectSubtitleLang([Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2600(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->existFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mInvaildSubtitle:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->callSelectSubtitlePopup()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitleValues()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->makeSubtitleFilesList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/view/VideoStateView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/io/FilenameFilter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFileFilter:Ljava/io/FilenameFilter;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Lcom/sec/android/app/mv/player/util/SubtitleUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->clickSync()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSelectSubtitlePopup()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleLanguagePopup()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleFontPopup()V

    return-void
.end method

.method private callSelectSubtitlePopup()V
    .locals 6

    .prologue
    .line 1057
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "callSelectSubtitlePopup()"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1059
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0a0081

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1061
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 1062
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1063
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    :cond_0
    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    new-instance v3, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$22;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1106
    :cond_1
    const v2, 0x7f0a0016

    new-instance v3, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$23;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1118
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1119
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    .line 1120
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;

    invoke-direct {v3, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$24;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1135
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1136
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSelectSubtitleDialog:Landroid/app/AlertDialog;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1143
    :goto_0
    return-void

    .line 1137
    :catch_0
    move-exception v1

    .line 1138
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "Subtitle Dialog :: NullPointerException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1139
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 1140
    .local v1, "e":Landroid/view/WindowManager$BadTokenException;
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "Subtitle Dialog :: BadTokenException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private callSubtitleFontPopup()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 756
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedFontIndex()I

    move-result v6

    .line 757
    .local v6, "selected":I
    const-string v8, "VideoSubtitlePopup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "callSubtitleFontPopup() :: selected = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontList()[Ljava/lang/String;

    move-result-object v3

    .line 761
    .local v3, "fonts":[Ljava/lang/String;
    if-nez v3, :cond_0

    .line 762
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    .line 823
    :goto_0
    return-void

    .line 766
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 767
    .local v0, "fontArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 768
    new-instance v7, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v9, 0x7f03001d

    invoke-direct {v7, p0, v8, v9, v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 771
    .local v7, "subtitleFontAdapter":Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleFontAdapter;
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v5, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 772
    .local v5, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v9, 0x7f0a0030

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 774
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 776
    .local v4, "inflate":Landroid/view/LayoutInflater;
    const v8, 0x7f03001e

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 777
    .local v2, "fontView":Landroid/view/View;
    const v8, 0x7f0d0099

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 779
    .local v1, "fontListView":Landroid/widget/ListView;
    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 780
    invoke-virtual {v1, v6, v11}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 782
    new-instance v8, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;

    invoke-direct {v8, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$11;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 799
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 801
    const v8, 0x7f0a0016

    new-instance v9, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$12;

    invoke-direct {v9, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$12;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 808
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    .line 809
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8, v11}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 810
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 812
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 814
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    new-instance v9, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$13;

    invoke-direct {v9, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$13;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 822
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    iput-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method private clickSync()V
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 565
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->createSubtitleSyncPopup()V

    .line 566
    return-void
.end method

.method private createSelectSubtitlePopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1195
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-nez v0, :cond_0

    .line 1197
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 1201
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    .line 1202
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeSubtitleListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1203
    return-void
.end method

.method private createShowInvalidSubtitle()V
    .locals 3

    .prologue
    .line 1032
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleInvalid()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v2, "VideoSubtitlePopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 1035
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1036
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a0045

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1037
    const v1, 0x7f0a0018

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1039
    const v1, 0x7f0a0060

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$20;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1046
    new-instance v1, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$21;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1053
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1054
    return-void
.end method

.method private createSubtitleBackgroundColorPopup()V
    .locals 6

    .prologue
    .line 852
    const/4 v2, 0x0

    .line 853
    .local v2, "selected":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getBackgroundColor()I

    move-result v0

    .line 855
    .local v0, "color":I
    const/high16 v4, -0x1000000

    if-ne v0, v4, :cond_1

    .line 856
    const/4 v2, 0x0

    .line 862
    :cond_0
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 863
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a008e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 864
    const v4, 0x7f0a0016

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$14;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$14;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 870
    const v4, 0x7f060004

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$15;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$15;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v1, v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 895
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 896
    .local v3, "subtitleBackgroundColorDialog":Landroid/app/AlertDialog;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 897
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 899
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 901
    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$16;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$16;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 908
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 909
    return-void

    .line 857
    .end local v1    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v3    # "subtitleBackgroundColorDialog":Landroid/app/AlertDialog;
    :cond_1
    const/4 v4, -0x1

    if-ne v0, v4, :cond_2

    .line 858
    const/4 v2, 0x1

    goto :goto_0

    .line 859
    :cond_2
    if-nez v0, :cond_0

    .line 860
    const/4 v2, 0x2

    goto :goto_0
.end method

.method private createSubtitleFontPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 710
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    if-eqz v0, :cond_1

    .line 711
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "createSubtitleFontPopup() :: mShowingSubtitleFontPopup is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    if-nez v0, :cond_2

    .line 718
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFontStateView:Lcom/sec/android/app/mv/player/view/VideoStateView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/VideoStateView;->setVisibility(I)V

    .line 722
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeFontListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeFontListTask;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$MakeFontListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private createSubtitleLanguagePopup()V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v12, 0x0

    .line 915
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-nez v8, :cond_0

    .line 916
    const-string v8, "VideoSubtitlePopup"

    const-string v9, "craeteSubtitleLanguagePopup() - mSubtitleUtil is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :goto_0
    return-void

    .line 921
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0080

    invoke-static {v8, v10, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mToastSelectAtLeastOne:Landroid/widget/Toast;

    .line 923
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v4, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 924
    .local v4, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0040

    invoke-virtual {v8, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 926
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v3

    .line 927
    .local v3, "length":I
    const-string v8, "VideoSubtitlePopup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createSubtitleLanguagePopup. LanguageArray.size() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const/4 v7, 0x0

    .line 929
    .local v7, "tempSelectIndex":[Z
    if-lez v3, :cond_1

    .line 930
    new-array v7, v3, [Z

    .line 932
    :cond_1
    move-object v5, v7

    .line 933
    .local v5, "select_index":[Z
    if-nez v3, :cond_3

    move v8, v9

    :goto_1
    new-array v2, v8, [Ljava/lang/String;

    .line 935
    .local v2, "la":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v3, :cond_5

    .line 936
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v0

    .line 937
    .local v0, "LanguageType":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->getLanguageTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v1

    .line 939
    const-string v8, "VideoSubtitlePopup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createSubtitleLanguagePopup.  array["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] LanguageType : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 942
    aput-boolean v12, v5, v1

    .line 935
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "LanguageType":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "la":[Ljava/lang/String;
    :cond_3
    move v8, v3

    .line 933
    goto :goto_1

    .line 944
    .restart local v0    # "LanguageType":Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v2    # "la":[Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v8

    if-ne v1, v8, :cond_2

    .line 945
    aput-boolean v9, v5, v1

    goto :goto_3

    .line 950
    .end local v0    # "LanguageType":Ljava/lang/String;
    :cond_5
    if-lez v3, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v8

    if-eqz v8, :cond_6

    .line 951
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v8

    array-length v6, v8

    .line 952
    .local v6, "subtitleIndex":I
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v6, :cond_6

    .line 953
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v8

    aget v8, v8, v1

    aput-boolean v9, v5, v8

    .line 952
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 957
    .end local v6    # "subtitleIndex":I
    :cond_6
    if-nez v3, :cond_7

    .line 958
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0093

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v12

    .line 959
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v8, v12}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 963
    :cond_7
    new-instance v8, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$18;

    invoke-direct {v8, p0, v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$18;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;[Z)V

    invoke-virtual {v4, v2, v5, v8}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v10, 0x7f0a0060

    new-instance v11, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$17;

    invoke-direct {v11, p0, v5}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$17;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;[Z)V

    invoke-virtual {v8, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v10, 0x7f0a0016

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 977
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    .line 978
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 979
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 980
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    iget-object v9, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 982
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    new-instance v9, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$19;

    invoke-direct {v9, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$19;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 989
    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    iput-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method private createSubtitleSizePopup()V
    .locals 6

    .prologue
    .line 581
    const/4 v1, 0x0

    .line 582
    .local v1, "selected":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v3

    .line 584
    .local v3, "textSize":I
    sget v4, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_LARGE:I

    if-ne v3, v4, :cond_0

    .line 585
    const/4 v1, 0x0

    .line 593
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 594
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0031

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 595
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0016

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$5;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 601
    const v4, 0x7f060006

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$6;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 626
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 627
    .local v2, "subtitleSizeDialog":Landroid/app/AlertDialog;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 628
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 630
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 632
    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$7;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 639
    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 640
    return-void

    .line 586
    .end local v0    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v2    # "subtitleSizeDialog":Landroid/app/AlertDialog;
    :cond_0
    sget v4, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_MEDIUM:I

    if-ne v3, v4, :cond_1

    .line 587
    const/4 v1, 0x1

    goto :goto_0

    .line 588
    :cond_1
    sget v4, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->SUBTITLE_SIZE_SMALL:I

    if-ne v3, v4, :cond_2

    .line 589
    const/4 v1, 0x2

    goto :goto_0

    .line 591
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private createSubtitleSyncPopup()V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    if-nez v0, :cond_0

    .line 570
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .line 574
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mShowingSubtitleFontPopup:Z

    .line 575
    return-void

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->show()V

    goto :goto_0
.end method

.method private createSubtitleTextColorPopup()V
    .locals 6

    .prologue
    .line 643
    const/4 v2, 0x0

    .line 644
    .local v2, "selected":I
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v0

    .line 646
    .local v0, "color":I
    const/high16 v4, -0x1000000

    if-ne v0, v4, :cond_1

    .line 647
    const/4 v2, 0x0

    .line 655
    :cond_0
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 656
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a008f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 658
    const v4, 0x7f0a0016

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$8;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$8;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 664
    const v4, 0x7f060005

    new-instance v5, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$9;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v1, v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 692
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 693
    .local v3, "subtitleTextColorDialog":Landroid/app/AlertDialog;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 694
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 696
    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 698
    new-instance v4, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$10;

    invoke-direct {v4, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$10;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 705
    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 706
    return-void

    .line 648
    .end local v1    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v3    # "subtitleTextColorDialog":Landroid/app/AlertDialog;
    :cond_1
    const v4, -0xffff01

    if-ne v0, v4, :cond_2

    .line 649
    const/4 v2, 0x1

    goto :goto_0

    .line 650
    :cond_2
    const v4, -0xff8000

    if-ne v0, v4, :cond_3

    .line 651
    const/4 v2, 0x2

    goto :goto_0

    .line 652
    :cond_3
    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 653
    const/4 v2, 0x3

    goto :goto_0
.end method

.method private static existFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "subTitleFileName"    # Ljava/lang/String;

    .prologue
    .line 1146
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1147
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private getLanguageTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "LanguageType"    # Ljava/lang/String;

    .prologue
    .line 1303
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1304
    .local v0, "temp":[Ljava/lang/String;
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    return-object v1
.end method

.method private initSubtitleMenu()V
    .locals 10

    .prologue
    .line 510
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "initSubtitleMenu()"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 512
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 514
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0091

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v5

    int-to-double v6, v5

    const-wide v8, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 516
    .local v1, "time":Ljava/lang/String;
    const-string v2, "MENU_VALUE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a007c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 520
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0081

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    .line 524
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 525
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleType()I

    move-result v2

    if-nez v2, :cond_1

    .line 526
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0046

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    .line 527
    const-string v2, "MENU_VALUE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 538
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0040

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 543
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0030

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 548
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a008f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 552
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0031

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 556
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a008e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    return-void

    .line 529
    :cond_1
    const-string v2, "MENU_VALUE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a008d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 532
    :cond_2
    const-string v2, "MENU_VALUE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private initSubtitlePreview()V
    .locals 4

    .prologue
    .line 487
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v1, :cond_0

    .line 488
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSubtitlePreview(). FontName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , FontPackageName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    const v2, 0x7f0d009c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 490
    .local v0, "subtitlePreview":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 492
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 495
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getFontPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFont(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;)V

    .line 499
    .end local v0    # "subtitlePreview":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method private initSubtitleValues()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 364
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "initSubtitleValues()"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-nez v3, :cond_0

    .line 366
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "initSubtitleValues() - mSubtitleUtil is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 371
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncTime:I

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleActive:Z

    .line 374
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v1

    .line 375
    .local v1, "length":I
    new-array v3, v1, [Z

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    .line 376
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 377
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    const/4 v4, 0x0

    aput-boolean v4, v3, v0

    .line 376
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 380
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 381
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    aput-boolean v5, v3, v0

    goto :goto_2

    .line 386
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v3

    if-eqz v3, :cond_4

    .line 387
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v3

    array-length v2, v3

    .line 389
    .local v2, "subtitleIndex":I
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_4

    .line 390
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    aget v4, v4, v0

    aput-boolean v5, v3, v4

    .line 389
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 395
    .end local v2    # "subtitleIndex":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedFont()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFont:Ljava/lang/String;

    .line 397
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontColor:I

    .line 398
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontSize:I

    .line 399
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getBackgroundColor()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleBGColor:I

    .line 401
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initSubtitleValues() - mSubtitleFilePath = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleSyncTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleActive = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleActive:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleFont = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFont:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleFontColor = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontColor:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleFontSize = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSubtitleBGColor = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleBGColor:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private makeFontList()Z
    .locals 2

    .prologue
    .line 747
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "makeFontList()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->initFontList()V

    .line 749
    const/4 v0, 0x1

    return v0
.end method

.method private makeSubtitleFilesList()Z
    .locals 15

    .prologue
    .line 1223
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    .line 1225
    new-instance v11, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;

    invoke-direct {v11, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$25;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    iput-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFileFilter:Ljava/io/FilenameFilter;

    .line 1243
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const-string v12, "storage"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageManager;

    .line 1244
    .local v6, "storageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v6}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v9

    .line 1245
    .local v9, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v5, v9

    .line 1247
    .local v5, "stlength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_2

    .line 1248
    aget-object v8, v9, v2

    .line 1249
    .local v8, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v10

    .line 1250
    .local v10, "subsystem":Ljava/lang/String;
    const-string v7, ""

    .line 1252
    .local v7, "storagePath":Ljava/lang/String;
    if-eqz v10, :cond_1

    const-string v11, "fuse"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "sd"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1253
    :cond_0
    invoke-virtual {v8}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 1254
    invoke-virtual {v6, v7}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "mounted"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1255
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1256
    .local v0, "file":Ljava/io/File;
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mFileFilter:Ljava/io/FilenameFilter;

    invoke-virtual {v0, v11}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    .line 1247
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1261
    .end local v7    # "storagePath":Ljava/lang/String;
    .end local v8    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v10    # "subsystem":Ljava/lang/String;
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleType()I

    move-result v11

    if-nez v11, :cond_3

    .line 1262
    const-string v11, "VideoSubtitlePopup"

    const-string v12, "makeSubtitleFilesList()-INBAND subtitle"

    invoke-static {v11, v12}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    new-instance v12, Ljava/io/File;

    iget-object v13, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v14, 0x7f0a0046

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1266
    :cond_3
    const-string v11, "VideoSubtitlePopup"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "makeSubtitleFilesList()-searched counts are :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_7

    .line 1268
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    .line 1269
    .local v1, "fileName":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v11, ""

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1270
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleType()I

    move-result v11

    if-nez v11, :cond_5

    .line 1271
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0046

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1275
    :cond_5
    if-nez v1, :cond_6

    .line 1276
    const/4 v11, 0x0

    .line 1287
    .end local v1    # "fileName":Ljava/lang/String;
    :goto_1
    return v11

    .line 1278
    .restart local v1    # "fileName":Ljava/lang/String;
    :cond_6
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 1279
    .local v3, "index":I
    const/4 v11, -0x1

    if-eq v3, v11, :cond_7

    .line 1280
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    .line 1282
    .local v4, "itemToMove":Ljava/io/File;
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1283
    iget-object v11, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubFilesList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1287
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v3    # "index":I
    .end local v4    # "itemToMove":Ljava/io/File;
    :cond_7
    const/4 v11, 0x1

    goto :goto_1
.end method

.method private notifyChange(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 1444
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnSvcNotificationListener;->onSvcNotification(ILjava/lang/String;)V

    .line 1445
    :cond_0
    return-void
.end method

.method private refreshSubtitleMenu()V
    .locals 2

    .prologue
    .line 502
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "refreshSubtitleMenu()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 506
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitlePreview()V

    .line 507
    return-void
.end method

.method private revertSubtitleSettings()V
    .locals 3

    .prologue
    .line 411
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings()"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-nez v1, :cond_2

    .line 413
    :cond_0
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings() - mSubtitleUtil or mServiceUtil is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    :cond_1
    :goto_0
    return-void

    .line 417
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 418
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->existFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 420
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->setSelectSubtitleLang([Z)V

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 422
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert Subtitle"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->selectOutbandSubtitle()Z

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->startSubtitle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSyncTime()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncTime:I

    if-eq v1, v2, :cond_3

    .line 455
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert synctime"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncTime:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSyncTime(I)V

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleSyncTime()V

    .line 460
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v1

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleActive:Z

    if-eq v1, v2, :cond_4

    .line 461
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert activation"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleActive:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    .line 465
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedFont()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFont:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 466
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert font"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFont:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setFont(Ljava/lang/String;)V

    .line 470
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextColor()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontColor:I

    if-eq v1, v2, :cond_6

    .line 471
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert font color"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontColor:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextColor(I)V

    .line 475
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getTextSize()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontSize:I

    if-eq v1, v2, :cond_7

    .line 476
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert font size"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFontSize:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setTextSize(I)V

    .line 480
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getBackgroundColor()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleBGColor:I

    if-eq v1, v2, :cond_1

    .line 481
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert font background color"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleBGColor:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 427
    :cond_8
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->initSelectSubtitleTrack()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 431
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings() - iputFilePath has an exception."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 434
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetSubtitle()V

    .line 435
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    goto/16 :goto_1

    .line 438
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleType()I

    move-result v1

    if-nez v1, :cond_c

    .line 439
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 440
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "revertSubtitleSettings : revert to INBAND"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    .line 442
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->setSelectSubtitleLang([Z)V

    .line 443
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setInbandSubtitle()V

    goto/16 :goto_1

    .line 445
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangArray:[Z

    invoke-direct {p0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->setSelectSubtitleLang([Z)V

    .line 446
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->initSelectSubtitleTrack()V

    goto/16 :goto_1

    .line 449
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resetSubtitle()V

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->stopSubtitle()V

    goto/16 :goto_1
.end method

.method private sendSubtitle()V
    .locals 2

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFilePath:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1438
    :cond_1
    const/16 v0, 0x83

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->notifyChange(I)V

    .line 1440
    :cond_2
    return-void
.end method

.method private setSelectSubtitleLang([Z)V
    .locals 7
    .param p1, "langArray"    # [Z

    .prologue
    const/4 v6, 0x1

    .line 993
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "setSelectSubtitleLang()"

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    if-nez p1, :cond_1

    .line 1029
    :cond_0
    return-void

    .line 997
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->resetSelectSubtitle()V

    .line 998
    array-length v2, p1

    .line 1000
    .local v2, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_4

    .line 1001
    aget-boolean v3, p1, v0

    if-ne v3, v6, :cond_2

    .line 1002
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->increaseSelectSubtitle()V

    .line 1005
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v3

    if-le v3, v6, :cond_3

    .line 1006
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setIsMultiSubtitle(Z)V

    .line 1000
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1008
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setIsMultiSubtitle(Z)V

    goto :goto_1

    .line 1012
    :cond_4
    const/4 v1, 0x0

    .line 1013
    .local v1, "k":I
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->createSelectedSubtitleIndexArray(I)V

    .line 1014
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_6

    .line 1015
    aget-boolean v3, p1, v0

    if-ne v3, v6, :cond_5

    .line 1016
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSelectedSubtitleIndex(II)V

    .line 1017
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSubtitleUtil.setSelectedSubtitleIndex["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    add-int/lit8 v1, v1, 0x1

    .line 1014
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1022
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 1023
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_0

    .line 1024
    aget-boolean v3, p1, v0

    if-ne v3, v6, :cond_7

    .line 1025
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1023
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 172
    :cond_1
    return-void
.end method

.method checkSubtitleLangDialogDoneBtn()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1291
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1293
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1300
    :cond_0
    :goto_0
    return-void

    .line 1295
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1296
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mToastSelectAtLeastOne:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 1297
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mToastSelectAtLeastOne:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public createSubtitleMenu()V
    .locals 13

    .prologue
    .line 184
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "createSubtitleMenu"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitleMenu()V

    .line 187
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleList:Ljava/util/ArrayList;

    const v4, 0x7f030022

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "MENU_TITLE"

    aput-object v6, v5, v1

    const/4 v1, 0x1

    const-string v6, "MENU_VALUE"

    aput-object v6, v5, v1

    const/4 v1, 0x2

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    .line 191
    new-instance v10, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    invoke-direct {v10, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 192
    .local v10, "popupMenu":Landroid/app/AlertDialog$Builder;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0045

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    .line 196
    .local v9, "inflate":Landroid/view/LayoutInflater;
    const v0, 0x7f030020

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    const v1, 0x7f0d009e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ListView;

    .line 199
    .local v11, "subtitleMenuList":Landroid/widget/ListView;
    const v0, 0x7f030021

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 200
    .local v12, "subtitleOnOff":Landroid/view/View;
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v11, v12, v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuAdapter:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SubtitleMenuListAdapter;

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleView:Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->initSubtitlePreview()V

    .line 207
    const v0, 0x7f0a0016

    new-instance v1, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v10, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 215
    const v0, 0x7f0a007a

    new-instance v1, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v10, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 237
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 239
    const v0, 0x7f0d00a0

    invoke-virtual {v12, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    .line 240
    .local v7, "cb":Landroid/widget/CheckBox;
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 241
    invoke-virtual {v7, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 242
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 243
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 245
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$4;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 312
    return-void

    .line 232
    .end local v7    # "cb":Landroid/widget/CheckBox;
    :catch_0
    move-exception v8

    .line 233
    .local v8, "e":Ljava/lang/NullPointerException;
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "Subtitle Menu Dialog :: NullPointerException"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 234
    .end local v8    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v8

    .line 235
    .local v8, "e":Landroid/view/WindowManager$BadTokenException;
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "Subtitle Menu Dialog :: BadTokenException"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 187
    nop

    :array_0
    .array-data 4
        0x7f0d00a1
        0x7f0d00a2
    .end array-data
.end method

.method public dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->isShowSyncPopup()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->isShowSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;->hide()V

    .line 154
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->revertSubtitleSettings()V

    .line 156
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 157
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleSyncPopup:Lcom/sec/android/app/mv/player/popup/VideoSubtitleSyncPopup;

    .line 158
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleMenuDialog:Landroid/app/AlertDialog;

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    if-eqz v0, :cond_5

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 164
    :cond_5
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public getMultiSelectedSubtitleLanguage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1324
    .local v1, "retValue":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v2

    .line 1325
    .local v2, "subtitle_cnt":I
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSubtitleUtil.getMultiSelectSubtitle() - selected subtitle_cnt : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1328
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1329
    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    add-int/lit8 v3, v2, -0x1

    if-ge v0, v3, :cond_0

    .line 1330
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1327
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1334
    :cond_1
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMultiSelectedSubtitleLanguage()-return string : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getSubtitleMenuString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1308
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1309
    .local v1, "retValue":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleCount()I

    move-result v2

    .line 1310
    .local v2, "subtitle_cnt":I
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSubtitleMenuString()-subtitle_cnt : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1313
    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->getLanguageTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1314
    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    add-int/lit8 v3, v2, -0x1

    if-ge v0, v3, :cond_0

    .line 1315
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1312
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1318
    :cond_1
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSubtitleMenuString()-return string : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubDialogs:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 180
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 1428
    if-eqz p2, :cond_0

    .line 1429
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    .line 1433
    :goto_0
    return-void

    .line 1431
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleUtil:Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->setSubtitleActive(Z)V

    goto :goto_0
.end method
