.class public Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
.super Landroid/app/Activity;
.source "PlaylistActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Activity;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ADD:I

.field private final DEL:I

.field private TAG:Ljava/lang/String;

.field private mActionMode:Landroid/view/ActionMode;

.field public mActionModeTitle:Landroid/widget/TextView;

.field private mDeviceName:Ljava/lang/String;

.field private mFromMoviePlayer:Z

.field public mIsEnterDeleteMode:Z

.field private mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

.field private mOnSelectAllClickListener:Landroid/view/View$OnClickListener;

.field private mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

.field public mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

.field private final mPlaylistReceiver:Landroid/content/BroadcastReceiver;

.field private mPlaylistType:I

.field private mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mSelectAllCheck:Landroid/widget/CheckBox;

.field private mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 557
    new-instance v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$2;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    const-class v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    .line 64
    iput v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->ADD:I

    .line 66
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->DEL:I

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mDeviceName:Ljava/lang/String;

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mFromMoviePlayer:Z

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    .line 86
    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    .line 456
    new-instance v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mOnSelectAllClickListener:Landroid/view/View$OnClickListener;

    .line 607
    new-instance v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$3;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    .line 840
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Lcom/sec/android/app/mv/player/playlist/PlaylistView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->restartLoader()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
    .param p1, "x1"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mOnSelectAllClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;ILjava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/activity/PlaylistActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V

    return-void
.end method

.method private callPlayer(JI)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v2, "callPlayer E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 401
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 402
    const-string v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 403
    const-string v1, "uri"

    sget-object v2, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    const-string v1, "DeviceName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    .line 408
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->startActivity(Landroid/content/Intent;)V

    .line 409
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->overridePendingTransition(II)V

    .line 410
    return-void
.end method

.method private callPlaylist(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 413
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 414
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "ListType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 415
    const-string v1, "FromMoviePlayer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 417
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->startActivity(Landroid/content/Intent;)V

    .line 418
    return-void
.end method

.method private canShowSelectAll()Z
    .locals 2

    .prologue
    .line 687
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private extractItem(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566
    .local p1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    sget-object v2, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mComparator:Ljava/util/Comparator;

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 568
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 569
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 570
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getItem()Lcom/sec/android/app/mv/player/type/PlaylistItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 572
    :cond_0
    return-object v0
.end method

.method private extractPos(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 576
    .local p1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    sget-object v2, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mComparator:Ljava/util/Comparator;

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 578
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v0, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 580
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;->getPos()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 582
    :cond_0
    return-object v0
.end method

.method private hideSelectAll()V
    .locals 3

    .prologue
    .line 702
    const v2, 0x7f0d002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 703
    .local v1, "selectAllLayout":Landroid/widget/LinearLayout;
    const v2, 0x7f0d0030

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 704
    .local v0, "selectAll":Landroid/widget/RelativeLayout;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 705
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 706
    return-void
.end method

.method private initActionBar()V
    .locals 9

    .prologue
    const v8, 0x7f0d0033

    const v7, 0x7f0a0082

    const v3, 0x7f0a0057

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 216
    .local v0, "action":Landroid/app/ActionBar;
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-nez v2, :cond_6

    .line 217
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v2, :cond_5

    .line 218
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 234
    :cond_0
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_T:Z

    if-eqz v2, :cond_a

    .line 235
    :cond_1
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 236
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 244
    :cond_2
    :goto_1
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v2, :cond_3

    .line 245
    const v2, 0x7f070060

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setLogo(I)V

    .line 246
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 249
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_b

    .line 250
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 251
    new-instance v2, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    .line 253
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 254
    .local v1, "layout":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 264
    :cond_4
    :goto_2
    return-void

    .line 220
    .end local v1    # "layout":Landroid/widget/RelativeLayout;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v7, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 222
    :cond_6
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v2, v5, :cond_7

    .line 223
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 224
    :cond_7
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    .line 225
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v2, :cond_8

    .line 226
    const v2, 0x7f0a0055

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 228
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v7, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 230
    :cond_9
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 231
    const v2, 0x7f0a0058

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    goto/16 :goto_0

    .line 238
    :cond_a
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v2, :cond_2

    .line 239
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 240
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto/16 :goto_1

    .line 257
    :cond_b
    invoke-virtual {p0, v8}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 258
    .restart local v1    # "layout":Landroid/widget/RelativeLayout;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 259
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v2, :cond_4

    .line 260
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v2, v5, :cond_4

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method private initSelectAll()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 667
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v3, " initSelectAll"

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const v2, 0x7f0d0031

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    .line 670
    const v2, 0x7f0d0030

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 671
    .local v0, "selectAll":Landroid/widget/RelativeLayout;
    const v2, 0x7f0d002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 673
    .local v1, "selectAllLayout":Landroid/widget/LinearLayout;
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->canShowSelectAll()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 674
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 675
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mOnSelectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 676
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 681
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_0

    .line 682
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 684
    :cond_0
    return-void

    .line 678
    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private registerBroadcastReciever()V
    .locals 2

    .prologue
    .line 631
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 632
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 633
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 634
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 635
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 636
    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 637
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 638
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 640
    return-void
.end method

.method private restartLoader()V
    .locals 3

    .prologue
    .line 758
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "restartLoader() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 760
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 762
    :cond_0
    return-void
.end method

.method private sendPlaylistEnterInfo(Z)V
    .locals 3
    .param p1, "isEntered"    # Z

    .prologue
    .line 446
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST_SHOW_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 448
    .local v0, "i":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 449
    const-string v1, "command"

    const-string v2, "mv_enter_list"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 454
    return-void

    .line 451
    :cond_0
    const-string v1, "command"

    const-string v2, "mv_exit_list"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private sendUpdatedPlaylist()V
    .locals 10

    .prologue
    .line 421
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v7, "sendUpdatedPlaylist E"

    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.START_VIDEO_FROM_MV_PLAYLIST"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423
    .local v1, "i":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v8, "lastPlayedItem"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/mv/player/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v4

    .line 424
    .local v4, "mLastPlayedID":J
    const/4 v0, 0x0

    .line 425
    .local v0, "contentUri":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getIndexOf(J)I

    move-result v3

    .line 426
    .local v3, "mLastPlayIndex":I
    const/4 v2, -0x1

    .line 428
    .local v2, "mCurPlayIndex":I
    const/4 v6, -0x1

    if-ne v3, v6, :cond_2

    .line 429
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getCurPlayingIndex()I

    move-result v2

    .line 430
    if-ltz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v6

    if-lt v2, v6, :cond_1

    .line 431
    :cond_0
    const/4 v2, 0x0

    .line 433
    :cond_1
    sget-object v6, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 435
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    const-string v7, "lastPlayedItem"

    iget-object v8, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/mv/player/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :goto_0
    const-string v6, "uri"

    invoke-virtual {v1, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 443
    return-void

    .line 437
    :cond_2
    sget-object v6, Lcom/sec/android/app/mv/player/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v6, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 438
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    goto :goto_0
.end method

.method private setImageCacheListener()V
    .locals 1

    .prologue
    .line 765
    new-instance v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$4;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;)V

    invoke-static {v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setOnUpdatedListener(Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;)V

    .line 776
    return-void
.end method

.method private showSelectAll()V
    .locals 3

    .prologue
    .line 693
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->canShowSelectAll()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 694
    const v2, 0x7f0d002f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 695
    .local v1, "selectAllLayout":Landroid/widget/LinearLayout;
    const v2, 0x7f0d0030

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 696
    .local v0, "selectAll":Landroid/widget/RelativeLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 697
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 699
    .end local v0    # "selectAll":Landroid/widget/RelativeLayout;
    .end local v1    # "selectAllLayout":Landroid/widget/LinearLayout;
    :cond_0
    return-void
.end method

.method private unRegisterBroadcastReciever()V
    .locals 2

    .prologue
    .line 643
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 645
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updatePlaylist(ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 586
    .local p2, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "updatePlaylist E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    if-nez p1, :cond_0

    .line 589
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->extractItem(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->add(Ljava/util/ArrayList;)V

    .line 595
    :goto_0
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->initAdapter()V

    .line 604
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->sendUpdatedPlaylist()V

    .line 605
    return-void

    .line 591
    :cond_0
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->extractPos(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->remove(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 597
    :cond_1
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "check box position won\'t be udpated. Therefore, UI becomes mismatch. so, just leave list before exit remove list."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 600
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->notifyDataSetChanged()V

    goto :goto_1
.end method


# virtual methods
.method public changeAllState(Z)V
    .locals 13
    .param p1, "checked"    # Z

    .prologue
    .line 483
    const/4 v0, 0x0

    .line 485
    .local v0, "childCount":I
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v7, :cond_0

    .line 486
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7, p1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->changeAllState(Z)V

    .line 490
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getView()Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 491
    .local v6, "mView":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 493
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 494
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 495
    .local v3, "itemView":Landroid/view/View;
    const v7, 0x7f0d002b

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 496
    .local v2, "itemCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v2, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 493
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 499
    .end local v2    # "itemCheckBox":Landroid/widget/CheckBox;
    .end local v3    # "itemView":Landroid/view/View;
    :cond_1
    const/4 v5, 0x0

    .line 501
    .local v5, "listCount":I
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 502
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ListView;->getCount()I

    move-result v5

    .line 505
    :cond_2
    sget-boolean v7, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-nez v7, :cond_a

    .line 506
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v5, :cond_6

    .line 507
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_3

    .line 508
    if-eqz p1, :cond_5

    .line 509
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 510
    iget-boolean v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v7, :cond_4

    .line 511
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    const/4 v8, 0x1

    iput-boolean v8, v7, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    .line 515
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v4, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 506
    :cond_3
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 513
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    const/4 v8, 0x0

    iput-boolean v8, v7, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->mIsEnterDeleteModeListView:Z

    goto :goto_2

    .line 518
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 519
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v7}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_3

    .line 525
    :cond_6
    if-eqz p1, :cond_b

    .line 526
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_7

    .line 527
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0082

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    :cond_7
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v7

    if-nez v7, :cond_9

    iget v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v7, :cond_8

    iget v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_9

    .line 532
    :cond_8
    invoke-virtual {p0, v5}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBar(I)V

    .line 545
    :cond_9
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_a

    .line 546
    iget-boolean v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    if-eqz v7, :cond_e

    .line 547
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0d00c0

    invoke-interface {v7, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 548
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0d00c0

    invoke-interface {v7, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 555
    .end local v4    # "j":I
    :cond_a
    :goto_5
    return-void

    .line 535
    .restart local v4    # "j":I
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_c

    .line 536
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionModeTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0082

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    :cond_c
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v7

    if-nez v7, :cond_9

    iget v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v7, :cond_d

    iget v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_9

    .line 541
    :cond_d
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBar(I)V

    goto :goto_4

    .line 550
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0d00bf

    invoke-interface {v7, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_5
.end method

.method public getActionMode()Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method public getListCount()I
    .locals 1

    .prologue
    .line 837
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v0

    return v0
.end method

.method public getSelectedListSize()I
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isAllSelected()Z
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isAllSelected()Z

    move-result v0

    return v0
.end method

.method public isAllUnSelected()Z
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isAllUnSelected()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 93
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "ListType"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    .line 94
    const-string v2, "DeviceName"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mDeviceName:Ljava/lang/String;

    .line 95
    const-string v2, "FromMoviePlayer"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mFromMoviePlayer:Z

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate. mPlaylistType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " DeviceName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is From Movie Player : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mFromMoviePlayer:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const v2, 0x7f03000f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->setContentView(I)V

    .line 100
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->setVolumeControlStream(I)V

    .line 102
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    new-instance v2, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;

    iget v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistTabletView;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    .line 109
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->initActionBar()V

    .line 111
    invoke-static {p0}, Lcom/sec/android/app/mv/player/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/VideoDB;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mVideoDB:Lcom/sec/android/app/mv/player/db/VideoDB;

    .line 112
    invoke-static {p0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPrefMgr:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->initSelectAll()V

    .line 116
    const v2, 0x7f0d0021

    invoke-virtual {p0, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 117
    .local v1, "layout":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    iget v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-nez v2, :cond_0

    .line 121
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->clear()V

    .line 124
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mFromMoviePlayer:Z

    if-ne v2, v6, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 128
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setContext(Landroid/content/Context;)V

    .line 130
    invoke-direct {p0, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->sendPlaylistEnterInfo(Z)V

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->registerBroadcastReciever()V

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->initAdapter()V

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 136
    return-void

    .line 105
    .end local v1    # "layout":Landroid/widget/RelativeLayout;
    :cond_2
    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->setRequestedOrientation(I)V

    .line 106
    new-instance v2, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;

    iget v3, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    invoke-direct {v2, p0, v3, v5}, Lcom/sec/android/app/mv/player/playlist/PlaylistPhoneView;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 711
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v2, "onCreateLoader() E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    new-instance v0, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/mv/player/adapter/CustomCursorLoader;-><init>(Landroid/content/Context;ILcom/sec/android/app/mv/player/common/PlaylistUtils;)V

    .line 713
    .local v0, "l":Landroid/content/AsyncTaskLoader;, "Landroid/content/AsyncTaskLoader<Landroid/database/Cursor;>;"
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/content/AsyncTaskLoader;->setUpdateThrottle(J)V

    .line 714
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 268
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 278
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 271
    :cond_0
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 274
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->sendPlaylistEnterInfo(Z)V

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->unRegisterBroadcastReciever()V

    .line 202
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x3

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onLoadFinished() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->swapCursor(Landroid/database/Cursor;)V

    .line 723
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->initStartView()V

    .line 725
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_5

    .line 726
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateActionBarBtn()V

    .line 733
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v0, v2, :cond_2

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isListEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->listgetCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 735
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    .line 740
    :cond_2
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 743
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    .line 747
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->invalidateOptionsMenu()V

    .line 748
    return-void

    .line 728
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->canShowSelectAll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updateSelectAllState()V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 753
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v1, "onLoaderReset() E"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->swapCursor(Landroid/database/Cursor;)V

    .line 755
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v10, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 334
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->print()V

    .line 335
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 337
    .local v0, "id":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v2, "selectedArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v2

    .line 340
    sparse-switch v0, :sswitch_data_0

    .line 395
    :goto_0
    return v9

    .line 342
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 346
    :sswitch_1
    iget v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v6, v8, :cond_0

    .line 347
    invoke-direct {p0, v9, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 349
    :cond_0
    iget v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v6, v10, :cond_2

    .line 350
    invoke-direct {p0, v7, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 352
    const/4 v3, 0x0

    .line 354
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 355
    const v6, 0x7f0a006b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 360
    :goto_1
    invoke-static {p0, v3, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 357
    :cond_1
    const v6, 0x7f0a006d

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 363
    .end local v3    # "str":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v9, v2}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 365
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 366
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v4

    .line 367
    .local v4, "videoid":J
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getResumePos(I)I

    move-result v1

    .line 368
    .local v1, "resPos":I
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 369
    invoke-direct {p0, v4, v5, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->callPlayer(JI)V

    .line 371
    .end local v1    # "resPos":I
    .end local v4    # "videoid":J
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 376
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    goto :goto_0

    .line 380
    :sswitch_3
    invoke-direct {p0, v8}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->callPlaylist(I)V

    goto :goto_0

    .line 384
    :sswitch_4
    sget-boolean v6, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v6, :cond_4

    .line 385
    iput-boolean v7, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mIsEnterDeleteMode:Z

    .line 386
    iget-object v6, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto/16 :goto_0

    .line 388
    :cond_4
    invoke-direct {p0, v10}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->callPlaylist(I)V

    goto/16 :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0d00bb -> :sswitch_1
        0x7f0d00bc -> :sswitch_2
        0x7f0d00bd -> :sswitch_3
        0x7f0d00be -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 185
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/adapter/ImageCache;->setOnUpdatedListener(Lcom/sec/android/app/mv/player/adapter/ImageCache$OnUpdatedListener;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->onPause()V

    .line 187
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0d00be

    const/4 v6, 0x2

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->TAG:Ljava/lang/String;

    const-string v2, "onPrepareOptionsMenu() E"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v1, v6, :cond_0

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v1, v5, :cond_1

    .line 287
    :cond_0
    const v1, 0x7f0d00bb

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 288
    .local v0, "menuDone":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    .line 290
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 304
    .end local v0    # "menuDone":Landroid/view/MenuItem;
    :cond_1
    :goto_0
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v1, v6, :cond_2

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v1, v5, :cond_3

    .line 306
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_9

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 317
    :cond_3
    :goto_1
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v1, v3, :cond_4

    .line 318
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    if-gt v1, v3, :cond_b

    .line 319
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 325
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isListEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 326
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->hideSelectAll()V

    .line 329
    :cond_5
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1

    .line 293
    .restart local v0    # "menuDone":Landroid/view/MenuItem;
    :cond_6
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v1, v5, :cond_8

    .line 294
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    if-le v1, v3, :cond_7

    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 296
    :cond_7
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 298
    :cond_8
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 309
    .end local v0    # "menuDone":Landroid/view/MenuItem;
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isAllSelected()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 312
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 321
    :cond_b
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 140
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->setImageCacheListener()V

    .line 144
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v0, v2, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->initAdapter()V

    .line 148
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_K:Z

    if-eqz v0, :cond_3

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    if-nez v0, :cond_2

    .line 155
    new-instance v0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;-><init>(Lcom/sec/android/app/mv/player/activity/PlaylistActivity;Lcom/sec/android/app/mv/player/activity/PlaylistActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    .line 158
    :cond_2
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v0, v2, :cond_3

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->twSetCustomMultiChoiceMode(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mModeCallback:Lcom/sec/android/app/mv/player/activity/PlaylistActivity$ModeCallback;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 165
    :cond_3
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v0, v2, :cond_4

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->restartLoader()V

    .line 169
    :cond_4
    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v0, v3, :cond_5

    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_6

    .line 170
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->removeDeletedIdsFromSelectedList()V

    .line 173
    :cond_6
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eq v0, v2, :cond_7

    iget v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-ne v0, v3, :cond_8

    .line 175
    :cond_7
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->removeDeletedIdsFromPlayList(Landroid/content/Context;)V

    .line 178
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->invalidateOptionsMenu()V

    .line 179
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 191
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 192
    return-void
.end method

.method public toggleSelectAll()V
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isAllSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    .line 788
    :goto_0
    return-void

    .line 786
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    goto :goto_0
.end method

.method public updateActionBar(I)V
    .locals 6
    .param p1, "selectedfileValue"    # I

    .prologue
    .line 206
    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 209
    .local v0, "action":Landroid/app/ActionBar;
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0082

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 211
    .end local v0    # "action":Landroid/app/ActionBar;
    :cond_1
    return-void
.end method

.method public updateActionBarBtn()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    if-nez v0, :cond_0

    .line 834
    :goto_0
    return-void

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->getDoneBtn()Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setDoneBtnEnabled(Z)V

    .line 829
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->getListCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0

    .line 825
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->setDoneBtnEnabled(Z)V

    goto :goto_1

    .line 832
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlaylistActionBar:Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/playlist/PlaylistActionBar;->mCounterSpinner:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateListandCallPlayer()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 799
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->print()V

    .line 801
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 802
    .local v1, "selectedArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/type/PlaylistSelectedItem;>;"
    iget-object v4, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v1

    .line 804
    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->updatePlaylist(ILjava/util/ArrayList;)V

    .line 806
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 807
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getVideoID(I)J

    move-result-wide v2

    .line 808
    .local v2, "videoid":J
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getResumePos(I)I

    move-result v0

    .line 809
    .local v0, "resPos":I
    invoke-static {}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->getInstance()Lcom/sec/android/app/mv/player/common/PlaylistUtils;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/mv/player/common/PlaylistUtils;->setCurPlayingIndex(I)V

    .line 810
    invoke-direct {p0, v2, v3, v0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->callPlayer(JI)V

    .line 812
    .end local v0    # "resPos":I
    .end local v2    # "videoid":J
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->finish()V

    .line 813
    return-void
.end method

.method public updateSelectAllState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isListEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 656
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->changeAllState(Z)V

    .line 657
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->hideSelectAll()V

    .line 662
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->invalidateOptionsMenu()V

    .line 664
    :cond_0
    return-void

    .line 659
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mSelectAllCheck:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->mPlayListView:Lcom/sec/android/app/mv/player/playlist/PlaylistView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/playlist/PlaylistView;->isAllSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 660
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/activity/PlaylistActivity;->showSelectAll()V

    goto :goto_0
.end method
