.class public Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;
.super Ljava/lang/Object;
.source "MVMeasureSize.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MVMeasureSize"


# instance fields
.field private mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/multivision/MVUtil;)V
    .locals 0
    .param p1, "util"    # Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    .line 13
    return-void
.end method

.method private measureCropRatio(Ljava/util/List;I)V
    .locals 13
    .param p2, "multivisionJoinedNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "playerList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;>;"
    new-array v9, p2, [I

    .line 17
    .local v9, "widthInchInfo":[I
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getChangedRotation()Z

    move-result v2

    .line 18
    .local v2, "needToPositionReverse":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 19
    const/16 v10, 0x9c4

    aput v10, v9, v0

    .line 18
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 25
    .local v3, "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 26
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getDisplayWidth()I

    move-result v10

    mul-int/lit16 v10, v10, 0x3e8

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getXdpi()I

    move-result v11

    div-int v8, v10, v11

    .line 27
    .local v8, "widthInch":I
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v4

    .line 29
    .local v4, "position":I
    if-lez v4, :cond_2

    if-gt v4, p2, :cond_2

    .line 30
    add-int/lit8 v10, v4, -0x1

    aput v8, v9, v10

    .line 31
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", wInch : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v4, -0x1

    aget v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 33
    :cond_2
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio something is wrong position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", multivisionJoinedNumber : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 39
    .end local v3    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    .end local v4    # "position":I
    .end local v8    # "widthInch":I
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayWidth()I

    move-result v10

    mul-int/lit16 v10, v10, 0x3e8

    iget-object v11, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getXdpi()I

    move-result v11

    div-int v6, v10, v11

    .line 40
    .local v6, "serverWidthInch":I
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVServerPosition()I

    move-result v5

    .line 41
    .local v5, "serverPosition":I
    if-lez v5, :cond_4

    if-gt v5, p2, :cond_4

    .line 42
    add-int/lit8 v10, v5, -0x1

    aput v6, v9, v10

    .line 43
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", wInch : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v5, -0x1

    aget v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :goto_2
    const/4 v7, 0x0

    .line 50
    .local v7, "totalWidthInch":I
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p2, :cond_5

    .line 51
    aget v10, v9, v0

    add-int/2addr v7, v10

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 45
    .end local v7    # "totalWidthInch":I
    :cond_4
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio something is wrong position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", multivisionJoinedNumber : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 53
    .restart local v7    # "totalWidthInch":I
    :cond_5
    const/4 v0, 0x1

    :goto_4
    if-ge v0, p2, :cond_6

    .line 54
    aget v10, v9, v0

    add-int/lit8 v11, v0, -0x1

    aget v11, v9, v11

    add-int/2addr v10, v11

    aput v10, v9, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 58
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 59
    .restart local v3    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 60
    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v4

    .line 61
    .restart local v4    # "position":I
    if-eqz v2, :cond_8

    .line 62
    sub-int v10, p2, v4

    add-int/lit8 v4, v10, 0x1

    .line 65
    :cond_8
    const/4 v10, 0x1

    if-gt v4, v10, :cond_9

    .line 66
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAdjustLeftCropRatio(F)V

    .line 71
    :goto_6
    if-lt v4, p2, :cond_a

    .line 72
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v3, v10}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAdjustRightCropRatio(F)V

    .line 76
    :goto_7
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", leftCropRatio : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustLeftCropRatio()F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", rightCropRatio : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustRightCropRatio()F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", totalWInch : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 68
    :cond_9
    add-int/lit8 v10, v4, -0x2

    aget v10, v9, v10

    int-to-float v10, v10

    int-to-float v11, v7

    div-float/2addr v10, v11

    invoke-virtual {v3, v10}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAdjustLeftCropRatio(F)V

    goto :goto_6

    .line 74
    :cond_a
    add-int/lit8 v10, v4, -0x1

    aget v10, v9, v10

    int-to-float v10, v10

    int-to-float v11, v7

    div-float/2addr v10, v11

    invoke-virtual {v3, v10}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAdjustRightCropRatio(F)V

    goto :goto_7

    .line 82
    .end local v3    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    .end local v4    # "position":I
    :cond_b
    if-eqz v2, :cond_c

    .line 83
    sub-int v10, p2, v5

    add-int/lit8 v5, v10, 0x1

    .line 86
    :cond_c
    const/4 v10, 0x1

    if-gt v5, v10, :cond_d

    .line 87
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAdjustLeftCropRatio(F)V

    .line 92
    :goto_8
    if-lt v5, p2, :cond_e

    .line 93
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAdjustRightCropRatio(F)V

    .line 101
    :goto_9
    const-string v10, "MVMeasureSize"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureCropRatio position : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", leftCropRatio : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getAdjustLeftCropRatio()F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", rightCropRatio : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getAdjustRightCropRatio()F

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", totalWInch : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    return-void

    .line 89
    :cond_d
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    add-int/lit8 v11, v5, -0x2

    aget v11, v9, v11

    int-to-float v11, v11

    int-to-float v12, v7

    div-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAdjustLeftCropRatio(F)V

    goto :goto_8

    .line 95
    :cond_e
    if-lez v5, :cond_f

    .line 96
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    add-int/lit8 v11, v5, -0x1

    aget v11, v9, v11

    int-to-float v11, v11

    int-to-float v12, v7

    div-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAdjustRightCropRatio(F)V

    goto :goto_9

    .line 98
    :cond_f
    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setAdjustRightCropRatio(F)V

    goto :goto_9
.end method

.method private measureCropSize(Ljava/util/List;II)V
    .locals 15
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "playerList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v10, v12, 0x1

    .line 108
    .local v10, "shareGroupNumber":I
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v7

    .line 111
    .local v7, "multivisionJoinedNumber":I
    const-string v12, "MVMeasureSize"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "measureCropSize multivisionJoinedNumber number : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", videoWidth : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", videoHeight : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    move/from16 v0, p3

    move/from16 v1, p2

    if-le v0, v1, :cond_0

    .line 114
    const-string v12, "MVMeasureSize"

    const-string v13, "measureCropSize videoHeight > videoWidth, call setChangedRotation(true)"

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setChangedRotation(Z)V

    .line 118
    :cond_0
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->measureCropRatio(Ljava/util/List;I)V

    .line 120
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v10, :cond_5

    .line 121
    add-int/lit8 v12, v5, 0x1

    if-ne v10, v12, :cond_2

    .line 122
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getAdjustLeftCropRatio()F

    move-result v3

    .line 123
    .local v3, "cropLeftRatio":F
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getAdjustRightCropRatio()F

    move-result v4

    .line 124
    .local v4, "cropRightRatio":F
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVServerPosition()I

    move-result v8

    .line 136
    .local v8, "multivisionPosition":I
    :goto_1
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getChangedRotation()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 137
    const/4 v6, 0x0

    .line 138
    .local v6, "left":I
    move/from16 v9, p2

    .line 139
    .local v9, "right":I
    move/from16 v0, p3

    int-to-float v12, v0

    mul-float/2addr v12, v3

    float-to-int v11, v12

    .line 140
    .local v11, "top":I
    move/from16 v0, p3

    int-to-float v12, v0

    mul-float/2addr v12, v4

    float-to-int v2, v12

    .line 148
    .local v2, "bottom":I
    :goto_2
    add-int/lit8 v12, v5, 0x1

    if-ne v10, v12, :cond_4

    .line 149
    iget-object v12, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v12, v6, v11, v9, v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMeasuredServerCropSize(IIII)V

    .line 154
    :goto_3
    const-string v12, "MVMeasureSize"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "measureCropSize position : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", left : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", top : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", right : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", bottom : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    .end local v2    # "bottom":I
    .end local v3    # "cropLeftRatio":F
    .end local v4    # "cropRightRatio":F
    .end local v6    # "left":I
    .end local v8    # "multivisionPosition":I
    .end local v9    # "right":I
    .end local v11    # "top":I
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 126
    :cond_2
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 127
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustLeftCropRatio()F

    move-result v3

    .line 128
    .restart local v3    # "cropLeftRatio":F
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustRightCropRatio()F

    move-result v4

    .line 129
    .restart local v4    # "cropRightRatio":F
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getMVClientPosition()I

    move-result v8

    .restart local v8    # "multivisionPosition":I
    goto/16 :goto_1

    .line 142
    :cond_3
    const/4 v11, 0x0

    .line 143
    .restart local v11    # "top":I
    move/from16 v2, p3

    .line 144
    .restart local v2    # "bottom":I
    move/from16 v0, p2

    int-to-float v12, v0

    mul-float/2addr v12, v3

    float-to-int v6, v12

    .line 145
    .restart local v6    # "left":I
    move/from16 v0, p2

    int-to-float v12, v0

    mul-float/2addr v12, v4

    float-to-int v9, v12

    .restart local v9    # "right":I
    goto/16 :goto_2

    .line 151
    :cond_4
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12, v6, v11, v9, v2}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMeasuredClientCropSize(IIII)V

    .line 152
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    invoke-virtual {v12, v7}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMVPlayNumber(I)V

    goto/16 :goto_3

    .line 156
    .end local v2    # "bottom":I
    .end local v3    # "cropLeftRatio":F
    .end local v4    # "cropRightRatio":F
    .end local v6    # "left":I
    .end local v8    # "multivisionPosition":I
    .end local v9    # "right":I
    .end local v11    # "top":I
    :cond_5
    return-void
.end method

.method private measureSurfaceSizeRatio(Ljava/util/List;II)V
    .locals 12
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .local p1, "playerList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;>;"
    const/16 v11, 0x3e8

    .line 159
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getMVMembersNumber()I

    move-result v3

    .line 160
    .local v3, "multivisionJoinedNumber":I
    const/4 v0, 0x0

    .line 161
    .local v0, "adjustHeight":I
    const/4 v5, 0x0

    .line 163
    .local v5, "serverAdjustInch":I
    const-string v8, "MVMeasureSize"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "measureSurfaceSizeRatio videoWidth : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", videoHeight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",  multivisionJoinedNumber : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 168
    .local v4, "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 169
    if-le p2, p3, :cond_2

    .line 170
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getDisplayWidth()I

    move-result v8

    mul-int/2addr v8, p3

    div-int/2addr v8, p2

    mul-int v0, v8, v3

    .line 175
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getDisplayHeight()I

    move-result v8

    if-le v0, v8, :cond_1

    .line 176
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getDisplayHeight()I

    move-result v0

    .line 178
    :cond_1
    mul-int/lit16 v8, v0, 0x3e8

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getYdpi()I

    move-result v9

    div-int v1, v8, v9

    .line 179
    .local v1, "adjustInch":I
    const-string v8, "MVMeasureSize"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "measureSurfaceSizeRatio ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] adjustHeight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", adjustInch : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ydpi : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getYdpi()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {v4, v1}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setAdjustHeightInch(I)V

    goto :goto_0

    .line 172
    .end local v1    # "adjustInch":I
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getDisplayWidth()I

    move-result v8

    mul-int/2addr v8, p2

    div-int/2addr v8, p3

    mul-int v0, v8, v3

    goto :goto_1

    .line 185
    .end local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_3
    if-le p2, p3, :cond_6

    .line 186
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayWidth()I

    move-result v8

    mul-int/2addr v8, p3

    div-int/2addr v8, p2

    mul-int v0, v8, v3

    .line 191
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayHeight()I

    move-result v8

    if-le v0, v8, :cond_4

    .line 192
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayHeight()I

    move-result v0

    .line 194
    :cond_4
    mul-int/lit16 v8, v0, 0x3e8

    iget-object v9, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getYdpi()I

    move-result v9

    div-int v5, v8, v9

    .line 195
    const-string v8, "MVMeasureSize"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "measureSurfaceSizeRatio [Server] adjustHeight : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", serverAdjustInch : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ydpi : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v10}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getYdpi()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    move v7, v5

    .line 199
    .local v7, "smallestInch":I
    const/4 v6, 0x0

    .line 200
    .local v6, "smallestId":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 201
    .restart local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustHeightInch()I

    move-result v8

    if-ge v8, v7, :cond_5

    .line 202
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustHeightInch()I

    move-result v7

    .line 203
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v6

    goto :goto_3

    .line 188
    .end local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    .end local v6    # "smallestId":I
    .end local v7    # "smallestInch":I
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getDisplayWidth()I

    move-result v8

    mul-int/2addr v8, p2

    div-int/2addr v8, p3

    mul-int v0, v8, v3

    goto :goto_2

    .line 206
    .restart local v6    # "smallestId":I
    .restart local v7    # "smallestInch":I
    :cond_7
    const-string v8, "MVMeasureSize"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "measureSurfaceSizeRatio smallestId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", smallestInch : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    if-nez v6, :cond_9

    .line 210
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v8, v11}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMeasuredHeightRatio(I)V

    .line 215
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;

    .line 216
    .restart local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->isGroupMember()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 217
    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getId()I

    move-result v8

    if-ne v8, v6, :cond_a

    .line 218
    invoke-virtual {v4, v11}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMeasuredHeightRatio(I)V

    goto :goto_5

    .line 212
    .end local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    mul-int/lit16 v9, v7, 0x3e8

    div-int/2addr v9, v5

    invoke-virtual {v8, v9}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->setMeasuredHeightRatio(I)V

    goto :goto_4

    .line 220
    .restart local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_a
    mul-int/lit16 v8, v7, 0x3e8

    invoke-virtual {v4}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->getAdjustHeightInch()I

    move-result v9

    div-int/2addr v8, v9

    invoke-virtual {v4, v8}, Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;->setMeasuredHeightRatio(I)V

    goto :goto_5

    .line 224
    .end local v4    # "playerServer":Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;
    :cond_b
    const-string v8, "MVMeasureSize"

    const-string v9, "measureSurfaceSizeRatio - out"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return-void
.end method


# virtual methods
.method public measureMultiVisionSize(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p1, "playerList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/mv/player/multivision/MVPlayerServer;>;"
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getVideoWidth()I

    move-result v1

    .line 229
    .local v1, "videoWidth":I
    iget-object v2, p0, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->mMVUtil:Lcom/sec/android/app/mv/player/multivision/MVUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/multivision/MVUtil;->getVideoHeight()I

    move-result v0

    .line 231
    .local v0, "videoHeight":I
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 232
    :cond_0
    const-string v2, "MVMeasureSize"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "measureMultiVisionSize failed : videoWidth = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", videoHeight = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-direct {p0, p1, v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->measureCropSize(Ljava/util/List;II)V

    .line 240
    invoke-direct {p0, p1, v1, v0}, Lcom/sec/android/app/mv/player/multivision/MVMeasureSize;->measureSurfaceSizeRatio(Ljava/util/List;II)V

    goto :goto_0
.end method
