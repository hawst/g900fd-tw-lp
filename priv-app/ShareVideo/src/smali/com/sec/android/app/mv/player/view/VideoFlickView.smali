.class public Lcom/sec/android/app/mv/player/view/VideoFlickView;
.super Ljava/lang/Object;
.source "VideoFlickView.java"


# static fields
.field private static final CHANGE_LEFT_IMAGE:I = 0x1

.field private static final CHANGE_RIGHT_IMAGE:I = 0x2

.field private static final SEC:Ljava/lang/String; = "5"


# instance fields
.field private count:I

.field private mContext:Landroid/content/Context;

.field private mFlickView:Landroid/widget/RelativeLayout;

.field private final mHandler:Landroid/os/Handler;

.field private mLeftArrayListImageView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mLeftFlickLayout:Landroid/widget/RelativeLayout;

.field private mLeftText:Landroid/widget/TextView;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mRightArrayListImageView:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mRightFlickLayout:Landroid/widget/RelativeLayout;

.field private mRightText:Landroid/widget/TextView;

.field mTempFlickLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mContext:Landroid/content/Context;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mTempFlickLayout:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I

    .line 90
    new-instance v0, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView$1;-><init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;

    .line 44
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/mv/player/view/VideoFlickView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I

    return p1
.end method

.method static synthetic access$008(Lcom/sec/android/app/mv/player/view/VideoFlickView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->count:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/view/VideoFlickView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/view/VideoFlickView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private initViews()V
    .locals 4

    .prologue
    const v3, 0x7f0a007c

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0078

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d007c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d007d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0081

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "5 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "5 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d007b

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d007a

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0079

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0080

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d007f

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightArrayListImageView:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d007e

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 48
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 50
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v2, 0x7f030018

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    .line 52
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 53
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView;->initViews()V

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    return-void
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 88
    :cond_0
    return-void
.end method

.method public leftFlickView()V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView$2;-><init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 154
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 82
    return-void
.end method

.method public rightFlickView()V
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/VideoFlickView$3;-><init>(Lcom/sec/android/app/mv/player/view/VideoFlickView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 185
    return-void
.end method
