.class Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;
.super Landroid/os/Handler;
.source "VideoServiceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1049
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_6

    .line 1051
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayingChecker()V

    .line 1054
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1056
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isSurfaceExist()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1057
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler - STARTPLAYBACK"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1061
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mCurPlayingPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$500(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->checkDRMFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1062
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;->openPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1063
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->setTitleName()V

    .line 1103
    :cond_0
    :goto_0
    return-void

    .line 1065
    :cond_1
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler. openPath return false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1072
    :catch_0
    move-exception v0

    .line 1073
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteException occured  5 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1069
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler() - checkDRMFile return false."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1077
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v1

    if-gez v1, :cond_4

    .line 1078
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler. mPlayingCheckTime is below 0. stopPlayingChecker() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 1081
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 1082
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$600(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v1

    invoke-interface {v1, v4, v4}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$OnNotificationListener;->onNotification(II)V

    goto :goto_0

    .line 1086
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const/16 v2, 0x12c

    # -= operator for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$020(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;I)I

    .line 1087
    const-string v2, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler Error state. mPlayingCheckTime : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", surfaceExist : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getSurfaceView()Lcom/sec/android/app/mv/player/view/VideoSurface;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", sService : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->sService:Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/service/IMoviePlaybackService;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->startPlayback()Z

    goto/16 :goto_0

    .line 1087
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getSurfaceView()Lcom/sec/android/app/mv/player/view/VideoSurface;

    move-result-object v1

    goto :goto_1

    .line 1097
    :cond_6
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 1098
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$700(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v1

    if-ne v1, v3, :cond_7

    .line 1099
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # invokes: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->ffSeek()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$800(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    goto/16 :goto_0

    .line 1100
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->mLongSeekMode:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$700(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1101
    iget-object v1, p0, Lcom/sec/android/app/mv/player/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    # invokes: Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->rewSeek()V
    invoke-static {v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->access$900(Lcom/sec/android/app/mv/player/util/VideoServiceUtil;)V

    goto/16 :goto_0
.end method
