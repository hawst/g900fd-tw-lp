.class public Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;
.super Landroid/app/AlertDialog;
.source "ShareVideoGuidePopup.java"


# instance fields
.field private mCheckBoxText:Landroid/widget/TextView;

.field private mCheckbox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mGuideImage:Landroid/widget/ImageView;

.field private mGuidePopUpText:Landroid/widget/TextView;

.field private mPopupView:Landroid/view/View;

.field private mSpfManager:Lcom/sec/android/app/mv/player/db/SharedPreference;

.field private mTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckBoxText:Landroid/widget/TextView;

    .line 26
    iput-object v2, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mGuidePopUpText:Landroid/widget/TextView;

    .line 71
    new-instance v0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mContext:Landroid/content/Context;

    .line 31
    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->setTitle(I)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030013

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    const v1, 0x7f0d003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mGuidePopUpText:Landroid/widget/TextView;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mGuidePopUpText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0085

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    const v1, 0x7f0d003f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mGuideImage:Landroid/widget/ImageView;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mGuideImage:Landroid/widget/ImageView;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckBoxText:Landroid/widget/TextView;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckBoxText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckBoxText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/mv/player/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/mv/player/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mSpfManager:Lcom/sec/android/app/mv/player/db/SharedPreference;

    .line 61
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0060

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mPopupView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->setView(Landroid/view/View;)V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;)Lcom/sec/android/app/mv/player/db/SharedPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/ShareVideoGuidePopup;->mSpfManager:Lcom/sec/android/app/mv/player/db/SharedPreference;

    return-object v0
.end method
