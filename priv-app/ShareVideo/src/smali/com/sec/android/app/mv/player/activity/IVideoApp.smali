.class public interface abstract Lcom/sec/android/app/mv/player/activity/IVideoApp;
.super Ljava/lang/Object;
.source "IVideoApp.java"


# virtual methods
.method public abstract createKDrmPopup(I)V
.end method

.method public abstract exitApp()V
.end method

.method public abstract getAppContext()Landroid/content/Context;
.end method

.method public abstract getAppWindow()Landroid/view/Window;
.end method

.method public abstract getAudioUtil()Lcom/sec/android/app/mv/player/util/VideoAudioUtil;
.end method

.method public abstract getKDrmUtil()Lcom/sec/android/app/mv/player/util/VideoKDRMUtil;
.end method

.method public abstract getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;
.end method

.method public abstract getSurfaceView()Lcom/sec/android/app/mv/player/view/VideoSurface;
.end method

.method public abstract getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;
.end method

.method public abstract initSubtitle()V
.end method
