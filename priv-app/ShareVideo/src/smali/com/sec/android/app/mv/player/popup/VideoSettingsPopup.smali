.class public Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;
    }
.end annotation


# static fields
.field public static final MENU_BRIGHTNESS:I = 0x0

.field public static final MENU_MAX:I = 0x2

.field public static final MENU_SUBTITLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoSettingsPopup"

.field private static final TITLE:Ljava/lang/String; = "MENU_TITLE"

.field private static final VALUE:Ljava/lang/String; = "MENU_VALUE"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

.field private mAvailableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIsHeadsetUnplugged:Z

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOnPopupCreatedListener:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

.field mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

.field private mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

.field private mUIInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mVisibleMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V
    .locals 2
    .param p1, "app"    # Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    .line 337
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$3;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 364
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$4;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    .line 55
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    .line 56
    invoke-interface {p1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getServiceUtil()Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->initVisbleMap()V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->showPopup(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)Lcom/sec/android/app/mv/player/activity/IVideoApp;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    return-object v0
.end method

.method private dismissSubPopup()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->dismiss()V

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    .line 195
    :cond_0
    return-void
.end method

.method private initBrightness()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 258
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 259
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 260
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "MENU_TITLE"

    iget-object v6, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0013

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const/4 v4, 0x0

    .line 263
    .local v4, "text":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v5, :cond_0

    .line 264
    new-instance v5, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v6}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;-><init>(Landroid/view/Window;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 267
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 268
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 276
    :goto_0
    const-string v5, "MENU_VALUE"

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    .end local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "text":Ljava/lang/String;
    :cond_1
    return-void

    .line 270
    .restart local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "text":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    .line 271
    .local v1, "level":I
    iget-object v5, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v3

    .line 272
    .local v3, "maxLevel":I
    int-to-float v5, v1

    int-to-float v6, v3

    div-float/2addr v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    float-to-int v0, v5

    .line 273
    .local v0, "currentLevel":I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private initMenuData()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 234
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    .line 239
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    .line 245
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->initBrightness()V

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->showSubtitleMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->initSubtitles()V

    .line 250
    :cond_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method private initSubtitles()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 308
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0045

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v2}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isSubtitleActivated()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    .local v1, "text":Ljava/lang/String;
    :goto_0
    const-string v2, "MENU_VALUE"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    return-void

    .line 315
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private initVisbleMap()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->setAllVisible()V

    .line 221
    :cond_0
    return-void
.end method

.method private openPopup(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismissSubPopup()V

    .line 116
    packed-switch p1, :pswitch_data_0

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;->onPopupCreated(Lcom/sec/android/app/mv/player/popup/IVideoPopup;)V

    .line 183
    :cond_0
    return-void

    .line 118
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    if-nez v0, :cond_1

    .line 119
    new-instance v0, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v1}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getAppWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;-><init>(Landroid/view/Window;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    .line 121
    :cond_1
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/mv/player/popup/VideoBrightnessPopup;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;Lcom/sec/android/app/mv/player/util/VideoBrightnessUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    goto :goto_0

    .line 140
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getMainVideoView()Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    .line 143
    :cond_2
    new-instance v0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-direct {v0, v1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;-><init>(Lcom/sec/android/app/mv/player/activity/IVideoApp;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showPopup(I)V
    .locals 2
    .param p1, "selected"    # I

    .prologue
    .line 107
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 109
    .local v0, "id":I
    invoke-direct {p0, v0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->openPopup(I)V

    .line 111
    .end local v0    # "id":I
    :cond_0
    return-void
.end method

.method private showSubtitleMenu()Z
    .locals 1

    .prologue
    .line 253
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isSingleVisionMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/activity/IVideoApp;->getUtil()Lcom/sec/android/app/mv/player/util/VideoUtility;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/VideoUtility;->getSubtitleUtil()Lcom/sec/android/app/mv/player/util/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/util/SubtitleUtil;->isMultivisionSubtitleShowingDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public MenuVisibilityCount()I
    .locals 4

    .prologue
    .line 224
    const/4 v0, 0x0

    .line 225
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 225
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 229
    :cond_1
    return v0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 188
    :cond_0
    return-void
.end method

.method public isPopupShowing()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 103
    :goto_0
    return v0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/mv/player/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/mv/player/popup/IVideoPopup;->isShowing()Z

    move-result v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAllPopup()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismissSubPopup()V

    .line 204
    return-void
.end method

.method public setAllVisible()V
    .locals 4

    .prologue
    .line 212
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_0
    return-void
.end method

.method public setDismiss4UnpluggedHeadset()V
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    .line 472
    return-void
.end method

.method public setMenuVisibility(IZ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mVisibleMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    :cond_0
    return-void
.end method

.method public setOnPopupCreatedListener(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .prologue
    .line 467
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .line 468
    return-void
.end method

.method public showPopup()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    const-string v2, "VideoSettingsPopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->initMenuData()V

    .line 64
    const/4 v0, 0x0

    .line 65
    .local v0, "adapter":Landroid/widget/SimpleAdapter;
    new-instance v0, Landroid/widget/SimpleAdapter;

    .end local v0    # "adapter":Landroid/widget/SimpleAdapter;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    const v3, 0x7f030011

    new-array v4, v9, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "MENU_TITLE"

    aput-object v7, v4, v5

    const-string v5, "MENU_VALUE"

    aput-object v5, v4, v8

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 69
    .restart local v0    # "adapter":Landroid/widget/SimpleAdapter;
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v6, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    .local v6, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0083

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 72
    new-instance v1, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$1;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    const v1, 0x7f0a0016

    new-instance v2, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup$2;-><init>(Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;)V

    invoke-virtual {v6, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 86
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 93
    return-void

    .line 65
    :array_0
    .array-data 4
        0x7f0d003a
        0x7f0d003b
    .end array-data
.end method
