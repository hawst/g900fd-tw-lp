.class Lcom/sec/android/app/mv/player/view/VideoBtnController$24;
.super Landroid/os/Handler;
.source "VideoBtnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1985
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 1990
    const/4 v1, 0x0

    .line 1992
    .local v1, "progress":I
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_1

    .line 1994
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 1995
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 1996
    .local v2, "width":I
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1998
    .local v0, "height":I
    const-string v3, "VideoPlayerBtnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " mainthread bitmap width : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/ImageView;

    move-result-object v3

    if-nez v3, :cond_2

    .line 2001
    :cond_0
    const-string v3, "VideoPlayerBtnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mProgressPreviewShowTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2002
    const-string v3, "VideoPlayerBtnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mProgressPreviewImage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033
    .end local v0    # "height":I
    .end local v2    # "width":I
    :cond_1
    :goto_0
    return-void

    .line 2006
    .restart local v0    # "height":I
    .restart local v2    # "width":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget v6, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3100(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2007
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2009
    const-string v3, "VideoPlayerBtnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " mainthread position : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010
    const-string v3, "VideoPlayerBtnController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " mainthread currenttime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget v7, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->stringForTimeCur(I)Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3100(Lcom/sec/android/app/mv/player/view/VideoBtnController;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextWidth:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 2013
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    .line 2016
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-static {}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    :goto_1
    # invokes: Lcom/sec/android/app/mv/player/view/VideoBtnController;->updateProgressbarPreviewView(IZ)I
    invoke-static {v5, v6, v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3500(Lcom/sec/android/app/mv/player/view/VideoBtnController;IZ)I

    move-result v1

    .line 2018
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/16 v5, 0x19

    invoke-virtual {v3, v1, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2020
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v3

    if-ne v3, v4, :cond_5

    .line 2021
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020032

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2028
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2500(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v4

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mLeftPadding:I
    invoke-static {v6}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRightPadding:I
    invoke-static {v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2100(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    iget-object v7, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTopPadding:I
    invoke-static {v7}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3700(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mBottomPadding:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v7, v0

    iget-object v8, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mTimetextHeight:I
    invoke-static {v8}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2300(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v5, v6, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v5}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2016
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 2022
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressThumbViewState:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$3600(Lcom/sec/android/app/mv/player/view/VideoBtnController;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 2023
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020033

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 2025
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$24;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$2400(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020031

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2
.end method
