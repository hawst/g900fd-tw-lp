.class Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VideoSubtitlePopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectSubtitleAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151
    .local p3, "subtitleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iput-object p1, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    .line 1152
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1153
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 1156
    if-nez p2, :cond_0

    .line 1157
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1158
    .local v6, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f03001c

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1161
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getCount()I

    move-result v7

    if-nez v7, :cond_2

    .line 1190
    :cond_1
    :goto_0
    return-object p2

    .line 1163
    :cond_2
    const v7, 0x7f0d0095

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1164
    .local v4, "subtitleName":Landroid/widget/TextView;
    const v7, 0x7f0d0096

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1165
    .local v5, "subtitlePath":Landroid/widget/TextView;
    const v7, 0x7f0d0097

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 1167
    .local v0, "checkImage":Landroid/widget/CheckedTextView;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1168
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1170
    .local v2, "path":Ljava/lang/String;
    const/16 v7, 0x2f

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1171
    .local v3, "pos":I
    if-lez v3, :cond_3

    .line 1172
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v2, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1175
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1176
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1178
    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0a0046

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1179
    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1182
    :cond_4
    if-nez p1, :cond_5

    invoke-virtual {p0, p1}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$2500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1183
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 1187
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;

    # getter for: Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;->access$500(Lcom/sec/android/app/mv/player/popup/VideoSubtitlePopup;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a008d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1188
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1185
    :cond_5
    invoke-virtual {v0, v10}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1
.end method
