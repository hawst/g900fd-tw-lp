.class Lcom/sec/android/app/mv/player/view/VideoBtnController$12;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1164
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v2, 0xbb8

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    iget-object v0, v0, Lcom/sec/android/app/mv/player/view/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1167
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1186
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 1169
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1173
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1177
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$200(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->controlRequest(I)V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/VideoBtnController$12;->this$0:Lcom/sec/android/app/mv/player/view/VideoBtnController;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/VideoBtnController;->access$000(Lcom/sec/android/app/mv/player/view/VideoBtnController;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 1167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
