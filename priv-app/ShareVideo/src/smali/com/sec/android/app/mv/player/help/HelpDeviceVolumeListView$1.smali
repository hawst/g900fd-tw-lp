.class Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;
.super Ljava/lang/Object;
.source "HelpVolumeDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "dialog"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I
    invoke-static {v3}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v2

    .line 154
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    iget-object v4, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 155
    .local v0, "seekBar":Landroid/widget/SeekBar;
    const/4 v1, 0x0

    .line 156
    .local v1, "volume":I
    if-eqz v0, :cond_2

    .line 157
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 160
    :cond_2
    packed-switch p2, :pswitch_data_0

    .line 182
    const/4 v2, 0x0

    goto :goto_0

    .line 162
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)I

    move-result v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->setVolumeProgress(II)V

    .line 164
    if-eqz v0, :cond_0

    .line 165
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 171
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mHelpVolumeObject:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;
    invoke-static {v3}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$100(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    # getter for: Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->mIndex:I
    invoke-static {v4}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->access$000(Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;)I

    move-result v4

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView$HelpVolumeObject;->setVolumeProgress(II)V

    .line 173
    if-eqz v0, :cond_0

    .line 174
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
