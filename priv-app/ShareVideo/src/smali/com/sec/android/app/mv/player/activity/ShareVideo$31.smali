.class Lcom/sec/android/app/mv/player/activity/ShareVideo$31;
.super Landroid/database/ContentObserver;
.source "ShareVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 3148
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 3150
    const-string v0, "ShareVideo"

    const-string v1, "registerContentObserver onChange()"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3152
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->V_ROTATE_SECOND_SCREEN:Z

    if-eqz v0, :cond_1

    .line 3153
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$1400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/mv/player/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3154
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen()V

    .line 3161
    :goto_0
    return-void

    .line 3156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateSecondScreen()V

    goto :goto_0

    .line 3159
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$31;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->rotateScreen()V

    goto :goto_0
.end method
