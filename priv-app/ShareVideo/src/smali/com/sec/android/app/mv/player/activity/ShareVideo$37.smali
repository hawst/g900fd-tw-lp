.class Lcom/sec/android/app/mv/player/activity/ShareVideo$37;
.super Ljava/lang/Object;
.source "ShareVideo.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/activity/ShareVideo;->sendMessageDetermineMasterNode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/activity/ShareVideo;)V
    .locals 0

    .prologue
    .line 3657
    iput-object p1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 3659
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3660
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "ServerTitle"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4200(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3661
    const-string v1, "ServerIP"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mServerIP:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4300(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3662
    const-string v1, "rtsp://multivision"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3663
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3664
    const-string v1, "DeviceName"

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->deviceName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4400(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3665
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mShareVideoManager:Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4600(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    # getter for: Lcom/sec/android/app/mv/player/activity/ShareVideo;->mOwnNodeName:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->access$4500(Lcom/sec/android/app/mv/player/activity/ShareVideo;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/mv/player/multivision/manager/ShareVideoManager;->setNodeTypeChangeToClient(Ljava/lang/String;Z)V

    .line 3666
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->finish()V

    .line 3667
    iget-object v1, p0, Lcom/sec/android/app/mv/player/activity/ShareVideo$37;->this$0:Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->startActivity(Landroid/content/Intent;)V

    .line 3668
    return-void
.end method
