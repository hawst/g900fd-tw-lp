.class public Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;
.super Lcom/sec/android/app/mv/player/widget/TwHelpDialog;
.source "TwHelpAnimatedDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnimCount:I

.field private mCurrentStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "step"    # I

    .prologue
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;-><init>(Landroid/content/Context;)V

    .line 32
    iput p3, p0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mCurrentStep:I

    .line 34
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setContentView(I)V

    .line 35
    sget-object v0, Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;->OPAQUE:Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setTouchTransparencyMode(Lcom/sec/android/app/mv/player/widget/TwHelpDialog$TouchMode;)V

    .line 36
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setShowWrongInputToast(Z)V

    .line 37
    invoke-virtual {p0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setIgnoreHoverEvt(Z)V

    .line 38
    invoke-virtual {p0, p2}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 39
    return-void
.end method

.method static synthetic access$008(Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mAnimCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mAnimCount:I

    return v0
.end method

.method private getMargin(IIII)[I
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 317
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    aput p3, v0, v1

    const/4 v1, 0x3

    aput p4, v0, v1

    return-object v0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 27

    .prologue
    .line 43
    sget-object v23, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->TAG:Ljava/lang/String;

    const-string v24, "onAttachedToWindow()"

    invoke-static/range {v23 .. v24}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mCurrentStep:I

    move/from16 v23, v0

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    invoke-super/range {p0 .. p0}, Lcom/sec/android/app/mv/player/widget/TwHelpDialog;->onAttachedToWindow()V

    .line 50
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 52
    .local v8, "context":Landroid/content/Context;
    const v23, 0x7f0d000c

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/mv/player/widget/TwTouchPunchView;

    .line 53
    .local v14, "punchView":Lcom/sec/android/app/mv/player/widget/TwTouchPunchView;
    const v23, 0x7f0d000d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 55
    .local v9, "imageViewPunch":Landroid/view/View;
    const v23, 0x7f0d000e

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v22

    .line 56
    .local v22, "summaryView":Landroid/view/View;
    const v23, 0x7f0d0010

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 57
    .local v18, "summaryArrow":Landroid/widget/ImageView;
    const v23, 0x7f0d000f

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 59
    .local v21, "summaryText":Landroid/widget/TextView;
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v14}, Lcom/sec/android/app/mv/player/widget/TwTouchPunchView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    .local v10, "paramsPunchView":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    .local v13, "paramsSummaryView":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    .local v12, "paramsSummaryText":Landroid/widget/RelativeLayout$LayoutParams;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    .local v11, "paramsSummaryArrow":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 70
    .local v16, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mCurrentStep:I

    move/from16 v23, v0

    packed-switch v23, :pswitch_data_0

    .line 250
    :cond_2
    :goto_1
    invoke-virtual {v14, v10}, Lcom/sec/android/app/mv/player/widget/TwTouchPunchView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    invoke-virtual {v9, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 252
    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 253
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    const v23, 0x7f040002

    move/from16 v0, v23

    invoke-static {v8, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v7

    .line 258
    .local v7, "animStart":Landroid/view/animation/Animation;
    const/high16 v23, 0x7f040000

    move/from16 v0, v23

    invoke-static {v8, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    .line 260
    .local v5, "animAppear":Landroid/view/animation/Animation;
    const v23, 0x7f040001

    move/from16 v0, v23

    invoke-static {v8, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 263
    .local v6, "animDisappear":Landroid/view/animation/Animation;
    new-instance v23, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9, v5}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$1;-><init>(Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;Landroid/view/View;Landroid/view/animation/Animation;)V

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 278
    new-instance v23, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9, v6}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$2;-><init>(Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;Landroid/view/View;Landroid/view/animation/Animation;)V

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 295
    new-instance v23, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9, v5}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog$3;-><init>(Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;Landroid/view/View;Landroid/view/animation/Animation;)V

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 310
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->mAnimCount:I

    move/from16 v23, v0

    if-nez v23, :cond_0

    .line 311
    invoke-virtual {v9, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 312
    const v23, 0x7f040003

    move/from16 v0, v23

    invoke-static {v8, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 72
    .end local v5    # "animAppear":Landroid/view/animation/Animation;
    .end local v6    # "animDisappear":Landroid/view/animation/Animation;
    .end local v7    # "animStart":Landroid/view/animation/Animation;
    :pswitch_0
    const v23, 0x7f080067

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 73
    const v23, 0x7f02002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 74
    const v23, 0x7f0a0035

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 104
    const v23, 0x7f08005c

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f08005f

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 109
    const/16 v23, 0x9

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 112
    const/16 v23, 0x3

    const v24, 0x7f0d000c

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 115
    const/16 v23, 0x9

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 116
    const/16 v23, 0x0

    const v24, 0x7f080065

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 122
    const/16 v23, 0x9

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 123
    const v23, 0x7f080059

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 129
    sget-boolean v23, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v23, :cond_2

    .line 130
    const v23, 0x7f08005d

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f080060

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 136
    const/16 v23, 0x0

    const v24, 0x7f080065

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 141
    const v23, 0x7f08005a

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1

    .line 151
    :pswitch_1
    const v23, 0x7f02002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 152
    const v23, 0x7f0a0036

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 155
    const/16 v23, 0x0

    const v24, 0x7f080063

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f080062

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 158
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 159
    invoke-static {v8}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v23

    if-eqz v23, :cond_3

    .line 160
    const v23, 0x7f080064

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 161
    const v23, 0x7f080064

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 165
    :cond_3
    const/16 v23, 0x3

    const v24, 0x7f0d000c

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 168
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 169
    const/16 v23, 0x0

    const v24, 0x7f080065

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 173
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 174
    const/16 v23, 0x0

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f080061

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1

    .line 180
    :pswitch_2
    const v23, 0x7f02002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 181
    const v23, 0x7f0a0039

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 184
    const/16 v23, 0x0

    const v24, 0x7f08006c

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f08006b

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 187
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 188
    invoke-static {v8}, Lcom/sec/android/app/mv/player/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v23

    if-eqz v23, :cond_4

    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v23

    if-eqz v23, :cond_4

    .line 189
    const v23, 0x7f08006d

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 191
    const v23, 0x7f08006d

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    move/from16 v0, v23

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 196
    :cond_4
    const/16 v23, 0xe

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 197
    const/16 v23, 0x3

    const v24, 0x7f0d000c

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 200
    const/16 v23, 0xe

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 201
    const/16 v23, 0x0

    const v24, 0x7f080065

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 205
    const/16 v23, 0xb

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 206
    const/16 v23, 0x0

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f08006a

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1

    .line 212
    :pswitch_3
    const v23, 0x7f02002d

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 213
    const v23, 0x7f0a0034

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 215
    const/16 v17, 0xb

    .line 218
    .local v17, "rules":I
    const/16 v23, 0x0

    const v24, 0x7f080065

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getMargin(IIII)[I

    move-result-object v20

    .line 221
    .local v20, "summaryMargin":[I
    sget-boolean v23, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v23, :cond_5

    .line 222
    const/16 v17, 0x9

    .line 223
    const v23, 0x7f080056

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f080055

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getMargin(IIII)[I

    move-result-object v15

    .line 224
    .local v15, "punchViewMargin":[I
    const v23, 0x7f080054

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getMargin(IIII)[I

    move-result-object v19

    .line 231
    .local v19, "summaryArrowMargin":[I
    :goto_2
    const/16 v23, 0x0

    aget v23, v15, v23

    const/16 v24, 0x1

    aget v24, v15, v24

    const/16 v25, 0x2

    aget v25, v15, v25

    const/16 v26, 0x3

    aget v26, v15, v26

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 232
    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 235
    const/16 v23, 0x3

    const v24, 0x7f0d000c

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 238
    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 239
    const/16 v23, 0x0

    aget v23, v20, v23

    const/16 v24, 0x1

    aget v24, v20, v24

    const/16 v25, 0x2

    aget v25, v20, v25

    const/16 v26, 0x3

    aget v26, v20, v26

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 242
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 243
    const/16 v23, 0x0

    aget v23, v19, v23

    const/16 v24, 0x1

    aget v24, v19, v24

    const/16 v25, 0x2

    aget v25, v19, v25

    const/16 v26, 0x3

    aget v26, v19, v26

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1

    .line 226
    .end local v15    # "punchViewMargin":[I
    .end local v19    # "summaryArrowMargin":[I
    :cond_5
    const/16 v23, 0x0

    const v24, 0x7f080055

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f080056

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getMargin(IIII)[I

    move-result-object v15

    .line 227
    .restart local v15    # "punchViewMargin":[I
    const/16 v23, 0x0

    const v24, 0x7f080057

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    const v25, 0x7f080054

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/mv/player/widget/TwHelpAnimatedDialog;->getMargin(IIII)[I

    move-result-object v19

    .restart local v19    # "summaryArrowMargin":[I
    goto/16 :goto_2

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
