.class Lcom/sec/android/app/mv/player/view/MainVideoView$3;
.super Landroid/os/Handler;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 1188
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1190
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1296
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1195
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/mv/player/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$000(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto :goto_0

    .line 1201
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/mv/player/util/VideoServiceUtil;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 1207
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_1

    .line 1208
    const-string v0, "MainVideoView"

    const-string v1, "mHandler - exit player by H/W back key"

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a006f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0060

    new-instance v2, Lcom/sec/android/app/mv/player/view/MainVideoView$3$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$3$2;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/mv/player/view/MainVideoView$3$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/mv/player/view/MainVideoView$3$1;-><init>(Lcom/sec/android/app/mv/player/view/MainVideoView$3;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->removeHandler()V

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto :goto_0

    .line 1246
    :cond_1
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHandler - update timer for expiring. current timer ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v2}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1400(Lcom/sec/android/app/mv/player/view/MainVideoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1400(Lcom/sec/android/app/mv/player/view/MainVideoView;)I

    move-result v0

    if-lez v0, :cond_2

    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1400(Lcom/sec/android/app/mv/player/view/MainVideoView;)I

    move-result v1

    add-int/lit16 v1, v1, -0xc8

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1402(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    .line 1250
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1252
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1402(Lcom/sec/android/app/mv/player/view/MainVideoView;I)I

    goto/16 :goto_0

    .line 1258
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$400(Lcom/sec/android/app/mv/player/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1260
    sget-boolean v0, Lcom/sec/android/app/mv/player/common/feature/Feature;->F_SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mv/player/activity/ShareVideo;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/activity/ShareVideo;->getPlayerListState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1264
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->attachVideoGestureView()V

    .line 1267
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->forceHideController()V

    .line 1271
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$800(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/mv/player/view/MainVideoView$VideoGestureMode;

    if-ne v0, v1, :cond_6

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->showVolume()V

    .line 1276
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # setter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$602(Lcom/sec/android/app/mv/player/view/MainVideoView;Z)Z

    goto/16 :goto_0

    .line 1274
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/mv/player/view/VideoGestureView;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoGestureView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoGestureView;->showBrightness()V

    goto :goto_1

    .line 1282
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->hideController()V

    goto/16 :goto_0

    .line 1285
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/mv/player/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 1290
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/mv/player/view/MainVideoView$3;->this$0:Lcom/sec/android/app/mv/player/view/MainVideoView;

    # getter for: Lcom/sec/android/app/mv/player/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/mv/player/view/VideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/view/MainVideoView;->access$1500(Lcom/sec/android/app/mv/player/view/MainVideoView;)Lcom/sec/android/app/mv/player/view/VideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/view/VideoSurface;->requestLayout()V

    goto/16 :goto_0

    .line 1191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
