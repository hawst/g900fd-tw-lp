.class Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;
.super Ljava/lang/Object;
.source "VideoActionBarView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;)V
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;->this$1:Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 519
    packed-switch p2, :pswitch_data_0

    .line 531
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    .line 521
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;->this$1:Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mApp:Lcom/sec/android/app/mv/player/activity/IVideoApp;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$300(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/activity/IVideoApp;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6$1;->this$1:Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;

    iget-object v1, v1, Lcom/sec/android/app/mv/player/view/VideoActionBarView$6;->this$0:Lcom/sec/android/app/mv/player/view/VideoActionBarView;

    # getter for: Lcom/sec/android/app/mv/player/view/VideoActionBarView;->mMainVideoView:Lcom/sec/android/app/mv/player/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/view/VideoActionBarView;->access$400(Lcom/sec/android/app/mv/player/view/VideoActionBarView;)Lcom/sec/android/app/mv/player/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/mv/player/view/MainVideoView;->changeLockStatusForOnKeyEvent(Landroid/view/KeyEvent;)V

    .line 524
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 525
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
    .end packed-switch
.end method
