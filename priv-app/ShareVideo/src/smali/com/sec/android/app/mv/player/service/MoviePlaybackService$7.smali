.class Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;
.super Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 2337
    iput-object p1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-direct {p0}, Lcom/sec/android/app/mv/player/service/IMoviePlaybackService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 2446
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 2447
    return-void
.end method

.method public checkDRMFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 2442
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->checkDRMFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 2364
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 2348
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getBufferPercentage()I

    move-result v0

    return v0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2478
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getFPS()I
    .locals 1

    .prologue
    .line 2470
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getFPS()I

    move-result v0

    return v0
.end method

.method public getServerTimestampInfo()[J
    .locals 1

    .prologue
    .line 2499
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getServerTimestampInfo()[J

    move-result-object v0

    return-object v0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 2356
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getVideoHeight()I

    move-result v0

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->getVideoWidth()I

    move-result v0

    return v0
.end method

.method public initSelectSubtitleTrack()V
    .locals 1

    .prologue
    .line 2454
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->initSelectSubtitleTrack()V

    .line 2455
    return-void
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 2507
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->initSubtitle(Ljava/lang/String;Z)V

    .line 2508
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 2344
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public isPausedByTransientLossOfFocus()Z
    .locals 3

    .prologue
    .line 2482
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPausedByTransientLossOfFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2483
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$1000(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 2340
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isSecVideo()Z
    .locals 1

    .prologue
    .line 2474
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->isSecVideo()Z

    move-result v0

    return v0
.end method

.method public muteDevice()V
    .locals 1

    .prologue
    .line 2430
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->muteDevice()V

    .line 2431
    return-void
.end method

.method public openPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2422
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->openPath(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 2398
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2399
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2400
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2401
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2402
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x66

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2404
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->pause()V

    .line 2405
    return-void
.end method

.method public play()V
    .locals 4

    .prologue
    .line 2408
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2409
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2410
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2411
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2412
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x65

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2414
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->play()V

    .line 2415
    return-void
.end method

.method public position()J
    .locals 2

    .prologue
    .line 2360
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public realSeek(II)J
    .locals 2
    .param p1, "pos"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 2373
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->realSeek(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 2418
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->reset()V

    .line 2419
    return-void
.end method

.method public resetSubtitle()V
    .locals 1

    .prologue
    .line 2458
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetSubtitle()V

    .line 2459
    return-void
.end method

.method public resetTrackInfo()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2503
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->resetTrackInfo()V

    .line 2504
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 2368
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->seek(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sendMasterClock(JJJJJ)V
    .locals 13
    .param p1, "currentTime"    # J
    .param p3, "clockdelta"    # J
    .param p5, "videoTime"    # J
    .param p7, "currentSecMediaClock"    # J
    .param p9, "currentAudioTimestamp"    # J

    .prologue
    .line 2491
    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    move-wide v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->sendMasterClock(JJJJJ)V

    .line 2492
    return-void
.end method

.method public setAudioChannel(I)V
    .locals 1
    .param p1, "audioChannel"    # I

    .prologue
    .line 2495
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setAudioChannel(I)V

    .line 2496
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 1

    .prologue
    .line 2450
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setInbandSubtitle()V

    .line 2451
    return-void
.end method

.method public setKeepAudioFocus(Z)V
    .locals 1
    .param p1, "keepAudioFocus"    # Z

    .prologue
    .line 2377
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setKeepAudioFocus(Z)V

    .line 2378
    return-void
.end method

.method public setSubtitleLanguage(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2466
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setSubtitleLanguage(I)V

    .line 2467
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 1
    .param p1, "syncTime"    # I

    .prologue
    .line 2462
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setSubtitleSyncTime(I)V

    .line 2463
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2487
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setVideoCrop(IIII)V

    .line 2488
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "vol"    # F

    .prologue
    .line 2426
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->setVolume(F)V

    .line 2427
    return-void
.end method

.method public startPlay()V
    .locals 1

    .prologue
    .line 2438
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->startPlay()V

    .line 2439
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 2381
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDCheckCnt:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3102(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2382
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2383
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2385
    invoke-static {}, Lcom/sec/android/app/mv/player/common/SUtils;->getInstance()Lcom/sec/android/app/mv/player/common/SUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/common/SUtils;->isClient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2386
    const-string v0, "MoviePlaybackService"

    const-string v1, "Share Video status, don\'t send ISSTOPCHECK msg"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    const/16 v1, 0x67

    # setter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mCMDAction:I
    invoke-static {v0, v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$3202(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;I)I

    .line 2394
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->stop()V

    .line 2395
    return-void

    .line 2390
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/mv/player/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public unMuteDevice()V
    .locals 1

    .prologue
    .line 2434
    iget-object v0, p0, Lcom/sec/android/app/mv/player/service/MoviePlaybackService$7;->this$0:Lcom/sec/android/app/mv/player/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/mv/player/service/MoviePlaybackService;->unMuteDevice()V

    .line 2435
    return-void
.end method
