.class Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;
.super Landroid/os/Handler;
.source "HelpVolumeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 50
    # getter for: Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mDismissHandler"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/mv/player/help/HelpPlayerActivity;->exitApp(I)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;->this$0:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
