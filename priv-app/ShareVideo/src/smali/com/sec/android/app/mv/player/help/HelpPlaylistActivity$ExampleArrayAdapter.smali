.class Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "HelpPlaylistActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExampleArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;",
        ">;"
    }
.end annotation


# instance fields
.field private mContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 350
    .local p4, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;>;"
    iput-object p1, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    .line 351
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 352
    iput-object p4, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    .line 353
    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 357
    # getter for: Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->ITEM_SIZE:I
    invoke-static {}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->access$300()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 362
    move-object v6, p2

    .line 364
    .local v6, "v":Landroid/view/View;
    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v8, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->this$0:Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0b0003

    invoke-direct {v2, v8, v9}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 365
    .local v2, "mThemeWrapperContext":Landroid/content/Context;
    if-nez v6, :cond_0

    .line 366
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f030014

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 370
    :cond_0
    const v8, 0x7f0d0029

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 371
    .local v5, "title":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->title:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    const v8, 0x7f0d002a

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 375
    .local v4, "time":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    iget-object v8, v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->time:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    const v8, 0x7f0d0025

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 379
    .local v3, "thumbNail":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    iget v8, v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->thumbNail:I

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 380
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 381
    const v8, 0x7f0d0027

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 384
    const v8, 0x7f0d0026

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    .line 385
    .local v7, "videoProgress":Landroid/widget/ProgressBar;
    const/16 v8, 0x3e8

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 386
    const/16 v8, 0x1f4

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 387
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 390
    const v8, 0x7f0d002b

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 391
    .local v0, "checkBox":Landroid/widget/CheckBox;
    iget-object v8, p0, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;->mContent:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;

    iget-boolean v8, v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleVideo;->isChecked:Z

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 392
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 394
    sget-boolean v8, Lcom/sec/android/app/mv/player/common/feature/Feature;->MODEL_UPGRADE:Z

    if-eqz v8, :cond_1

    .line 395
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 396
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, 0x0

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 399
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    if-nez p1, :cond_2

    .line 400
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 401
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 402
    invoke-virtual {v0}, Landroid/widget/CheckBox;->requestFocus()Z

    .line 403
    new-instance v8, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpPlaylistActivity$ExampleArrayAdapter;)V

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    :cond_2
    return-object v6
.end method
