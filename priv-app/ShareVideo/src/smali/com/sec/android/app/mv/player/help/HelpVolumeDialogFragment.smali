.class public Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
.super Landroid/app/DialogFragment;
.source "HelpVolumeDialogFragment.java"


# static fields
.field private static final DISMISS_DELAYED_TIME:I = 0xbb8

.field private static final DISMISS_DIALOG:I

.field public static final END_FROM_VOLUME_CONTROL:I

.field private static final TAG:Ljava/lang/String;

.field private static frag:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;


# instance fields
.field private listView:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

.field private final mDismissHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 47
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$1;-><init>(Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->mDismissHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->mDismissHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
    .locals 2

    .prologue
    .line 67
    const-class v1, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;-><init>()V

    sput-object v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;

    .line 70
    :cond_0
    sget-object v0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->frag:Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 84
    .local v1, "context":Landroid/content/Context;
    new-instance v5, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-direct {v5, v1}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    .line 86
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030002

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 87
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0d0009

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 88
    .local v3, "linearLayout":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 90
    new-instance v2, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 91
    .local v2, "dialog":Landroid/app/Dialog;
    invoke-virtual {v2, v8}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 92
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 93
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 94
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02005a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96
    const v5, 0x1030129

    invoke-virtual {p0, v8, v5}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->setStyle(II)V

    .line 98
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 99
    .local v0, "WMLP":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x11

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 100
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->listView:Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;

    invoke-virtual {v5}, Lcom/sec/android/app/mv/player/help/HelpDeviceVolumeListView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08003d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 102
    const/4 v5, -0x2

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 103
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->mDismissHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 106
    iget-object v5, p0, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->mDismissHandler:Landroid/os/Handler;

    const-wide/16 v6, 0xbb8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 108
    new-instance v5, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment$2;-><init>(Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;)V

    invoke-virtual {v2, v5}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 124
    return-object v2
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/mv/player/help/HelpVolumeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 77
    :cond_0
    return-void
.end method
