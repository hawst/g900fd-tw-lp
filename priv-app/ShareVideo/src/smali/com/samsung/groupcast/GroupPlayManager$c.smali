.class public final enum Lcom/samsung/groupcast/GroupPlayManager$c;
.super Ljava/lang/Enum;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/GroupPlayManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/GroupPlayManager$c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/samsung/groupcast/GroupPlayManager$c;

.field public static final enum b:Lcom/samsung/groupcast/GroupPlayManager$c;

.field public static final enum c:Lcom/samsung/groupcast/GroupPlayManager$c;

.field public static final enum d:Lcom/samsung/groupcast/GroupPlayManager$c;

.field public static final enum e:Lcom/samsung/groupcast/GroupPlayManager$c;

.field private static final synthetic g:[Lcom/samsung/groupcast/GroupPlayManager$c;


# instance fields
.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    const-string v1, "REGISTER"

    const-string v2, "com.samsung.groupcast.action.GP_REGISTER"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/groupcast/GroupPlayManager$c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->a:Lcom/samsung/groupcast/GroupPlayManager$c;

    .line 56
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    const-string v1, "UPDATE"

    const-string v2, "com.samsung.groupcast.action.GP_UPDATE"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/groupcast/GroupPlayManager$c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->b:Lcom/samsung/groupcast/GroupPlayManager$c;

    .line 57
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    const-string v1, "SERVICE"

    const-string v2, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/groupcast/GroupPlayManager$c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->c:Lcom/samsung/groupcast/GroupPlayManager$c;

    .line 58
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    const-string v1, "NETWORK_STATUS"

    const-string v2, "com.samsung.groupcast.action.GP_NETWORK_STATUS"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/groupcast/GroupPlayManager$c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->d:Lcom/samsung/groupcast/GroupPlayManager$c;

    .line 59
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    const-string v1, "UNREGISTER"

    const-string v2, "com.samsung.groupcast.action.GP_UNREGISTER"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/groupcast/GroupPlayManager$c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->e:Lcom/samsung/groupcast/GroupPlayManager$c;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/groupcast/GroupPlayManager$c;

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->a:Lcom/samsung/groupcast/GroupPlayManager$c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->b:Lcom/samsung/groupcast/GroupPlayManager$c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->c:Lcom/samsung/groupcast/GroupPlayManager$c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->d:Lcom/samsung/groupcast/GroupPlayManager$c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->e:Lcom/samsung/groupcast/GroupPlayManager$c;

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->g:[Lcom/samsung/groupcast/GroupPlayManager$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput-object p3, p0, Lcom/samsung/groupcast/GroupPlayManager$c;->f:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/GroupPlayManager$c;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/GroupPlayManager$c;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/GroupPlayManager$c;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->g:[Lcom/samsung/groupcast/GroupPlayManager$c;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/groupcast/GroupPlayManager$c;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$c;->f:Ljava/lang/String;

    return-object v0
.end method
