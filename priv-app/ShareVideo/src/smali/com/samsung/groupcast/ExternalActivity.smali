.class public Lcom/samsung/groupcast/ExternalActivity;
.super Landroid/app/Activity;
.source "ExternalActivity.java"

# interfaces
.implements Lcom/samsung/groupcast/GroupPlayManager$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;
    }
.end annotation


# static fields
.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field private f:Lcom/samsung/groupcast/ExternalActivity;

.field private g:Lcom/samsung/groupcast/GroupPlayManager;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "GroupPlayBandpkt"

    sput-object v0, Lcom/samsung/groupcast/ExternalActivity;->d:Ljava/lang/String;

    .line 19
    const-string v0, "GroupPlayBandLog"

    sput-object v0, Lcom/samsung/groupcast/ExternalActivity;->e:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    iput-object p0, p0, Lcom/samsung/groupcast/ExternalActivity;->f:Lcom/samsung/groupcast/ExternalActivity;

    .line 28
    invoke-virtual {p0}, Lcom/samsung/groupcast/ExternalActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->h:Ljava/lang/String;

    .line 29
    const-string v0, "GroupPlay"

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->a:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->b:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity;->h:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->c:Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity;->f:Lcom/samsung/groupcast/ExternalActivity;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    .line 34
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    .line 83
    :cond_0
    return-void
.end method

.method public onGpService(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/samsung/groupcast/ExternalActivity;->e:Ljava/lang/String;

    const-string v1, "[EXT_APP]receive : onGPservice : ExternalActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[onPause]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 63
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-virtual {v0}, Lcom/samsung/groupcast/GroupPlayManager;->a()V

    .line 65
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 50
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 51
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/groupcast/ExternalActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/GroupPlayManager;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/groupcast/ExternalActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/groupcast/ExternalActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/groupcast/GroupPlayManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[onResume]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 42
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[onStart]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager$b;)V

    .line 45
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 69
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[onStop]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-virtual {v0}, Lcom/samsung/groupcast/GroupPlayManager;->b()V

    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 75
    return-void
.end method

.method public setStatusInfo(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_0

    new-instance v0, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;-><init>(Lcom/samsung/groupcast/ExternalActivity;Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/samsung/groupcast/ExternalActivity;->b:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/samsung/groupcast/ExternalActivity;->g:Lcom/samsung/groupcast/GroupPlayManager;

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/groupcast/ExternalActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/groupcast/ExternalActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/groupcast/GroupPlayManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method
