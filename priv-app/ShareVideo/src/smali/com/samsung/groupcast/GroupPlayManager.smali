.class public Lcom/samsung/groupcast/GroupPlayManager;
.super Ljava/lang/Object;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/GroupPlayManager$a;,
        Lcom/samsung/groupcast/GroupPlayManager$b;,
        Lcom/samsung/groupcast/GroupPlayManager$c;
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Lcom/samsung/groupcast/GroupPlayManager$b;

.field private e:Landroid/content/BroadcastReceiver;

.field private f:Landroid/content/IntentFilter;

.field private g:Lcom/samsung/groupcast/GroupPlayManager$a;

.field private h:Landroid/content/BroadcastReceiver;

.field private i:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "GroupPlayBandpkt"

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    .line 23
    const-string v0, "GroupPlayBandLog"

    sput-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager;->c:Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/GroupPlayManager$1;-><init>(Lcom/samsung/groupcast/GroupPlayManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->e:Landroid/content/BroadcastReceiver;

    .line 95
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->f:Landroid/content/IntentFilter;

    .line 96
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->f:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_NETWORK_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->f:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/samsung/groupcast/GroupPlayManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/GroupPlayManager$2;-><init>(Lcom/samsung/groupcast/GroupPlayManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->h:Landroid/content/BroadcastReceiver;

    .line 132
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->i:Landroid/content/IntentFilter;

    .line 133
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->i:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_REGISTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->i:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->i:Landroid/content/IntentFilter;

    const-string v1, "com.samsung.groupcast.action.GP_UNREGISTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method static synthetic a(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$b;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->d:Lcom/samsung/groupcast/GroupPlayManager$b;

    return-object v0
.end method

.method private a(Lcom/samsung/groupcast/GroupPlayManager$c;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/samsung/groupcast/GroupPlayManager$c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    const-string v1, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 143
    return-void
.end method

.method static synthetic b(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$a;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->g:Lcom/samsung/groupcast/GroupPlayManager$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[EXT_APP]send : UNREGISTER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager$c;->e:Lcom/samsung/groupcast/GroupPlayManager$c;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager$c;Landroid/os/Bundle;)V

    .line 148
    return-void
.end method

.method public final a(Lcom/samsung/groupcast/GroupPlayManager$b;)V
    .locals 3

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager;->d:Lcom/samsung/groupcast/GroupPlayManager$b;

    .line 176
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->e:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/groupcast/GroupPlayManager;->f:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 177
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 152
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v1, "com.samsung.groupcast.extra.CAPABILITY_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->a:Lcom/samsung/groupcast/GroupPlayManager$c;

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager$c;Landroid/os/Bundle;)V

    .line 156
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[EXT_APP]send : REGISTER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "     PACKAGE_NAME :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "     CAPABILITY_INFO :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 163
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v1, "com.samsung.groupcast.extra.STATUS_INFO"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v1, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    sget-object v1, Lcom/samsung/groupcast/GroupPlayManager$c;->b:Lcom/samsung/groupcast/GroupPlayManager$c;

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager$c;Landroid/os/Bundle;)V

    .line 168
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[EXT_APP]send : UPDATE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "     PACKAGE_NAME :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "     STATUS_INFO :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "     SCREEN_ID :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->d:Lcom/samsung/groupcast/GroupPlayManager$b;

    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
