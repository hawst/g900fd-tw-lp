.class final Lcom/samsung/groupcast/GroupPlayManager$2;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/groupcast/GroupPlayManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/GroupPlayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->a:Lcom/samsung/groupcast/GroupPlayManager;

    .line 100
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v0}, Lcom/samsung/groupcast/GroupPlayManager;->b(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$a;

    move-result-object v0

    if-nez v0, :cond_1

    .line 105
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[GP]Error : ExtMessageManager.mExtListener is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.action.GP_REGISTER"

    if-ne v0, v1, :cond_2

    .line 111
    const-string v0, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 112
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 113
    const-string v1, "com.samsung.groupcast.extra.CAPABILITY_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v0}, Lcom/samsung/groupcast/GroupPlayManager;->b(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$a;

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.action.GP_UPDATE"

    if-ne v0, v1, :cond_3

    .line 117
    const-string v0, "com.samsung.groupcast.extra.MSG_BUNDEL"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 118
    const-string v1, "com.samsung.groupcast.extra.PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 119
    const-string v1, "com.samsung.groupcast.extra.STATUS_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 120
    const-string v1, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 122
    const-string v0, "GroupPlayBandLog"

    const-string v1, "[GP]receive : GP_UPDATE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v0}, Lcom/samsung/groupcast/GroupPlayManager;->b(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$a;

    goto :goto_0

    .line 125
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.action.GP_UNREGISTER"

    if-ne v0, v1, :cond_0

    .line 126
    const-string v0, "GroupPlayBandLog"

    const-string v1, "[GP]receive : GP_UNREGISTER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$2;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v0}, Lcom/samsung/groupcast/GroupPlayManager;->b(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$a;

    goto :goto_0
.end method
