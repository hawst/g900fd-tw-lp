.class public Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;
.super Ljava/lang/Exception;
.source "ExternalActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/ExternalActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InvalidLengthException"
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/groupcast/ExternalActivity;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/ExternalActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;->a:Lcom/samsung/groupcast/ExternalActivity;

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 102
    iput-object p2, p0, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;->b:Ljava/lang/String;

    .line 103
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Length of Statusinfo("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/ExternalActivity$InvalidLengthException;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "byte. Too long! we support maximun 8byte."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
