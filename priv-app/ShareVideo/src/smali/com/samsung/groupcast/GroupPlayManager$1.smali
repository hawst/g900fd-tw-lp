.class final Lcom/samsung/groupcast/GroupPlayManager$1;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/groupcast/GroupPlayManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/GroupPlayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->a:Lcom/samsung/groupcast/GroupPlayManager;

    .line 76
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v0}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 82
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[EXT_APP]Error : GPMessageManager.mListener is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.groupcast.action.GP_SERVICE"

    if-ne v0, v1, :cond_0

    .line 87
    sget-object v0, Lcom/samsung/groupcast/GroupPlayManager;->b:Ljava/lang/String;

    const-string v1, "[EXT_APP]receive : GP_SERVICE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    const-string v1, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->a:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-static {v1}, Lcom/samsung/groupcast/GroupPlayManager;->a(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/groupcast/GroupPlayManager$b;->onGpService(Ljava/lang/String;)V

    goto :goto_0
.end method
