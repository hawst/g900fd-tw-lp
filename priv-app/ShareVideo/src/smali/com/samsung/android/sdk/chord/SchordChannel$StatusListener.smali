.class public interface abstract Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;
.super Ljava/lang/Object;
.source "SchordChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chord/SchordChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StatusListener"
.end annotation


# static fields
.field public static final ERROR_FILE_CANCELED:I = 0x7d2

.field public static final ERROR_FILE_CREATE_FAILED:I = 0x7d3

.field public static final ERROR_FILE_NO_RESOURCE:I = 0x7d4

.field public static final ERROR_FILE_REJECTED:I = 0x7d1

.field public static final ERROR_FILE_SEND_FAILED:I = 0x7d0

.field public static final ERROR_FILE_TIMEOUT:I = 0x7d5

.field public static final ERROR_NONE:I = 0x7d6


# virtual methods
.method public abstract onDataReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
.end method

.method public abstract onFileChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
.end method

.method public abstract onFileChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
.end method

.method public abstract onFileFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onFileReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
.end method

.method public abstract onFileSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFileWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract onMultiFilesChunkReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJ)V
.end method

.method public abstract onMultiFilesChunkSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JJJ)V
.end method

.method public abstract onMultiFilesFailed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract onMultiFilesFinished(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onMultiFilesReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;)V
.end method

.method public abstract onMultiFilesSent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onMultiFilesWillReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
.end method

.method public abstract onNodeJoined(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onNodeLeft(Ljava/lang/String;Ljava/lang/String;)V
.end method
