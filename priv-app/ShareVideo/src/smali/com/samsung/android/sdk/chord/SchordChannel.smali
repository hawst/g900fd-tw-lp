.class public interface abstract Lcom/samsung/android/sdk/chord/SchordChannel;
.super Ljava/lang/Object;
.source "SchordChannel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;
    }
.end annotation


# virtual methods
.method public abstract acceptFile(Ljava/lang/String;IIJ)V
.end method

.method public abstract acceptMultiFiles(Ljava/lang/String;IIJ)V
.end method

.method public abstract cancelFile(Ljava/lang/String;)V
.end method

.method public abstract cancelMultiFiles(Ljava/lang/String;)V
.end method

.method public abstract getJoinedNodeList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNodeIpAddress(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isSecureChannel()Z
.end method

.method public abstract rejectFile(Ljava/lang/String;)V
.end method

.method public abstract rejectMultiFiles(Ljava/lang/String;)V
.end method

.method public abstract sendData(Ljava/lang/String;Ljava/lang/String;[[B)V
.end method

.method public abstract sendDataToAll(Ljava/lang/String;[[B)V
.end method

.method public abstract sendFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract sendMultiFiles(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method
