.class Lcom/samsung/android/sdk/chord/SchordManager$2;
.super Ljava/lang/Object;
.source "SchordManager.java"

# interfaces
.implements Lcom/samsung/android/sdk/chord/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chord/SchordManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/chord/SchordManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/chord/SchordManager;)V
    .locals 0

    .prologue
    .line 1275
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1309
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : onUpdateWifiLinkProperties : interface["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v2}, Lcom/samsung/android/sdk/chord/SchordManager;->c(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/b;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v3}, Lcom/samsung/android/sdk/chord/SchordManager;->b(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chord/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] status["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v2}, Lcom/samsung/android/sdk/chord/SchordManager;->d(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->b(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 1329
    :cond_0
    :goto_0
    return-void

    .line 1317
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->d(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1321
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->c(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 1322
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : onUpdateWifiLinkProperties : old["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v3}, Lcom/samsung/android/sdk/chord/SchordManager;->f(Lcom/samsung/android/sdk/chord/SchordManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] => new["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v1}, Lcom/samsung/android/sdk/chord/SchordManager;->f(Lcom/samsung/android/sdk/chord/SchordManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1325
    invoke-static {}, Lcom/samsung/android/sdk/chord/SchordManager;->a()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd49

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1326
    invoke-static {}, Lcom/samsung/android/sdk/chord/SchordManager;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/chord/SchordManager;->a()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1280
    invoke-static {}, Lcom/samsung/android/sdk/chord/SchordManager;->a()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd48

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1281
    invoke-static {}, Lcom/samsung/android/sdk/chord/SchordManager;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1284
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->b(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1300
    :goto_0
    return-void

    .line 1290
    :cond_1
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : onChangedConnectivity : interface["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v2}, Lcom/samsung/android/sdk/chord/SchordManager;->c(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/chord/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] status["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v1}, Lcom/samsung/android/sdk/chord/SchordManager;->d(Lcom/samsung/android/sdk/chord/SchordManager;)I

    move-result v1

    if-ne v0, v1, :cond_2

    if-nez p2, :cond_2

    .line 1296
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Lcom/samsung/android/sdk/chord/SchordManager;Z)Z

    .line 1299
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->e(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/ChordAgent;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1303
    const-string v0, "chord_api"

    const-string v1, "SchordManager : onScreenOn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager$2;->a:Lcom/samsung/android/sdk/chord/SchordManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/SchordManager;->resetSmartDiscoveryPeriod()V

    .line 1305
    return-void
.end method
