.class public Lcom/samsung/android/sdk/chord/SchordManager;
.super Ljava/lang/Object;
.source "SchordManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;,
        Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;
    }
.end annotation


# static fields
.field public static final INTERFACE_TYPE_WIFI:I = 0x0

.field public static final INTERFACE_TYPE_WIFI_AP:I = 0x2

.field public static final INTERFACE_TYPE_WIFI_P2P:I = 0x1

.field public static final SECURE_PREFIX:Ljava/lang/String; = "#"

.field private static c:Landroid/os/Handler;

.field private static w:I


# instance fields
.field private a:Lcom/samsung/android/sdk/chord/ChordAgent;

.field private b:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

.field private f:I

.field private g:Lcom/samsung/android/sdk/chord/b;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

.field private n:[Ljava/lang/Integer;

.field private o:J

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Lcom/samsung/android/sdk/chord/b/a;

.field private x:Lcom/samsung/android/sdk/chord/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    .line 265
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 228
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 232
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 234
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 236
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 238
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 240
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 242
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 244
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    .line 250
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 251
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    .line 253
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 254
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 255
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 257
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 258
    iput-boolean v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 260
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 261
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 263
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 1275
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chord/SchordManager$2;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    .line 282
    const-string v0, "chord_api"

    const-string v1, "SchordManager : SchordManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/content/Context;)V

    .line 284
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 228
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 232
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 234
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 236
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 238
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 240
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 242
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 244
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    .line 250
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 251
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    .line 253
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 254
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 255
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 257
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 258
    iput-boolean v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 260
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 261
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 263
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 1275
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chord/SchordManager$2;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    .line 298
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/content/Context;)V

    .line 299
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 300
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chord/SchordManager;->setNetworkListener(Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V

    .line 301
    return-void
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 933
    if-eqz p2, :cond_0

    const-string v0, "joinSecureChannel : "

    move-object v2, v0

    .line 934
    :goto_0
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "<"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ">"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v5, v0, :cond_1

    .line 939
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 933
    :cond_0
    const-string v0, "joinChannel : "

    move-object v2, v0

    goto :goto_0

    .line 944
    :cond_1
    if-nez p3, :cond_2

    .line 947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 952
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 954
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid channelName!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 958
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    if-nez v0, :cond_5

    if-eqz p2, :cond_5

    .line 960
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid using security! You must call setUseSecureMode before this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 964
    :cond_5
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    .line 966
    if-eqz v1, :cond_9

    move-object v0, v1

    .line 968
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 970
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/a;->a()I

    move-result v3

    if-nez v3, :cond_7

    .line 972
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Already joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/chord/a;->a(Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    .line 1020
    :cond_6
    :goto_1
    return-object v1

    .line 978
    :cond_7
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/a;->a()I

    move-result v3

    if-ne v4, v3, :cond_8

    .line 980
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "re-joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/chord/a;->a(Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    goto :goto_1

    .line 988
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 992
    :cond_9
    new-instance v1, Lcom/samsung/android/sdk/chord/a;

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/samsung/android/sdk/chord/a;-><init>(Lcom/samsung/android/sdk/chord/ChordAgent;Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    .line 994
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v5, v0, :cond_a

    move-object v0, v1

    .line 995
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    .line 998
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    const-string v0, "Chord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 1002
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/a;->a(Ljava/util/List;)V

    goto :goto_1

    .line 1008
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->joinChannel(Ljava/lang/String;)I

    move-result v0

    .line 1010
    if-eqz v0, :cond_6

    .line 1014
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1015
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to join "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1035
    if-nez p1, :cond_0

    .line 1036
    const-string v0, "INTERFACE_TYPE_WIFI"

    .line 1044
    :goto_0
    return-object v0

    .line 1038
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 1039
    const-string v0, "INTERFACE_TYPE_WIFI_P2P"

    goto :goto_0

    .line 1041
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 1042
    const-string v0, "INTERFACE_TYPE_WIFI_AP"

    goto :goto_0

    .line 1044
    :cond_2
    const-string v0, "INTERFACE_TYPE_UNKNOWN"

    goto :goto_0
.end method

.method private a(IZLcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 814
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v6, v0, :cond_0

    .line 817
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 824
    new-instance v0, Lcom/samsung/android/sdk/chord/InvalidInterfaceException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid interface - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/InvalidInterfaceException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 829
    :cond_1
    if-nez p3, :cond_2

    .line 832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid listener. It must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 838
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 840
    :cond_3
    const-string v0, "chord_api"

    const-string v1, "SchordManager : start : Please call setTempDirectory before start!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    if-nez v0, :cond_5

    if-eqz p2, :cond_5

    .line 846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call setUseSecureMode before this."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 850
    :cond_5
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : start : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->getChordAgentState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 852
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordInit(Ljava/lang/String;)I

    move-result v0

    .line 854
    if-eqz v0, :cond_6

    .line 857
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to initialize core - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 862
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSecureMode(Z)Z

    .line 864
    iput-object p3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 866
    iput p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 868
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 870
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-nez v0, :cond_7

    .line 871
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 874
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    sget-object v1, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 876
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUdpDiscover(Z)V

    .line 878
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chord/ChordAgent;->setNodeExpiry(Z)V

    .line 879
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 880
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    .line 881
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 885
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 887
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : interface("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : smart discover("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/b;->c(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 897
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":*"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUsingInterface(Ljava/lang/String;)V

    .line 898
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 899
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStart()Z

    move-result v0

    if-nez v0, :cond_8

    .line 903
    iput-object v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 905
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 907
    iput-object v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 909
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 911
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 913
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 914
    iput v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 915
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Fail to start core"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 921
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 922
    const-string v0, "chord_api"

    const-string v1, "Call Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    new-instance v0, Lcom/samsung/android/sdk/chord/b/a;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 924
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->start()V

    .line 929
    :cond_9
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xbb9

    .line 779
    sget v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    if-eqz v0, :cond_0

    .line 780
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not allow multiple instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 783
    :cond_0
    if-nez p1, :cond_1

    .line 784
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid context!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 787
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/chord/Schord;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 788
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "you should initialize Schord before getting instance!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 791
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 792
    invoke-static {}, Lcom/samsung/android/sdk/chord/Schord;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 793
    new-instance v0, Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 794
    new-instance v0, Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 795
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    .line 796
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 798
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 799
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 800
    const-string v0, "chord_api"

    const-string v1, "SchordManager : remove stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 805
    :cond_3
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    .line 807
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Hi Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1069
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1268
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1071
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-eqz v0, :cond_0

    .line 1074
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v0, v1, :cond_1

    .line 1075
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : Net Type :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CUR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1079
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1081
    iget v0, p1, Landroid/os/Message;->arg2:I

    if-nez v0, :cond_2

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;->onConnected(I)V

    goto :goto_0

    .line 1084
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;->onDisconnected(I)V

    goto :goto_0

    .line 1090
    :sswitch_2
    const/4 v0, 0x3

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_3

    .line 1091
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1092
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 1093
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    goto :goto_0

    .line 1095
    :cond_3
    const/4 v0, 0x5

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_4

    .line 1096
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    goto :goto_0

    .line 1102
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 1103
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryIP()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1106
    const/4 v0, 0x6

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_f

    .line 1108
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chord/b;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1109
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 1111
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 1112
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 1116
    :goto_2
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1117
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v1, :cond_0

    .line 1118
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : ****onStarted("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStarted(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1126
    :sswitch_3
    const-string v0, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SchordManager : handleMessages : state is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v1, v0, :cond_7

    .line 1128
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1130
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_6

    .line 1131
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.ERROR_START_FAIL)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1135
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    goto/16 :goto_0

    .line 1138
    :cond_7
    const/4 v0, 0x3

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_8

    .line 1139
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 1140
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 1141
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1143
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_0

    .line 1144
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.STOPPED_BY_USER)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3ea

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1148
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v5, :cond_0

    .line 1149
    iput-object v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    goto/16 :goto_0

    .line 1153
    :cond_8
    const/4 v0, 0x5

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_9

    .line 1158
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1159
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    goto/16 :goto_0

    .line 1163
    :cond_9
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1164
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_b

    .line 1165
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 1166
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_3

    .line 1168
    :cond_a
    iput-object v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1169
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.NETWORK_DISCONNECTED)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3eb

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1173
    :cond_b
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1175
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    goto/16 :goto_0

    .line 1188
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 1189
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1190
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 1192
    if-eqz v0, :cond_0

    .line 1193
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_c

    .line 1194
    const-string v1, "chord_api"

    const-string v2, "SchordManager : ****onStopped(StatusListener.ERROR_START_FAIL)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    const/16 v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1196
    :cond_c
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    .line 1197
    const-string v1, "chord_api"

    const-string v2, "SchordManager : ****onStopped(StatusListener.ERROR_UNEXPECTED_STOP)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1198
    const/16 v1, 0x3e9

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1222
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/d;

    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/a/d;->a()Ljava/lang/String;

    move-result-object v1

    .line 1225
    const-string v0, "Chord"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1226
    const-string v0, ""

    .line 1227
    const/16 v0, 0xc1d

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_e

    .line 1228
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/a;

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/a/a;->b:Ljava/lang/String;

    .line 1229
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1230
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237
    :cond_d
    :goto_4
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 1238
    if-eqz v0, :cond_0

    .line 1239
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1231
    :cond_e
    const/16 v0, 0xc1e

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_d

    .line 1232
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/a;

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/a/a;->b:Ljava/lang/String;

    .line 1233
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1246
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1249
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 1250
    if-eqz v0, :cond_0

    .line 1251
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1257
    :sswitch_7
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    if-eqz v0, :cond_0

    .line 1258
    const-string v0, "chord_api"

    const-string v2, "SchordManager : NETWORK_DHCP_IP_RENEW"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1259
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1260
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto/16 :goto_2

    .line 1069
    nop

    :sswitch_data_0
    .sparse-switch
        0xbb8 -> :sswitch_2
        0xbb9 -> :sswitch_3
        0xbba -> :sswitch_4
        0xbbb -> :sswitch_0
        0xbbc -> :sswitch_4
        0xc1c -> :sswitch_6
        0xc1d -> :sswitch_5
        0xc1e -> :sswitch_5
        0xc80 -> :sswitch_5
        0xce4 -> :sswitch_5
        0xce5 -> :sswitch_5
        0xce6 -> :sswitch_5
        0xce8 -> :sswitch_5
        0xce9 -> :sswitch_5
        0xcea -> :sswitch_5
        0xcf8 -> :sswitch_5
        0xcf9 -> :sswitch_5
        0xcfa -> :sswitch_5
        0xcfc -> :sswitch_5
        0xcfd -> :sswitch_5
        0xcfe -> :sswitch_5
        0xcff -> :sswitch_5
        0xd48 -> :sswitch_1
        0xd49 -> :sswitch_7
    .end sparse-switch
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/samsung/android/sdk/chord/SchordManager;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    return v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1048
    if-nez p1, :cond_0

    .line 1049
    const-string v0, "STATE_INIT"

    .line 1062
    :goto_0
    return-object v0

    .line 1050
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 1051
    const-string v0, "STATE_REQ_START"

    goto :goto_0

    .line 1052
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 1053
    const-string v0, "STATE_STARTED"

    goto :goto_0

    .line 1054
    :cond_2
    const/4 v0, 0x3

    if-ne v0, p1, :cond_3

    .line 1055
    const-string v0, "STATE_REQ_STOP"

    goto :goto_0

    .line 1056
    :cond_3
    const/4 v0, 0x4

    if-ne v0, p1, :cond_4

    .line 1057
    const-string v0, "STATE_STOPPED"

    goto :goto_0

    .line 1058
    :cond_4
    const/4 v0, 0x5

    if-ne v0, p1, :cond_5

    .line 1059
    const-string v0, "STATE_REQ_RELEASE"

    goto :goto_0

    .line 1060
    :cond_5
    const/4 v0, 0x6

    if-ne v0, p1, :cond_6

    .line 1061
    const-string v0, "STATE_DISCONNECTED"

    goto :goto_0

    .line 1062
    :cond_6
    const-string v0, "STATE_UNKNOWN"

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/b;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    return-object v0
.end method

.method static synthetic d(Lcom/samsung/android/sdk/chord/SchordManager;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    return v0
.end method

.method static synthetic e(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/ChordAgent;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    return-object v0
.end method

.method static synthetic f(Lcom/samsung/android/sdk/chord/SchordManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 309
    const-string v0, "chord_api"

    const-string v1, "SchordManager : close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    sput v3, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    .line 312
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 313
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 314
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 320
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 322
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 323
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 326
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v4, v0, :cond_2

    .line 336
    :goto_0
    return-void

    .line 330
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 331
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 333
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Bye Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableInterfaceTypes()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 350
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-nez v1, :cond_1

    .line 351
    const-string v1, "chord_api"

    const-string v2, "SchordManager : getAvailableInterfaceTypes : Invalid condition. Call this after initialize"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    :goto_0
    return-object v0

    .line 357
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 358
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 361
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 625
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 637
    :goto_0
    monitor-exit p0

    return-object v0

    .line 629
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 631
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 637
    goto :goto_0

    .line 625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getJoinedChannelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 648
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 649
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    return-object v0
.end method

.method public isSmartDiscoveryEnabled()Z
    .locals 1

    .prologue
    .line 759
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    return v0
.end method

.method public joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 1

    .prologue
    .line 553
    const-string v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 554
    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    return-object v0

    .line 553
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveChannel(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 569
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v2, v0, :cond_1

    const/4 v0, 0x4

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_1

    .line 571
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : leaveChannel : ERROR_INVALID_STATE - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 579
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid channelName!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    .line 585
    if-nez v1, :cond_4

    .line 587
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : leaveChannel : ERROR_INVALID_PARAM - Have not joined @"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 594
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    .line 596
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 598
    const-string v0, "Chord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->leaveChannelEx(Ljava/lang/String;)I

    move-result v0

    .line 606
    if-eqz v0, :cond_0

    .line 610
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to leave "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public resetSmartDiscoveryPeriod()V
    .locals 2

    .prologue
    .line 769
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->resetSmartDiscoveryPeriod()V

    .line 772
    :cond_1
    return-void
.end method

.method public setLooper(Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setLooper : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :goto_0
    return-void

    .line 409
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager$1;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    goto :goto_0
.end method

.method public setNetworkListener(Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 435
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 436
    iput-object v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 437
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Handler. It must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 440
    :goto_0
    const/4 v0, 0x3

    if-ge v2, v0, :cond_2

    .line 441
    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    .line 440
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 441
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 444
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 446
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-eqz v0, :cond_4

    .line 447
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 455
    :cond_3
    :goto_2
    return-void

    .line 451
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-nez v0, :cond_3

    .line 452
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    goto :goto_2
.end method

.method public setNodeKeepAliveTimeout(I)V
    .locals 4

    .prologue
    .line 690
    if-lez p1, :cond_2

    .line 691
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 696
    :goto_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 700
    :cond_1
    return-void

    .line 693
    :cond_2
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    goto :goto_0
.end method

.method public setSecureModeEnabled(Z)V
    .locals 0

    .prologue
    .line 727
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 728
    return-void
.end method

.method public setSendMultiFilesLimitCount(I)V
    .locals 2

    .prologue
    .line 710
    iput p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 712
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 714
    const-string v0, "chord_api"

    const-string v1, "SchordManager : fail to setSendFileLimit"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    :cond_1
    return-void
.end method

.method public setSmartDiscoveryEnabled(Z)V
    .locals 2

    .prologue
    .line 745
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 746
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 749
    :cond_1
    return-void
.end method

.method public setTempDirectory(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 380
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 383
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 385
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 386
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setTempDirectory : fail to create dir"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setTempDirectory : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public start(ILcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
        }
    .end annotation

    .prologue
    .line 471
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->a(IZLcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V

    .line 472
    return-void
.end method

.method public stop()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 480
    const-string v0, "chord_api"

    const-string v1, "SchordManager : stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    const-string v0, "chord_api"

    const-string v1, "Stop  Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    if-eqz v0, :cond_0

    .line 489
    const-string v0, "chord_api"

    const-string v1, "UDPUnicastReceiver is closed "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->a()V

    .line 491
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->interrupt()V

    .line 492
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-nez v0, :cond_1

    .line 497
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 501
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 504
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 506
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 509
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_0

    .line 513
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 515
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 517
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v3, v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    if-ne v0, v3, :cond_5

    .line 518
    :cond_3
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 519
    const-string v0, "chord_api"

    const-string v1, "SchordManager : stop : STATE_REQ_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_4
    :goto_1
    return-void

    .line 524
    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v4, v0, :cond_6

    .line 525
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 526
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStop()Z

    goto :goto_1

    .line 528
    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v6, v0, :cond_7

    const/4 v0, 0x6

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_4

    .line 529
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 530
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 531
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 532
    iput v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    goto :goto_1
.end method
