.class public Lcom/samsung/android/sdk/chord/Schord;
.super Ljava/lang/Object;
.source "Schord.java"

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Z
    .locals 1

    .prologue
    .line 76
    sget-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    return v0
.end method

.method static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "1.5.0_20130828_296808"

    return-object v0
.end method


# virtual methods
.method public getVersionCode()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "1.5.0_20130828_296808"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/SsdkVendorCheck;->isSamsungDevice()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v1, "Vendor is not SAMSUNG!"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 60
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    goto :goto_0
.end method

.method public isFeatureEnabled(I)Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method
