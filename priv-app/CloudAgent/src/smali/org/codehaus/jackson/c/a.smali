.class public final Lorg/codehaus/jackson/c/a;
.super Ljava/lang/Object;


# instance fields
.field final a:Lorg/codehaus/jackson/c/a;

.field final b:Z

.field private c:I

.field private d:I

.field private e:[I

.field private f:[Lorg/codehaus/jackson/c/e;

.field private g:[Lorg/codehaus/jackson/c/b;

.field private h:I

.field private i:I

.field private transient j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method private constructor <init>(IZ)V
    .locals 2

    const/16 v0, 0x10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/codehaus/jackson/c/a;->a:Lorg/codehaus/jackson/c/a;

    iput-boolean p2, p0, Lorg/codehaus/jackson/c/a;->b:Z

    if-ge p1, v0, :cond_1

    move p1, v0

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/jackson/c/a;->c(I)V

    return-void

    :cond_1
    add-int/lit8 v1, p1, -0x1

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    :goto_1
    if-ge v0, p1, :cond_2

    add-int/2addr v0, v0

    goto :goto_1

    :cond_2
    move p1, v0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/codehaus/jackson/c/a;Z)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/codehaus/jackson/c/a;->a:Lorg/codehaus/jackson/c/a;

    iput-boolean p2, p0, Lorg/codehaus/jackson/c/a;->b:Z

    iget v0, p1, Lorg/codehaus/jackson/c/a;->c:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    iget v0, p1, Lorg/codehaus/jackson/c/a;->d:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->d:I

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->e:[I

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iget v0, p1, Lorg/codehaus/jackson/c/a;->h:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    iget v0, p1, Lorg/codehaus/jackson/c/a;->i:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->j:Z

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->k:Z

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->l:Z

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->m:Z

    return-void
.end method

.method public static a()Lorg/codehaus/jackson/c/a;
    .locals 3

    new-instance v0, Lorg/codehaus/jackson/c/a;

    const/16 v1, 0x40

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lorg/codehaus/jackson/c/a;-><init>(IZ)V

    return-object v0
.end method

.method private static a(ILjava/lang/String;[II)Lorg/codehaus/jackson/c/e;
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    if-ge p3, v0, :cond_0

    packed-switch p3, :pswitch_data_0

    :cond_0
    new-array v2, p3, [I

    move v0, v1

    :goto_0
    if-ge v0, p3, :cond_1

    aget v1, p2, v0

    aput v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    new-instance v0, Lorg/codehaus/jackson/c/f;

    aget v1, p2, v1

    invoke-direct {v0, p1, p0, v1}, Lorg/codehaus/jackson/c/f;-><init>(Ljava/lang/String;II)V

    :goto_1
    return-object v0

    :pswitch_1
    new-instance v0, Lorg/codehaus/jackson/c/g;

    aget v1, p2, v1

    aget v2, p2, v2

    invoke-direct {v0, p1, p0, v1, v2}, Lorg/codehaus/jackson/c/g;-><init>(Ljava/lang/String;III)V

    goto :goto_1

    :pswitch_2
    new-instance v0, Lorg/codehaus/jackson/c/h;

    aget v3, p2, v1

    aget v4, p2, v2

    const/4 v1, 0x2

    aget v5, p2, v1

    move-object v1, p1

    move v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/codehaus/jackson/c/h;-><init>(Ljava/lang/String;IIII)V

    goto :goto_1

    :cond_1
    new-instance v0, Lorg/codehaus/jackson/c/i;

    invoke-direct {v0, p1, p0, v2, p3}, Lorg/codehaus/jackson/c/i;-><init>(Ljava/lang/String;I[II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILorg/codehaus/jackson/c/e;)V
    .locals 6

    const/4 v5, 0x1

    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->j()V

    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->g()V

    :cond_1
    iget v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    iget v0, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int v1, p1, v0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aget-object v0, v0, v1

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    shl-int/lit8 v2, p1, 0x8

    aput v2, v0, v1

    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->l:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->l()V

    :cond_2
    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aput-object p2, v0, v1

    :goto_0
    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    array-length v0, v0

    iget v1, p0, Lorg/codehaus/jackson/c/a;->c:I

    shr-int/lit8 v2, v0, 0x1

    if-le v1, v2, :cond_3

    shr-int/lit8 v1, v0, 0x2

    iget v2, p0, Lorg/codehaus/jackson/c/a;->c:I

    sub-int/2addr v0, v1

    if-le v2, v0, :cond_9

    iput-boolean v5, p0, Lorg/codehaus/jackson/c/a;->j:Z

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->m:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->k()V

    :cond_5
    iget v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    aget v2, v0, v1

    and-int/lit16 v0, v2, 0xff

    if-nez v0, :cond_8

    iget v0, p0, Lorg/codehaus/jackson/c/a;->i:I

    const/16 v3, 0xfe

    if-gt v0, v3, :cond_7

    iget v0, p0, Lorg/codehaus/jackson/c/a;->i:I

    iget v3, p0, Lorg/codehaus/jackson/c/a;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/codehaus/jackson/c/a;->i:I

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    array-length v3, v3

    if-lt v0, v3, :cond_6

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->m()V

    :cond_6
    :goto_2
    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->e:[I

    and-int/lit16 v2, v2, -0x100

    add-int/lit8 v4, v0, 0x1

    or-int/2addr v2, v4

    aput v2, v3, v1

    :goto_3
    iget-object v1, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    new-instance v2, Lorg/codehaus/jackson/c/b;

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    aget-object v3, v3, v0

    invoke-direct {v2, p2, v3}, Lorg/codehaus/jackson/c/b;-><init>(Lorg/codehaus/jackson/c/e;Lorg/codehaus/jackson/c/b;)V

    aput-object v2, v1, v0

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->i()I

    move-result v0

    goto :goto_2

    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_9
    iget v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    if-lt v0, v1, :cond_3

    iput-boolean v5, p0, Lorg/codehaus/jackson/c/a;->j:Z

    goto :goto_1
.end method

.method private declared-synchronized a(Lorg/codehaus/jackson/c/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p1, Lorg/codehaus/jackson/c/a;->c:I

    iget v1, p0, Lorg/codehaus/jackson/c/a;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v0, v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lorg/codehaus/jackson/c/a;->c()I

    move-result v0

    const/16 v1, 0x1770

    if-le v0, v1, :cond_1

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lorg/codehaus/jackson/c/a;->c(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget v0, p1, Lorg/codehaus/jackson/c/a;->c:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->e:[I

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->k:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->l:Z

    iget v0, p1, Lorg/codehaus/jackson/c/a;->d:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->d:I

    iget-object v0, p1, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iget v0, p1, Lorg/codehaus/jackson/c/a;->h:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    iget v0, p1, Lorg/codehaus/jackson/c/a;->i:I

    iput v0, p0, Lorg/codehaus/jackson/c/a;->i:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static final b(I)I
    .locals 2

    ushr-int/lit8 v0, p0, 0x10

    xor-int/2addr v0, p0

    ushr-int/lit8 v1, v0, 0x8

    xor-int/2addr v0, v1

    return v0
.end method

.method public static final b(II)I
    .locals 2

    mul-int/lit8 v0, p0, 0x1f

    add-int/2addr v0, p1

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x8

    xor-int/2addr v0, v1

    return v0
.end method

.method public static final b([II)I
    .locals 3

    const/4 v0, 0x0

    aget v1, p0, v0

    const/4 v0, 0x1

    :goto_0
    if-ge v0, p1, :cond_0

    mul-int/lit8 v1, v1, 0x1f

    aget v2, p0, v0

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    ushr-int/lit8 v0, v1, 0x10

    xor-int/2addr v0, v1

    ushr-int/lit8 v1, v0, 0x8

    xor-int/2addr v0, v1

    return v0
.end method

.method private c(I)V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lorg/codehaus/jackson/c/a;->c:I

    new-array v0, p1, [I

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    new-array v0, p1, [Lorg/codehaus/jackson/c/e;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->k:Z

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->l:Z

    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lorg/codehaus/jackson/c/a;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iput v1, p0, Lorg/codehaus/jackson/c/a;->i:I

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->j:Z

    return-void
.end method

.method public static e()Lorg/codehaus/jackson/c/e;
    .locals 1

    invoke-static {}, Lorg/codehaus/jackson/c/f;->b()Lorg/codehaus/jackson/c/f;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->k:Z

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->l:Z

    iput-boolean v0, p0, Lorg/codehaus/jackson/c/a;->m:Z

    return-void
.end method

.method private g()V
    .locals 12

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->j:Z

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->l:Z

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    array-length v3, v0

    add-int v0, v3, v3

    const/high16 v2, 0x10000

    if-le v0, v2, :cond_1

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->h()V

    :cond_0
    return-void

    :cond_1
    new-array v2, v0, [I

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->e:[I

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lorg/codehaus/jackson/c/a;->d:I

    iget-object v4, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    new-array v0, v0, [Lorg/codehaus/jackson/c/e;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v5, v4, v2

    if-eqz v5, :cond_2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v5}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v6

    iget v7, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int/2addr v7, v6

    iget-object v8, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aput-object v5, v8, v7

    iget-object v5, p0, Lorg/codehaus/jackson/c/a;->e:[I

    shl-int/lit8 v6, v6, 0x8

    aput v6, v5, v7

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iget v4, p0, Lorg/codehaus/jackson/c/a;->i:I

    if-eqz v4, :cond_0

    iput v1, p0, Lorg/codehaus/jackson/c/a;->h:I

    iput v1, p0, Lorg/codehaus/jackson/c/a;->i:I

    iput-boolean v1, p0, Lorg/codehaus/jackson/c/a;->m:Z

    iget-object v5, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    array-length v2, v5

    new-array v2, v2, [Lorg/codehaus/jackson/c/b;

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    move v3, v1

    move v1, v0

    :goto_1
    if-ge v3, v4, :cond_9

    aget-object v0, v5, v3

    move-object v11, v0

    move v0, v1

    move-object v1, v11

    :goto_2
    if-eqz v1, :cond_8

    add-int/lit8 v2, v0, 0x1

    iget-object v6, v1, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v6}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v0

    iget v7, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int/2addr v7, v0

    iget-object v8, p0, Lorg/codehaus/jackson/c/a;->e:[I

    aget v8, v8, v7

    iget-object v9, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aget-object v9, v9, v7

    if-nez v9, :cond_4

    iget-object v8, p0, Lorg/codehaus/jackson/c/a;->e:[I

    shl-int/lit8 v0, v0, 0x8

    aput v0, v8, v7

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aput-object v6, v0, v7

    :goto_3
    iget-object v0, v1, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    move-object v1, v0

    move v0, v2

    goto :goto_2

    :cond_4
    iget v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/codehaus/jackson/c/a;->h:I

    and-int/lit16 v0, v8, 0xff

    if-nez v0, :cond_7

    iget v0, p0, Lorg/codehaus/jackson/c/a;->i:I

    const/16 v9, 0xfe

    if-gt v0, v9, :cond_6

    iget v0, p0, Lorg/codehaus/jackson/c/a;->i:I

    iget v9, p0, Lorg/codehaus/jackson/c/a;->i:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/codehaus/jackson/c/a;->i:I

    iget-object v9, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    array-length v9, v9

    if-lt v0, v9, :cond_5

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->m()V

    :cond_5
    :goto_4
    iget-object v9, p0, Lorg/codehaus/jackson/c/a;->e:[I

    and-int/lit16 v8, v8, -0x100

    add-int/lit8 v10, v0, 0x1

    or-int/2addr v8, v10

    aput v8, v9, v7

    :goto_5
    iget-object v7, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    new-instance v8, Lorg/codehaus/jackson/c/b;

    iget-object v9, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    aget-object v9, v9, v0

    invoke-direct {v8, v6, v9}, Lorg/codehaus/jackson/c/b;-><init>(Lorg/codehaus/jackson/c/e;Lorg/codehaus/jackson/c/b;)V

    aput-object v8, v7, v0

    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->i()I

    move-result v0

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_9
    iget v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal error: count after rehash "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/codehaus/jackson/c/a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private h()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput v1, p0, Lorg/codehaus/jackson/c/a;->c:I

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    iput v1, p0, Lorg/codehaus/jackson/c/a;->h:I

    iput v1, p0, Lorg/codehaus/jackson/c/a;->i:I

    return-void
.end method

.method private i()I
    .locals 6

    iget-object v4, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    const v3, 0x7fffffff

    const/4 v0, -0x1

    const/4 v1, 0x0

    iget v5, p0, Lorg/codehaus/jackson/c/a;->i:I

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lorg/codehaus/jackson/c/b;->a()I

    move-result v2

    if-ge v2, v3, :cond_2

    const/4 v0, 0x1

    if-ne v2, v0, :cond_0

    :goto_1
    return v1

    :cond_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method private j()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->e:[I

    iget-object v1, p0, Lorg/codehaus/jackson/c/a;->e:[I

    array-length v1, v1

    new-array v2, v1, [I

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->e:[I

    iget-object v2, p0, Lorg/codehaus/jackson/c/a;->e:[I

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-boolean v3, p0, Lorg/codehaus/jackson/c/a;->k:Z

    return-void
.end method

.method private k()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    if-nez v0, :cond_0

    const/16 v0, 0x20

    new-array v0, v0, [Lorg/codehaus/jackson/c/b;

    iput-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    :goto_0
    iput-boolean v3, p0, Lorg/codehaus/jackson/c/a;->m:Z

    return-void

    :cond_0
    array-length v1, v0

    new-array v2, v1, [Lorg/codehaus/jackson/c/b;

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iget-object v2, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    array-length v1, v0

    new-array v2, v1, [Lorg/codehaus/jackson/c/e;

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    iget-object v2, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-boolean v3, p0, Lorg/codehaus/jackson/c/a;->l:Z

    return-void
.end method

.method private m()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    array-length v1, v0

    add-int v2, v1, v1

    new-array v2, v2, [Lorg/codehaus/jackson/c/b;

    iput-object v2, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    iget-object v2, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(ZZ)Lorg/codehaus/jackson/c/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/codehaus/jackson/c/a;

    invoke-direct {v0, p0, p2}, Lorg/codehaus/jackson/c/a;-><init>(Lorg/codehaus/jackson/c/a;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)Lorg/codehaus/jackson/c/e;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Lorg/codehaus/jackson/c/a;->b(I)I

    move-result v2

    iget v1, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int/2addr v1, v2

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->e:[I

    aget v3, v3, v1

    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aget-object v1, v4, v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, p1}, Lorg/codehaus/jackson/c/e;->a(I)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_0

    :cond_3
    and-int/lit16 v1, v3, 0xff

    if-lez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    aget-object v1, v3, v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v2, p1, v0}, Lorg/codehaus/jackson/c/b;->a(III)Lorg/codehaus/jackson/c/e;

    move-result-object v0

    goto :goto_0
.end method

.method public a(II)Lorg/codehaus/jackson/c/e;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1, p2}, Lorg/codehaus/jackson/c/a;->b(II)I

    move-result v2

    iget v1, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int/2addr v1, v2

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->e:[I

    aget v3, v3, v1

    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aget-object v1, v4, v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1, p1, p2}, Lorg/codehaus/jackson/c/e;->a(II)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_0

    :cond_3
    and-int/lit16 v1, v3, 0xff

    if-lez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    aget-object v1, v3, v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2, p1, p2}, Lorg/codehaus/jackson/c/b;->a(III)Lorg/codehaus/jackson/c/e;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[II)Lorg/codehaus/jackson/c/e;
    .locals 2

    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/codehaus/jackson/util/InternCache;->instance:Lorg/codehaus/jackson/util/InternCache;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/util/InternCache;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-static {p2, p3}, Lorg/codehaus/jackson/c/a;->b([II)I

    move-result v0

    invoke-static {v0, p1, p2, p3}, Lorg/codehaus/jackson/c/a;->a(ILjava/lang/String;[II)Lorg/codehaus/jackson/c/e;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/codehaus/jackson/c/a;->a(ILorg/codehaus/jackson/c/e;)V

    return-object v1
.end method

.method public a([II)Lorg/codehaus/jackson/c/e;
    .locals 5

    const/4 v1, 0x0

    invoke-static {p1, p2}, Lorg/codehaus/jackson/c/a;->b([II)I

    move-result v2

    iget v0, p0, Lorg/codehaus/jackson/c/a;->d:I

    and-int/2addr v0, v2

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->e:[I

    aget v3, v3, v0

    shr-int/lit8 v4, v3, 0x8

    xor-int/2addr v4, v2

    shl-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/codehaus/jackson/c/a;->f:[Lorg/codehaus/jackson/c/e;

    aget-object v0, v4, v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/c/e;->a([II)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    and-int/lit16 v0, v3, 0xff

    if-lez v0, :cond_3

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Lorg/codehaus/jackson/c/a;->g:[Lorg/codehaus/jackson/c/b;

    aget-object v0, v3, v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2, p1, p2}, Lorg/codehaus/jackson/c/b;->a(I[II)Lorg/codehaus/jackson/c/e;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/jackson/c/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->a:Lorg/codehaus/jackson/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/jackson/c/a;->a:Lorg/codehaus/jackson/c/a;

    invoke-direct {v0, p0}, Lorg/codehaus/jackson/c/a;->a(Lorg/codehaus/jackson/c/a;)V

    invoke-direct {p0}, Lorg/codehaus/jackson/c/a;->f()V

    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lorg/codehaus/jackson/c/a;->c:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lorg/codehaus/jackson/c/a;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
