.class final Lorg/codehaus/jackson/c/b;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lorg/codehaus/jackson/c/e;

.field protected final b:Lorg/codehaus/jackson/c/b;


# direct methods
.method constructor <init>(Lorg/codehaus/jackson/c/e;Lorg/codehaus/jackson/c/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    iput-object p2, p0, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    :goto_0
    if-eqz v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    iget-object v0, v0, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    goto :goto_0

    :cond_0
    return v1
.end method

.method public a(III)Lorg/codehaus/jackson/c/e;
    .locals 3

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0, p2, p3}, Lorg/codehaus/jackson/c/e;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v1, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, p2, p3}, Lorg/codehaus/jackson/c/e;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v0, v1, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    move-object v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I[II)Lorg/codehaus/jackson/c/e;
    .locals 3

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0, p2, p3}, Lorg/codehaus/jackson/c/e;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v1, Lorg/codehaus/jackson/c/b;->a:Lorg/codehaus/jackson/c/e;

    invoke-virtual {v0}, Lorg/codehaus/jackson/c/e;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, p2, p3}, Lorg/codehaus/jackson/c/e;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v0, v1, Lorg/codehaus/jackson/c/b;->b:Lorg/codehaus/jackson/c/b;

    move-object v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
