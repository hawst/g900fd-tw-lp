.class Lorg/apache/james/mime4j/field/address/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lorg/apache/james/mime4j/field/address/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/james/mime4j/field/address/a;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/a;-><init>()V

    sput-object v0, Lorg/apache/james/mime4j/field/address/a;->a:Lorg/apache/james/mime4j/field/address/a;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;
    .locals 4

    iget-object v0, p1, Lorg/apache/james/mime4j/field/address/parser/u;->a:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v1, p1, Lorg/apache/james/mime4j/field/address/parser/u;->b:Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    :goto_0
    if-eq v0, v1, :cond_1

    iget-object v3, v0, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    if-nez p2, :cond_0

    iget-object v3, v0, Lorg/apache/james/mime4j/field/address/parser/v;->h:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {p0, v2, v3}, Lorg/apache/james/mime4j/field/address/a;->a(Ljava/lang/StringBuilder;Lorg/apache/james/mime4j/field/address/parser/v;)V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/k;)Lorg/apache/james/mime4j/field/address/DomainList;
    .locals 5

    const/4 v4, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lorg/apache/james/mime4j/field/address/parser/k;->c()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v2, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    :goto_0
    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v3, v0, Lorg/apache/james/mime4j/field/address/parser/e;

    if-eqz v3, :cond_0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/e;

    invoke-direct {p0, v0, v4}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/james/mime4j/field/address/DomainList;

    invoke-direct {v0, v1, v4}, Lorg/apache/james/mime4j/field/address/DomainList;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method private a(Lorg/apache/james/mime4j/field/address/DomainList;Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v1, p2}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/g;

    invoke-direct {p0, v0, v3}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/e;

    invoke-direct {p0, v0, v3}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/james/mime4j/field/address/Mailbox;

    invoke-direct {v1, p1, v2, v0}, Lorg/apache/james/mime4j/field/address/Mailbox;-><init>(Lorg/apache/james/mime4j/field/address/DomainList;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/DomainList;Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/d;)Lorg/apache/james/mime4j/field/address/Mailbox;
    .locals 4

    new-instance v2, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v2, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    const/4 v1, 0x0

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v3, v0, Lorg/apache/james/mime4j/field/address/parser/k;

    if-eqz v3, :cond_1

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/k;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/k;)Lorg/apache/james/mime4j/field/address/DomainList;

    move-result-object v1

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    :cond_0
    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/a;

    if-eqz v2, :cond_2

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/a;

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/DomainList;Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    return-object v0

    :cond_1
    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/a;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/i;)Lorg/apache/james/mime4j/field/address/Mailbox;
    .locals 3

    new-instance v1, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v1, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/j;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/j;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/d;

    if-eqz v1, :cond_1

    invoke-static {v2}, Lorg/apache/james/mime4j/codec/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/james/mime4j/field/address/Mailbox;

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/d;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/d;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lorg/apache/james/mime4j/field/address/Mailbox;-><init>(Ljava/lang/String;Lorg/apache/james/mime4j/field/address/Mailbox;)V

    return-object v2

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private a(Lorg/apache/james/mime4j/field/address/parser/f;)Lorg/apache/james/mime4j/field/address/MailboxList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v2, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    :goto_0
    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v3, v0, Lorg/apache/james/mime4j/field/address/parser/h;

    if-eqz v3, :cond_0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/h;

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/h;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/james/mime4j/field/address/MailboxList;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lorg/apache/james/mime4j/field/address/MailboxList;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public static a()Lorg/apache/james/mime4j/field/address/a;
    .locals 1

    sget-object v0, Lorg/apache/james/mime4j/field/address/a;->a:Lorg/apache/james/mime4j/field/address/a;

    return-object v0
.end method

.method private a(Ljava/lang/StringBuilder;Lorg/apache/james/mime4j/field/address/parser/v;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p2, Lorg/apache/james/mime4j/field/address/parser/v;->h:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {p0, p1, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Ljava/lang/StringBuilder;Lorg/apache/james/mime4j/field/address/parser/v;)V

    iget-object v0, p2, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lorg/apache/james/mime4j/field/address/parser/b;)Lorg/apache/james/mime4j/field/address/Address;
    .locals 3

    new-instance v1, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v1, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/a;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/a;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/d;

    if-eqz v2, :cond_1

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/d;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/d;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lorg/apache/james/mime4j/field/address/parser/j;

    if-eqz v2, :cond_4

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/j;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/u;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/f;

    if-eqz v1, :cond_2

    new-instance v1, Lorg/apache/james/mime4j/field/address/Group;

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/f;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/f;)Lorg/apache/james/mime4j/field/address/MailboxList;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/james/mime4j/field/address/Group;-><init>(Ljava/lang/String;Lorg/apache/james/mime4j/field/address/MailboxList;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/d;

    if-eqz v1, :cond_3

    invoke-static {v2}, Lorg/apache/james/mime4j/codec/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lorg/apache/james/mime4j/field/address/Mailbox;

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/d;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/d;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/james/mime4j/field/address/Mailbox;-><init>(Ljava/lang/String;Lorg/apache/james/mime4j/field/address/Mailbox;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public a(Lorg/apache/james/mime4j/field/address/parser/c;)Lorg/apache/james/mime4j/field/address/AddressList;
    .locals 3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lorg/apache/james/mime4j/field/address/parser/c;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Lorg/apache/james/mime4j/field/address/parser/c;->a(I)Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/b;

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/b;)Lorg/apache/james/mime4j/field/address/Address;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/james/mime4j/field/address/AddressList;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v1}, Lorg/apache/james/mime4j/field/address/AddressList;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public a(Lorg/apache/james/mime4j/field/address/parser/h;)Lorg/apache/james/mime4j/field/address/Mailbox;
    .locals 2

    new-instance v0, Lorg/apache/james/mime4j/field/address/b;

    invoke-direct {v0, p1}, Lorg/apache/james/mime4j/field/address/b;-><init>(Lorg/apache/james/mime4j/field/address/parser/u;)V

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/a;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/a;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/a;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/d;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/d;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/d;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lorg/apache/james/mime4j/field/address/parser/i;

    if-eqz v1, :cond_2

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/i;

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/a;->a(Lorg/apache/james/mime4j/field/address/parser/i;)Lorg/apache/james/mime4j/field/address/Mailbox;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
