.class public Lorg/apache/james/mime4j/field/address/parser/AddressListParser;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/apache/james/mime4j/field/address/parser/n;
.implements Lorg/apache/james/mime4j/field/address/parser/p;


# static fields
.field private static n:[I

.field private static o:[I


# instance fields
.field protected a:Lorg/apache/james/mime4j/field/address/parser/r;

.field public b:Lorg/apache/james/mime4j/field/address/parser/o;

.field c:Lorg/apache/james/mime4j/field/address/parser/t;

.field public d:Lorg/apache/james/mime4j/field/address/parser/v;

.field public e:Lorg/apache/james/mime4j/field/address/parser/v;

.field public f:Z

.field private h:I

.field private i:Lorg/apache/james/mime4j/field/address/parser/v;

.field private j:Lorg/apache/james/mime4j/field/address/parser/v;

.field private k:I

.field private l:I

.field private final m:[I

.field private final p:[Lorg/apache/james/mime4j/field/address/parser/m;

.field private q:Z

.field private r:I

.field private final s:Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess;

.field private t:Ljava/util/Vector;

.field private u:[I

.field private v:I

.field private w:[I

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->A()V

    invoke-static {}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->B()V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 6

    const/16 v5, 0x16

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-direct {v1}, Lorg/apache/james/mime4j/field/address/parser/r;-><init>()V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    iput-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->f:Z

    new-array v1, v5, [I

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x2

    new-array v1, v1, [Lorg/apache/james/mime4j/field/address/parser/m;

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    iput-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q:Z

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->r:I

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess;-><init>(Lorg/apache/james/mime4j/field/address/parser/l;)V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->s:Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess;

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v:I

    const/16 v1, 0x64

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->w:[I

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-direct {v1, p1, v4, v4}, Lorg/apache/james/mime4j/field/address/parser/t;-><init>(Ljava/io/Reader;II)V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->c:Lorg/apache/james/mime4j/field/address/parser/t;

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/o;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->c:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-direct {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/o;-><init>(Lorg/apache/james/mime4j/field/address/parser/t;)V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b:Lorg/apache/james/mime4j/field/address/parser/o;

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {v1}, Lorg/apache/james/mime4j/field/address/parser/v;-><init>()V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    new-instance v2, Lorg/apache/james/mime4j/field/address/parser/m;

    invoke-direct {v2}, Lorg/apache/james/mime4j/field/address/parser/m;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private static A()V
    .locals 1

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->n:[I

    return-void

    :array_0
    .array-data 4
        0x2
        -0x7fffbfc0
        0x8
        -0x7fffbfc0
        0x50
        -0x7fffbfc0
        -0x7fffbfc0
        -0x7fffbfc0
        0x8
        -0x7fffbfc0
        0x100
        0x108
        0x8
        -0x7fffc000
        -0x7fffc000
        -0x7fffc000
        -0x7fffbe00
        0x200
        -0x7fffc000
        0x4200
        0x200
        0x44000
    .end array-data
.end method

.method private static B()V
    .locals 1

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->o:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private final C()I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e:Lorg/apache/james/mime4j/field/address/parser/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b:Lorg/apache/james/mime4j/field/address/parser/o;

    invoke-virtual {v1}, Lorg/apache/james/mime4j/field/address/parser/o;->b()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e:Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, v0, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0
.end method

.method private final D()V
    .locals 5

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q:Z

    move v1, v2

    :goto_0
    const/4 v0, 0x2

    if-ge v1, v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    aget-object v0, v0, v1

    :cond_0
    iget v3, v0, Lorg/apache/james/mime4j/field/address/parser/m;->a:I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    if-le v3, v4, :cond_1

    iget v3, v0, Lorg/apache/james/mime4j/field/address/parser/m;->c:I

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    iget-object v3, v0, Lorg/apache/james/mime4j/field/address/parser/m;->b:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/m;->d:Lorg/apache/james/mime4j/field/address/parser/m;

    if-nez v0, :cond_0

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->z()Z

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2

    :pswitch_1
    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u()Z
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q:Z

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(II)V
    .locals 7

    const/4 v1, 0x0

    const/16 v0, 0x64

    if-lt p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    add-int/lit8 v0, v0, 0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->w:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    aput p1, v0, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    move v0, v1

    :goto_1
    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->w:[I

    aget v3, v3, v0

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    move v2, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    array-length v3, v0

    iget-object v5, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    array-length v5, v5

    if-ne v3, v5, :cond_6

    const/4 v3, 0x1

    move v2, v1

    :goto_3
    iget-object v5, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    array-length v5, v5

    if-ge v2, v5, :cond_8

    aget v5, v0, v2

    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    aget v6, v6, v2

    if-eq v5, v6, :cond_5

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    :goto_5
    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_4
    if-eqz p2, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->w:[I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    add-int/lit8 v1, p2, -0x1

    aput p1, v0, v1

    goto :goto_0

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move v0, v2

    :cond_7
    move v2, v0

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_5
.end method

.method private final b(II)V
    .locals 3

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    aget-object v0, v0, p1

    :goto_0
    iget v1, v0, Lorg/apache/james/mime4j/field/address/parser/m;->a:I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/m;->d:Lorg/apache/james/mime4j/field/address/parser/m;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/m;

    invoke-direct {v1}, Lorg/apache/james/mime4j/field/address/parser/m;-><init>()V

    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/m;->d:Lorg/apache/james/mime4j/field/address/parser/m;

    move-object v0, v1

    :cond_0
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    add-int/2addr v1, p2

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    sub-int/2addr v1, v2

    iput v1, v0, Lorg/apache/james/mime4j/field/address/parser/m;->a:I

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/m;->b:Lorg/apache/james/mime4j/field/address/parser/v;

    iput p2, v0, Lorg/apache/james/mime4j/field/address/parser/m;->c:I

    return-void

    :cond_1
    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/m;->d:Lorg/apache/james/mime4j/field/address/parser/m;

    goto :goto_0
.end method

.method private final b(I)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    :try_start_0
    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->z()Z
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    throw v0
.end method

.method private final c(I)Z
    .locals 2

    const/4 v1, 0x1

    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    :try_start_0
    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u()Z
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-direct {p0, v1, p1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(II)V

    throw v0
.end method

.method private final d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v2, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v2, v2, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    :goto_0
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget v2, v2, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    if-ne v2, p1, :cond_4

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->r:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->r:I

    const/16 v2, 0x64

    if-le v1, v2, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->r:I

    :goto_1
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p:[Lorg/apache/james/mime4j/field/address/parser/m;

    aget-object v1, v1, v0

    :goto_2
    if-eqz v1, :cond_2

    iget v2, v1, Lorg/apache/james/mime4j/field/address/parser/m;->a:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    if-ge v2, v3, :cond_0

    const/4 v2, 0x0

    iput-object v2, v1, Lorg/apache/james/mime4j/field/address/parser/m;->b:Lorg/apache/james/mime4j/field/address/parser/v;

    :cond_0
    iget-object v1, v1, Lorg/apache/james/mime4j/field/address/parser/m;->d:Lorg/apache/james/mime4j/field/address/parser/m;

    goto :goto_2

    :cond_1
    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b:Lorg/apache/james/mime4j/field/address/parser/o;

    invoke-virtual {v3}, Lorg/apache/james/mime4j/field/address/parser/o;->b()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    return-object v0

    :cond_4
    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v:I

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->r()Lorg/apache/james/mime4j/field/address/parser/ParseException;

    move-result-object v0

    throw v0
.end method

.method private final e(I)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    if-ne v1, v2, :cond_1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v1, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b:Lorg/apache/james/mime4j/field/address/parser/o;

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/parser/o;->b()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    :goto_0
    iget-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    move v2, v0

    :goto_1
    if-eqz v1, :cond_2

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    if-eq v1, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    iget-object v1, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v1, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v1, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-direct {p0, p1, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(II)V

    :cond_3
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget v1, v1, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    if-eq v1, p1, :cond_5

    const/4 v0, 0x1

    :cond_4
    return v0

    :cond_5
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k:I

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j:Lorg/apache/james/mime4j/field/address/parser/v;

    if-ne v1, v2, :cond_4

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->s:Lorg/apache/james/mime4j/field/address/parser/AddressListParser$LookaheadSuccess;

    throw v0
.end method

.method private final s()Z
    .locals 2

    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final t()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final u()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final v()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final w()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final x()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    :cond_0
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final y()Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final z()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/apache/james/mime4j/field/address/parser/c;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d()V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/r;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/c;
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(I)Lorg/apache/james/mime4j/field/address/parser/v;
    .locals 3

    iget-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i:Lorg/apache/james/mime4j/field/address/parser/v;

    :goto_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_1
    if-ge v1, p1, :cond_2

    iget-object v0, v2, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b:Lorg/apache/james/mime4j/field/address/parser/o;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/o;->b()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    iput-object v0, v2, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_2

    :cond_2
    return-object v2
.end method

.method a(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 1

    check-cast p1, Lorg/apache/james/mime4j/field/address/parser/u;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    iput-object v0, p1, Lorg/apache/james/mime4j/field/address/parser/u;->a:Lorg/apache/james/mime4j/field/address/parser/v;

    return-void
.end method

.method public b()Lorg/apache/james/mime4j/field/address/parser/b;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->e()V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/r;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/b;
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 1

    check-cast p1, Lorg/apache/james/mime4j/field/address/parser/u;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    iput-object v0, p1, Lorg/apache/james/mime4j/field/address/parser/u;->b:Lorg/apache/james/mime4j/field/address/parser/v;

    return-void
.end method

.method public c()Lorg/apache/james/mime4j/field/address/parser/h;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->f()V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/r;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/h;
    :try_end_0
    .catch Lorg/apache/james/mime4j/field/address/parser/TokenMgrError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final d()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->g()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    return-void
.end method

.method public final g()V
    .locals 6

    const/4 v5, -0x1

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/c;

    invoke-direct {v3, v2}, Lorg/apache/james/mime4j/field/address/parser/c;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :try_start_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x1

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    :goto_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_2
    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x2

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_2

    :pswitch_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_3

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_4
    sparse-switch v0, :sswitch_data_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x3

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    goto :goto_1

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_3

    :cond_3
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_4

    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :cond_4
    :try_start_5
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_5

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xe -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_1
        0xe -> :sswitch_1
        0x1f -> :sswitch_1
    .end sparse-switch
.end method

.method public final h()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, -0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/b;

    const/4 v0, 0x2

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/b;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const v0, 0x7fffffff

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->o()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_1
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x5

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_1

    :sswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l()V

    goto :goto_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2

    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->n()V

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_3
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x4

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0

    :cond_3
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->k()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :cond_4
    :try_start_5
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_5

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xe -> :sswitch_1
        0x1f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final i()V
    .locals 5

    const/4 v1, -0x1

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/h;

    const/4 v0, 0x3

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/h;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const v0, 0x7fffffff

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->o()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_1
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x6

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_1

    :sswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l()V

    goto :goto_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2

    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->j()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :cond_3
    :try_start_5
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_4

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xe -> :sswitch_1
        0x1f -> :sswitch_1
    .end sparse-switch
.end method

.method public final j()V
    .locals 5

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/i;

    const/4 v0, 0x4

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/i;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->n()V

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v1, 0x0

    :try_start_2
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_1

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_0
    throw v0

    :cond_1
    :try_start_3
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_2

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_0
.end method

.method public final k()V
    .locals 6

    const/4 v0, 0x5

    const/4 v2, 0x1

    const/4 v5, -0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/f;

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/f;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const/4 v0, 0x4

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/4 v1, 0x7

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    :goto_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_2
    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x8

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_2

    :pswitch_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_3

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_4
    sparse-switch v0, :sswitch_data_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x9

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    goto :goto_1

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_3

    :cond_3
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_4

    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->i()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :cond_4
    :try_start_5
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_5

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xe -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_1
        0xe -> :sswitch_1
        0x1f -> :sswitch_1
    .end sparse-switch
.end method

.method public final l()V
    .locals 5

    const/4 v0, 0x6

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/d;

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/d;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const/4 v0, 0x6

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0xa

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    :goto_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->o()V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_2

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_3

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public final m()V
    .locals 6

    const/4 v5, -0x1

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/k;

    const/4 v0, 0x7

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/k;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const/16 v0, 0x8

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q()V

    :goto_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_1
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0xb

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    :sswitch_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_2
    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0xc

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v0, v1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x0

    :try_start_3
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_1
    throw v0

    :cond_2
    :try_start_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :cond_3
    :try_start_5
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_4

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final n()V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, -0x1

    new-instance v1, Lorg/apache/james/mime4j/field/address/parser/j;

    const/16 v0, 0x8

    invoke-direct {v1, v0}, Lorg/apache/james/mime4j/field/address/parser/j;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v1}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :sswitch_0
    :try_start_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v2, 0xd

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v2

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v2, v1, v4}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :sswitch_1
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    :goto_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_2
    sparse-switch v0, :sswitch_data_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v2, 0xe

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v1, v4}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :sswitch_2
    const/16 v0, 0x1f

    :try_start_2
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_1

    :cond_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0xe -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xe -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch
.end method

.method public final o()V
    .locals 5

    const/4 v2, 0x1

    new-instance v3, Lorg/apache/james/mime4j/field/address/parser/a;

    const/16 v0, 0x9

    invoke-direct {v3, v0}, Lorg/apache/james/mime4j/field/address/parser/a;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->p()V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->q()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3}, Lorg/apache/james/mime4j/field/address/parser/r;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v1, 0x0

    :try_start_2
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_1

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v3, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :cond_0
    throw v0

    :cond_1
    :try_start_3
    instance-of v4, v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    if-eqz v4, :cond_2

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/Error;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_0
.end method

.method public final p()V
    .locals 8

    const/4 v7, 0x1

    const/16 v6, 0x1f

    const/4 v5, -0x1

    new-instance v2, Lorg/apache/james/mime4j/field/address/parser/g;

    const/16 v0, 0xa

    invoke-direct {v2, v0}, Lorg/apache/james/mime4j/field/address/parser/g;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :try_start_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0xf

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v2, v7}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :sswitch_0
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    :goto_1
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v1, v5, :cond_1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v1

    :goto_2
    sparse-switch v1, :sswitch_data_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x10

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v2, v7}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :sswitch_1
    const/16 v0, 0x1f

    :try_start_2
    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_2

    :sswitch_2
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v1, v5, :cond_3

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v1

    :goto_3
    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v3, 0x11

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v1, v3

    :goto_4
    iget v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    if-eq v1, v6, :cond_2

    iget-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_4

    :cond_2
    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    const-string v1, "Words in local part must be separated by \'.\'"

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_3

    :pswitch_0
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    goto :goto_4

    :cond_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_5

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_5
    sparse-switch v0, :sswitch_data_2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x12

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0

    :cond_5
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_5

    :sswitch_3
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_4
    const/16 v0, 0x1f

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0xe -> :sswitch_0
        0x1f -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_2
        0xe -> :sswitch_2
        0x1f -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        0xe -> :sswitch_3
        0x1f -> :sswitch_4
    .end sparse-switch
.end method

.method public final q()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, -0x1

    new-instance v2, Lorg/apache/james/mime4j/field/address/parser/e;

    const/16 v0, 0xb

    invoke-direct {v2, v0}, Lorg/apache/james/mime4j/field/address/parser/e;-><init>(I)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v2}, Lorg/apache/james/mime4j/field/address/parser/r;->c(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    :try_start_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v0, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v0

    :goto_0
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x15

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v1, v2, v6}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_0

    :sswitch_0
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    :goto_1
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v1, v5, :cond_1

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v1

    :goto_2
    sparse-switch v1, :sswitch_data_1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v1, 0x13

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v3, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a:Lorg/apache/james/mime4j/field/address/parser/r;

    invoke-virtual {v0, v2, v6}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V

    invoke-virtual {p0, v2}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->b(Lorg/apache/james/mime4j/field/address/parser/s;)V

    return-void

    :cond_1
    :try_start_2
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_2

    :sswitch_1
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    if-ne v1, v5, :cond_2

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->C()I

    move-result v1

    :goto_4
    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    const/16 v3, 0x14

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    aput v4, v1, v3

    :goto_5
    iget-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    iget-object v0, v0, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_3

    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    const-string v1, "Atoms in domain names must be separated by \'.\'"

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->h:I

    goto :goto_4

    :pswitch_0
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    goto :goto_5

    :cond_3
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    goto :goto_1

    :sswitch_2
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d(I)Lorg/apache/james/mime4j/field/address/parser/v;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0xe -> :sswitch_0
        0x12 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_1
        0xe -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public r()Lorg/apache/james/mime4j/field/address/parser/ParseException;
    .locals 8

    const/16 v7, 0x22

    const/4 v6, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->removeAllElements()V

    new-array v3, v7, [Z

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_0

    aput-boolean v0, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v:I

    if-ltz v1, :cond_1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v:I

    aput-boolean v6, v3, v1

    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->v:I

    :cond_1
    move v2, v0

    :goto_1
    const/16 v1, 0x16

    if-ge v2, v1, :cond_5

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->m:[I

    aget v1, v1, v2

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->l:I

    if-ne v1, v4, :cond_4

    move v1, v0

    :goto_2
    const/16 v4, 0x20

    if-ge v1, v4, :cond_4

    sget-object v4, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->n:[I

    aget v4, v4, v2

    shl-int v5, v6, v1

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    aput-boolean v6, v3, v1

    :cond_2
    sget-object v4, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->o:[I

    aget v4, v4, v2

    shl-int v5, v6, v1

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    add-int/lit8 v4, v1, 0x20

    aput-boolean v6, v3, v4

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_5
    move v1, v0

    :goto_3
    if-ge v1, v7, :cond_7

    aget-boolean v2, v3, v1

    if-eqz v2, :cond_6

    new-array v2, v6, [I

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    aput v1, v2, v0

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->u:[I

    invoke-virtual {v2, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->x:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->D()V

    invoke-direct {p0, v0, v0}, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a(II)V

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v2, v1, [[I

    move v1, v0

    :goto_4
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->t:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_8
    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/ParseException;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->d:Lorg/apache/james/mime4j/field/address/parser/v;

    sget-object v3, Lorg/apache/james/mime4j/field/address/parser/AddressListParser;->a_:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/james/mime4j/field/address/parser/ParseException;-><init>(Lorg/apache/james/mime4j/field/address/parser/v;[[I[Ljava/lang/String;)V

    return-object v0
.end method
