.class public Lorg/apache/james/mime4j/field/address/parser/o;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/apache/james/mime4j/field/address/parser/n;


# static fields
.field static a:I

.field static final c:[J

.field static final d:[I

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field public static final h:[I

.field static final i:[J

.field static final j:[J

.field static final k:[J

.field static final l:[J


# instance fields
.field public b:Ljava/io/PrintStream;

.field protected m:Lorg/apache/james/mime4j/field/address/parser/t;

.field n:Ljava/lang/StringBuffer;

.field o:I

.field p:I

.field protected q:C

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field private final x:[I

.field private final y:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-array v0, v6, [J

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    new-array v0, v4, [I

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->d:[I

    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    const-string v1, "\r"

    aput-object v1, v0, v5

    const-string v1, "\n"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string v2, ","

    aput-object v2, v0, v1

    const-string v1, ":"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, ";"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "@"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0xa

    aput-object v3, v0, v1

    const/16 v1, 0xb

    aput-object v3, v0, v1

    const/16 v1, 0xc

    aput-object v3, v0, v1

    const/16 v1, 0xd

    aput-object v3, v0, v1

    const/16 v1, 0xe

    aput-object v3, v0, v1

    const/16 v1, 0xf

    aput-object v3, v0, v1

    const/16 v1, 0x10

    aput-object v3, v0, v1

    const/16 v1, 0x11

    aput-object v3, v0, v1

    const/16 v1, 0x12

    aput-object v3, v0, v1

    const/16 v1, 0x13

    aput-object v3, v0, v1

    const/16 v1, 0x14

    aput-object v3, v0, v1

    const/16 v1, 0x15

    aput-object v3, v0, v1

    const/16 v1, 0x16

    aput-object v3, v0, v1

    const/16 v1, 0x17

    aput-object v3, v0, v1

    const/16 v1, 0x18

    aput-object v3, v0, v1

    const/16 v1, 0x19

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    aput-object v3, v0, v1

    const/16 v1, 0x1c

    aput-object v3, v0, v1

    const/16 v1, 0x1d

    aput-object v3, v0, v1

    const/16 v1, 0x1e

    aput-object v3, v0, v1

    const/16 v1, 0x1f

    aput-object v3, v0, v1

    const/16 v1, 0x20

    aput-object v3, v0, v1

    const/16 v1, 0x21

    aput-object v3, v0, v1

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->e:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "DEFAULT"

    aput-object v1, v0, v4

    const-string v1, "INDOMAINLITERAL"

    aput-object v1, v0, v5

    const-string v1, "INCOMMENT"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string v2, "NESTED_COMMENT"

    aput-object v2, v0, v1

    const-string v1, "INQUOTEDSTRING"

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->f:[Ljava/lang/String;

    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    new-array v0, v5, [J

    const-wide v2, 0x800443ffL

    aput-wide v2, v0, v4

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->i:[J

    new-array v0, v5, [J

    const-wide/32 v2, 0x100400

    aput-wide v2, v0, v4

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->j:[J

    new-array v0, v5, [J

    const-wide/16 v2, 0x400

    aput-wide v2, v0, v4

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->k:[J

    new-array v0, v5, [J

    const-wide/32 v2, 0x7feb8000

    aput-wide v2, v0, v4

    sput-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->l:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        -0x1
        -0x1
    .end array-data

    :array_1
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x1
        -0x1
        -0x1
        0x0
        0x2
        0x0
        -0x1
        0x3
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x4
        -0x1
        -0x1
        0x0
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/james/mime4j/field/address/parser/t;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->b:Ljava/io/PrintStream;

    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->x:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->s:I

    iput-object p1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    return-void
.end method

.method private final a(II)I
    .locals 1

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    add-int/lit8 v0, p1, 0x1

    return v0
.end method

.method private final b(II)I
    .locals 13

    const/4 v12, 0x2

    const v1, 0x7fffffff

    const/4 v4, 0x0

    const-wide/16 v10, 0x0

    const/16 v5, 0xe

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v0, 0x1

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    aput p1, v2, v4

    move v2, v0

    move v3, v4

    move v0, v1

    :goto_0
    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    if-ne v6, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->i()V

    :cond_0
    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v7, 0x40

    if-ge v6, v7, :cond_8

    const-wide/16 v6, 0x1

    iget-char v8, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    shl-long/2addr v6, v8

    :cond_1
    iget-object v8, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v2, v2, -0x1

    aget v8, v8, v2

    packed-switch v8, :pswitch_data_0

    :cond_2
    :goto_1
    if-ne v2, v3, :cond_1

    :goto_2
    if-eq v0, v1, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    move v0, v1

    :cond_3
    add-int/lit8 p2, p2, 0x1

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    rsub-int/lit8 v3, v3, 0x3

    if-ne v2, v3, :cond_e

    :goto_3
    return p2

    :pswitch_0
    const-wide v8, -0x5c00530600000000L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_5

    if-le v0, v5, :cond_4

    move v0, v5

    :cond_4
    invoke-direct {p0, v12}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_1

    :cond_5
    const-wide v8, 0x100000200L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    const/16 v8, 0xa

    if-le v0, v8, :cond_6

    const/16 v0, 0xa

    :cond_6
    invoke-direct {p0, v4}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_1

    :pswitch_1
    const-wide v8, 0x100000200L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    const/16 v0, 0xa

    invoke-direct {p0, v4}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_1

    :pswitch_2
    const-wide v8, -0x5c00130600000000L

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    if-le v0, v5, :cond_7

    move v0, v5

    :cond_7
    invoke-direct {p0, v12}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_1

    :cond_8
    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v7, 0x80

    if-ge v6, v7, :cond_c

    const-wide/16 v6, 0x1

    iget-char v8, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v8, v8, 0x3f

    shl-long/2addr v6, v8

    :cond_9
    iget-object v8, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v2, v2, -0x1

    aget v8, v8, v2

    packed-switch v8, :pswitch_data_1

    :cond_a
    :goto_4
    if-ne v2, v3, :cond_9

    goto :goto_2

    :pswitch_3
    const-wide v8, 0x7fffffffc7fffffeL

    and-long/2addr v8, v6

    cmp-long v8, v8, v10

    if-eqz v8, :cond_a

    if-le v0, v5, :cond_b

    move v0, v5

    :cond_b
    invoke-direct {p0, v12}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_4

    :cond_c
    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit16 v6, v6, 0xff

    shr-int/lit8 v6, v6, 0x6

    const-wide/16 v6, 0x1

    iget-char v8, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v8, v8, 0x3f

    shl-long/2addr v6, v8

    :cond_d
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v2, v2, -0x1

    aget v6, v6, v2

    if-ne v2, v3, :cond_d

    goto/16 :goto_2

    :cond_e
    :try_start_0
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v6}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v6

    iput-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private final b(I)V
    .locals 3

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->x:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    aput p1, v0, v1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->x:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    aput v1, v0, p1

    :cond_0
    return-void
.end method

.method private final c(II)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    const/4 v3, 0x0

    aput p1, v0, v3

    const v0, 0x7fffffff

    :goto_0
    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->i()V

    :cond_0
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_4

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    shl-long/2addr v4, v3

    :cond_1
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    const v0, 0x7fffffff

    :cond_3
    add-int/lit8 p2, p2, 0x1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_b

    :goto_3
    return p2

    :pswitch_0
    const/16 v3, 0x17

    if-le v0, v3, :cond_2

    const/16 v0, 0x17

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x15

    if-le v0, v3, :cond_2

    const/16 v0, 0x15

    goto :goto_1

    :cond_4
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_8

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v3, v3, 0x3f

    shl-long/2addr v4, v3

    :cond_5
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    :cond_6
    :goto_4
    if-ne v1, v2, :cond_5

    goto :goto_2

    :pswitch_2
    const/16 v3, 0x17

    if-le v0, v3, :cond_7

    const/16 v0, 0x17

    :cond_7
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v5, 0x1

    aput v5, v3, v4

    goto :goto_4

    :pswitch_3
    const/16 v3, 0x15

    if-le v0, v3, :cond_6

    const/16 v0, 0x15

    goto :goto_4

    :pswitch_4
    const/16 v3, 0x17

    if-le v0, v3, :cond_6

    const/16 v0, 0x17

    goto :goto_4

    :cond_8
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    const-wide/16 v4, 0x1

    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    :cond_9
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    :cond_a
    :goto_5
    if-ne v1, v2, :cond_9

    goto :goto_2

    :pswitch_5
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x17

    if-le v0, v6, :cond_a

    const/16 v0, 0x17

    goto :goto_5

    :pswitch_6
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x15

    if-le v0, v6, :cond_a

    const/16 v0, 0x15

    goto :goto_5

    :cond_b
    :try_start_0
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v3}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v3

    iput-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private final d()I
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    sparse-switch v0, :sswitch_data_0

    invoke-direct {p0, v2, v1}, Lorg/apache/james/mime4j/field/address/parser/o;->b(II)I

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, v1, v2}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1c

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x13

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x3

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x9

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_6
    const/4 v0, 0x4

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_7
    const/4 v0, 0x5

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_8
    const/4 v0, 0x6

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_9
    const/4 v0, 0x7

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_a
    const/16 v0, 0x8

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_b
    const/16 v0, 0xf

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xd -> :sswitch_1
        0x22 -> :sswitch_2
        0x28 -> :sswitch_3
        0x2c -> :sswitch_4
        0x2e -> :sswitch_5
        0x3a -> :sswitch_6
        0x3b -> :sswitch_7
        0x3c -> :sswitch_8
        0x3e -> :sswitch_9
        0x40 -> :sswitch_a
        0x5b -> :sswitch_b
    .end sparse-switch
.end method

.method private final d(II)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    const/4 v3, 0x0

    aput p1, v0, v3

    const v0, 0x7fffffff

    :goto_0
    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->i()V

    :cond_0
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_5

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    shl-long/2addr v4, v3

    :cond_1
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    const v0, 0x7fffffff

    :cond_3
    add-int/lit8 p2, p2, 0x1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_f

    :goto_3
    return p2

    :pswitch_0
    const-wide v6, -0x400000001L

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_2

    const/16 v3, 0x1e

    if-le v0, v3, :cond_4

    const/16 v0, 0x1e

    :cond_4
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x1d

    if-le v0, v3, :cond_2

    const/16 v0, 0x1d

    goto :goto_1

    :cond_5
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_b

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v3, v3, 0x3f

    shl-long/2addr v4, v3

    :cond_6
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    :cond_7
    :goto_4
    if-ne v1, v2, :cond_6

    goto :goto_2

    :pswitch_2
    const-wide/32 v6, -0x10000001

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_9

    const/16 v3, 0x1e

    if-le v0, v3, :cond_8

    const/16 v0, 0x1e

    :cond_8
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_4

    :cond_9
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v6, 0x5c

    if-ne v3, v6, :cond_7

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v7, 0x1

    aput v7, v3, v6

    goto :goto_4

    :pswitch_3
    const/16 v3, 0x1d

    if-le v0, v3, :cond_7

    const/16 v0, 0x1d

    goto :goto_4

    :pswitch_4
    const-wide/32 v6, -0x10000001

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_7

    const/16 v3, 0x1e

    if-le v0, v3, :cond_a

    const/16 v0, 0x1e

    :cond_a
    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_4

    :cond_b
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    const-wide/16 v4, 0x1

    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    :cond_c
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    :cond_d
    :goto_5
    if-ne v1, v2, :cond_c

    goto/16 :goto_2

    :pswitch_5
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_d

    const/16 v6, 0x1e

    if-le v0, v6, :cond_e

    const/16 v0, 0x1e

    :cond_e
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lorg/apache/james/mime4j/field/address/parser/o;->b(I)V

    goto :goto_5

    :pswitch_6
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_d

    const/16 v6, 0x1d

    if-le v0, v6, :cond_d

    const/16 v0, 0x1d

    goto :goto_5

    :cond_f
    :try_start_0
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v3}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v3

    iput-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private final e()I
    .locals 2

    const/4 v1, 0x0

    iget-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lorg/apache/james/mime4j/field/address/parser/o;->c(II)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x16

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final e(II)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    const/4 v3, 0x0

    aput p1, v0, v3

    const v0, 0x7fffffff

    :goto_0
    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->i()V

    :cond_0
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_4

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    shl-long/2addr v4, v3

    :cond_1
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    const v0, 0x7fffffff

    :cond_3
    add-int/lit8 p2, p2, 0x1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_b

    :goto_3
    return p2

    :pswitch_0
    const/16 v3, 0x1b

    if-le v0, v3, :cond_2

    const/16 v0, 0x1b

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x18

    if-le v0, v3, :cond_2

    const/16 v0, 0x18

    goto :goto_1

    :cond_4
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_8

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v3, v3, 0x3f

    shl-long/2addr v4, v3

    :cond_5
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    :cond_6
    :goto_4
    if-ne v1, v2, :cond_5

    goto :goto_2

    :pswitch_2
    const/16 v3, 0x1b

    if-le v0, v3, :cond_7

    const/16 v0, 0x1b

    :cond_7
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v5, 0x1

    aput v5, v3, v4

    goto :goto_4

    :pswitch_3
    const/16 v3, 0x18

    if-le v0, v3, :cond_6

    const/16 v0, 0x18

    goto :goto_4

    :pswitch_4
    const/16 v3, 0x1b

    if-le v0, v3, :cond_6

    const/16 v0, 0x1b

    goto :goto_4

    :cond_8
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    const-wide/16 v4, 0x1

    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    :cond_9
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    :cond_a
    :goto_5
    if-ne v1, v2, :cond_9

    goto :goto_2

    :pswitch_5
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x1b

    if-le v0, v6, :cond_a

    const/16 v0, 0x1b

    goto :goto_5

    :pswitch_6
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x18

    if-le v0, v6, :cond_a

    const/16 v0, 0x18

    goto :goto_5

    :cond_b
    :try_start_0
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v3}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v3

    iput-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private final f()I
    .locals 2

    const/4 v1, 0x0

    iget-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lorg/apache/james/mime4j/field/address/parser/o;->d(II)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x1f

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x22
        :pswitch_0
    .end packed-switch
.end method

.method private final f(II)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    const/4 v3, 0x0

    aput p1, v0, v3

    const v0, 0x7fffffff

    :goto_0
    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->i()V

    :cond_0
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x40

    if-ge v3, v4, :cond_4

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    shl-long/2addr v4, v3

    :cond_1
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    if-ne v1, v2, :cond_1

    :goto_2
    const v1, 0x7fffffff

    if-eq v0, v1, :cond_3

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    const v0, 0x7fffffff

    :cond_3
    add-int/lit8 p2, p2, 0x1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    iput v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    rsub-int/lit8 v2, v2, 0x3

    if-ne v1, v2, :cond_b

    :goto_3
    return p2

    :pswitch_0
    const/16 v3, 0x11

    if-le v0, v3, :cond_2

    const/16 v0, 0x11

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x10

    if-le v0, v3, :cond_2

    const/16 v0, 0x10

    goto :goto_1

    :cond_4
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v4, 0x80

    if-ge v3, v4, :cond_8

    const-wide/16 v4, 0x1

    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v3, v3, 0x3f

    shl-long/2addr v4, v3

    :cond_5
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v3, v1

    packed-switch v3, :pswitch_data_1

    :cond_6
    :goto_4
    if-ne v1, v2, :cond_5

    goto :goto_2

    :pswitch_2
    const-wide/32 v6, -0x38000001

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_7

    const/16 v3, 0x11

    if-le v0, v3, :cond_6

    const/16 v0, 0x11

    goto :goto_4

    :cond_7
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v6, 0x5c

    if-ne v3, v6, :cond_6

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->t:I

    const/4 v7, 0x1

    aput v7, v3, v6

    goto :goto_4

    :pswitch_3
    const/16 v3, 0x10

    if-le v0, v3, :cond_6

    const/16 v0, 0x10

    goto :goto_4

    :pswitch_4
    const-wide/32 v6, -0x38000001

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_6

    const/16 v3, 0x11

    if-le v0, v3, :cond_6

    const/16 v0, 0x11

    goto :goto_4

    :cond_8
    iget-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v3, v3, 0x6

    const-wide/16 v4, 0x1

    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    and-int/lit8 v6, v6, 0x3f

    shl-long/2addr v4, v6

    :cond_9
    iget-object v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v6, v6, v1

    packed-switch v6, :pswitch_data_2

    :cond_a
    :goto_5
    if-ne v1, v2, :cond_9

    goto/16 :goto_2

    :pswitch_5
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x11

    if-le v0, v6, :cond_a

    const/16 v0, 0x11

    goto :goto_5

    :pswitch_6
    sget-object v6, Lorg/apache/james/mime4j/field/address/parser/o;->c:[J

    aget-wide v6, v6, v3

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    const/16 v6, 0x10

    if-le v0, v6, :cond_a

    const/16 v0, 0x10

    goto :goto_5

    :cond_b
    :try_start_0
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v3}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v3

    iput-char v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private final g()I
    .locals 2

    const/4 v1, 0x0

    iget-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lorg/apache/james/mime4j/field/address/parser/o;->e(II)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x19

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x1a

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final h()I
    .locals 2

    const/4 v1, 0x0

    iget-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lorg/apache/james/mime4j/field/address/parser/o;->f(II)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x12

    invoke-direct {p0, v1, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(II)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5d
        :pswitch_0
    .end packed-switch
.end method

.method private final i()V
    .locals 3

    const v0, -0x7fffffff

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->u:I

    const/4 v0, 0x3

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->x:[I

    const/high16 v2, -0x80000000

    aput v2, v0, v1

    move v0, v1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Lorg/apache/james/mime4j/field/address/parser/v;
    .locals 3

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    invoke-static {v0}, Lorg/apache/james/mime4j/field/address/parser/v;->a(I)Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->a:I

    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->e:[Ljava/lang/String;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->h()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->g()I

    move-result v0

    iput v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->b:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->f()I

    move-result v0

    iput v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->c:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->e()I

    move-result v0

    iput v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->d:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->d()I

    move-result v0

    iput v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->e:I

    return-object v1
.end method

.method public a(I)V
    .locals 3

    const/4 v0, 0x5

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Ignoring invalid lexical state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State unchanged."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    return-void
.end method

.method a(Lorg/apache/james/mime4j/field/address/parser/v;)V
    .locals 4

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->p:I

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->p:I

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lorg/apache/james/mime4j/field/address/parser/v;->f:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x1f -> :sswitch_1
    .end sparse-switch
.end method

.method public b()Lorg/apache/james/mime4j/field/address/parser/v;
    .locals 13

    const/4 v5, 0x0

    const/4 v12, -0x1

    const/4 v6, 0x1

    const v4, 0x7fffffff

    const/4 v7, 0x0

    move v0, v7

    move-object v1, v5

    :goto_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b()C

    move-result v2

    iput-char v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iput-object v5, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    :goto_1
    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    packed-switch v2, :pswitch_data_0

    move v2, v0

    :goto_2
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    if-eq v0, v4, :cond_6

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    add-int/lit8 v0, v0, 0x1

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    sub-int v3, v2, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Lorg/apache/james/mime4j/field/address/parser/t;->a(I)V

    :cond_0
    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->i:[J

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v8, v0, v3

    const-wide/16 v10, 0x1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    and-int/lit8 v0, v0, 0x3f

    shl-long/2addr v10, v0

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->a()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->h:Lorg/apache/james/mime4j/field/address/parser/v;

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(Lorg/apache/james/mime4j/field/address/parser/v;)V

    sget-object v1, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v1, v1, v2

    if-eq v1, v12, :cond_1

    sget-object v1, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    :cond_1
    :goto_3
    return-object v0

    :catch_0
    move-exception v0

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->a()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->h:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_3

    :pswitch_0
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->d()I

    move-result v0

    move v2, v0

    goto :goto_2

    :pswitch_1
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->h()I

    move-result v0

    move v2, v0

    goto :goto_2

    :pswitch_2
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->e()I

    move-result v0

    move v2, v0

    goto :goto_2

    :pswitch_3
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->g()I

    move-result v0

    move v2, v0

    goto :goto_2

    :pswitch_4
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    iput v7, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->f()I

    move-result v0

    move v2, v0

    goto :goto_2

    :cond_2
    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->j:[J

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v8, v0, v3

    const-wide/16 v10, 0x1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    and-int/lit8 v0, v0, 0x3f

    shl-long/2addr v10, v0

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_4

    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->k:[J

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    shr-int/lit8 v3, v3, 0x6

    aget-wide v8, v0, v3

    const-wide/16 v10, 0x1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    and-int/lit8 v0, v0, 0x3f

    shl-long/2addr v10, v0

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->a()Lorg/apache/james/mime4j/field/address/parser/v;

    move-result-object v0

    if-nez v1, :cond_3

    :goto_4
    sget-object v1, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v1, v1, v3

    if-eq v1, v12, :cond_d

    sget-object v1, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v1, v1, v3

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    move-object v1, v0

    move v0, v2

    goto/16 :goto_0

    :cond_3
    iput-object v1, v0, Lorg/apache/james/mime4j/field/address/parser/v;->h:Lorg/apache/james/mime4j/field/address/parser/v;

    iput-object v0, v1, Lorg/apache/james/mime4j/field/address/parser/v;->g:Lorg/apache/james/mime4j/field/address/parser/v;

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/o;->c()V

    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v0, v0, v2

    if-eq v0, v12, :cond_5

    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/o;->h:[I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    aget v0, v0, v2

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    :cond_5
    iput v4, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    :try_start_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v0

    iput-char v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v7

    goto/16 :goto_1

    :catch_1
    move-exception v0

    move v2, v7

    :cond_6
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->e()I

    move-result v3

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->d()I

    move-result v4

    :try_start_2
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/james/mime4j/field/address/parser/t;->a(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move v1, v7

    :goto_5
    if-nez v1, :cond_7

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0, v6}, Lorg/apache/james/mime4j/field/address/parser/t;->a(I)V

    if-gt v2, v6, :cond_b

    const-string v0, ""

    :goto_6
    move-object v5, v0

    :cond_7
    new-instance v0, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->r:I

    iget-char v6, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    invoke-direct/range {v0 .. v7}, Lorg/apache/james/mime4j/field/address/parser/TokenMgrError;-><init>(ZIIILjava/lang/String;CI)V

    throw v0

    :catch_2
    move-exception v0

    if-gt v2, v6, :cond_9

    const-string v0, ""

    :goto_7
    iget-char v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v5, 0xa

    if-eq v1, v5, :cond_8

    iget-char v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->q:C

    const/16 v5, 0xd

    if-ne v1, v5, :cond_a

    :cond_8
    add-int/lit8 v3, v3, 0x1

    move v1, v6

    move-object v5, v0

    move v4, v7

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_a
    add-int/lit8 v4, v4, 0x1

    move v1, v6

    move-object v5, v0

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/field/address/parser/t;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_c
    move-object v0, v1

    goto/16 :goto_4

    :cond_d
    move-object v1, v0

    move v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method c()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->v:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->p:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->w:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_2
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_3
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    const/4 v0, 0x1

    sput v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_4
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_5
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    sget v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_6
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    sget v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    sget v0, Lorg/apache/james/mime4j/field/address/parser/o;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/o;->a(I)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_7
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    :cond_8
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->m:Lorg/apache/james/mime4j/field/address/parser/t;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    invoke-virtual {v1, v2}, Lorg/apache/james/mime4j/field/address/parser/t;->b(I)[C

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    iput v3, p0, Lorg/apache/james/mime4j/field/address/parser/o;->o:I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/o;->n:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
