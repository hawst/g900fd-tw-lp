.class public Lorg/apache/james/mime4j/field/address/parser/t;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field b:I

.field c:I

.field public d:I

.field protected e:[I

.field protected f:[I

.field protected g:I

.field protected h:I

.field protected i:Z

.field protected j:Z

.field protected k:Ljava/io/Reader;

.field protected l:[C

.field protected m:I

.field protected n:I

.field protected o:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;II)V
    .locals 1

    const/16 v0, 0x1000

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/james/mime4j/field/address/parser/t;-><init>(Ljava/io/Reader;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;III)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    iput-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->i:Z

    iput-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->j:Z

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->o:I

    iput-object p1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->k:Ljava/io/Reader;

    iput p2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    add-int/lit8 v0, p3, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    iput p4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iput p4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    new-array v0, p4, [C

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    new-array v0, p4, [I

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    new-array v0, p4, [I

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 7

    const/16 v2, 0x800

    const/4 v6, -0x1

    const/4 v5, 0x0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    if-le v0, v2, :cond_2

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->k:Ljava/io/Reader;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/Reader;->read([CII)I

    move-result v0

    if-ne v0, v6, :cond_7

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->k:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    invoke-virtual {p0, v5}, Lorg/apache/james/mime4j/field/address/parser/t;->a(I)V

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    if-ne v1, v6, :cond_1

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    :cond_1
    throw v0

    :cond_2
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    if-gez v0, :cond_3

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    iput v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v5}, Lorg/apache/james/mime4j/field/address/parser/t;->a(Z)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    if-le v0, v1, :cond_5

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    goto :goto_0

    :cond_5
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    sub-int/2addr v0, v1

    if-ge v0, v2, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/t;->a(Z)V

    goto :goto_0

    :cond_6
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    goto :goto_0

    :cond_7
    :try_start_1
    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method

.method protected a(C)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    iget-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->j:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->j:Z

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    aput v2, v0, v1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    aput v2, v0, v1

    return-void

    :cond_1
    iget-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->i:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->i:Z

    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    iput-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->j:Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->h:I

    goto :goto_0

    :pswitch_1
    iput-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->i:Z

    goto :goto_1

    :pswitch_2
    iput-boolean v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->j:Z

    goto :goto_1

    :pswitch_3
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->o:I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->o:I

    rem-int/2addr v2, v3

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->g:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(I)V
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    if-gez v0, :cond_0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 9

    const/4 v8, 0x0

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    add-int/lit16 v0, v0, 0x800

    new-array v0, v0, [C

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    add-int/lit16 v1, v1, 0x800

    new-array v1, v1, [I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    add-int/lit16 v2, v2, 0x800

    new-array v2, v2, [I

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v5, 0x0

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v7, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v6, v7

    invoke-static {v3, v4, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v5, v6

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    invoke-static {v3, v4, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v5, v6

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    const/4 v1, 0x0

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v3, v4

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    add-int/lit16 v0, v0, 0x800

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->b:I

    iput v8, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    return-void

    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v5, 0x0

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v7, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v6, v7

    invoke-static {v3, v4, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v6, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v5, v6

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public b()C
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/t;->c()C

    move-result v0

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    return v0
.end method

.method public b(I)[C
    .locals 5

    const/4 v4, 0x0

    new-array v0, p1, [C

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v1, v1, 0x1

    if-lt v1, p1, :cond_0

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    sub-int/2addr v2, p1

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2, v0, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    sub-int v3, p1, v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    sub-int v3, p1, v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    sub-int v2, p1, v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public c()C
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    if-lez v0, :cond_1

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->n:I

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    aget-char v0, v0, v1

    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->m:I

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/t;->a()V

    :cond_2
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/james/mime4j/field/address/parser/t;->a(C)V

    goto :goto_0
.end method

.method public d()I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->f:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    aget v0, v0, v1

    return v0
.end method

.method public g()I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->e:[I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    aget v0, v0, v1

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 6

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    iget v3, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->a:I

    iget v5, p0, Lorg/apache/james/mime4j/field/address/parser/t;->c:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/t;->l:[C

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/james/mime4j/field/address/parser/t;->d:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
