.class public Lorg/apache/james/mime4j/codec/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private a:Lorg/apache/james/mime4j/codec/f;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/codec/c;->b:I

    new-instance v0, Lorg/apache/james/mime4j/codec/f;

    invoke-direct {v0}, Lorg/apache/james/mime4j/codec/f;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    return-void
.end method


# virtual methods
.method public a()B
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/codec/f;->c()B

    move-result v0

    return v0
.end method

.method public a(B)V
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-virtual {v0, p1}, Lorg/apache/james/mime4j/codec/f;->a(B)Z

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/codec/f;->a()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/codec/c;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lorg/apache/james/mime4j/codec/f;

    iget v1, p0, Lorg/apache/james/mime4j/codec/c;->b:I

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/codec/f;-><init>(I)V

    iput-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lorg/apache/james/mime4j/codec/f;

    invoke-direct {v0}, Lorg/apache/james/mime4j/codec/f;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/c;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/codec/f;->d()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
