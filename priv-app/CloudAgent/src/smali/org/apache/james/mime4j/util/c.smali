.class Lorg/apache/james/mime4j/util/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/util/c;->a:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/james/mime4j/util/c;->b:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/james/mime4j/util/c;->c:[Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/james/mime4j/util/c;->a:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/james/mime4j/util/c;->b:Ljava/lang/String;

    iput-object p3, p0, Lorg/apache/james/mime4j/util/c;->c:[Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lorg/apache/james/mime4j/util/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/james/mime4j/util/c;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lorg/apache/james/mime4j/util/c;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/util/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lorg/apache/james/mime4j/util/c;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/util/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lorg/apache/james/mime4j/util/c;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/util/c;->c:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lorg/apache/james/mime4j/util/c;)I
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/util/c;->a:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/james/mime4j/util/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lorg/apache/james/mime4j/util/c;

    invoke-virtual {p0, p1}, Lorg/apache/james/mime4j/util/c;->a(Lorg/apache/james/mime4j/util/c;)I

    move-result v0

    return v0
.end method
