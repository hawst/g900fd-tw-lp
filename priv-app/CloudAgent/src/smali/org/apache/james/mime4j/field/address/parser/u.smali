.class public Lorg/apache/james/mime4j/field/address/parser/u;
.super Lorg/apache/james/mime4j/field/address/parser/q;

# interfaces
.implements Lorg/apache/james/mime4j/field/address/parser/s;


# instance fields
.field protected c:Lorg/apache/james/mime4j/field/address/parser/s;

.field protected d:[Lorg/apache/james/mime4j/field/address/parser/s;

.field protected e:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/james/mime4j/field/address/parser/q;-><init>()V

    iput p1, p0, Lorg/apache/james/mime4j/field/address/parser/u;->e:I

    return-void
.end method


# virtual methods
.method public a(I)Lorg/apache/james/mime4j/field/address/parser/s;
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/james/mime4j/field/address/parser/u;->c:Lorg/apache/james/mime4j/field/address/parser/s;

    return-void
.end method

.method public a(Lorg/apache/james/mime4j/field/address/parser/s;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    if-nez v0, :cond_1

    add-int/lit8 v0, p2, 0x1

    new-array v0, v0, [Lorg/apache/james/mime4j/field/address/parser/s;

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    aput-object p1, v0, p2

    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    array-length v0, v0

    if-lt p2, v0, :cond_0

    add-int/lit8 v0, p2, 0x1

    new-array v0, v0, [Lorg/apache/james/mime4j/field/address/parser/s;

    iget-object v1, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    iget-object v2, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    goto :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/u;->d:[Lorg/apache/james/mime4j/field/address/parser/s;

    array-length v0, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    sget-object v0, Lorg/apache/james/mime4j/field/address/parser/p;->g:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/u;->e:I

    aget-object v0, v0, v1

    return-object v0
.end method
