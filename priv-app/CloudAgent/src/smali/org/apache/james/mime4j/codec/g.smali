.class Lorg/apache/james/mime4j/codec/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:Lorg/apache/james/mime4j/codec/f;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lorg/apache/james/mime4j/codec/f;)V
    .locals 1

    iput-object p1, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v0, v0, Lorg/apache/james/mime4j/codec/f;->b:I

    iput v0, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Byte;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/james/mime4j/codec/g;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    iput v0, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v1, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    invoke-static {v0, v1}, Lorg/apache/james/mime4j/codec/f;->a(Lorg/apache/james/mime4j/codec/f;I)I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    new-instance v0, Ljava/lang/Byte;

    iget-object v1, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v1, v1, Lorg/apache/james/mime4j/codec/f;->a:[B

    iget v2, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    aget-byte v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Byte;-><init>(B)V

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    iget-object v1, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v1, v1, Lorg/apache/james/mime4j/codec/f;->c:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/codec/g;->a()Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, -0x1

    iget v0, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    if-ne v0, v5, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v2, v2, Lorg/apache/james/mime4j/codec/f;->b:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/codec/f;->c()B

    iput v5, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v2, v2, Lorg/apache/james/mime4j/codec/f;->c:I

    if-eq v0, v2, :cond_3

    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v2, v2, Lorg/apache/james/mime4j/codec/f;->a:[B

    array-length v2, v2

    if-lt v0, v2, :cond_2

    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v2, v2, Lorg/apache/james/mime4j/codec/f;->a:[B

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v3, v3, Lorg/apache/james/mime4j/codec/f;->a:[B

    aget-byte v3, v3, v1

    aput-byte v3, v2, v0

    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v2, v2, Lorg/apache/james/mime4j/codec/f;->a:[B

    add-int/lit8 v3, v0, -0x1

    iget-object v4, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v4, v4, Lorg/apache/james/mime4j/codec/f;->a:[B

    aget-byte v4, v4, v0

    aput-byte v4, v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iput v5, p0, Lorg/apache/james/mime4j/codec/g;->c:I

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v3, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v3, v3, Lorg/apache/james/mime4j/codec/f;->c:I

    invoke-static {v2, v3}, Lorg/apache/james/mime4j/codec/f;->b(Lorg/apache/james/mime4j/codec/f;I)I

    move-result v2

    iput v2, v0, Lorg/apache/james/mime4j/codec/f;->c:I

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget-object v0, v0, Lorg/apache/james/mime4j/codec/f;->a:[B

    iget-object v2, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v2, v2, Lorg/apache/james/mime4j/codec/f;->c:I

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/james/mime4j/codec/g;->a:Lorg/apache/james/mime4j/codec/f;

    iget v1, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    invoke-static {v0, v1}, Lorg/apache/james/mime4j/codec/f;->b(Lorg/apache/james/mime4j/codec/f;I)I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/codec/g;->b:I

    goto :goto_0
.end method
