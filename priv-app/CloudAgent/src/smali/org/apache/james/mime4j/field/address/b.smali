.class Lorg/apache/james/mime4j/field/address/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:Lorg/apache/james/mime4j/field/address/parser/u;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lorg/apache/james/mime4j/field/address/parser/u;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/james/mime4j/field/address/b;->a:Lorg/apache/james/mime4j/field/address/parser/u;

    invoke-virtual {p1}, Lorg/apache/james/mime4j/field/address/parser/u;->c()I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/b;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/b;->b:I

    return-void
.end method


# virtual methods
.method public a()Lorg/apache/james/mime4j/field/address/parser/s;
    .locals 3

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/b;->a:Lorg/apache/james/mime4j/field/address/parser/u;

    iget v1, p0, Lorg/apache/james/mime4j/field/address/b;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/james/mime4j/field/address/b;->b:I

    invoke-virtual {v0, v1}, Lorg/apache/james/mime4j/field/address/parser/u;->a(I)Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/field/address/b;->b:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/b;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/b;->a()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
