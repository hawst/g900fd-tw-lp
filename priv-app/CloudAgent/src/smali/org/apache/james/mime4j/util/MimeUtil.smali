.class public final Lorg/apache/james/mime4j/util/MimeUtil;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lorg/apache/commons/logging/Log;

.field private static final b:Ljava/util/Random;

.field private static c:I

.field private static final d:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/james/mime4j/util/MimeUtil;

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/james/mime4j/util/MimeUtil;->a:Lorg/apache/commons/logging/Log;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lorg/apache/james/mime4j/util/MimeUtil;->b:Ljava/util/Random;

    const/4 v0, 0x0

    sput v0, Lorg/apache/james/mime4j/util/MimeUtil;->c:I

    new-instance v0, Lorg/apache/james/mime4j/util/d;

    invoke-direct {v0}, Lorg/apache/james/mime4j/util/d;-><init>()V

    sput-object v0, Lorg/apache/james/mime4j/util/MimeUtil;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
