.class Lorg/apache/james/mime4j/field/address/parser/r;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/Stack;

.field private b:Ljava/util/Stack;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->a:Ljava/util/Stack;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    iput v1, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    return-void
.end method


# virtual methods
.method a()Lorg/apache/james/mime4j/field/address/parser/s;
    .locals 2

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->a:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/s;

    return-object v0
.end method

.method a(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    return-void
.end method

.method a(Lorg/apache/james/mime4j/field/address/parser/s;Z)V
    .locals 2

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/r;->c()I

    move-result v1

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    move v0, v1

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/r;->b()Lorg/apache/james/mime4j/field/address/parser/s;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/apache/james/mime4j/field/address/parser/s;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    invoke-interface {p1, v0, v1}, Lorg/apache/james/mime4j/field/address/parser/s;->a(Lorg/apache/james/mime4j/field/address/parser/s;I)V

    move v0, v1

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lorg/apache/james/mime4j/field/address/parser/s;->b()V

    invoke-virtual {p0, p1}, Lorg/apache/james/mime4j/field/address/parser/r;->a(Lorg/apache/james/mime4j/field/address/parser/s;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->e:Z

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->e:Z

    goto :goto_1
.end method

.method b()Lorg/apache/james/mime4j/field/address/parser/s;
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/field/address/parser/s;

    return-object v0
.end method

.method b(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 2

    :goto_0
    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/james/mime4j/field/address/parser/r;->b()Lorg/apache/james/mime4j/field/address/parser/s;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    return-void
.end method

.method c()I
    .locals 2

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    iget v1, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    sub-int/2addr v0, v1

    return v0
.end method

.method c(Lorg/apache/james/mime4j/field/address/parser/s;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->b:Ljava/util/Stack;

    new-instance v1, Ljava/lang/Integer;

    iget v2, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->c:I

    iput v0, p0, Lorg/apache/james/mime4j/field/address/parser/r;->d:I

    invoke-interface {p1}, Lorg/apache/james/mime4j/field/address/parser/s;->a()V

    return-void
.end method
