.class public Lcom/dropbox/client2/SamsungDropboxAPI;
.super Lcom/dropbox/client2/DropboxAPI;


# direct methods
.method public constructor <init>(Lcom/dropbox/client2/session/Session;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/dropbox/client2/DropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/i;
    .locals 1

    invoke-static {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->b(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/i;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 6

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/i;
    .locals 3

    :try_start_0
    new-instance v1, Lcom/dropbox/client2/i;

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/client2/jsonextract/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0}, Lcom/dropbox/client2/i;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error parsing file metadata object ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/client2/jsonextract/j;->a(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error parsing file metadata object ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/client2/jsonextract/j;->a(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/r;
    .locals 6

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->c()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media_transcode/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->e()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v0, 0x10

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "locale"

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v0}, Lcom/dropbox/client2/session/Session;->f()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "container"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "hls,mpegts"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "screen_resolution"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p2, v4, v0

    const/4 v0, 0x6

    const-string v1, "connection_type"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p3, v4, v0

    const/16 v0, 0x8

    const-string v1, "model"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xa

    const-string v1, "sys_version"

    aput-object v1, v4, v0

    const/16 v0, 0xb

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0xc

    const-string v1, "platform"

    aput-object v1, v4, v0

    const/16 v0, 0xd

    const-string v1, "android"

    aput-object v1, v4, v0

    const/16 v0, 0xe

    const-string v1, "manufacturer"

    aput-object v1, v4, v0

    const/16 v0, 0xf

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    aput-object v1, v4, v0

    :try_start_0
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    new-instance v1, Lcom/dropbox/client2/r;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/dropbox/client2/r;-><init>(Ljava/util/Map;Lcom/dropbox/client2/p;)V
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxServerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    iget v1, v0, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v2, 0x19f

    if-ne v1, v2, :cond_0

    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;

    invoke-direct {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;-><init>()V

    throw v0

    :cond_0
    throw v0
.end method

.method public a(Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/y;
    .locals 6

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->c()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/samsung_thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->e()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    invoke-virtual {p2}, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->toAPISize()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p3}, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->f()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->b(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/k;

    move-result-object v0

    new-instance v1, Lcom/dropbox/client2/y;

    iget-object v2, v0, Lcom/dropbox/client2/k;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v0, v0, Lcom/dropbox/client2/k;->b:Lorg/apache/http/HttpResponse;

    invoke-direct {v1, v2, v0}, Lcom/dropbox/client2/y;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v1
.end method

.method public a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x1

    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "delta_cursor"

    aput-object v1, v4, v0

    aput-object p2, v4, v3

    const/4 v0, 0x2

    const-string v1, "hook_id"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "device_id"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p3, v4, v0

    const/4 v0, 0x6

    const-string v1, "type"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    const-string v1, "delta"

    aput-object v1, v4, v0

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks/add_trigger"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "ping"

    const-string v2, "ret"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "trigger_id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->e(Ljava/lang/String;)Lcom/dropbox/client2/q;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/dropbox/client2/q;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    .locals 6

    const/4 v3, 0x1

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "hook_id"

    aput-object v1, v4, v0

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    aput-object v0, v4, v3

    const/4 v0, 0x2

    const-string v1, "trigger_id"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p2, v4, v0

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks/remove_trigger"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string v1, "success"

    const-string v2, "ret"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)Lcom/dropbox/client2/e;
    .locals 6

    const/4 v3, 0x1

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "cursor"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    const/4 v0, 0x2

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->f()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->POST:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/music/delta"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/dropbox/client2/jsonextract/j;

    invoke-direct {v1, v0}, Lcom/dropbox/client2/jsonextract/j;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lcom/dropbox/client2/s;->c:Lcom/dropbox/client2/jsonextract/b;

    invoke-static {v1, v0}, Lcom/dropbox/client2/e;->a(Lcom/dropbox/client2/jsonextract/j;Lcom/dropbox/client2/jsonextract/b;)Lcom/dropbox/client2/e;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing /music/delta results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public d()Ljava/util/List;
    .locals 5

    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->c()V

    const-string v0, "/samsung_albums"

    const/4 v1, 0x0

    sget-object v2, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v3, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v3}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/dropbox/client2/jsonextract/j;

    invoke-direct {v1, v0}, Lcom/dropbox/client2/jsonextract/j;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v0

    const-string v1, "albums"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonextract/j;->c()Lcom/dropbox/client2/jsonextract/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/c;->a()I

    move-result v0

    new-array v3, v0, [Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/c;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/jsonextract/j;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->extractFromJson(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    move-result-object v0

    aput-object v0, v3, v1
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/dropbox/client2/exception/DropboxParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "server returned unxpected JSON: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonextract/JsonExtractionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/dropbox/client2/q;
    .locals 6

    invoke-virtual {p0}, Lcom/dropbox/client2/SamsungDropboxAPI;->c()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/music/cover/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->e()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    new-array v4, v0, [Ljava/lang/String;

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->b(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/k;

    move-result-object v0

    new-instance v1, Lcom/dropbox/client2/q;

    iget-object v2, v0, Lcom/dropbox/client2/k;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v0, v0, Lcom/dropbox/client2/k;->b:Lorg/apache/http/HttpResponse;

    invoke-direct {v1, v2, v0}, Lcom/dropbox/client2/q;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v1
.end method

.method public f(Ljava/lang/String;)Ljava/util/List;
    .locals 8

    const/4 v6, 0x0

    const/4 v3, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "hook_type"

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    :goto_0
    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    iget-object v1, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-interface {v1}, Lcom/dropbox/client2/session/Session;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/hooks"

    iget-object v5, p0, Lcom/dropbox/client2/SamsungDropboxAPI;->b:Lcom/dropbox/client2/session/Session;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->a(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/simple/JSONArray;

    invoke-virtual {v0}, Lorg/json/simple/JSONArray;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/util/Map;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/util/Map;

    const-string v1, "hook_id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "type"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;

    invoke-direct {v3, v1, v0, v6}, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/p;)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v7

    :cond_2
    move-object v4, v6

    goto :goto_0
.end method
