.class public Lcom/dropbox/client2/y;
.super Lcom/dropbox/client2/g;


# instance fields
.field private final a:Lcom/dropbox/client2/w;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/dropbox/client2/g;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    invoke-static {p2}, Lcom/dropbox/client2/y;->a(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/w;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/client2/y;->a:Lcom/dropbox/client2/w;

    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/w;
    .locals 4

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "x-dropbox-photo-metadata"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/json/simple/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    :try_start_0
    sget-object v2, Lcom/dropbox/client2/w;->g:Lcom/dropbox/client2/jsonextract/b;

    new-instance v3, Lcom/dropbox/client2/jsonextract/j;

    invoke-direct {v3, v0}, Lcom/dropbox/client2/jsonextract/j;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lcom/dropbox/client2/jsonextract/b;->b(Lcom/dropbox/client2/jsonextract/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/w;
    :try_end_0
    .catch Lcom/dropbox/client2/jsonextract/JsonExtractionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public b()Lcom/dropbox/client2/w;
    .locals 1

    iget-object v0, p0, Lcom/dropbox/client2/y;->a:Lcom/dropbox/client2/w;

    return-object v0
.end method
