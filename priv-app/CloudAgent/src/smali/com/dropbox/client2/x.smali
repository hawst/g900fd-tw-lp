.class final Lcom/dropbox/client2/x;
.super Lcom/dropbox/client2/jsonextract/b;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/w;
    .locals 10

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v6

    const-string v1, "date_taken"

    invoke-virtual {v6, v1}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v0

    :goto_0
    const-string v2, "width"

    invoke-virtual {v6, v2}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v2, v0

    :goto_1
    const-string v3, "height"

    invoke-virtual {v6, v3}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v3, v0

    :goto_2
    const-string v4, "orientation"

    invoke-virtual {v6, v4}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v4

    if-nez v4, :cond_3

    move-object v4, v0

    :goto_3
    const-string v5, "latitude"

    invoke-virtual {v6, v5}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v5

    if-nez v5, :cond_4

    move-object v5, v0

    :goto_4
    const-string v7, "longitude"

    invoke-virtual {v6, v7}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v6

    if-nez v6, :cond_5

    move-object v6, v0

    :goto_5
    new-instance v0, Lcom/dropbox/client2/w;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/client2/w;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;)V

    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/dropbox/client2/jsonextract/j;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/j;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Lcom/dropbox/client2/jsonextract/j;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_3

    :cond_4
    invoke-virtual {v5}, Lcom/dropbox/client2/jsonextract/j;->f()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    goto :goto_4

    :cond_5
    invoke-virtual {v6}, Lcom/dropbox/client2/jsonextract/j;->f()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    goto :goto_5
.end method

.method public synthetic b(Lcom/dropbox/client2/jsonextract/j;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/dropbox/client2/x;->a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/w;

    move-result-object v0

    return-object v0
.end method
