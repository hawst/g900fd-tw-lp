.class public final enum Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum ArrayStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum End:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum ObjectStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

.field public static final enum Primitive:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "ObjectStart"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "ObjectEnd"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "FieldStart"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "ArrayStart"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "ArrayEnd"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "Primitive"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->Primitive:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    new-instance v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const-string v1, "End"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->End:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ObjectEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->FieldStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayStart:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->ArrayEnd:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->Primitive:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->End:Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->$VALUES:[Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .locals 1

    const-class v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;
    .locals 1

    sget-object v0, Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->$VALUES:[Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    invoke-virtual {v0}, [Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/client2/jsonstream/JsonPullParser$Event;

    return-object v0
.end method
