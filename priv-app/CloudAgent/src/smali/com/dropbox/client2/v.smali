.class final Lcom/dropbox/client2/v;
.super Lcom/dropbox/client2/jsonextract/b;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/dropbox/client2/jsonextract/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/u;
    .locals 8

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v6

    const-string v1, "album"

    invoke-virtual {v6, v1}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v0

    :goto_0
    const-string v2, "artist"

    invoke-virtual {v6, v2}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v2, v0

    :goto_1
    const-string v3, "track"

    invoke-virtual {v6, v3}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v3, v0

    :goto_2
    const-string v4, "title"

    invoke-virtual {v6, v4}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v4

    if-nez v4, :cond_3

    move-object v4, v0

    :goto_3
    const-string v5, "genre"

    invoke-virtual {v6, v5}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v5

    if-nez v5, :cond_4

    move-object v5, v0

    :goto_4
    const-string v7, "duration"

    invoke-virtual {v6, v7}, Lcom/dropbox/client2/jsonextract/f;->c(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v6

    if-nez v6, :cond_5

    move-object v6, v0

    :goto_5
    new-instance v0, Lcom/dropbox/client2/u;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/client2/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/j;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_4
    invoke-virtual {v5}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_5
    invoke-virtual {v6}, Lcom/dropbox/client2/jsonextract/j;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_5
.end method

.method public synthetic b(Lcom/dropbox/client2/jsonextract/j;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/dropbox/client2/v;->a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/u;

    move-result-object v0

    return-object v0
.end method
