.class public final Lcom/dropbox/client2/SamsungDropboxAPI$Album;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x132df74L


# instance fields
.field public final contents:Ljava/util/List;

.field public final metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

.field public final rev:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->metadata:Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->rev:Ljava/lang/String;

    iput-object p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Album;->contents:Ljava/util/List;

    return-void
.end method
