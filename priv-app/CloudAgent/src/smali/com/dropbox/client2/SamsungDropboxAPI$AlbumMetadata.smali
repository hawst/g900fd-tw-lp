.class public final Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x132df1aL


# instance fields
.field public final count:J

.field public final cover:Lcom/dropbox/client2/i;

.field public final id:Ljava/lang/String;

.field public final modified:Ljava/util/Date;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Lcom/dropbox/client2/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->id:Ljava/lang/String;

    iput-wide p3, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->count:J

    iput-object p5, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->modified:Ljava/util/Date;

    iput-object p6, p0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->cover:Lcom/dropbox/client2/i;

    return-void
.end method

.method public static extractFromJson(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;
    .locals 8

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v2

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v3

    const-string v1, "count"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->d()J

    move-result-wide v4

    const-string v1, "modified"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v0, "invalid date format"

    invoke-virtual {v1, v0}, Lcom/dropbox/client2/jsonextract/j;->a(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/JsonExtractionException;

    move-result-object v0

    throw v0

    :cond_0
    const-string v1, "cover"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Lcom/dropbox/client2/jsonextract/j;)Lcom/dropbox/client2/i;

    move-result-object v7

    new-instance v1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    invoke-direct/range {v1 .. v7}, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Lcom/dropbox/client2/i;)V

    return-object v1
.end method
