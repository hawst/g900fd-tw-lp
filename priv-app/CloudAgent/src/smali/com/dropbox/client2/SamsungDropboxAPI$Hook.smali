.class public Lcom/dropbox/client2/SamsungDropboxAPI$Hook;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1bca3554df3941f0L


# instance fields
.field public final TYPE_ANDROID:Ljava/lang/String;

.field public final TYPE_WEB:Ljava/lang/String;

.field public final hookId:Ljava/lang/String;

.field public final type:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "android"

    iput-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->TYPE_ANDROID:Ljava/lang/String;

    const-string v0, "web"

    iput-object v0, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->TYPE_WEB:Ljava/lang/String;

    iput-object p1, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    iput-object p2, p0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->type:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/p;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
