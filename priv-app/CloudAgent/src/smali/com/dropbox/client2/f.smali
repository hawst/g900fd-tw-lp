.class public final Lcom/dropbox/client2/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Lcom/dropbox/client2/i;


# direct methods
.method private constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/client2/f;->a:Ljava/lang/String;

    iput-wide v4, p0, Lcom/dropbox/client2/f;->b:J

    iput-object v0, p0, Lcom/dropbox/client2/f;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/client2/f;->d:Lcom/dropbox/client2/i;

    invoke-static {p1}, Lcom/dropbox/client2/f;->a(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/i;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/client2/f;->d:Lcom/dropbox/client2/i;

    iget-object v0, p0, Lcom/dropbox/client2/f;->d:Lcom/dropbox/client2/i;

    if-nez v0, :cond_0

    new-instance v0, Lcom/dropbox/client2/exception/DropboxParseException;

    const-string v1, "Error parsing metadata."

    invoke-direct {v0, v1}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/dropbox/client2/f;->d:Lcom/dropbox/client2/i;

    invoke-static {p1, v0}, Lcom/dropbox/client2/f;->a(Lorg/apache/http/HttpResponse;Lcom/dropbox/client2/i;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dropbox/client2/f;->b:J

    iget-wide v0, p0, Lcom/dropbox/client2/f;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    new-instance v0, Lcom/dropbox/client2/exception/DropboxParseException;

    const-string v1, "Error determining file size."

    invoke-direct {v0, v1}, Lcom/dropbox/client2/exception/DropboxParseException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_2

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/client2/f;->a:Ljava/lang/String;

    :cond_2
    array-length v1, v0

    if-le v1, v2, :cond_3

    aget-object v0, v0, v2

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-le v1, v2, :cond_3

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/client2/f;->c:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/HttpResponse;Lcom/dropbox/client2/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/dropbox/client2/f;-><init>(Lorg/apache/http/HttpResponse;)V

    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Lcom/dropbox/client2/i;)J
    .locals 4

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    if-eqz p1, :cond_1

    iget-wide v0, p1, Lcom/dropbox/client2/i;->a:J

    goto :goto_0

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Lcom/dropbox/client2/i;
    .locals 2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "X-Dropbox-Metadata"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/json/simple/c;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    check-cast v0, Ljava/util/Map;

    new-instance v1, Lcom/dropbox/client2/i;

    invoke-direct {v1, v0}, Lcom/dropbox/client2/i;-><init>(Ljava/util/Map;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lcom/dropbox/client2/f;->b:J

    return-wide v0
.end method
