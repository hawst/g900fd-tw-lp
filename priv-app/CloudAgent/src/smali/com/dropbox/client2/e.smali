.class public final Lcom/dropbox/client2/e;
.super Ljava/lang/Object;


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;

.field public final d:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/dropbox/client2/e;->a:Z

    iput-object p2, p0, Lcom/dropbox/client2/e;->c:Ljava/util/List;

    iput-object p3, p0, Lcom/dropbox/client2/e;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/dropbox/client2/e;->d:Z

    return-void
.end method

.method public static a(Lcom/dropbox/client2/jsonextract/j;Lcom/dropbox/client2/jsonextract/b;)Lcom/dropbox/client2/e;
    .locals 5

    invoke-virtual {p0}, Lcom/dropbox/client2/jsonextract/j;->b()Lcom/dropbox/client2/jsonextract/f;

    move-result-object v0

    const-string v1, "reset"

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/client2/jsonextract/j;->i()Z

    move-result v1

    const-string v2, "cursor"

    invoke-virtual {v0, v2}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/client2/jsonextract/j;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "has_more"

    invoke-virtual {v0, v3}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/client2/jsonextract/j;->i()Z

    move-result v3

    const-string v4, "entries"

    invoke-virtual {v0, v4}, Lcom/dropbox/client2/jsonextract/f;->b(Ljava/lang/String;)Lcom/dropbox/client2/jsonextract/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/client2/jsonextract/j;->c()Lcom/dropbox/client2/jsonextract/c;

    move-result-object v0

    new-instance v4, Lcom/dropbox/client2/d;

    invoke-direct {v4, p1}, Lcom/dropbox/client2/d;-><init>(Lcom/dropbox/client2/jsonextract/b;)V

    invoke-virtual {v0, v4}, Lcom/dropbox/client2/jsonextract/c;->a(Lcom/dropbox/client2/jsonextract/b;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v4, Lcom/dropbox/client2/e;

    invoke-direct {v4, v1, v0, v2, v3}, Lcom/dropbox/client2/e;-><init>(ZLjava/util/List;Ljava/lang/String;Z)V

    return-object v4
.end method
