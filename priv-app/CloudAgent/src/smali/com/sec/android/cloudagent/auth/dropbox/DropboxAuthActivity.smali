.class public Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;
.super Landroid/app/Activity;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->finish()V

    return-void
.end method

.method private c()V
    .locals 4

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a(I)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/y;->e()Z
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method b()V
    .locals 1

    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/y;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "requestSignIn"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->b:Z

    const-string v0, "TAG"

    const-string v1, "CREATE ACTIVITY"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->b()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "DropAuthActivity : onResume"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->b:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Authentication successful"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/y;->f()V

    invoke-static {}, Lcom/sec/android/cloudagent/y;->d()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, v5

    aget-object v0, v0, v4

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/auth/d;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/cloudagent/auth/dropbox/a;->h()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "Clearing lastSyncID"

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/sync/SyncManager;->f()V

    :cond_1
    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/cloudagent/auth/dropbox/a;->c(Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v4, v1, v4}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->a(I)V

    :cond_2
    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->b:Z

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t authenticate with Dropbox : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->c()V

    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v0, "authToken is null !!! "

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->c()V

    goto :goto_1

    :cond_4
    const-string v0, "Authentication failed !!!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;->c()V

    goto :goto_1
.end method
