.class public Lcom/sec/android/cloudagent/e/i;
.super Lcom/sec/android/cloudagent/e/j;


# instance fields
.field private a:Lcom/sec/android/cloudagent/adt/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/cloudagent/e/j;-><init>(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/i;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->c(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/c;)V

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/j;->a()V

    return-void
.end method

.method public d()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-static {v1}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v2, v3}, Lcom/sec/android/cloudagent/ae;->m(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v1

    const-string v3, "cloud_revision"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/j;->i()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/e/i;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-static {v1}, Lcom/sec/android/cloudagent/y;->b(Lcom/sec/android/cloudagent/adt/c;)Z
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method
