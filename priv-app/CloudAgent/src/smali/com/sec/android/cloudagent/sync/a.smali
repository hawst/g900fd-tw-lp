.class public Lcom/sec/android/cloudagent/sync/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/b/g;Z)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/a;->c:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->a:Ljava/util/List;

    invoke-virtual {p1}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/f;

    if-nez p2, :cond_1

    instance-of v1, v0, Lcom/samsung/scloud/b/e;

    if-nez v1, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/samsung/scloud/b/d;

    invoke-virtual {v1}, Lcom/samsung/scloud/b/d;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/sec/android/cloudagent/v;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/a;->a:Ljava/util/List;

    new-instance v3, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-direct {v3, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;-><init>(Lcom/samsung/scloud/b/f;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/scloud/b/g;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/scloud/b/g;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/a;->c:Z

    return-void
.end method


# virtual methods
.method a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->a:Ljava/util/List;

    return-object v0
.end method

.method b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/sync/a;->c:Z

    return v0
.end method
