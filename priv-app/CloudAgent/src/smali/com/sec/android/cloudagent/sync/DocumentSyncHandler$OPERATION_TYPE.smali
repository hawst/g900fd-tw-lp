.class public final enum Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field public static final enum UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "UPDATED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "RENAMED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "DELETED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "DELTED_AND_CREATED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    new-instance v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "OTHERS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->$VALUES:[Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 1

    const-class v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->$VALUES:[Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    return-object v0
.end method
