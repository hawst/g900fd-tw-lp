.class Lcom/sec/android/cloudagent/auth/gdrive/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    const-string v2, "authAccount"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    iget-object v2, v2, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;I)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    iget-object v2, v2, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v2, v4}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-static {v0, v4}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    iget-object v2, v2, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v2, v4}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-static {v0, v4}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;I)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Ljava/io/IOException;)V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/b;->a:Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-static {v0, v4}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;I)V

    goto :goto_0
.end method
