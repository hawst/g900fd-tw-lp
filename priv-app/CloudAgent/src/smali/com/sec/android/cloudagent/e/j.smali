.class public abstract Lcom/sec/android/cloudagent/e/j;
.super Lcom/sec/android/cloudagent/e/e;

# interfaces
.implements Lcom/sec/android/cloudagent/e/m;


# instance fields
.field private a:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/e/e;-><init>()V

    iput-object p1, p0, Lcom/sec/android/cloudagent/e/j;->a:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v0, "localPath should not be null !"

    invoke-static {p2, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/j;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/cloudagent/e/j;->d:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public b()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onFailure : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public abstract d()Z
.end method

.method public e()Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->a:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/j;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/sec/android/cloudagent/e/j;

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->c:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->e(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/b;->c()I
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/cloudagent/ae;->a(Ljava/lang/String;I)V

    const/4 v2, 0x5

    if-le v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->i(Ljava/lang/String;)V

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
