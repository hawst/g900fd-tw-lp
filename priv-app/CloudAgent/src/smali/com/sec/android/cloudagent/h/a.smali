.class public Lcom/sec/android/cloudagent/h/a;
.super Ljava/lang/Object;


# direct methods
.method private static a(Lcom/sec/android/cloudagent/c/b;)Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/b;->b()Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 2

    invoke-static {p1, p2, p3, p4}, Lcom/sec/android/cloudagent/y;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/cloudagent/c/b;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, p0, Lcom/sec/android/cloudagent/e/u;

    if-eqz v1, :cond_0

    check-cast p0, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/u;->a(Lcom/sec/android/cloudagent/c/b;)V

    :goto_0
    invoke-static {v0}, Lcom/sec/android/cloudagent/h/a;->a(Lcom/sec/android/cloudagent/c/b;)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    instance-of v1, p0, Lcom/sec/android/cloudagent/e/g;

    if-eqz v1, :cond_1

    check-cast p0, Lcom/sec/android/cloudagent/e/g;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/g;->a(Lcom/sec/android/cloudagent/c/b;)V

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/sec/android/cloudagent/e/l;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/l;->a(Lcom/sec/android/cloudagent/c/b;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteUploadDB("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object v0, p0

    check-cast v0, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    check-cast p0, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->d(Ljava/lang/String;)Z

    return-void
.end method

.method public static b(Lcom/sec/android/cloudagent/e/m;)V
    .locals 0

    return-void
.end method
