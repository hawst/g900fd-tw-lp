.class public Lcom/sec/android/cloudagent/e/t;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/e/m;


# instance fields
.field private a:Lcom/sec/android/cloudagent/adt/c;

.field private b:Ljava/lang/String;

.field private c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

.field private d:Lcom/sec/android/cloudagent/adt/CloudObject;


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/t;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    const-string v0, "CloudID should not be null !"

    invoke-static {p1, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/t;->a:Lcom/sec/android/cloudagent/adt/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/e/t;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    iput-object p2, p0, Lcom/sec/android/cloudagent/e/t;->c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-static {p0}, Lcom/sec/android/cloudagent/g/a;->a(Lcom/sec/android/cloudagent/e/t;)V

    invoke-static {p0}, Lcom/sec/android/cloudagent/g/a;->a(Lcom/sec/android/cloudagent/e/m;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/cloudagent/g/a;->f(Lcom/sec/android/cloudagent/e/m;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    return-void
.end method

.method public b()V
    .locals 0

    invoke-static {p0}, Lcom/sec/android/cloudagent/g/a;->c(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public c()Lcom/sec/android/cloudagent/adt/c;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->a:Lcom/sec/android/cloudagent/adt/c;

    return-object v0
.end method

.method public d()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/t;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/sec/android/cloudagent/e/t;

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/t;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/t;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/t;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/e/t;->a:Lcom/sec/android/cloudagent/adt/c;

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/t;->c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    iget-object v2, p0, Lcom/sec/android/cloudagent/e/t;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/g/a;->a(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/t;->d:Lcom/sec/android/cloudagent/adt/CloudObject;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/t;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
