.class public Lcom/sec/android/cloudagent/adt/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/adt/b;->a:Landroid/content/ContentValues;

    iput-object p1, p0, Lcom/sec/android/cloudagent/adt/b;->a:Landroid/content/ContentValues;

    return-void
.end method

.method public static a(I)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_4

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/b;->a:Landroid/content/ContentValues;

    const-string v1, "local_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/b;->a:Landroid/content/ContentValues;

    const-string v1, "operation_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/adt/b;->a(I)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/b;->a:Landroid/content/ContentValues;

    const-string v1, "number_of_tries"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
