.class public Lcom/sec/android/cloudagent/f/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/f/b;


# static fields
.field private static n:Lcom/sec/android/cloudagent/f/d;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/f/d;->n:Lcom/sec/android/cloudagent/f/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/f/d;->b()V

    invoke-static {}, Lcom/sec/android/cloudagent/f/c;->a()Lcom/sec/android/cloudagent/f/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/f/c;->a(Lcom/sec/android/cloudagent/f/b;)V

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/f/d;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/f/d;->n:Lcom/sec/android/cloudagent/f/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/f/d;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/f/d;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/f/d;->n:Lcom/sec/android/cloudagent/f/d;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/f/d;->n:Lcom/sec/android/cloudagent/f/d;

    return-object v0
.end method

.method private a(IZ)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/cloudagent/s;->a(IZ)V

    return-void
.end method

.method private c(Lcom/sec/android/cloudagent/f/a;)V
    .locals 4

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->a:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->a:Z

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->b:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->b:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    const/16 v0, 0x20

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    const/16 v0, 0x40

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    const/16 v0, 0x80

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Z)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "wifiOnlyType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "wifiOnlyValue"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "photo"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x100

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto/16 :goto_0

    :cond_1
    const-string v3, "video"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x200

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto/16 :goto_0

    :cond_2
    const-string v3, "music"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x400

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto/16 :goto_0

    :cond_3
    const-string v3, "doc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x800

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(IZ)V

    goto/16 :goto_0

    :cond_4
    const-string v2, "all"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "photo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    const-string v1, "video"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    const-string v1, "music"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    const-string v1, "doc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/f/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x2710 -> :sswitch_2
        0x2af8 -> :sswitch_3
        0x2ee0 -> :sswitch_4
        0x32c8 -> :sswitch_5
        0x32ca -> :sswitch_6
        0x36b0 -> :sswitch_7
        0x3a98 -> :sswitch_8
    .end sparse-switch
.end method

.method public static d()V
    .locals 3

    const-string v0, "Sending cloud respond "

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.settings.ACTION_RESPONSE_CLOUD_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "cloudSettings"

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/sec/android/cloudagent/f/f;->a(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "com.android.settings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0}, Lcom/sec/android/cloudagent/c;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public static e()V
    .locals 1

    new-instance v0, Lcom/sec/android/cloudagent/f/e;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/f/e;-><init>()V

    invoke-static {v0}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/aj;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    sget-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/f/d;->c()V

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/f/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/f/d;->c(Lcom/sec/android/cloudagent/f/a;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/f/d;->c()V

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DIR:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v2, :cond_3

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v2, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    if-eqz v0, :cond_4

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v2, :cond_6

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    const-string v1, "photo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "video"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    goto :goto_0

    :cond_2
    const-string v1, "music"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    goto :goto_0

    :cond_3
    const-string v1, "doc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    goto :goto_0

    :cond_4
    const-string v1, "all"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    if-nez v1, :cond_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/sec/android/cloudagent/f/f;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "autoUpload"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->a:Z

    const-string v1, "useMobileDataForAutoUpload"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->b:Z

    const-string v1, "photoSync"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    const-string v1, "videoSync"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    const-string v1, "musicSync"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    const-string v1, "docSync"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    const-string v1, "numberOfDisplayingPhotos"

    sget v2, Lcom/sec/android/cloudagent/e;->k:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/cloudagent/f/d;->g:I

    const-string v1, "useMobileDataForDocSync"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    const-string v1, "photo"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    const-string v1, "video"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    const-string v1, "music"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    const-string v1, "doc"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    const-string v1, "enableSyncRoaming"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    :cond_0
    return-void
.end method

.method public b(Lcom/sec/android/cloudagent/f/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/f/d;->c(Lcom/sec/android/cloudagent/f/a;)V

    return-void
.end method

.method public b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z
    .locals 2

    const/4 v0, 0x1

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DIR:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->OTHERS:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_5

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 3

    invoke-static {}, Lcom/sec/android/cloudagent/f/f;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "autoUpload"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->a:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "useMobileDataForAutoUpload"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->b:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "photoSync"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->c:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "videoSync"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->d:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "musicSync"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->e:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "docSync"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->f:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "numberOfDisplayingPhotos"

    iget v2, p0, Lcom/sec/android/cloudagent/f/d;->g:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "useMobileDataForDocSync"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "photo"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->j:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "video"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->k:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "music"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->l:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "doc"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->m:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "enableSyncRoaming"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->a:Z

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->b:Z

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/sec/android/cloudagent/f/d;->g:I

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->h:Z

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/f/d;->i:Z

    return v0
.end method
