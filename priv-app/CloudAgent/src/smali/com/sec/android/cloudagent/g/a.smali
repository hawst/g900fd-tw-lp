.class public Lcom/sec/android/cloudagent/g/a;
.super Ljava/lang/Object;


# direct methods
.method public static a()I
    .locals 5

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->o()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    new-instance v3, Lcom/sec/android/cloudagent/e/t;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/sec/android/cloudagent/e/t;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-static {v3}, Lcom/sec/android/cloudagent/g/a;->d(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object p1

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/android/cloudagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :cond_3
    sget v1, Lcom/sec/android/cloudagent/e;->m:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;II)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doMakeThumb SyncOption has been set off and delete file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_4
    const-string v0, "doMakeThumb but file was not exist"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_5
    return-object v1
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    new-instance v2, Lcom/sec/android/cloudagent/e/t;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/cloudagent/e/t;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-static {v2}, Lcom/sec/android/cloudagent/g/a;->e(Lcom/sec/android/cloudagent/e/m;)V

    :goto_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t get the Thumbnail from server : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;)V
    .locals 4

    move-object v0, p0

    check-cast v0, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/t;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/t;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/t;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/cloudagent/g/a;->c(Lcom/sec/android/cloudagent/e/m;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/t;)V
    .locals 3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/t;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->d(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/t;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/e;->a(Ljava/util/List;)V

    return-void
.end method

.method public static a(I)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v2, Lcom/sec/android/cloudagent/e;->n:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    if-ne p0, v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    check-cast p0, Lcom/sec/android/cloudagent/e/t;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/t;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I

    return-void
.end method

.method public static b(Ljava/util/List;)V
    .locals 5

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/e/s;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/s;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/cloudagent/ae;->g(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v4}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/s;->a()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static c(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    check-cast p0, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/t;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_0
    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method public static d(Lcom/sec/android/cloudagent/e/m;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/e;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public static e(Lcom/sec/android/cloudagent/e/m;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/h;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public static f(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    check-cast p0, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/t;->f()Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->h()Landroid/content/ContentValues;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/t;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)V

    return-void
.end method
