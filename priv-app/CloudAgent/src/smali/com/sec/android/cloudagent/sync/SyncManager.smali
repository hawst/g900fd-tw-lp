.class public Lcom/sec/android/cloudagent/sync/SyncManager;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/sync/d;


# static fields
.field private static volatile c:Lcom/sec/android/cloudagent/sync/SyncManager;


# instance fields
.field protected a:Z

.field b:J

.field private d:Lcom/sec/android/cloudagent/d/b;

.field private e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

.field private f:Lcom/sec/android/cloudagent/sync/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/sync/SyncManager;->c:Lcom/sec/android/cloudagent/sync/SyncManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/sec/android/cloudagent/d/d;->a()Lcom/sec/android/cloudagent/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->d:Lcom/sec/android/cloudagent/d/b;

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->b:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/sync/SyncManager;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager;->c:Lcom/sec/android/cloudagent/sync/SyncManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/sync/SyncManager;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/sync/SyncManager;->c:Lcom/sec/android/cloudagent/sync/SyncManager;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager;->c:Lcom/sec/android/cloudagent/sync/SyncManager;

    return-object v0
.end method

.method private i()V
    .locals 3

    const-string v0, "PROFILE"

    const-string v1, "Sync START "

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->b:J

    new-instance v0, Lcom/sec/android/cloudagent/sync/e;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/sync/SyncManager;->g()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/cloudagent/sync/e;-><init>(Lcom/sec/android/cloudagent/sync/d;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/e;->start()V

    return-void
.end method

.method private j()V
    .locals 1

    const-string v0, "lastSyncedID"

    invoke-static {v0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;)V

    const-string v0, "lastMusicSyncedID"

    invoke-static {v0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/sec/android/cloudagent/e/r;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestSync : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->d:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v0, p1}, Lcom/sec/android/cloudagent/d/b;->a(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sync is not processible by Policy!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->RUNNING:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/sync/SyncManager$State;)V

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->c()V

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->i()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->f()V

    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/r;->i()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "skip sync! state is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/sync/SyncManager$State;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncManager :: onFailure() : errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/sync/SyncManager$State;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    new-instance v0, Lcom/sec/android/cloudagent/e/r;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/e/r;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/e/r;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncManager :: onComplete() : SyncID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MusicSyncID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const-string v0, "PROFILE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sync onComplete : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->b:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/sync/SyncManager;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/sec/android/cloudagent/sync/SyncManager;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/sync/SyncManager$State;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->g()V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    new-instance v0, Lcom/sec/android/cloudagent/e/r;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/e/r;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/e/r;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "requestDeinit"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->f:Lcom/sec/android/cloudagent/sync/e;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/e;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    const-string v0, "lastSyncedID"

    const-string v1, "lastSyncedID"

    invoke-static {v0, v1, p1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 2

    const-string v0, "lastMusicSyncedID"

    const-string v1, "lastMusicSyncedID"

    invoke-static {v0, v1, p1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public d()Z
    .locals 1

    const-string v0, "Sync is idle"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->a:Z

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/SyncManager;->i()V

    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/SyncManager;->e:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->RUNNING:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/SyncManager;->j()V

    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 3

    const-string v0, "lastSyncedID"

    const-string v1, "lastSyncedID"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadLastSynedTimeOnPersistent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    return-object v0
.end method

.method public h()I
    .locals 3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/sync/SyncManager;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " documents are recovered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
