.class public Lcom/sec/android/cloudagent/detector/b;
.super Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Time capture before DB operation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->i()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Time capture after DB operation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->c()I

    move-result v0

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/detector/SmartDetector;->d()I

    move-result v1

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/detector/SmartDetector;->e()I

    move-result v2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/ae;->j()V

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->e()V

    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const-string v0, "smartDetectorThread Running!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/detector/b;->a()V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Run again- state is set!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->h()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
