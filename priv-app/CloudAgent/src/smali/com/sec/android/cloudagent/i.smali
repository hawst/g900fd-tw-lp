.class public final Lcom/sec/android/cloudagent/i;
.super Ljava/lang/Object;


# direct methods
.method public static a()Ljava/lang/String;
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS cloudfiles (_id INTEGER PRIMARY KEY AUTOINCREMENT,cloud_server_id TEXT UNIQUE NOT NULL,cloud_is_cached INTEGER,cloud_is_available_offline INTEGER,cloud_is_dir INTEGER,cloud_server_path TEXT,cloud_cached_path TEXT,cloud_thumb_path TEXT,cloud_coverart_path TEXT,cloud_is_available_thumb INTEGER,cloud_last_viewed INTEGER,cloud_revision TEXT,cloud_etag TEXT,cloud_cache_date_modified INTEGER,_data TEXT NOT NULL,_size INTEGER,date_added INTEGER,date_modified INTEGER,mime_type TEXT,media_type INTEGER,title TEXT,_display_name TEXT,orientation INTEGER,latitude DOUBLE,longitude DOUBLE,datetaken INTEGER,width INTEGER,height INTEGER,bucket_id TEXT,bucket_display_name TEXT,album TEXT,artist TEXT,composer TEXT,track INTEGER,year INTEGER,duration INTEGER,genre_name TEXT,resumePos INTEGER,isPlayed INTEGER,resolution TEXT);"

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT INTO cloudfiles (_id,cloud_server_id,cloud_is_cached,cloud_is_available_offline,cloud_is_dir,cloud_server_path,cloud_cached_path,cloud_thumb_path,cloud_revision,cloud_etag,cloud_coverart_path,cloud_is_available_thumb,cloud_last_viewed,_data,_size,date_added,date_modified,mime_type,media_type,title,_display_name,orientation,latitude,longitude,datetaken,width,height,bucket_id,bucket_display_name,album,artist,composer,track,year,duration,genre_name,resumePos,isPlayed,resolution)select _id,cloud_server_id,cloud_is_cached,cloud_is_available_offline,cloud_is_dir,cloud_server_path,cloud_cached_path,cloud_thumb_path,cloud_revision,cloud_etag,cloud_albumart_path ,cloud_is_available_thumb,cloud_last_viewed,_data,_size,date_added,date_modified,mime_type,media_type,title,_display_name,orientation,latitude,longitude,datetaken,width,height,bucket_id,bucket_display_name,album,artist,composer,track,year,duration,genre_name,resumePos,isPlayed,resolution FROM cloudfiles_backup;"

    return-object v0
.end method

.method public static c()Landroid/net/Uri;
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected static d()Landroid/net/Uri;
    .locals 1

    const-string v0, "content://cloud/data/cloudfiles_no_server_op"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
