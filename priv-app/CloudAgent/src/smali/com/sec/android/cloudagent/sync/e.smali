.class public Lcom/sec/android/cloudagent/sync/e;
.super Ljava/lang/Thread;


# instance fields
.field private a:Lcom/sec/android/cloudagent/sync/d;

.field private b:Lcom/sec/android/cloudagent/d/b;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/sync/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->a:Lcom/sec/android/cloudagent/sync/d;

    invoke-static {}, Lcom/sec/android/cloudagent/d/d;->a()Lcom/sec/android/cloudagent/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/sync/e;->b:Lcom/sec/android/cloudagent/d/b;

    iput-boolean v2, p0, Lcom/sec/android/cloudagent/sync/e;->e:Z

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->j:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/cloudagent/sync/e;->a:Lcom/sec/android/cloudagent/sync/d;

    iput-object p2, p0, Lcom/sec/android/cloudagent/sync/e;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/cloudagent/sync/e;->d:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V
    .locals 3

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->e(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->d(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/sync/e;->i:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    move v0, v1

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v2, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    invoke-direct {p0, v2, p2}, Lcom/sec/android/cloudagent/sync/e;->b(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    :goto_0
    return v3

    :cond_2
    if-eqz v2, :cond_5

    invoke-virtual {p0, v2}, Lcom/sec/android/cloudagent/sync/e;->a(Ljava/util/List;)Z

    move-result v2

    :goto_1
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v6

    if-nez v6, :cond_1

    if-eqz v2, :cond_3

    iget-object v6, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    if-eqz v6, :cond_3

    sget-boolean v6, Lcom/sec/android/cloudagent/s;->a:Z

    if-nez v6, :cond_3

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->b()Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez p2, :cond_6

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/cloudagent/sync/SyncManager;->b(Ljava/lang/String;)V

    :cond_3
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NoOfIteration: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  No of pages:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  time taken:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    iget-boolean v4, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    if-eqz v4, :cond_4

    sget-boolean v4, Lcom/sec/android/cloudagent/s;->a:Z

    if-eqz v4, :cond_0

    :cond_4
    move v3, v2

    goto :goto_0

    :cond_5
    const-string v2, "doSync : CloudObject list is null !"

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    move v2, v3

    goto :goto_1

    :cond_6
    if-ne p2, v1, :cond_3

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/cloudagent/sync/SyncManager;->c(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private b(Ljava/lang/String;I)Ljava/util/List;
    .locals 10

    const/4 v3, 0x0

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    move v2, v3

    move-object v4, v1

    :cond_0
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    if-nez p2, :cond_4

    iget-object v4, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/cloudagent/y;->a(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/a;

    move-result-object v4

    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/sync/a;->c()Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/sync/a;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/sync/a;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SyncThread :: deltaSync() - total entries = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "time taken on for getdelta call:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SyncThread :: deltaSync() - cursor = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SyncThread :: deltaSync() - Retrunedcursor = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SyncThread :: deltaSync() - hasMore = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    iget-boolean v5, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    if-eqz v5, :cond_3

    const/4 v5, 0x2

    if-ge v2, v5, :cond_3

    sget-boolean v5, Lcom/sec/android/cloudagent/s;->a:Z

    if-eqz v5, :cond_0

    :cond_3
    :goto_1
    return-object v0

    :cond_4
    const/4 v5, 0x1

    if-ne p2, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/cloudagent/sync/e;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/cloudagent/y;->b(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iput-boolean v3, p0, Lcom/sec/android/cloudagent/sync/e;->h:Z

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1
.end method

.method private declared-synchronized d()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncThread : isStopped() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/sync/e;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/sync/e;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()J
    .locals 2

    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method private f()Lcom/sec/android/cloudagent/adt/c;
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->b(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/y;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v0, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->d(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;)I

    :cond_0
    if-nez v0, :cond_1

    const-string v1, "Can\'t create ROOT folder.."

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method private g()V
    .locals 2

    const-string v0, "deleteSyncOffFiles() ++"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    :cond_2
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-static {}, Lcom/sec/android/cloudagent/c;->b()V

    :cond_3
    const-string v0, "deleteSyncOffFiles() --"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/cloudagent/sync/e;->e:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncThread : stopSyncThread() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/cloudagent/sync/e;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/sec/android/cloudagent/sync/f;)V
    .locals 6

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/cloudagent/e;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/cloudagent/e;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->a:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->a(Ljava/util/List;)V

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->b:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->e(Ljava/util/List;)I

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->e:Ljava/util/List;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/util/List;Z)V

    :cond_2
    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->c:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->d(Ljava/util/List;)V

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/util/List;)V

    :cond_3
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "SyncThread : Need to clean up again!!!!!!!!!!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->i()V

    sget-boolean v0, Lcom/sec/android/cloudagent/s;->a:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->b()V

    :cond_4
    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->f()V

    :cond_5
    return-void
.end method

.method public a(Ljava/util/List;)Z
    .locals 3

    const-string v0, "SyncThread :: doSync()"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SyncThread : doSync() - CloudObject list is empty"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/e;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/sync/e;->b(Ljava/util/List;)Lcom/sec/android/cloudagent/sync/f;

    move-result-object v0

    :goto_1
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Lcom/sec/android/cloudagent/sync/f;)V

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/sync/e;->c(Ljava/util/List;)Lcom/sec/android/cloudagent/sync/f;

    move-result-object v0

    goto :goto_1
.end method

.method public b(Ljava/util/List;)Lcom/sec/android/cloudagent/sync/f;
    .locals 9

    new-instance v1, Lcom/sec/android/cloudagent/sync/f;

    invoke-direct {v1}, Lcom/sec/android/cloudagent/sync/f;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->k()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v3, v1, Lcom/sec/android/cloudagent/sync/f;->f:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/cloudagent/adt/CloudObject;->a(Z)V

    :goto_1
    iget-object v3, v1, Lcom/sec/android/cloudagent/sync/f;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v5, v1, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    new-instance v6, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->f()Z

    move-result v8

    invoke-direct {v6, v4, v3, v7, v8}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public b()Z
    .locals 10

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->f()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/y;->a()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-interface {v4, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->c()Ljava/util/List;

    move-result-object v1

    const/4 v9, 0x0

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    :cond_2
    move v0, v3

    :goto_1
    return v0

    :cond_3
    :try_start_1
    invoke-interface {v4, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-object v0, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->c()Ljava/util/List;

    move-result-object v2

    const/4 v9, 0x0

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/cloudagent/adt/c;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, "/"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/adt/CloudObject;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    invoke-interface {v4, v5}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v7}, Lcom/sec/android/cloudagent/sync/e;->a(Ljava/util/List;)Z
    :try_end_1
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1
.end method

.method public c(Ljava/util/List;)Lcom/sec/android/cloudagent/sync/f;
    .locals 14

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v5

    new-instance v6, Lcom/sec/android/cloudagent/sync/f;

    invoke-direct {v6}, Lcom/sec/android/cloudagent/sync/f;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->k()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    move v4, v1

    :goto_1
    if-nez v4, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v10

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v5, v10}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;

    move-result-object v3

    const/4 v2, 0x1

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/adt/a;->a()Z
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->a(Z)V

    if-eqz v4, :cond_6

    if-eqz v2, :cond_0

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/adt/a;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, v6, Lcom/sec/android/cloudagent/sync/f;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_3
    iget-object v0, v6, Lcom/sec/android/cloudagent/sync/f;->c:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    move v4, v1

    goto :goto_1

    :cond_5
    if-eqz v1, :cond_3

    iget-object v0, v6, Lcom/sec/android/cloudagent/sync/f;->e:Ljava/util/List;

    new-instance v1, Lcom/sec/android/cloudagent/e/b;

    invoke-direct {v1, v10}, Lcom/sec/android/cloudagent/e/b;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    if-eqz v2, :cond_f

    new-instance v2, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->f()Z

    move-result v11

    invoke-direct {v2, v10, v8, v4, v11}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Z)V

    if-nez v1, :cond_7

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v8, v1, :cond_9

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v8, v1, :cond_9

    :cond_7
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v1, :cond_8

    invoke-virtual {v5, v10}, Lcom/sec/android/cloudagent/ae;->e(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->e()J

    move-result-wide v12

    invoke-virtual {v5, v10, v12, v13}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;J)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_9
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v1, :cond_a

    invoke-static {v9}, Lcom/sec/android/cloudagent/sync/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {v9}, Lcom/sec/android/cloudagent/sync/b;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget-object v1, v6, Lcom/sec/android/cloudagent/sync/f;->b:Ljava/util/List;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v6, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v8, v1, :cond_c

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v1, :cond_d

    :cond_c
    invoke-static {v2}, Lcom/sec/android/cloudagent/a/a;->d(Lcom/sec/android/cloudagent/e/c;)Z

    move-result v1

    if-nez v1, :cond_e

    :cond_d
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v1, :cond_0

    invoke-virtual {v5, v10}, Lcom/sec/android/cloudagent/ae;->e(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_e
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->a(Z)V

    iget-object v0, v6, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_f
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v1, :cond_10

    invoke-static {v9}, Lcom/sec/android/cloudagent/sync/b;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-static {v9}, Lcom/sec/android/cloudagent/sync/b;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, v6, Lcom/sec/android/cloudagent/sync/f;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->a(Z)V

    :goto_4
    iget-object v1, v6, Lcom/sec/android/cloudagent/sync/f;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_11
    iget-object v1, v6, Lcom/sec/android/cloudagent/sync/f;->d:Ljava/util/List;

    new-instance v2, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->f()Z

    move-result v9

    invoke-direct {v2, v10, v8, v4, v9}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_12
    return-object v6

    :catch_0
    move-exception v11

    goto/16 :goto_2
.end method

.method public c()Ljava/util/List;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/sync/e;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/y;->b()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->g()V

    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->b:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v1}, Lcom/sec/android/cloudagent/d/b;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->e()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/sync/e;->c()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->a:Lcom/sec/android/cloudagent/sync/d;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/e;->a:Lcom/sec/android/cloudagent/sync/d;

    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/cloudagent/sync/e;->j:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/cloudagent/sync/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->g()V

    return-void

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/cloudagent/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    :goto_2
    invoke-direct {p0}, Lcom/sec/android/cloudagent/sync/e;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_3
    const-string v1, "doSync : CloudObject list is null !"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/cloudagent/sync/e;->a(Ljava/lang/String;I)Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/sync/e;->f:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/sync/e;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/cloudagent/sync/e;->a:Lcom/sec/android/cloudagent/sync/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/cloudagent/sync/d;->a(Ljava/lang/String;)Z

    goto :goto_1
.end method
