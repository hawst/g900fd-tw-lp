.class public Lcom/sec/android/cloudagent/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:I

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/f/d;->h()I

    move-result v0

    sput v0, Lcom/sec/android/cloudagent/a/a;->a:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/cloudagent/a/a;->b:Z

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/a;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/cloudagent/e/a;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    invoke-direct {v3, p0, v1, v2, v0}, Lcom/sec/android/cloudagent/e/a;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    invoke-static {v3}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/c;)V

    :try_start_1
    const-string v1, "Making current thread wait with latch"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const-wide/16 v2, 0x1e

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache path after thread wait:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/e/c;)V
    .locals 3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->d(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/c;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez p2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object p2

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3}, Lcom/sec/android/cloudagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p2, v4, :cond_5

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :cond_3
    :goto_0
    sget-object v3, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p2, v3, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p3}, Lcom/sec/android/cloudagent/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/g/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0x21

    if-ne v0, v3, :cond_a

    const/4 v0, 0x1

    :goto_1
    invoke-static {p1, v2, v1, v0}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;II)Lcom/sec/android/cloudagent/adt/CloudObject;

    :goto_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doCache SyncOption has been set off and delete file"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doCache but Document Sync Folder does not exist, so throw exception, and denit serverOperatiomManager pool"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->i()V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_5
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    goto :goto_0

    :cond_6
    const-string v0, "do Cache..getting cancelable @1"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p0, p1, v2}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    const-string v0, "do Cache..getting cancelable @2"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p0, p1, v2}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    const-string v0, "doCache but file was not exist"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_9
    return-void

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method private static a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/sec/android/cloudagent/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/cloudagent/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "do Cache..getting cancelable @1:not null:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/c;->a(Lcom/sec/android/cloudagent/c/a;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/a;->b()V

    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    check-cast p0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;Z)V
    .locals 2

    check-cast p0, Lcom/sec/android/cloudagent/e/c;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Z)I

    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    if-eqz p0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 10

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->d()Z

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MakeCache("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") cloudPath = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", ThumbAvailable = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v8

    sget-object v9, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v9, :cond_1

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v7, :cond_0

    new-instance v7, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/sec/android/cloudagent/e/t;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v9, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v9, :cond_2

    if-eqz v7, :cond_0

    new-instance v7, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/sec/android/cloudagent/e/t;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object v7, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v7, :cond_3

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    sget-object v7, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v7, :cond_0

    new-instance v7, Lcom/sec/android/cloudagent/e/f;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/sec/android/cloudagent/e/f;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v2}, Lcom/sec/android/cloudagent/g/a;->a(Ljava/util/List;)V

    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/cloudagent/c/h;->a(Ljava/util/List;)V

    :cond_6
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v3}, Lcom/sec/android/cloudagent/a/a;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_7
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/c/e;->a(Ljava/util/List;)V

    :cond_8
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {v5}, Lcom/sec/android/cloudagent/b/a;->a(Ljava/util/List;)V

    :cond_9
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/cloudagent/a/a;->b:Z

    return-void
.end method

.method public static a(Ljava/util/List;Z)V
    .locals 10

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/e/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/b;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/cloudagent/ae;->k(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v8}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/b;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v8

    sget-object v9, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v8, v9, :cond_1

    invoke-static {v7}, Lcom/sec/android/cloudagent/c;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/b;->a()V

    :cond_2
    if-eqz p1, :cond_0

    invoke-virtual {v1, v6}, Lcom/sec/android/cloudagent/ae;->e(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/e/s;

    invoke-direct {v0, v6}, Lcom/sec/android/cloudagent/e/s;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {v2}, Lcom/sec/android/cloudagent/g/a;->b(Ljava/util/List;)V

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "elapsed time for deleteCache is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static a()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isCacheUpdateRequired() -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/android/cloudagent/a/a;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    sget-boolean v0, Lcom/sec/android/cloudagent/a/a;->b:Z

    return v0
.end method

.method private static a(Lcom/sec/android/cloudagent/adt/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/a;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/a;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/a;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/e/n;Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()I
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->d()I

    move-result v0

    :cond_0
    return v0
.end method

.method public static b(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/adt/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/a;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/ae;->c()I

    move-result v2

    if-eqz p0, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/ae;->d()J

    move-result-wide v4

    sget v6, Lcom/sec/android/cloudagent/a/a;->a:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_6

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    sget v0, Lcom/sec/android/cloudagent/a/a;->a:I

    if-le v2, v0, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/ae;->e()Ljava/util/List;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    sget-object v5, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v2, v5}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    sget-object v5, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v2, v5}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v2

    if-eqz v2, :cond_5

    add-int/lit8 v2, v1, 0x1

    sget v5, Lcom/sec/android/cloudagent/a/a;->a:I

    if-ge v1, v5, :cond_4

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/adt/a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/sec/android/cloudagent/e/c;

    sget-object v5, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/adt/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v5, v4}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_3
    move v1, v2

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    sget v0, Lcom/sec/android/cloudagent/a/a;->a:I

    sub-int/2addr v0, v2

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/ae;->b(I)Ljava/util/List;

    move-result-object v0

    move v1, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/cloudagent/adt/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->d(Lcom/sec/android/cloudagent/adt/c;)V

    goto :goto_3

    :cond_5
    invoke-interface {p0}, Ljava/util/List;->clear()V

    :cond_6
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public static b(Lcom/sec/android/cloudagent/e/c;)V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/c;)V

    :cond_0
    return-void
.end method

.method public static b(Lcom/sec/android/cloudagent/e/m;)V
    .locals 8

    check-cast p0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v4

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;IZ)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7, v4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;JI)I

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;JIJ)V

    :cond_0
    return-void
.end method

.method public static c()I
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->c()V

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->c(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/util/List;)V

    :cond_1
    return v1
.end method

.method public static c(Lcom/sec/android/cloudagent/e/c;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/e;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public static c(Lcom/sec/android/cloudagent/e/m;)V
    .locals 5

    check-cast p0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;IZ)I

    return-void
.end method

.method private static c(Lcom/sec/android/cloudagent/adt/c;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/cloudagent/ae;->c(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->e()J

    move-result-wide v4

    invoke-virtual {v2, p0, v4, v5}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;J)Z
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method private static d()I
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/c/e;->a(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    sput-boolean v1, Lcom/sec/android/cloudagent/a/a;->b:Z

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static d(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 4

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CacheHandler::updateCache(), delete cached file : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/e/b;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/e/b;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public static d(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    check-cast p0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static d(Lcom/sec/android/cloudagent/e/c;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
