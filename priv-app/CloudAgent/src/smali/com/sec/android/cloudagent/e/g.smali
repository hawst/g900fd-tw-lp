.class public Lcom/sec/android/cloudagent/e/g;
.super Lcom/sec/android/cloudagent/e/j;


# instance fields
.field private a:Lcom/sec/android/cloudagent/adt/CloudObject;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/cloudagent/e/j;-><init>(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-static {p0, v0}, Lcom/sec/android/cloudagent/sync/b;->a(Lcom/sec/android/cloudagent/e/j;Lcom/sec/android/cloudagent/adt/CloudObject;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-static {p0, v0}, Lcom/sec/android/cloudagent/sync/b;->b(Lcom/sec/android/cloudagent/e/j;Lcom/sec/android/cloudagent/adt/CloudObject;)V

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/sync/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "post-execute for conflict localPath:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v1, v0}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Lcom/sec/android/cloudagent/adt/c;)V

    :cond_0
    invoke-super {p0}, Lcom/sec/android/cloudagent/e/j;->a()V

    return-void

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/content/ContentValues;)I

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;IZ)I

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public i()Z
    .locals 5

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/j;->i()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/g;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v1, v2, v3, v4}, Lcom/sec/android/cloudagent/h/a;->a(Lcom/sec/android/cloudagent/e/m;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/g;->a:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method
