.class public Lcom/sec/android/cloudagent/e/u;
.super Lcom/sec/android/cloudagent/e/e;

# interfaces
.implements Lcom/sec/android/cloudagent/e/m;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

.field private d:Lcom/sec/android/cloudagent/adt/CloudObject;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/e/e;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/u;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    const-string v0, "localPath should not be null !"

    invoke-static {p1, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/e/u;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/sec/android/cloudagent/e/u;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/cloudagent/e/u;->c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-static {p0}, Lcom/sec/android/cloudagent/h/a;->a(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public b()V
    .locals 0

    invoke-static {p0}, Lcom/sec/android/cloudagent/h/a;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/u;->a:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/u;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/u;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/sec/android/cloudagent/e/u;

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/u;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/u;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/u;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/u;->c:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/u;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/u;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v1, v2, v3, v4}, Lcom/sec/android/cloudagent/h/a;->a(Lcom/sec/android/cloudagent/e/m;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/cloudagent/e/u;->d:Lcom/sec/android/cloudagent/adt/CloudObject;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/u;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/u;->d:Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/u;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
