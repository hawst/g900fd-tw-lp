.class public Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;
.super Ljava/lang/Object;


# direct methods
.method protected static a(Lcom/sec/android/cloudagent/adt/b;Ljava/util/concurrent/CountDownLatch;)Lcom/sec/android/cloudagent/e/j;
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/b;->b()Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/e/g;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/cloudagent/e/g;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/sec/android/cloudagent/e/l;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/cloudagent/e/l;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/sec/android/cloudagent/e/i;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/cloudagent/e/i;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/sec/android/cloudagent/e/h;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/cloudagent/e/h;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v3, 0x2f

    const-string v0, "/"

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/CloudObject;)V
    .locals 4

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/content/ContentValues;)I

    new-instance v0, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->d()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/c;)V

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/cloudagent/ae;->d(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)V

    new-instance v1, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/c;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Lcom/sec/android/cloudagent/adt/CloudObject;)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;J)V
    .locals 5

    const/4 v1, 0x0

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-static {v1, p1, v0, p3, p4}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;J)V

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-static {v1, p0, v0, p3, p4}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->c(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->h(Ljava/lang/String;)Z

    move-result v2

    sget-object v3, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p2, v3, :cond_5

    sget-object v0, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v3, v0}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/cloudagent/ae;->d(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->f(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p2, v3, :cond_a

    if-eqz v0, :cond_8

    if-eqz v2, :cond_7

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->f(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v2, :cond_6

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_6
    invoke-virtual {v1, p2, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_7
    invoke-virtual {v1, p2, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_8
    if-eqz v2, :cond_9

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_9
    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-virtual {v1, v0, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p2, v3, :cond_0

    if-eqz v0, :cond_c

    if-eqz v2, :cond_b

    invoke-virtual {v1, p2, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto :goto_0

    :cond_b
    invoke-virtual {v1, p2, p1, p3, p4}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->i(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static a()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->c()I

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->e()V

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/ae;->f()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/util/List;)Z

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->e()V

    goto :goto_0
.end method

.method public static a(J)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/cloudagent/ae;->a(J)Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->e()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p0, v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p0, v2, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/cloudagent/ae;->c(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/cloudagent/ae;->n(Lcom/sec/android/cloudagent/adt/c;)J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p0, v2, :cond_5

    if-eqz p1, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne p0, v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sourcePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " destPath:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/sec/android/cloudagent/v;->b(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid Document Type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "invalid operation!! so skipped!"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {p1, p2, v1, v2, v3}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;J)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/cloudagent/sync/c;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/sync/c;-><init>(Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method private static b(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v1, "FILE_CREATED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "FILE_UPDATED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_2
    const-string v1, "FILE_RENAMED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->RENAMED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0

    :cond_3
    const-string v1, "FILE_DELETED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELETED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public static c()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->p()V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->f(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    return-void
.end method

.method static synthetic d()V
    .locals 0

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->e()V

    return-void
.end method

.method private static e()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/sync/SyncManager$State;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->g()V

    return-void
.end method
