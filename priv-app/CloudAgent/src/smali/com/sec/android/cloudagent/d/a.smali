.class public Lcom/sec/android/cloudagent/d/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/d/b;


# static fields
.field private static a:Lcom/sec/android/cloudagent/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/d/a;->a:Lcom/sec/android/cloudagent/d/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/d/a;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/d/a;->a:Lcom/sec/android/cloudagent/d/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/d/a;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/d/a;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/d/a;->a:Lcom/sec/android/cloudagent/d/a;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/d/a;->a:Lcom/sec/android/cloudagent/d/a;

    return-object v0
.end method

.method private a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_2

    const-string v0, "photo"

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_3

    const-string v0, "doc"

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_4

    const-string v0, "music"

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v1, :cond_0

    const-string v0, "video"

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lcom/sec/android/cloudagent/s;->a:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/sec/android/cloudagent/e/c;

    if-eqz v1, :cond_3

    check-cast p1, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/c;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/d/a;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    instance-of v1, p1, Lcom/sec/android/cloudagent/e/t;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/sec/android/cloudagent/e/t;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/t;->d()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/d/a;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_4
    instance-of v1, p1, Lcom/sec/android/cloudagent/e/g;

    if-nez v1, :cond_5

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/l;

    if-nez v1, :cond_5

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/i;

    if-eqz v1, :cond_2

    :cond_5
    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/d/a;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/d/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
