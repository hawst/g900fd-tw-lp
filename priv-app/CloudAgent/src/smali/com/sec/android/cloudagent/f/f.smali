.class public Lcom/sec/android/cloudagent/f/f;
.super Ljava/lang/Object;


# direct methods
.method public static a()Landroid/content/SharedPreferences;
    .locals 2

    const-string v0, "CloudSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Landroid/os/Bundle;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/auth/d;->b(I)Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v3

    const-string v4, "storageVender"

    invoke-virtual {v2, v4, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "accountNameList"

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->c()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, "accountName"

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "documentSyncFolderName"

    const-string v4, "DocumentSync"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "numberOfDisplayingPhotos"

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/f/d;->h()I

    move-result v4

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/f/d;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x1000

    :goto_0
    or-int/lit8 v4, v0, 0xb

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/f/d;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x2000

    :goto_1
    or-int/2addr v4, v0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x10

    :goto_2
    or-int/2addr v4, v0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x20

    :goto_3
    or-int/2addr v4, v0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x40

    :goto_4
    or-int/2addr v4, v0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x80

    :goto_5
    or-int/2addr v4, v0

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/f/d;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x4000

    :goto_6
    or-int/2addr v4, v0

    const-string v0, "photo"

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x100

    :goto_7
    or-int/2addr v4, v0

    const-string v0, "video"

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x200

    :goto_8
    or-int/2addr v4, v0

    const-string v0, "music"

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0x400

    :goto_9
    or-int/2addr v0, v4

    const-string v4, "doc"

    invoke-virtual {v3, v4}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v1, 0x800

    :cond_1
    or-int/2addr v0, v1

    const-string v1, "caToSettingsValues"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_6

    :cond_9
    move v0, v1

    goto :goto_7

    :cond_a
    move v0, v1

    goto :goto_8

    :cond_b
    move v0, v1

    goto :goto_9
.end method

.method public static a(ILcom/samsung/scloud/b/j;)Landroid/os/Bundle;
    .locals 14

    const-wide/16 v8, 0x0

    const-wide/high16 v12, 0x41d0000000000000L    # 1.073741824E9

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/auth/d;->b(I)Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    const-string v2, "storageVender"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->c()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->d()J

    move-result-wide v6

    add-long/2addr v4, v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "total size : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->b()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " used size : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->b()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " shared size : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/scloud/b/j;->d()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    cmp-long v0, v2, v8

    if-ltz v0, :cond_0

    cmp-long v0, v4, v8

    if-gez v0, :cond_1

    :cond_0
    const-string v0, "storageUsage"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v6, "0.#"

    invoke-direct {v0, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    long-to-double v6, v4

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    long-to-double v8, v2

    div-double/2addr v6, v8

    sget-object v8, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v8}, Ljava/text/DecimalFormat;->setRoundingMode(Ljava/math/RoundingMode;)V

    long-to-double v8, v4

    cmpl-double v8, v8, v12

    if-ltz v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    long-to-double v4, v4

    div-double/2addr v4, v12

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " GB ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%) / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    long-to-double v2, v2

    div-double/2addr v2, v12

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " GB"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v2, "storageUsage"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    long-to-double v4, v4

    const-wide/high16 v10, 0x4130000000000000L    # 1048576.0

    div-double/2addr v4, v10

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MB ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%) / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    long-to-double v2, v2

    div-double/2addr v2, v12

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " GB"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
