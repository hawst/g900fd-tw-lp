.class public Lcom/sec/android/cloudagent/d/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/d/b;


# static fields
.field private static a:Lcom/sec/android/cloudagent/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/d/d;->a:Lcom/sec/android/cloudagent/d/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/d/d;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/d/d;->a:Lcom/sec/android/cloudagent/d/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/d/d;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/d/d;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/d/d;->a:Lcom/sec/android/cloudagent/d/d;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/d/d;->a:Lcom/sec/android/cloudagent/d/d;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/auth/d;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/auth/d;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Cloud account not there..do not Sync!"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/sec/android/cloudagent/e/k;

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/d/d;->b()Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/d/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
