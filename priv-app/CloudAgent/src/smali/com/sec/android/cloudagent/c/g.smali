.class public Lcom/sec/android/cloudagent/c/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lcom/sec/android/cloudagent/e/m;


# direct methods
.method private constructor <init>(Lcom/sec/android/cloudagent/e/m;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;)Lcom/sec/android/cloudagent/c/g;
    .locals 1

    new-instance v0, Lcom/sec/android/cloudagent/c/g;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/c/g;-><init>(Lcom/sec/android/cloudagent/e/m;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/sec/android/cloudagent/e/m;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/sec/android/cloudagent/c/g;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/sec/android/cloudagent/c/g;

    iget-object v1, p1, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    iget-object v2, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-interface {v2}, Lcom/sec/android/cloudagent/e/m;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/cloudagent/s;->a:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-interface {v2}, Lcom/sec/android/cloudagent/e/m;->a()V

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-string v2, "PROFILE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Complete : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-interface {v2}, Lcom/sec/android/cloudagent/e/m;->b()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/g;->a:Lcom/sec/android/cloudagent/e/m;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
