.class public Lcom/sec/android/cloudagent/b/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v2

    sget-object v3, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v2, v3, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/a;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I

    :cond_2
    new-instance v1, Lcom/sec/android/cloudagent/e/f;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/cloudagent/e/f;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-static {v1}, Lcom/sec/android/cloudagent/b/a;->c(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object p1

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/android/cloudagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :cond_3
    invoke-static {p0, v0}, Lcom/sec/android/cloudagent/y;->a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)Z

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doMakeCoverArt SyncOption has beem set off and delete file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_4
    const-string v0, "doMakeCoverArt but file was not exist"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_5
    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/f;)V
    .locals 3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->d(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/f;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/e/m;)V
    .locals 4

    move-object v0, p0

    check-cast v0, Lcom/sec/android/cloudagent/e/f;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/f;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/f;->c()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/cloudagent/b/a;->b(Lcom/sec/android/cloudagent/e/m;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/e;->a(Ljava/util/List;)V

    return-void
.end method

.method public static b(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    check-cast p0, Lcom/sec/android/cloudagent/e/f;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/f;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_0
    if-eqz v1, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method public static c(Lcom/sec/android/cloudagent/e/m;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/c/e;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method
