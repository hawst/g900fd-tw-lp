.class public Lcom/sec/android/cloudagent/y;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/samsung/scloud/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/scloud/j;->a(Ljava/lang/String;)Lcom/samsung/scloud/b/d;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0

    :cond_1
    new-instance v1, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-direct {v1, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;-><init>(Lcom/samsung/scloud/b/f;)V

    return-object v1
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;II)Lcom/sec/android/cloudagent/adt/CloudObject;
    .locals 7

    const/4 v6, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudSDK getThumbnail ! ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v0, Lcom/samsung/scloud/a;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/scloud/a;->a(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/c/a;)Lcom/samsung/scloud/b/d;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-direct {v0, v2}, Lcom/sec/android/cloudagent/adt/CloudObject;-><init>(Lcom/samsung/scloud/b/f;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_2
    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/cloudagent/c/a;
    .locals 6

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudSDK getFileCancelable ! ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/samsung/scloud/b;

    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v1, Lcom/samsung/scloud/a;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/scloud/b;-><init>(Lcom/samsung/scloud/a;Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/c/a;)V

    new-instance v1, Lcom/sec/android/cloudagent/c/a;

    invoke-direct {v1, v0}, Lcom/sec/android/cloudagent/c/a;-><init>(Lcom/samsung/scloud/b;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/sec/android/cloudagent/c/b;
    .locals 7

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudSDK putFile ! filename = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " destinationID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/samsung/scloud/d;

    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v1, Lcom/samsung/scloud/a;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/scloud/d;-><init>(Lcom/samsung/scloud/a;Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/c/a;)V

    new-instance v1, Lcom/sec/android/cloudagent/c/b;

    invoke-direct {v1, v0}, Lcom/sec/android/cloudagent/c/b;-><init>(Lcom/samsung/scloud/d;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/a;
    .locals 3

    :try_start_0
    new-instance v0, Lcom/sec/android/cloudagent/sync/a;

    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v1, p0}, Lcom/samsung/scloud/j;->d(Ljava/lang/String;)Lcom/samsung/scloud/b/g;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/cloudagent/sync/a;-><init>(Lcom/samsung/scloud/b/g;Z)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static a()Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    const/16 v2, 0x1f4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/samsung/scloud/j;->a(ILjava/lang/String;)Lcom/samsung/scloud/b/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/e;

    new-instance v3, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-direct {v3, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;-><init>(Lcom/samsung/scloud/b/f;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/samsung/scloud/a;->a(Landroid/content/Context;)Lcom/samsung/scloud/a;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/aj;)V
    .locals 1

    new-instance v0, Lcom/sec/android/cloudagent/z;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/z;-><init>(Lcom/sec/android/cloudagent/aj;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/z;->start()V

    return-void
.end method

.method public static a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)Z
    .locals 4

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CloudSDK getCoverArt ! ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    sget-object v2, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/samsung/scloud/j;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/sec/android/cloudagent/ak;->a(Ljava/io/File;)V

    :cond_2
    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static b(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/a;
    .locals 3

    :try_start_0
    new-instance v1, Lcom/sec/android/cloudagent/sync/a;

    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v0, Lcom/samsung/scloud/a;

    invoke-virtual {v0, p0}, Lcom/samsung/scloud/a;->c(Ljava/lang/String;)Lcom/samsung/scloud/b/g;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/sec/android/cloudagent/sync/a;-><init>(Lcom/samsung/scloud/b/g;Z)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "android"

    invoke-static {v0}, Lcom/sec/android/cloudagent/y;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding trigger for hook:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;->hookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for cursor::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with regID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :try_start_0
    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v1, Lcom/samsung/scloud/a;

    invoke-virtual {v1, v0, p0, p1}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got trigger id for addTrigger:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static b()Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    const/16 v2, 0x1f4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/samsung/scloud/j;->b(ILjava/lang/String;)Lcom/samsung/scloud/b/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/d;

    new-instance v3, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-direct {v3, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;-><init>(Lcom/samsung/scloud/b/f;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0, p0}, Lcom/samsung/scloud/j;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static b(Lcom/sec/android/cloudagent/adt/c;)Z
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/scloud/j;->b(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static c()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v1}, Lcom/samsung/scloud/j;->a()Lcom/samsung/scloud/b/j;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/scloud/b/j;->a()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static c(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/c;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->i()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getStreamingURL("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Screen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Network:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v3, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v1, v2}, Lcom/samsung/scloud/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/b/i;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/scloud/b/i;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStreamingURL("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") -> URL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0, p0}, Lcom/samsung/scloud/j;->f(Ljava/lang/String;)V

    return-void
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0}, Lcom/samsung/scloud/j;->b()Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static d(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/scloud/j;->e(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "android"

    invoke-static {v0}, Lcom/sec/android/cloudagent/y;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/SamsungDropboxAPI$Hook;

    :try_start_0
    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v1, Lcom/samsung/scloud/a;

    invoke-virtual {v1, v0, p0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static e(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/scloud/j;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/b/h;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/scloud/b/h;->a()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method private static e(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    check-cast v0, Lcom/samsung/scloud/a;

    invoke-virtual {v0, p0}, Lcom/samsung/scloud/a;->g(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static e()Z
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0}, Lcom/samsung/scloud/j;->d()Z
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/scloud/j;->h(Ljava/lang/String;)Lcom/samsung/scloud/b/i;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/scloud/b/i;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStreamingURL("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") -> URL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static f()V
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0}, Lcom/samsung/scloud/j;->c()V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method public static g()V
    .locals 1

    :try_start_0
    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    invoke-interface {v0}, Lcom/samsung/scloud/j;->e()V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method

.method static synthetic h()Lcom/samsung/scloud/j;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/y;->a:Lcom/samsung/scloud/j;

    return-object v0
.end method
