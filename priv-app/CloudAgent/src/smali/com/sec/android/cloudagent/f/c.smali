.class public Lcom/sec/android/cloudagent/f/c;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/cloudagent/f/c;


# instance fields
.field private b:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/f/c;->b:Ljava/util/Set;

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/f/c;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/f/c;->a:Lcom/sec/android/cloudagent/f/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/f/c;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/f/c;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/f/c;->a:Lcom/sec/android/cloudagent/f/c;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/f/c;->a:Lcom/sec/android/cloudagent/f/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/android/cloudagent/f/a;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/f/c;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/f/b;

    invoke-interface {v0, p1}, Lcom/sec/android/cloudagent/f/b;->a(Lcom/sec/android/cloudagent/f/a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/f/b;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/f/c;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
