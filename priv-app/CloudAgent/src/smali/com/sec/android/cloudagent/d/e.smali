.class public Lcom/sec/android/cloudagent/d/e;
.super Ljava/lang/Object;


# static fields
.field private static a:I

.field private static b:Z

.field private static c:Z

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/cloudagent/d/e;->a:I

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->b:Z

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->c:Z

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->d:Z

    return-void
.end method

.method public static a()V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->j()I

    move-result v0

    sput v0, Lcom/sec/android/cloudagent/d/e;->a:I

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->k()I

    move-result v0

    const/16 v3, 0xf

    if-ge v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/cloudagent/d/e;->c:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->h()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/cloudagent/d/e;->b:Z

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->l()J

    move-result-wide v4

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->m()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    :goto_1
    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->d:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget v2, Lcom/sec/android/cloudagent/d/e;->a:I

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->j()I

    move-result v3

    sput v3, Lcom/sec/android/cloudagent/d/e;->a:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getConnectionType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/cloudagent/d/e;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v0

    move v2, v0

    :goto_0
    move v5, v0

    move v0, v1

    move v1, v5

    :goto_1
    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/s;->f()V

    :cond_0
    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/s;->h()V

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->e()V

    :cond_2
    return-void

    :cond_3
    invoke-static {v2}, Lcom/sec/android/cloudagent/d/e;->a(I)Z

    move-result v3

    if-eqz v3, :cond_4

    sget v3, Lcom/sec/android/cloudagent/d/e;->a:I

    if-nez v3, :cond_4

    move v1, v0

    move v2, v0

    goto :goto_0

    :cond_4
    sget v3, Lcom/sec/android/cloudagent/d/e;->a:I

    invoke-static {v3}, Lcom/sec/android/cloudagent/d/e;->a(I)Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v2, :cond_c

    move v1, v0

    move v2, v0

    goto :goto_0

    :cond_5
    const-string v3, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    sput-boolean v0, Lcom/sec/android/cloudagent/d/e;->c:Z

    move v0, v1

    move v2, v1

    goto :goto_1

    :cond_6
    const-string v3, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->c:Z

    move v2, v0

    move v0, v1

    goto :goto_1

    :cond_7
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    sput-boolean v0, Lcom/sec/android/cloudagent/d/e;->b:Z

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->c:Z

    move v2, v0

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_1

    :cond_8
    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->b:Z

    move v0, v1

    move v2, v1

    goto/16 :goto_1

    :cond_9
    const-string v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    sput-boolean v0, Lcom/sec/android/cloudagent/d/e;->d:Z

    move v0, v1

    move v2, v1

    goto/16 :goto_1

    :cond_a
    const-string v3, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    sput-boolean v1, Lcom/sec/android/cloudagent/d/e;->d:Z

    move v2, v0

    move v0, v1

    goto/16 :goto_1

    :cond_b
    move v0, v1

    move v2, v1

    goto/16 :goto_1

    :cond_c
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method private static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    invoke-static {v1}, Lcom/sec/android/cloudagent/d/e;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static c()Z
    .locals 1

    sget v0, Lcom/sec/android/cloudagent/d/e;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 3

    const/4 v0, 0x1

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    if-eq v1, v0, :cond_0

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Z
    .locals 2

    sget v0, Lcom/sec/android/cloudagent/d/e;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 2

    const/4 v0, 0x1

    sget-boolean v1, Lcom/sec/android/cloudagent/d/e;->b:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-boolean v1, Lcom/sec/android/cloudagent/d/e;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/cloudagent/d/e;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static h()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "plugged"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    if-eq v1, v0, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/sec/android/cloudagent/d/e;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "3g"

    goto :goto_0

    :pswitch_1
    const-string v0, "lte"

    goto :goto_0

    :pswitch_2
    const-string v0, "wifi"

    goto :goto_0

    :pswitch_3
    const-string v0, "roaming"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private static j()I
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x4

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    const-string v4, "connectivity"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    if-nez v4, :cond_1

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v5, v3, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    if-nez v5, :cond_6

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method private static k()I
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "level"

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    return v0
.end method

.method private static l()J
    .locals 4

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private static m()J
    .locals 6

    const-wide/32 v0, 0x1f400000

    invoke-static {}, Lcom/sec/android/cloudagent/c;->e()J

    move-result-wide v2

    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method
