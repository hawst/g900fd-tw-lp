.class public Lcom/sec/android/cloudagent/e/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/e/m;


# instance fields
.field private a:Lcom/sec/android/cloudagent/adt/c;


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CloudID should not be null !"

    invoke-static {p1, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/q;->a:Lcom/sec/android/cloudagent/adt/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public i()Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/e/q;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-static {v0}, Lcom/sec/android/cloudagent/y;->b(Lcom/sec/android/cloudagent/adt/c;)Z
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method
