.class public final enum Lcom/sec/android/cloudagent/sync/SyncManager$State;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/cloudagent/sync/SyncManager$State;

.field public static final enum IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

.field public static final enum RUNNING:Lcom/sec/android/cloudagent/sync/SyncManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/cloudagent/sync/SyncManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    new-instance v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/cloudagent/sync/SyncManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->RUNNING:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/cloudagent/sync/SyncManager$State;

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->IDLE:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/cloudagent/sync/SyncManager$State;->RUNNING:Lcom/sec/android/cloudagent/sync/SyncManager$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->$VALUES:[Lcom/sec/android/cloudagent/sync/SyncManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/SyncManager$State;
    .locals 1

    const-class v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/cloudagent/sync/SyncManager$State;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/sync/SyncManager$State;->$VALUES:[Lcom/sec/android/cloudagent/sync/SyncManager$State;

    invoke-virtual {v0}, [Lcom/sec/android/cloudagent/sync/SyncManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/cloudagent/sync/SyncManager$State;

    return-object v0
.end method
