.class public Lcom/sec/android/cloudagent/auth/d;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/cloudagent/auth/d;


# instance fields
.field private b:Ljava/util/HashMap;

.field private c:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/d;->e()V

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/auth/d;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/auth/d;->a:Lcom/sec/android/cloudagent/auth/d;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/auth/d;->a:Lcom/sec/android/cloudagent/auth/d;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/cloudagent/auth/d;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/auth/d;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/auth/d;->a:Lcom/sec/android/cloudagent/auth/d;

    sget-object v0, Lcom/sec/android/cloudagent/auth/d;->a:Lcom/sec/android/cloudagent/auth/d;

    goto :goto_0
.end method

.method private a(II)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->b()V

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/sec/android/cloudagent/auth/e;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/cloudagent/auth/e;-><init>(Lcom/sec/android/cloudagent/auth/d;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/auth/b;->a(Lcom/sec/android/cloudagent/auth/c;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private e()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/cloudagent/auth/gdrive/c;

    invoke-direct {v2, v4}, Lcom/sec/android/cloudagent/auth/gdrive/c;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/cloudagent/auth/dropbox/a;

    invoke-direct {v2, v3}, Lcom/sec/android/cloudagent/auth/dropbox/a;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/push/a;->a()Lcom/sec/android/cloudagent/push/a;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/push/a;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/cloudagent/s;->a:Z

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/d;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/s;->b(Landroid/content/Context;)Z

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->d()V

    return-void
.end method


# virtual methods
.method public a(ILcom/samsung/scloud/exception/SCloudException;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0, p2}, Lcom/sec/android/cloudagent/auth/b;->a(Lcom/samsung/scloud/exception/SCloudException;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0, p2}, Lcom/sec/android/cloudagent/auth/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(ILjava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/cloudagent/auth/b;->a(ILjava/lang/String;Z)V

    if-eqz p3, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/d;->g()V

    :cond_0
    return-void
.end method

.method public a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->e()Z

    move-result v0

    return v0
.end method

.method public b(I)Lcom/sec/android/cloudagent/auth/a;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {p0, v3}, Lcom/sec/android/cloudagent/auth/d;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/d;->d(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/cloudagent/auth/d;->a(II)V

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/d;->b:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/auth/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/b;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(I)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/d;->f()V

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/sec/android/cloudagent/auth/d;->a(II)V

    return-void
.end method
