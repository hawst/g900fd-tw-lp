.class public Lcom/sec/android/cloudagent/d/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/d/b;


# static fields
.field private static a:Lcom/sec/android/cloudagent/d/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/d/f;->a:Lcom/sec/android/cloudagent/d/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/d/f;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/d/f;->a:Lcom/sec/android/cloudagent/d/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/d/f;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/d/f;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/d/f;->a:Lcom/sec/android/cloudagent/d/f;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/d/f;->a:Lcom/sec/android/cloudagent/d/f;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requested File size() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide v4, 0x80000000L

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public a(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/f/d;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/d/f;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/d/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/d/f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
