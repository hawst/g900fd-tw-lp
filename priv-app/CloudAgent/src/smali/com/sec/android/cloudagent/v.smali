.class public Lcom/sec/android/cloudagent/v;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/a;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lcom/sec/android/cloudagent/a;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    invoke-static {v1}, Lcom/sec/android/cloudagent/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    invoke-static {v1}, Lcom/sec/android/cloudagent/a;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    invoke-static {p0}, Lcom/sec/android/cloudagent/v;->a(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/cloudagent/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/v;->a(Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public static b(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/android/cloudagent/v;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/sec/android/cloudagent/v;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->a(I)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    return-object v0
.end method
