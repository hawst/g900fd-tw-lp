.class public Lcom/sec/android/cloudagent/ae;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/cloudagent/ae;


# instance fields
.field private b:Landroid/content/ContentResolver;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I
    .locals 4

    const-string v1, "cloud_server_id= ?"

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-virtual {v2, p1, p3, v1, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a()Lcom/sec/android/cloudagent/ae;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/ae;->a:Lcom/sec/android/cloudagent/ae;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/ae;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ae;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/ae;->a:Lcom/sec/android/cloudagent/ae;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/ae;->a:Lcom/sec/android/cloudagent/ae;

    return-object v0
.end method

.method private b(Landroid/net/Uri;)Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v2, v6

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    if-nez v3, :cond_4

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;-><init>()V

    throw v0

    :cond_4
    return-object v3
.end method


# virtual methods
.method public a(Landroid/content/ContentValues;)I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return v0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;)I
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "cloud_server_id"

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "_data"

    const-string v2, "/"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "cloud_server_path"

    const-string v2, "/"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "cloud_is_dir"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;JI)I
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "cloud_last_viewed"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "media_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I
    .locals 2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-nez p2, :cond_0

    const-string v1, "cloud_thumb_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0

    :cond_0
    const-string v1, "cloud_thumb_path"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;IZ)I
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "cloud_is_cached"

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    if-nez p2, :cond_0

    const-string v1, "cloud_cached_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "_data"

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "media_type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0

    :cond_0
    const-string v1, "cloud_cached_path"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "_data"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "media_type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;Z)I
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "cloud_is_available_offline"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Z)I
    .locals 5

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "need_to_sync"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "local_path= ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget-object v3, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public a(I)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 1

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->OTHERS:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->OTHERS:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "cloud_server_id"

    aput-object v1, v2, v6

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v3, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v3
.end method

.method public a(J)Ljava/util/List;
    .locals 7

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "local_last_modified=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    new-instance v3, Lcom/sec/android/cloudagent/adt/b;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/adt/b;-><init>(Landroid/content/ContentValues;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    if-nez v2, :cond_3

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;-><init>()V

    throw v0

    :cond_3
    return-object v2

    :cond_4
    move-object v3, v2

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;I)Ljava/util/List;
    .locals 10

    const/4 v4, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "media_type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_is_cached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 0 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_cached_path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IS NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz p2, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cloud_last_viewed DESC LIMIT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v8, "cloud_server_id"

    aput-object v8, v2, v7

    const-string v8, "cloud_server_path"

    aput-object v8, v2, v6

    const-string v8, "cloud_is_available_thumb"

    aput-object v8, v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_4

    move v0, v6

    :goto_1
    new-instance v5, Lcom/sec/android/cloudagent/e/c;

    new-instance v8, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v8, v3}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v8, p1, v4, v0}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Z)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v2

    :cond_4
    move v0, v7

    goto :goto_1

    :cond_5
    move-object v5, v4

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "media_type=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/i;->d()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;JIJ)V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "cloud_cache_date_modified"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "media_type"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v1, 0x1

    if-ne p4, v1, :cond_0

    const-string v1, "Updating cached size for photos!!"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const-string v1, "_size"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V
    .locals 5

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "local_path= ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const-string v3, "local_last_modified"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "operation_type"

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v3, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 5

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "local_path= ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string v3, "number_of_tries"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v3, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/ae;->c(Ljava/util/List;)I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;J)Z
    .locals 10

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v8, "date_modified"

    aput-object v8, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    move v0, v6

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_2
    return v0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    move v0, v7

    goto :goto_2

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 8

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v3, "cloud_localpath= ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v7

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/q;->b()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v6

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    return v0

    :cond_0
    move v0, v7

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_1
.end method

.method public b(Landroid/content/ContentValues;)I
    .locals 3

    const-string v0, "cloud_server_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v2, v0}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, v2, p1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method public b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne p1, v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/sec/android/cloudagent/adt/c;Ljava/lang/String;)I
    .locals 2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-nez p2, :cond_0

    const-string v1, "cloud_coverart_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;Lcom/sec/android/cloudagent/adt/c;Landroid/content/ContentValues;)I

    move-result v0

    return v0

    :cond_0
    const-string v1, "cloud_coverart_path"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v6, 0x0

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cloud_server_path=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_is_dir"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "cloud_server_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v4, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v4, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v4
.end method

.method public b(I)Ljava/util/List;
    .locals 9

    const/4 v8, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "cloud_is_cached=0 AND cloud_cached_path IS NULL"

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/ab;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "cloud_server_id"

    aput-object v4, v2, v8

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cloud_last_viewed ASC LIMIT "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v2, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v6
.end method

.method public b(Ljava/util/List;)Ljava/util/List;
    .locals 5

    const/16 v1, 0x2710

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v1, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_0
    if-ge v3, v2, :cond_0

    invoke-interface {p1, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit16 v3, v3, 0x2710

    add-int/lit16 v1, v1, 0x2710

    if-le v1, v2, :cond_2

    move v1, v2

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/i;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/CloudProvider;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/CloudProvider;->a()V

    :cond_0
    return-void
.end method

.method public b(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 4

    const-string v1, "cloud_server_id= ?"

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/i;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public b(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;J)V
    .locals 5

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "local_path"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "local_last_modified"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "operation_type"

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "need_to_sync"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "number_of_tries"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public c()I
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-string v3, "cloud_is_cached=\"1\""

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/ab;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "cloud_server_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method public c(Ljava/util/List;)I
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Landroid/content/ContentValues;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/CloudObject;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->g()Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    const-string v3, "cloud_cached_path= ?"

    new-array v4, v2, [Ljava/lang/String;

    aput-object p1, v4, v7

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "cloud_server_id"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v5, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v5, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5
.end method

.method public c(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/cloudagent/ae;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/ContentValues;)Z
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/q;->b()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/sec/android/cloudagent/adt/c;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_3

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v8, "cloud_is_cached"

    aput-object v8, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_0

    move v0, v6

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_2
    return v0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    move v0, v7

    goto :goto_1

    :cond_2
    move v0, v7

    goto :goto_2

    :cond_3
    move-object v4, v5

    goto :goto_0
.end method

.method public d()J
    .locals 8

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/ab;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "cloud_server_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    int-to-long v0, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v6

    goto :goto_0
.end method

.method public d(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "media_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_is_cached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_cached_path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "cloud_cached_path"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v1
.end method

.method public d(Ljava/util/List;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/c;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(Lcom/sec/android/cloudagent/adt/c;)Z
    .locals 8

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v6

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_2
    return v0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    move v0, v7

    goto :goto_2

    :cond_2
    move-object v4, v2

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lcom/sec/android/cloudagent/q;->b()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "cloud_localpath= ?"

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iget-object v3, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return v4
.end method

.method public e(Ljava/util/List;)I
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Landroid/content/ContentValues;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public e(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/b;
    .locals 6

    const/4 v2, 0x0

    const-string v3, "local_path= ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    new-instance v2, Lcom/sec/android/cloudagent/adt/b;

    invoke-direct {v2, v1}, Lcom/sec/android/cloudagent/adt/b;-><init>(Landroid/content/ContentValues;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    if-nez v2, :cond_2

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;-><init>()V

    throw v0

    :cond_2
    return-object v2
.end method

.method public e()Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    const-string v3, "cloud_is_cached=1 AND cloud_cached_path NOT NULL"

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/ab;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "cloud_server_id"

    aput-object v4, v2, v6

    const/4 v4, 0x0

    const-string v5, "cloud_last_viewed DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v3, v2}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v1
.end method

.method public e(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Ljava/util/List;
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "media_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_thumb_path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "cloud_thumb_path"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v1
.end method

.method public e(Lcom/sec/android/cloudagent/adt/c;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v8, "cloud_thumb_path"

    aput-object v8, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v6

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_2
    return v0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    move v0, v7

    goto :goto_2

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public f(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I
    .locals 5

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "media_type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_is_cached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloud_cached_path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "cloud_is_cached"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "cloud_cached_path"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/String;)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;
    .locals 8

    const/4 v2, 0x1

    const/4 v7, 0x0

    sget-object v6, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->OTHERS:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    const-string v3, "local_path= ?"

    new-array v4, v2, [Ljava/lang/String;

    aput-object p1, v4, v7

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "operation_type"

    aput-object v5, v2, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/adt/b;->a(I)Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    :cond_0
    move-object v0, v6

    goto :goto_0

    :cond_1
    move-object v0, v6

    goto :goto_1
.end method

.method public f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "cloud_cached_path"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/cloudagent/ae;->a(J)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()Landroid/database/Cursor;
    .locals 9

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v3, "media_type NOT NULL AND _data like ?"

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/cloudagent/c;->a:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v8

    const-string v4, "_data"

    aput-object v4, v2, v7

    const/4 v4, 0x2

    const-string v6, "_size"

    aput-object v6, v2, v4

    const/4 v4, 0x3

    const-string v6, "date_added"

    aput-object v6, v2, v4

    const/4 v4, 0x4

    const-string v6, "date_modified"

    aput-object v6, v2, v4

    const/4 v4, 0x5

    const-string v6, "mime_type"

    aput-object v6, v2, v4

    new-array v4, v7, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    move-object v5, v0

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public g(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "cloud_thumb_path"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)Z
    .locals 8

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v3, "local_path= ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v7

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v5, "need_to_sync"

    aput-object v5, v2, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_0

    move v0, v6

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    return v0

    :cond_0
    move v0, v7

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1
.end method

.method public h(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public h()Z
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ae;->e(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/b;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public i(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "_display_name"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public i()V
    .locals 5

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ae;->g()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/v;->b(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v2

    sget-object v3, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-eq v2, v3, :cond_3

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/m;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Landroid/content/ContentValues;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/ContentValues;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-void

    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v1, v2}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)V
    .locals 4

    const-string v0, "local_path= ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object v2, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v7, "media_type"

    aput-object v7, v2, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/ae;->a(I)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v6

    goto :goto_1

    :cond_1
    move v0, v6

    goto :goto_2

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public j()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/i;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/CloudProvider;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/CloudProvider;->b()V

    :cond_0
    return-void
.end method

.method public k(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "cloud_is_cached"

    aput-object v6, v2, v8

    const-string v6, "cloud_cached_path"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v7, :cond_0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public k()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/n;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    .locals 6

    const/4 v2, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    new-instance v2, Lcom/sec/android/cloudagent/adt/a;

    invoke-direct {v2, v1}, Lcom/sec/android/cloudagent/adt/a;-><init>(Landroid/content/ContentValues;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    if-nez v2, :cond_2

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;-><init>()V

    throw v0

    :cond_2
    return-object v2

    :cond_3
    move-object v4, v2

    goto :goto_0
.end method

.method public l()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/k;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public m(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "cloud_revision"

    aput-object v6, v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v5

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public m()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/o;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public n(Lcom/sec/android/cloudagent/adt/c;)J
    .locals 10

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v9, 0x0

    const-wide/16 v6, 0x0

    const-string v3, "cloud_server_id= ?"

    if-eqz p1, :cond_2

    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v8, "cloud_cache_date_modified"

    aput-object v8, v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_2
    return-wide v0

    :cond_0
    move-wide v0, v6

    goto :goto_1

    :cond_1
    move-wide v0, v6

    goto :goto_2

    :cond_2
    move-object v4, v5

    goto :goto_0
.end method

.method public n()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/l;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/ae;->b(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/List;
    .locals 7

    const/4 v6, 0x0

    const/4 v4, 0x0

    const-string v3, "cloud_is_available_thumb = 1 AND cloud_thumb_path IS NULL AND media_type in (1,3)"

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/aa;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "cloud_server_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    new-instance v1, Lcom/sec/android/cloudagent/adt/c;

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    if-nez v4, :cond_3

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;-><init>()V

    throw v0

    :cond_3
    return-object v4
.end method

.method public p()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ae;->b:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/sec/android/cloudagent/h;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
