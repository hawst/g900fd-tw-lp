.class public Lcom/sec/android/cloudagent/e/h;
.super Lcom/sec/android/cloudagent/e/j;


# instance fields
.field private a:Lcom/sec/android/cloudagent/e/j;

.field private b:Lcom/sec/android/cloudagent/e/j;

.field private c:Lcom/sec/android/cloudagent/adt/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->DELTED_AND_CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/cloudagent/e/j;-><init>(Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/h;->c:Lcom/sec/android/cloudagent/adt/c;

    new-instance v0, Lcom/sec/android/cloudagent/e/i;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/cloudagent/e/i;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/h;->a:Lcom/sec/android/cloudagent/e/j;

    new-instance v0, Lcom/sec/android/cloudagent/e/g;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/cloudagent/e/g;-><init>(Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/h;->b:Lcom/sec/android/cloudagent/e/j;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->c(Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/h;->c:Lcom/sec/android/cloudagent/adt/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/h;->b:Lcom/sec/android/cloudagent/e/j;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/j;->a()V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/h;->a:Lcom/sec/android/cloudagent/e/j;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/j;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/h;->b:Lcom/sec/android/cloudagent/e/j;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/j;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/h;->a:Lcom/sec/android/cloudagent/e/j;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/j;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/h;->c:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->b(Lcom/sec/android/cloudagent/adt/c;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/h;->b:Lcom/sec/android/cloudagent/e/j;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/j;->i()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
