.class public final Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;
.super Landroid/app/Activity;


# instance fields
.field a:Lcom/google/api/client/googleapis/a/a/a/a;

.field b:Ljava/lang/String;

.field c:Lcom/google/api/client/googleapis/auth/a/a;

.field d:I

.field private e:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/api/client/googleapis/auth/a/a;

    invoke-direct {v0}, Lcom/google/api/client/googleapis/auth/a/a;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c:Lcom/google/api/client/googleapis/auth/a/a;

    return-void
.end method

.method private a(I)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c()V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->finish()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(I)V

    return-void
.end method

.method private b()V
    .locals 9

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a:Lcom/google/api/client/googleapis/a/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/a/a/a/a;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    const-string v2, "oauth2:https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/drive"

    new-instance v7, Lcom/sec/android/cloudagent/auth/gdrive/b;

    invoke-direct {v7, p0}, Lcom/sec/android/cloudagent/auth/gdrive/b;-><init>(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;)V

    move-object v4, p0

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v8}, Landroid/accounts/AccountManager;->getAuthTokenByFeatures(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f040011

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a:Lcom/google/api/client/googleapis/a/a/a/a;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/a/a/a/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a:Lcom/google/api/client/googleapis/a/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/a/a/a/a;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "oauth2:https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/drive"

    const/4 v4, 0x1

    new-instance v5, Lcom/sec/android/cloudagent/auth/gdrive/a;

    invoke-direct {v5, p0}, Lcom/sec/android/cloudagent/auth/gdrive/a;-><init>(Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;)V

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method a(Ljava/io/IOException;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    iget v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->d:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/cloudagent/auth/d;->a(ILjava/lang/String;Z)V

    invoke-direct {p0, v3}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a(I)V

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    invoke-virtual {p1}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a:Lcom/google/api/client/googleapis/a/a/a/a;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c:Lcom/google/api/client/googleapis/auth/a/a;

    invoke-virtual {v1}, Lcom/google/api/client/googleapis/auth/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/a/a/a/a;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c:Lcom/google/api/client/googleapis/auth/a/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/auth/a/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    const-string v0, "googleCredential"

    const-string v1, "accessToken"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->d:I

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a()V

    goto :goto_0
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "googleCredential"

    const-string v1, "accessToken"

    invoke-static {v0, v1, p1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c:Lcom/google/api/client/googleapis/auth/a/a;

    invoke-virtual {v0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->b:Ljava/lang/String;

    const-string v0, "googleCredential"

    const-string v1, "accessToken"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c:Lcom/google/api/client/googleapis/auth/a/a;

    invoke-virtual {v1, v0}, Lcom/google/api/client/googleapis/auth/a/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    new-instance v0, Lcom/google/api/client/googleapis/a/a/a/a;

    invoke-direct {v0, p0}, Lcom/google/api/client/googleapis/a/a/a/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a:Lcom/google/api/client/googleapis/a/a/a/a;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->d:I

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->a()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;->c()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
