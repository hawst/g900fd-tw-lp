.class public Lcom/sec/android/cloudagent/adt/CloudObject;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/samsung/scloud/b/f;

.field private b:Lcom/sec/android/cloudagent/adt/c;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/util/List;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/b/f;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->c:Z

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->d:Z

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->e:Z

    iput-object v1, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->f:Ljava/util/List;

    iput-object v1, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->g:Ljava/lang/String;

    const-string v0, "cloudNode should not be null"

    invoke-static {p1, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/f;

    iput-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/scloud/b/f;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->c:Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->h()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->h()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->f:Ljava/util/List;

    :cond_1
    new-instance v0, Lcom/sec/android/cloudagent/adt/c;

    iget-object v1, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v1}, Lcom/samsung/scloud/b/f;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->b:Lcom/sec/android/cloudagent/adt/c;

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->g:Ljava/lang/String;

    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, "<unknown>"

    :cond_0
    return-object p1
.end method

.method private c(Ljava/lang/String;)I
    .locals 2

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/sec/android/cloudagent/e;->c:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/e;->c:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->g:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->g:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->d:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->c:Z

    return v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->f:Ljava/util/List;

    return-object v0
.end method

.method public d()Lcom/sec/android/cloudagent/adt/c;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->b:Lcom/sec/android/cloudagent/adt/c;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->e()Z

    move-result v0

    return v0
.end method

.method public g()Landroid/content/ContentValues;
    .locals 8

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/cloudagent/v;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const-string v4, "_size"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "date_added"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->l()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "date_modified"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->m()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "mime_type"

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "cloud_etag"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "cloud_revision"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x2

    if-ne v3, v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->b()Lcom/samsung/scloud/b/a;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v4, "title"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "artist"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "album"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "track"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "genre_name"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->f()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "duration"

    invoke-virtual {v2}, Lcom/samsung/scloud/b/a;->e()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const/4 v2, 0x1

    if-ne v3, v2, :cond_1

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->v_()Lcom/samsung/scloud/b/c;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "width"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "height"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "orientation"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "datetaken"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "longitude"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "latitude"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "_data"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "_display_name"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "bucket_id"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/cloudagent/adt/CloudObject;->c(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "bucket_display_name"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/cloudagent/adt/CloudObject;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cloud_last_viewed"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->m()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "cloud_server_id"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->b:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cloud_is_dir"

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "cloud_is_cached"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "cloud_is_available_offline"

    iget-boolean v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "cloud_server_path"

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cloud_is_available_thumb"

    iget-object v2, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v2}, Lcom/samsung/scloud/b/f;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v1
.end method

.method public h()Landroid/content/ContentValues;
    .locals 6

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->v_()Lcom/samsung/scloud/b/c;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "width"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "height"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "orientation"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "datetaken"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "longitude"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "latitude"

    invoke-virtual {v0}, Lcom/samsung/scloud/b/c;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v1
.end method

.method public i()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->OTHERS:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->k()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DIR:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/adt/CloudObject;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/v;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->n()Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/adt/CloudObject;->a:Lcom/samsung/scloud/b/f;

    invoke-virtual {v0}, Lcom/samsung/scloud/b/f;->g()Z

    move-result v0

    return v0
.end method
