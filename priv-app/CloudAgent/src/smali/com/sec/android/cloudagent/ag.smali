.class public Lcom/sec/android/cloudagent/ag;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    invoke-direct {p0}, Lcom/sec/android/cloudagent/ag;->a()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/ag;->b()V

    return-void
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/cloudfiles"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/cloudfiles/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/uploads"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/uploads/#"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/documents"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/cloudfiles_no_server_op"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/media_old"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/media_new"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/media_created"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/media_updated"

    const/16 v3, 0x12f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/media_deleted"

    const/16 v3, 0x130

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/images/media"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/images/media/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/video/media"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/video/media/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/music/media"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    const-string v1, "cloud"

    const-string v2, "*/music/media/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/ag;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/net/Uri;Landroid/content/ContentValues;)Lcom/sec/android/cloudagent/ah;
    .locals 3

    new-instance v0, Lcom/sec/android/cloudagent/ah;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ah;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v1, "uploads"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    :goto_0
    iput-object p2, v0, Lcom/sec/android/cloudagent/ah;->b:Landroid/content/ContentValues;

    return-object v0

    :sswitch_1
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const-string v1, "documents"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const-string v1, "media_old"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const-string v1, "media_new"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const-string v1, "media_created"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    const-string v1, "media_updated"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const-string v1, "media_deleted"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_0
        0x9 -> :sswitch_2
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x12d -> :sswitch_4
        0x12e -> :sswitch_5
        0x12f -> :sswitch_6
        0x130 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    new-instance v1, Lcom/sec/android/cloudagent/ai;

    invoke-direct {v1}, Lcom/sec/android/cloudagent/ai;-><init>()V

    const-string v0, "groupBy"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "limit"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "distinct"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    :cond_0
    invoke-virtual {p0, p2}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "images"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v0, "distinct"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    :cond_1
    :goto_0
    iput-object p3, v1, Lcom/sec/android/cloudagent/ai;->c:[Ljava/lang/String;

    iput-object p4, v1, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    iput-object p5, v1, Lcom/sec/android/cloudagent/ai;->d:[Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/cloudagent/ai;->e:Ljava/lang/String;

    iput-object v3, v1, Lcom/sec/android/cloudagent/ai;->f:Ljava/lang/String;

    return-object v1

    :sswitch_1
    const-string v0, "images"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v0, "distinct"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    const-string v0, "video"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v0, "video"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_4
    const-string v0, "music"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    const-string v0, "music"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "cloudfiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "cloudfiles"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "uploads"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "uploads"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "documents"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "media_old"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "media_new"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "media_created"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "media_updated"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "media_deleted"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_7
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0x64 -> :sswitch_4
        0x65 -> :sswitch_5
        0xc8 -> :sswitch_6
        0x12c -> :sswitch_b
        0x12d -> :sswitch_c
        0x12e -> :sswitch_d
        0x12f -> :sswitch_e
        0x130 -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;
    .locals 3

    new-instance v0, Lcom/sec/android/cloudagent/ai;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ai;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    :goto_0
    iget-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    iput-object p2, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    :cond_0
    :goto_1
    return-object v0

    :sswitch_1
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const-string v1, "media_type=1"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const-string v1, "media_type=3"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const-string v1, "media_type=2"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    const-string v1, "uploads"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    const-string v1, "uploads"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const-string v1, "documents"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    const-string v1, "media_old"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    const-string v1, "media_new"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    const-string v1, "media_created"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    const-string v1, "media_updated"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    const-string v1, "media_deleted"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_0
        0x5 -> :sswitch_3
        0x6 -> :sswitch_0
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x9 -> :sswitch_7
        0x64 -> :sswitch_4
        0x65 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_8
        0x12d -> :sswitch_9
        0x12e -> :sswitch_a
        0x12f -> :sswitch_b
        0x130 -> :sswitch_c
    .end sparse-switch
.end method

.method public b(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;
    .locals 3

    new-instance v0, Lcom/sec/android/cloudagent/ai;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ai;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    :goto_0
    iget-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    iput-object p2, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    :cond_0
    :goto_1
    return-object v0

    :sswitch_1
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const-string v1, "cloudfiles"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const-string v1, "uploads"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const-string v1, "uploads"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    const-string v1, "documents"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const-string v1, "media_old"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    const-string v1, "media_new"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    const-string v1, "media_created"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    const-string v1, "media_updated"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "media_deleted"

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    if-eqz p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0x64 -> :sswitch_2
        0x65 -> :sswitch_3
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_7
        0x12d -> :sswitch_8
        0x12e -> :sswitch_9
        0x12f -> :sswitch_a
        0x130 -> :sswitch_b
    .end sparse-switch
.end method

.method public b(Landroid/net/Uri;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method

.method public c(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_0
        0x6 -> :sswitch_0
        0x65 -> :sswitch_0
    .end sparse-switch
.end method
