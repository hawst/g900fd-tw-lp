.class public Lcom/sec/android/cloudagent/e/c;
.super Lcom/sec/android/cloudagent/e/d;

# interfaces
.implements Lcom/sec/android/cloudagent/e/m;


# instance fields
.field private a:Lcom/sec/android/cloudagent/adt/c;

.field protected b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

.field protected c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/e/d;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/c;->d:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/e/c;->c:Z

    iput-boolean v1, p0, Lcom/sec/android/cloudagent/e/c;->f:Z

    const-string v0, "CloudID should not be null !"

    invoke-static {p1, v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/adt/c;

    iput-object v0, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    iput-object p2, p0, Lcom/sec/android/cloudagent/e/c;->b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    iput-object p3, p0, Lcom/sec/android/cloudagent/e/c;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    iput-boolean p4, p0, Lcom/sec/android/cloudagent/e/c;->c:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->d(Lcom/sec/android/cloudagent/adt/c;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/m;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/c;)V

    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/e/m;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/e/c;->d:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/cloudagent/e/c;->f:Z

    return-void
.end method

.method public b()V
    .locals 0

    invoke-static {p0}, Lcom/sec/android/cloudagent/a/a;->d(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/cloudagent/e;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/e/c;->c:Z

    return v0
.end method

.method public e()Lcom/sec/android/cloudagent/adt/c;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/sec/android/cloudagent/e/c;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/sec/android/cloudagent/e/c;

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/c;->e()Lcom/sec/android/cloudagent/adt/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/e/c;->a:Lcom/sec/android/cloudagent/adt/c;

    iget-object v2, p0, Lcom/sec/android/cloudagent/e/c;->b:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v2, v3}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/c;Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while caching -- "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    instance-of v2, v1, Lcom/sec/android/cloudagent/exception/CloudCancelException;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/e/c;->a(Z)V

    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/cloudagent/e/c;->f:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/e/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
