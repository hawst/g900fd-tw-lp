.class public Lcom/sec/android/cloudagent/detector/SmartDetector;
.super Ljava/lang/Object;


# static fields
.field private static b:Lcom/sec/android/cloudagent/detector/SmartDetector;

.field private static d:Z


# instance fields
.field public final a:Landroid/os/Handler;

.field private c:Lcom/sec/android/cloudagent/detector/b;

.field private e:Lcom/sec/android/cloudagent/detector/SmartDetector$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->b:Lcom/sec/android/cloudagent/detector/SmartDetector;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->d:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    sget-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector$State;->IDLE:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    iput-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->e:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    new-instance v0, Lcom/sec/android/cloudagent/detector/a;

    invoke-direct {v0, p0}, Lcom/sec/android/cloudagent/detector/a;-><init>(Lcom/sec/android/cloudagent/detector/SmartDetector;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->a:Landroid/os/Handler;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)I
    .locals 5

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, v0}, Lcom/sec/android/cloudagent/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartDetector found records! : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/cloudagent/detector/SmartObserverService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a()Lcom/sec/android/cloudagent/detector/SmartDetector;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->b:Lcom/sec/android/cloudagent/detector/SmartDetector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/detector/SmartDetector;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->b:Lcom/sec/android/cloudagent/detector/SmartDetector;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->b:Lcom/sec/android/cloudagent/detector/SmartDetector;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/detector/SmartDetector;)Lcom/sec/android/cloudagent/detector/b;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/detector/SmartDetector;Lcom/sec/android/cloudagent/detector/b;)Lcom/sec/android/cloudagent/detector/b;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    return-object p1
.end method

.method public static a(Z)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->f()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    goto :goto_0
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->d:Z

    return-void
.end method

.method public static f()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->b(Z)V

    return-void
.end method

.method public static g()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->k()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->b(Z)V

    return-void
.end method

.method public static i()Z
    .locals 1

    sget-boolean v0, Lcom/sec/android/cloudagent/detector/SmartDetector;->d:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/b;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->e:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    sget-object v1, Lcom/sec/android/cloudagent/detector/SmartDetector$State;->IDLE:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "Starting new thread from SmartDetector!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/cloudagent/detector/b;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/detector/b;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->c:Lcom/sec/android/cloudagent/detector/b;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/b;->start()V

    :goto_0
    return-void

    :cond_1
    const-string v0, "SmartThread is alaive!Set state to run for SD !"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/detector/SmartDetector$State;->RUN_AGAIN:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Lcom/sec/android/cloudagent/detector/SmartDetector$State;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/detector/SmartDetector$State;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->e:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    return-void
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->e:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    sget-object v1, Lcom/sec/android/cloudagent/detector/SmartDetector$State;->RUN_AGAIN:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->l()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "FILE_CREATED"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->m()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "FILE_UPDATED"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->n()Ljava/util/List;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string v1, "FILE_DELETED"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/detector/SmartDetector;->a:Landroid/os/Handler;

    return-object v0
.end method
