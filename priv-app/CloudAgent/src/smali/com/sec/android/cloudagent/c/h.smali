.class public Lcom/sec/android/cloudagent/c/h;
.super Lcom/sec/android/cloudagent/c/d;


# static fields
.field private static a:Lcom/sec/android/cloudagent/c/h;


# instance fields
.field private b:Lcom/sec/android/cloudagent/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/c/h;->a:Lcom/sec/android/cloudagent/c/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x2

    const/4 v0, 0x1

    invoke-direct {p0, v1, v1, v0}, Lcom/sec/android/cloudagent/c/d;-><init>(IIZ)V

    invoke-static {}, Lcom/sec/android/cloudagent/d/a;->a()Lcom/sec/android/cloudagent/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/c/h;->b:Lcom/sec/android/cloudagent/d/b;

    return-void
.end method

.method private f(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    check-cast p1, Lcom/sec/android/cloudagent/e/e;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/e;->c()Lcom/sec/android/cloudagent/c/b;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "CancelableUploadRequest is set!!!"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/cloudagent/c/i;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/cloudagent/c/i;-><init>(Lcom/sec/android/cloudagent/c/h;Lcom/sec/android/cloudagent/c/b;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method private g(Lcom/sec/android/cloudagent/e/m;)V
    .locals 3

    const-string v0, "SOM!DownloadRequest instance of CancelableDownloadRequest!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/sec/android/cloudagent/e/d;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/d;->k()Lcom/sec/android/cloudagent/c/a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SOM!Cancelable for --"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p1, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const-string v1, "SOM!CacheRequest is cancelable"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/cloudagent/c/j;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/cloudagent/c/j;-><init>(Lcom/sec/android/cloudagent/c/h;Lcom/sec/android/cloudagent/c/a;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public static h()Lcom/sec/android/cloudagent/c/h;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/c/h;->a:Lcom/sec/android/cloudagent/c/h;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/c/h;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/c/h;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/c/h;->a:Lcom/sec/android/cloudagent/c/h;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/c/h;->a:Lcom/sec/android/cloudagent/c/h;

    return-object v0
.end method

.method private static k()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/c/h;->a:Lcom/sec/android/cloudagent/c/h;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/h;->b:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v0}, Lcom/sec/android/cloudagent/d/b;->b()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/h;->b:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v0, p1}, Lcom/sec/android/cloudagent/d/b;->a(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    return v0
.end method

.method public e(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServerOperationManager : Put!! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/h;->j()V

    invoke-super {p0}, Lcom/sec/android/cloudagent/c/d;->c()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->k()V

    return-void
.end method

.method public j()V
    .locals 3

    const-string v0, "cancelCurrentDocTasks!Copied into new!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/h;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/sec/android/cloudagent/c/g;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/g;->a()Lcom/sec/android/cloudagent/e/m;

    move-result-object v0

    instance-of v2, v0, Lcom/sec/android/cloudagent/e/e;

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/c/h;->f(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lcom/sec/android/cloudagent/e/d;

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/c/h;->g(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :cond_2
    return-void
.end method
