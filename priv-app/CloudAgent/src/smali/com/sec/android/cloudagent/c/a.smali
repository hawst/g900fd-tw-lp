.class public Lcom/sec/android/cloudagent/c/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/samsung/scloud/b;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    iput-object p1, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    if-eqz v0, :cond_0

    const-string v0, "instance is not null!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    invoke-virtual {v0}, Lcom/samsung/scloud/b;->b()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Download Cancel result ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/c/a;->a:Lcom/samsung/scloud/b;

    invoke-virtual {v0}, Lcom/samsung/scloud/b;->a()V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/exception/a;->a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;

    move-result-object v0

    throw v0
.end method
