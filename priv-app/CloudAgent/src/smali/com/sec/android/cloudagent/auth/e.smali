.class Lcom/sec/android/cloudagent/auth/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/auth/c;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/sec/android/cloudagent/auth/d;


# direct methods
.method constructor <init>(Lcom/sec/android/cloudagent/auth/d;I)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/cloudagent/auth/e;->b:Lcom/sec/android/cloudagent/auth/d;

    iput p2, p0, Lcom/sec/android/cloudagent/auth/e;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/android/cloudagent/exception/CloudException;)V
    .locals 0

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    return-void
.end method

.method public a(Z)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_OUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "storageVender"

    iget v2, p0, Lcom/sec/android/cloudagent/auth/e;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v0}, Lcom/sec/android/cloudagent/c;->a(Landroid/content/Intent;)V

    const-string v0, "CloudAgent Signed out !"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    return-void
.end method
