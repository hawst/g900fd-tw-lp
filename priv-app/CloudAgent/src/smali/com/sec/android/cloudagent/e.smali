.class public Lcom/sec/android/cloudagent/e;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/Long;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field public static final h:Ljava/lang/String;

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:I

.field public static final l:[Ljava/lang/String;

.field public static final m:I

.field public static final n:[I

.field public static final o:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x4

    const-string v0, "DROPBOX"

    sput-object v0, Lcom/sec/android/cloudagent/e;->a:Ljava/lang/String;

    const-wide/32 v0, 0x1499700

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->b:Ljava/lang/Long;

    const-string v0, "Dropbox"

    sput-object v0, Lcom/sec/android/cloudagent/e;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cloudagent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->e:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DocumentSync"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->g:Ljava/lang/String;

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/cloudagent/e;->h:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->i:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "coverart"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/e;->j:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/cloudagent/c;->e()J

    move-result-wide v0

    const-wide/32 v2, 0x400000

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/16 v0, 0x1f4

    sput v0, Lcom/sec/android/cloudagent/e;->k:I

    :goto_0
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "csc_pref_key_wifi_only"

    aput-object v1, v0, v8

    const/4 v1, 0x1

    const-string v2, "csc_pref_key_picture_sync"

    aput-object v2, v0, v1

    const-string v1, "csc_pref_key_video_sync"

    aput-object v1, v0, v7

    const-string v1, "csc_pref_key_music_sync"

    aput-object v1, v0, v5

    const-string v1, "csc_pref_key_doc_sync"

    aput-object v1, v0, v4

    const/4 v1, 0x5

    const-string v2, "csc_pref_key_doc_use_mobile_data"

    aput-object v2, v0, v1

    const-string v1, "csc_pref_key_enable_sync_roaming"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/cloudagent/e;->l:[Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/cloudagent/c;->f()I

    move-result v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_2

    sput v6, Lcom/sec/android/cloudagent/e;->m:I

    :goto_1
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/cloudagent/e;->n:[I

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.samsung.android.app.episodes"

    aput-object v1, v0, v8

    const/4 v1, 0x1

    const-string v2, "com.sec.android.app.myfiles"

    aput-object v2, v0, v1

    const-string v1, "com.infraware.polarisoffice5"

    aput-object v1, v0, v7

    const-string v1, "com.infraware.polarisoffice4"

    aput-object v1, v0, v5

    const-string v1, "com.infraware.PolarisOfficeStdForTablet"

    aput-object v1, v0, v4

    const/4 v1, 0x5

    const-string v2, "com.infraware.polarisoffice5tablet"

    aput-object v2, v0, v1

    const-string v1, "com.sec.android.cloudagent"

    aput-object v1, v0, v6

    const/4 v1, 0x7

    const-string v2, "com.sec.android.app.cloudagentdemo"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.samsung.android.app.lifetimes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/cloudagent/e;->o:[Ljava/lang/String;

    return-void

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/c;->e()J

    move-result-wide v0

    const-wide/32 v2, 0x800000

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/cloudagent/e;->k:I

    goto :goto_0

    :cond_1
    const/16 v0, 0x7d0

    sput v0, Lcom/sec/android/cloudagent/e;->k:I

    goto :goto_0

    :cond_2
    const/16 v1, 0xa0

    if-ne v0, v1, :cond_3

    sput v4, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_3
    const/16 v1, 0xd5

    if-ne v0, v1, :cond_4

    sput v4, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_4
    const/16 v1, 0xf0

    if-ne v0, v1, :cond_5

    sput v4, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_5
    const/16 v1, 0x140

    if-ne v0, v1, :cond_6

    sput v4, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_6
    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_7

    sput v5, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_7
    const/16 v1, 0x280

    if-ne v0, v1, :cond_8

    sput v5, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    :cond_8
    sput v6, Lcom/sec/android/cloudagent/e;->m:I

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x1f
        0x21
    .end array-data
.end method
