.class public abstract Lcom/sec/android/cloudagent/c/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/cloudagent/c/c;


# instance fields
.field private a:Lcom/sec/android/cloudagent/c/k;


# direct methods
.method public constructor <init>(IIZ)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    new-instance v1, Lcom/sec/android/cloudagent/c/k;

    const-wide/16 v4, 0x1388

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    move v2, p1

    move v3, p2

    move-object v8, p0

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/cloudagent/c/k;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Lcom/sec/android/cloudagent/c/c;)V

    iput-object v1, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0, p3}, Lcom/sec/android/cloudagent/c/k;->a(Z)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/e/m;

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/c/d;->b(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/d;->g()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->c()V

    return-void
.end method

.method public b(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/c/d;->a(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/c/d;->d(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Duplicated request : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/c/k;->d(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/c/d;->c(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Executing request : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/c/d;->e(Lcom/sec/android/cloudagent/e/m;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/c/k;->b(Lcom/sec/android/cloudagent/e/m;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " does not put!!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->shutdownNow()Ljava/util/List;

    return-void
.end method

.method public c(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/c/k;->a(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->a()Z

    move-result v0

    return v0
.end method

.method public d(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/c/k;->c(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract e(Lcom/sec/android/cloudagent/e/m;)V
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->d()Z

    move-result v0

    return v0
.end method

.method public f()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/d;->a:Lcom/sec/android/cloudagent/c/k;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/k;->e()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public abstract g()V
.end method
