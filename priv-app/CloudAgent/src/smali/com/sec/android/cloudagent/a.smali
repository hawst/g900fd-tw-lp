.class public Lcom/sec/android/cloudagent/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/HashMap;

.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v7, 0x1f

    const/16 v6, 0x8

    const/16 v5, 0x68

    const/16 v4, 0x1c

    const/16 v3, 0xe

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/a;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/a;->b:Ljava/util/HashMap;

    const-string v0, "MP3"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MPGA"

    const/4 v1, 0x1

    const-string v2, "audio/mpeg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M4A"

    const/4 v1, 0x2

    const-string v2, "audio/mp4"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WAV"

    const/4 v1, 0x3

    const-string v2, "audio/x-wav"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AMR"

    const/4 v1, 0x4

    const-string v2, "audio/amr"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AWB"

    const/4 v1, 0x5

    const-string v2, "audio/amr-wb"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3GP"

    const/16 v1, 0xc

    const-string v2, "audio/3gpp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3GA"

    const/16 v1, 0xc

    const-string v2, "audio/3gpp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "ASF"

    const/4 v1, 0x6

    const-string v2, "audio/x-ms-wma"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WMA"

    const/4 v1, 0x6

    const-string v2, "audio/x-ms-wma"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PYA"

    const/16 v1, 0xa

    const-string v2, "audio/vnd.ms-playready.media.pya"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AAC"

    const-string v1, "audio/mp4a-latm"

    invoke-static {v0, v6, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AAC"

    const-string v1, "audio/m4a"

    invoke-static {v0, v6, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MID"

    const-string v1, "audio/mid"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MIDI"

    const-string v1, "audio/mid"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PYV"

    const/16 v1, 0xc9

    const-string v2, "video/vnd.ms-playready.media.pyv"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DM"

    const/16 v1, 0x34

    const-string v2, "application/vnd.oma.drm.content"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DCF"

    const/16 v1, 0x35

    const-string v2, "application/vnd.oma.drm.content"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "JPG"

    const-string v1, "image/jpg"

    invoke-static {v0, v7, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "JPEG"

    const-string v1, "image/jpg"

    invoke-static {v0, v7, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "BMP"

    const/16 v1, 0x22

    const-string v2, "image/bmp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "OGG"

    const/4 v1, 0x7

    const-string v2, "audio/ogg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "OGG"

    const/4 v1, 0x7

    const-string v2, "application/ogg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "OGA"

    const/4 v1, 0x7

    const-string v2, "application/ogg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AAC"

    const-string v1, "audio/aac"

    invoke-static {v0, v6, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AAC"

    const-string v1, "audio/aac-adts"

    invoke-static {v0, v6, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MKA"

    const/16 v1, 0x9

    const-string v2, "audio/x-matroska"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MID"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MIDI"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "XMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "RTTTL"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "SMF"

    const/16 v1, 0xf

    const-string v2, "audio/sp-midi"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "IMY"

    const/16 v1, 0x10

    const-string v2, "audio/imelody"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "RTX"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "OTA"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MXMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v3, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MPEG"

    const/16 v1, 0x15

    const-string v2, "video/mpeg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MPG"

    const/16 v1, 0x15

    const-string v2, "video/mpeg"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MP4"

    const/16 v1, 0x15

    const-string v2, "video/mp4"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M4V"

    const/16 v1, 0x16

    const-string v2, "video/mp4"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3GP"

    const/16 v1, 0x17

    const-string v2, "video/3gpp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3GPP"

    const/16 v1, 0x17

    const-string v2, "video/3gpp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3G2"

    const/16 v1, 0x18

    const-string v2, "video/3gpp2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "3GPP2"

    const/16 v1, 0x18

    const-string v2, "video/3gpp2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MKV"

    const/16 v1, 0x1b

    const-string v2, "video/x-matroska"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WEBM"

    const/16 v1, 0x1e

    const-string v2, "video/webm"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "TS"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "AVI"

    const/16 v1, 0x1d

    const-string v2, "video/avi"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WMV"

    const/16 v1, 0x19

    const-string v2, "video/x-ms-wmv"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "ASF"

    const/16 v1, 0x1a

    const-string v2, "video/x-ms-asf"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "JPG"

    const-string v1, "image/jpeg"

    invoke-static {v0, v7, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "JPEG"

    const-string v1, "image/jpeg"

    invoke-static {v0, v7, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "GIF"

    const/16 v1, 0x20

    const-string v2, "image/gif"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PNG"

    const/16 v1, 0x21

    const-string v2, "image/png"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "BMP"

    const/16 v1, 0x22

    const-string v2, "image/x-ms-bmp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WBMP"

    const/16 v1, 0x23

    const-string v2, "image/vnd.wap.wbmp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WEBP"

    const/16 v1, 0x24

    const-string v2, "image/webp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M3U"

    const/16 v1, 0x29

    const-string v2, "audio/x-mpegurl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M3U"

    const/16 v1, 0x29

    const-string v2, "application/x-mpegurl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PLS"

    const/16 v1, 0x2a

    const-string v2, "audio/x-scpls"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "WPL"

    const/16 v1, 0x2b

    const-string v2, "application/vnd.ms-wpl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M3U8"

    const/16 v1, 0x2c

    const-string v2, "application/vnd.apple.mpegurl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M3U8"

    const/16 v1, 0x2c

    const-string v2, "audio/mpegurl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M3U8"

    const/16 v1, 0x2c

    const-string v2, "audio/x-mpegurl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "FL"

    const/16 v1, 0x33

    const-string v2, "application/x-android-drm-fl"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PDF"

    const/16 v1, 0x67

    const-string v2, "application/pdf"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "RTF"

    const-string v1, "application/msword"

    invoke-static {v0, v5, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DOC"

    const-string v1, "application/msword"

    invoke-static {v0, v5, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DOCX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-static {v0, v5, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DOT"

    const-string v1, "application/msword"

    invoke-static {v0, v5, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DOTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    invoke-static {v0, v5, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "CSV"

    const/16 v1, 0x66

    const-string v2, "text/comma-separated-values"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "XLS"

    const/16 v1, 0x69

    const-string v2, "application/vnd.ms-excel"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "XLSX"

    const/16 v1, 0x69

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "XLT"

    const/16 v1, 0x69

    const-string v2, "application/vnd.ms-excel"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "XLTX"

    const/16 v1, 0x69

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PPS"

    const/16 v1, 0x65

    const-string v2, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PPT"

    const/16 v1, 0x6a

    const-string v2, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "PPTX"

    const/16 v1, 0x6a

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "POT"

    const/16 v1, 0x6a

    const-string v2, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "POTX"

    const/16 v1, 0x6a

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "ASC"

    const/16 v1, 0x64

    const-string v2, "text/plain"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "TXT"

    const/16 v1, 0x6b

    const-string v2, "text/plain"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "HWP"

    const/16 v1, 0x6c

    const-string v2, "application/x-hwp"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "GUL"

    const/16 v1, 0x6d

    const-string v2, "application/jungumword"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "FLAC"

    const/16 v1, 0xd

    const-string v2, "audio/flac"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "ZIP"

    const/16 v1, 0x3c

    const-string v2, "application/zip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MPG"

    const/16 v1, 0xc8

    const-string v2, "video/mp2p"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MPEG"

    const/16 v1, 0xc8

    const-string v2, "video/mp2p"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "DIVX"

    const/16 v1, 0xca

    const-string v2, "video/divx"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "FLV"

    const/16 v1, 0xcc

    const-string v2, "video/flv"

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "TP"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "TRP"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M2TS"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "MTS"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    const-string v0, "M2T"

    const-string v1, "video/mp2ts"

    invoke-static {v0, v4, v1}, Lcom/sec/android/cloudagent/a;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/a;->a:Ljava/util/HashMap;

    new-instance v1, Lcom/sec/android/cloudagent/b;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/cloudagent/b;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/sec/android/cloudagent/a;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-lt p0, v0, :cond_0

    const/16 v1, 0xd

    if-le p0, v1, :cond_2

    :cond_0
    const/16 v1, 0xe

    if-lt p0, v1, :cond_1

    const/16 v1, 0x10

    if-le p0, v1, :cond_2

    :cond_1
    const/16 v1, 0x11

    if-lt p0, v1, :cond_3

    const/16 v1, 0x12

    if-gt p0, v1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/sec/android/cloudagent/a;->c(Ljava/lang/String;)Lcom/sec/android/cloudagent/b;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/sec/android/cloudagent/b;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 1

    const/16 v0, 0x15

    if-lt p0, v0, :cond_0

    const/16 v0, 0x1e

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_2

    const/16 v0, 0xcd

    if-gt p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Lcom/sec/android/cloudagent/b;
    .locals 2

    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/sec/android/cloudagent/a;->a:Ljava/util/HashMap;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/cloudagent/b;

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 1

    const/16 v0, 0x1f

    if-lt p0, v0, :cond_0

    const/16 v0, 0x24

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)Z
    .locals 1

    const/16 v0, 0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x6d

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
