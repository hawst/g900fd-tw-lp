.class public Lcom/sec/android/cloudagent/CloudProvider;
.super Landroid/content/ContentProvider;


# static fields
.field private static final c:Landroid/net/Uri;

.field private static final d:Landroid/net/Uri;

.field private static final e:Landroid/net/Uri;

.field private static final f:Landroid/net/Uri;


# instance fields
.field private a:Lcom/sec/android/cloudagent/x;

.field private final b:Lcom/sec/android/cloudagent/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/j;->b()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/CloudProvider;->c:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/cloudagent/r;->b()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/CloudProvider;->d:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/cloudagent/i;->c()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/CloudProvider;->e:Landroid/net/Uri;

    invoke-static {}, Lcom/sec/android/cloudagent/p;->b()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/cloudagent/CloudProvider;->f:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Lcom/sec/android/cloudagent/ag;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ag;-><init>()V

    iput-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_0
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/cloudagent/e/b;

    new-instance v4, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v4, v2}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/sec/android/cloudagent/e/b;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/sec/android/cloudagent/adt/c;

    invoke-direct {v3, v2}, Lcom/sec/android/cloudagent/adt/c;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/cloudagent/a/a;->a(Ljava/util/List;Z)V

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/cloudagent/w;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/cloudagent/w;-><init>(Lcom/sec/android/cloudagent/CloudProvider;Ljava/util/List;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)V
    .locals 12

    new-instance v1, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v0, "media_new"

    invoke-direct {v1, p1, v0}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    const-string v0, "_id"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v0, "_data"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const-string v0, "_size"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    const-string v0, "date_added"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const-string v0, "date_modified"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    const-string v0, "mime_type"

    invoke-virtual {v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    array-length v8, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_6

    aget-object v9, p2, v0

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->prepareForInsert()V

    const-string v10, "_id"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_0

    const-string v10, "_id"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v1, v2, v10}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_0
    const-string v10, "_data"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    const-string v10, "_data"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v3, v10}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_1
    const-string v10, "_size"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    if-eqz v10, :cond_2

    const-string v10, "_size"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v1, v4, v10, v11}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_2
    const-string v10, "date_added"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    if-eqz v10, :cond_3

    const-string v10, "date_added"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v1, v5, v10, v11}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_3
    const-string v10, "date_modified"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    if-eqz v10, :cond_4

    const-string v10, "date_modified"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v1, v6, v10, v11}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_4
    const-string v10, "mime_type"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_5

    const-string v10, "mime_type"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v7, v9}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->execute()J

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/cloudagent/CloudProvider;->e:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/i;->d()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/CloudProvider;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/CloudProvider;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/CloudProvider;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p2, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const/4 v0, 0x1

    return v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)V
    .locals 44

    new-instance v5, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v4, "cloudfiles"

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v4}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    const-string v4, "_size"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    const-string v4, "date_added"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v4, "date_modified"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    const-string v4, "mime_type"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    const-string v4, "media_type"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v4, "cloud_server_id"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    const-string v4, "_data"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    const-string v4, "title"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    const-string v4, "_display_name"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    const-string v4, "cloud_is_dir"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    const-string v4, "cloud_is_cached"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    const-string v4, "cloud_is_available_offline"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    const-string v4, "bucket_id"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    const-string v4, "bucket_display_name"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    const-string v4, "cloud_server_path"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    const-string v4, "cloud_cached_path"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    const-string v4, "cloud_thumb_path"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const-string v4, "cloud_is_available_thumb"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    const-string v4, "cloud_last_viewed"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    const-string v4, "cloud_revision"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    const-string v4, "width"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const-string v4, "height"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    const-string v4, "datetaken"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    const-string v4, "longitude"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    const-string v4, "latitude"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    const-string v4, "orientation"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    const-string v4, "genre_name"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    const-string v4, "year"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    const-string v4, "artist"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    const-string v4, "album"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    const-string v4, "composer"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    const-string v4, "track"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    const-string v4, "duration"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    const-string v4, "cloud_coverart_path"

    invoke-virtual {v5, v4}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v39

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v40, v0

    const/4 v4, 0x0

    :goto_0
    move/from16 v0, v40

    if-ge v4, v0, :cond_22

    aget-object v41, p2, v4

    invoke-virtual {v5}, Landroid/database/DatabaseUtils$InsertHelper;->prepareForInsert()V

    const-string v42, "_size"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    if-eqz v42, :cond_0

    const-string v42, "_size"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move-wide/from16 v0, v42

    invoke-virtual {v5, v6, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_0
    const-string v42, "date_added"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    if-eqz v42, :cond_1

    const-string v42, "date_added"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move-wide/from16 v0, v42

    invoke-virtual {v5, v7, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_1
    const-string v42, "date_modified"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    if-eqz v42, :cond_2

    const-string v42, "date_modified"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move-wide/from16 v0, v42

    invoke-virtual {v5, v8, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_2
    const-string v42, "mime_type"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_3

    const-string v42, "mime_type"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v5, v9, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_3
    const-string v42, "media_type"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_4

    const-string v42, "media_type"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v42

    invoke-virtual {v5, v10, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_4
    const-string v42, "cloud_server_id"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_5

    const-string v42, "cloud_server_id"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v5, v11, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_5
    const-string v42, "_data"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_6

    const-string v42, "_data"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v5, v12, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_6
    const-string v42, "title"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_7

    const-string v42, "title"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v5, v13, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_7
    const-string v42, "_display_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_8

    const-string v42, "_display_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v5, v14, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_8
    const-string v42, "cloud_is_dir"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    if-eqz v42, :cond_9

    const-string v42, "cloud_is_dir"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v42

    move/from16 v0, v42

    invoke-virtual {v5, v15, v0}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IZ)V

    :cond_9
    const-string v42, "cloud_is_cached"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    if-eqz v42, :cond_a

    const-string v42, "cloud_is_cached"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v42

    move/from16 v0, v16

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IZ)V

    :cond_a
    const-string v42, "cloud_is_available_offline"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    if-eqz v42, :cond_b

    const-string v42, "cloud_is_available_offline"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v42

    move/from16 v0, v17

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IZ)V

    :cond_b
    const-string v42, "bucket_id"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_c

    const-string v42, "bucket_id"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v18

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_c
    const-string v42, "bucket_display_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_d

    const-string v42, "bucket_display_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v19

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_d
    const-string v42, "cloud_server_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_e

    const-string v42, "cloud_server_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v20

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_e
    const-string v42, "cloud_cached_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_f

    const-string v42, "cloud_cached_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v21

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_f
    const-string v42, "cloud_thumb_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_10

    const-string v42, "cloud_thumb_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v22

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_10
    const-string v42, "cloud_is_available_thumb"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    if-eqz v42, :cond_11

    const-string v42, "cloud_is_available_thumb"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v42

    move/from16 v0, v23

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IZ)V

    :cond_11
    const-string v42, "cloud_last_viewed"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    if-eqz v42, :cond_12

    const-string v42, "cloud_last_viewed"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move/from16 v0, v24

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_12
    const-string v42, "cloud_revision"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_13

    const-string v42, "cloud_revision"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v25

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_13
    const-string v42, "cloud_coverart_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_14

    const-string v42, "cloud_coverart_path"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v39

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_14
    const-string v42, "width"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_15

    const-string v42, "width"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v26

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_15
    const-string v42, "height"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_16

    const-string v42, "height"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v27

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_16
    const-string v42, "datetaken"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    if-eqz v42, :cond_17

    const-string v42, "datetaken"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move/from16 v0, v28

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_17
    const-string v42, "longitude"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v42

    if-eqz v42, :cond_18

    const-string v42, "longitude"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v42

    move/from16 v0, v29

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ID)V

    :cond_18
    const-string v42, "longitude"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v42

    if-eqz v42, :cond_19

    const-string v42, "latitude"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v42

    move/from16 v0, v30

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ID)V

    :cond_19
    const-string v42, "orientation"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_1a

    const-string v42, "orientation"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v31

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_1a
    const-string v42, "genre_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_1b

    const-string v42, "genre_name"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v32

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_1b
    const-string v42, "year"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_1c

    const-string v42, "year"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v42

    move/from16 v0, v33

    move/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(II)V

    :cond_1c
    const-string v42, "artist"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_1d

    const-string v42, "artist"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v34

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_1d
    const-string v42, "album"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_1e

    const-string v42, "album"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v35

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_1e
    const-string v42, "composer"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    if-eqz v42, :cond_1f

    const-string v42, "composer"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    move/from16 v0, v36

    move-object/from16 v1, v42

    invoke-virtual {v5, v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    :cond_1f
    const-string v42, "track"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_20

    const-string v42, "track"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move/from16 v0, v37

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_20
    const-string v42, "duration"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    if-eqz v42, :cond_21

    const-string v42, "duration"

    invoke-virtual/range {v41 .. v42}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/Long;->longValue()J

    move-result-wide v42

    move/from16 v0, v38

    move-wide/from16 v1, v42

    invoke-virtual {v5, v0, v1, v2}, Landroid/database/DatabaseUtils$InsertHelper;->bind(IJ)V

    :cond_21
    invoke-virtual {v5}, Landroid/database/DatabaseUtils$InsertHelper;->execute()J

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_22
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v5}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    return-void

    :catchall_0
    move-exception v4

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v5}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    throw v4
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM cloudfiles"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM uploads"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM documents"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM media_old"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM media_new"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/CloudProvider;->e:Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM media_old"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "INSERT INTO media_old SELECT * FROM media_new;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DELETE FROM media_new"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 3

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    array-length v0, p2

    const/4 v2, 0x1

    if-ge v0, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/i;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v1, p2}, Lcom/sec/android/cloudagent/CloudProvider;->b(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/android/cloudagent/m;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1, p2}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/database/sqlite/SQLiteDatabase;[Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/d;->c()I

    move-result v0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/auth/d;->a(I)Z

    move-result v2

    const/4 v0, 0x0

    const-string v1, "cloud_available"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "API call : isCloudAvailable = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    const-string v1, "cloudAvailable"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v1, "cloud_vendor_available"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/cloudagent/auth/d;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "API call : isCloudVendorAvailable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    const-string v3, "cloud_vendor_available"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    :cond_2
    const-string v1, "cloud_vendor"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "API call : cloudvendor uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "cloud_vendor"

    sget-object v3, Lcom/sec/android/cloudagent/e;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloudvendor cloudVendorID = 1, cloudVendorName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/sec/android/cloudagent/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_3
    if-eqz v2, :cond_11

    const-string v1, "is_sync_on"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->OTHERS:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/f/d;->a(Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "API call : isSyncOn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    const-string v2, "isSyncOn"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    :cond_4
    :goto_1
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->DOC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    goto :goto_0

    :cond_5
    const-string v1, "sync"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "API call : sync"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/s;->d()V

    goto :goto_1

    :cond_6
    const-string v1, "get_thumbnail"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : getThumbnail id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sec/android/cloudagent/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ThumbnailBitmap"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v1, "get_streaming_URL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : getStreamingURL uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "STREAMING_URL"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exception"

    new-instance v3, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_8
    const-string v1, "get_download_URL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : getDownloadURL uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    :try_start_1
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->g(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "DOWNLOAD_URL"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exception"

    new-instance v3, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_9
    const-string v1, "prefetch_with_blocking"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : requestBlockingPrefetch uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v3

    if-ne v2, v3, :cond_a

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "exception"

    new-instance v2, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    new-instance v3, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v3}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    invoke-direct {v2, v3}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_a
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->c(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "CACHED_PATH"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    const-string v1, "get_share_URL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : getShareURL uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    :try_start_2
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->h(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "SHARE_URL"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exception"

    new-instance v3, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_c
    const-string v1, "download"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : download uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->b()Z

    move-result v1

    if-nez v1, :cond_d

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "exception"

    new-instance v2, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    new-instance v3, Lcom/sec/android/cloudagent/exception/CloudException;

    const-string v4, "Network not available"

    invoke-direct {v3, v4}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_d
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    :try_start_3
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->e(Lcom/sec/android/cloudagent/adt/c;)V
    :try_end_3
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v0

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exception"

    new-instance v3, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    invoke-direct {v3, v1}, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    :cond_e
    const-string v1, "prefetch"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : prefetch uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestMakeCache cloudID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mediaType = CLOUD_FILE_TYPE.MUSIC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->a(Lcom/sec/android/cloudagent/adt/c;)V

    goto/16 :goto_1

    :cond_f
    const-string v1, "make_available_offline"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : makeAvailableOffline uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestMakeCache cloudID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/adt/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->b(Lcom/sec/android/cloudagent/adt/c;)V

    goto/16 :goto_1

    :cond_10
    const-string v1, "revert_available_offline"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API call : revertAvailableOffline uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestRevertAvailableOffline cloudID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mediaType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/s;->d(Lcom/sec/android/cloudagent/adt/c;)V

    goto/16 :goto_1

    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cloud isn\'t available !!! : isCloudAvailable = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;

    move-result-object v8

    iget-object v1, v8, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    const-string v2, "cloudfiles"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ag;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v8, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "cloud_server_id"

    aput-object v5, v2, v3

    iget-object v3, v8, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/database/Cursor;)V

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v1, v8, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    iget-object v2, v8, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_1
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    if-eqz p2, :cond_1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    :goto_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/x;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    invoke-virtual {v2, p1, v0}, Lcom/sec/android/cloudagent/ag;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Lcom/sec/android/cloudagent/ah;

    move-result-object v0

    iget-object v2, v0, Lcom/sec/android/cloudagent/ah;->a:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/sec/android/cloudagent/ah;->b:Landroid/content/ContentValues;

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_0
    return-object p1

    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/s;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/s;->b(Landroid/content/Context;)Z

    new-instance v0, Lcom/sec/android/cloudagent/x;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/cloudagent/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    const/4 v0, 0x0

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ag;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v3

    sget-object v4, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->PHOTO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v3, v4, :cond_0

    new-instance v4, Lcom/sec/android/cloudagent/e/c;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/adt/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v1, v3, v2}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/android/cloudagent/a/a;->c(Lcom/sec/android/cloudagent/e/c;)V

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t have cached path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t have cached path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    const/4 v7, 0x0

    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v8, v0

    :goto_0
    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/cloudagent/ag;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;

    move-result-object v0

    iget-object v3, v0, Lcom/sec/android/cloudagent/ai;->c:[Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/sec/android/cloudagent/ai;->d:[Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/cloudagent/ai;->e:Ljava/lang/String;

    iget-object v9, v0, Lcom/sec/android/cloudagent/ai;->f:Ljava/lang/String;

    move-object v2, v8

    move-object v8, p5

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v7, v0, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :cond_0
    return-object v7

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    move-object v8, v7

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    iget-object v0, p0, Lcom/sec/android/cloudagent/CloudProvider;->a:Lcom/sec/android/cloudagent/x;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/x;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/CloudProvider;->b:Lcom/sec/android/cloudagent/ag;

    invoke-virtual {v1, p1, p3}, Lcom/sec/android/cloudagent/ag;->b(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/cloudagent/ai;

    move-result-object v1

    iget-object v2, v1, Lcom/sec/android/cloudagent/ai;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/sec/android/cloudagent/ai;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, p2, v1, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    const/4 v0, 0x1

    if-lez v1, :cond_1

    const-string v2, "media_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "media_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "media_type"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/CloudProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/cloudagent/CloudProvider;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_1
    return v1
.end method
