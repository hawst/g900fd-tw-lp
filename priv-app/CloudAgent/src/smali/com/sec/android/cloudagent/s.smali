.class public Lcom/sec/android/cloudagent/s;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field private static b:Lcom/sec/android/cloudagent/s;


# instance fields
.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/cloudagent/s;->a:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/s;->b:Lcom/sec/android/cloudagent/s;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/s;->c:Landroid/content/Context;

    return-void
.end method

.method public static a()Lcom/sec/android/cloudagent/s;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/s;->b:Lcom/sec/android/cloudagent/s;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/s;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/s;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/s;->b:Lcom/sec/android/cloudagent/s;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/s;->b:Lcom/sec/android/cloudagent/s;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/s;->l()V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->a()Lcom/sec/android/cloudagent/f/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/f/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 3

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/cloudagent/e;->g:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/cloudagent/e;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->b(Ljava/io/File;)V

    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/.nomedia"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private j()V
    .locals 1

    const-string v0, "CloudManager : deleteDefaultFolders"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/e;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/c;->c()V

    return-void
.end method

.method private k()V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->f()V

    return-void
.end method

.method private l()V
    .locals 3

    const-string v0, "Network state is changed : need to send RESUME request.."

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/cloudagent/g/a;->a()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " thumbnails are recovered."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->b()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cached files are recovered."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v0, "doc"

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/s;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/cloudagent/a/a;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "documents are recovered"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->d()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "doc"

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/s;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->j()V

    :cond_2
    return-void

    :cond_3
    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->b()V

    const-string v0, "photo"

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/s;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->j()V

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->b()V

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/net/Uri;)Lcom/sec/android/cloudagent/adt/c;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v1, v2, :cond_1

    invoke-static {v0}, Lcom/sec/android/cloudagent/b/a;->a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/sec/android/cloudagent/g/a;->a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSyncSettingChanged fileType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->b()V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->c()V

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/c/l;->h()Lcom/sec/android/cloudagent/c/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/l;->i()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/s;->k()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/s;->d()V

    return-void

    :sswitch_0
    if-nez p2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->i()V

    goto :goto_0

    :sswitch_1
    if-eqz p2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->i()V

    goto :goto_0

    :sswitch_2
    if-nez p2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->i()V

    goto :goto_0

    :sswitch_3
    if-eqz p2, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->i()V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->b()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x80 -> :sswitch_2
        0x100 -> :sswitch_1
        0x800 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/cloudagent/s;->c:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ui/g;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    return-void
.end method

.method public a(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/cloudagent/e/o;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-direct {v2, p1, v0, v1}, Lcom/sec/android/cloudagent/e/o;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/e/c;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/cloudagent/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    new-instance v1, Lcom/sec/android/cloudagent/e/u;

    invoke-direct {v1, p1, p2, v0}, Lcom/sec/android/cloudagent/e/u;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;)V

    invoke-static {}, Lcom/sec/android/cloudagent/c/l;->h()Lcom/sec/android/cloudagent/c/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/c/l;->b(Lcom/sec/android/cloudagent/e/m;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1, p2, p3}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/cloudagent/e/n;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    invoke-direct {v2, p1, v0, v1}, Lcom/sec/android/cloudagent/e/n;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/sec/android/cloudagent/a/a;->b(Lcom/sec/android/cloudagent/e/c;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/cloudagent/s;->a:Z

    const-string v0, "CloudManager : requestDeInit"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->b()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/l;->h()Lcom/sec/android/cloudagent/c/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/l;->i()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/e;->h()Lcom/sec/android/cloudagent/c/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/e;->i()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/h;->h()Lcom/sec/android/cloudagent/c/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/h;->i()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/s;->k()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/s;->j()V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ae;->b()V

    return v1
.end method

.method public b(Landroid/content/Context;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/auth/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/s;->i()V

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/d/e;->a()V

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/s;->a(Landroid/content/Context;)V

    return v1
.end method

.method public c()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/s;->c:Landroid/content/Context;

    return-object v0
.end method

.method public c(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/cloudagent/e/r;

    invoke-direct {v1}, Lcom/sec/android/cloudagent/e/r;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/e/r;)V

    return-void
.end method

.method public d(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 2

    new-instance v0, Lcom/sec/android/cloudagent/e/p;

    invoke-direct {v0, p1}, Lcom/sec/android/cloudagent/e/p;-><init>(Lcom/sec/android/cloudagent/adt/c;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/a/a;->a(Lcom/sec/android/cloudagent/e/m;Z)V

    return-void
.end method

.method public e()V
    .locals 2

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/cloudagent/e/k;

    invoke-direct {v1}, Lcom/sec/android/cloudagent/e/k;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/sync/SyncManager;->a(Lcom/sec/android/cloudagent/e/r;)V

    return-void
.end method

.method public e(Lcom/sec/android/cloudagent/adt/c;)V
    .locals 2

    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->d(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->i(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/sec/android/cloudagent/ak;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    throw v0

    :cond_2
    const-string v0, "CloudSDK.getDownloadURL() = null"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/cloudagent/ae;->j(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->VIDEO:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v2, v1, :cond_1

    const-string v0, "API call : requesting streaming URL for video"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->c(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v2, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v2, v1, :cond_0

    const-string v0, "API call : requesting streaming URL for music"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->f(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/cloudagent/t;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/t;-><init>(Lcom/sec/android/cloudagent/s;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public g(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    :try_start_0
    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/cloudagent/ae;->l(Lcom/sec/android/cloudagent/adt/c;)Lcom/sec/android/cloudagent/adt/a;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudRecordNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/cloudagent/adt/a;->h()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;->MUSIC:Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    if-ne v1, v0, :cond_1

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "API call : requesting download URL for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->d(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public g()V
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/push/a;->a()Lcom/sec/android/cloudagent/push/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/push/a;->b()V

    return-void
.end method

.method public h(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/cloudagent/y;->e(Lcom/sec/android/cloudagent/adt/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public h()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/cloudagent/u;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/u;-><init>(Lcom/sec/android/cloudagent/s;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
