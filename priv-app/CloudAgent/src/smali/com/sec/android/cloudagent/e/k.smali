.class public Lcom/sec/android/cloudagent/e/k;
.super Lcom/sec/android/cloudagent/e/r;


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/cloudagent/e/r;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/cloudagent/e/k;->a:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/cloudagent/e/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/r;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 4

    iget-wide v0, p0, Lcom/sec/android/cloudagent/e/k;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/cloudagent/e/k;->a:J

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
