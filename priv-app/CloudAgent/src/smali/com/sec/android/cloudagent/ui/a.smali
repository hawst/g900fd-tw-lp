.class public Lcom/sec/android/cloudagent/ui/a;
.super Landroid/app/DialogFragment;


# static fields
.field static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/cloudagent/ui/a;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/sec/android/cloudagent/ui/a;
    .locals 3

    new-instance v0, Lcom/sec/android/cloudagent/ui/a;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ui/a;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "DialogID"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ui/a;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private b(I)Landroid/app/AlertDialog$Builder;
    .locals 6

    const/4 v0, 0x0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ui/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    move-object v1, v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/cloudagent/ui/f;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/ui/f;-><init>(Lcom/sec/android/cloudagent/ui/a;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    return-object v2

    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ui/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040005

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ui/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f040006

    invoke-virtual {v0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const v3, 0x7f040007

    invoke-virtual {p0, v3}, Lcom/sec/android/cloudagent/ui/a;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lcom/sec/android/cloudagent/ui/c;

    invoke-direct {v4, p0}, Lcom/sec/android/cloudagent/ui/c;-><init>(Lcom/sec/android/cloudagent/ui/a;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f040008

    invoke-virtual {p0, v4}, Lcom/sec/android/cloudagent/ui/a;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v5, Lcom/sec/android/cloudagent/ui/b;

    invoke-direct {v5, p0}, Lcom/sec/android/cloudagent/ui/b;-><init>(Lcom/sec/android/cloudagent/ui/a;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ui/a;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040009

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const v0, 0x7f04000a

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/ui/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x7f040014

    invoke-virtual {p0, v5}, Lcom/sec/android/cloudagent/ui/a;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f04000b

    invoke-virtual {p0, v3}, Lcom/sec/android/cloudagent/ui/a;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v4, Lcom/sec/android/cloudagent/ui/e;

    invoke-direct {v4, p0}, Lcom/sec/android/cloudagent/ui/e;-><init>(Lcom/sec/android/cloudagent/ui/a;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f04000c

    invoke-virtual {p0, v4}, Lcom/sec/android/cloudagent/ui/a;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v5, Lcom/sec/android/cloudagent/ui/d;

    invoke-direct {v5, p0}, Lcom/sec/android/cloudagent/ui/d;-><init>(Lcom/sec/android/cloudagent/ui/a;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4e20
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/ui/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DialogID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/ui/a;->b(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/cloudagent/ui/a;->a:Z

    return-object v0
.end method
