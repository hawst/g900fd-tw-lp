.class public Lcom/sec/android/cloudagent/e/a;
.super Lcom/sec/android/cloudagent/e/c;


# instance fields
.field a:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/cloudagent/e/c;-><init>(Lcom/sec/android/cloudagent/adt/c;Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/sec/android/cloudagent/e/a;->a:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/c;->a()V

    const-string v0, "onSuccess:BlockingCacheRequest"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/a;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/c;->b()V

    const-string v0, "onFailure:BlockingCacheRequest"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/e/a;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/cloudagent/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Lcom/sec/android/cloudagent/e/c;->hashCode()I

    move-result v0

    return v0
.end method
