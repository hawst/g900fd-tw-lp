.class public Lcom/sec/android/cloudagent/c/l;
.super Lcom/sec/android/cloudagent/c/d;


# static fields
.field private static b:Lcom/sec/android/cloudagent/c/l;


# instance fields
.field private a:Lcom/sec/android/cloudagent/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/c/l;->b:Lcom/sec/android/cloudagent/c/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0, v0}, Lcom/sec/android/cloudagent/c/d;-><init>(IIZ)V

    invoke-static {}, Lcom/sec/android/cloudagent/d/f;->a()Lcom/sec/android/cloudagent/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/c/l;->a:Lcom/sec/android/cloudagent/d/b;

    return-void
.end method

.method public static h()Lcom/sec/android/cloudagent/c/l;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/c/l;->b:Lcom/sec/android/cloudagent/c/l;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/c/l;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/c/l;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/c/l;->b:Lcom/sec/android/cloudagent/c/l;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/c/l;->b:Lcom/sec/android/cloudagent/c/l;

    return-object v0
.end method

.method private static k()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/c/l;->b:Lcom/sec/android/cloudagent/c/l;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/l;->a:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v0}, Lcom/sec/android/cloudagent/d/b;->b()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/android/cloudagent/e/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/c/l;->a:Lcom/sec/android/cloudagent/d/b;

    invoke-interface {v0, p1}, Lcom/sec/android/cloudagent/d/b;->a(Lcom/sec/android/cloudagent/e/m;)Z

    move-result v0

    return v0
.end method

.method public e(Lcom/sec/android/cloudagent/e/m;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/sec/android/cloudagent/c/l;->f(Lcom/sec/android/cloudagent/e/m;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadExecutor : Put!! ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object v0, p1

    check-cast v0, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/u;->f()Lcom/sec/android/cloudagent/adt/CloudObject$CLOUD_FILE_TYPE;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public f(Lcom/sec/android/cloudagent/e/m;)V
    .locals 4

    move-object v0, p1

    check-cast v0, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/u;->d()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lcom/sec/android/cloudagent/e/u;

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/e/u;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/cloudagent/ae;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertUploadDB("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") into "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "cloud_localpath"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cloud_serverpath"

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/cloudagent/ae;->c(Landroid/content/ContentValues;)Z

    :cond_0
    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/l;->j()V

    invoke-super {p0}, Lcom/sec/android/cloudagent/c/d;->c()V

    invoke-static {}, Lcom/sec/android/cloudagent/c/l;->k()V

    return-void
.end method

.method public j()V
    .locals 3

    const-string v0, "cancelCurrentUploadTasks!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/c/l;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/sec/android/cloudagent/c/g;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/g;->a()Lcom/sec/android/cloudagent/e/m;

    move-result-object v0

    instance-of v2, v0, Lcom/sec/android/cloudagent/e/e;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/sec/android/cloudagent/e/e;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/e/e;->c()Lcom/sec/android/cloudagent/c/b;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "CacheRequest for is cancelable"

    invoke-static {v2}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/c/b;->a()V

    goto :goto_0

    :cond_1
    return-void
.end method
