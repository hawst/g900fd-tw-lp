.class public Lcom/sec/android/cloudagent/detector/c;
.super Landroid/database/ContentObserver;


# static fields
.field private static a:Lcom/sec/android/cloudagent/detector/c;

.field private static b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    sput-object v0, Lcom/sec/android/cloudagent/detector/c;->b:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    sput-object p2, Lcom/sec/android/cloudagent/detector/c;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/android/cloudagent/detector/c;
    .locals 2

    sget-object v0, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/detector/c;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, p0}, Lcom/sec/android/cloudagent/detector/c;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    const-string v0, "register smart observer"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/detector/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/c;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public b()V
    .locals 2

    const-string v0, "unregister smart observer"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/cloudagent/detector/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/detector/c;->a:Lcom/sec/android/cloudagent/detector/c;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SmartMediaObserver:onChange!!Uri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method
