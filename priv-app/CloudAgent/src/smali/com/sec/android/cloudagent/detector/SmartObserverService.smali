.class public Lcom/sec/android/cloudagent/detector/SmartObserverService;
.super Landroid/app/Service;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/cloudagent/detector/d;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/detector/d;-><init>(Lcom/sec/android/cloudagent/detector/SmartObserverService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "lastMSRunTime"

    const-string v1, "lastMSRunTime"

    invoke-static {v0, v1, p1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/c;->a()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private c()Z
    .locals 6

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    const-string v0, "lastMSRunTime"

    const-string v1, "lastMSRunTime"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "SmartObserver:onCreate!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/c;->a(Landroid/content/Context;)Lcom/sec/android/cloudagent/detector/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/c;->a()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->a()V

    invoke-direct {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->b()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "SmartObserverService:onDestroy!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/detector/SmartObserverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/c;->a(Landroid/content/Context;)Lcom/sec/android/cloudagent/detector/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/detector/c;->b()V

    invoke-static {}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a()Lcom/sec/android/cloudagent/detector/SmartDetector;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/detector/SmartDetector$State;->IDLE:Lcom/sec/android/cloudagent/detector/SmartDetector$State;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/detector/SmartDetector;->a(Lcom/sec/android/cloudagent/detector/SmartDetector$State;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/cloudagent/detector/SmartDetector;->b(Z)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const-string v0, "SmartObserverService:onStartCommand"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
