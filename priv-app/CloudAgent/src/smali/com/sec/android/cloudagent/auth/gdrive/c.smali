.class public Lcom/sec/android/cloudagent/auth/gdrive/c;
.super Lcom/sec/android/cloudagent/auth/b;


# instance fields
.field private b:Lcom/sec/android/cloudagent/auth/a;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/auth/b;-><init>(I)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->b:Lcom/sec/android/cloudagent/auth/a;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/auth/gdrive/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->j()V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "result"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :try_start_0
    const-string v0, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "storageVender"

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->b:Lcom/sec/android/cloudagent/auth/a;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/auth/a;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_1
    :try_start_1
    const-string v0, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_OUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "storageVender"

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->b:Lcom/sec/android/cloudagent/auth/a;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/auth/a;->a()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, "result"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-eqz v1, :cond_0

    const-string v2, "com.google"

    invoke-virtual {v0, v2, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "googleCredential"

    invoke-static {v0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Z)V
    .locals 3

    if-eqz p3, :cond_2

    const-string v0, "googleCredential"

    const-string v1, "curAccountName"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "googleCredential"

    const-string v2, "curAccountName"

    invoke-static {v1, v2, p2}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/s;->b(Landroid/content/Context;)Z

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->d()V

    :goto_1
    const-string v0, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-direct {p0, v0, p3}, Lcom/sec/android/cloudagent/auth/gdrive/c;->a(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    return-void

    :cond_1
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/s;->b(Landroid/content/Context;)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->j()V

    const-string v0, "Sign in failed !"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 3

    invoke-virtual {p1}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    const-string v1, "server token expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-eqz v1, :cond_0

    const-string v2, "com.google"

    invoke-virtual {v0, v2, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "googleCredential"

    const-string v1, "accessToken"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->i()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    const-string v1, "server account info changed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/auth/c;)V
    .locals 1

    new-instance v0, Lcom/sec/android/cloudagent/auth/gdrive/d;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/cloudagent/auth/gdrive/d;-><init>(Lcom/sec/android/cloudagent/auth/gdrive/c;Lcom/sec/android/cloudagent/auth/c;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/gdrive/d;->start()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->e()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v3, "Package name is null!!!"

    invoke-static {v3}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.sec.android.cloudagent.dropbox.credential.name"

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.sec.android.cloudagent.dropbox.credential.tokenkey"

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->h()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->h()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()Lcom/sec/android/cloudagent/auth/a;
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    array-length v5, v2

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v2, v0

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "googleCredential"

    const-string v2, "curAccountName"

    invoke-static {v0, v2}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "googleCredential"

    const-string v5, "accessToken"

    invoke-static {v0, v5}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    new-instance v0, Lcom/sec/android/cloudagent/auth/a;

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/cloudagent/auth/a;-><init>(ILjava/lang/String;Ljava/util/HashSet;[Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public e()Z
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v5, v3

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v3, v2

    invoke-virtual {v4}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v7

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    goto :goto_1
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/gdrive/c;->c:Landroid/content/Context;

    const-class v2, Lcom/sec/android/cloudagent/auth/gdrive/GDriveAuthActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    return-object v0
.end method

.method public i()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/gdrive/c;->h()Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v1

    const v2, 0x7f040015

    const/high16 v3, 0x7f020000

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/cloudagent/ui/g;->a(ILandroid/content/Intent;I)V

    return-void
.end method
