.class public Lcom/sec/android/cloudagent/push/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/cloudagent/push/a;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/cloudagent/push/a;->a:Lcom/sec/android/cloudagent/push/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    const-string v1, "context should not be null"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/ak;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/cloudagent/push/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a()Lcom/sec/android/cloudagent/push/a;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/push/a;->a:Lcom/sec/android/cloudagent/push/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/push/a;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/push/a;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/push/a;->a:Lcom/sec/android/cloudagent/push/a;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/push/a;->a:Lcom/sec/android/cloudagent/push/a;

    return-object v0
.end method

.method private e()Z
    .locals 4

    const-string v0, "push_trigger"

    const-string v1, "triggeredTime"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/sec/android/cloudagent/e;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "Unregistering the device/app!!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/a/a/c;->b(Landroid/content/Context;)V

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 4

    invoke-static {}, Lcom/sec/android/cloudagent/sync/SyncManager;->a()Lcom/sec/android/cloudagent/sync/SyncManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/sync/SyncManager;->g()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0, p1}, Lcom/sec/android/cloudagent/y;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, "push_trigger"

    const-string v2, "deltaTrigger"

    invoke-static {v1, v2, v0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "push_trigger"

    const-string v1, "triggeredTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->d()V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/cloudagent/push/b;

    invoke-direct {v1, p0}, Lcom/sec/android/cloudagent/push/b;-><init>(Lcom/sec/android/cloudagent/push/a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public c()V
    .locals 4

    const-string v0, "Push registration start!!"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/a/a/c;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/a/a/c;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "No Reg id...Registering for first time"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "735665981150"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/a/a/c;->a(Landroid/content/Context;[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Device is already registered on GCM, check app server"

    invoke-static {v1}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/a/a/c;->h(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device is already registered on app server:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/push/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Could not register with app server.Unregistering with GCM also"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/cloudagent/push/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/a/a/c;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/push/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Handler for re-registering"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/push/a;->b()V

    :cond_0
    return-void
.end method
