.class public Lcom/sec/android/cloudagent/exception/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/samsung/scloud/exception/SCloudException;)Lcom/sec/android/cloudagent/exception/CloudException;
    .locals 2

    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudAuthException;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/d;->a()Lcom/sec/android/cloudagent/auth/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/cloudagent/auth/d;->a(ILcom/samsung/scloud/exception/SCloudException;)V

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudAuthException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudAuthException;-><init>()V

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/scloud/exception/SCloudException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/exception/CloudException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    invoke-virtual {p0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    return-object v0

    :cond_0
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudUnlinkedException;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudAuthException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudAuthException;-><init>()V

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudFileSizeException;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    goto :goto_0

    :cond_2
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudIOException;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-virtual {p0}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    goto :goto_0

    :cond_4
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudLocalStorageException;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudLocalStorageFullException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudLocalStorageFullException;-><init>()V

    goto :goto_0

    :cond_5
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    goto :goto_0

    :cond_6
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudParserException;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    goto :goto_0

    :cond_7
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudServerException;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    goto :goto_0

    :cond_8
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudSSLException;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudNetworkException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudNetworkException;-><init>()V

    goto :goto_0

    :cond_9
    instance-of v0, p0, Lcom/samsung/scloud/exception/SCloudCancelException;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudCancelException;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/exception/CloudCancelException;-><init>()V

    goto :goto_0

    :cond_a
    new-instance v0, Lcom/sec/android/cloudagent/exception/CloudException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
