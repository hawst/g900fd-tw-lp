.class public final Lcom/sec/android/cloudagent/o;
.super Ljava/lang/Object;


# direct methods
.method public static a()Ljava/lang/String;
    .locals 1

    const-string v0, "CREATE VIEW IF NOT EXISTS media_updated AS SELECT * FROM media_new WHERE EXISTS ( SELECT * FROM media_old WHERE media_old._id=media_new._id AND media_old._data=media_new._data AND media_old.date_modified!=media_new.date_modified);"

    return-object v0
.end method

.method public static b()Landroid/net/Uri;
    .locals 1

    const-string v0, "content://cloud/data/media_updated"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
