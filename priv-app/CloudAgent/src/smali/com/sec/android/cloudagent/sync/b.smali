.class public Lcom/sec/android/cloudagent/sync/b;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lcom/sec/android/cloudagent/e/j;Lcom/sec/android/cloudagent/adt/CloudObject;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->e()Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v1

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v1, v2, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/CloudObject;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/cloudagent/adt/CloudObject;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->f()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got Server Conflict in Doc operation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/cloudagent/ae;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got Client conflict in Doc operation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/sec/android/cloudagent/e/j;Lcom/sec/android/cloudagent/adt/CloudObject;)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/e/j;->e()Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->CREATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Lcom/sec/android/cloudagent/adt/CloudObject;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ui/g;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;->UPDATED:Lcom/sec/android/cloudagent/sync/DocumentSyncHandler$OPERATION_TYPE;

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/cloudagent/sync/DocumentSyncHandler;->a(Lcom/sec/android/cloudagent/adt/CloudObject;)V

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/ui/g;->e()V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/cloudagent/e;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/cloudagent/ae;->a(Ljava/lang/String;Z)I

    return-void
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/sec/android/cloudagent/ae;->a()Lcom/sec/android/cloudagent/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/cloudagent/ae;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
