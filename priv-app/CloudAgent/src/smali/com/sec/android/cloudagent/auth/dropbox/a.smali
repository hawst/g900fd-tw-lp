.class public Lcom/sec/android/cloudagent/auth/dropbox/a;
.super Lcom/sec/android/cloudagent/auth/b;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/cloudagent/auth/a;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/auth/b;-><init>(I)V

    invoke-static {}, Lcom/sec/android/cloudagent/s;->a()Lcom/sec/android/cloudagent/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/s;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->c:Lcom/sec/android/cloudagent/auth/a;

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->c:Lcom/sec/android/cloudagent/auth/a;

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/y;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "dropboxCredential"

    const-string v1, "dropboxTokenKey"

    invoke-static {v0, v1, p0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "dropboxCredential"

    const-string v1, "dropboxTokenSecret"

    invoke-static {v0, v1, p1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "storageVender"

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->c:Lcom/sec/android/cloudagent/auth/a;

    invoke-virtual {v2}, Lcom/sec/android/cloudagent/auth/a;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 2

    const-string v0, "dropboxCredential"

    const-string v1, "dropboxAccountName"

    invoke-static {v0, v1, p0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static h()Ljava/lang/String;
    .locals 2

    const-string v0, "dropboxCredential"

    const-string v1, "dropboxAccountName"

    invoke-static {v0, v1}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static i()[Ljava/lang/String;
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "dropboxCredential"

    const-string v3, "dropboxTokenKey"

    invoke-static {v2, v3}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dropboxCredential"

    const-string v3, "dropboxTokenSecret"

    invoke-static {v2, v3}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static j()V
    .locals 1

    const-string v0, "dropboxCredential"

    invoke-static {v0}, Lcom/sec/android/cloudagent/af;->a(Ljava/lang/String;)V

    return-void
.end method

.method private n()V
    .locals 0

    invoke-static {}, Lcom/sec/android/cloudagent/auth/dropbox/a;->j()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Z)V
    .locals 1

    if-nez p3, :cond_0

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->n()V

    const-string v0, "Sign in failed !"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    :cond_0
    const-string v0, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-direct {p0, v0, p3}, Lcom/sec/android/cloudagent/auth/dropbox/a;->a(Ljava/lang/String;Z)V

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    return-void
.end method

.method public a(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "server token expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v2, v2}, Lcom/sec/android/cloudagent/auth/dropbox/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->l()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "server account info changed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/cloudagent/auth/c;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->n()V

    new-instance v0, Lcom/sec/android/cloudagent/auth/dropbox/b;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/cloudagent/auth/dropbox/b;-><init>(Lcom/sec/android/cloudagent/auth/dropbox/a;Lcom/sec/android/cloudagent/auth/c;)V

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/dropbox/b;->start()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v3, "Package name is null!!!"

    invoke-static {v3}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "storageVender"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "com.sec.android.cloudagent.dropbox.credential.name"

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.sec.android.cloudagent.dropbox.credential.tokenkey"

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.sec.android.cloudagent.dropbox.credential.tokensecret"

    aget-object v1, v1, v4

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public b()V
    .locals 3

    const-string v0, "com.dropbox.android"

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v0

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ui/g;->a(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Samsung Apps is needed"

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "com.dropbox.android"

    invoke-virtual {p0, v0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v0

    const/16 v1, 0x4e22

    invoke-virtual {v0, v1}, Lcom/sec/android/cloudagent/ui/g;->a(I)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    const-class v2, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "requestSignIn"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    const-class v2, Lcom/sec/android/cloudagent/auth/dropbox/DropboxAuthActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "requestSignIn"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()Lcom/sec/android/cloudagent/auth/a;
    .locals 6

    invoke-static {}, Lcom/sec/android/cloudagent/auth/dropbox/a;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/auth/dropbox/a;->i()[Ljava/lang/String;

    move-result-object v4

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/android/cloudagent/auth/a;

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/cloudagent/auth/a;-><init>(ILjava/lang/String;Ljava/util/HashSet;[Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/cloudagent/auth/a;->d()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-boolean v0, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public f()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->d()Lcom/sec/android/cloudagent/auth/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/cloudagent/auth/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/auth/dropbox/a;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/cloudagent/f/d;->d()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.dropbox.android.account"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.cloudagent.DROPBOX_AUTH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "requestSignIn"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public l()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/cloudagent/auth/dropbox/a;->k()Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/ui/g;->a()Lcom/sec/android/cloudagent/ui/g;

    move-result-object v1

    const v2, 0x7f040014

    const/high16 v3, 0x7f020000

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/cloudagent/ui/g;->a(ILandroid/content/Intent;I)V

    return-void
.end method

.method public m()Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/cloudagent/auth/dropbox/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
