.class public Lcom/sec/android/cloudagent/ui/g;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/sec/android/cloudagent/ui/g;


# instance fields
.field private b:Landroid/app/NotificationManager;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    return-void
.end method

.method private a(Landroid/content/Context;IILandroid/content/Intent;I)Landroid/app/Notification$Builder;
    .locals 6

    const v2, 0x7f040019

    const/4 v3, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x2710

    if-ne p2, v1, :cond_2

    const v1, 0x7f040002

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v5, v5, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_0
    :goto_0
    invoke-virtual {v0, p5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    if-eqz p4, :cond_1

    const/high16 v1, 0x8000000

    invoke-static {p1, v5, p4, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_1
    return-object v0

    :cond_2
    const/16 v1, 0x2711

    if-ne p2, v1, :cond_3

    const v1, 0x7f040003

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f040004

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0

    :cond_3
    const/16 v1, 0x2712

    if-ne p2, v1, :cond_4

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f040017

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0

    :cond_4
    const/16 v1, 0x2713

    if-ne p2, v1, :cond_0

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f040018

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method public static a()Lcom/sec/android/cloudagent/ui/g;
    .locals 1

    sget-object v0, Lcom/sec/android/cloudagent/ui/g;->a:Lcom/sec/android/cloudagent/ui/g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/cloudagent/ui/g;

    invoke-direct {v0}, Lcom/sec/android/cloudagent/ui/g;-><init>()V

    sput-object v0, Lcom/sec/android/cloudagent/ui/g;->a:Lcom/sec/android/cloudagent/ui/g;

    :cond_0
    sget-object v0, Lcom/sec/android/cloudagent/ui/g;->a:Lcom/sec/android/cloudagent/ui/g;

    return-object v0
.end method

.method private a(IILandroid/content/Intent;I)V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/cloudagent/ui/g;->c:Landroid/content/Context;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/cloudagent/ui/g;->a(Landroid/content/Context;IILandroid/content/Intent;I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showDialog() dialogID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/cloudagent/d;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.cloudagent"

    const-string v2, "com.sec.android.cloudagent.ui.CloudUIActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DialogID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x50000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/cloudagent/ui/g;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public a(ILandroid/content/Intent;I)V
    .locals 1

    const/16 v0, 0x2711

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/sec/android/cloudagent/ui/g;->a(IILandroid/content/Intent;I)V

    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/sec/android/cloudagent/ui/g;->b:Landroid/app/NotificationManager;

    iput-object p1, p0, Lcom/sec/android/cloudagent/ui/g;->c:Landroid/content/Context;

    return-void
.end method

.method public b()V
    .locals 4

    const/16 v0, 0x2710

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x7f020002

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/cloudagent/ui/g;->a(IILandroid/content/Intent;I)V

    return-void
.end method

.method public c()V
    .locals 1

    const/16 v0, 0x2710

    invoke-direct {p0, v0}, Lcom/sec/android/cloudagent/ui/g;->b(I)V

    return-void
.end method

.method public d()V
    .locals 4

    const/16 v0, 0x2712

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x7f020000

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/cloudagent/ui/g;->a(IILandroid/content/Intent;I)V

    return-void
.end method

.method public e()V
    .locals 4

    const/16 v0, 0x2713

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x7f020000

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/cloudagent/ui/g;->a(IILandroid/content/Intent;I)V

    return-void
.end method
