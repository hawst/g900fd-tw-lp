.class public final Lcom/google/protobuf/ap;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/ap;


# instance fields
.field private b:Ljava/util/List;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:J

.field private g:Z

.field private h:J

.field private i:Z

.field private j:D

.field private k:Z

.field private l:Lcom/google/protobuf/f;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/ap;

    invoke-direct {v0}, Lcom/google/protobuf/ap;-><init>()V

    sput-object v0, Lcom/google/protobuf/ap;->a:Lcom/google/protobuf/ap;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ap;->b:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/ap;->d:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/protobuf/ap;->f:J

    iput-wide v2, p0, Lcom/google/protobuf/ap;->h:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protobuf/ap;->j:D

    sget-object v0, Lcom/google/protobuf/f;->a:Lcom/google/protobuf/f;

    iput-object v0, p0, Lcom/google/protobuf/ap;->l:Lcom/google/protobuf/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ap;->m:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/ap;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/ap;D)D
    .locals 1

    iput-wide p1, p0, Lcom/google/protobuf/ap;->j:D

    return-wide p1
.end method

.method static synthetic a(Lcom/google/protobuf/ap;J)J
    .locals 1

    iput-wide p1, p0, Lcom/google/protobuf/ap;->f:J

    return-wide p1
.end method

.method public static a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ap;->q()Lcom/google/protobuf/aq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/ap;Lcom/google/protobuf/f;)Lcom/google/protobuf/f;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ap;->l:Lcom/google/protobuf/f;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ap;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ap;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ap;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ap;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ap;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ap;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protobuf/ap;J)J
    .locals 1

    iput-wide p1, p0, Lcom/google/protobuf/ap;->h:J

    return-wide p1
.end method

.method public static b()Lcom/google/protobuf/ap;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ap;->a:Lcom/google/protobuf/ap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ap;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ap;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ap;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ap;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/ap;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ap;->g:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protobuf/ap;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ap;->i:Z

    return p1
.end method

.method static synthetic e(Lcom/google/protobuf/ap;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ap;->k:Z

    return p1
.end method

.method public static q()Lcom/google/protobuf/aq;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/aq;->i()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public static final t_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->I()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/ap;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ar;

    invoke-virtual {v0}, Lcom/google/protobuf/ar;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->J()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/ap;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ap;->a:Lcom/google/protobuf/ap;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ap;->b:Ljava/util/List;

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ap;->c:Z

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ap;->d:Ljava/lang/String;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ap;->e:Z

    return v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protobuf/ap;->f:J

    return-wide v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ap;->g:Z

    return v0
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Lcom/google/protobuf/ap;->h:J

    return-wide v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ap;->i:Z

    return v0
.end method

.method public n()D
    .locals 2

    iget-wide v0, p0, Lcom/google/protobuf/ap;->j:D

    return-wide v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ap;->k:Z

    return v0
.end method

.method public p()Lcom/google/protobuf/f;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ap;->l:Lcom/google/protobuf/f;

    return-object v0
.end method

.method public r()Lcom/google/protobuf/aq;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ap;->q()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/google/protobuf/aq;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ap;->r()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ap;->c()Lcom/google/protobuf/ap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ap;->s()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method
