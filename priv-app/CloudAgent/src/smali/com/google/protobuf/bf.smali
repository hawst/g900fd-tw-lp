.class public final Lcom/google/protobuf/bf;
.super Lcom/google/protobuf/a;


# instance fields
.field private final a:Lcom/google/protobuf/au;

.field private final b:Lcom/google/protobuf/bm;

.field private final c:Lcom/google/protobuf/cl;

.field private d:I


# direct methods
.method private constructor <init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/a;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/bf;->d:I

    iput-object p1, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    iput-object p2, p0, Lcom/google/protobuf/bf;->b:Lcom/google/protobuf/bm;

    iput-object p3, p0, Lcom/google/protobuf/bf;->c:Lcom/google/protobuf/cl;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;Lcom/google/protobuf/bg;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/protobuf/bf;-><init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/bf;)Lcom/google/protobuf/au;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    return-object v0
.end method

.method public static a(Lcom/google/protobuf/au;)Lcom/google/protobuf/bf;
    .locals 3

    new-instance v0, Lcom/google/protobuf/bf;

    invoke-static {}, Lcom/google/protobuf/bm;->b()Lcom/google/protobuf/bm;

    move-result-object v1

    invoke-static {}, Lcom/google/protobuf/cl;->c()Lcom/google/protobuf/cl;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/protobuf/bf;-><init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/google/protobuf/bf;->b(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/protobuf/bf;)Lcom/google/protobuf/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->b:Lcom/google/protobuf/bm;

    return-object v0
.end method

.method private b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->r()Lcom/google/protobuf/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private static b(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/au;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bo;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/bm;->e()Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/protobuf/bf;)Lcom/google/protobuf/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->c:Lcom/google/protobuf/cl;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/protobuf/bf;->b:Lcom/google/protobuf/bm;

    invoke-static {v0, v1}, Lcom/google/protobuf/bf;->b(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/protobuf/bf;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bf;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bo;)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/google/protobuf/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/bf;->a(Lcom/google/protobuf/au;)Lcom/google/protobuf/bf;

    move-result-object v0

    return-object v0
.end method

.method public c_()Lcom/google/protobuf/au;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    return-object v0
.end method

.method public d()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0}, Lcom/google/protobuf/bm;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/protobuf/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bf;->c:Lcom/google/protobuf/cl;

    return-object v0
.end method

.method public f()Lcom/google/protobuf/bh;
    .locals 3

    new-instance v0, Lcom/google/protobuf/bh;

    iget-object v1, p0, Lcom/google/protobuf/bf;->a:Lcom/google/protobuf/au;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/bh;-><init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bg;)V

    return-object v0
.end method

.method public g()Lcom/google/protobuf/bh;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bf;->f()Lcom/google/protobuf/bh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bf;->f()Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bf;->c()Lcom/google/protobuf/bf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bf;->g()Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method
