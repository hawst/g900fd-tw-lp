.class public final Lcom/google/protobuf/aj;
.super Lcom/google/protobuf/bs;


# static fields
.field private static final a:Lcom/google/protobuf/aj;


# instance fields
.field private b:Ljava/util/List;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/aj;

    invoke-direct {v0}, Lcom/google/protobuf/aj;-><init>()V

    sput-object v0, Lcom/google/protobuf/aj;->a:Lcom/google/protobuf/aj;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bs;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/aj;->b:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/aj;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/aj;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/aj;)Lcom/google/protobuf/ak;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/aj;->g()Lcom/google/protobuf/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/ak;->a(Lcom/google/protobuf/aj;)Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/aj;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/aj;->b:Ljava/util/List;

    return-object p1
.end method

.method public static b()Lcom/google/protobuf/aj;
    .locals 1

    sget-object v0, Lcom/google/protobuf/aj;->a:Lcom/google/protobuf/aj;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/aj;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/aj;->b:Ljava/util/List;

    return-object v0
.end method

.method public static g()Lcom/google/protobuf/ak;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ak;->o()Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method

.method public static final q_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->G()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/aj;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ap;

    invoke-virtual {v0}, Lcom/google/protobuf/ap;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/aj;->r()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->H()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/aj;
    .locals 1

    sget-object v0, Lcom/google/protobuf/aj;->a:Lcom/google/protobuf/aj;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/aj;->b:Ljava/util/List;

    return-object v0
.end method

.method public h()Lcom/google/protobuf/ak;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/aj;->g()Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/google/protobuf/ak;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/aj;->a(Lcom/google/protobuf/aj;)Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aj;->h()Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aj;->c()Lcom/google/protobuf/aj;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aj;->i()Lcom/google/protobuf/ak;

    move-result-object v0

    return-object v0
.end method
