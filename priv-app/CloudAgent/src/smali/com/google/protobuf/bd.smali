.class public final Lcom/google/protobuf/bd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bc;


# instance fields
.field private final a:I

.field private b:Lcom/google/protobuf/ah;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/protobuf/ba;

.field private final e:Lcom/google/protobuf/be;

.field private f:Lcom/google/protobuf/au;

.field private g:Lcom/google/protobuf/au;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/ah;Lcom/google/protobuf/ba;Lcom/google/protobuf/be;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/protobuf/bd;->a:I

    iput-object p1, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    iput-object p2, p0, Lcom/google/protobuf/bd;->d:Lcom/google/protobuf/ba;

    iput-object p3, p0, Lcom/google/protobuf/bd;->e:Lcom/google/protobuf/be;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/google/protobuf/be;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/protobuf/ah;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bd;->c:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/bc;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/ah;Lcom/google/protobuf/ba;Lcom/google/protobuf/be;ILcom/google/protobuf/at;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/bd;-><init>(Lcom/google/protobuf/ah;Lcom/google/protobuf/ba;Lcom/google/protobuf/be;I)V

    return-void
.end method

.method private a(Lcom/google/protobuf/ah;)V
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/bd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bd;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/bd;Lcom/google/protobuf/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/bd;->a(Lcom/google/protobuf/ah;)V

    return-void
.end method

.method private e()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x22

    iget-object v0, p0, Lcom/google/protobuf/bd;->d:Lcom/google/protobuf/ba;

    invoke-static {v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    invoke-virtual {v1}, Lcom/google/protobuf/ah;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/protobuf/av;->a(Ljava/lang/String;Lcom/google/protobuf/bc;)Lcom/google/protobuf/bc;

    move-result-object v0

    instance-of v1, v0, Lcom/google/protobuf/au;

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/Descriptors$DescriptorValidationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    invoke-virtual {v2}, Lcom/google/protobuf/ah;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/google/protobuf/Descriptors$DescriptorValidationException;-><init>(Lcom/google/protobuf/bc;Ljava/lang/String;Lcom/google/protobuf/at;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/au;

    iput-object v0, p0, Lcom/google/protobuf/bd;->f:Lcom/google/protobuf/au;

    iget-object v0, p0, Lcom/google/protobuf/bd;->d:Lcom/google/protobuf/ba;

    invoke-static {v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    invoke-virtual {v1}, Lcom/google/protobuf/ah;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/protobuf/av;->a(Ljava/lang/String;Lcom/google/protobuf/bc;)Lcom/google/protobuf/bc;

    move-result-object v0

    instance-of v1, v0, Lcom/google/protobuf/au;

    if-nez v1, :cond_1

    new-instance v0, Lcom/google/protobuf/Descriptors$DescriptorValidationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    invoke-virtual {v2}, Lcom/google/protobuf/ah;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/google/protobuf/Descriptors$DescriptorValidationException;-><init>(Lcom/google/protobuf/bc;Ljava/lang/String;Lcom/google/protobuf/at;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/protobuf/au;

    iput-object v0, p0, Lcom/google/protobuf/bd;->g:Lcom/google/protobuf/au;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/protobuf/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bd;->b:Lcom/google/protobuf/ah;

    invoke-virtual {v0}, Lcom/google/protobuf/ah;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bd;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bd;->d:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method public synthetic i()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bd;->a()Lcom/google/protobuf/ah;

    move-result-object v0

    return-object v0
.end method
