.class public final Lcom/google/protobuf/DescriptorProtos$FieldOptions;
.super Lcom/google/protobuf/bs;


# static fields
.field private static final a:Lcom/google/protobuf/DescriptorProtos$FieldOptions;


# instance fields
.field private b:Z

.field private c:Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/util/List;

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;-><init>()V

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->a:Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/bs;-><init>()V

    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;->CORD:Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->c:Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;

    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->e:Z

    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->g:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->i:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->j:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->k:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;)Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->c:Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;

    return-object p1
.end method

.method public static a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;)Lcom/google/protobuf/x;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->o()Lcom/google/protobuf/x;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/x;->a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;)Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->j:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->a:Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/DescriptorProtos$FieldOptions;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->e:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->f:Z

    return p1
.end method

.method static synthetic e(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->g:Z

    return p1
.end method

.method static synthetic f(Lcom/google/protobuf/DescriptorProtos$FieldOptions;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->h:Z

    return p1
.end method

.method public static final k_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->y()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static o()Lcom/google/protobuf/x;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/x;->o()Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ap;

    invoke-virtual {v0}, Lcom/google/protobuf/ap;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->r()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->z()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->a:Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->b:Z

    return v0
.end method

.method public g()Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->c:Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->d:Z

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->e:Z

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->f:Z

    return v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->g:Z

    return v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->h:Z

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->i:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->j:Ljava/util/List;

    return-object v0
.end method

.method public p()Lcom/google/protobuf/x;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->o()Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/google/protobuf/x;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->a(Lcom/google/protobuf/DescriptorProtos$FieldOptions;)Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->p()Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->c()Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->q()Lcom/google/protobuf/x;

    move-result-object v0

    return-object v0
.end method
