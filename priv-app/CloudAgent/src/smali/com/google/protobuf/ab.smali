.class public final Lcom/google/protobuf/ab;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/ab;


# instance fields
.field private b:Ljava/util/List;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/ab;

    invoke-direct {v0}, Lcom/google/protobuf/ab;-><init>()V

    sput-object v0, Lcom/google/protobuf/ab;->a:Lcom/google/protobuf/ab;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ab;->b:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ab;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/ab;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/ab;)Lcom/google/protobuf/ac;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ab;->g()Lcom/google/protobuf/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/ac;->a(Lcom/google/protobuf/ab;)Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/ab;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ab;->b:Ljava/util/List;

    return-object p1
.end method

.method public static b()Lcom/google/protobuf/ab;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ab;->a:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ab;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ab;->b:Ljava/util/List;

    return-object v0
.end method

.method public static g()Lcom/google/protobuf/ac;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ac;->i()Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method public static final m_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->c()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/ab;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/z;

    invoke-virtual {v0}, Lcom/google/protobuf/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->d()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/ab;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ab;->a:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ab;->b:Ljava/util/List;

    return-object v0
.end method

.method public h()Lcom/google/protobuf/ac;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ab;->g()Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/google/protobuf/ac;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/ab;)Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ab;->h()Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ab;->c()Lcom/google/protobuf/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ab;->i()Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method
