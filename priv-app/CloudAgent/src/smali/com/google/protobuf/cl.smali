.class public final Lcom/google/protobuf/cl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/cg;


# static fields
.field private static final a:Lcom/google/protobuf/cl;


# instance fields
.field private b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protobuf/cl;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/cl;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/protobuf/cl;->a:Lcom/google/protobuf/cl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Lcom/google/protobuf/cm;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/cl;-><init>(Ljava/util/Map;)V

    return-void
.end method

.method public static a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/cl;->b()Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/google/protobuf/cn;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/cn;->c()Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/cl;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    return-object v0
.end method

.method public static c()Lcom/google/protobuf/cl;
    .locals 1

    sget-object v0, Lcom/google/protobuf/cl;->a:Lcom/google/protobuf/cl;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    return-object v0
.end method

.method public e()Lcom/google/protobuf/cn;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/cl;->b()Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/google/protobuf/cl;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    check-cast p1, Lcom/google/protobuf/cl;

    iget-object v2, p1, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/cl;->b:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/TextFormat;->a(Lcom/google/protobuf/cl;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/cl;->e()Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method
