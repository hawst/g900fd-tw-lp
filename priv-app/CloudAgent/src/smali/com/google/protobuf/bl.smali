.class public Lcom/google/protobuf/bl;
.super Ljava/lang/Object;


# static fields
.field private static final b:Lcom/google/protobuf/bl;


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protobuf/bl;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protobuf/bl;-><init>(Z)V

    sput-object v0, Lcom/google/protobuf/bl;->b:Lcom/google/protobuf/bl;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/bl;->a:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(Lcom/google/protobuf/bl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/protobuf/bl;->b:Lcom/google/protobuf/bl;

    if-ne p1, v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bl;->a:Ljava/util/Map;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/protobuf/bl;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bl;->a:Ljava/util/Map;

    goto :goto_0
.end method

.method private constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bl;->a:Ljava/util/Map;

    return-void
.end method

.method public static b()Lcom/google/protobuf/bl;
    .locals 1

    sget-object v0, Lcom/google/protobuf/bl;->b:Lcom/google/protobuf/bl;

    return-object v0
.end method
