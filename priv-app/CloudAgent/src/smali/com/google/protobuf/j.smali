.class public final Lcom/google/protobuf/j;
.super Lcom/google/protobuf/bq;


# instance fields
.field private a:Lcom/google/protobuf/i;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bq;-><init>()V

    return-void
.end method

.method static synthetic o()Lcom/google/protobuf/j;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/j;->p()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method private static p()Lcom/google/protobuf/j;
    .locals 3

    new-instance v0, Lcom/google/protobuf/j;

    invoke-direct {v0}, Lcom/google/protobuf/j;-><init>()V

    new-instance v1, Lcom/google/protobuf/i;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protobuf/i;-><init>(Lcom/google/protobuf/h;)V

    iput-object v1, v0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    return-object v0
.end method


# virtual methods
.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->d()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/j;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/j;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lcom/google/protobuf/af;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0, p1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Lcom/google/protobuf/af;)Lcom/google/protobuf/af;

    return-object p0
.end method

.method public a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/i;->b()Lcom/google/protobuf/i;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/i;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/i;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->a(Ljava/lang/String;)Lcom/google/protobuf/j;

    :cond_1
    invoke-static {p1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-static {p1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_5
    invoke-static {p1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_6
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_7
    invoke-static {p1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_8
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_9
    invoke-static {p1}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_b
    invoke-virtual {p1}, Lcom/google/protobuf/i;->q()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/i;->r()Lcom/google/protobuf/af;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/af;)Lcom/google/protobuf/j;

    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/i;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto/16 :goto_0
.end method

.method public a(Lcom/google/protobuf/k;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lcom/google/protobuf/m;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0, p1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->d()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/j;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b(Lcom/google/protobuf/af;)Lcom/google/protobuf/j;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->g(Lcom/google/protobuf/i;)Lcom/google/protobuf/af;

    move-result-object v0

    invoke-static {}, Lcom/google/protobuf/af;->b()Lcom/google/protobuf/af;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->g(Lcom/google/protobuf/i;)Lcom/google/protobuf/af;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/af;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ag;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ag;->h()Lcom/google/protobuf/af;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Lcom/google/protobuf/af;)Lcom/google/protobuf/af;

    :goto_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0, p1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Lcom/google/protobuf/af;)Lcom/google/protobuf/af;

    goto :goto_0
.end method

.method public b(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/j;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/j;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/google/protobuf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->d()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/j;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/j;->p()Lcom/google/protobuf/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/j;
    .locals 1

    instance-of v0, p1, Lcom/google/protobuf/i;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/i;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    goto :goto_0
.end method

.method public d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/j;
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/j;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->a(Ljava/lang/String;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->y()Lcom/google/protobuf/u;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/u;->h()Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protobuf/i;->s()Lcom/google/protobuf/j;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/j;->h()Lcom/google/protobuf/i;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/protobuf/m;->l()Lcom/google/protobuf/n;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/n;->h()Lcom/google/protobuf/m;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/m;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protobuf/k;->j()Lcom/google/protobuf/l;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/l;->h()Lcom/google/protobuf/k;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/k;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->y()Lcom/google/protobuf/u;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/u;->h()Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)Lcom/google/protobuf/j;

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/protobuf/af;->k()Lcom/google/protobuf/ag;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->j()Lcom/google/protobuf/af;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/ag;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/ag;->h()Lcom/google/protobuf/af;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/j;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/i;->a_()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/j;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/j;->h()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/protobuf/i;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->b(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->c(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->d(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_4
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v0}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    iget-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-static {v1}, Lcom/google/protobuf/i;->f(Lcom/google/protobuf/i;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/i;->e(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->q()Z

    move-result v0

    return v0
.end method

.method public j()Lcom/google/protobuf/af;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/j;->a:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->r()Lcom/google/protobuf/af;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->c()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->d()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->g()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/j;->g()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method
