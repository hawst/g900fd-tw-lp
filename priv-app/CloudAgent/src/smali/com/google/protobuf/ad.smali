.class public final Lcom/google/protobuf/ad;
.super Lcom/google/protobuf/br;


# instance fields
.field private a:Lcom/google/protobuf/DescriptorProtos$FileOptions;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/br;-><init>()V

    return-void
.end method

.method static synthetic o()Lcom/google/protobuf/ad;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ad;->p()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method private static p()Lcom/google/protobuf/ad;
    .locals 3

    new-instance v0, Lcom/google/protobuf/ad;

    invoke-direct {v0}, Lcom/google/protobuf/ad;-><init>()V

    new-instance v1, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protobuf/DescriptorProtos$FileOptions;-><init>(Lcom/google/protobuf/h;)V

    iput-object v1, v0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;)Lcom/google/protobuf/ad;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->e(Lcom/google/protobuf/DescriptorProtos$FileOptions;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0, p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;)Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;

    return-object p0
.end method

.method public a(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Lcom/google/protobuf/ad;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->a(Ljava/lang/String;)Lcom/google/protobuf/ad;

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->b(Ljava/lang/String;)Lcom/google/protobuf/ad;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->k()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->a(Z)Lcom/google/protobuf/ad;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->m()Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;)Lcom/google/protobuf/ad;

    :cond_4
    invoke-static {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Ljava/util/List;)Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/bs;)V

    invoke-virtual {p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/ad;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/protobuf/ad;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0, p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/google/protobuf/ad;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->c(Lcom/google/protobuf/DescriptorProtos$FileOptions;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0, p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->d(Lcom/google/protobuf/DescriptorProtos$FileOptions;Z)Z

    return-object p0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/ad;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/ad;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/protobuf/ad;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0, p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/ad;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/ad;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/ad;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ad;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/ad;->p()Lcom/google/protobuf/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/ad;
    .locals 1

    instance-of v0, p1, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Lcom/google/protobuf/ad;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/br;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    goto :goto_0
.end method

.method public d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ad;
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/ad;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/ad;->a(Ljava/lang/String;)Lcom/google/protobuf/ad;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/ad;->b(Ljava/lang/String;)Lcom/google/protobuf/ad;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/g;->n()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;->valueOf(I)Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;

    move-result-object v2

    if-nez v2, :cond_1

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Lcom/google/protobuf/cn;->a(II)Lcom/google/protobuf/cn;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;)Lcom/google/protobuf/ad;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/g;->j()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/ad;->a(Z)Lcom/google/protobuf/ad;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/protobuf/ap;->q()Lcom/google/protobuf/aq;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/aq;->h()Lcom/google/protobuf/ap;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/ad;->a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/ad;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x42 -> :sswitch_2
        0x48 -> :sswitch_3
        0x50 -> :sswitch_4
        0x1f3a -> :sswitch_5
    .end sparse-switch
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->n_()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/ad;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/ad;->h()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    iget-object v1, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-static {v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b(Lcom/google/protobuf/DescriptorProtos$FileOptions;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a(Lcom/google/protobuf/DescriptorProtos$FileOptions;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/ad;->a:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method

.method protected synthetic i()Lcom/google/protobuf/bs;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->c()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic j()Lcom/google/protobuf/br;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->c()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->d()Lcom/google/protobuf/ad;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->g()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ad;->g()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method
