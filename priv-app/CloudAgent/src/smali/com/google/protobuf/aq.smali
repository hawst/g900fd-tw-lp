.class public final Lcom/google/protobuf/aq;
.super Lcom/google/protobuf/bq;


# instance fields
.field private a:Lcom/google/protobuf/ap;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bq;-><init>()V

    return-void
.end method

.method static synthetic i()Lcom/google/protobuf/aq;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/aq;->j()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method private static j()Lcom/google/protobuf/aq;
    .locals 3

    new-instance v0, Lcom/google/protobuf/aq;

    invoke-direct {v0}, Lcom/google/protobuf/aq;-><init>()V

    new-instance v1, Lcom/google/protobuf/ap;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protobuf/ap;-><init>(Lcom/google/protobuf/h;)V

    iput-object v1, v0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    return-object v0
.end method


# virtual methods
.method public a(D)Lcom/google/protobuf/aq;
    .locals 3

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->d(Lcom/google/protobuf/ap;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0, p1, p2}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;D)D

    return-object p0
.end method

.method public a(J)Lcom/google/protobuf/aq;
    .locals 3

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0, p1, p2}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;J)J

    return-object p0
.end method

.method public a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/ap;->b()Lcom/google/protobuf/ap;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-static {p1}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/ap;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/aq;->a(Ljava/lang/String;)Lcom/google/protobuf/aq;

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/ap;->j()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protobuf/aq;->a(J)Lcom/google/protobuf/aq;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protobuf/ap;->l()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protobuf/aq;->b(J)Lcom/google/protobuf/aq;

    :cond_5
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/ap;->n()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/protobuf/aq;->a(D)Lcom/google/protobuf/aq;

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/ap;->p()Lcom/google/protobuf/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/f;)Lcom/google/protobuf/aq;

    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/ap;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/aq;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/ar;)Lcom/google/protobuf/aq;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lcom/google/protobuf/f;)Lcom/google/protobuf/aq;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->e(Lcom/google/protobuf/ap;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0, p1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Lcom/google/protobuf/f;)Lcom/google/protobuf/f;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/protobuf/aq;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0, p1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->d()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/aq;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/aq;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/google/protobuf/aq;
    .locals 3

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->c(Lcom/google/protobuf/ap;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0, p1, p2}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;J)J

    return-object p0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->d()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/aq;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/google/protobuf/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/aq;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/aq;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->d()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/aq;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/aq;->j()Lcom/google/protobuf/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/aq;
    .locals 1

    instance-of v0, p1, Lcom/google/protobuf/ap;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/ap;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/ap;)Lcom/google/protobuf/aq;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    goto :goto_0
.end method

.method public d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/aq;
    .locals 4

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/aq;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/aq;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/google/protobuf/ar;->j()Lcom/google/protobuf/as;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/as;->h()Lcom/google/protobuf/ar;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/ar;)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/aq;->a(Ljava/lang/String;)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/g;->e()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/protobuf/aq;->a(J)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/g;->f()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/protobuf/aq;->b(J)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/g;->c()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/protobuf/aq;->a(D)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/g;->l()Lcom/google/protobuf/f;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/aq;->a(Lcom/google/protobuf/f;)Lcom/google/protobuf/aq;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x31 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ap;->t_()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-virtual {v0}, Lcom/google/protobuf/ap;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/aq;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/aq;->h()Lcom/google/protobuf/ap;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/protobuf/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v0}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    iget-object v1, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    invoke-static {v1}, Lcom/google/protobuf/ap;->b(Lcom/google/protobuf/ap;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/ap;->a(Lcom/google/protobuf/ap;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/aq;->a:Lcom/google/protobuf/ap;

    return-object v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->c()Lcom/google/protobuf/ap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->d()Lcom/google/protobuf/aq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->g()Lcom/google/protobuf/ap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/aq;->g()Lcom/google/protobuf/ap;

    move-result-object v0

    return-object v0
.end method
