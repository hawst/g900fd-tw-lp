.class public final Lcom/google/protobuf/ba;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/google/protobuf/z;

.field private final b:[Lcom/google/protobuf/au;

.field private final c:[Lcom/google/protobuf/ay;

.field private final d:[Lcom/google/protobuf/be;

.field private final e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

.field private final f:[Lcom/google/protobuf/ba;

.field private final g:Lcom/google/protobuf/av;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/z;[Lcom/google/protobuf/ba;Lcom/google/protobuf/av;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/protobuf/ba;->g:Lcom/google/protobuf/av;

    iput-object p1, p0, Lcom/google/protobuf/ba;->a:Lcom/google/protobuf/z;

    invoke-virtual {p2}, [Lcom/google/protobuf/ba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protobuf/ba;

    iput-object v0, p0, Lcom/google/protobuf/ba;->f:[Lcom/google/protobuf/ba;

    invoke-virtual {p0}, Lcom/google/protobuf/ba;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0, p0}, Lcom/google/protobuf/av;->a(Ljava/lang/String;Lcom/google/protobuf/ba;)V

    invoke-virtual {p1}, Lcom/google/protobuf/z;->l()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/au;

    iput-object v0, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    move v4, v6

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/z;->l()I

    move-result v0

    if-ge v4, v0, :cond_0

    iget-object v7, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/au;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/z;->b(I)Lcom/google/protobuf/i;

    move-result-object v1

    move-object v2, p0

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/i;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;ILcom/google/protobuf/at;)V

    aput-object v0, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/z;->n()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/ay;

    iput-object v0, p0, Lcom/google/protobuf/ba;->c:[Lcom/google/protobuf/ay;

    move v4, v6

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/z;->n()I

    move-result v0

    if-ge v4, v0, :cond_1

    iget-object v7, p0, Lcom/google/protobuf/ba;->c:[Lcom/google/protobuf/ay;

    new-instance v0, Lcom/google/protobuf/ay;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/z;->c(I)Lcom/google/protobuf/m;

    move-result-object v1

    move-object v2, p0

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/ay;-><init>(Lcom/google/protobuf/m;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;ILcom/google/protobuf/at;)V

    aput-object v0, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/z;->p()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/be;

    iput-object v0, p0, Lcom/google/protobuf/ba;->d:[Lcom/google/protobuf/be;

    move v0, v6

    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/z;->p()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/protobuf/ba;->d:[Lcom/google/protobuf/be;

    new-instance v2, Lcom/google/protobuf/be;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/z;->d(I)Lcom/google/protobuf/al;

    move-result-object v4

    invoke-direct {v2, v4, p0, v0, v3}, Lcom/google/protobuf/be;-><init>(Lcom/google/protobuf/al;Lcom/google/protobuf/ba;ILcom/google/protobuf/at;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/z;->r()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/Descriptors$FieldDescriptor;

    iput-object v0, p0, Lcom/google/protobuf/ba;->e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    move v4, v6

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/z;->r()I

    move-result v0

    if-ge v4, v0, :cond_3

    iget-object v7, p0, Lcom/google/protobuf/ba;->e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    new-instance v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/z;->e(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v1

    const/4 v5, 0x1

    move-object v2, p0

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/Descriptors$FieldDescriptor;-><init>(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;IZLcom/google/protobuf/at;)V

    aput-object v0, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ba;->g:Lcom/google/protobuf/av;

    return-object v0
.end method

.method public static a(Lcom/google/protobuf/z;[Lcom/google/protobuf/ba;)Lcom/google/protobuf/ba;
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/google/protobuf/av;

    invoke-direct {v0, p1}, Lcom/google/protobuf/av;-><init>([Lcom/google/protobuf/ba;)V

    new-instance v1, Lcom/google/protobuf/ba;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/protobuf/ba;-><init>(Lcom/google/protobuf/z;[Lcom/google/protobuf/ba;Lcom/google/protobuf/av;)V

    array-length v0, p1

    invoke-virtual {p0}, Lcom/google/protobuf/z;->j()I

    move-result v2

    if-eq v0, v2, :cond_0

    new-instance v0, Lcom/google/protobuf/Descriptors$DescriptorValidationException;

    const-string v2, "Dependencies passed to FileDescriptor.buildFrom() don\'t match those listed in the FileDescriptorProto."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/protobuf/Descriptors$DescriptorValidationException;-><init>(Lcom/google/protobuf/ba;Ljava/lang/String;Lcom/google/protobuf/at;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/protobuf/z;->j()I

    move-result v2

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/protobuf/ba;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/protobuf/z;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Lcom/google/protobuf/Descriptors$DescriptorValidationException;

    const-string v2, "Dependencies passed to FileDescriptor.buildFrom() don\'t match those listed in the FileDescriptorProto."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/protobuf/Descriptors$DescriptorValidationException;-><init>(Lcom/google/protobuf/ba;Ljava/lang/String;Lcom/google/protobuf/at;)V

    throw v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {v1}, Lcom/google/protobuf/ba;->e()V

    return-object v1
.end method

.method private a(Lcom/google/protobuf/z;)V
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/protobuf/ba;->a:Lcom/google/protobuf/z;

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/z;->b(I)Lcom/google/protobuf/i;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/au;->a(Lcom/google/protobuf/au;Lcom/google/protobuf/i;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/protobuf/ba;->c:[Lcom/google/protobuf/ay;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/protobuf/ba;->c:[Lcom/google/protobuf/ay;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/z;->c(I)Lcom/google/protobuf/m;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/ay;->a(Lcom/google/protobuf/ay;Lcom/google/protobuf/m;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/protobuf/ba;->d:[Lcom/google/protobuf/be;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/protobuf/ba;->d:[Lcom/google/protobuf/be;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/z;->d(I)Lcom/google/protobuf/al;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/be;->a(Lcom/google/protobuf/be;Lcom/google/protobuf/al;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/protobuf/ba;->e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/protobuf/ba;->e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Lcom/google/protobuf/z;->e(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    return-void
.end method

.method public static a([Ljava/lang/String;[Lcom/google/protobuf/ba;Lcom/google/protobuf/bb;)V
    .locals 5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ISO-8859-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    invoke-static {v0}, Lcom/google/protobuf/z;->a([B)Lcom/google/protobuf/z;
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :try_start_2
    invoke-static {v1, p1}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/z;[Lcom/google/protobuf/ba;)Lcom/google/protobuf/ba;
    :try_end_2
    .catch Lcom/google/protobuf/Descriptors$DescriptorValidationException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/protobuf/bb;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/bi;

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_3
    invoke-static {v0, v2}, Lcom/google/protobuf/z;->a([BLcom/google/protobuf/bl;)Lcom/google/protobuf/z;
    :try_end_3
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/z;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Standard encoding ISO-8859-1 not supported by JVM."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Failed to parse protocol buffer descriptor for generated code."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid embedded descriptor for \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/protobuf/z;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\"."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Failed to parse protocol buffer descriptor for generated code."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private e()V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-static {v4}, Lcom/google/protobuf/au;->a(Lcom/google/protobuf/au;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/ba;->d:[Lcom/google/protobuf/be;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-static {v4}, Lcom/google/protobuf/be;->a(Lcom/google/protobuf/be;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/ba;->e:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Lcom/google/protobuf/z;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ba;->a:Lcom/google/protobuf/z;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ba;->a:Lcom/google/protobuf/z;

    invoke-virtual {v0}, Lcom/google/protobuf/z;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ba;->a:Lcom/google/protobuf/z;

    invoke-virtual {v0}, Lcom/google/protobuf/z;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ba;->b:[Lcom/google/protobuf/au;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
