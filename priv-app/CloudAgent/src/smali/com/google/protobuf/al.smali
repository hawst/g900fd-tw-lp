.class public final Lcom/google/protobuf/al;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/al;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:Z

.field private f:Lcom/google/protobuf/an;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/al;

    invoke-direct {v0}, Lcom/google/protobuf/al;-><init>()V

    sput-object v0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/al;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/al;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    invoke-static {}, Lcom/google/protobuf/an;->b()Lcom/google/protobuf/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/al;->f:Lcom/google/protobuf/an;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/al;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/al;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/al;->l()Lcom/google/protobuf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/al;Lcom/google/protobuf/an;)Lcom/google/protobuf/an;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/al;->f:Lcom/google/protobuf/an;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/al;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/al;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/al;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/al;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/al;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/al;
    .locals 1

    sget-object v0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/al;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/al;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/al;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/al;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/al;)Lcom/google/protobuf/an;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->f:Lcom/google/protobuf/an;

    return-object v0
.end method

.method public static l()Lcom/google/protobuf/am;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/am;->o()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public static final r_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->q()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/protobuf/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/al;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ah;

    invoke-virtual {v0}, Lcom/google/protobuf/ah;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/al;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/protobuf/al;->k()Lcom/google/protobuf/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/an;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->r()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/al;
    .locals 1

    sget-object v0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/al;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/al;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/al;->e:Z

    return v0
.end method

.method public k()Lcom/google/protobuf/an;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/al;->f:Lcom/google/protobuf/an;

    return-object v0
.end method

.method public m()Lcom/google/protobuf/am;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/al;->l()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/protobuf/am;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/al;->m()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/al;->c()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/al;->n()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method
