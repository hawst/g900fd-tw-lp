.class public final Lcom/google/protobuf/l;
.super Lcom/google/protobuf/bq;


# instance fields
.field private a:Lcom/google/protobuf/k;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bq;-><init>()V

    return-void
.end method

.method static synthetic i()Lcom/google/protobuf/l;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/l;->j()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method private static j()Lcom/google/protobuf/l;
    .locals 3

    new-instance v0, Lcom/google/protobuf/l;

    invoke-direct {v0}, Lcom/google/protobuf/l;-><init>()V

    new-instance v1, Lcom/google/protobuf/k;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protobuf/k;-><init>(Lcom/google/protobuf/h;)V

    iput-object v1, v0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    return-object v0
.end method


# virtual methods
.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->d()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/l;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/l;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/google/protobuf/l;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/k;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    invoke-static {v0, p1}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/k;I)I

    return-object p0
.end method

.method public a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/k;->b()Lcom/google/protobuf/k;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/k;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/k;->g()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/l;->a(I)Lcom/google/protobuf/l;

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/k;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/k;->i()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/l;->b(I)Lcom/google/protobuf/l;

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/k;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/l;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->d()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/l;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/google/protobuf/l;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/k;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    invoke-static {v0, p1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/k;I)I

    return-object p0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/l;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/l;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/google/protobuf/k;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->d()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/l;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/l;->j()Lcom/google/protobuf/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/l;->a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/l;
    .locals 1

    instance-of v0, p1, Lcom/google/protobuf/k;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/k;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/l;->a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    goto :goto_0
.end method

.method public d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/l;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/l;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/protobuf/l;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/l;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/l;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/g;->g()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/l;->a(I)Lcom/google/protobuf/l;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/g;->g()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/l;->b(I)Lcom/google/protobuf/l;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/k;->e_()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/k;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/l;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    invoke-static {v0}, Lcom/google/protobuf/l;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/l;->h()Lcom/google/protobuf/k;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/protobuf/k;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/l;->a:Lcom/google/protobuf/k;

    return-object v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->c()Lcom/google/protobuf/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->d()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->g()Lcom/google/protobuf/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/l;->g()Lcom/google/protobuf/k;

    move-result-object v0

    return-object v0
.end method
