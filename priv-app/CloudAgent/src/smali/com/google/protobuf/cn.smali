.class public final Lcom/google/protobuf/cn;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/ch;


# instance fields
.field private a:Ljava/util/Map;

.field private b:I

.field private c:Lcom/google/protobuf/cp;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(I)Lcom/google/protobuf/cp;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/protobuf/cn;->b:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/protobuf/cn;->b:I

    iget-object v1, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    invoke-virtual {v1}, Lcom/google/protobuf/cp;->a()Lcom/google/protobuf/co;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/protobuf/cn;->b(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;

    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/co;

    iput p1, p0, Lcom/google/protobuf/cn;->b:I

    invoke-static {}, Lcom/google/protobuf/co;->a()Lcom/google/protobuf/cp;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    invoke-virtual {v1, v0}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/co;)Lcom/google/protobuf/cp;

    :cond_3
    iget-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    goto :goto_0
.end method

.method static synthetic c()Lcom/google/protobuf/cn;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/cn;->d()Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method private static d()Lcom/google/protobuf/cn;
    .locals 1

    new-instance v0, Lcom/google/protobuf/cn;

    invoke-direct {v0}, Lcom/google/protobuf/cn;-><init>()V

    invoke-direct {v0}, Lcom/google/protobuf/cn;->e()V

    return-object v0
.end method

.method private e()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/cn;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/protobuf/cl;
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    iget-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/protobuf/cl;->c()Lcom/google/protobuf/cl;

    move-result-object v0

    :goto_0
    iput-object v2, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    return-object v0

    :cond_0
    new-instance v0, Lcom/google/protobuf/cl;

    iget-object v1, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/cl;-><init>(Ljava/util/Map;Lcom/google/protobuf/cm;)V

    goto :goto_0
.end method

.method public a(II)Lcom/google/protobuf/cn;
    .locals 4

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v0

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/protobuf/cp;->a(J)Lcom/google/protobuf/cp;

    return-object p0
.end method

.method public a(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/protobuf/cn;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/co;)Lcom/google/protobuf/cp;

    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/cn;->b(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;
    .locals 3

    invoke-static {}, Lcom/google/protobuf/cl;->c()Lcom/google/protobuf/cl;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-static {p1}, Lcom/google/protobuf/cl;->b(Lcom/google/protobuf/cl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/co;

    invoke-virtual {p0, v1, v0}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Lcom/google/protobuf/g;)Lcom/google/protobuf/cn;
    .locals 1

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0, p1}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/g;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    return-object p0
.end method

.method public a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/cn;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/g;)Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Z
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/protobuf/cn;->b:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILcom/google/protobuf/g;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v1

    invoke-static {p1}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/google/protobuf/InvalidProtocolBufferException;->invalidWireType()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :pswitch_0
    invoke-direct {p0, v1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/protobuf/g;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/protobuf/cp;->a(J)Lcom/google/protobuf/cp;

    :goto_0
    return v0

    :pswitch_1
    invoke-direct {p0, v1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/protobuf/g;->h()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/protobuf/cp;->b(J)Lcom/google/protobuf/cp;

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/protobuf/g;->l()Lcom/google/protobuf/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/f;)Lcom/google/protobuf/cp;

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/google/protobuf/cl;->b()Lcom/google/protobuf/cn;

    move-result-object v2

    invoke-static {}, Lcom/google/protobuf/bi;->a()Lcom/google/protobuf/bi;

    move-result-object v3

    invoke-virtual {p2, v1, v2, v3}, Lcom/google/protobuf/g;->a(ILcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-direct {p0, v1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cp;

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v1}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/protobuf/g;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/cp;->a(I)Lcom/google/protobuf/cp;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b()Lcom/google/protobuf/cn;
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/cn;->b(I)Lcom/google/protobuf/cp;

    invoke-static {}, Lcom/google/protobuf/cl;->b()Lcom/google/protobuf/cn;

    move-result-object v0

    new-instance v1, Lcom/google/protobuf/cl;

    iget-object v2, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/protobuf/cl;-><init>(Ljava/util/Map;Lcom/google/protobuf/cm;)V

    invoke-virtual {v0, v1}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public b(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/protobuf/cn;->b:I

    if-ne v0, p1, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/cn;->c:Lcom/google/protobuf/cp;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/cn;->b:I

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/cn;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/cn;->b()Lcom/google/protobuf/cn;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    return-object v0
.end method
