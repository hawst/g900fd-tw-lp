.class public final Lcom/google/protobuf/k;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/k;


# instance fields
.field private b:Z

.field private c:I

.field private d:Z

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/k;

    invoke-direct {v0}, Lcom/google/protobuf/k;-><init>()V

    sput-object v0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/k;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    iput v0, p0, Lcom/google/protobuf/k;->c:I

    iput v0, p0, Lcom/google/protobuf/k;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/k;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/k;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/k;I)I
    .locals 0

    iput p1, p0, Lcom/google/protobuf/k;->c:I

    return p1
.end method

.method public static a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/k;->j()Lcom/google/protobuf/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/l;->a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/k;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/k;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/google/protobuf/k;I)I
    .locals 0

    iput p1, p0, Lcom/google/protobuf/k;->e:I

    return p1
.end method

.method public static b()Lcom/google/protobuf/k;
    .locals 1

    sget-object v0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/k;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/k;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/k;->d:Z

    return p1
.end method

.method public static final e_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->i()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static j()Lcom/google/protobuf/l;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/l;->i()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->j()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/k;
    .locals 1

    sget-object v0, Lcom/google/protobuf/k;->a:Lcom/google/protobuf/k;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/k;->b:Z

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/k;->c:I

    return v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/k;->d:Z

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/k;->e:I

    return v0
.end method

.method public k()Lcom/google/protobuf/l;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/k;->j()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/google/protobuf/l;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/k;)Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/k;->k()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/k;->c()Lcom/google/protobuf/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/k;->l()Lcom/google/protobuf/l;

    move-result-object v0

    return-object v0
.end method
