.class public final Lcom/google/protobuf/ar;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/ar;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/ar;

    invoke-direct {v0}, Lcom/google/protobuf/ar;-><init>()V

    sput-object v0, Lcom/google/protobuf/ar;->a:Lcom/google/protobuf/ar;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/ar;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protobuf/ar;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ar;->f:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/ar;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/ar;)Lcom/google/protobuf/as;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ar;->j()Lcom/google/protobuf/as;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/as;->a(Lcom/google/protobuf/ar;)Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/ar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ar;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ar;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/ar;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ar;->a:Lcom/google/protobuf/ar;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ar;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/ar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ar;->e:Z

    return p1
.end method

.method public static j()Lcom/google/protobuf/as;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/as;->i()Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method

.method public static final u_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->K()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/protobuf/ar;->b:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/protobuf/ar;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->L()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/ar;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ar;->a:Lcom/google/protobuf/ar;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ar;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ar;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ar;->d:Z

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ar;->e:Z

    return v0
.end method

.method public k()Lcom/google/protobuf/as;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ar;->j()Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/google/protobuf/as;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/ar;->a(Lcom/google/protobuf/ar;)Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ar;->k()Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ar;->c()Lcom/google/protobuf/ar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ar;->l()Lcom/google/protobuf/as;

    move-result-object v0

    return-object v0
.end method
