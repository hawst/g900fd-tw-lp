.class public abstract Lcom/google/protobuf/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/ch;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract b()Lcom/google/protobuf/e;
.end method

.method public b(Lcom/google/protobuf/g;)Lcom/google/protobuf/e;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/e;->b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
.end method

.method public b([B)Lcom/google/protobuf/e;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/protobuf/e;->b([BII)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public b([BII)Lcom/google/protobuf/e;
    .locals 3

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/g;->a([BII)Lcom/google/protobuf/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/e;->b(Lcom/google/protobuf/g;)Lcom/google/protobuf/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/g;->a(I)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 3

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/g;->a([BII)Lcom/google/protobuf/g;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lcom/google/protobuf/e;->b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/g;->a(I)V
    :try_end_0
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b([BLcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/protobuf/e;->b([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/e;->b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/e;->b()Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method
