.class public final Lcom/google/protobuf/bi;
.super Lcom/google/protobuf/bl;


# static fields
.field private static final c:Lcom/google/protobuf/bi;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protobuf/bi;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/protobuf/bi;-><init>(Z)V

    sput-object v0, Lcom/google/protobuf/bi;->c:Lcom/google/protobuf/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bl;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/bi;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/bi;->b:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    invoke-static {}, Lcom/google/protobuf/bl;->b()Lcom/google/protobuf/bl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/bl;-><init>(Lcom/google/protobuf/bl;)V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bi;->a:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bi;->b:Ljava/util/Map;

    return-void
.end method

.method public static a()Lcom/google/protobuf/bi;
    .locals 1

    sget-object v0, Lcom/google/protobuf/bi;->c:Lcom/google/protobuf/bi;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/protobuf/au;I)Lcom/google/protobuf/bk;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/bi;->b:Ljava/util/Map;

    new-instance v1, Lcom/google/protobuf/bj;

    invoke-direct {v1, p1, p2}, Lcom/google/protobuf/bj;-><init>(Lcom/google/protobuf/au;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/bk;

    return-object v0
.end method
