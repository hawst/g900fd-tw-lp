.class public final Lcom/google/protobuf/be;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bc;


# instance fields
.field private final a:I

.field private b:Lcom/google/protobuf/al;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/protobuf/ba;

.field private e:[Lcom/google/protobuf/bd;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/al;Lcom/google/protobuf/ba;I)V
    .locals 7

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lcom/google/protobuf/be;->a:I

    iput-object p1, p0, Lcom/google/protobuf/be;->b:Lcom/google/protobuf/al;

    invoke-virtual {p1}, Lcom/google/protobuf/al;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v5, v0}, Lcom/google/protobuf/Descriptors;->a(Lcom/google/protobuf/ba;Lcom/google/protobuf/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/be;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/protobuf/be;->d:Lcom/google/protobuf/ba;

    invoke-virtual {p1}, Lcom/google/protobuf/al;->i()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/bd;

    iput-object v0, p0, Lcom/google/protobuf/be;->e:[Lcom/google/protobuf/bd;

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/al;->i()I

    move-result v0

    if-ge v4, v0, :cond_0

    iget-object v6, p0, Lcom/google/protobuf/be;->e:[Lcom/google/protobuf/bd;

    new-instance v0, Lcom/google/protobuf/bd;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/al;->a(I)Lcom/google/protobuf/ah;

    move-result-object v1

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/bd;-><init>(Lcom/google/protobuf/ah;Lcom/google/protobuf/ba;Lcom/google/protobuf/be;ILcom/google/protobuf/at;)V

    aput-object v0, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/bc;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/al;Lcom/google/protobuf/ba;ILcom/google/protobuf/at;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/protobuf/be;-><init>(Lcom/google/protobuf/al;Lcom/google/protobuf/ba;I)V

    return-void
.end method

.method private a(Lcom/google/protobuf/al;)V
    .locals 3

    iput-object p1, p0, Lcom/google/protobuf/be;->b:Lcom/google/protobuf/al;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/protobuf/be;->e:[Lcom/google/protobuf/bd;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/be;->e:[Lcom/google/protobuf/bd;

    aget-object v1, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/al;->a(I)Lcom/google/protobuf/ah;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/bd;->a(Lcom/google/protobuf/bd;Lcom/google/protobuf/ah;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/be;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/be;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/be;Lcom/google/protobuf/al;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/be;->a(Lcom/google/protobuf/al;)V

    return-void
.end method

.method private e()V
    .locals 4

    iget-object v1, p0, Lcom/google/protobuf/be;->e:[Lcom/google/protobuf/bd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/protobuf/bd;->a(Lcom/google/protobuf/bd;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/google/protobuf/al;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/be;->b:Lcom/google/protobuf/al;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/be;->b:Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/be;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/be;->d:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method public synthetic i()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/be;->a()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method
