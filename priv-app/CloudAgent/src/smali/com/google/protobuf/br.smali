.class public abstract Lcom/google/protobuf/br;
.super Lcom/google/protobuf/bq;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bq;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/br;->e(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->j()Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/protobuf/bs;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/br;->i()Lcom/google/protobuf/bs;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;)Lcom/google/protobuf/bm;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;)Lcom/google/protobuf/bm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bm;)V

    return-void
.end method

.method protected a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->i()Lcom/google/protobuf/bs;

    invoke-static {p1, p2, p3, p0, p4}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;Lcom/google/protobuf/cf;I)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/br;->f(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->j()Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/br;->f(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->j()Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/br;->e(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method

.method public e(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/br;->i()Lcom/google/protobuf/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    invoke-static {v0}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;)Lcom/google/protobuf/bm;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bo;Ljava/lang/Object;)V

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/br;

    move-object p0, v0

    goto :goto_0
.end method

.method public f(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/br;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/br;->i()Lcom/google/protobuf/bs;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    invoke-static {v0}, Lcom/google/protobuf/bs;->a(Lcom/google/protobuf/bs;)Lcom/google/protobuf/bm;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/bm;->b(Lcom/google/protobuf/bo;Ljava/lang/Object;)V

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/bq;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/br;

    move-object p0, v0

    goto :goto_0
.end method

.method protected abstract i()Lcom/google/protobuf/bs;
.end method

.method public j()Lcom/google/protobuf/br;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->i()Lcom/google/protobuf/bs;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/br;->j()Lcom/google/protobuf/br;

    move-result-object v0

    return-object v0
.end method
