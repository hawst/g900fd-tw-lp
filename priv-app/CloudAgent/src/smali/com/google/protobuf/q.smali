.class public final Lcom/google/protobuf/q;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/q;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Lcom/google/protobuf/s;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/q;

    invoke-direct {v0}, Lcom/google/protobuf/q;-><init>()V

    sput-object v0, Lcom/google/protobuf/q;->a:Lcom/google/protobuf/q;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/q;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/q;->e:I

    invoke-static {}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/q;->g:Lcom/google/protobuf/s;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/q;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/q;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/q;I)I
    .locals 0

    iput p1, p0, Lcom/google/protobuf/q;->e:I

    return p1
.end method

.method public static a(Lcom/google/protobuf/q;)Lcom/google/protobuf/r;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/q;->l()Lcom/google/protobuf/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/r;->a(Lcom/google/protobuf/q;)Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/q;Lcom/google/protobuf/s;)Lcom/google/protobuf/s;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/q;->g:Lcom/google/protobuf/s;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/q;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/q;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/q;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/q;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/q;
    .locals 1

    sget-object v0, Lcom/google/protobuf/q;->a:Lcom/google/protobuf/q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/q;)Lcom/google/protobuf/s;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/q;->g:Lcom/google/protobuf/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/q;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/q;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/q;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/q;->f:Z

    return p1
.end method

.method public static final h_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->o()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static l()Lcom/google/protobuf/r;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/r;->o()Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/q;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/q;->k()Lcom/google/protobuf/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/s;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->p()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/q;
    .locals 1

    sget-object v0, Lcom/google/protobuf/q;->a:Lcom/google/protobuf/q;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/q;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/q;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/q;->d:Z

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/q;->e:I

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/q;->f:Z

    return v0
.end method

.method public k()Lcom/google/protobuf/s;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/q;->g:Lcom/google/protobuf/s;

    return-object v0
.end method

.method public m()Lcom/google/protobuf/r;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/q;->l()Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/protobuf/r;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/q;->a(Lcom/google/protobuf/q;)Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/q;->m()Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/q;->c()Lcom/google/protobuf/q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/q;->n()Lcom/google/protobuf/r;

    move-result-object v0

    return-object v0
.end method
