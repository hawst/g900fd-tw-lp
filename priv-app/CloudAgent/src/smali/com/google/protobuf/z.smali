.class public final Lcom/google/protobuf/z;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/z;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/util/List;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private k:Z

.field private l:Lcom/google/protobuf/DescriptorProtos$FileOptions;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/z;

    invoke-direct {v0}, Lcom/google/protobuf/z;-><init>()V

    sput-object v0, Lcom/google/protobuf/z;->a:Lcom/google/protobuf/z;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/z;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/z;->e:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->f:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->b()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/z;->l:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/z;->m:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/z;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/z;Lcom/google/protobuf/DescriptorProtos$FileOptions;)Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->l:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object p1
.end method

.method public static a(Lcom/google/protobuf/z;)Lcom/google/protobuf/aa;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/z;->u()Lcom/google/protobuf/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/aa;->a(Lcom/google/protobuf/z;)Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Lcom/google/protobuf/z;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/z;->u()Lcom/google/protobuf/aa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/aa;->a([B)Lcom/google/protobuf/c;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/aa;

    invoke-static {v0}, Lcom/google/protobuf/aa;->a(Lcom/google/protobuf/aa;)Lcom/google/protobuf/z;

    move-result-object v0

    return-object v0
.end method

.method public static a([BLcom/google/protobuf/bl;)Lcom/google/protobuf/z;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/z;->u()Lcom/google/protobuf/aa;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/protobuf/aa;->a([BLcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/aa;

    invoke-static {v0}, Lcom/google/protobuf/aa;->a(Lcom/google/protobuf/aa;)Lcom/google/protobuf/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/z;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/z;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/z;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/z;
    .locals 1

    sget-object v0, Lcom/google/protobuf/z;->a:Lcom/google/protobuf/z;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/z;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protobuf/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protobuf/z;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/z;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/protobuf/z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/google/protobuf/z;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/z;->k:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protobuf/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protobuf/z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    return-object p1
.end method

.method static synthetic e(Lcom/google/protobuf/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/protobuf/z;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    return-object p1
.end method

.method static synthetic f(Lcom/google/protobuf/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/google/protobuf/z;)Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->l:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method

.method public static final l_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->e()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static u()Lcom/google/protobuf/aa;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/aa;->o()Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/z;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/z;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-virtual {v0}, Lcom/google/protobuf/m;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/protobuf/z;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/protobuf/z;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->a()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/protobuf/z;->s()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/protobuf/z;->t()Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->a()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(I)Lcom/google/protobuf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/i;

    return-object v0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->f()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/google/protobuf/m;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    return-object v0
.end method

.method public c()Lcom/google/protobuf/z;
    .locals 1

    sget-object v0, Lcom/google/protobuf/z;->a:Lcom/google/protobuf/z;

    return-object v0
.end method

.method public d(I)Lcom/google/protobuf/al;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/al;

    return-object v0
.end method

.method public e(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/z;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/z;->d:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public k()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    return-object v0
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    return-object v0
.end method

.method public n()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public o()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    return-object v0
.end method

.method public p()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public q()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    return-object v0
.end method

.method public r()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/z;->k:Z

    return v0
.end method

.method public t()Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/z;->l:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/z;->y()Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/z;->c()Lcom/google/protobuf/z;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/z;->z()Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method

.method public y()Lcom/google/protobuf/aa;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/z;->u()Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method

.method public z()Lcom/google/protobuf/aa;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/z;->a(Lcom/google/protobuf/z;)Lcom/google/protobuf/aa;

    move-result-object v0

    return-object v0
.end method
