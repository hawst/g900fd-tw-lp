.class public final Lcom/google/protobuf/bh;
.super Lcom/google/protobuf/c;


# instance fields
.field private final a:Lcom/google/protobuf/au;

.field private b:Lcom/google/protobuf/bm;

.field private c:Lcom/google/protobuf/cl;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/au;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/c;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    invoke-static {}, Lcom/google/protobuf/bm;->a()Lcom/google/protobuf/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-static {}, Lcom/google/protobuf/cl;->c()Lcom/google/protobuf/cl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bg;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/bh;-><init>(Lcom/google/protobuf/au;)V

    return-void
.end method

.method private d(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->r()Lcom/google/protobuf/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bh;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newBuilderForField is only valid for fields with message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/protobuf/bh;

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->t()Lcom/google/protobuf/au;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/bh;-><init>(Lcom/google/protobuf/au;)V

    return-object v0
.end method

.method public a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bh;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bo;Ljava/lang/Object;)V

    return-object p0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->f()Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bh;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bh;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/bm;->b(Lcom/google/protobuf/bo;Ljava/lang/Object;)V

    return-object p0
.end method

.method public b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bh;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    return-object p0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->f()Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bm;->b(Lcom/google/protobuf/bo;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->t()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bf;->a(Lcom/google/protobuf/au;)Lcom/google/protobuf/bf;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->o()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Lcom/google/protobuf/bf;
    .locals 5

    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->g()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/protobuf/bf;

    iget-object v1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    iget-object v3, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bf;-><init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;Lcom/google/protobuf/bg;)V

    invoke-static {v0}, Lcom/google/protobuf/bh;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/bh;->d()Lcom/google/protobuf/bf;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bh;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    return-object p0
.end method

.method public synthetic c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bh;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/bh;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bh;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->f()Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/bf;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v0}, Lcom/google/protobuf/bm;->c()V

    new-instance v0, Lcom/google/protobuf/bf;

    iget-object v1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    iget-object v3, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bf;-><init>(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;Lcom/google/protobuf/cl;Lcom/google/protobuf/bg;)V

    iput-object v4, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    iput-object v4, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/bh;
    .locals 2

    instance-of v0, p1, Lcom/google/protobuf/bf;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/protobuf/bf;

    invoke-static {p1}, Lcom/google/protobuf/bf;->a(Lcom/google/protobuf/bf;)Lcom/google/protobuf/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFrom(Message) can only merge messages of the same type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-static {p1}, Lcom/google/protobuf/bf;->b(Lcom/google/protobuf/bf;)Lcom/google/protobuf/bm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bm;)V

    invoke-static {p1}, Lcom/google/protobuf/bf;->c(Lcom/google/protobuf/bf;)Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/bh;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bh;

    :goto_0
    return-object p0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/bh;

    move-object p0, v0

    goto :goto_0
.end method

.method public synthetic d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/bh;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bh;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bh;

    move-result-object v0

    return-object v0
.end method

.method public d_()Lcom/google/protobuf/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bh;->c:Lcom/google/protobuf/cl;

    return-object v0
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    return-object v0
.end method

.method public f()Lcom/google/protobuf/bh;
    .locals 3

    new-instance v0, Lcom/google/protobuf/bh;

    iget-object v1, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    invoke-direct {v0, v1}, Lcom/google/protobuf/bh;-><init>(Lcom/google/protobuf/au;)V

    iget-object v1, v0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    iget-object v2, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-virtual {v1, v2}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bm;)V

    return-object v0
.end method

.method public g()Z
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/bh;->a:Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/protobuf/bh;->b:Lcom/google/protobuf/bm;

    invoke-static {v0, v1}, Lcom/google/protobuf/bf;->a(Lcom/google/protobuf/au;Lcom/google/protobuf/bm;)Z

    move-result v0

    return v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->c()Lcom/google/protobuf/bf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bh;->c()Lcom/google/protobuf/bf;

    move-result-object v0

    return-object v0
.end method
