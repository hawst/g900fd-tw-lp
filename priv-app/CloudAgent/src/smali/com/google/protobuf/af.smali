.class public final Lcom/google/protobuf/af;
.super Lcom/google/protobuf/bs;


# static fields
.field private static final a:Lcom/google/protobuf/af;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/util/List;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/af;

    invoke-direct {v0}, Lcom/google/protobuf/af;-><init>()V

    sput-object v0, Lcom/google/protobuf/af;->a:Lcom/google/protobuf/af;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/bs;-><init>()V

    iput-boolean v0, p0, Lcom/google/protobuf/af;->c:Z

    iput-boolean v0, p0, Lcom/google/protobuf/af;->e:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/af;->f:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/af;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/af;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/af;->k()Lcom/google/protobuf/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/ag;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/af;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/af;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/af;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/af;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/af;
    .locals 1

    sget-object v0, Lcom/google/protobuf/af;->a:Lcom/google/protobuf/af;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/af;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/af;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/af;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/af;->c:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/af;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/af;->d:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protobuf/af;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/af;->e:Z

    return p1
.end method

.method public static k()Lcom/google/protobuf/ag;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ag;->o()Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method

.method public static final o_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->w()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/af;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ap;

    invoke-virtual {v0}, Lcom/google/protobuf/ap;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/af;->r()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->x()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/af;
    .locals 1

    sget-object v0, Lcom/google/protobuf/af;->a:Lcom/google/protobuf/af;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/af;->b:Z

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/af;->c:Z

    return v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/af;->d:Z

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/af;->e:Z

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/af;->f:Ljava/util/List;

    return-object v0
.end method

.method public l()Lcom/google/protobuf/ag;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/af;->k()Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/google/protobuf/ag;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/af;->a(Lcom/google/protobuf/af;)Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/af;->l()Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/af;->c()Lcom/google/protobuf/af;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/af;->m()Lcom/google/protobuf/ag;

    move-result-object v0

    return-object v0
.end method
