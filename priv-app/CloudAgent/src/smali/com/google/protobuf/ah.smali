.class public final Lcom/google/protobuf/ah;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/ah;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Lcom/google/protobuf/aj;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/ah;

    invoke-direct {v0}, Lcom/google/protobuf/ah;-><init>()V

    sput-object v0, Lcom/google/protobuf/ah;->a:Lcom/google/protobuf/ah;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/ah;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/ah;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/ah;->g:Ljava/lang/String;

    invoke-static {}, Lcom/google/protobuf/aj;->b()Lcom/google/protobuf/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ah;->i:Lcom/google/protobuf/aj;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ah;->j:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/ah;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/ah;)Lcom/google/protobuf/ai;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ah;->n()Lcom/google/protobuf/ai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/ai;->a(Lcom/google/protobuf/ah;)Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/ah;Lcom/google/protobuf/aj;)Lcom/google/protobuf/aj;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ah;->i:Lcom/google/protobuf/aj;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ah;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ah;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/ah;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ah;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/ah;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ah;->a:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ah;)Lcom/google/protobuf/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ah;->i:Lcom/google/protobuf/aj;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/ah;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ah;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protobuf/ah;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ah;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/ah;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/ah;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/google/protobuf/ah;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ah;->f:Z

    return p1
.end method

.method static synthetic d(Lcom/google/protobuf/ah;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/ah;->h:Z

    return p1
.end method

.method public static n()Lcom/google/protobuf/ai;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ai;->o()Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method

.method public static final p_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->s()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ah;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/ah;->m()Lcom/google/protobuf/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->t()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/ah;
    .locals 1

    sget-object v0, Lcom/google/protobuf/ah;->a:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ah;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ah;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ah;->d:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ah;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ah;->f:Z

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ah;->g:Ljava/lang/String;

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/ah;->h:Z

    return v0
.end method

.method public m()Lcom/google/protobuf/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ah;->i:Lcom/google/protobuf/aj;

    return-object v0
.end method

.method public o()Lcom/google/protobuf/ai;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ah;->n()Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/google/protobuf/ai;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/ah;)Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ah;->o()Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ah;->c()Lcom/google/protobuf/ah;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ah;->p()Lcom/google/protobuf/ai;

    move-result-object v0

    return-object v0
.end method
