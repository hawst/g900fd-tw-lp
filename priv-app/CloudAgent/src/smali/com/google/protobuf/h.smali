.class Lcom/google/protobuf/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bb;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/bi;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/protobuf/DescriptorProtos;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->a(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->c()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "File"

    aput-object v3, v2, v5

    const-class v3, Lcom/google/protobuf/ab;

    const-class v4, Lcom/google/protobuf/ac;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->a(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->b(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->e()Lcom/google/protobuf/au;

    move-result-object v1

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Package"

    aput-object v3, v2, v6

    const-string v3, "Dependency"

    aput-object v3, v2, v7

    const-string v3, "MessageType"

    aput-object v3, v2, v8

    const-string v3, "EnumType"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Service"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "Extension"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Options"

    aput-object v4, v2, v3

    const-class v3, Lcom/google/protobuf/z;

    const-class v4, Lcom/google/protobuf/aa;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->b(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->c(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->g()Lcom/google/protobuf/au;

    move-result-object v1

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Field"

    aput-object v3, v2, v6

    const-string v3, "Extension"

    aput-object v3, v2, v7

    const-string v3, "NestedType"

    aput-object v3, v2, v8

    const-string v3, "EnumType"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "ExtensionRange"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "Options"

    aput-object v4, v2, v3

    const-class v3, Lcom/google/protobuf/i;

    const-class v4, Lcom/google/protobuf/j;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->c(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->g()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/au;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->d(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->i()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Start"

    aput-object v3, v2, v5

    const-string v3, "End"

    aput-object v3, v2, v6

    const-class v3, Lcom/google/protobuf/k;

    const-class v4, Lcom/google/protobuf/l;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->d(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->e(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->k()Lcom/google/protobuf/au;

    move-result-object v1

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Number"

    aput-object v3, v2, v6

    const-string v3, "Label"

    aput-object v3, v2, v7

    const-string v3, "Type"

    aput-object v3, v2, v8

    const-string v3, "TypeName"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Extendee"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "DefaultValue"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Options"

    aput-object v4, v2, v3

    const-class v3, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    const-class v4, Lcom/google/protobuf/u;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->e(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->f(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->m()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Value"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    const-class v3, Lcom/google/protobuf/m;

    const-class v4, Lcom/google/protobuf/n;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->f(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->g(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->o()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Number"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    const-class v3, Lcom/google/protobuf/q;

    const-class v4, Lcom/google/protobuf/r;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->g(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->h(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->q()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Method"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    const-class v3, Lcom/google/protobuf/al;

    const-class v4, Lcom/google/protobuf/am;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->h(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->i(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->s()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "InputType"

    aput-object v3, v2, v6

    const-string v3, "OutputType"

    aput-object v3, v2, v7

    const-string v3, "Options"

    aput-object v3, v2, v8

    const-class v3, Lcom/google/protobuf/ah;

    const-class v4, Lcom/google/protobuf/ai;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->i(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->j(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->u()Lcom/google/protobuf/au;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "JavaPackage"

    aput-object v3, v2, v5

    const-string v3, "JavaOuterClassname"

    aput-object v3, v2, v6

    const-string v3, "JavaMultipleFiles"

    aput-object v3, v2, v7

    const-string v3, "OptimizeFor"

    aput-object v3, v2, v8

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v9

    const-class v3, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    const-class v4, Lcom/google/protobuf/ad;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->j(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->k(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->w()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "MessageSetWireFormat"

    aput-object v3, v2, v5

    const-string v3, "NoStandardDescriptorAccessor"

    aput-object v3, v2, v6

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v7

    const-class v3, Lcom/google/protobuf/af;

    const-class v4, Lcom/google/protobuf/ag;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->k(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->l(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->y()Lcom/google/protobuf/au;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Ctype"

    aput-object v3, v2, v5

    const-string v3, "Packed"

    aput-object v3, v2, v6

    const-string v3, "Deprecated"

    aput-object v3, v2, v7

    const-string v3, "ExperimentalMapKey"

    aput-object v3, v2, v8

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v9

    const-class v3, Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    const-class v4, Lcom/google/protobuf/x;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->l(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->m(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->A()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    const-class v3, Lcom/google/protobuf/o;

    const-class v4, Lcom/google/protobuf/p;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->m(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->n(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->C()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    const-class v3, Lcom/google/protobuf/s;

    const-class v4, Lcom/google/protobuf/t;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->n(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->o(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->E()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    const-class v3, Lcom/google/protobuf/an;

    const-class v4, Lcom/google/protobuf/ao;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->o(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->p(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->G()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    const-class v3, Lcom/google/protobuf/aj;

    const-class v4, Lcom/google/protobuf/ak;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->p(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ba;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->q(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->I()Lcom/google/protobuf/au;

    move-result-object v1

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "IdentifierValue"

    aput-object v3, v2, v6

    const-string v3, "PositiveIntValue"

    aput-object v3, v2, v7

    const-string v3, "NegativeIntValue"

    aput-object v3, v2, v8

    const-string v3, "DoubleValue"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "StringValue"

    aput-object v4, v2, v3

    const-class v3, Lcom/google/protobuf/ap;

    const-class v4, Lcom/google/protobuf/aq;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->q(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->I()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/au;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/au;

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->r(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;

    new-instance v0, Lcom/google/protobuf/bt;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->K()Lcom/google/protobuf/au;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "NamePart"

    aput-object v3, v2, v5

    const-string v3, "IsExtension"

    aput-object v3, v2, v6

    const-class v3, Lcom/google/protobuf/ar;

    const-class v4, Lcom/google/protobuf/as;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/protobuf/bt;-><init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/google/protobuf/DescriptorProtos;->r(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;

    const/4 v0, 0x0

    return-object v0
.end method
