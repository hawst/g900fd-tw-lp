.class public final Lcom/google/protobuf/bt;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/protobuf/au;

.field private final b:[Lcom/google/protobuf/bu;


# direct methods
.method public constructor <init>(Lcom/google/protobuf/au;[Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/protobuf/bt;->a:Lcom/google/protobuf/au;

    invoke-virtual {p1}, Lcom/google/protobuf/au;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/bu;

    iput-object v0, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    invoke-virtual {p1}, Lcom/google/protobuf/au;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/bx;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/bx;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->ENUM:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/bv;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/bv;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/bw;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/bw;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/ca;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/ca;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->ENUM:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/by;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/by;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    new-instance v3, Lcom/google/protobuf/bz;

    aget-object v4, p2, v1

    invoke-direct {v3, v0, v4, p3, p4}, Lcom/google/protobuf/bz;-><init>(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    aput-object v3, v2, v1

    goto :goto_1

    :cond_5
    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/bt;)Lcom/google/protobuf/au;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bt;->a:Lcom/google/protobuf/au;

    return-object v0
.end method

.method private a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->r()Lcom/google/protobuf/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bt;->a:Lcom/google/protobuf/au;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This type does not have extensions."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/bt;->b:[Lcom/google/protobuf/bu;

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->a()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/bt;Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/protobuf/bt;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;

    move-result-object v0

    return-object v0
.end method
