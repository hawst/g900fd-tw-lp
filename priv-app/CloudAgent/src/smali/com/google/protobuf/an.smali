.class public final Lcom/google/protobuf/an;
.super Lcom/google/protobuf/bs;


# static fields
.field private static final a:Lcom/google/protobuf/an;


# instance fields
.field private b:Ljava/util/List;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/an;

    invoke-direct {v0}, Lcom/google/protobuf/an;-><init>()V

    sput-object v0, Lcom/google/protobuf/an;->a:Lcom/google/protobuf/an;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bs;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/an;->b:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/an;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/an;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/an;->g()Lcom/google/protobuf/ao;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/ao;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/an;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/an;->b:Ljava/util/List;

    return-object p1
.end method

.method public static b()Lcom/google/protobuf/an;
    .locals 1

    sget-object v0, Lcom/google/protobuf/an;->a:Lcom/google/protobuf/an;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/an;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/an;->b:Ljava/util/List;

    return-object v0
.end method

.method public static g()Lcom/google/protobuf/ao;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/ao;->o()Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method

.method public static final s_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->E()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/an;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ap;

    invoke-virtual {v0}, Lcom/google/protobuf/ap;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/an;->r()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->F()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/an;
    .locals 1

    sget-object v0, Lcom/google/protobuf/an;->a:Lcom/google/protobuf/an;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/an;->b:Ljava/util/List;

    return-object v0
.end method

.method public h()Lcom/google/protobuf/ao;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/an;->g()Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/google/protobuf/ao;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/an;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/an;->h()Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/an;->c()Lcom/google/protobuf/an;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/an;->i()Lcom/google/protobuf/ao;

    move-result-object v0

    return-object v0
.end method
