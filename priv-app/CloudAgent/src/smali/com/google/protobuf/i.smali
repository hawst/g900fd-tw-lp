.class public final Lcom/google/protobuf/i;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/i;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:Ljava/util/List;

.field private f:Ljava/util/List;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Z

.field private j:Lcom/google/protobuf/af;

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/i;

    invoke-direct {v0}, Lcom/google/protobuf/i;-><init>()V

    sput-object v0, Lcom/google/protobuf/i;->a:Lcom/google/protobuf/i;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/i;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->h:Ljava/util/List;

    invoke-static {}, Lcom/google/protobuf/af;->b()Lcom/google/protobuf/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/i;->j:Lcom/google/protobuf/af;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/i;->k:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/i;Lcom/google/protobuf/af;)Lcom/google/protobuf/af;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->j:Lcom/google/protobuf/af;

    return-object p1
.end method

.method public static a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/i;->s()Lcom/google/protobuf/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/i;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/i;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/i;->b:Z

    return p1
.end method

.method public static final a_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->g()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/google/protobuf/i;
    .locals 1

    sget-object v0, Lcom/google/protobuf/i;->a:Lcom/google/protobuf/i;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/i;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/protobuf/i;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/i;->i:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/i;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    return-object p1
.end method

.method static synthetic d(Lcom/google/protobuf/i;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    return-object p1
.end method

.method static synthetic e(Lcom/google/protobuf/i;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/protobuf/i;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/i;->h:Ljava/util/List;

    return-object p1
.end method

.method static synthetic f(Lcom/google/protobuf/i;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->j:Lcom/google/protobuf/af;

    return-object v0
.end method

.method public static s()Lcom/google/protobuf/j;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/j;->o()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    return-object v0
.end method

.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/i;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/i;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/protobuf/i;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/protobuf/i;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    invoke-virtual {v0}, Lcom/google/protobuf/m;->a()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/protobuf/i;->q()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/protobuf/i;->r()Lcom/google/protobuf/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/af;->a()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    return-object v0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->h()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/i;
    .locals 1

    sget-object v0, Lcom/google/protobuf/i;->a:Lcom/google/protobuf/i;

    return-object v0
.end method

.method public c(I)Lcom/google/protobuf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/i;

    return-object v0
.end method

.method public d(I)Lcom/google/protobuf/m;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/m;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/i;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public l()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    return-object v0
.end method

.method public m()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    return-object v0
.end method

.method public o()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public p()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->h:Ljava/util/List;

    return-object v0
.end method

.method public q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/i;->i:Z

    return v0
.end method

.method public r()Lcom/google/protobuf/af;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/i;->j:Lcom/google/protobuf/af;

    return-object v0
.end method

.method public t()Lcom/google/protobuf/j;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/i;->s()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public u()Lcom/google/protobuf/j;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/i;->a(Lcom/google/protobuf/i;)Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/i;->t()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/i;->c()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/i;->u()Lcom/google/protobuf/j;

    move-result-object v0

    return-object v0
.end method
