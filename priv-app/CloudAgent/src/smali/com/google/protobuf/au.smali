.class public final Lcom/google/protobuf/au;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bc;


# instance fields
.field private final a:I

.field private b:Lcom/google/protobuf/i;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/protobuf/ba;

.field private final e:Lcom/google/protobuf/au;

.field private final f:[Lcom/google/protobuf/au;

.field private final g:[Lcom/google/protobuf/ay;

.field private final h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

.field private final i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/i;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;I)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/protobuf/au;->a:I

    iput-object p1, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    invoke-virtual {p1}, Lcom/google/protobuf/i;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/google/protobuf/Descriptors;->a(Lcom/google/protobuf/ba;Lcom/google/protobuf/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/au;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/protobuf/au;->d:Lcom/google/protobuf/ba;

    iput-object p3, p0, Lcom/google/protobuf/au;->e:Lcom/google/protobuf/au;

    invoke-virtual {p1}, Lcom/google/protobuf/i;->m()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/au;

    iput-object v0, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/i;->m()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    new-instance v2, Lcom/google/protobuf/au;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/i;->c(I)Lcom/google/protobuf/i;

    move-result-object v3

    invoke-direct {v2, v3, p2, p0, v0}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/i;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;I)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/i;->o()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/ay;

    iput-object v0, p0, Lcom/google/protobuf/au;->g:[Lcom/google/protobuf/ay;

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/i;->o()I

    move-result v0

    if-ge v4, v0, :cond_1

    iget-object v6, p0, Lcom/google/protobuf/au;->g:[Lcom/google/protobuf/ay;

    new-instance v0, Lcom/google/protobuf/ay;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/i;->d(I)Lcom/google/protobuf/m;

    move-result-object v1

    const/4 v5, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/ay;-><init>(Lcom/google/protobuf/m;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;ILcom/google/protobuf/at;)V

    aput-object v0, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/i;->i()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/Descriptors$FieldDescriptor;

    iput-object v0, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/i;->i()I

    move-result v0

    if-ge v4, v0, :cond_2

    iget-object v7, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    new-instance v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/i;->a(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/Descriptors$FieldDescriptor;-><init>(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;IZLcom/google/protobuf/at;)V

    aput-object v0, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/i;->k()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/Descriptors$FieldDescriptor;

    iput-object v0, p0, Lcom/google/protobuf/au;->i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    const/4 v4, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/i;->k()I

    move-result v0

    if-ge v4, v0, :cond_3

    iget-object v7, p0, Lcom/google/protobuf/au;->i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    new-instance v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/i;->b(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/Descriptors$FieldDescriptor;-><init>(Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;IZLcom/google/protobuf/at;)V

    aput-object v0, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/bc;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/i;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;ILcom/google/protobuf/at;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/i;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/au;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/au;->j()V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/au;Lcom/google/protobuf/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/au;->a(Lcom/google/protobuf/i;)V

    return-void
.end method

.method private a(Lcom/google/protobuf/i;)V
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/i;->c(I)Lcom/google/protobuf/i;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/protobuf/au;->a(Lcom/google/protobuf/i;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/protobuf/au;->g:[Lcom/google/protobuf/ay;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/protobuf/au;->g:[Lcom/google/protobuf/ay;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/i;->d(I)Lcom/google/protobuf/m;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/ay;->a(Lcom/google/protobuf/ay;Lcom/google/protobuf/m;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/i;->a(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/protobuf/au;->i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/protobuf/au;->i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Lcom/google/protobuf/i;->b(I)Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    return-void
.end method

.method private j()V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-direct {v4}, Lcom/google/protobuf/au;->j()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-static {v4}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/au;->i:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Lcom/google/protobuf/i;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    return-object v0
.end method

.method public a(I)Z
    .locals 3

    iget-object v0, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->g()I

    move-result v2

    if-gt v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/k;->i()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lcom/google/protobuf/Descriptors$FieldDescriptor;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/au;->d:Lcom/google/protobuf/ba;

    invoke-static {v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/av;)Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/google/protobuf/aw;

    invoke-direct {v1, p0, p1}, Lcom/google/protobuf/aw;-><init>(Lcom/google/protobuf/bc;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->d:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method public e()Lcom/google/protobuf/af;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->b:Lcom/google/protobuf/i;

    invoke-virtual {v0}, Lcom/google/protobuf/i;->r()Lcom/google/protobuf/af;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->h:[Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->f:[Lcom/google/protobuf/au;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/au;->g:[Lcom/google/protobuf/ay;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/au;->a()Lcom/google/protobuf/i;

    move-result-object v0

    return-object v0
.end method
