.class public final Lcom/google/protobuf/am;
.super Lcom/google/protobuf/bq;


# instance fields
.field private a:Lcom/google/protobuf/al;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/bq;-><init>()V

    return-void
.end method

.method static synthetic o()Lcom/google/protobuf/am;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/am;->p()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method private static p()Lcom/google/protobuf/am;
    .locals 3

    new-instance v0, Lcom/google/protobuf/am;

    invoke-direct {v0}, Lcom/google/protobuf/am;-><init>()V

    new-instance v1, Lcom/google/protobuf/al;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/protobuf/al;-><init>(Lcom/google/protobuf/h;)V

    iput-object v1, v0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/protobuf/ah;)Lcom/google/protobuf/am;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/al;->b()Lcom/google/protobuf/al;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/al;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/al;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/am;->a(Ljava/lang/String;)Lcom/google/protobuf/am;

    :cond_1
    invoke-static {p1}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Ljava/util/List;)Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/al;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/al;->k()Lcom/google/protobuf/an;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/am;->b(Lcom/google/protobuf/an;)Lcom/google/protobuf/am;

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/al;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/am;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/an;)Lcom/google/protobuf/am;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0, p1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Lcom/google/protobuf/an;)Lcom/google/protobuf/an;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/protobuf/am;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Z)Z

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0, p1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->d()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/am;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/am;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/protobuf/an;)Lcom/google/protobuf/am;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->c(Lcom/google/protobuf/al;)Lcom/google/protobuf/an;

    move-result-object v0

    invoke-static {}, Lcom/google/protobuf/an;->b()Lcom/google/protobuf/an;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    iget-object v1, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v1}, Lcom/google/protobuf/al;->c(Lcom/google/protobuf/al;)Lcom/google/protobuf/an;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/an;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/ao;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/ao;->h()Lcom/google/protobuf/an;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Lcom/google/protobuf/an;)Lcom/google/protobuf/an;

    :goto_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;Z)Z

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0, p1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Lcom/google/protobuf/an;)Lcom/google/protobuf/an;

    goto :goto_0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->d()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/am;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/google/protobuf/al;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/am;->d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/am;->d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->d()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/protobuf/am;
    .locals 2

    invoke-static {}, Lcom/google/protobuf/am;->p()Lcom/google/protobuf/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/protobuf/ce;)Lcom/google/protobuf/am;
    .locals 1

    instance-of v0, p1, Lcom/google/protobuf/al;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/protobuf/al;

    invoke-virtual {p0, p1}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/al;)Lcom/google/protobuf/am;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    goto :goto_0
.end method

.method public d(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/am;
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/am;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/am;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    :goto_1
    return-object p0

    :sswitch_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/am;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/g;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/am;->a(Ljava/lang/String;)Lcom/google/protobuf/am;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/protobuf/ah;->n()Lcom/google/protobuf/ai;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/ai;->h()Lcom/google/protobuf/ah;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/ah;)Lcom/google/protobuf/am;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/protobuf/an;->g()Lcom/google/protobuf/ao;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->j()Lcom/google/protobuf/an;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/protobuf/ao;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/ao;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-virtual {v1}, Lcom/google/protobuf/ao;->h()Lcom/google/protobuf/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/protobuf/am;->a(Lcom/google/protobuf/an;)Lcom/google/protobuf/am;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/al;->r_()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->a()Z

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/al;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/am;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/am;->b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/am;->h()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/protobuf/al;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v0}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    iget-object v1, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-static {v1}, Lcom/google/protobuf/al;->b(Lcom/google/protobuf/al;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/al;->a(Lcom/google/protobuf/al;Ljava/util/List;)Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->j()Z

    move-result v0

    return v0
.end method

.method public j()Lcom/google/protobuf/an;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/am;->a:Lcom/google/protobuf/al;

    invoke-virtual {v0}, Lcom/google/protobuf/al;->k()Lcom/google/protobuf/an;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k()Lcom/google/protobuf/bp;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->c()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->d()Lcom/google/protobuf/am;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->g()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Lcom/google/protobuf/cg;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/am;->g()Lcom/google/protobuf/al;

    move-result-object v0

    return-object v0
.end method
