.class public final Lcom/google/protobuf/DescriptorProtos;
.super Ljava/lang/Object;


# static fields
.field private static A:Lcom/google/protobuf/au;

.field private static B:Lcom/google/protobuf/bt;

.field private static C:Lcom/google/protobuf/au;

.field private static D:Lcom/google/protobuf/bt;

.field private static E:Lcom/google/protobuf/au;

.field private static F:Lcom/google/protobuf/bt;

.field private static G:Lcom/google/protobuf/au;

.field private static H:Lcom/google/protobuf/bt;

.field private static I:Lcom/google/protobuf/au;

.field private static J:Lcom/google/protobuf/bt;

.field private static K:Lcom/google/protobuf/ba;

.field private static a:Lcom/google/protobuf/au;

.field private static b:Lcom/google/protobuf/bt;

.field private static c:Lcom/google/protobuf/au;

.field private static d:Lcom/google/protobuf/bt;

.field private static e:Lcom/google/protobuf/au;

.field private static f:Lcom/google/protobuf/bt;

.field private static g:Lcom/google/protobuf/au;

.field private static h:Lcom/google/protobuf/bt;

.field private static i:Lcom/google/protobuf/au;

.field private static j:Lcom/google/protobuf/bt;

.field private static k:Lcom/google/protobuf/au;

.field private static l:Lcom/google/protobuf/bt;

.field private static m:Lcom/google/protobuf/au;

.field private static n:Lcom/google/protobuf/bt;

.field private static o:Lcom/google/protobuf/au;

.field private static p:Lcom/google/protobuf/bt;

.field private static q:Lcom/google/protobuf/au;

.field private static r:Lcom/google/protobuf/bt;

.field private static s:Lcom/google/protobuf/au;

.field private static t:Lcom/google/protobuf/bt;

.field private static u:Lcom/google/protobuf/au;

.field private static v:Lcom/google/protobuf/bt;

.field private static w:Lcom/google/protobuf/au;

.field private static x:Lcom/google/protobuf/bt;

.field private static y:Lcom/google/protobuf/au;

.field private static z:Lcom/google/protobuf/bt;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\n google/protobuf/descriptor.proto\u0012\u000fgoogle.protobuf\"G\n\u0011FileDescriptorSet\u00122\n\u0004file\u0018\u0001 \u0003(\u000b2$.google.protobuf.FileDescriptorProto\"\u00dc\u0002\n\u0013FileDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000f\n\u0007package\u0018\u0002 \u0001(\t\u0012\u0012\n\ndependency\u0018\u0003 \u0003(\t\u00126\n\u000cmessage_type\u0018\u0004 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0005 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u00128\n\u0007service\u0018\u0006 \u0003(\u000b2\'.google.protobuf.ServiceDescriptorProto\u00128\n\textension\u0018\u0007 \u0003(\u000b2%.google.p"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "rotobuf.FieldDescriptorProto\u0012-\n\u0007options\u0018\u0008 \u0001(\u000b2\u001c.google.protobuf.FileOptions\"\u00a9\u0003\n\u000fDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001 \u0001(\t\u00124\n\u0005field\u0018\u0002 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00128\n\textension\u0018\u0006 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00125\n\u000bnested_type\u0018\u0003 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0004 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u0012H\n\u000fextension_range\u0018\u0005 \u0003(\u000b2/.google.protobuf.DescriptorProto.Extensi"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "onRange\u00120\n\u0007options\u0018\u0007 \u0001(\u000b2\u001f.google.protobuf.MessageOptions\u001a,\n\u000eExtensionRange\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0002 \u0001(\u0005\"\u0094\u0005\n\u0014FieldDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0003 \u0001(\u0005\u0012:\n\u0005label\u0018\u0004 \u0001(\u000e2+.google.protobuf.FieldDescriptorProto.Label\u00128\n\u0004type\u0018\u0005 \u0001(\u000e2*.google.protobuf.FieldDescriptorProto.Type\u0012\u0011\n\ttype_name\u0018\u0006 \u0001(\t\u0012\u0010\n\u0008extendee\u0018\u0002 \u0001(\t\u0012\u0015\n\rdefault_value\u0018\u0007 \u0001(\t\u0012.\n\u0007options\u0018\u0008 \u0001(\u000b2\u001d.google.protobuf.FieldOptions\"\u00b6\u0002\n\u0004Type\u0012\u000f\n\u000bTYP"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "E_DOUBLE\u0010\u0001\u0012\u000e\n\nTYPE_FLOAT\u0010\u0002\u0012\u000e\n\nTYPE_INT64\u0010\u0003\u0012\u000f\n\u000bTYPE_UINT64\u0010\u0004\u0012\u000e\n\nTYPE_INT32\u0010\u0005\u0012\u0010\n\u000cTYPE_FIXED64\u0010\u0006\u0012\u0010\n\u000cTYPE_FIXED32\u0010\u0007\u0012\r\n\tTYPE_BOOL\u0010\u0008\u0012\u000f\n\u000bTYPE_STRING\u0010\t\u0012\u000e\n\nTYPE_GROUP\u0010\n\u0012\u0010\n\u000cTYPE_MESSAGE\u0010\u000b\u0012\u000e\n\nTYPE_BYTES\u0010\u000c\u0012\u000f\n\u000bTYPE_UINT32\u0010\r\u0012\r\n\tTYPE_ENUM\u0010\u000e\u0012\u0011\n\rTYPE_SFIXED32\u0010\u000f\u0012\u0011\n\rTYPE_SFIXED64\u0010\u0010\u0012\u000f\n\u000bTYPE_SINT32\u0010\u0011\u0012\u000f\n\u000bTYPE_SINT64\u0010\u0012\"C\n\u0005Label\u0012\u0012\n\u000eLABEL_OPTIONAL\u0010\u0001\u0012\u0012\n\u000eLABEL_REQUIRED\u0010\u0002\u0012\u0012\n\u000eLABEL_REPEATED\u0010\u0003\"\u008c\u0001\n\u0013EnumDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, " \u0001(\t\u00128\n\u0005value\u0018\u0002 \u0003(\u000b2).google.protobuf.EnumValueDescriptorProto\u0012-\n\u0007options\u0018\u0003 \u0001(\u000b2\u001c.google.protobuf.EnumOptions\"l\n\u0018EnumValueDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0002 \u0001(\u0005\u00122\n\u0007options\u0018\u0003 \u0001(\u000b2!.google.protobuf.EnumValueOptions\"\u0090\u0001\n\u0016ServiceDescriptorProto\u0012\u000c\n\u0004name\u0018\u0001 \u0001(\t\u00126\n\u0006method\u0018\u0002 \u0003(\u000b2&.google.protobuf.MethodDescriptorProto\u00120\n\u0007options\u0018\u0003 \u0001(\u000b2\u001f.google.protobuf.ServiceOptions\"\u007f\n\u0015MethodDescriptorProto\u0012\u000c\n\u0004name\u0018"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\u0001 \u0001(\t\u0012\u0012\n\ninput_type\u0018\u0002 \u0001(\t\u0012\u0013\n\u000boutput_type\u0018\u0003 \u0001(\t\u0012/\n\u0007options\u0018\u0004 \u0001(\u000b2\u001e.google.protobuf.MethodOptions\"\u00b9\u0002\n\u000bFileOptions\u0012\u0014\n\u000cjava_package\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014java_outer_classname\u0018\u0008 \u0001(\t\u0012\"\n\u0013java_multiple_files\u0018\n \u0001(\u0008:\u0005false\u0012F\n\u000coptimize_for\u0018\t \u0001(\u000e2).google.protobuf.FileOptions.OptimizeMode:\u0005SPEED\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\":\n\u000cOptimizeMode\u0012\t\n\u0005SPEED\u0010\u0001\u0012\r\n\tCODE_SIZE\u0010\u0002\u0012\u0010\n\u000cLITE_RUNTIME\u0010\u0003"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u00b8\u0001\n\u000eMessageOptions\u0012&\n\u0017message_set_wire_format\u0018\u0001 \u0001(\u0008:\u0005false\u0012.\n\u001fno_standard_descriptor_accessor\u0018\u0002 \u0001(\u0008:\u0005false\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u0080\u0002\n\u000cFieldOptions\u00122\n\u0005ctype\u0018\u0001 \u0001(\u000e2#.google.protobuf.FieldOptions.CType\u0012\u000e\n\u0006packed\u0018\u0002 \u0001(\u0008\u0012\u0019\n\ndeprecated\u0018\u0003 \u0001(\u0008:\u0005false\u0012\u001c\n\u0014experimental_map_key\u0018\t \u0001(\t\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.Uninterpre"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "tedOption\"#\n\u0005CType\u0012\u0008\n\u0004CORD\u0010\u0001\u0012\u0010\n\u000cSTRING_PIECE\u0010\u0002*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"]\n\u000bEnumOptions\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"b\n\u0010EnumValueOptions\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"`\n\u000eServiceOptions\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"_\n\rMethodOptions\u0012C\n\u0014uninterpreted_option\u0018\u00e7\u0007 \u0003(\u000b2$"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ".google.protobuf.UninterpretedOption*\t\u0008\u00e8\u0007\u0010\u0080\u0080\u0080\u0080\u0002\"\u0085\u0002\n\u0013UninterpretedOption\u0012;\n\u0004name\u0018\u0002 \u0003(\u000b2-.google.protobuf.UninterpretedOption.NamePart\u0012\u0018\n\u0010identifier_value\u0018\u0003 \u0001(\t\u0012\u001a\n\u0012positive_int_value\u0018\u0004 \u0001(\u0004\u0012\u001a\n\u0012negative_int_value\u0018\u0005 \u0001(\u0003\u0012\u0014\n\u000cdouble_value\u0018\u0006 \u0001(\u0001\u0012\u0014\n\u000cstring_value\u0018\u0007 \u0001(\u000c\u001a3\n\u0008NamePart\u0012\u0011\n\tname_part\u0018\u0001 \u0002(\t\u0012\u0014\n\u000cis_extension\u0018\u0002 \u0002(\u0008B)\n\u0013com.google.protobufB\u0010DescriptorProtosH\u0001"

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/protobuf/h;

    invoke-direct {v1}, Lcom/google/protobuf/h;-><init>()V

    new-array v2, v3, [Lcom/google/protobuf/ba;

    invoke-static {v0, v2, v1}, Lcom/google/protobuf/ba;->a([Ljava/lang/String;[Lcom/google/protobuf/ba;Lcom/google/protobuf/bb;)V

    return-void
.end method

.method static synthetic A()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->y:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic B()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->z:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic C()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->A:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic D()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->B:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic E()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->C:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic F()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->D:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic G()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->E:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic H()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->F:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic I()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->G:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic J()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->H:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic K()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->I:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic L()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->J:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->a:Lcom/google/protobuf/au;

    return-object p0
.end method

.method public static a()Lcom/google/protobuf/ba;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->K:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/ba;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->K:Lcom/google/protobuf/ba;

    return-object p0
.end method

.method static synthetic a(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->b:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic b(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->c:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic b(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->d:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method public static b()V
    .locals 0

    return-void
.end method

.method static synthetic c()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->a:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic c(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->e:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic c(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->f:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic d(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->g:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic d()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->b:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic d(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->h:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic e()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->c:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic e(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->i:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic e(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->j:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic f(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->k:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic f()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->d:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic f(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->l:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic g()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->e:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic g(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->m:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic g(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->n:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic h(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->o:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic h()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->f:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic h(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->p:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic i()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->g:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic i(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->q:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic i(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->r:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic j(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->s:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic j()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->h:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic j(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->t:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic k()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->i:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic k(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->u:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic k(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->v:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic l(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->w:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic l()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->j:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic l(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->x:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic m()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->k:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic m(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->y:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic m(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->z:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic n(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->A:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic n()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->l:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic n(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->B:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic o()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->m:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic o(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->C:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic o(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->D:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic p(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->E:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic p()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->n:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic p(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->F:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic q()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->o:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic q(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->G:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic q(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->H:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic r(Lcom/google/protobuf/au;)Lcom/google/protobuf/au;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->I:Lcom/google/protobuf/au;

    return-object p0
.end method

.method static synthetic r()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->p:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic r(Lcom/google/protobuf/bt;)Lcom/google/protobuf/bt;
    .locals 0

    sput-object p0, Lcom/google/protobuf/DescriptorProtos;->J:Lcom/google/protobuf/bt;

    return-object p0
.end method

.method static synthetic s()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->q:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic t()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->r:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic u()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->s:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic v()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->t:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic w()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->u:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic x()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->v:Lcom/google/protobuf/bt;

    return-object v0
.end method

.method static synthetic y()Lcom/google/protobuf/au;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->w:Lcom/google/protobuf/au;

    return-object v0
.end method

.method static synthetic z()Lcom/google/protobuf/bt;
    .locals 1

    sget-object v0, Lcom/google/protobuf/DescriptorProtos;->x:Lcom/google/protobuf/bt;

    return-object v0
.end method
