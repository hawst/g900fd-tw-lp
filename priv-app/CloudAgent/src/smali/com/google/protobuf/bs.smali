.class public abstract Lcom/google/protobuf/bs;
.super Lcom/google/protobuf/bp;


# instance fields
.field private final a:Lcom/google/protobuf/bm;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    invoke-static {}, Lcom/google/protobuf/bm;->a()Lcom/google/protobuf/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/bs;)Lcom/google/protobuf/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/bs;Lcom/google/protobuf/Descriptors$FieldDescriptor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/bs;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    return-void
.end method

.method private c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->r()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protobuf/bs;->c_()Lcom/google/protobuf/au;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/bp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/bs;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/protobuf/bs;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/bo;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/bp;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/protobuf/bs;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)V

    iget-object v0, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bm;->b(Lcom/google/protobuf/bo;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->t()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bf;->a(Lcom/google/protobuf/au;)Lcom/google/protobuf/bf;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->o()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/bp;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Ljava/util/Map;
    .locals 2

    invoke-static {p0}, Lcom/google/protobuf/bp;->b(Lcom/google/protobuf/bp;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    invoke-virtual {v1}, Lcom/google/protobuf/bm;->d()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected r()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/bs;->a:Lcom/google/protobuf/bm;

    invoke-virtual {v0}, Lcom/google/protobuf/bm;->e()Z

    move-result v0

    return v0
.end method
