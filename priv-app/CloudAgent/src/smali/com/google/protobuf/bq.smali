.class public abstract Lcom/google/protobuf/bq;
.super Lcom/google/protobuf/c;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/c;-><init>()V

    return-void
.end method

.method private c()Lcom/google/protobuf/bt;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/bp;->b_()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bq;->c()Lcom/google/protobuf/bt;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bt;->a(Lcom/google/protobuf/bt;Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;

    move-result-object v0

    invoke-interface {v0, p0, p2}, Lcom/google/protobuf/bu;->a(Lcom/google/protobuf/bq;Ljava/lang/Object;)V

    return-object p0
.end method

.method public synthetic a()Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->l()Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bq;->c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;I)Z
    .locals 1

    invoke-virtual {p2, p4, p1}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/g;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bq;->c()Lcom/google/protobuf/bt;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bt;->a(Lcom/google/protobuf/bt;Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;

    move-result-object v0

    invoke-interface {v0, p0, p2}, Lcom/google/protobuf/bu;->b(Lcom/google/protobuf/bq;Ljava/lang/Object;)V

    return-object p0
.end method

.method public final b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bp;->a(Lcom/google/protobuf/bp;Lcom/google/protobuf/cl;)Lcom/google/protobuf/cl;

    return-object p0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->l()Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bp;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/bp;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bp;->a(Lcom/google/protobuf/bp;)Lcom/google/protobuf/cl;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/bp;->a(Lcom/google/protobuf/bp;Lcom/google/protobuf/cl;)Lcom/google/protobuf/cl;

    return-object p0
.end method

.method public c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bq;->c()Lcom/google/protobuf/bt;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/protobuf/bt;->a(Lcom/google/protobuf/bt;Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/bu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/bu;->a()Lcom/google/protobuf/cf;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/bq;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->l()Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/bq;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/bq;->b(Lcom/google/protobuf/cl;)Lcom/google/protobuf/bq;

    move-result-object v0

    return-object v0
.end method

.method public final d_()Lcom/google/protobuf/cl;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bp;->a(Lcom/google/protobuf/bp;)Lcom/google/protobuf/cl;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/google/protobuf/au;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bq;->c()Lcom/google/protobuf/bt;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/bt;->a(Lcom/google/protobuf/bt;)Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/bq;->k()Lcom/google/protobuf/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/bp;->a()Z

    move-result v0

    return v0
.end method

.method protected abstract k()Lcom/google/protobuf/bp;
.end method

.method public l()Lcom/google/protobuf/bq;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
