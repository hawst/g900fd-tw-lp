.class public final Lcom/google/protobuf/ay;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bc;
.implements Lcom/google/protobuf/cd;


# instance fields
.field private final a:I

.field private b:Lcom/google/protobuf/m;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/protobuf/ba;

.field private final e:Lcom/google/protobuf/au;

.field private f:[Lcom/google/protobuf/az;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/m;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;I)V
    .locals 7

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/protobuf/ay;->a:I

    iput-object p1, p0, Lcom/google/protobuf/ay;->b:Lcom/google/protobuf/m;

    invoke-virtual {p1}, Lcom/google/protobuf/m;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/google/protobuf/Descriptors;->a(Lcom/google/protobuf/ba;Lcom/google/protobuf/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ay;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/protobuf/ay;->d:Lcom/google/protobuf/ba;

    iput-object p3, p0, Lcom/google/protobuf/ay;->e:Lcom/google/protobuf/au;

    invoke-virtual {p1}, Lcom/google/protobuf/m;->i()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/protobuf/Descriptors$DescriptorValidationException;

    const-string v1, "Enums must contain at least one value."

    invoke-direct {v0, p0, v1, v5}, Lcom/google/protobuf/Descriptors$DescriptorValidationException;-><init>(Lcom/google/protobuf/bc;Ljava/lang/String;Lcom/google/protobuf/at;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/m;->i()I

    move-result v0

    new-array v0, v0, [Lcom/google/protobuf/az;

    iput-object v0, p0, Lcom/google/protobuf/ay;->f:[Lcom/google/protobuf/az;

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/m;->i()I

    move-result v0

    if-ge v4, v0, :cond_1

    iget-object v6, p0, Lcom/google/protobuf/ay;->f:[Lcom/google/protobuf/az;

    new-instance v0, Lcom/google/protobuf/az;

    invoke-virtual {p1, v4}, Lcom/google/protobuf/m;->a(I)Lcom/google/protobuf/q;

    move-result-object v1

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/az;-><init>(Lcom/google/protobuf/q;Lcom/google/protobuf/ba;Lcom/google/protobuf/ay;ILcom/google/protobuf/at;)V

    aput-object v0, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/bc;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/m;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;ILcom/google/protobuf/at;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/ay;-><init>(Lcom/google/protobuf/m;Lcom/google/protobuf/ba;Lcom/google/protobuf/au;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/ay;Lcom/google/protobuf/m;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/ay;->a(Lcom/google/protobuf/m;)V

    return-void
.end method

.method private a(Lcom/google/protobuf/m;)V
    .locals 3

    iput-object p1, p0, Lcom/google/protobuf/ay;->b:Lcom/google/protobuf/m;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/protobuf/ay;->f:[Lcom/google/protobuf/az;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/ay;->f:[Lcom/google/protobuf/az;

    aget-object v1, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/m;->a(I)Lcom/google/protobuf/q;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/az;->a(Lcom/google/protobuf/az;Lcom/google/protobuf/q;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/protobuf/az;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/ay;->d:Lcom/google/protobuf/ba;

    invoke-static {v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/av;->b(Lcom/google/protobuf/av;)Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/google/protobuf/aw;

    invoke-direct {v1, p0, p1}, Lcom/google/protobuf/aw;-><init>(Lcom/google/protobuf/bc;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/az;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/protobuf/az;
    .locals 3

    iget-object v0, p0, Lcom/google/protobuf/ay;->d:Lcom/google/protobuf/ba;

    invoke-static {v0}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/protobuf/ay;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/av;->a(Ljava/lang/String;)Lcom/google/protobuf/bc;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/protobuf/az;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/protobuf/az;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lcom/google/protobuf/m;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ay;->b:Lcom/google/protobuf/m;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ay;->b:Lcom/google/protobuf/m;

    invoke-virtual {v0}, Lcom/google/protobuf/m;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ay;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ay;->d:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ay;->f:[Lcom/google/protobuf/az;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ay;->a()Lcom/google/protobuf/m;

    move-result-object v0

    return-object v0
.end method
