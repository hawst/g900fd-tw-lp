.class public abstract Lcom/google/protobuf/c;
.super Lcom/google/protobuf/e;

# interfaces
.implements Lcom/google/protobuf/cf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/e;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/protobuf/Descriptors$FieldDescriptor;I)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(Lcom/google/protobuf/ce;Ljava/lang/String;Ljava/util/List;)V
    .locals 6

    invoke-interface {p0}, Lcom/google/protobuf/ce;->c_()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/au;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0, v0}, Lcom/google/protobuf/ce;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Lcom/google/protobuf/ce;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ce;

    add-int/lit8 v3, v2, 0x1

    invoke-static {p1, v1, v2}, Lcom/google/protobuf/c;->a(Ljava/lang/String;Lcom/google/protobuf/Descriptors$FieldDescriptor;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/ce;Ljava/lang/String;Ljava/util/List;)V

    move v2, v3

    goto :goto_2

    :cond_3
    invoke-interface {p0, v1}, Lcom/google/protobuf/ce;->a(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Z

    move-result v2

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/protobuf/ce;

    const/4 v2, -0x1

    invoke-static {p1, v1, v2}, Lcom/google/protobuf/c;->a(Ljava/lang/String;Lcom/google/protobuf/Descriptors$FieldDescriptor;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/ce;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method private static a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;Lcom/google/protobuf/cf;)V
    .locals 9

    const/4 v5, 0x0

    invoke-interface {p3}, Lcom/google/protobuf/cf;->e()Lcom/google/protobuf/au;

    move-result-object v6

    const/4 v4, 0x0

    move-object v1, v5

    move-object v2, v5

    move-object v3, v5

    :goto_0
    invoke-virtual {p0}, Lcom/google/protobuf/g;->a()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    sget v0, Lcom/google/protobuf/WireFormat;->b:I

    invoke-virtual {p0, v0}, Lcom/google/protobuf/g;->a(I)V

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/protobuf/cf;->m()Lcom/google/protobuf/ce;

    move-result-object v0

    invoke-interface {p3, v1, v0}, Lcom/google/protobuf/cf;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    :cond_1
    return-void

    :cond_2
    sget v7, Lcom/google/protobuf/WireFormat;->c:I

    if-ne v0, v7, :cond_7

    invoke-virtual {p0}, Lcom/google/protobuf/g;->m()I

    move-result v4

    if-eqz v4, :cond_b

    instance-of v0, p2, Lcom/google/protobuf/bi;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Lcom/google/protobuf/bi;

    invoke-virtual {v0, v6, v4}, Lcom/google/protobuf/bi;->a(Lcom/google/protobuf/au;I)Lcom/google/protobuf/bk;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_6

    iget-object v1, v0, Lcom/google/protobuf/bk;->a:Lcom/google/protobuf/Descriptors$FieldDescriptor;

    iget-object v0, v0, Lcom/google/protobuf/bk;->b:Lcom/google/protobuf/ce;

    invoke-interface {v0}, Lcom/google/protobuf/ce;->v()Lcom/google/protobuf/cf;

    move-result-object v2

    invoke-interface {p3, v1}, Lcom/google/protobuf/cf;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ce;

    if-eqz v0, :cond_3

    invoke-interface {v2, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/google/protobuf/f;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/g;->a(Ljava/io/InputStream;)Lcom/google/protobuf/g;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/g;)Lcom/google/protobuf/cf;

    move-object v3, v5

    :cond_4
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    :goto_2
    move v3, v4

    :goto_3
    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_5
    move-object v0, v5

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_c

    invoke-static {}, Lcom/google/protobuf/co;->a()Lcom/google/protobuf/cp;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/f;)Lcom/google/protobuf/cp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/cp;->a()Lcom/google/protobuf/co;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v5

    goto :goto_2

    :cond_7
    sget v7, Lcom/google/protobuf/WireFormat;->d:I

    if-ne v0, v7, :cond_a

    if-nez v4, :cond_8

    invoke-virtual {p0}, Lcom/google/protobuf/g;->l()Lcom/google/protobuf/f;

    move-result-object v0

    move v3, v4

    move-object v8, v2

    move-object v2, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_3

    :cond_8
    if-nez v2, :cond_9

    invoke-static {}, Lcom/google/protobuf/co;->a()Lcom/google/protobuf/cp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protobuf/g;->l()Lcom/google/protobuf/f;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/protobuf/cp;->a(Lcom/google/protobuf/f;)Lcom/google/protobuf/cp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/cp;->a()Lcom/google/protobuf/co;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/co;)Lcom/google/protobuf/cn;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move v3, v4

    goto :goto_3

    :cond_9
    invoke-virtual {p0, v2, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move v3, v4

    goto :goto_3

    :cond_a
    invoke-virtual {p0, v0}, Lcom/google/protobuf/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_b
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move v3, v4

    goto :goto_3

    :cond_c
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_2
.end method

.method static a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;Lcom/google/protobuf/cf;I)Z
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {p3}, Lcom/google/protobuf/cf;->e()Lcom/google/protobuf/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/au;->e()Lcom/google/protobuf/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/af;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/protobuf/WireFormat;->a:I

    if-ne p4, v0, :cond_0

    invoke-static {p0, p1, p2, p3}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;Lcom/google/protobuf/cf;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->a(I)I

    move-result v4

    invoke-static {p4}, Lcom/google/protobuf/WireFormat;->b(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/google/protobuf/au;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    instance-of v0, p2, Lcom/google/protobuf/bi;

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Lcom/google/protobuf/bi;

    invoke-virtual {v0, v3, v5}, Lcom/google/protobuf/bi;->a(Lcom/google/protobuf/au;I)Lcom/google/protobuf/bk;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    move-object v3, v1

    move-object v1, v0

    :goto_2
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->k()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->p()Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->i()Z

    move-result v6

    invoke-static {v0, v6}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/WireFormat$FieldType;Z)I

    move-result v0

    if-eq v4, v0, :cond_5

    :cond_1
    invoke-virtual {p1, p4, p0}, Lcom/google/protobuf/cn;->a(ILcom/google/protobuf/g;)Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/google/protobuf/bk;->a:Lcom/google/protobuf/Descriptors$FieldDescriptor;

    iget-object v0, v0, Lcom/google/protobuf/bk;->b:Lcom/google/protobuf/ce;

    goto :goto_1

    :cond_3
    move-object v3, v1

    goto :goto_2

    :cond_4
    invoke-virtual {v3, v5}, Lcom/google/protobuf/au;->b(I)Lcom/google/protobuf/Descriptors$FieldDescriptor;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->p()Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->i()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/protobuf/g;->s()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/g;->d(I)I

    move-result v0

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->k()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v1

    sget-object v4, Lcom/google/protobuf/WireFormat$FieldType;->ENUM:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v1, v4, :cond_7

    :goto_3
    invoke-virtual {p0}, Lcom/google/protobuf/g;->w()I

    move-result v1

    if-lez v1, :cond_8

    invoke-virtual {p0}, Lcom/google/protobuf/g;->n()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->u()Lcom/google/protobuf/ay;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/protobuf/ay;->a(I)Lcom/google/protobuf/az;

    move-result-object v1

    if-nez v1, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-interface {p3, v3, v1}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_3

    :cond_7
    :goto_4
    invoke-virtual {p0}, Lcom/google/protobuf/g;->w()I

    move-result v1

    if-lez v1, :cond_8

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->k()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p3, v3, v1}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_4

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/protobuf/g;->e(I)V

    :goto_5
    move v0, v2

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/google/protobuf/b;->a:[I

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->j()Lcom/google/protobuf/Descriptors$FieldDescriptor$Type;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/Descriptors$FieldDescriptor$Type;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->k()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/protobuf/bm;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/WireFormat$FieldType;)Ljava/lang/Object;

    move-result-object v0

    :cond_a
    :goto_6
    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {p3, v3, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_5

    :pswitch_0
    if-eqz v1, :cond_c

    invoke-interface {v1}, Lcom/google/protobuf/ce;->v()Lcom/google/protobuf/cf;

    move-result-object v0

    move-object v1, v0

    :goto_7
    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-interface {p3, v3}, Lcom/google/protobuf/cf;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ce;

    invoke-interface {v1, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;

    :cond_b
    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->f()I

    move-result v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/protobuf/g;->a(ILcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-interface {v1}, Lcom/google/protobuf/cf;->m()Lcom/google/protobuf/ce;

    move-result-object v0

    goto :goto_6

    :cond_c
    invoke-interface {p3, v3}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/cf;

    move-result-object v0

    move-object v1, v0

    goto :goto_7

    :pswitch_1
    if-eqz v1, :cond_e

    invoke-interface {v1}, Lcom/google/protobuf/ce;->v()Lcom/google/protobuf/cf;

    move-result-object v0

    move-object v1, v0

    :goto_8
    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-interface {p3, v3}, Lcom/google/protobuf/cf;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ce;

    invoke-interface {v1, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;

    :cond_d
    invoke-virtual {p0, v1, p2}, Lcom/google/protobuf/g;->a(Lcom/google/protobuf/ch;Lcom/google/protobuf/bl;)V

    invoke-interface {v1}, Lcom/google/protobuf/cf;->m()Lcom/google/protobuf/ce;

    move-result-object v0

    goto :goto_6

    :cond_e
    invoke-interface {p3, v3}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Lcom/google/protobuf/cf;

    move-result-object v0

    move-object v1, v0

    goto :goto_8

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/protobuf/g;->n()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->u()Lcom/google/protobuf/ay;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/ay;->a(I)Lcom/google/protobuf/az;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-virtual {p1, v5, v1}, Lcom/google/protobuf/cn;->a(II)Lcom/google/protobuf/cn;

    move v0, v2

    goto/16 :goto_0

    :cond_f
    invoke-interface {p3, v3, v0}, Lcom/google/protobuf/cf;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected static b(Lcom/google/protobuf/ce;)Lcom/google/protobuf/UninitializedMessageException;
    .locals 2

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-static {p0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/ce;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private static d(Lcom/google/protobuf/ce;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, ""

    invoke-static {p0, v1, v0}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/ce;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public abstract a()Lcom/google/protobuf/c;
.end method

.method public a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;
    .locals 5

    invoke-interface {p1}, Lcom/google/protobuf/ce;->c_()Lcom/google/protobuf/au;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protobuf/c;->e()Lcom/google/protobuf/au;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFrom(Message) can only merge messages of the same type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lcom/google/protobuf/ce;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/Descriptors$FieldDescriptor;

    invoke-virtual {v1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/protobuf/c;->c(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/protobuf/Descriptors$FieldDescriptor;->g()Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    move-result-object v2

    sget-object v4, Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;->MESSAGE:Lcom/google/protobuf/Descriptors$FieldDescriptor$JavaType;

    if-ne v2, v4, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/protobuf/c;->b(Lcom/google/protobuf/Descriptors$FieldDescriptor;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/ce;

    invoke-interface {v2}, Lcom/google/protobuf/ce;->w()Lcom/google/protobuf/ce;

    move-result-object v4

    if-ne v2, v4, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Lcom/google/protobuf/ce;->v()Lcom/google/protobuf/cf;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ce;

    invoke-interface {v2, v0}, Lcom/google/protobuf/cf;->c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/cf;->m()Lcom/google/protobuf/ce;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_0

    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/Descriptors$FieldDescriptor;Ljava/lang/Object;)Lcom/google/protobuf/cf;

    goto :goto_0

    :cond_5
    invoke-interface {p1}, Lcom/google/protobuf/ce;->e()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/c;

    return-object p0
.end method

.method public a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/c;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/protobuf/cn;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cf;

    return-object p0
.end method

.method public a(Lcom/google/protobuf/g;)Lcom/google/protobuf/c;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/bi;->a()Lcom/google/protobuf/bi;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/c;->d_()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/cl;->a(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cn;

    move-result-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/g;->a()I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    invoke-virtual {v0}, Lcom/google/protobuf/cn;->a()Lcom/google/protobuf/cl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/c;->d(Lcom/google/protobuf/cl;)Lcom/google/protobuf/cf;

    return-object p0

    :cond_1
    invoke-static {p1, v0, p2, p0, v1}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/cn;Lcom/google/protobuf/bl;Lcom/google/protobuf/cf;I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public a([B)Lcom/google/protobuf/c;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/protobuf/e;->b([B)Lcom/google/protobuf/e;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/c;

    return-object v0
.end method

.method public a([BII)Lcom/google/protobuf/c;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/google/protobuf/e;->b([BII)Lcom/google/protobuf/e;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/c;

    return-object v0
.end method

.method public a([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/protobuf/e;->b([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/c;

    return-object v0
.end method

.method public a([BLcom/google/protobuf/bl;)Lcom/google/protobuf/c;
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/protobuf/e;->b([BLcom/google/protobuf/bl;)Lcom/google/protobuf/e;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/c;

    return-object v0
.end method

.method public synthetic b()Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/c;->a()Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([B)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/c;->a([B)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([BII)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/protobuf/c;->a([BII)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/protobuf/c;->a([BIILcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([BLcom/google/protobuf/bl;)Lcom/google/protobuf/e;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/c;->a([BLcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/ce;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/ce;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;)Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/c;->a(Lcom/google/protobuf/g;Lcom/google/protobuf/bl;)Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/c;->a()Lcom/google/protobuf/c;

    move-result-object v0

    return-object v0
.end method
