.class public final Lcom/google/protobuf/m;
.super Lcom/google/protobuf/bp;


# static fields
.field private static final a:Lcom/google/protobuf/m;


# instance fields
.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:Z

.field private f:Lcom/google/protobuf/o;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/protobuf/m;

    invoke-direct {v0}, Lcom/google/protobuf/m;-><init>()V

    sput-object v0, Lcom/google/protobuf/m;->a:Lcom/google/protobuf/m;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->a()Lcom/google/protobuf/ba;

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->b()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/bp;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/m;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    invoke-static {}, Lcom/google/protobuf/o;->b()Lcom/google/protobuf/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/m;->f:Lcom/google/protobuf/o;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/m;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/m;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/protobuf/m;)Lcom/google/protobuf/n;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/m;->l()Lcom/google/protobuf/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/n;->a(Lcom/google/protobuf/m;)Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/protobuf/m;Lcom/google/protobuf/o;)Lcom/google/protobuf/o;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/m;->f:Lcom/google/protobuf/o;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/m;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/m;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/m;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/protobuf/m;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/m;->b:Z

    return p1
.end method

.method public static b()Lcom/google/protobuf/m;
    .locals 1

    sget-object v0, Lcom/google/protobuf/m;->a:Lcom/google/protobuf/m;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/m;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/m;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/protobuf/m;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/google/protobuf/m;)Lcom/google/protobuf/o;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->f:Lcom/google/protobuf/o;

    return-object v0
.end method

.method public static final f_()Lcom/google/protobuf/au;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->m()Lcom/google/protobuf/au;

    move-result-object v0

    return-object v0
.end method

.method public static l()Lcom/google/protobuf/n;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/n;->o()Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/google/protobuf/q;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/q;

    return-object v0
.end method

.method public final a()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/m;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/q;

    invoke-virtual {v0}, Lcom/google/protobuf/q;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/protobuf/m;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/protobuf/m;->k()Lcom/google/protobuf/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/o;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b_()Lcom/google/protobuf/bt;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/DescriptorProtos;->n()Lcom/google/protobuf/bt;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/protobuf/m;
    .locals 1

    sget-object v0, Lcom/google/protobuf/m;->a:Lcom/google/protobuf/m;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/m;->b:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    return-object v0
.end method

.method public i()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/m;->e:Z

    return v0
.end method

.method public k()Lcom/google/protobuf/o;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/m;->f:Lcom/google/protobuf/o;

    return-object v0
.end method

.method public m()Lcom/google/protobuf/n;
    .locals 1

    invoke-static {}, Lcom/google/protobuf/m;->l()Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method

.method public n()Lcom/google/protobuf/n;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/m;->a(Lcom/google/protobuf/m;)Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method

.method public synthetic v()Lcom/google/protobuf/cf;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/m;->m()Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method

.method public synthetic w()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/m;->c()Lcom/google/protobuf/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x()Lcom/google/protobuf/ch;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/m;->n()Lcom/google/protobuf/n;

    move-result-object v0

    return-object v0
.end method
