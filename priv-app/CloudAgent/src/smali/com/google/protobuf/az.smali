.class public final Lcom/google/protobuf/az;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/protobuf/bc;
.implements Lcom/google/protobuf/cc;


# instance fields
.field private final a:I

.field private b:Lcom/google/protobuf/q;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/protobuf/ba;

.field private final e:Lcom/google/protobuf/ay;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/q;Lcom/google/protobuf/ba;Lcom/google/protobuf/ay;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/protobuf/az;->a:I

    iput-object p1, p0, Lcom/google/protobuf/az;->b:Lcom/google/protobuf/q;

    iput-object p2, p0, Lcom/google/protobuf/az;->d:Lcom/google/protobuf/ba;

    iput-object p3, p0, Lcom/google/protobuf/az;->e:Lcom/google/protobuf/ay;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/google/protobuf/ay;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/protobuf/q;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/az;->c:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/bc;)V

    invoke-static {p2}, Lcom/google/protobuf/ba;->a(Lcom/google/protobuf/ba;)Lcom/google/protobuf/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/av;->a(Lcom/google/protobuf/az;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/q;Lcom/google/protobuf/ba;Lcom/google/protobuf/ay;ILcom/google/protobuf/at;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/az;-><init>(Lcom/google/protobuf/q;Lcom/google/protobuf/ba;Lcom/google/protobuf/ay;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/az;Lcom/google/protobuf/q;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/az;->a(Lcom/google/protobuf/q;)V

    return-void
.end method

.method private a(Lcom/google/protobuf/q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/protobuf/az;->b:Lcom/google/protobuf/q;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/az;->a:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->b:Lcom/google/protobuf/q;

    invoke-virtual {v0}, Lcom/google/protobuf/q;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->d:Lcom/google/protobuf/ba;

    return-object v0
.end method

.method public e()Lcom/google/protobuf/q;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->b:Lcom/google/protobuf/q;

    return-object v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->b:Lcom/google/protobuf/q;

    invoke-virtual {v0}, Lcom/google/protobuf/q;->i()I

    move-result v0

    return v0
.end method

.method public g()Lcom/google/protobuf/ay;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/az;->e:Lcom/google/protobuf/ay;

    return-object v0
.end method

.method public synthetic i()Lcom/google/protobuf/ce;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/az;->e()Lcom/google/protobuf/q;

    move-result-object v0

    return-object v0
.end method
