.class final Lcom/google/gson/internal/a/o;
.super Lcom/google/gson/f;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/gson/f;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/gson/stream/a;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/util/Calendar;

    invoke-virtual {p0, p1, p2}, Lcom/google/gson/internal/a/o;->a(Lcom/google/gson/stream/a;Ljava/util/Calendar;)V

    return-void
.end method

.method public a(Lcom/google/gson/stream/a;Ljava/util/Calendar;)V
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/stream/a;->e()Lcom/google/gson/stream/a;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/a;->c()Lcom/google/gson/stream/a;

    const-string v0, "year"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    const-string v0, "month"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    const-string v0, "dayOfMonth"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    const-string v0, "hourOfDay"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    const-string v0, "minute"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    const-string v0, "second"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    invoke-virtual {p1}, Lcom/google/gson/stream/a;->d()Lcom/google/gson/stream/a;

    goto :goto_0
.end method
