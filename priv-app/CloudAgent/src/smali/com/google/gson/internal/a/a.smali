.class public final Lcom/google/gson/internal/a/a;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/google/gson/f;

.field public static final B:Lcom/google/gson/g;

.field public static final C:Lcom/google/gson/f;

.field public static final D:Lcom/google/gson/g;

.field public static final E:Lcom/google/gson/f;

.field public static final F:Lcom/google/gson/g;

.field public static final G:Lcom/google/gson/f;

.field public static final H:Lcom/google/gson/g;

.field public static final I:Lcom/google/gson/g;

.field public static final J:Lcom/google/gson/f;

.field public static final K:Lcom/google/gson/g;

.field public static final L:Lcom/google/gson/f;

.field public static final M:Lcom/google/gson/g;

.field public static final N:Lcom/google/gson/f;

.field public static final O:Lcom/google/gson/g;

.field public static final P:Lcom/google/gson/g;

.field public static final a:Lcom/google/gson/f;

.field public static final b:Lcom/google/gson/g;

.field public static final c:Lcom/google/gson/f;

.field public static final d:Lcom/google/gson/g;

.field public static final e:Lcom/google/gson/f;

.field public static final f:Lcom/google/gson/f;

.field public static final g:Lcom/google/gson/g;

.field public static final h:Lcom/google/gson/f;

.field public static final i:Lcom/google/gson/g;

.field public static final j:Lcom/google/gson/f;

.field public static final k:Lcom/google/gson/g;

.field public static final l:Lcom/google/gson/f;

.field public static final m:Lcom/google/gson/g;

.field public static final n:Lcom/google/gson/f;

.field public static final o:Lcom/google/gson/f;

.field public static final p:Lcom/google/gson/f;

.field public static final q:Lcom/google/gson/f;

.field public static final r:Lcom/google/gson/g;

.field public static final s:Lcom/google/gson/f;

.field public static final t:Lcom/google/gson/g;

.field public static final u:Lcom/google/gson/f;

.field public static final v:Lcom/google/gson/g;

.field public static final w:Lcom/google/gson/f;

.field public static final x:Lcom/google/gson/g;

.field public static final y:Lcom/google/gson/f;

.field public static final z:Lcom/google/gson/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/gson/internal/a/b;

    invoke-direct {v0}, Lcom/google/gson/internal/a/b;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->a:Lcom/google/gson/f;

    const-class v0, Ljava/lang/Class;

    sget-object v1, Lcom/google/gson/internal/a/a;->a:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->b:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/m;

    invoke-direct {v0}, Lcom/google/gson/internal/a/m;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->c:Lcom/google/gson/f;

    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/gson/internal/a/a;->c:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->d:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/w;

    invoke-direct {v0}, Lcom/google/gson/internal/a/w;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->e:Lcom/google/gson/f;

    new-instance v0, Lcom/google/gson/internal/a/x;

    invoke-direct {v0}, Lcom/google/gson/internal/a/x;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->f:Lcom/google/gson/f;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gson/internal/a/a;->e:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->g:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/y;

    invoke-direct {v0}, Lcom/google/gson/internal/a/y;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->h:Lcom/google/gson/f;

    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gson/internal/a/a;->h:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->i:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/z;

    invoke-direct {v0}, Lcom/google/gson/internal/a/z;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->j:Lcom/google/gson/f;

    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gson/internal/a/a;->j:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->k:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/aa;

    invoke-direct {v0}, Lcom/google/gson/internal/a/aa;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->l:Lcom/google/gson/f;

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gson/internal/a/a;->l:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->m:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/ab;

    invoke-direct {v0}, Lcom/google/gson/internal/a/ab;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->n:Lcom/google/gson/f;

    new-instance v0, Lcom/google/gson/internal/a/ac;

    invoke-direct {v0}, Lcom/google/gson/internal/a/ac;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->o:Lcom/google/gson/f;

    new-instance v0, Lcom/google/gson/internal/a/c;

    invoke-direct {v0}, Lcom/google/gson/internal/a/c;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->p:Lcom/google/gson/f;

    new-instance v0, Lcom/google/gson/internal/a/d;

    invoke-direct {v0}, Lcom/google/gson/internal/a/d;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->q:Lcom/google/gson/f;

    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/gson/internal/a/a;->q:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->r:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/e;

    invoke-direct {v0}, Lcom/google/gson/internal/a/e;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->s:Lcom/google/gson/f;

    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gson/internal/a/a;->s:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->t:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/f;

    invoke-direct {v0}, Lcom/google/gson/internal/a/f;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->u:Lcom/google/gson/f;

    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/gson/internal/a/a;->u:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->v:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/g;

    invoke-direct {v0}, Lcom/google/gson/internal/a/g;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->w:Lcom/google/gson/f;

    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/gson/internal/a/a;->w:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->x:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/h;

    invoke-direct {v0}, Lcom/google/gson/internal/a/h;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->y:Lcom/google/gson/f;

    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/gson/internal/a/a;->y:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->z:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/i;

    invoke-direct {v0}, Lcom/google/gson/internal/a/i;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->A:Lcom/google/gson/f;

    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/gson/internal/a/a;->A:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->B:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/j;

    invoke-direct {v0}, Lcom/google/gson/internal/a/j;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->C:Lcom/google/gson/f;

    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/gson/internal/a/a;->C:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->D:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/k;

    invoke-direct {v0}, Lcom/google/gson/internal/a/k;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->E:Lcom/google/gson/f;

    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/gson/internal/a/a;->E:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->b(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->F:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/l;

    invoke-direct {v0}, Lcom/google/gson/internal/a/l;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->G:Lcom/google/gson/f;

    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/gson/internal/a/a;->G:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->H:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/n;

    invoke-direct {v0}, Lcom/google/gson/internal/a/n;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->I:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/o;

    invoke-direct {v0}, Lcom/google/gson/internal/a/o;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->J:Lcom/google/gson/f;

    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gson/internal/a/a;->J:Lcom/google/gson/f;

    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/a/a;->b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->K:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/p;

    invoke-direct {v0}, Lcom/google/gson/internal/a/p;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->L:Lcom/google/gson/f;

    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/gson/internal/a/a;->L:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->M:Lcom/google/gson/g;

    new-instance v0, Lcom/google/gson/internal/a/q;

    invoke-direct {v0}, Lcom/google/gson/internal/a/q;-><init>()V

    sput-object v0, Lcom/google/gson/internal/a/a;->N:Lcom/google/gson/f;

    const-class v0, Lcom/google/gson/b;

    sget-object v1, Lcom/google/gson/internal/a/a;->N:Lcom/google/gson/f;

    invoke-static {v0, v1}, Lcom/google/gson/internal/a/a;->a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->O:Lcom/google/gson/g;

    invoke-static {}, Lcom/google/gson/internal/a/a;->a()Lcom/google/gson/g;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/a/a;->P:Lcom/google/gson/g;

    return-void
.end method

.method public static a()Lcom/google/gson/g;
    .locals 1

    new-instance v0, Lcom/google/gson/internal/a/r;

    invoke-direct {v0}, Lcom/google/gson/internal/a/r;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;
    .locals 1

    new-instance v0, Lcom/google/gson/internal/a/s;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/s;-><init>(Ljava/lang/Class;Lcom/google/gson/f;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;
    .locals 1

    new-instance v0, Lcom/google/gson/internal/a/t;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/a/t;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;
    .locals 1

    new-instance v0, Lcom/google/gson/internal/a/v;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/a/v;-><init>(Ljava/lang/Class;Lcom/google/gson/f;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)Lcom/google/gson/g;
    .locals 1

    new-instance v0, Lcom/google/gson/internal/a/u;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/a/u;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/f;)V

    return-object v0
.end method
