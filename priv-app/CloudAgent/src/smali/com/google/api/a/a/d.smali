.class public final Lcom/google/api/a/a/d;
.super Lcom/google/api/client/googleapis/b/b;


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/s;)V
    .locals 6

    const-string v3, "https://www.googleapis.com/"

    const-string v4, "drive/v2/"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/googleapis/b/b;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/s;)V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/google/api/client/http/b/e;)Lcom/google/api/client/http/b/b;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/a/a/d;->b(Lcom/google/api/client/http/b/e;)Lcom/google/api/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lcom/google/api/client/http/b/b;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/a/a/d;->c(Ljava/lang/String;)Lcom/google/api/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/api/client/http/b/e;)Lcom/google/api/a/a/d;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/api/client/googleapis/b/b;->a(Lcom/google/api/client/http/b/e;)Lcom/google/api/client/http/b/b;

    return-object p0
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/google/api/client/http/b/b;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/a/a/d;->d(Ljava/lang/String;)Lcom/google/api/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/a/a/d;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/api/client/googleapis/b/b;->a(Ljava/lang/String;)Lcom/google/api/client/http/b/b;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/api/a/a/d;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/api/client/googleapis/b/b;->b(Ljava/lang/String;)Lcom/google/api/client/http/b/b;

    return-object p0
.end method

.method public k()Lcom/google/api/a/a/a;
    .locals 9

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/api/a/a/a;

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->c()Lcom/google/api/client/http/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->h()Lcom/google/api/client/http/b/e;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->i()Lcom/google/api/client/http/s;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->b()Lcom/google/api/client/json/d;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->d()Lcom/google/api/client/json/f;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->e()Lcom/google/api/client/http/i;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/client/http/i;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->j()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/api/a/a/a;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/b/e;Lcom/google/api/client/http/s;Lcom/google/api/client/json/d;Lcom/google/api/client/json/f;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/api/a/a/a;

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->c()Lcom/google/api/client/http/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->h()Lcom/google/api/client/http/b/e;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->i()Lcom/google/api/client/http/s;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->b()Lcom/google/api/client/json/d;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->d()Lcom/google/api/client/json/f;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/api/a/a/d;->j()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/api/a/a/a;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/b/e;Lcom/google/api/client/http/s;Lcom/google/api/client/json/d;Lcom/google/api/client/json/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
