.class public Lcom/google/api/a/a/h;
.super Lcom/google/api/a/a/i;


# instance fields
.field final synthetic a:Lcom/google/api/a/a/e;

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/api/a/a/e;Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/google/api/a/a/h;->a:Lcom/google/api/a/a/e;

    iget-object v0, p1, Lcom/google/api/a/a/e;->a:Lcom/google/api/a/a/a;

    sget-object v1, Lcom/google/api/client/http/HttpMethod;->POST:Lcom/google/api/client/http/HttpMethod;

    const-string v2, "files/{fileId}/trash"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/a/a/i;-><init>(Lcom/google/api/client/http/b/a;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "Required parameter fileId must be specified."

    invoke-static {p2, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/a/a/h;->b:Ljava/lang/String;

    return-void
.end method
