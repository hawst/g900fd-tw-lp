.class public Lcom/google/api/a/a/f;
.super Lcom/google/api/a/a/i;


# instance fields
.field final synthetic a:Lcom/google/api/a/a/e;

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private c:Lcom/google/api/client/googleapis/media/MediaHttpDownloader;


# direct methods
.method constructor <init>(Lcom/google/api/a/a/e;Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/google/api/a/a/f;->a:Lcom/google/api/a/a/e;

    iget-object v0, p1, Lcom/google/api/a/a/e;->a:Lcom/google/api/a/a/a;

    sget-object v1, Lcom/google/api/client/http/HttpMethod;->GET:Lcom/google/api/client/http/HttpMethod;

    const-string v2, "files/{fileId}"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/a/a/i;-><init>(Lcom/google/api/client/http/b/a;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "Required parameter fileId must be specified."

    invoke-static {p2, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/a/a/f;->b:Ljava/lang/String;

    new-instance v0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    iget-object v1, p1, Lcom/google/api/a/a/e;->a:Lcom/google/api/a/a/a;

    invoke-virtual {v1}, Lcom/google/api/a/a/a;->a()Lcom/google/api/client/http/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/http/r;->a()Lcom/google/api/client/http/v;

    move-result-object v1

    iget-object v2, p1, Lcom/google/api/a/a/e;->a:Lcom/google/api/a/a/a;

    invoke-virtual {v2}, Lcom/google/api/a/a/a;->a()Lcom/google/api/client/http/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/client/http/r;->b()Lcom/google/api/client/http/s;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/s;)V

    iput-object v0, p0, Lcom/google/api/a/a/f;->c:Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    return-void
.end method
