.class public final Lcom/google/api/a/a/a/h;
.super Lcom/google/api/client/json/b;


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private c:Lcom/google/api/a/a/a/i;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private h:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private l:Ljava/util/Map;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private n:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private o:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private p:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private t:Lcom/google/api/a/a/a/l;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private u:Lcom/google/api/client/util/DateTime;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/a/a/a/k;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/google/api/a/a/a/i;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->c:Lcom/google/api/a/a/a/i;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->f:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->g:Ljava/util/List;

    return-object v0
.end method

.method public l()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->h:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->k:Ljava/util/List;

    return-object v0
.end method

.method public n()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->l:Ljava/util/Map;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->m:Ljava/lang/String;

    return-object v0
.end method

.method public p()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->n:Ljava/lang/Long;

    return-object v0
.end method

.method public q()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->o:Ljava/lang/Long;

    return-object v0
.end method

.method public r()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->p:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->q:Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->r:Ljava/lang/String;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->s:Ljava/lang/String;

    return-object v0
.end method

.method public v()Lcom/google/api/a/a/a/l;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->t:Lcom/google/api/a/a/a/l;

    return-object v0
.end method

.method public w()Lcom/google/api/client/util/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/h;->u:Lcom/google/api/client/util/DateTime;

    return-object v0
.end method
