.class public final Lcom/google/api/a/a/a/a;
.super Lcom/google/api/client/json/b;


# instance fields
.field private a:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private f:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private h:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field

.field private k:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/json/i;
    .end annotation

    .annotation runtime Lcom/google/api/client/util/t;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/a/a/a/e;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/a/a/a/g;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/a/a/a/b;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/a/a/a/f;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/a/a/a/d;

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->c:Ljava/util/List;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->f:Ljava/lang/Long;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->h:Ljava/lang/Long;

    return-object v0
.end method

.method public m()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/a/a/a/a;->k:Ljava/lang/Long;

    return-object v0
.end method
