.class public final enum Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
.super Ljava/lang/Enum;


# static fields
.field private static final synthetic $VALUES:[Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

.field public static final enum ASSERTION:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/ad;
        a = "assertion"
    .end annotation
.end field

.field public static final enum AUTHORIZATION_CODE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/ad;
        a = "authorization_code"
    .end annotation
.end field

.field public static final enum NONE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/ad;
        a = "none"
    .end annotation
.end field

.field public static final enum PASSWORD:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/ad;
        a = "password"
    .end annotation
.end field

.field public static final enum REFRESH_TOKEN:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/ad;
        a = "refresh_token"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const-string v1, "AUTHORIZATION_CODE"

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->AUTHORIZATION_CODE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    new-instance v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const-string v1, "PASSWORD"

    invoke-direct {v0, v1, v3}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->PASSWORD:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    new-instance v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const-string v1, "ASSERTION"

    invoke-direct {v0, v1, v4}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->ASSERTION:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    new-instance v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const-string v1, "REFRESH_TOKEN"

    invoke-direct {v0, v1, v5}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->REFRESH_TOKEN:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    new-instance v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->NONE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    sget-object v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->AUTHORIZATION_CODE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->PASSWORD:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->ASSERTION:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->REFRESH_TOKEN:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->NONE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->$VALUES:[Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .locals 1

    const-class v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    return-object v0
.end method

.method public static values()[Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .locals 1

    sget-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->$VALUES:[Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    invoke-virtual {v0}, [Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    return-object v0
.end method
