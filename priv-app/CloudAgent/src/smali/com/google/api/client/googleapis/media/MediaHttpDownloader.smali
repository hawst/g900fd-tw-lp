.class public final Lcom/google/api/client/googleapis/media/MediaHttpDownloader;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/api/client/http/r;

.field private final b:Lcom/google/api/client/http/v;

.field private c:Z

.field private d:Z

.field private e:I

.field private f:Lcom/google/api/client/googleapis/media/MediaHttpDownloader$DownloadState;


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/s;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->c:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->d:Z

    const/high16 v0, 0x2000000

    iput v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->e:I

    sget-object v0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader$DownloadState;->NOT_STARTED:Lcom/google/api/client/googleapis/media/MediaHttpDownloader$DownloadState;

    iput-object v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->f:Lcom/google/api/client/googleapis/media/MediaHttpDownloader$DownloadState;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/v;

    iput-object v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->b:Lcom/google/api/client/http/v;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/http/v;->a()Lcom/google/api/client/http/r;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->a:Lcom/google/api/client/http/r;

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    goto :goto_0
.end method
