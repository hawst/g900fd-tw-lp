.class public Lcom/google/api/client/json/b;
.super Lcom/google/api/client/util/GenericData;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:Lcom/google/api/client/json/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/util/GenericData;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/api/client/json/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/json/b;->a:Lcom/google/api/client/json/d;

    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/json/b;->e()Lcom/google/api/client/json/b;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/b;->a:Lcom/google/api/client/json/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/client/json/b;->a:Lcom/google/api/client/json/d;

    invoke-virtual {v0, p0}, Lcom/google/api/client/json/d;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/api/client/util/GenericData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Lcom/google/api/client/json/b;
    .locals 1

    invoke-super {p0}, Lcom/google/api/client/util/GenericData;->f()Lcom/google/api/client/util/GenericData;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/b;

    return-object v0
.end method

.method public synthetic f()Lcom/google/api/client/util/GenericData;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/json/b;->e()Lcom/google/api/client/json/b;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/b;->a:Lcom/google/api/client/json/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/client/json/b;->a:Lcom/google/api/client/json/d;

    invoke-virtual {v0, p0}, Lcom/google/api/client/json/d;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/api/client/util/GenericData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
