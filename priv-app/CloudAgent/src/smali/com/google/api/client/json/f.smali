.class public Lcom/google/api/client/json/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/api/client/util/y;


# instance fields
.field private final a:Lcom/google/api/client/json/d;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/json/f;->a:Lcom/google/api/client/json/d;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/api/client/json/f;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/google/api/client/json/f;->a:Lcom/google/api/client/json/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/api/client/json/d;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Lcom/google/api/client/json/g;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p3, v1, v2}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Type;ZLcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
