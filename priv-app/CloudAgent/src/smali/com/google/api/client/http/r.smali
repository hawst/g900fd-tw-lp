.class public final Lcom/google/api/client/http/r;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/api/client/http/v;

.field private final b:Lcom/google/api/client/http/s;


# direct methods
.method constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/api/client/http/r;->a:Lcom/google/api/client/http/v;

    iput-object p2, p0, Lcom/google/api/client/http/r;->b:Lcom/google/api/client/http/s;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/i;Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/http/r;->a:Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Lcom/google/api/client/http/v;->b()Lcom/google/api/client/http/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/http/r;->b:Lcom/google/api/client/http/s;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/api/client/http/r;->b:Lcom/google/api/client/http/s;

    invoke-interface {v1, v0}, Lcom/google/api/client/http/s;->b(Lcom/google/api/client/http/p;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/p;

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/i;)Lcom/google/api/client/http/p;

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {v0, p3}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;

    :cond_2
    return-object v0
.end method

.method public a(Lcom/google/api/client/http/i;Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;
    .locals 1

    sget-object v0, Lcom/google/api/client/http/HttpMethod;->POST:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/api/client/http/r;->a(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/i;Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/api/client/http/v;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/r;->a:Lcom/google/api/client/http/v;

    return-object v0
.end method

.method public b()Lcom/google/api/client/http/s;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/r;->b:Lcom/google/api/client/http/s;

    return-object v0
.end method
