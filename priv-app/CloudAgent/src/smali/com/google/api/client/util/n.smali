.class final Lcom/google/api/client/util/n;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Map$Entry;


# instance fields
.field final synthetic a:Lcom/google/api/client/util/m;

.field private b:Ljava/lang/Object;

.field private final c:Lcom/google/api/client/util/q;


# direct methods
.method constructor <init>(Lcom/google/api/client/util/m;Lcom/google/api/client/util/q;Ljava/lang/Object;)V
    .locals 1

    iput-object p1, p0, Lcom/google/api/client/util/n;->a:Lcom/google/api/client/util/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/api/client/util/n;->c:Lcom/google/api/client/util/q;

    invoke-static {p3}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/util/n;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/util/n;->c:Lcom/google/api/client/util/q;

    invoke-virtual {v0}, Lcom/google/api/client/util/q;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/util/n;->a:Lcom/google/api/client/util/m;

    iget-object v1, v1, Lcom/google/api/client/util/m;->b:Lcom/google/api/client/util/h;

    invoke-virtual {v1}, Lcom/google/api/client/util/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {p0}, Lcom/google/api/client/util/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/api/client/util/n;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public synthetic getKey()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/util/n;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/util/n;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/api/client/util/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/api/client/util/n;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/google/api/client/util/n;->b:Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/google/api/client/util/n;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/api/client/util/n;->c:Lcom/google/api/client/util/q;

    iget-object v2, p0, Lcom/google/api/client/util/n;->a:Lcom/google/api/client/util/m;

    iget-object v2, v2, Lcom/google/api/client/util/m;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, p1}, Lcom/google/api/client/util/q;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
