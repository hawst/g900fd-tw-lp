.class public Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
.super Lcom/google/api/client/http/HttpResponseException;


# static fields
.field private static final serialVersionUID:J = 0x5aff10c793dbb70L


# instance fields
.field private final transient details:Lcom/google/api/client/googleapis/json/a;

.field private final transient jsonFactory:Lcom/google/api/client/json/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;Lcom/google/api/client/googleapis/json/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2, p4}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/t;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/d;

    iput-object p3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/a;

    return-void
.end method

.method public static execute(Lcom/google/api/client/json/d;Lcom/google/api/client/http/p;)Lcom/google/api/client/http/t;
    .locals 2

    invoke-static {p0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/api/client/http/p;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/api/client/http/p;->a(Z)Lcom/google/api/client/http/p;

    :cond_0
    invoke-virtual {p1}, Lcom/google/api/client/http/p;->n()Lcom/google/api/client/http/t;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/p;->a(Z)Lcom/google/api/client/http/p;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/api/client/http/t;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    return-object v1

    :cond_2
    invoke-static {p0, v1}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    move-result-object v0

    throw v0
.end method

.method public static from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .locals 5

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->c()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/api/client/json/c;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/api/client/http/n;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    if-eqz v0, :cond_7

    :try_start_1
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/d;->a(Ljava/io/InputStream;)Lcom/google/api/client/json/g;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    :try_start_2
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_9

    const-string v0, "error"

    invoke-virtual {v3, v0}, Lcom/google/api/client/json/g;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    if-eq v0, v2, :cond_9

    const-class v0, Lcom/google/api/client/googleapis/json/a;

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Lcom/google/api/client/json/g;->a(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/json/a;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v0}, Lcom/google/api/client/googleapis/json/a;->d()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    if-nez v3, :cond_3

    :try_start_4
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->g()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_1
    :goto_1
    invoke-static {p1}, Lcom/google/api/client/http/HttpResponseException;->computeMessageBuffer(Lcom/google/api/client/http/t;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/common/base/am;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    new-instance v0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;-><init>(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;Lcom/google/api/client/googleapis/json/a;Ljava/lang/String;)V

    return-object v0

    :cond_3
    if-nez v1, :cond_1

    :try_start_5
    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    :goto_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v3, v1

    move-object v0, v1

    :goto_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-nez v3, :cond_4

    :try_start_7
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->g()V

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1

    :cond_4
    if-nez v0, :cond_8

    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v3, v1

    move-object v2, v1

    :goto_4
    if-nez v3, :cond_6

    :try_start_8
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->g()V

    :cond_5
    :goto_5
    throw v0

    :catch_2
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v4

    goto :goto_2

    :cond_6
    if-nez v2, :cond_5

    invoke-virtual {v3}, Lcom/google/api/client/json/g;->b()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_5

    :cond_7
    :try_start_9
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->j()Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    move-result-object v0

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_4

    :catch_5
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catch_6
    move-exception v2

    goto :goto_3

    :cond_8
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1

    :cond_9
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final getDetails()Lcom/google/api/client/googleapis/json/a;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/a;

    return-object v0
.end method

.method public final getJsonFactory()Lcom/google/api/client/json/d;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/d;

    return-object v0
.end method
