.class public Lcom/google/api/client/http/b/b;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/api/client/http/v;

.field private b:Lcom/google/api/client/http/b/e;

.field private c:Lcom/google/api/client/http/s;

.field private final d:Lcom/google/api/client/json/d;

.field private e:Lcom/google/api/client/json/f;

.field private f:Lcom/google/api/client/http/i;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/api/client/http/b/b;->a:Lcom/google/api/client/http/v;

    iput-object p2, p0, Lcom/google/api/client/http/b/b;->d:Lcom/google/api/client/json/d;

    invoke-virtual {p0, p3}, Lcom/google/api/client/http/b/b;->a(Ljava/lang/String;)Lcom/google/api/client/http/b/b;

    invoke-virtual {p0, p4}, Lcom/google/api/client/http/b/b;->b(Ljava/lang/String;)Lcom/google/api/client/http/b/b;

    iput-object p5, p0, Lcom/google/api/client/http/b/b;->c:Lcom/google/api/client/http/s;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/b/e;)Lcom/google/api/client/http/b/b;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/b/b;->b:Lcom/google/api/client/http/b/e;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/client/http/b/b;
    .locals 2

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "root URL cannot be used if base URL is used."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-string v0, "root URL cannot be null."

    invoke-static {p1, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "root URL must end with a \"/\"."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/api/client/http/b/b;->g:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    return v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/http/b/b;
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "service path cannot be used if base URL is used."

    invoke-static {v0, v3}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-string v0, "service path cannot be null."

    invoke-static {p1, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v1, :cond_2

    const-string v1, "/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "service path must equal \"/\" if it is of length 1."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-string v0, ""

    :cond_0
    :goto_1
    iput-object v0, p0, Lcom/google/api/client/http/b/b;->h:Ljava/lang/String;

    return-object p0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    const-string v2, "service path must end with a \"/\" and not begin with a \"/\"."

    invoke-static {v1, v2}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final b()Lcom/google/api/client/json/d;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->d:Lcom/google/api/client/json/d;

    return-object v0
.end method

.method public final c()Lcom/google/api/client/http/v;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->a:Lcom/google/api/client/http/v;

    return-object v0
.end method

.method public final d()Lcom/google/api/client/json/f;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->e:Lcom/google/api/client/json/f;

    return-object v0
.end method

.method public final e()Lcom/google/api/client/http/i;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->f:Lcom/google/api/client/http/i;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "root URL cannot be used if base URL is used."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->g:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/google/api/client/http/b/b;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "service path cannot be used if base URL is used."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->h:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/google/api/client/http/b/e;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->b:Lcom/google/api/client/http/b/e;

    return-object v0
.end method

.method public final i()Lcom/google/api/client/http/s;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->c:Lcom/google/api/client/http/s;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/b;->i:Ljava/lang/String;

    return-object v0
.end method
