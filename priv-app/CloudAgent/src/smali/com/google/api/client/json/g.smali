.class public abstract Lcom/google/api/client/json/g;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;
    .locals 8

    invoke-static {p3, p2}, Lcom/google/api/client/util/l;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/Class;

    :goto_0
    instance-of v2, v1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-static {v0}, Lcom/google/api/client/util/aa;->a(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/Class;

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/api/client/json/h;->a:[I

    invoke-virtual {v2}, Lcom/google/api/client/json/JsonToken;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": unexpected JSON node type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    invoke-static {v1}, Lcom/google/api/client/util/aa;->a(Ljava/lang/reflect/Type;)Z

    move-result v4

    if-eqz v1, :cond_2

    if-nez v4, :cond_2

    if-eqz v0, :cond_6

    const-class v2, Ljava/util/Collection;

    invoke-static {v0, v2}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    const/4 v2, 0x1

    :goto_1
    const-string v3, "%s: expected collection or array type but got %s for field %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v6, 0x2

    aput-object p1, v5, v6

    invoke-static {v2, v3, v5}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x0

    if-eqz p5, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p5, p4, p1}, Lcom/google/api/client/json/a;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/util/Collection;

    move-result-object v3

    :cond_3
    if-nez v3, :cond_4

    invoke-static {v1}, Lcom/google/api/client/util/l;->b(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v3

    :cond_4
    const/4 v2, 0x0

    if-eqz v4, :cond_7

    invoke-static {v1}, Lcom/google/api/client/util/aa;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    :goto_2
    invoke-static {p3, v0}, Lcom/google/api/client/util/l;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, v3, v0, p3, p5}, Lcom/google/api/client/json/g;->a(Ljava/util/Collection;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Lcom/google/api/client/json/a;)V

    if-eqz v4, :cond_8

    invoke-static {p3, v0}, Lcom/google/api/client/util/aa;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/api/client/util/aa;->a(Ljava/util/Collection;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    :cond_5
    :goto_3
    return-object v2

    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    :cond_7
    if-eqz v0, :cond_2e

    const-class v5, Ljava/lang/Iterable;

    invoke-virtual {v5, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-static {v1}, Lcom/google/api/client/util/aa;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_2

    :cond_8
    move-object v2, v3

    goto :goto_3

    :pswitch_1
    invoke-static {v1}, Lcom/google/api/client/util/aa;->a(Ljava/lang/reflect/Type;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x1

    :goto_4
    const-string v3, "%s: expected object or map type but got %s for field %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    if-eqz v0, :cond_9

    if-eqz p5, :cond_9

    invoke-virtual {p5, p4, v0}, Lcom/google/api/client/json/a;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    :cond_9
    if-eqz v0, :cond_e

    const-class v3, Ljava/util/Map;

    invoke-static {v0, v3}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    :goto_5
    if-nez v2, :cond_b

    if-nez v3, :cond_a

    if-nez v0, :cond_f

    :cond_a
    invoke-static {v0}, Lcom/google/api/client/util/l;->b(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v2

    :cond_b
    :goto_6
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v1, :cond_c

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    if-eqz v3, :cond_11

    const-class v3, Lcom/google/api/client/util/GenericData;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_11

    const-class v3, Ljava/util/Map;

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {v1}, Lcom/google/api/client/util/aa;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    move-object v3, v0

    :goto_7
    if-eqz v3, :cond_11

    move-object v0, v2

    check-cast v0, Ljava/util/Map;

    invoke-direct {p0, v0, v3, p3, p5}, Lcom/google/api/client/json/g;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Lcom/google/api/client/json/a;)V

    goto :goto_3

    :cond_d
    const/4 v2, 0x0

    goto :goto_4

    :cond_e
    const/4 v3, 0x0

    goto :goto_5

    :cond_f
    invoke-static {v0}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_6

    :cond_10
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_7

    :cond_11
    invoke-direct {p0, p3, v2, p5}, Lcom/google/api/client/json/g;->a(Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)V

    if-eqz v1, :cond_5

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_3

    :pswitch_2
    if-eqz v1, :cond_12

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v0, v3, :cond_12

    if-eqz v0, :cond_13

    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    const/4 v0, 0x1

    :goto_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%s: expected type Boolean or boolean but got %s for field %s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/api/client/json/JsonToken;->VALUE_TRUE:Lcom/google/api/client/json/JsonToken;

    if-ne v2, v0, :cond_14

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_9
    move-object v2, v0

    goto/16 :goto_3

    :cond_13
    const/4 v0, 0x0

    goto :goto_8

    :cond_14
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_9

    :pswitch_3
    if-eqz p1, :cond_15

    const-class v2, Lcom/google/api/client/json/i;

    invoke-virtual {p1, v2}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v2

    if-nez v2, :cond_17

    :cond_15
    const/4 v2, 0x1

    :goto_a
    const-string v3, "%s: number type formatted as a JSON number cannot use @JsonString annotation on the field %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_16

    const-class v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_16
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->q()Ljava/math/BigDecimal;

    move-result-object v2

    goto/16 :goto_3

    :cond_17
    const/4 v2, 0x0

    goto :goto_a

    :cond_18
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_19

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->n()Ljava/math/BigInteger;

    move-result-object v2

    goto/16 :goto_3

    :cond_19
    const-class v2, Lcom/google/common/primitives/UnsignedInteger;

    if-ne v0, v2, :cond_1a

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->o()Lcom/google/common/primitives/UnsignedInteger;

    move-result-object v2

    goto/16 :goto_3

    :cond_1a
    const-class v2, Lcom/google/common/primitives/UnsignedLong;

    if-ne v0, v2, :cond_1b

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->p()Lcom/google/common/primitives/UnsignedLong;

    move-result-object v2

    goto/16 :goto_3

    :cond_1b
    const-class v2, Ljava/lang/Double;

    if-eq v0, v2, :cond_1c

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1d

    :cond_1c
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->m()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto/16 :goto_3

    :cond_1d
    const-class v2, Ljava/lang/Long;

    if-eq v0, v2, :cond_1e

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1f

    :cond_1e
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_3

    :cond_1f
    const-class v2, Ljava/lang/Float;

    if-eq v0, v2, :cond_20

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_21

    :cond_20
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->k()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto/16 :goto_3

    :cond_21
    const-class v2, Ljava/lang/Integer;

    if-eq v0, v2, :cond_22

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_23

    :cond_22
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_3

    :cond_23
    const-class v2, Ljava/lang/Short;

    if-eq v0, v2, :cond_24

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_25

    :cond_24
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->i()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    goto/16 :goto_3

    :cond_25
    const-class v2, Ljava/lang/Byte;

    if-eq v0, v2, :cond_26

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_27

    :cond_26
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->h()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto/16 :goto_3

    :cond_27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": expected numeric type but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    if-eqz v0, :cond_28

    const-class v2, Ljava/lang/Number;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_28

    if-eqz p1, :cond_29

    const-class v0, Lcom/google/api/client/json/i;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_29

    :cond_28
    const/4 v0, 0x1

    :goto_b
    const-string v2, "%s: number field formatted as a JSON string must use the @JsonString annotation: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto/16 :goto_3

    :cond_29
    const/4 v0, 0x0

    goto :goto_b

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for field "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :pswitch_5
    if-eqz v0, :cond_2a

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-nez v2, :cond_2b

    :cond_2a
    const/4 v2, 0x1

    :goto_c
    const-string v3, "%s: primitive number field but found a JSON null: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/common/base/ah;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_2d

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    and-int/lit16 v2, v2, 0x600

    if-eqz v2, :cond_2d

    const-class v2, Ljava/util/Collection;

    invoke-static {v0, v2}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-static {v1}, Lcom/google/api/client/util/l;->b(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_3

    :cond_2b
    const/4 v2, 0x0

    goto :goto_c

    :cond_2c
    const-class v2, Ljava/util/Map;

    invoke-static {v0, v2}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-static {v0}, Lcom/google/api/client/util/l;->b(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_3

    :cond_2d
    invoke-static {p3, v1}, Lcom/google/api/client/util/aa;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/util/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto/16 :goto_3

    :cond_2e
    move-object v0, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)V
    .locals 11

    const/4 v10, 0x0

    instance-of v0, p2, Lcom/google/api/client/json/b;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/api/client/json/b;

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->a()Lcom/google/api/client/json/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/b;->a(Lcom/google/api/client/json/d;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/api/client/json/g;->s()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/util/h;->a(Ljava/lang/Class;)Lcom/google/api/client/util/h;

    move-result-object v7

    const-class v2, Lcom/google/api/client/util/GenericData;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    if-nez v8, :cond_3

    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_3

    check-cast p2, Ljava/util/Map;

    invoke-static {v1}, Lcom/google/api/client/util/aa;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-direct {p0, p2, v0, p1, p3}, Lcom/google/api/client/json/g;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Lcom/google/api/client/json/a;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v6}, Lcom/google/api/client/util/q;->a()Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Lcom/google/api/client/util/q;->d()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v6, p2, v0}, Lcom/google/api/client/util/q;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    :cond_3
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    if-eqz p3, :cond_4

    invoke-virtual {p3, p2, v9}, Lcom/google/api/client/json/a;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_4
    invoke-virtual {v7, v9}, Lcom/google/api/client/util/h;->a(Ljava/lang/String;)Lcom/google/api/client/util/q;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/google/api/client/util/q;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/google/api/client/util/q;->f()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "final array/object fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-eqz v8, :cond_6

    move-object v6, p2

    check-cast v6, Lcom/google/api/client/util/GenericData;

    move-object v0, p0

    move-object v1, v10

    move-object v2, v10

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v9, v0}, Lcom/google/api/client/util/GenericData;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual {p3, p2, v9}, Lcom/google/api/client/json/a;->b(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->f()Lcom/google/api/client/json/g;

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Lcom/google/api/client/json/a;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/api/client/json/g;->s()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Lcom/google/api/client/json/a;)V
    .locals 7

    invoke-direct {p0}, Lcom/google/api/client/json/g;->s()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    if-eqz p4, :cond_1

    invoke-virtual {p4, p1, v6}, Lcom/google/api/client/json/a;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Lcom/google/api/client/json/JsonToken;
    .locals 3

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->d()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v2, "no JSON input found"

    invoke-static {v0, v2}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private s()Lcom/google/api/client/json/JsonToken;
    .locals 3

    invoke-direct {p0}, Lcom/google/api/client/json/g;->r()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/api/client/json/h;->a:[I

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonToken;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v1

    sget-object v0, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    if-ne v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a()Lcom/google/api/client/json/d;
.end method

.method public final a(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/api/client/json/g;->b(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->b()V

    throw v0
.end method

.method public a(Ljava/lang/reflect/Type;ZLcom/google/api/client/json/a;)Ljava/lang/Object;
    .locals 6

    :try_start_0
    invoke-direct {p0}, Lcom/google/api/client/json/g;->r()Lcom/google/api/client/json/JsonToken;

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;Lcom/google/api/client/json/a;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->b()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->b()V

    :cond_1
    throw v0
.end method

.method public final a(Ljava/util/Set;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/api/client/json/g;->s()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/api/client/json/g;->f()Lcom/google/api/client/json/g;

    invoke-virtual {p0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/g;->a(Ljava/util/Set;)Ljava/lang/String;

    return-void
.end method

.method public final b(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/json/g;->r()Lcom/google/api/client/json/JsonToken;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/api/client/json/g;->a(Ljava/lang/reflect/Type;ZLcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()V
.end method

.method public abstract c()Lcom/google/api/client/json/JsonToken;
.end method

.method public abstract d()Lcom/google/api/client/json/JsonToken;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Lcom/google/api/client/json/g;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()B
.end method

.method public abstract i()S
.end method

.method public abstract j()I
.end method

.method public abstract k()F
.end method

.method public abstract l()J
.end method

.method public abstract m()D
.end method

.method public abstract n()Ljava/math/BigInteger;
.end method

.method public abstract o()Lcom/google/common/primitives/UnsignedInteger;
.end method

.method public abstract p()Lcom/google/common/primitives/UnsignedLong;
.end method

.method public abstract q()Ljava/math/BigDecimal;
.end method
