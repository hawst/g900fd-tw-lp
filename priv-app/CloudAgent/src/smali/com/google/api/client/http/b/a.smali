.class public Lcom/google/api/client/http/b/a;
.super Ljava/lang/Object;


# static fields
.field static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Lcom/google/api/client/http/r;

.field private final c:Lcom/google/api/client/http/b/e;

.field private final d:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/api/client/json/d;

.field private final i:Lcom/google/api/client/json/f;

.field private final j:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/client/http/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/api/client/http/b/a;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/b/e;Lcom/google/api/client/http/s;Lcom/google/api/client/json/d;Lcom/google/api/client/json/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/api/client/http/b/a;->c:Lcom/google/api/client/http/b/e;

    invoke-static {p6}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/b/a;->d:Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iput-object p7, p0, Lcom/google/api/client/http/b/a;->g:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/http/b/a;->h:Lcom/google/api/client/json/d;

    iput-object p5, p0, Lcom/google/api/client/http/b/a;->i:Lcom/google/api/client/json/f;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/http/v;->a()Lcom/google/api/client/http/r;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/api/client/http/b/a;->b:Lcom/google/api/client/http/r;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/api/client/http/b/a;->j:Z

    iput-object v1, p0, Lcom/google/api/client/http/b/a;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/api/client/http/b/a;->f:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1, p3}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/b/e;Lcom/google/api/client/http/s;Lcom/google/api/client/json/d;Lcom/google/api/client/json/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/api/client/http/b/a;->c:Lcom/google/api/client/http/b/e;

    const-string v0, "root URL cannot be null."

    invoke-static {p6, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/b/a;->e:Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "root URL must end with a \"/\"."

    invoke-static {v0, v3}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-string v0, "service path cannot be null"

    invoke-static {p7, v0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p7}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v1, :cond_1

    const-string v0, "/"

    invoke-virtual {v0, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "service path must equal \"/\" if it is of length 1."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-string p7, ""

    :cond_0
    :goto_0
    iput-object p7, p0, Lcom/google/api/client/http/b/a;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/api/client/http/b/a;->g:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/http/b/a;->h:Lcom/google/api/client/json/d;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p5, p0, Lcom/google/api/client/http/b/a;->i:Lcom/google/api/client/json/f;

    if-nez p3, :cond_3

    invoke-virtual {p1}, Lcom/google/api/client/http/v;->a()Lcom/google/api/client/http/r;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/api/client/http/b/a;->b:Lcom/google/api/client/http/r;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/api/client/http/b/a;->d:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/api/client/http/b/a;->j:Z

    return-void

    :cond_1
    invoke-virtual {p7}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "/"

    invoke-virtual {p7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "/"

    invoke-virtual {p7, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v1, "service path must end with a \"/\" and not begin with a \"/\"."

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    invoke-virtual {p1, p3}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/api/client/http/r;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/a;->b:Lcom/google/api/client/http/r;

    return-object v0
.end method

.method protected a(Lcom/google/api/client/http/b/d;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/http/b/a;->b()Lcom/google/api/client/http/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/http/b/a;->b()Lcom/google/api/client/http/b/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/api/client/http/b/e;->a(Lcom/google/api/client/http/b/d;)V

    :cond_0
    return-void
.end method

.method public final b()Lcom/google/api/client/http/b/e;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/a;->c:Lcom/google/api/client/http/b/e;

    return-object v0
.end method
