.class public Lcom/google/api/client/auth/oauth2/g;
.super Lcom/google/api/client/auth/oauth2/i;


# instance fields
.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "refresh_token"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/i;Ljava/lang/String;)V
    .locals 1

    const-string v0, "refresh_token"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/api/client/auth/oauth2/i;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/i;Ljava/lang/String;)V

    invoke-virtual {p0, p4}, Lcom/google/api/client/auth/oauth2/g;->b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/g;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/g;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/i;->b(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/i;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/g;

    return-object v0
.end method

.method public a(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/g;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/i;->b(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/i;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/g;

    return-object v0
.end method

.method public a(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/g;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/i;->b(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/i;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/g;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/g;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/i;->c(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/i;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/g;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/g;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/g;->c:Ljava/lang/String;

    return-object p0
.end method

.method public synthetic b(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/auth/oauth2/g;->a(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/auth/oauth2/g;->a(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/auth/oauth2/g;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/auth/oauth2/g;->a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    return-object v0
.end method
