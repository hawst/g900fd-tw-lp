.class public abstract Lcom/google/api/client/json/e;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(D)V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Lcom/google/common/primitives/UnsignedInteger;)V
.end method

.method public abstract a(Lcom/google/common/primitives/UnsignedLong;)V
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1}, Lcom/google/api/client/util/l;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/json/e;->d()V

    goto :goto_0

    :cond_1
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/api/client/json/e;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    instance-of v1, p1, Ljava/math/BigDecimal;

    if-eqz v1, :cond_3

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p0, p1}, Lcom/google/api/client/json/e;->a(Ljava/math/BigDecimal;)V

    goto :goto_0

    :cond_3
    instance-of v1, p1, Ljava/math/BigInteger;

    if-eqz v1, :cond_4

    check-cast p1, Ljava/math/BigInteger;

    invoke-virtual {p0, p1}, Lcom/google/api/client/json/e;->a(Ljava/math/BigInteger;)V

    goto :goto_0

    :cond_4
    instance-of v1, p1, Lcom/google/common/primitives/UnsignedInteger;

    if-eqz v1, :cond_5

    check-cast p1, Lcom/google/common/primitives/UnsignedInteger;

    invoke-virtual {p0, p1}, Lcom/google/api/client/json/e;->a(Lcom/google/common/primitives/UnsignedInteger;)V

    goto :goto_0

    :cond_5
    instance-of v1, p1, Lcom/google/common/primitives/UnsignedLong;

    if-eqz v1, :cond_6

    check-cast p1, Lcom/google/common/primitives/UnsignedLong;

    invoke-virtual {p0, p1}, Lcom/google/api/client/json/e;->a(Lcom/google/common/primitives/UnsignedLong;)V

    goto :goto_0

    :cond_6
    instance-of v1, p1, Ljava/lang/Double;

    if-eqz v1, :cond_7

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/json/e;->a(D)V

    goto :goto_0

    :cond_7
    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_8

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/api/client/json/e;->a(J)V

    goto :goto_0

    :cond_8
    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_9

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->a(F)V

    goto :goto_0

    :cond_9
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_a

    instance-of v1, p1, Ljava/lang/Short;

    if-nez v1, :cond_a

    instance-of v1, p1, Ljava/lang/Byte;

    if-eqz v1, :cond_b

    :cond_a
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->a(I)V

    goto :goto_0

    :cond_b
    instance-of v1, p1, Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->a(Z)V

    goto/16 :goto_0

    :cond_c
    instance-of v1, p1, Lcom/google/api/client/util/DateTime;

    if-eqz v1, :cond_d

    check-cast p1, Lcom/google/api/client/util/DateTime;

    invoke-virtual {p1}, Lcom/google/api/client/util/DateTime;->toStringRfc3339()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    instance-of v1, p1, Ljava/lang/Iterable;

    if-nez v1, :cond_e

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_e
    invoke-virtual {p0}, Lcom/google/api/client/json/e;->e()V

    invoke-static {p1}, Lcom/google/api/client/util/aa;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/api/client/json/e;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_f
    invoke-virtual {p0}, Lcom/google/api/client/json/e;->b()V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v1

    if-eqz v1, :cond_12

    check-cast p1, Ljava/lang/Enum;

    invoke-static {p1}, Lcom/google/api/client/util/q;->a(Ljava/lang/Enum;)Lcom/google/api/client/util/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/q;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/google/api/client/json/e;->d()V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/api/client/json/e;->f()V

    invoke-static {v0}, Lcom/google/api/client/util/h;->a(Ljava/lang/Class;)Lcom/google/api/client/util/h;

    move-result-object v2

    invoke-static {p1}, Lcom/google/api/client/util/l;->b(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_13
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_14

    invoke-virtual {v2, v0}, Lcom/google/api/client/util/h;->b(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    if-eqz v4, :cond_14

    const-class v5, Lcom/google/api/client/json/i;

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v4

    if-eqz v4, :cond_14

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_14
    invoke-virtual {p0, v0}, Lcom/google/api/client/json/e;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/api/client/json/e;->a(Ljava/lang/Object;)V

    goto :goto_2

    :cond_15
    invoke-virtual {p0}, Lcom/google/api/client/json/e;->c()V

    goto/16 :goto_0
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/math/BigDecimal;)V
.end method

.method public abstract a(Ljava/math/BigInteger;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public g()V
    .locals 0

    return-void
.end method
