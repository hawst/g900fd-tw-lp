.class public Lcom/google/api/client/http/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/api/client/http/c;


# instance fields
.field a:J

.field private b:I

.field private final c:I

.field private final d:D

.field private final e:D

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>()V
    .locals 8

    const/16 v1, 0x1f4

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    const v6, 0xea60

    const v7, 0xdbba0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/api/client/http/f;-><init>(IDDII)V

    return-void
.end method

.method constructor <init>(IDDII)V
    .locals 8

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    const-wide/16 v4, 0x0

    cmpg-double v0, v4, p2

    if-gtz v0, :cond_1

    cmpg-double v0, p2, v6

    if-gez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    cmpl-double v0, p4, v6

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    if-lt p6, p1, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    if-lez p7, :cond_4

    :goto_4
    invoke-static {v1}, Lcom/google/common/base/ah;->a(Z)V

    iput p1, p0, Lcom/google/api/client/http/f;->c:I

    iput-wide p2, p0, Lcom/google/api/client/http/f;->d:D

    iput-wide p4, p0, Lcom/google/api/client/http/f;->e:D

    iput p6, p0, Lcom/google/api/client/http/f;->f:I

    iput p7, p0, Lcom/google/api/client/http/f;->g:I

    invoke-virtual {p0}, Lcom/google/api/client/http/f;->a()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method

.method static a(DDI)I
    .locals 6

    int-to-double v0, p4

    mul-double/2addr v0, p0

    int-to-double v2, p4

    sub-double/2addr v2, v0

    int-to-double v4, p4

    add-double/2addr v0, v4

    sub-double/2addr v0, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v4

    mul-double/2addr v0, p2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static d()Lcom/google/api/client/http/g;
    .locals 1

    new-instance v0, Lcom/google/api/client/http/g;

    invoke-direct {v0}, Lcom/google/api/client/http/g;-><init>()V

    return-object v0
.end method

.method private e()V
    .locals 6

    iget v0, p0, Lcom/google/api/client/http/f;->b:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/api/client/http/f;->f:I

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/google/api/client/http/f;->e:D

    div-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/api/client/http/f;->f:I

    iput v0, p0, Lcom/google/api/client/http/f;->b:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/api/client/http/f;->b:I

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/google/api/client/http/f;->e:D

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/api/client/http/f;->b:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget v0, p0, Lcom/google/api/client/http/f;->c:I

    iput v0, p0, Lcom/google/api/client/http/f;->b:I

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/api/client/http/f;->a:J

    return-void
.end method

.method public a(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()J
    .locals 5

    invoke-virtual {p0}, Lcom/google/api/client/http/f;->c()J

    move-result-wide v0

    iget v2, p0, Lcom/google/api/client/http/f;->g:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/api/client/http/f;->d:D

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    iget v4, p0, Lcom/google/api/client/http/f;->b:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/api/client/http/f;->a(DDI)I

    move-result v0

    invoke-direct {p0}, Lcom/google/api/client/http/f;->e()V

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/api/client/http/f;->a:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method
