.class public Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;
.super Lcom/google/api/client/util/GenericData;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Lcom/google/api/client/http/v;

.field public b:Lcom/google/api/client/json/d;

.field public c:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "grant_type"
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "client_id"
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/util/GenericData;-><init>()V

    sget-object v0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;->NONE:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->c:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest$GrantType;

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/v;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->a:Lcom/google/api/client/http/v;

    invoke-static {p2}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->b:Lcom/google/api/client/json/d;

    invoke-static {p3}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->f:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;)V

    invoke-static {p4}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p4}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/api/client/http/t;
    .locals 4

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->a:Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Lcom/google/api/client/http/v;->a()Lcom/google/api/client/http/r;

    move-result-object v0

    new-instance v1, Lcom/google/api/client/http/i;

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/api/client/http/i;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/google/api/client/http/aa;

    invoke-direct {v2, p0}, Lcom/google/api/client/http/aa;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/http/r;->a(Lcom/google/api/client/http/i;Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;

    move-result-object v0

    new-instance v1, Lcom/google/api/client/http/b/c;

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->b:Lcom/google/api/client/json/d;

    invoke-direct {v1, v2}, Lcom/google/api/client/http/b/c;-><init>(Lcom/google/api/client/json/d;)V

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/o;)V

    iget-boolean v1, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/api/client/http/p;->g()Lcom/google/api/client/http/l;

    move-result-object v1

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/http/l;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/api/client/http/p;->n()Lcom/google/api/client/http/t;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v1, "client_secret"

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->e:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b()Lcom/google/api/client/auth/oauth2/draft10/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenRequest;->a()Lcom/google/api/client/http/t;

    move-result-object v0

    const-class v1, Lcom/google/api/client/auth/oauth2/draft10/c;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/draft10/c;

    return-object v0
.end method
