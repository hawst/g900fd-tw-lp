.class public Lcom/google/api/client/auth/oauth2/i;
.super Lcom/google/api/client/util/GenericData;


# instance fields
.field a:Lcom/google/api/client/http/s;

.field b:Lcom/google/api/client/http/k;

.field private final c:Lcom/google/api/client/http/v;

.field private final d:Lcom/google/api/client/json/d;

.field private e:Lcom/google/api/client/http/i;

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "grant_type"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/i;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/util/GenericData;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/v;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/i;->c:Lcom/google/api/client/http/v;

    invoke-static {p2}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/i;->d:Lcom/google/api/client/json/d;

    invoke-virtual {p0, p3}, Lcom/google/api/client/auth/oauth2/i;->b(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/i;

    invoke-virtual {p0, p4}, Lcom/google/api/client/auth/oauth2/i;->c(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/i;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/api/client/http/t;
    .locals 3

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/i;->c:Lcom/google/api/client/http/v;

    new-instance v1, Lcom/google/api/client/auth/oauth2/j;

    invoke-direct {v1, p0}, Lcom/google/api/client/auth/oauth2/j;-><init>(Lcom/google/api/client/auth/oauth2/i;)V

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/i;->e:Lcom/google/api/client/http/i;

    new-instance v2, Lcom/google/api/client/http/aa;

    invoke-direct {v2, p0}, Lcom/google/api/client/http/aa;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/http/r;->a(Lcom/google/api/client/http/i;Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;

    move-result-object v0

    new-instance v1, Lcom/google/api/client/json/f;

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/i;->d:Lcom/google/api/client/json/d;

    invoke-direct {v1, v2}, Lcom/google/api/client/json/f;-><init>(Lcom/google/api/client/json/d;)V

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/util/y;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/p;->a(Z)Lcom/google/api/client/http/p;

    invoke-virtual {v0}, Lcom/google/api/client/http/p;->n()Lcom/google/api/client/http/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/t;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/i;->d:Lcom/google/api/client/json/d;

    invoke-static {v1, v0}, Lcom/google/api/client/auth/oauth2/TokenResponseException;->from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/auth/oauth2/TokenResponseException;

    move-result-object v0

    throw v0
.end method

.method public b(Lcom/google/api/client/http/i;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/i;->e:Lcom/google/api/client/http/i;

    invoke-virtual {p1}, Lcom/google/api/client/http/i;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/i;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/i;->b:Lcom/google/api/client/http/k;

    return-object p0
.end method

.method public b(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/i;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/i;->a:Lcom/google/api/client/http/s;

    return-object p0
.end method

.method public b()Lcom/google/api/client/auth/oauth2/l;
    .locals 2

    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/i;->a()Lcom/google/api/client/http/t;

    move-result-object v0

    const-class v1, Lcom/google/api/client/auth/oauth2/l;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/t;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/l;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/i;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/i;->f:Ljava/lang/String;

    return-object p0
.end method
