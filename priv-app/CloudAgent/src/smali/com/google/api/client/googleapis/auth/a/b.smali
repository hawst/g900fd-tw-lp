.class public Lcom/google/api/client/googleapis/auth/a/b;
.super Lcom/google/api/client/auth/oauth2/e;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/security/PrivateKey;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lcom/google/api/client/auth/oauth2/a;->a()Lcom/google/api/client/auth/oauth2/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/api/client/auth/oauth2/e;-><init>(Lcom/google/api/client/auth/oauth2/d;)V

    const-string v0, "https://accounts.google.com/o/oauth2/token"

    invoke-virtual {p0, v0}, Lcom/google/api/client/googleapis/auth/a/b;->b(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/b;

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/google/api/client/http/v;)Lcom/google/api/client/auth/oauth2/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/b;->b(Lcom/google/api/client/http/v;)Lcom/google/api/client/googleapis/auth/a/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/api/client/json/d;)Lcom/google/api/client/auth/oauth2/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/b;->b(Lcom/google/api/client/json/d;)Lcom/google/api/client/googleapis/auth/a/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/b;->b(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/b;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/api/client/http/v;)Lcom/google/api/client/googleapis/auth/a/b;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/e;->a(Lcom/google/api/client/http/v;)Lcom/google/api/client/auth/oauth2/e;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/b;

    return-object v0
.end method

.method public b(Lcom/google/api/client/json/d;)Lcom/google/api/client/googleapis/auth/a/b;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/e;->a(Lcom/google/api/client/json/d;)Lcom/google/api/client/auth/oauth2/e;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/b;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/b;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/e;->a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/e;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/b;

    return-object v0
.end method

.method public i()Lcom/google/api/client/googleapis/auth/a/a;
    .locals 13

    new-instance v0, Lcom/google/api/client/googleapis/auth/a/a;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->a()Lcom/google/api/client/auth/oauth2/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->b()Lcom/google/api/client/http/v;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->d()Lcom/google/api/client/json/d;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->e()Lcom/google/api/client/http/i;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->f()Lcom/google/api/client/http/k;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->g()Lcom/google/api/client/http/s;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->h()Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/google/api/client/googleapis/auth/a/b;->a:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/api/client/googleapis/auth/a/b;->b:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/api/client/googleapis/auth/a/b;->c:Ljava/security/PrivateKey;

    iget-object v11, p0, Lcom/google/api/client/googleapis/auth/a/b;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->c()Lcom/google/api/client/util/j;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/google/api/client/googleapis/auth/a/a;-><init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/j;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/b;->e()Lcom/google/api/client/http/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/http/i;->c()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
