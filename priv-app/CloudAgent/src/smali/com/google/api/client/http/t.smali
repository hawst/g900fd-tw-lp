.class public final Lcom/google/api/client/http/t;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/google/api/client/http/z;

.field private b:Ljava/io/InputStream;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/api/client/http/n;

.field private final f:Lcom/google/api/client/http/l;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/api/client/http/v;

.field private final j:Lcom/google/api/client/http/p;

.field private k:I

.field private l:Z

.field private m:Z


# direct methods
.method constructor <init>(Lcom/google/api/client/http/p;Lcom/google/api/client/http/z;)V
    .locals 8

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/api/client/http/t;->j:Lcom/google/api/client/http/p;

    invoke-virtual {p1}, Lcom/google/api/client/http/p;->a()Lcom/google/api/client/http/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/http/t;->i:Lcom/google/api/client/http/v;

    invoke-virtual {p1}, Lcom/google/api/client/http/p;->h()Lcom/google/api/client/http/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/http/t;->f:Lcom/google/api/client/http/l;

    invoke-virtual {p1}, Lcom/google/api/client/http/p;->e()I

    move-result v0

    iput v0, p0, Lcom/google/api/client/http/t;->k:I

    invoke-virtual {p1}, Lcom/google/api/client/http/p;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/api/client/http/t;->l:Z

    iput-object p2, p0, Lcom/google/api/client/http/t;->a:Lcom/google/api/client/http/z;

    invoke-virtual {p2}, Lcom/google/api/client/http/z;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/http/t;->c:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/api/client/http/z;->e()I

    move-result v2

    iput v2, p0, Lcom/google/api/client/http/t;->g:I

    invoke-virtual {p2}, Lcom/google/api/client/http/z;->f()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/api/client/http/t;->h:Ljava/lang/String;

    sget-object v5, Lcom/google/api/client/http/v;->a:Ljava/util/logging/Logger;

    iget-boolean v0, p0, Lcom/google/api/client/http/t;->l:Z

    if-eqz v0, :cond_3

    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v5, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-------------- RESPONSE --------------"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/google/api/client/http/z;->d()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_1
    sget-object v2, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    iget-object v4, p0, Lcom/google/api/client/http/t;->f:Lcom/google/api/client/http/l;

    if-eqz v3, :cond_5

    move-object v2, v0

    :goto_3
    invoke-virtual {v4, p2, v2}, Lcom/google/api/client/http/l;->a(Lcom/google/api/client/http/z;Ljava/lang/StringBuilder;)V

    invoke-virtual {p2}, Lcom/google/api/client/http/z;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/api/client/http/t;->f:Lcom/google/api/client/http/l;

    invoke-virtual {v2}, Lcom/google/api/client/http/l;->c()Ljava/lang/String;

    move-result-object v2

    :cond_1
    iput-object v2, p0, Lcom/google/api/client/http/t;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_4
    iput-object v1, p0, Lcom/google/api/client/http/t;->e:Lcom/google/api/client/http/n;

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-eqz v4, :cond_0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    move-object v2, v1

    goto :goto_3

    :cond_6
    new-instance v1, Lcom/google/api/client/http/n;

    invoke-direct {v1, v2}, Lcom/google/api/client/http/n;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/google/api/client/http/t;->j:Lcom/google/api/client/http/p;

    invoke-virtual {v0}, Lcom/google/api/client/http/p;->k()Lcom/google/api/client/util/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->k()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/google/api/client/util/y;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/api/client/http/t;->i()Lcom/google/api/client/http/o;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->g()V

    iget-object v0, p0, Lcom/google/api/client/http/t;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Missing Content-Type header in response"

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No parser defined for Content-Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/api/client/http/t;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {v0, p0, p1}, Lcom/google/api/client/http/o;->a(Lcom/google/api/client/http/t;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/t;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/google/api/client/http/l;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/t;->f:Lcom/google/api/client/http/l;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Lcom/google/api/client/http/t;->g:I

    invoke-static {v0}, Lcom/google/api/client/http/u;->a(I)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/api/client/http/t;->g:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/t;->h:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/io/InputStream;
    .locals 5

    iget-boolean v0, p0, Lcom/google/api/client/http/t;->m:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/api/client/http/t;->a:Lcom/google/api/client/http/z;

    invoke-virtual {v0}, Lcom/google/api/client/http/z;->a()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/http/t;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, "gzip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v1, v0

    :cond_0
    sget-object v2, Lcom/google/api/client/http/v;->a:Ljava/util/logging/Logger;

    iget-boolean v0, p0, Lcom/google/api/client/http/t;->l:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v2, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/api/client/util/v;

    sget-object v3, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    iget v4, p0, Lcom/google/api/client/http/t;->k:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/api/client/util/v;-><init>(Ljava/io/InputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    move-object v1, v0

    :cond_1
    iput-object v1, p0, Lcom/google/api/client/http/t;->b:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/api/client/http/t;->m:Z

    :cond_3
    iget-object v0, p0, Lcom/google/api/client/http/t;->b:Ljava/io/InputStream;

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public g()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->g()V

    iget-object v0, p0, Lcom/google/api/client/http/t;->a:Lcom/google/api/client/http/z;

    invoke-virtual {v0}, Lcom/google/api/client/http/z;->h()V

    return-void
.end method

.method public i()Lcom/google/api/client/http/o;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/api/client/http/t;->j:Lcom/google/api/client/http/p;

    iget-object v1, p0, Lcom/google/api/client/http/t;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/p;->a(Ljava/lang/String;)Lcom/google/api/client/http/o;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v0, v1}, Lcom/google/api/client/http/b;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Lcom/google/api/client/http/t;->k()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public k()Ljava/nio/charset/Charset;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/t;->e:Lcom/google/api/client/http/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/client/http/t;->e:Lcom/google/api/client/http/n;

    invoke-virtual {v0}, Lcom/google/api/client/http/n;->d()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/common/base/s;->b:Ljava/nio/charset/Charset;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/api/client/http/t;->e:Lcom/google/api/client/http/n;

    invoke-virtual {v0}, Lcom/google/api/client/http/n;->d()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method
