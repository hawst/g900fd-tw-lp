.class public abstract Lcom/google/api/client/http/v;
.super Ljava/lang/Object;


# static fields
.field static final a:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/api/client/http/v;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/api/client/http/r;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;
    .locals 1

    new-instance v0, Lcom/google/api/client/http/r;

    invoke-direct {v0, p0, p1}, Lcom/google/api/client/http/r;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/s;)V

    return-object v0
.end method

.method protected abstract a(Ljava/lang/String;)Lcom/google/api/client/http/y;
.end method

.method b()Lcom/google/api/client/http/p;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/api/client/http/p;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/HttpMethod;)V

    return-object v0
.end method

.method protected abstract b(Ljava/lang/String;)Lcom/google/api/client/http/y;
.end method

.method protected c(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected d(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract e(Ljava/lang/String;)Lcom/google/api/client/http/y;
.end method

.method protected abstract f(Ljava/lang/String;)Lcom/google/api/client/http/y;
.end method
