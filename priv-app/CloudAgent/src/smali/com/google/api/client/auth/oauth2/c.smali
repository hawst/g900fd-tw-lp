.class public Lcom/google/api/client/auth/oauth2/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/api/client/http/k;
.implements Lcom/google/api/client/http/s;
.implements Lcom/google/api/client/http/w;


# static fields
.field static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Ljava/util/concurrent/locks/Lock;

.field private final c:Lcom/google/api/client/auth/oauth2/d;

.field private final d:Lcom/google/api/client/util/j;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/String;

.field private final h:Lcom/google/api/client/http/v;

.field private final i:Lcom/google/api/client/http/k;

.field private final j:Lcom/google/api/client/json/d;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/util/List;

.field private final m:Lcom/google/api/client/http/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/client/auth/oauth2/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/api/client/auth/oauth2/c;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;)V
    .locals 9

    sget-object v8, Lcom/google/api/client/util/j;->a:Lcom/google/api/client/util/j;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/api/client/auth/oauth2/c;-><init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Lcom/google/api/client/util/j;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Lcom/google/api/client/util/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/d;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->c:Lcom/google/api/client/auth/oauth2/d;

    iput-object p2, p0, Lcom/google/api/client/auth/oauth2/c;->h:Lcom/google/api/client/http/v;

    iput-object p3, p0, Lcom/google/api/client/auth/oauth2/c;->j:Lcom/google/api/client/json/d;

    iput-object p4, p0, Lcom/google/api/client/auth/oauth2/c;->k:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/api/client/auth/oauth2/c;->i:Lcom/google/api/client/http/k;

    iput-object p6, p0, Lcom/google/api/client/auth/oauth2/c;->m:Lcom/google/api/client/http/s;

    if-nez p7, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->l:Ljava/util/List;

    invoke-static {p8}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/util/j;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->d:Lcom/google/api/client/util/j;

    return-void

    :cond_0
    invoke-static {p7}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p1}, Lcom/google/api/client/auth/oauth2/l;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;

    invoke-virtual {p1}, Lcom/google/api/client/auth/oauth2/l;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/auth/oauth2/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;

    :cond_0
    invoke-virtual {p1}, Lcom/google/api/client/auth/oauth2/l;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->b(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;

    return-object p0
.end method

.method public a(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/c;->f:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a(Lcom/google/api/client/http/p;)V
    .locals 4

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/c;->e()Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3c

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/c;->g()Z

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->c:Lcom/google/api/client/auth/oauth2/d;

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/api/client/auth/oauth2/d;->a(Lcom/google/api/client/http/p;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a(Lcom/google/api/client/http/p;Lcom/google/api/client/http/t;Z)Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/api/client/http/t;->d()I

    move-result v0

    const/16 v2, 0x191

    if-ne v0, v2, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/c;->c:Lcom/google/api/client/auth/oauth2/d;

    invoke-interface {v2, p1}, Lcom/google/api/client/auth/oauth2/d;->a(Lcom/google/api/client/http/p;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/common/base/aa;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/c;->g()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/api/client/auth/oauth2/c;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "unable to refresh token"

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;
    .locals 6

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->a(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->d:Lcom/google/api/client/util/j;

    invoke-interface {v0}, Lcom/google/api/client/util/j;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->j:Lcom/google/api/client/json/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->h:Lcom/google/api/client/http/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->i:Lcom/google/api/client/http/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Please use the Builder and call setJsonFactory, setTransport, setClientAuthentication and setTokenServerUrl/setTokenServerEncodedUrl"

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/c;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()Lcom/google/api/client/util/j;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->d:Lcom/google/api/client/util/j;

    return-object v0
.end method

.method public b(Lcom/google/api/client/http/p;)V
    .locals 0

    invoke-virtual {p1, p0}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/k;)Lcom/google/api/client/http/p;

    invoke-virtual {p1, p0}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/w;)Lcom/google/api/client/http/p;

    return-void
.end method

.method public final c()Lcom/google/api/client/http/v;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->h:Lcom/google/api/client/http/v;

    return-object v0
.end method

.method public final d()Lcom/google/api/client/json/d;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->j:Lcom/google/api/client/json/d;

    return-object v0
.end method

.method public final e()Ljava/lang/Long;
    .locals 4

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->f:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/c;->d:Lcom/google/api/client/util/j;

    invoke-interface {v2}, Lcom/google/api/client/util/j;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final f()Lcom/google/api/client/http/k;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->i:Lcom/google/api/client/http/k;

    return-object v0
.end method

.method public final g()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/api/client/auth/oauth2/c;->h()Lcom/google/api/client/auth/oauth2/l;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v3}, Lcom/google/api/client/auth/oauth2/c;->a(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/auth/oauth2/c;

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/f;

    invoke-interface {v0, p0, v3}, Lcom/google/api/client/auth/oauth2/f;->a(Lcom/google/api/client/auth/oauth2/c;Lcom/google/api/client/auth/oauth2/l;)V
    :try_end_0
    .catch Lcom/google/api/client/auth/oauth2/TokenResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v3, v0

    const/16 v0, 0x190

    :try_start_1
    invoke-virtual {v3}, Lcom/google/api/client/auth/oauth2/TokenResponseException;->getStatusCode()I

    move-result v4

    if-gt v0, v4, :cond_2

    invoke-virtual {v3}, Lcom/google/api/client/auth/oauth2/TokenResponseException;->getStatusCode()I

    move-result v0

    const/16 v4, 0x1f4

    if-ge v0, v4, :cond_2

    :goto_1
    invoke-virtual {v3}, Lcom/google/api/client/auth/oauth2/TokenResponseException;->getDetails()Lcom/google/api/client/auth/oauth2/h;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/api/client/auth/oauth2/c;->b(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;

    :cond_0
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/f;

    invoke-virtual {v3}, Lcom/google/api/client/auth/oauth2/TokenResponseException;->getDetails()Lcom/google/api/client/auth/oauth2/h;

    move-result-object v5

    invoke-interface {v0, p0, v5}, Lcom/google/api/client/auth/oauth2/f;->a(Lcom/google/api/client/auth/oauth2/c;Lcom/google/api/client/auth/oauth2/h;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    :goto_3
    return v0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    :try_start_2
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v2

    goto :goto_3
.end method

.method protected h()Lcom/google/api/client/auth/oauth2/l;
    .locals 5

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/c;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/api/client/auth/oauth2/g;

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->h:Lcom/google/api/client/http/v;

    iget-object v2, p0, Lcom/google/api/client/auth/oauth2/c;->j:Lcom/google/api/client/json/d;

    new-instance v3, Lcom/google/api/client/http/i;

    iget-object v4, p0, Lcom/google/api/client/auth/oauth2/c;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/api/client/http/i;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/api/client/auth/oauth2/c;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/api/client/auth/oauth2/g;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/i;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->i:Lcom/google/api/client/http/k;

    invoke-virtual {v0, v1}, Lcom/google/api/client/auth/oauth2/g;->a(Lcom/google/api/client/http/k;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/auth/oauth2/c;->m:Lcom/google/api/client/http/s;

    invoke-virtual {v0, v1}, Lcom/google/api/client/auth/oauth2/g;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/auth/oauth2/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/auth/oauth2/g;->b()Lcom/google/api/client/auth/oauth2/l;

    move-result-object v0

    goto :goto_0
.end method
