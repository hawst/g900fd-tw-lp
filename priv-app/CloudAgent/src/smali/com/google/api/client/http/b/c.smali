.class public Lcom/google/api/client/http/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/api/client/http/o;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/api/client/json/d;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/d;)V
    .locals 1

    const-string v0, "application/json"

    invoke-direct {p0, p1, v0}, Lcom/google/api/client/http/b/c;-><init>(Lcom/google/api/client/json/d;Ljava/lang/String;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/json/d;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/http/b/c;->b:Lcom/google/api/client/json/d;

    iput-object p2, p0, Lcom/google/api/client/http/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/json/g;
    .locals 2

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/api/client/json/d;->a(Ljava/io/InputStream;)Lcom/google/api/client/json/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/json/g;->c()Lcom/google/api/client/json/JsonToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/t;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/http/b/c;->b:Lcom/google/api/client/json/d;

    invoke-static {v0, p1}, Lcom/google/api/client/http/b/c;->a(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/json/g;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/api/client/json/g;->a(Ljava/lang/Class;Lcom/google/api/client/json/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/b/c;->a:Ljava/lang/String;

    return-object v0
.end method
