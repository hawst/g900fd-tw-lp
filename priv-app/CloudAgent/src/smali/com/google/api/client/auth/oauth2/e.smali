.class public Lcom/google/api/client/auth/oauth2/e;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/api/client/auth/oauth2/d;

.field private b:Lcom/google/api/client/http/v;

.field private c:Lcom/google/api/client/json/d;

.field private d:Lcom/google/api/client/http/i;

.field private e:Lcom/google/api/client/util/j;

.field private f:Lcom/google/api/client/http/k;

.field private g:Lcom/google/api/client/http/s;

.field private h:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/api/client/auth/oauth2/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/api/client/util/j;->a:Lcom/google/api/client/util/j;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->e:Lcom/google/api/client/util/j;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->h:Ljava/util/List;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/d;

    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->a:Lcom/google/api/client/auth/oauth2/d;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/api/client/auth/oauth2/d;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->a:Lcom/google/api/client/auth/oauth2/d;

    return-object v0
.end method

.method public a(Lcom/google/api/client/http/v;)Lcom/google/api/client/auth/oauth2/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/e;->b:Lcom/google/api/client/http/v;

    return-object p0
.end method

.method public a(Lcom/google/api/client/json/d;)Lcom/google/api/client/auth/oauth2/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/oauth2/e;->c:Lcom/google/api/client/json/d;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/e;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->d:Lcom/google/api/client/http/i;

    return-object p0

    :cond_0
    new-instance v0, Lcom/google/api/client/http/i;

    invoke-direct {v0, p1}, Lcom/google/api/client/http/i;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Lcom/google/api/client/http/v;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->b:Lcom/google/api/client/http/v;

    return-object v0
.end method

.method public final c()Lcom/google/api/client/util/j;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->e:Lcom/google/api/client/util/j;

    return-object v0
.end method

.method public final d()Lcom/google/api/client/json/d;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->c:Lcom/google/api/client/json/d;

    return-object v0
.end method

.method public final e()Lcom/google/api/client/http/i;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->d:Lcom/google/api/client/http/i;

    return-object v0
.end method

.method public final f()Lcom/google/api/client/http/k;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->f:Lcom/google/api/client/http/k;

    return-object v0
.end method

.method public final g()Lcom/google/api/client/http/s;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->g:Lcom/google/api/client/http/s;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/e;->h:Ljava/util/List;

    return-object v0
.end method
