.class public final Lcom/google/api/client/http/p;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/google/api/client/http/k;

.field private b:Lcom/google/api/client/http/l;

.field private c:Lcom/google/api/client/http/l;

.field private d:Z

.field private e:I

.field private f:I

.field private g:Z

.field private h:Lcom/google/api/client/http/j;

.field private final i:Lcom/google/api/client/http/v;

.field private j:Lcom/google/api/client/http/HttpMethod;

.field private k:Lcom/google/api/client/http/i;

.field private l:I

.field private m:I

.field private n:Lcom/google/api/client/http/w;

.field private final o:Ljava/util/Map;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private p:Lcom/google/api/client/util/y;

.field private q:Z

.field private r:Lcom/google/api/client/http/c;

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method constructor <init>(Lcom/google/api/client/http/v;Lcom/google/api/client/http/HttpMethod;)V
    .locals 3

    const/16 v2, 0x4e20

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/api/client/http/l;

    invoke-direct {v0}, Lcom/google/api/client/http/l;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    new-instance v0, Lcom/google/api/client/http/l;

    invoke-direct {v0}, Lcom/google/api/client/http/l;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/p;->c:Lcom/google/api/client/http/l;

    iput-boolean v1, p0, Lcom/google/api/client/http/p;->d:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/api/client/http/p;->e:I

    const/16 v0, 0x4000

    iput v0, p0, Lcom/google/api/client/http/p;->f:I

    iput-boolean v1, p0, Lcom/google/api/client/http/p;->g:Z

    iput v2, p0, Lcom/google/api/client/http/p;->l:I

    iput v2, p0, Lcom/google/api/client/http/p;->m:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/p;->o:Ljava/util/Map;

    iput-boolean v1, p0, Lcom/google/api/client/http/p;->s:Z

    iput-boolean v1, p0, Lcom/google/api/client/http/p;->t:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/api/client/http/p;->u:Z

    iput-object p1, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    iput-object p2, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    return-void
.end method

.method private a(J)V
    .locals 1

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Lcom/google/api/client/http/t;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->b()Lcom/google/api/client/http/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/l;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/api/client/http/i;

    invoke-direct {v1, v0}, Lcom/google/api/client/http/i;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/i;)Lcom/google/api/client/http/p;

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->d()I

    move-result v0

    const/16 v1, 0x12f

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/api/client/http/HttpMethod;->GET:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {p0, v0}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/p;

    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/16 v0, 0x3b

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private b(Lcom/google/api/client/http/t;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->d()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->b()Lcom/google/api/client/http/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/http/l;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/api/client/http/o;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p1}, Lcom/google/api/client/http/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/http/p;->o:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/o;

    return-object v0
.end method

.method public a(I)Lcom/google/api/client/http/p;
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iput p1, p0, Lcom/google/api/client/http/p;->l:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/p;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/HttpMethod;

    iput-object v0, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    return-object p0
.end method

.method public a(Lcom/google/api/client/http/c;)Lcom/google/api/client/http/p;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    return-object p0
.end method

.method public a(Lcom/google/api/client/http/i;)Lcom/google/api/client/http/p;
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/i;

    iput-object v0, p0, Lcom/google/api/client/http/p;->k:Lcom/google/api/client/http/i;

    return-object p0
.end method

.method public a(Lcom/google/api/client/http/j;)Lcom/google/api/client/http/p;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/p;->h:Lcom/google/api/client/http/j;

    return-object p0
.end method

.method public a(Lcom/google/api/client/http/k;)Lcom/google/api/client/http/p;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/k;

    return-object p0
.end method

.method public a(Lcom/google/api/client/http/w;)Lcom/google/api/client/http/p;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/p;->n:Lcom/google/api/client/http/w;

    return-object p0
.end method

.method public a(Z)Lcom/google/api/client/http/p;
    .locals 0

    iput-boolean p1, p0, Lcom/google/api/client/http/p;->t:Z

    return-object p0
.end method

.method public a()Lcom/google/api/client/http/v;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    return-object v0
.end method

.method public a(Lcom/google/api/client/http/o;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-interface {p1}, Lcom/google/api/client/http/o;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/http/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/client/http/p;->o:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lcom/google/api/client/util/y;)V
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/p;->p:Lcom/google/api/client/util/y;

    return-void
.end method

.method public b()Lcom/google/api/client/http/HttpMethod;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    return-object v0
.end method

.method public b(I)Lcom/google/api/client/http/p;
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iput p1, p0, Lcom/google/api/client/http/p;->m:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/google/api/client/http/i;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->k:Lcom/google/api/client/http/i;

    return-object v0
.end method

.method public c(I)Lcom/google/api/client/http/p;
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iput p1, p0, Lcom/google/api/client/http/p;->e:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/google/api/client/http/j;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->h:Lcom/google/api/client/http/j;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/google/api/client/http/p;->f:I

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/api/client/http/p;->g:Z

    return v0
.end method

.method public g()Lcom/google/api/client/http/l;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    return-object v0
.end method

.method public h()Lcom/google/api/client/http/l;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->c:Lcom/google/api/client/http/l;

    return-object v0
.end method

.method public i()Lcom/google/api/client/http/k;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/k;

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/api/client/http/p;->d:Z

    return v0
.end method

.method public final k()Lcom/google/api/client/util/y;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/p;->p:Lcom/google/api/client/util/y;

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/api/client/http/p;->s:Z

    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/api/client/http/p;->t:Z

    return v0
.end method

.method public n()Lcom/google/api/client/http/t;
    .locals 14

    iget v0, p0, Lcom/google/api/client/http/p;->e:I

    if-ltz v0, :cond_11

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iget v0, p0, Lcom/google/api/client/http/p;->e:I

    iget-object v1, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    invoke-interface {v1}, Lcom/google/api/client/http/c;->a()V

    :cond_0
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-static {v1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/api/client/http/p;->k:Lcom/google/api/client/http/i;

    invoke-static {v1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move v11, v0

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->g()V

    :cond_1
    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/k;

    invoke-interface {v0, p0}, Lcom/google/api/client/http/k;->a(Lcom/google/api/client/http/p;)V

    :cond_2
    iget-object v0, p0, Lcom/google/api/client/http/p;->k:Lcom/google/api/client/http/i;

    invoke-virtual {v0}, Lcom/google/api/client/http/i;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/api/client/http/q;->a:[I

    iget-object v2, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {v2}, Lcom/google/api/client/http/HttpMethod;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->b(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    :goto_2
    sget-object v13, Lcom/google/api/client/http/v;->a:Ljava/util/logging/Logger;

    iget-boolean v0, p0, Lcom/google/api/client/http/p;->g:Z

    if-eqz v0, :cond_12

    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v13, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    move v12, v0

    :goto_3
    const/4 v0, 0x0

    if-eqz v12, :cond_20

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-------------- REQUEST  --------------"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v8, v0

    :goto_4
    iget-object v0, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    invoke-virtual {v0}, Lcom/google/api/client/http/l;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    const-string v2, "Google-HTTP-Java-Client/1.10.3-beta (gzip)"

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/l;->b(Ljava/lang/String;)V

    :goto_5
    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    invoke-static {v1, v8, v13, v7}, Lcom/google/api/client/http/l;->a(Lcom/google/api/client/http/l;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Lcom/google/api/client/http/y;)V

    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    invoke-virtual {v1, v0}, Lcom/google/api/client/http/l;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/api/client/http/p;->h:Lcom/google/api/client/http/j;

    invoke-virtual {p0}, Lcom/google/api/client/http/p;->j()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->PUT:Lcom/google/api/client/http/HttpMethod;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->POST:Lcom/google/api/client/http/HttpMethod;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/api/client/http/p;->j:Lcom/google/api/client/http/HttpMethod;

    sget-object v2, Lcom/google/api/client/http/HttpMethod;->PATCH:Lcom/google/api/client/http/HttpMethod;

    if-ne v0, v2, :cond_5

    :cond_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/google/api/client/http/j;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_5

    :cond_4
    const/4 v0, 0x0

    const-string v1, " "

    invoke-static {v0, v1}, Lcom/google/api/client/http/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/d;

    move-result-object v1

    :cond_5
    if-eqz v1, :cond_9

    invoke-interface {v1}, Lcom/google/api/client/http/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/api/client/http/j;->b()J

    move-result-wide v4

    invoke-interface {v1}, Lcom/google/api/client/http/j;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v12, :cond_1f

    new-instance v0, Lcom/google/api/client/http/x;

    iget v6, p0, Lcom/google/api/client/http/p;->f:I

    invoke-direct/range {v0 .. v6}, Lcom/google/api/client/http/x;-><init>(Lcom/google/api/client/http/j;Ljava/lang/String;Ljava/lang/String;JI)V

    :goto_6
    iget-boolean v1, p0, Lcom/google/api/client/http/p;->q:Z

    if-eqz v1, :cond_1e

    new-instance v1, Lcom/google/api/client/http/h;

    invoke-direct {v1, v0, v2}, Lcom/google/api/client/http/h;-><init>(Lcom/google/api/client/http/j;Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/google/api/client/http/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/api/client/http/j;->b()J

    move-result-wide v4

    :goto_7
    if-eqz v12, :cond_8

    if-eqz v2, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content-Type: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    if-eqz v3, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Content-Encoding: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-ltz v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Content-Length: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {v7, v1}, Lcom/google/api/client/http/y;->a(Lcom/google/api/client/http/j;)V

    :cond_9
    if-eqz v12, :cond_a

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    :cond_a
    if-lez v11, :cond_14

    if-eqz v1, :cond_b

    invoke-interface {v1}, Lcom/google/api/client/http/j;->f()Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_b
    const/4 v0, 0x1

    :goto_8
    iget v1, p0, Lcom/google/api/client/http/p;->l:I

    iget v2, p0, Lcom/google/api/client/http/p;->m:I

    invoke-virtual {v7, v1, v2}, Lcom/google/api/client/http/y;->a(II)V

    :try_start_0
    invoke-virtual {v7}, Lcom/google/api/client/http/y;->a()Lcom/google/api/client/http/z;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :try_start_1
    new-instance v1, Lcom/google/api/client/http/t;

    invoke-direct {v1, p0, v2}, Lcom/google/api/client/http/t;-><init>(Lcom/google/api/client/http/p;Lcom/google/api/client/http/z;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v1

    move-object v1, v9

    :goto_9
    if-eqz v2, :cond_18

    :try_start_2
    invoke-virtual {v2}, Lcom/google/api/client/http/t;->c()Z

    move-result v3

    if-nez v3, :cond_18

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/api/client/http/p;->n:Lcom/google/api/client/http/w;

    if-eqz v6, :cond_c

    iget-object v5, p0, Lcom/google/api/client/http/p;->n:Lcom/google/api/client/http/w;

    invoke-interface {v5, p0, v2, v0}, Lcom/google/api/client/http/w;->a(Lcom/google/api/client/http/p;Lcom/google/api/client/http/t;Z)Z

    move-result v5

    :cond_c
    if-nez v5, :cond_d

    invoke-virtual {p0}, Lcom/google/api/client/http/p;->l()Z

    move-result v6

    if-eqz v6, :cond_16

    invoke-direct {p0, v2}, Lcom/google/api/client/http/p;->b(Lcom/google/api/client/http/t;)Z

    move-result v6

    if-eqz v6, :cond_16

    invoke-direct {p0, v2}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/t;)V

    const/4 v3, 0x1

    :cond_d
    :goto_a
    if-nez v5, :cond_e

    if-nez v3, :cond_e

    if-eqz v4, :cond_17

    :cond_e
    const/4 v3, 0x1

    :goto_b
    and-int/2addr v0, v3

    if-eqz v0, :cond_f

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_f
    :goto_c
    add-int/lit8 v3, v11, -0x1

    if-eqz v2, :cond_10

    :cond_10
    if-nez v0, :cond_1d

    if-nez v2, :cond_1b

    throw v1

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->a(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Lcom/google/api/client/http/v;->c()Z

    move-result v0

    const-string v2, "HTTP transport doesn\'t support HEAD"

    invoke-static {v0, v2}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->c(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Lcom/google/api/client/http/v;->d()Z

    move-result v0

    const-string v2, "HTTP transport doesn\'t support PATCH"

    invoke-static {v0, v2}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->d(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->e(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/google/api/client/http/p;->i:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->f(Ljava/lang/String;)Lcom/google/api/client/http/y;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_2

    :cond_12
    const/4 v0, 0x0

    move v12, v0

    goto/16 :goto_3

    :cond_13
    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/l;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Google-HTTP-Java-Client/1.10.3-beta (gzip)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/l;->b(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_8

    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v2}, Lcom/google/api/client/http/z;->a()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v1

    move-object v2, v10

    iget-boolean v3, p0, Lcom/google/api/client/http/p;->u:Z

    if-nez v3, :cond_15

    throw v1

    :cond_15
    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    :cond_16
    if-eqz v0, :cond_d

    :try_start_4
    iget-object v6, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->d()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/google/api/client/http/c;->a(I)Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/google/api/client/http/p;->r:Lcom/google/api/client/http/c;

    invoke-interface {v6}, Lcom/google/api/client/http/c;->b()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_d

    invoke-direct {p0, v6, v7}, Lcom/google/api/client/http/p;->a(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v4, 0x1

    goto/16 :goto_a

    :cond_17
    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_18
    if-nez v2, :cond_19

    const/4 v3, 0x1

    :goto_d
    and-int/2addr v0, v3

    goto/16 :goto_c

    :cond_19
    const/4 v3, 0x0

    goto :goto_d

    :catchall_1
    move-exception v0

    if-eqz v2, :cond_1a

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->h()V

    :cond_1a
    throw v0

    :cond_1b
    iget-boolean v0, p0, Lcom/google/api/client/http/p;->t:Z

    if-eqz v0, :cond_1c

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->c()Z

    move-result v0

    if-nez v0, :cond_1c

    :try_start_5
    new-instance v0, Lcom/google/api/client/http/HttpResponseException;

    invoke-direct {v0, v2}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/t;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lcom/google/api/client/http/t;->h()V

    throw v0

    :cond_1c
    return-object v2

    :cond_1d
    move v11, v3

    goto/16 :goto_1

    :cond_1e
    move-object v1, v0

    goto/16 :goto_7

    :cond_1f
    move-object v0, v1

    goto/16 :goto_6

    :cond_20
    move-object v8, v0

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
