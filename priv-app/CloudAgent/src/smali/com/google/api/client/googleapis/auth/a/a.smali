.class public Lcom/google/api/client/googleapis/auth/a/a;
.super Lcom/google/api/client/auth/oauth2/c;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/security/PrivateKey;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/api/client/auth/oauth2/a;->a()Lcom/google/api/client/auth/oauth2/d;

    move-result-object v1

    const-string v4, "https://accounts.google.com/o/oauth2/token"

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/api/client/auth/oauth2/c;-><init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/j;)V
    .locals 10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p12

    invoke-direct/range {v1 .. v9}, Lcom/google/api/client/auth/oauth2/c;-><init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Lcom/google/api/client/util/j;)V

    if-nez p10, :cond_1

    if-nez p8, :cond_0

    if-nez p9, :cond_0

    if-nez p11, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/ah;->a(Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-static/range {p8 .. p8}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/api/client/googleapis/auth/a/a;->b:Ljava/lang/String;

    invoke-static/range {p9 .. p9}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/api/client/googleapis/auth/a/a;->c:Ljava/lang/String;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/api/client/googleapis/auth/a/a;->d:Ljava/security/PrivateKey;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/api/client/googleapis/auth/a/a;->e:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public synthetic a(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->b(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->c(Ljava/lang/Long;)Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->d(Ljava/lang/Long;)Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->d(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/googleapis/auth/a/a;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/c;->a(Lcom/google/api/client/auth/oauth2/l;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/a;

    return-object v0
.end method

.method public c(Ljava/lang/Long;)Lcom/google/api/client/googleapis/auth/a/a;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/c;->a(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/a;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/c;->a(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/a;

    return-object v0
.end method

.method public d(Ljava/lang/Long;)Lcom/google/api/client/googleapis/auth/a/a;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/c;->b(Ljava/lang/Long;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/a;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->d()Lcom/google/api/client/json/d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->c()Lcom/google/api/client/http/v;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->f()Lcom/google/api/client/http/k;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Please use the Builder and call setJsonFactory, setTransport and setClientSecrets"

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/api/client/auth/oauth2/c;->b(Ljava/lang/String;)Lcom/google/api/client/auth/oauth2/c;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/auth/a/a;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected h()Lcom/google/api/client/auth/oauth2/l;
    .locals 10

    const-wide/16 v8, 0x3e8

    iget-object v0, p0, Lcom/google/api/client/googleapis/auth/a/a;->d:Ljava/security/PrivateKey;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/api/client/auth/oauth2/c;->h()Lcom/google/api/client/auth/oauth2/l;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/api/client/auth/a/b;

    invoke-direct {v0}, Lcom/google/api/client/auth/a/b;-><init>()V

    const-string v1, "RS256"

    invoke-virtual {v0, v1}, Lcom/google/api/client/auth/a/b;->b(Ljava/lang/String;)Lcom/google/api/client/auth/a/b;

    const-string v1, "JWT"

    invoke-virtual {v0, v1}, Lcom/google/api/client/auth/a/b;->a(Ljava/lang/String;)Lcom/google/api/client/auth/a/b;

    new-instance v1, Lcom/google/api/client/auth/a/e;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->b()Lcom/google/api/client/util/j;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/api/client/auth/a/e;-><init>(Lcom/google/api/client/util/j;)V

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->b()Lcom/google/api/client/util/j;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/api/client/util/j;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/api/client/googleapis/auth/a/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/google/api/client/auth/a/e;->a(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;

    move-result-object v4

    const-string v5, "https://accounts.google.com/o/oauth2/token"

    invoke-virtual {v4, v5}, Lcom/google/api/client/auth/a/e;->b(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;

    move-result-object v4

    div-long v6, v2, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/client/auth/a/e;->b(Ljava/lang/Long;)Lcom/google/api/client/auth/a/e;

    move-result-object v4

    div-long/2addr v2, v8

    const-wide/16 v6, 0xe10

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/api/client/auth/a/e;->a(Ljava/lang/Long;)Lcom/google/api/client/auth/a/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/api/client/googleapis/auth/a/a;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/api/client/auth/a/e;->c(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;

    const-string v2, "scope"

    iget-object v3, p0, Lcom/google/api/client/googleapis/auth/a/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/auth/a/e;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v2, p0, Lcom/google/api/client/googleapis/auth/a/a;->d:Ljava/security/PrivateKey;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->d()Lcom/google/api/client/json/d;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lcom/google/api/client/auth/a/f;->a(Ljava/security/PrivateKey;Lcom/google/api/client/json/d;Lcom/google/api/client/auth/a/b;Lcom/google/api/client/auth/a/e;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/api/client/auth/oauth2/i;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->c()Lcom/google/api/client/http/v;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/auth/a/a;->d()Lcom/google/api/client/json/d;

    move-result-object v3

    new-instance v4, Lcom/google/api/client/http/i;

    const-string v5, "https://accounts.google.com/o/oauth2/token"

    invoke-direct {v4, v5}, Lcom/google/api/client/http/i;-><init>(Ljava/lang/String;)V

    const-string v5, "assertion"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/api/client/auth/oauth2/i;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/i;Ljava/lang/String;)V

    const-string v2, "assertion_type"

    const-string v3, "http://oauth.net/grant_type/jwt/1.0/bearer"

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/auth/oauth2/i;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "assertion"

    invoke-virtual {v1, v2, v0}, Lcom/google/api/client/auth/oauth2/i;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/api/client/auth/oauth2/i;->b()Lcom/google/api/client/auth/oauth2/l;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method
