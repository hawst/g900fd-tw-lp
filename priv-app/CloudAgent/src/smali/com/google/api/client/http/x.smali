.class final Lcom/google/api/client/http/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/api/client/http/j;


# instance fields
.field private final a:Lcom/google/api/client/http/j;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:I


# direct methods
.method constructor <init>(Lcom/google/api/client/http/j;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/api/client/http/x;->a:Lcom/google/api/client/http/j;

    iput-object p2, p0, Lcom/google/api/client/http/x;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/api/client/http/x;->d:J

    iput-object p3, p0, Lcom/google/api/client/http/x;->c:Ljava/lang/String;

    iput p6, p0, Lcom/google/api/client/http/x;->e:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/x;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 4

    new-instance v1, Lcom/google/api/client/util/w;

    sget-object v0, Lcom/google/api/client/http/v;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    iget v3, p0, Lcom/google/api/client/http/x;->e:I

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/google/api/client/util/w;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/http/x;->a:Lcom/google/api/client/http/j;

    invoke-interface {v0, v1}, Lcom/google/api/client/http/j;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/api/client/util/w;->a()Lcom/google/api/client/util/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/u;->close()V

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/api/client/util/w;->a()Lcom/google/api/client/util/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/util/u;->close()V

    throw v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/google/api/client/http/x;->d:J

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/x;->a:Lcom/google/api/client/http/j;

    invoke-interface {v0}, Lcom/google/api/client/http/j;->f()Z

    move-result v0

    return v0
.end method
