.class final Lcom/google/api/client/util/s;
.super Ljava/util/AbstractSet;


# instance fields
.field final synthetic a:Lcom/google/api/client/util/GenericData;

.field private final b:Lcom/google/api/client/util/p;


# direct methods
.method constructor <init>(Lcom/google/api/client/util/GenericData;)V
    .locals 2

    iput-object p1, p0, Lcom/google/api/client/util/s;->a:Lcom/google/api/client/util/GenericData;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    new-instance v0, Lcom/google/api/client/util/m;

    iget-object v1, p1, Lcom/google/api/client/util/GenericData;->j:Lcom/google/api/client/util/h;

    invoke-virtual {v1}, Lcom/google/api/client/util/h;->a()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/google/api/client/util/m;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v0}, Lcom/google/api/client/util/m;->a()Lcom/google/api/client/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/util/s;->b:Lcom/google/api/client/util/p;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/util/s;->a:Lcom/google/api/client/util/GenericData;

    iget-object v0, v0, Lcom/google/api/client/util/GenericData;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/api/client/util/s;->b:Lcom/google/api/client/util/p;

    invoke-virtual {v0}, Lcom/google/api/client/util/p;->clear()V

    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Lcom/google/api/client/util/r;

    iget-object v1, p0, Lcom/google/api/client/util/s;->a:Lcom/google/api/client/util/GenericData;

    iget-object v2, p0, Lcom/google/api/client/util/s;->b:Lcom/google/api/client/util/p;

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/util/r;-><init>(Lcom/google/api/client/util/GenericData;Lcom/google/api/client/util/p;)V

    return-object v0
.end method

.method public size()I
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/util/s;->a:Lcom/google/api/client/util/GenericData;

    iget-object v0, v0, Lcom/google/api/client/util/GenericData;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/api/client/util/s;->b:Lcom/google/api/client/util/p;

    invoke-virtual {v1}, Lcom/google/api/client/util/p;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
