.class public Lcom/google/api/client/auth/oauth2/TokenResponseException;
.super Lcom/google/api/client/http/HttpResponseException;


# static fields
.field private static final serialVersionUID:J = 0x37cc5b6d7204050cL


# instance fields
.field private final transient details:Lcom/google/api/client/auth/oauth2/h;


# direct methods
.method private constructor <init>(Lcom/google/api/client/http/t;Lcom/google/api/client/auth/oauth2/h;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/t;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/api/client/auth/oauth2/TokenResponseException;->details:Lcom/google/api/client/auth/oauth2/h;

    return-void
.end method

.method public static from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/t;)Lcom/google/api/client/auth/oauth2/TokenResponseException;
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->a()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->c()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    sget-object v2, Lcom/google/api/client/json/c;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/api/client/http/n;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/api/client/json/f;

    invoke-direct {v0, p0}, Lcom/google/api/client/json/f;-><init>(Lcom/google/api/client/json/d;)V

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->f()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/client/http/t;->k()Ljava/nio/charset/Charset;

    move-result-object v3

    const-class v4, Lcom/google/api/client/auth/oauth2/h;

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/api/client/json/f;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v0}, Lcom/google/api/client/auth/oauth2/h;->d()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    invoke-static {p1}, Lcom/google/api/client/http/HttpResponseException;->computeMessageBuffer(Lcom/google/api/client/http/t;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/common/base/am;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/api/client/util/z;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Lcom/google/api/client/auth/oauth2/TokenResponseException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/api/client/auth/oauth2/TokenResponseException;-><init>(Lcom/google/api/client/http/t;Lcom/google/api/client/auth/oauth2/h;Ljava/lang/String;)V

    return-object v0

    :cond_1
    :try_start_2
    invoke-virtual {p1}, Lcom/google/api/client/http/t;->j()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    :goto_1
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method public final getDetails()Lcom/google/api/client/auth/oauth2/h;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/TokenResponseException;->details:Lcom/google/api/client/auth/oauth2/h;

    return-object v0
.end method
