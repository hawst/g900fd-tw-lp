.class Lcom/google/api/client/json/gson/GsonGenerator;
.super Lcom/google/api/client/json/e;


# instance fields
.field private final a:Lcom/google/gson/stream/a;


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->f()V

    return-void
.end method

.method public a(D)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/stream/a;->a(D)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(F)V
    .locals 4

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/stream/a;->a(D)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/stream/a;->a(J)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Lcom/google/common/primitives/UnsignedInteger;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->a(Ljava/lang/Number;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Lcom/google/common/primitives/UnsignedLong;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->a(Ljava/lang/Number;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->b(Ljava/lang/String;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->a(Ljava/lang/Number;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Ljava/math/BigInteger;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->a(Ljava/lang/Number;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->b(Z)Lcom/google/gson/stream/a;

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->b()Lcom/google/gson/stream/a;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0, p1}, Lcom/google/gson/stream/a;->c(Ljava/lang/String;)Lcom/google/gson/stream/a;

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->d()Lcom/google/gson/stream/a;

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->e()Lcom/google/gson/stream/a;

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->a()Lcom/google/gson/stream/a;

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    invoke-virtual {v0}, Lcom/google/gson/stream/a;->c()Lcom/google/gson/stream/a;

    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/google/api/client/json/gson/GsonGenerator;->a:Lcom/google/gson/stream/a;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/gson/stream/a;->a(Ljava/lang/String;)V

    return-void
.end method
