.class public Lcom/google/api/client/http/g;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:D

.field private c:D

.field private d:I

.field private e:I


# direct methods
.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/api/client/http/g;->a:I

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lcom/google/api/client/http/g;->b:D

    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    iput-wide v0, p0, Lcom/google/api/client/http/g;->c:D

    const v0, 0xea60

    iput v0, p0, Lcom/google/api/client/http/g;->d:I

    const v0, 0xdbba0

    iput v0, p0, Lcom/google/api/client/http/g;->e:I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/client/http/f;
    .locals 8

    new-instance v0, Lcom/google/api/client/http/f;

    iget v1, p0, Lcom/google/api/client/http/g;->a:I

    iget-wide v2, p0, Lcom/google/api/client/http/g;->b:D

    iget-wide v4, p0, Lcom/google/api/client/http/g;->c:D

    iget v6, p0, Lcom/google/api/client/http/g;->d:I

    iget v7, p0, Lcom/google/api/client/http/g;->e:I

    invoke-direct/range {v0 .. v7}, Lcom/google/api/client/http/f;-><init>(IDDII)V

    return-object v0
.end method

.method public a(D)Lcom/google/api/client/http/g;
    .locals 1

    iput-wide p1, p0, Lcom/google/api/client/http/g;->b:D

    return-object p0
.end method

.method public a(I)Lcom/google/api/client/http/g;
    .locals 0

    iput p1, p0, Lcom/google/api/client/http/g;->a:I

    return-object p0
.end method

.method public b(D)Lcom/google/api/client/http/g;
    .locals 1

    iput-wide p1, p0, Lcom/google/api/client/http/g;->c:D

    return-object p0
.end method

.method public b(I)Lcom/google/api/client/http/g;
    .locals 0

    iput p1, p0, Lcom/google/api/client/http/g;->d:I

    return-object p0
.end method

.method public c(I)Lcom/google/api/client/http/g;
    .locals 0

    iput p1, p0, Lcom/google/api/client/http/g;->e:I

    return-object p0
.end method
