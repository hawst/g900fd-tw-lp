.class public Lcom/google/api/client/auth/a/e;
.super Lcom/google/api/client/json/b;


# instance fields
.field private final a:Lcom/google/api/client/util/j;

.field private b:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "exp"
    .end annotation
.end field

.field private c:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "iat"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "iss"
    .end annotation
.end field

.field private e:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "aud"
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "prn"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/api/client/util/j;->a:Lcom/google/api/client/util/j;

    invoke-direct {p0, v0}, Lcom/google/api/client/auth/a/e;-><init>(Lcom/google/api/client/util/j;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/client/util/j;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/json/b;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/util/j;

    iput-object v0, p0, Lcom/google/api/client/auth/a/e;->a:Lcom/google/api/client/util/j;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lcom/google/api/client/auth/a/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/a/e;->b:Ljava/lang/Long;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/a/e;->d:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/lang/Long;)Lcom/google/api/client/auth/a/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/a/e;->c:Ljava/lang/Long;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/a/e;->e:Ljava/lang/String;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/client/auth/a/e;
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/auth/a/e;->f:Ljava/lang/String;

    return-object p0
.end method
