.class public Lcom/google/api/client/http/b/d;
.super Lcom/google/api/client/util/GenericData;


# instance fields
.field private final a:Lcom/google/api/client/http/b/a;

.field private final b:Lcom/google/api/client/http/HttpMethod;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/Object;

.field private e:Lcom/google/api/client/http/l;

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/api/client/http/b/a;Lcom/google/api/client/http/HttpMethod;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/api/client/util/GenericData;-><init>()V

    new-instance v0, Lcom/google/api/client/http/l;

    invoke-direct {v0}, Lcom/google/api/client/http/l;-><init>()V

    iput-object v0, p0, Lcom/google/api/client/http/b/d;->e:Lcom/google/api/client/http/l;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/api/client/http/b/d;->f:Z

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/b/a;

    iput-object v0, p0, Lcom/google/api/client/http/b/d;->a:Lcom/google/api/client/http/b/a;

    invoke-static {p2}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/HttpMethod;

    iput-object v0, p0, Lcom/google/api/client/http/b/d;->b:Lcom/google/api/client/http/HttpMethod;

    invoke-static {p3}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/b/d;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/api/client/http/b/d;->d:Ljava/lang/Object;

    return-void
.end method
