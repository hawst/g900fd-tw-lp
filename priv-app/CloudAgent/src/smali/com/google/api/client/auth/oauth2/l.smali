.class public Lcom/google/api/client/auth/oauth2/l;
.super Lcom/google/api/client/json/b;


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "access_token"
    .end annotation
.end field

.field private b:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "expires_in"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/t;
        a = "refresh_token"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/json/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/l;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/l;->c:Ljava/lang/String;

    return-object v0
.end method
