.class public final Lcom/google/api/client/http/d;
.super Lcom/google/api/client/http/b;


# instance fields
.field private final a:[B

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p2

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/api/client/http/d;-><init>(Ljava/lang/String;[BII)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BII)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/api/client/http/b;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/api/client/http/d;->a:[B

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    add-int v0, p3, p4

    array-length v1, p2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/ah;->a(Z)V

    iput p3, p0, Lcom/google/api/client/http/d;->b:I

    iput p4, p0, Lcom/google/api/client/http/d;->c:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/d;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/d;

    invoke-static {p1}, Lcom/google/api/client/util/z;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/api/client/http/d;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method


# virtual methods
.method public synthetic a(Ljava/lang/String;)Lcom/google/api/client/http/b;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/d;->b(Ljava/lang/String;)Lcom/google/api/client/http/d;

    move-result-object v0

    return-object v0
.end method

.method public b()J
    .locals 2

    iget v0, p0, Lcom/google/api/client/http/d;->c:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/client/http/d;
    .locals 1

    invoke-super {p0, p1}, Lcom/google/api/client/http/b;->a(Ljava/lang/String;)Lcom/google/api/client/http/b;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/http/d;

    return-object v0
.end method

.method public c()Ljava/io/InputStream;
    .locals 4

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/api/client/http/d;->a:[B

    iget v2, p0, Lcom/google/api/client/http/d;->b:I

    iget v3, p0, Lcom/google/api/client/http/d;->c:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
