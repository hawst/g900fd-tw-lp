.class public final Lcom/google/api/client/http/a/c;
.super Lcom/google/api/client/http/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/api/client/http/v;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/a/c;->g(Ljava/lang/String;)Lcom/google/api/client/http/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/a/c;->h(Ljava/lang/String;)Lcom/google/api/client/http/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/a/c;->i(Ljava/lang/String;)Lcom/google/api/client/http/a/a;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic e(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/a/c;->j(Ljava/lang/String;)Lcom/google/api/client/http/a/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f(Ljava/lang/String;)Lcom/google/api/client/http/y;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/api/client/http/a/c;->k(Ljava/lang/String;)Lcom/google/api/client/http/a/a;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/google/api/client/http/a/a;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/a/a;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/google/api/client/http/a/a;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/a/a;

    const-string v1, "GET"

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public i(Ljava/lang/String;)Lcom/google/api/client/http/a/a;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/a/a;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public j(Ljava/lang/String;)Lcom/google/api/client/http/a/a;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/a/a;

    const-string v1, "POST"

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public k(Ljava/lang/String;)Lcom/google/api/client/http/a/a;
    .locals 2

    new-instance v0, Lcom/google/api/client/http/a/a;

    const-string v1, "PUT"

    invoke-direct {v0, v1, p1}, Lcom/google/api/client/http/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
