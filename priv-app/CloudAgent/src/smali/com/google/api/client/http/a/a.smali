.class final Lcom/google/api/client/http/a/a;
.super Lcom/google/api/client/http/y;


# instance fields
.field private final a:Ljava/net/HttpURLConnection;

.field private b:Lcom/google/api/client/http/j;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/api/client/http/y;-><init>()V

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/google/api/client/http/a/a;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/client/http/z;
    .locals 8

    const-wide/16 v6, 0x0

    iget-object v1, p0, Lcom/google/api/client/http/a/a;->a:Ljava/net/HttpURLConnection;

    iget-object v0, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    invoke-interface {v0}, Lcom/google/api/client/http/j;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "Content-Type"

    invoke-virtual {p0, v2, v0}, Lcom/google/api/client/http/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    invoke-interface {v0}, Lcom/google/api/client/http/j;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "Content-Encoding"

    invoke-virtual {p0, v2, v0}, Lcom/google/api/client/http/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    invoke-interface {v0}, Lcom/google/api/client/http/j;->b()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-ltz v0, :cond_2

    const-string v0, "Content-Length"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/google/api/client/http/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    cmp-long v0, v2, v6

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    cmp-long v0, v2, v6

    if-ltz v0, :cond_4

    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    long-to-int v0, v2

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    :goto_0
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    :try_start_0
    iget-object v0, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    invoke-interface {v0, v2}, Lcom/google/api/client/http/j;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    new-instance v0, Lcom/google/api/client/http/a/b;

    invoke-direct {v0, v1}, Lcom/google/api/client/http/a/b;-><init>(Ljava/net/HttpURLConnection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-object v0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v0

    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/a/a;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    iget-object v0, p0, Lcom/google/api/client/http/a/a;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    return-void
.end method

.method public a(Lcom/google/api/client/http/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/api/client/http/a/a;->b:Lcom/google/api/client/http/j;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/api/client/http/a/a;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
