.class Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final bits:I

.field final hashFunction:Lcom/google/common/hash/n;


# direct methods
.method constructor <init>(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->bits:I
    invoke-static {p1}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->access$200(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)I

    move-result v0

    iput v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;->bits:I

    # getter for: Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->hashFunction:Lcom/google/common/hash/n;
    invoke-static {p1}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->access$300(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)Lcom/google/common/hash/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;->hashFunction:Lcom/google/common/hash/n;

    return-void
.end method


# virtual methods
.method readResolve()Ljava/lang/Object;
    .locals 2

    iget v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;->bits:I

    iget-object v1, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;->hashFunction:Lcom/google/common/hash/n;

    invoke-static {v0, v1}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->withBits(ILcom/google/common/hash/n;)Lcom/google/common/hash/BloomFilterStrategies$From128ToN;

    move-result-object v0

    return-object v0
.end method
