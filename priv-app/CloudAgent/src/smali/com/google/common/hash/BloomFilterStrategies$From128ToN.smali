.class Lcom/google/common/hash/BloomFilterStrategies$From128ToN;
.super Lcom/google/common/hash/a;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final bits:I

.field private final hashFunction:Lcom/google/common/hash/n;


# direct methods
.method private constructor <init>(ILcom/google/common/hash/n;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/common/hash/n;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/common/hash/a;-><init>([Lcom/google/common/hash/n;)V

    iput-object p2, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->hashFunction:Lcom/google/common/hash/n;

    iput p1, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->bits:I

    return-void
.end method

.method static synthetic access$200(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)I
    .locals 1

    iget v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->bits:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)Lcom/google/common/hash/n;
    .locals 1

    iget-object v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->hashFunction:Lcom/google/common/hash/n;

    return-object v0
.end method

.method static withBits(ILcom/google/common/hash/n;)Lcom/google/common/hash/BloomFilterStrategies$From128ToN;
    .locals 2

    new-instance v0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;

    invoke-static {p0}, Lcom/google/common/hash/BloomFilterStrategies;->a(I)I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;-><init>(ILcom/google/common/hash/n;)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;

    invoke-direct {v0, p0}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN$SerialForm;-><init>(Lcom/google/common/hash/BloomFilterStrategies$From128ToN;)V

    return-object v0
.end method


# virtual methods
.method public bits()I
    .locals 1

    iget v0, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->bits:I

    return v0
.end method

.method makeHash([Lcom/google/common/hash/o;)Lcom/google/common/hash/i;
    .locals 5

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Lcom/google/common/hash/o;->a()Lcom/google/common/hash/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/hash/i;->b()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iget v4, p0, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->bits:I

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/common/hash/BloomFilterStrategies;->a(JJI)Lcom/google/common/hash/i;

    move-result-object v0

    return-object v0
.end method
