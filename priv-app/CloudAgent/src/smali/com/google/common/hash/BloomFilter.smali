.class public final Lcom/google/common/hash/BloomFilter;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final LN2:D

.field private static final LN2_SQUARED:D


# instance fields
.field private final bits:Lcom/google/common/hash/g;

.field private final funnel:Lcom/google/common/hash/h;

.field private final hashBitsPerSlice:I

.field private final hashFunction:Lcom/google/common/hash/n;

.field private final numHashFunctions:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/common/hash/BloomFilter;->LN2:D

    sget-wide v0, Lcom/google/common/hash/BloomFilter;->LN2:D

    sget-wide v2, Lcom/google/common/hash/BloomFilter;->LN2:D

    mul-double/2addr v0, v2

    sput-wide v0, Lcom/google/common/hash/BloomFilter;->LN2_SQUARED:D

    return-void
.end method

.method private constructor <init>(Lcom/google/common/hash/g;ILcom/google/common/hash/h;Lcom/google/common/hash/n;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "numHashFunctions zero or negative"

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/hash/g;

    iput-object v0, p0, Lcom/google/common/hash/BloomFilter;->bits:Lcom/google/common/hash/g;

    iput p2, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    invoke-static {p3}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/hash/h;

    iput-object v0, p0, Lcom/google/common/hash/BloomFilter;->funnel:Lcom/google/common/hash/h;

    invoke-static {p4}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/hash/n;

    iput-object v0, p0, Lcom/google/common/hash/BloomFilter;->hashFunction:Lcom/google/common/hash/n;

    invoke-virtual {p1}, Lcom/google/common/hash/g;->a()I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget-object v1, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v0, v1}, Lcom/google/common/b/a;->a(ILjava/math/RoundingMode;)I

    move-result v0

    iput v0, p0, Lcom/google/common/hash/BloomFilter;->hashBitsPerSlice:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/common/hash/g;ILcom/google/common/hash/h;Lcom/google/common/hash/n;Lcom/google/common/hash/f;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/hash/BloomFilter;-><init>(Lcom/google/common/hash/g;ILcom/google/common/hash/h;Lcom/google/common/hash/n;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/common/hash/BloomFilter;)Lcom/google/common/hash/g;
    .locals 1

    iget-object v0, p0, Lcom/google/common/hash/BloomFilter;->bits:Lcom/google/common/hash/g;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/common/hash/BloomFilter;)I
    .locals 1

    iget v0, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/common/hash/BloomFilter;)Lcom/google/common/hash/h;
    .locals 1

    iget-object v0, p0, Lcom/google/common/hash/BloomFilter;->funnel:Lcom/google/common/hash/h;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/common/hash/BloomFilter;)Lcom/google/common/hash/n;
    .locals 1

    iget-object v0, p0, Lcom/google/common/hash/BloomFilter;->hashFunction:Lcom/google/common/hash/n;

    return-object v0
.end method

.method public static create(Lcom/google/common/hash/h;I)Lcom/google/common/hash/BloomFilter;
    .locals 2

    const-wide v0, 0x3f9eb851eb851eb8L    # 0.03

    invoke-static {p0, p1, v0, v1}, Lcom/google/common/hash/BloomFilter;->create(Lcom/google/common/hash/h;ID)Lcom/google/common/hash/BloomFilter;

    move-result-object v0

    return-object v0
.end method

.method public static create(Lcom/google/common/hash/h;ID)Lcom/google/common/hash/BloomFilter;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p1, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Expected insertions must be positive"

    invoke-static {v0, v3}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    const-wide/16 v4, 0x0

    cmpl-double v0, p2, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v3, p2, v4

    if-gez v3, :cond_0

    move v2, v1

    :cond_0
    and-int/2addr v0, v2

    const-string v2, "False positive probability in (0.0, 1.0)"

    invoke-static {v0, v2}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    invoke-static {p1, p2, p3}, Lcom/google/common/hash/BloomFilter;->optimalM(ID)I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/common/hash/BloomFilter;->optimalK(II)I

    move-result v2

    new-instance v3, Lcom/google/common/hash/g;

    const/16 v4, 0x40

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget-object v4, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v0, v4}, Lcom/google/common/b/a;->a(ILjava/math/RoundingMode;)I

    move-result v0

    shl-int v0, v1, v0

    invoke-direct {v3, v0}, Lcom/google/common/hash/g;-><init>(I)V

    invoke-virtual {v3}, Lcom/google/common/hash/g;->a()I

    move-result v0

    mul-int/2addr v0, v2

    invoke-static {}, Lcom/google/common/hash/p;->a()Lcom/google/common/hash/n;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/hash/BloomFilterStrategies$From128ToN;->withBits(ILcom/google/common/hash/n;)Lcom/google/common/hash/BloomFilterStrategies$From128ToN;

    move-result-object v0

    new-instance v1, Lcom/google/common/hash/BloomFilter;

    invoke-direct {v1, v3, v2, p0, v0}, Lcom/google/common/hash/BloomFilter;-><init>(Lcom/google/common/hash/g;ILcom/google/common/hash/h;Lcom/google/common/hash/n;)V

    return-object v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method static optimalK(II)I
    .locals 6

    const/4 v0, 0x1

    div-int v1, p1, p0

    int-to-double v2, v1

    sget-wide v4, Lcom/google/common/hash/BloomFilter;->LN2:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static optimalM(ID)I
    .locals 5

    neg-int v0, p0

    int-to-double v0, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sget-wide v2, Lcom/google/common/hash/BloomFilter;->LN2_SQUARED:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/common/hash/BloomFilter$SerialForm;

    invoke-direct {v0, p0}, Lcom/google/common/hash/BloomFilter$SerialForm;-><init>(Lcom/google/common/hash/BloomFilter;)V

    return-object v0
.end method


# virtual methods
.method computeExpectedFalsePositiveRate(I)D
    .locals 8

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget v2, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    neg-int v2, v2

    int-to-double v2, v2

    int-to-double v4, p1

    iget-object v6, p0, Lcom/google/common/hash/BloomFilter;->bits:Lcom/google/common/hash/g;

    invoke-virtual {v6}, Lcom/google/common/hash/g;->a()I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method getHashCount()I
    .locals 1

    iget v0, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    return v0
.end method

.method public mightContain(Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/common/hash/BloomFilter;->hashFunction:Lcom/google/common/hash/n;

    invoke-interface {v0}, Lcom/google/common/hash/n;->newHasher()Lcom/google/common/hash/o;

    move-result-object v0

    iget-object v2, p0, Lcom/google/common/hash/BloomFilter;->funnel:Lcom/google/common/hash/h;

    invoke-interface {v0, p1, v2}, Lcom/google/common/hash/o;->a(Ljava/lang/Object;Lcom/google/common/hash/h;)Lcom/google/common/hash/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/o;->a()Lcom/google/common/hash/i;

    move-result-object v0

    iget v2, p0, Lcom/google/common/hash/BloomFilter;->hashBitsPerSlice:I

    invoke-static {v0, v2}, Lcom/google/common/hash/j;->a(Lcom/google/common/hash/i;I)Lcom/google/common/hash/l;

    move-result-object v2

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/common/hash/BloomFilter;->bits:Lcom/google/common/hash/g;

    invoke-virtual {v2}, Lcom/google/common/hash/l;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/common/hash/g;->b(I)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public put(Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lcom/google/common/hash/BloomFilter;->hashFunction:Lcom/google/common/hash/n;

    invoke-interface {v0}, Lcom/google/common/hash/n;->newHasher()Lcom/google/common/hash/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/hash/BloomFilter;->funnel:Lcom/google/common/hash/h;

    invoke-interface {v0, p1, v1}, Lcom/google/common/hash/o;->a(Ljava/lang/Object;Lcom/google/common/hash/h;)Lcom/google/common/hash/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/o;->a()Lcom/google/common/hash/i;

    move-result-object v0

    iget v1, p0, Lcom/google/common/hash/BloomFilter;->hashBitsPerSlice:I

    invoke-static {v0, v1}, Lcom/google/common/hash/j;->a(Lcom/google/common/hash/i;I)Lcom/google/common/hash/l;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/common/hash/BloomFilter;->numHashFunctions:I

    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Lcom/google/common/hash/l;->a()I

    move-result v2

    iget-object v3, p0, Lcom/google/common/hash/BloomFilter;->bits:Lcom/google/common/hash/g;

    invoke-virtual {v3, v2}, Lcom/google/common/hash/g;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
