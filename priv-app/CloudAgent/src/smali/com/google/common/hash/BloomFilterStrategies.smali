.class final Lcom/google/common/hash/BloomFilterStrategies;
.super Ljava/lang/Object;


# direct methods
.method static synthetic a(I)I
    .locals 1

    invoke-static {p0}, Lcom/google/common/hash/BloomFilterStrategies;->b(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(JJI)Lcom/google/common/hash/i;
    .locals 2

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/common/hash/BloomFilterStrategies;->b(JJI)Lcom/google/common/hash/i;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)I
    .locals 2

    if-lez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Number of bits must be positive"

    invoke-static {v0, v1}, Lcom/google/common/base/ah;->a(ZLjava/lang/Object;)V

    add-int/lit8 v0, p0, 0x3f

    and-int/lit8 v0, v0, -0x40

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(JJI)Lcom/google/common/hash/i;
    .locals 10

    const-wide/16 v2, 0x1

    div-int/lit8 v0, p4, 0x8

    new-array v4, v0, [B

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    div-int/lit8 v0, p4, 0x40

    int-to-long v6, v0

    move-wide v0, v2

    :goto_0
    cmp-long v8, v0, v6

    if-gtz v8, :cond_0

    mul-long v8, v0, p2

    add-long/2addr v8, p0

    invoke-virtual {v5, v8, v9}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    invoke-static {v4}, Lcom/google/common/hash/j;->a([B)Lcom/google/common/hash/i;

    move-result-object v0

    return-object v0
.end method
