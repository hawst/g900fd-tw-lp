.class Lcom/google/common/hash/l;
.super Ljava/lang/Object;


# instance fields
.field final a:[B

.field final b:I

.field c:I

.field d:I


# direct methods
.method constructor <init>([BI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/common/hash/l;->a:[B

    iput p2, p0, Lcom/google/common/hash/l;->b:I

    return-void
.end method


# virtual methods
.method a()I
    .locals 5

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    :goto_0
    iget v3, p0, Lcom/google/common/hash/l;->b:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/common/hash/l;->a:[B

    iget v4, p0, Lcom/google/common/hash/l;->c:I

    aget-byte v3, v3, v4

    iget v4, p0, Lcom/google/common/hash/l;->d:I

    ushr-int/2addr v3, v4

    and-int/lit8 v3, v3, 0x1

    shl-int/lit8 v2, v2, 0x1

    or-int/2addr v2, v3

    iget v3, p0, Lcom/google/common/hash/l;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/common/hash/l;->d:I

    iget v3, p0, Lcom/google/common/hash/l;->d:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    iput v1, p0, Lcom/google/common/hash/l;->d:I

    iget v3, p0, Lcom/google/common/hash/l;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/common/hash/l;->c:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method
