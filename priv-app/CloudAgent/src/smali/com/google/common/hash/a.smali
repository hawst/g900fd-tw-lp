.class abstract Lcom/google/common/hash/a;
.super Lcom/google/common/hash/d;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final functions:[Lcom/google/common/hash/n;


# direct methods
.method varargs constructor <init>([Lcom/google/common/hash/n;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/hash/d;-><init>()V

    iput-object p1, p0, Lcom/google/common/hash/a;->functions:[Lcom/google/common/hash/n;

    return-void
.end method


# virtual methods
.method abstract makeHash([Lcom/google/common/hash/o;)Lcom/google/common/hash/i;
.end method

.method public newHasher()Lcom/google/common/hash/o;
    .locals 3

    iget-object v0, p0, Lcom/google/common/hash/a;->functions:[Lcom/google/common/hash/n;

    array-length v0, v0

    new-array v1, v0, [Lcom/google/common/hash/o;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/common/hash/a;->functions:[Lcom/google/common/hash/n;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/common/hash/n;->newHasher()Lcom/google/common/hash/o;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/common/hash/b;

    invoke-direct {v0, p0, v1}, Lcom/google/common/hash/b;-><init>(Lcom/google/common/hash/a;[Lcom/google/common/hash/o;)V

    return-object v0
.end method
