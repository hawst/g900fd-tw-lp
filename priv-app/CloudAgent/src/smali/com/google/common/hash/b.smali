.class Lcom/google/common/hash/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/common/hash/o;


# instance fields
.field final synthetic a:[Lcom/google/common/hash/o;

.field final synthetic b:Lcom/google/common/hash/a;


# direct methods
.method constructor <init>(Lcom/google/common/hash/a;[Lcom/google/common/hash/o;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/hash/b;->b:Lcom/google/common/hash/a;

    iput-object p2, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/hash/i;
    .locals 2

    iget-object v0, p0, Lcom/google/common/hash/b;->b:Lcom/google/common/hash/a;

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    invoke-virtual {v0, v1}, Lcom/google/common/hash/a;->makeHash([Lcom/google/common/hash/o;)Lcom/google/common/hash/i;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Lcom/google/common/hash/o;
    .locals 5

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2}, Lcom/google/common/hash/o;->a(J)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/google/common/hash/o;
    .locals 4

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/google/common/hash/o;->a(Ljava/lang/CharSequence;)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/google/common/hash/o;
    .locals 4

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2}, Lcom/google/common/hash/o;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Object;Lcom/google/common/hash/h;)Lcom/google/common/hash/o;
    .locals 4

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2}, Lcom/google/common/hash/o;->a(Ljava/lang/Object;Lcom/google/common/hash/h;)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a([B)Lcom/google/common/hash/o;
    .locals 4

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/google/common/hash/o;->a([B)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a([BII)Lcom/google/common/hash/o;
    .locals 4

    iget-object v1, p0, Lcom/google/common/hash/b;->a:[Lcom/google/common/hash/o;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2, p3}, Lcom/google/common/hash/o;->a([BII)Lcom/google/common/hash/o;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public synthetic b(Ljava/lang/CharSequence;)Lcom/google/common/hash/v;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/hash/b;->a(Ljava/lang/CharSequence;)Lcom/google/common/hash/o;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([B)Lcom/google/common/hash/v;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/hash/b;->a([B)Lcom/google/common/hash/o;

    move-result-object v0

    return-object v0
.end method
