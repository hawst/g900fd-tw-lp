.class public final Lcom/google/common/util/concurrent/g;
.super Ljava/util/concurrent/FutureTask;

# interfaces
.implements Lcom/google/common/util/concurrent/f;


# instance fields
.field private final a:Lcom/google/common/util/concurrent/b;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    new-instance v0, Lcom/google/common/util/concurrent/b;

    invoke-direct {v0}, Lcom/google/common/util/concurrent/b;-><init>()V

    iput-object v0, p0, Lcom/google/common/util/concurrent/g;->a:Lcom/google/common/util/concurrent/b;

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    new-instance v0, Lcom/google/common/util/concurrent/b;

    invoke-direct {v0}, Lcom/google/common/util/concurrent/b;-><init>()V

    iput-object v0, p0, Lcom/google/common/util/concurrent/g;->a:Lcom/google/common/util/concurrent/b;

    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/g;
    .locals 1

    new-instance v0, Lcom/google/common/util/concurrent/g;

    invoke-direct {v0, p0, p1}, Lcom/google/common/util/concurrent/g;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/g;
    .locals 1

    new-instance v0, Lcom/google/common/util/concurrent/g;

    invoke-direct {v0, p0}, Lcom/google/common/util/concurrent/g;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lcom/google/common/util/concurrent/g;->a:Lcom/google/common/util/concurrent/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/util/concurrent/b;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected done()V
    .locals 1

    iget-object v0, p0, Lcom/google/common/util/concurrent/g;->a:Lcom/google/common/util/concurrent/b;

    invoke-virtual {v0}, Lcom/google/common/util/concurrent/b;->a()V

    return-void
.end method
