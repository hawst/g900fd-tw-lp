.class Lcom/google/common/collect/lp;
.super Lcom/google/common/collect/hf;


# instance fields
.field final synthetic a:Lcom/google/common/collect/ln;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/ln;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/lp;->a:Lcom/google/common/collect/ln;

    invoke-direct {p0}, Lcom/google/common/collect/hf;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/ln;Lcom/google/common/collect/ku;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/lp;-><init>(Lcom/google/common/collect/ln;)V

    return-void
.end method


# virtual methods
.method a()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/lp;->a:Lcom/google/common/collect/ln;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/lp;->a:Lcom/google/common/collect/ln;

    invoke-virtual {v0}, Lcom/google/common/collect/ln;->a()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/ef;->b()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v0, Lcom/google/common/collect/lq;

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/lq;-><init>(Lcom/google/common/collect/lp;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/lp;->a:Lcom/google/common/collect/ln;

    invoke-virtual {v0}, Lcom/google/common/collect/ln;->a()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method
