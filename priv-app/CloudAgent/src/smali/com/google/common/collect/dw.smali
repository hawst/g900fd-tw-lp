.class public final Lcom/google/common/collect/dw;
.super Lcom/google/common/collect/do;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/common/collect/do;-><init>()V

    new-instance v0, Lcom/google/common/collect/ImmutableSetMultimap$BuilderMultimap;

    invoke-direct {v0}, Lcom/google/common/collect/ImmutableSetMultimap$BuilderMultimap;-><init>()V

    iput-object v0, p0, Lcom/google/common/collect/dw;->a:Lcom/google/common/collect/hw;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/ImmutableSetMultimap;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/dw;->a:Lcom/google/common/collect/hw;

    iget-object v1, p0, Lcom/google/common/collect/dw;->b:Ljava/util/Comparator;

    # invokes: Lcom/google/common/collect/ImmutableSetMultimap;->copyOf(Lcom/google/common/collect/hw;Ljava/util/Comparator;)Lcom/google/common/collect/ImmutableSetMultimap;
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSetMultimap;->access$000(Lcom/google/common/collect/hw;Ljava/util/Comparator;)Lcom/google/common/collect/ImmutableSetMultimap;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/dw;
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/dw;->a:Lcom/google/common/collect/hw;

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/collect/hw;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-object p0
.end method

.method public synthetic b()Lcom/google/common/collect/ImmutableMultimap;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/dw;->a()Lcom/google/common/collect/ImmutableSetMultimap;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/do;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dw;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/dw;

    move-result-object v0

    return-object v0
.end method
