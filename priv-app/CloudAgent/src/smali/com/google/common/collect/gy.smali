.class final Lcom/google/common/collect/gy;
.super Lcom/google/common/collect/gw;

# interfaces
.implements Lcom/google/common/collect/gi;


# instance fields
.field volatile d:J

.field e:Lcom/google/common/collect/gi;

.field f:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/collect/gw;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/common/collect/gy;->d:J

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gy;->e:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gy;->f:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public getExpirationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/gy;->d:J

    return-wide v0
.end method

.method public getNextExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gy;->e:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gy;->f:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public setExpirationTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/common/collect/gy;->d:J

    return-void
.end method

.method public setNextExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gy;->e:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gy;->f:Lcom/google/common/collect/gi;

    return-void
.end method
