.class final Lcom/google/common/collect/na;
.super Lcom/google/common/collect/cy;


# instance fields
.field private final a:Ljava/util/Map;

.field private b:Ljava/util/Set;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/common/collect/cy;-><init>()V

    iput-object p1, p0, Lcom/google/common/collect/na;->a:Ljava/util/Map;

    return-void
.end method

.method static a(Ljava/util/Map;)Lcom/google/common/collect/na;
    .locals 1

    new-instance v0, Lcom/google/common/collect/na;

    invoke-direct {v0, p0}, Lcom/google/common/collect/na;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic delegate()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/na;->delegate()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected delegate()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/na;->a:Ljava/util/Map;

    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/na;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/na;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/nb;

    invoke-direct {v1, p0}, Lcom/google/common/collect/nb;-><init>(Ljava/util/Map;)V

    invoke-static {v0, v1}, Lcom/google/common/collect/Sets;->a(Ljava/util/Set;Lcom/google/common/collect/kb;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/na;->b:Ljava/util/Set;

    goto :goto_0
.end method
