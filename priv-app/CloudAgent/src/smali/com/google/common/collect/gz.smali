.class final Lcom/google/common/collect/gz;
.super Lcom/google/common/collect/gw;

# interfaces
.implements Lcom/google/common/collect/gi;


# instance fields
.field volatile d:J

.field e:Lcom/google/common/collect/gi;

.field f:Lcom/google/common/collect/gi;

.field g:Lcom/google/common/collect/gi;

.field h:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/collect/gw;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/common/collect/gz;->d:J

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gz;->e:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gz;->f:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gz;->g:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gz;->h:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public getExpirationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/gz;->d:J

    return-wide v0
.end method

.method public getNextEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gz;->g:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getNextExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gz;->e:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gz;->h:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gz;->f:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public setExpirationTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/common/collect/gz;->d:J

    return-void
.end method

.method public setNextEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gz;->g:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setNextExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gz;->e:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gz;->h:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gz;->f:Lcom/google/common/collect/gi;

    return-void
.end method
