.class final Lcom/google/common/collect/fz;
.super Ljava/util/AbstractQueue;


# instance fields
.field final a:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    new-instance v0, Lcom/google/common/collect/ga;

    invoke-direct {v0, p0}, Lcom/google/common/collect/ga;-><init>(Lcom/google/common/collect/fz;)V

    iput-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/gi;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public a(Lcom/google/common/collect/gi;)Z
    .locals 2

    invoke-interface {p1}, Lcom/google/common/collect/gi;->getPreviousEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/MapMakerInternalMap;->connectEvictables(Lcom/google/common/collect/gi;Lcom/google/common/collect/gi;)V

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getPreviousEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/MapMakerInternalMap;->connectEvictables(Lcom/google/common/collect/gi;Lcom/google/common/collect/gi;)V

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-static {p1, v0}, Lcom/google/common/collect/MapMakerInternalMap;->connectEvictables(Lcom/google/common/collect/gi;Lcom/google/common/collect/gi;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()Lcom/google/common/collect/gi;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/common/collect/fz;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    if-eq v0, v1, :cond_0

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v1

    invoke-static {v0}, Lcom/google/common/collect/MapMakerInternalMap;->nullifyEvictable(Lcom/google/common/collect/gi;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0, v1}, Lcom/google/common/collect/gi;->setNextEvictable(Lcom/google/common/collect/gi;)V

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0, v1}, Lcom/google/common/collect/gi;->setPreviousEvictable(Lcom/google/common/collect/gi;)V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/google/common/collect/gi;

    invoke-interface {p1}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/MapMakerInternalMap$NullEntry;->INSTANCE:Lcom/google/common/collect/MapMakerInternalMap$NullEntry;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lcom/google/common/collect/gb;

    invoke-virtual {p0}, Lcom/google/common/collect/fz;->a()Lcom/google/common/collect/gi;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/gb;-><init>(Lcom/google/common/collect/fz;Lcom/google/common/collect/gi;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/common/collect/gi;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/fz;->a(Lcom/google/common/collect/gi;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/fz;->a()Lcom/google/common/collect/gi;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/fz;->b()Lcom/google/common/collect/gi;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    check-cast p1, Lcom/google/common/collect/gi;

    invoke-interface {p1}, Lcom/google/common/collect/gi;->getPreviousEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/MapMakerInternalMap;->connectEvictables(Lcom/google/common/collect/gi;Lcom/google/common/collect/gi;)V

    invoke-static {p1}, Lcom/google/common/collect/MapMakerInternalMap;->nullifyEvictable(Lcom/google/common/collect/gi;)V

    sget-object v0, Lcom/google/common/collect/MapMakerInternalMap$NullEntry;->INSTANCE:Lcom/google/common/collect/MapMakerInternalMap$NullEntry;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/common/collect/fz;->a:Lcom/google/common/collect/gi;

    if-eq v0, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0}, Lcom/google/common/collect/gi;->getNextEvictable()Lcom/google/common/collect/gi;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method
