.class Lcom/google/common/collect/eq;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Lcom/google/common/collect/ep;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ep;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/eq;->a:Lcom/google/common/collect/ep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/common/collect/jj;Lcom/google/common/collect/jj;)I
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/eq;->a:Lcom/google/common/collect/ep;

    iget-object v0, v0, Lcom/google/common/collect/ep;->b:Ljava/util/Comparator;

    invoke-interface {p1}, Lcom/google/common/collect/jj;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/common/collect/jj;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/common/collect/jj;

    check-cast p2, Lcom/google/common/collect/jj;

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/eq;->a(Lcom/google/common/collect/jj;Lcom/google/common/collect/jj;)I

    move-result v0

    return v0
.end method
