.class Lcom/google/common/collect/gd;
.super Lcom/google/common/collect/fw;


# instance fields
.field a:Lcom/google/common/collect/gi;

.field b:Lcom/google/common/collect/gi;

.field final synthetic c:Lcom/google/common/collect/gc;


# direct methods
.method constructor <init>(Lcom/google/common/collect/gc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gd;->c:Lcom/google/common/collect/gc;

    invoke-direct {p0}, Lcom/google/common/collect/fw;-><init>()V

    iput-object p0, p0, Lcom/google/common/collect/gd;->a:Lcom/google/common/collect/gi;

    iput-object p0, p0, Lcom/google/common/collect/gd;->b:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public getExpirationTime()J
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public getNextExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gd;->a:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gd;->b:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public setExpirationTime(J)V
    .locals 0

    return-void
.end method

.method public setNextExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gd;->a:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gd;->b:Lcom/google/common/collect/gi;

    return-void
.end method
