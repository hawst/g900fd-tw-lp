.class public interface abstract Lcom/google/common/collect/kk;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/common/collect/is;
.implements Lcom/google/common/collect/kf;


# virtual methods
.method public abstract comparator()Ljava/util/Comparator;
.end method

.method public abstract descendingMultiset()Lcom/google/common/collect/kk;
.end method

.method public abstract elementSet()Ljava/util/SortedSet;
.end method

.method public abstract firstEntry()Lcom/google/common/collect/it;
.end method

.method public abstract headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
.end method

.method public abstract lastEntry()Lcom/google/common/collect/it;
.end method

.method public abstract subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
.end method

.method public abstract tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
.end method
