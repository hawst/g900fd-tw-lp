.class Lcom/google/common/collect/fj;
.super Ljava/util/AbstractSet;


# instance fields
.field final synthetic a:Lcom/google/common/collect/fh;


# direct methods
.method constructor <init>(Lcom/google/common/collect/fh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/fj;->a:Lcom/google/common/collect/fh;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Lcom/google/common/collect/fg;

    iget-object v1, p0, Lcom/google/common/collect/fj;->a:Lcom/google/common/collect/fh;

    iget-object v1, v1, Lcom/google/common/collect/fh;->a:Lcom/google/common/collect/LinkedListMultimap;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/fg;-><init>(Lcom/google/common/collect/LinkedListMultimap;Lcom/google/common/collect/ev;)V

    new-instance v1, Lcom/google/common/collect/fk;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/fk;-><init>(Lcom/google/common/collect/fj;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/fj;->a:Lcom/google/common/collect/fh;

    iget-object v0, v0, Lcom/google/common/collect/fh;->a:Lcom/google/common/collect/LinkedListMultimap;

    # getter for: Lcom/google/common/collect/LinkedListMultimap;->keyCount:Lcom/google/common/collect/is;
    invoke-static {v0}, Lcom/google/common/collect/LinkedListMultimap;->access$600(Lcom/google/common/collect/LinkedListMultimap;)Lcom/google/common/collect/is;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/is;->elementSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
