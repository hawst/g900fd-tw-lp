.class Lcom/google/common/collect/la;
.super Lcom/google/common/collect/AbstractIterator;


# instance fields
.field final a:Ljava/util/Iterator;

.field final synthetic b:Lcom/google/common/collect/ky;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ky;)V
    .locals 1

    iput-object p1, p0, Lcom/google/common/collect/la;->b:Lcom/google/common/collect/ky;

    invoke-direct {p0}, Lcom/google/common/collect/AbstractIterator;-><init>()V

    iget-object v0, p0, Lcom/google/common/collect/la;->b:Lcom/google/common/collect/ky;

    iget-object v0, v0, Lcom/google/common/collect/ky;->d:Lcom/google/common/collect/StandardTable;

    iget-object v0, v0, Lcom/google/common/collect/StandardTable;->backingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/la;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/la;->c()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method protected c()Ljava/util/Map$Entry;
    .locals 3

    :cond_0
    iget-object v0, p0, Lcom/google/common/collect/la;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/common/collect/la;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    iget-object v2, p0, Lcom/google/common/collect/la;->b:Lcom/google/common/collect/ky;

    iget-object v2, v2, Lcom/google/common/collect/ky;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/common/collect/lb;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/lb;-><init>(Lcom/google/common/collect/la;Ljava/util/Map$Entry;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/common/collect/la;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method
