.class abstract Lcom/google/common/collect/ja;
.super Ljava/util/AbstractSet;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a()Lcom/google/common/collect/is;
.end method

.method public clear()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/ja;->a()Lcom/google/common/collect/is;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/is;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/common/collect/it;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/common/collect/it;

    invoke-interface {p1}, Lcom/google/common/collect/it;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/common/collect/ja;->a()Lcom/google/common/collect/is;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/common/collect/it;->getElement()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/common/collect/is;->count(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {p1}, Lcom/google/common/collect/it;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ja;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/common/collect/ja;->a()Lcom/google/common/collect/is;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/is;->elementSet()Ljava/util/Set;

    move-result-object v0

    check-cast p1, Lcom/google/common/collect/it;

    invoke-interface {p1}, Lcom/google/common/collect/it;->getElement()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
