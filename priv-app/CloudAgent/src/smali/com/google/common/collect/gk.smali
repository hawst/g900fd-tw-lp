.class final Lcom/google/common/collect/gk;
.super Lcom/google/common/collect/gj;

# interfaces
.implements Lcom/google/common/collect/gi;


# instance fields
.field d:Lcom/google/common/collect/gi;

.field e:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/collect/gj;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gi;)V

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->d:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public getNextEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->d:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public setNextEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->d:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gi;

    return-void
.end method
