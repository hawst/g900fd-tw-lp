.class Lcom/google/common/collect/dl;
.super Lcom/google/common/collect/mz;


# instance fields
.field final synthetic a:Lcom/google/common/collect/mz;

.field final synthetic b:Lcom/google/common/collect/ImmutableList$ReverseImmutableList;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ImmutableList$ReverseImmutableList;Lcom/google/common/collect/mz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/dl;->b:Lcom/google/common/collect/ImmutableList$ReverseImmutableList;

    iput-object p2, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-direct {p0}, Lcom/google/common/collect/mz;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v0}, Lcom/google/common/collect/mz;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v0}, Lcom/google/common/collect/mz;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v0}, Lcom/google/common/collect/mz;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/dl;->b:Lcom/google/common/collect/ImmutableList$ReverseImmutableList;

    iget-object v1, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v1}, Lcom/google/common/collect/mz;->previousIndex()I

    move-result v1

    # invokes: Lcom/google/common/collect/ImmutableList$ReverseImmutableList;->reverseIndex(I)I
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList$ReverseImmutableList;->access$000(Lcom/google/common/collect/ImmutableList$ReverseImmutableList;I)I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v0}, Lcom/google/common/collect/mz;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/dl;->b:Lcom/google/common/collect/ImmutableList$ReverseImmutableList;

    iget-object v1, p0, Lcom/google/common/collect/dl;->a:Lcom/google/common/collect/mz;

    invoke-virtual {v1}, Lcom/google/common/collect/mz;->nextIndex()I

    move-result v1

    # invokes: Lcom/google/common/collect/ImmutableList$ReverseImmutableList;->reverseIndex(I)I
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableList$ReverseImmutableList;->access$000(Lcom/google/common/collect/ImmutableList$ReverseImmutableList;I)I

    move-result v0

    return v0
.end method
