.class final Lcom/google/common/collect/ha;
.super Ljava/lang/ref/WeakReference;

# interfaces
.implements Lcom/google/common/collect/gu;


# instance fields
.field final a:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/common/collect/gi;)V
    .locals 0

    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    iput-object p3, p0, Lcom/google/common/collect/ha;->a:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ha;->a:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Lcom/google/common/collect/gi;)Lcom/google/common/collect/gu;
    .locals 2

    new-instance v0, Lcom/google/common/collect/ha;

    invoke-virtual {p0}, Lcom/google/common/collect/ha;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, Lcom/google/common/collect/ha;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/google/common/collect/gi;)V

    return-object v0
.end method

.method public a(Lcom/google/common/collect/gu;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/common/collect/ha;->clear()V

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/ha;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
