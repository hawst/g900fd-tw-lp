.class Lcom/google/common/collect/ll;
.super Lcom/google/common/collect/my;


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Lcom/google/common/collect/lk;


# direct methods
.method constructor <init>(Lcom/google/common/collect/lk;Ljava/util/Iterator;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/ll;->b:Lcom/google/common/collect/lk;

    iput-object p2, p0, Lcom/google/common/collect/ll;->a:Ljava/util/Iterator;

    invoke-direct {p0}, Lcom/google/common/collect/my;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Map$Entry;
    .locals 3

    iget-object v0, p0, Lcom/google/common/collect/ll;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lcom/google/common/collect/ImmutableEntry;

    iget-object v2, p0, Lcom/google/common/collect/ll;->b:Lcom/google/common/collect/lk;

    iget-object v2, v2, Lcom/google/common/collect/lk;->a:Lcom/google/common/collect/lj;

    iget-object v2, v2, Lcom/google/common/collect/lj;->b:Lcom/google/common/collect/StandardTable;

    invoke-virtual {v2, v0}, Lcom/google/common/collect/StandardTable;->column(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/common/collect/ImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ll;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/ll;->a()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method
