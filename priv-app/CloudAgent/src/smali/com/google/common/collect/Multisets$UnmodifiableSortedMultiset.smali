.class final Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;
.super Lcom/google/common/collect/Multisets$UnmodifiableMultiset;

# interfaces
.implements Lcom/google/common/collect/kk;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private transient descendingMultiset:Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;


# direct methods
.method private constructor <init>(Lcom/google/common/collect/kk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/Multisets$UnmodifiableMultiset;-><init>(Lcom/google/common/collect/is;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/common/collect/kk;Lcom/google/common/collect/iu;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;-><init>(Lcom/google/common/collect/kk;)V

    return-void
.end method


# virtual methods
.method public comparator()Ljava/util/Comparator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/kk;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method bridge synthetic createElementSet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->createElementSet()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method createElementSet()Ljava/util/SortedSet;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/kk;->elementSet()Ljava/util/SortedSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic delegate()Lcom/google/common/collect/is;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method

.method protected delegate()Lcom/google/common/collect/kk;
    .locals 1

    invoke-super {p0}, Lcom/google/common/collect/Multisets$UnmodifiableMultiset;->delegate()Lcom/google/common/collect/is;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/kk;

    return-object v0
.end method

.method protected bridge synthetic delegate()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic delegate()Ljava/util/Collection;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method

.method public descendingMultiset()Lcom/google/common/collect/kk;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->descendingMultiset:Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/common/collect/kk;->descendingMultiset()Lcom/google/common/collect/kk;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;-><init>(Lcom/google/common/collect/kk;)V

    iput-object p0, v0, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->descendingMultiset:Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;

    iput-object v0, p0, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->descendingMultiset:Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic elementSet()Ljava/util/Set;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->elementSet()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public elementSet()Ljava/util/SortedSet;
    .locals 1

    invoke-super {p0}, Lcom/google/common/collect/Multisets$UnmodifiableMultiset;->elementSet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public firstEntry()Lcom/google/common/collect/it;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/kk;->firstEntry()Lcom/google/common/collect/it;

    move-result-object v0

    return-object v0
.end method

.method public headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/kk;->headMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Multisets;->a(Lcom/google/common/collect/kk;)Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method

.method public lastEntry()Lcom/google/common/collect/it;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/kk;->lastEntry()Lcom/google/common/collect/it;

    move-result-object v0

    return-object v0
.end method

.method public pollFirstEntry()Lcom/google/common/collect/it;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public pollLastEntry()Lcom/google/common/collect/it;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/common/collect/kk;->subMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Multisets;->a(Lcom/google/common/collect/kk;)Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method

.method public tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/Multisets$UnmodifiableSortedMultiset;->delegate()Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/collect/kk;->tailMultiset(Ljava/lang/Object;Lcom/google/common/collect/BoundType;)Lcom/google/common/collect/kk;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/Multisets;->a(Lcom/google/common/collect/kk;)Lcom/google/common/collect/kk;

    move-result-object v0

    return-object v0
.end method
