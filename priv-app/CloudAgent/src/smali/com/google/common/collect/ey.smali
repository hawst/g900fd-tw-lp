.class Lcom/google/common/collect/ey;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final synthetic a:Lcom/google/common/collect/fn;

.field final synthetic b:Lcom/google/common/collect/ex;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ex;Lcom/google/common/collect/fn;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/ey;->b:Lcom/google/common/collect/ex;

    iput-object p2, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->hasNext()Z

    move-result v0

    return v0
.end method

.method public hasPrevious()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->a()Lcom/google/common/collect/fm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/common/collect/fm;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->nextIndex()I

    move-result v0

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->b()Lcom/google/common/collect/fm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/common/collect/fm;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->previousIndex()I

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0}, Lcom/google/common/collect/fn;->remove()V

    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ey;->a:Lcom/google/common/collect/fn;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/fn;->a(Ljava/lang/Object;)V

    return-void
.end method
