.class public Lcom/google/common/collect/ea;
.super Lcom/google/common/collect/dt;


# instance fields
.field private final b:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1

    invoke-static {p1}, Lcom/google/common/collect/TreeMultiset;->create(Ljava/util/Comparator;)Lcom/google/common/collect/TreeMultiset;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/common/collect/dt;-><init>(Lcom/google/common/collect/is;)V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/google/common/collect/ea;->b:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/google/common/collect/ImmutableMultiset;
    .locals 1

    invoke-virtual {p0}, Lcom/google/common/collect/ea;->b()Lcom/google/common/collect/ImmutableSortedMultiset;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Iterable;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c(Ljava/lang/Iterable;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c(Ljava/lang/Object;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c([Ljava/lang/Object;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;I)Lcom/google/common/collect/dt;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/ea;->b(Ljava/lang/Object;I)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/common/collect/ImmutableSortedMultiset;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/ea;->b:Ljava/util/Comparator;

    iget-object v1, p0, Lcom/google/common/collect/ea;->a:Lcom/google/common/collect/is;

    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSortedMultiset;->copyOf(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableSortedMultiset;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Iterable;)Lcom/google/common/collect/dt;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c(Ljava/lang/Iterable;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Lcom/google/common/collect/dt;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c(Ljava/lang/Object;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([Ljava/lang/Object;)Lcom/google/common/collect/dt;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/ea;->c([Ljava/lang/Object;)Lcom/google/common/collect/ea;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)Lcom/google/common/collect/ea;
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/common/collect/dt;->a(Ljava/lang/Object;I)Lcom/google/common/collect/dt;

    return-object p0
.end method

.method public c(Ljava/lang/Iterable;)Lcom/google/common/collect/ea;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/dt;->b(Ljava/lang/Iterable;)Lcom/google/common/collect/dt;

    return-object p0
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/ea;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/dt;->b(Ljava/lang/Object;)Lcom/google/common/collect/dt;

    return-object p0
.end method

.method public varargs c([Ljava/lang/Object;)Lcom/google/common/collect/ea;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/dt;->b([Ljava/lang/Object;)Lcom/google/common/collect/dt;

    return-object p0
.end method
