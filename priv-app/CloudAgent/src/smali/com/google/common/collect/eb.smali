.class public final Lcom/google/common/collect/eb;
.super Lcom/google/common/collect/du;


# instance fields
.field private final b:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/common/collect/du;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/google/common/collect/eb;->b:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/ImmutableSortedSet;
    .locals 2

    iget-object v0, p0, Lcom/google/common/collect/eb;->b:Ljava/util/Comparator;

    iget-object v1, p0, Lcom/google/common/collect/eb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    # invokes: Lcom/google/common/collect/ImmutableSortedSet;->copyOfInternal(Ljava/util/Comparator;Ljava/util/Iterator;)Lcom/google/common/collect/ImmutableSortedSet;
    invoke-static {v0, v1}, Lcom/google/common/collect/ImmutableSortedSet;->access$000(Ljava/util/Comparator;Ljava/util/Iterator;)Lcom/google/common/collect/ImmutableSortedSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Iterable;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c(Ljava/lang/Iterable;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c(Ljava/lang/Object;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([Ljava/lang/Object;)Lcom/google/common/collect/dj;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c([Ljava/lang/Object;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Iterable;)Lcom/google/common/collect/du;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c(Ljava/lang/Iterable;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Lcom/google/common/collect/du;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c(Ljava/lang/Object;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b([Ljava/lang/Object;)Lcom/google/common/collect/du;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/common/collect/eb;->c([Ljava/lang/Object;)Lcom/google/common/collect/eb;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Iterable;)Lcom/google/common/collect/eb;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/du;->b(Ljava/lang/Iterable;)Lcom/google/common/collect/du;

    return-object p0
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/eb;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/du;->b(Ljava/lang/Object;)Lcom/google/common/collect/du;

    return-object p0
.end method

.method public varargs c([Ljava/lang/Object;)Lcom/google/common/collect/eb;
    .locals 0

    invoke-super {p0, p1}, Lcom/google/common/collect/du;->b([Ljava/lang/Object;)Lcom/google/common/collect/du;

    return-object p0
.end method
