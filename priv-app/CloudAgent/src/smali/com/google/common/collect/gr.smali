.class final Lcom/google/common/collect/gr;
.super Lcom/google/common/collect/go;

# interfaces
.implements Lcom/google/common/collect/gi;


# instance fields
.field volatile e:J

.field f:Lcom/google/common/collect/gi;

.field g:Lcom/google/common/collect/gi;

.field h:Lcom/google/common/collect/gi;

.field i:Lcom/google/common/collect/gi;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/common/collect/gi;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/collect/go;-><init>(Ljava/lang/Object;ILcom/google/common/collect/gi;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/common/collect/gr;->e:J

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gr;->f:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gr;->g:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gr;->h:Lcom/google/common/collect/gi;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gr;->i:Lcom/google/common/collect/gi;

    return-void
.end method


# virtual methods
.method public getExpirationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/gr;->e:J

    return-wide v0
.end method

.method public getNextEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gr;->h:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getNextExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gr;->f:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousEvictable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gr;->i:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public getPreviousExpirable()Lcom/google/common/collect/gi;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gr;->g:Lcom/google/common/collect/gi;

    return-object v0
.end method

.method public setExpirationTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/common/collect/gr;->e:J

    return-void
.end method

.method public setNextEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gr;->h:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setNextExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gr;->f:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousEvictable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gr;->i:Lcom/google/common/collect/gi;

    return-void
.end method

.method public setPreviousExpirable(Lcom/google/common/collect/gi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gr;->g:Lcom/google/common/collect/gi;

    return-void
.end method
