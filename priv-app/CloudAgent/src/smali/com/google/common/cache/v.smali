.class Lcom/google/common/cache/v;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/common/cache/ag;


# instance fields
.field volatile a:Lcom/google/common/cache/ag;

.field final b:Lcom/google/common/util/concurrent/l;

.field final c:Lcom/google/common/base/ak;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lcom/google/common/cache/LocalCache;->o()Lcom/google/common/cache/ag;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/common/cache/v;-><init>(Lcom/google/common/cache/ag;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/common/cache/ag;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/util/concurrent/l;->b()Lcom/google/common/util/concurrent/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    new-instance v0, Lcom/google/common/base/ak;

    invoke-direct {v0}, Lcom/google/common/base/ak;-><init>()V

    iput-object v0, p0, Lcom/google/common/cache/v;->c:Lcom/google/common/base/ak;

    iput-object p1, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    return-void
.end method

.method private static a(Lcom/google/common/util/concurrent/l;Ljava/lang/Throwable;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/common/util/concurrent/l;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/f;
    .locals 1

    invoke-static {}, Lcom/google/common/util/concurrent/l;->b()Lcom/google/common/util/concurrent/l;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/cache/v;->a(Lcom/google/common/util/concurrent/l;Ljava/lang/Throwable;)Z

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    invoke-interface {v0}, Lcom/google/common/cache/ag;->a()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Lcom/google/common/cache/x;)Lcom/google/common/cache/ag;
    .locals 0

    return-object p0
.end method

.method public a(Ljava/lang/Object;Lcom/google/common/cache/CacheLoader;)Lcom/google/common/util/concurrent/f;
    .locals 2

    iget-object v0, p0, Lcom/google/common/cache/v;->c:Lcom/google/common/base/ak;

    invoke-virtual {v0}, Lcom/google/common/base/ak;->a()Lcom/google/common/base/ak;

    iget-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    invoke-interface {v0}, Lcom/google/common/cache/ag;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-virtual {p2, p1}, Lcom/google/common/cache/CacheLoader;->load(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/common/cache/v;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Lcom/google/common/util/concurrent/d;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/f;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p2, p1, v0}, Lcom/google/common/cache/CacheLoader;->reload(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/util/concurrent/f;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/common/util/concurrent/d;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/f;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/google/common/cache/v;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/common/cache/v;->b(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/f;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/common/cache/v;->b(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/common/cache/LocalCache;->o()Lcom/google/common/cache/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    invoke-static {v0, p1}, Lcom/google/common/cache/v;->a(Lcom/google/common/util/concurrent/l;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/google/common/cache/x;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/l;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    invoke-interface {v0}, Lcom/google/common/cache/ag;->d()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->b:Lcom/google/common/util/concurrent/l;

    invoke-static {v0}, Lcom/google/common/util/concurrent/m;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    iget-object v0, p0, Lcom/google/common/cache/v;->c:Lcom/google/common/base/ak;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/common/base/ak;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Lcom/google/common/cache/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    return-object v0
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/common/cache/v;->a:Lcom/google/common/cache/ag;

    invoke-interface {v0}, Lcom/google/common/cache/ag;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
