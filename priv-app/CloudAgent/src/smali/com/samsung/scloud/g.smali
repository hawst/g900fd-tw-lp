.class public Lcom/samsung/scloud/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/scloud/j;


# static fields
.field private static n:Lcom/samsung/scloud/g;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field public d:Z

.field public e:Lcom/samsung/scloud/a/b/a;

.field public f:Lcom/google/api/client/http/v;

.field public g:Lcom/google/api/client/json/d;

.field public h:Lcom/google/api/client/http/r;

.field public i:Lcom/google/api/a/a/a;

.field j:Z

.field public k:Ljava/lang/String;

.field private l:Ljava/util/concurrent/CountDownLatch;

.field private m:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/g;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/g;->c:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/samsung/scloud/g;->d:Z

    iput-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    iput-boolean v1, p0, Lcom/samsung/scloud/g;->j:Z

    iput-object v0, p0, Lcom/samsung/scloud/g;->k:Ljava/lang/String;

    iput-object p1, p0, Lcom/samsung/scloud/g;->m:Landroid/content/Context;

    new-instance v0, Lcom/google/api/client/http/a/c;

    invoke-direct {v0}, Lcom/google/api/client/http/a/c;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/g;->f:Lcom/google/api/client/http/v;

    new-instance v0, Lcom/google/api/client/json/a/a;

    invoke-direct {v0}, Lcom/google/api/client/json/a/a;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/g;->g:Lcom/google/api/client/json/d;

    new-instance v0, Lcom/samsung/scloud/a/b/b;

    invoke-direct {v0}, Lcom/samsung/scloud/a/b/b;-><init>()V

    iget-object v1, p0, Lcom/samsung/scloud/g;->g:Lcom/google/api/client/json/d;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/a/b/b;->b(Lcom/google/api/client/json/d;)Lcom/google/api/client/googleapis/auth/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/g;->f:Lcom/google/api/client/http/v;

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/auth/a/b;->b(Lcom/google/api/client/http/v;)Lcom/google/api/client/googleapis/auth/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/auth/a/b;->i()Lcom/google/api/client/googleapis/auth/a/a;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/a/b/a;

    iput-object v0, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    iget-object v0, p0, Lcom/samsung/scloud/g;->f:Lcom/google/api/client/http/v;

    iget-object v1, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/v;->a(Lcom/google/api/client/http/s;)Lcom/google/api/client/http/r;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/g;->h:Lcom/google/api/client/http/r;

    new-instance v0, Lcom/google/api/a/a/d;

    iget-object v1, p0, Lcom/samsung/scloud/g;->f:Lcom/google/api/client/http/v;

    iget-object v2, p0, Lcom/samsung/scloud/g;->g:Lcom/google/api/client/json/d;

    iget-object v3, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/api/a/a/d;-><init>(Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Lcom/google/api/client/http/s;)V

    new-instance v1, Lcom/google/api/client/googleapis/b/c;

    const-string v2, "AIzaSyAEKSS6tGrKKGR25Emd-gXKs22SlF3QjBQ"

    invoke-direct {v1, v2}, Lcom/google/api/client/googleapis/b/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/api/a/a/d;->b(Lcom/google/api/client/http/b/e;)Lcom/google/api/a/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/d;->k()Lcom/google/api/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {p0}, Lcom/samsung/scloud/g;->f()V

    return-void
.end method

.method private a(Lcom/google/api/a/a/a/h;)Lcom/samsung/scloud/b/f;
    .locals 7

    const/4 v0, 0x0

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "application/vnd.google-apps.folder"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lcom/samsung/scloud/b/e;

    invoke-direct {v1}, Lcom/samsung/scloud/b/e;-><init>()V

    sget-object v2, Lcom/samsung/scloud/b/f;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->m()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->m()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    move v3, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/k;

    invoke-virtual {v0}, Lcom/google/api/a/a/a/k;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/google/api/a/a/a/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    const/4 v2, 0x1

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/google/api/a/a/a/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/scloud/b/f;->g(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/api/a/a/a/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move v0, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_2

    :cond_4
    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V

    goto :goto_1

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v1, v4}, Lcom/samsung/scloud/b/f;->b(Ljava/util/List;)V

    :cond_6
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->i(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->h(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->l()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->l()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->c(J)V

    :cond_7
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->c()Lcom/google/api/a/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->c()Lcom/google/api/a/a/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/a/i;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->b(Z)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->c()Lcom/google/api/a/a/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/a/i;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->c(Z)V

    :goto_3
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->k()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->a(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->v()Lcom/google/api/a/a/a/l;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->v()Lcom/google/api/a/a/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/a/l;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->j(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->l(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->m(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->n(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->r()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->r()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->d(J)V

    :goto_5
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->w()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->w()Lcom/google/api/client/util/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->e(J)V

    :goto_6
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->o(Ljava/lang/String;)V

    instance-of v0, v1, Lcom/samsung/scloud/b/d;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->p()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->b(J)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->q()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->q()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->f(J)V

    :goto_7
    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->k(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->p()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v0, v2, v4

    if-gez v0, :cond_d

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->n()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->n()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_d

    sget-object v0, Lcom/samsung/scloud/b/f;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    :goto_8
    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->e(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/scloud/g;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[parseDriveFile] Label is null - title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_9
    const-string v0, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[parseDriveFile] Role is null - title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_a
    const-string v0, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[parseDriveFile] CreatedDate is null - title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_b
    const-string v0, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[parseDriveFile] UpdatedTime is null - title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_c
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->f(J)V

    goto/16 :goto_7

    :cond_d
    sget-object v0, Lcom/samsung/scloud/b/f;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_e
    const-string v0, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[parseDriveFile] MimeType is null - title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/a/a/a/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private a(Lcom/google/api/a/a/a/a;)Lcom/samsung/scloud/b/j;
    .locals 6

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/samsung/scloud/b/j;

    invoke-direct {v1}, Lcom/samsung/scloud/b/j;-><init>()V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/j;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/j;->d(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->l()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->l()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/j;->e(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->j()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/j;->a(J)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/j;->b(J)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->m()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/j;->c(J)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/j;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/j;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/api/a/a/a/a;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/g;

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.drawing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/b/j;->h(J)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.presentation"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/b/j;->g(J)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.document"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/b/j;->e(J)V

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.spreadsheet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/b/j;->f(J)V

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/pdf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/google/api/a/a/a/g;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/scloud/b/j;->i(J)V

    goto/16 :goto_1

    :cond_7
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/samsung/scloud/g;
    .locals 2

    const-class v1, Lcom/samsung/scloud/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/scloud/g;->n:Lcom/samsung/scloud/g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/scloud/g;

    invoke-direct {v0, p0}, Lcom/samsung/scloud/g;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/scloud/g;->n:Lcom/samsung/scloud/g;

    :cond_0
    sget-object v0, Lcom/samsung/scloud/g;->n:Lcom/samsung/scloud/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    move v3, v1

    move v0, v1

    :goto_1
    const/4 v4, 0x3

    if-ge v3, v4, :cond_5

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v4, "execute"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v4, 0x0

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getting excute() method of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed [SecurityException] -"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getting excute() method of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed [NoSuchMethodException] -"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object v0, v2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Executing excute() method of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed [IllegalArgumentException] -"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Executing excute() method of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed [IllegalAccessException] -"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v5, v4, Ljavax/net/ssl/SSLException;

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    const-string v0, "SCLOUD_SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[safeExcute] - handle SSL Exception(retry #"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") causeMsg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v4, 0x7d0

    invoke-direct {p0, v4, v5}, Lcom/samsung/scloud/g;->a(J)V

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    invoke-direct {p0, v4}, Lcom/samsung/scloud/g;->a(Ljava/lang/Throwable;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/scloud/g;->g()V

    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V

    const-string v5, "SCLOUD_SDK"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[safeExcute] - handle Auth Exception(retry #"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") causeMsg="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    instance-of v1, v4, Ljava/io/IOException;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IOException occured during executing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Other(not IO) Exception occured during executing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-eqz v0, :cond_6

    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v1, "server token expired"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v1, "[safeExcute] - IOException"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/samsung/scloud/g;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/g;->l:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private a(J)V
    .locals 1

    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    instance-of v0, p1, Lcom/samsung/scloud/exception/SCloudIOException;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    instance-of v0, p1, Lcom/samsung/scloud/exception/SCloudAuthException;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/lang/Throwable;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/api/client/http/HttpResponseException;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/api/client/http/HttpResponseException;

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x191

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x193

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x194

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/samsung/scloud/g;->d:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->m:Landroid/content/Context;

    const-string v1, "googleCredential"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "accessToken"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    const-string v1, "curAccountName"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/g;->c:Ljava/lang/String;

    const-string v0, "SCLOUD_SDK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading Access token : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Account name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/g;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    iget-object v1, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/a/b/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    goto :goto_0

    :cond_1
    const-string v0, "SCLOUD_SDK"

    const-string v1, "Token is null !"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/samsung/scloud/b/d;
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/a/a/a;->c()Lcom/google/api/a/a/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/api/a/a/e;->b(Ljava/lang/String;)Lcom/google/api/a/a/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Lcom/google/api/a/a/a/h;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/d;

    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "getFileInfo failed"

    invoke-direct {p0, v0, v2}, Lcom/samsung/scloud/g;->a(Ljava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/a/a/a;->c()Lcom/google/api/a/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/e;->a()Lcom/google/api/a/a/g;

    move-result-object v0

    const-string v1, "mimeType=\'application/vnd.google-apps.folder\'"

    invoke-virtual {v0, v1}, Lcom/google/api/a/a/g;->a(Ljava/lang/String;)Lcom/google/api/a/a/g;

    if-le p1, v4, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/a/a/g;->a(Ljava/lang/Integer;)Lcom/google/api/a/a/g;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/api/a/a/g;->b(Ljava/lang/String;)Lcom/google/api/a/a/g;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/j;
    :try_end_1
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v4, :cond_3

    :cond_2
    move-object v0, v2

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWholeFolderList - build request failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    throw v0

    :cond_3
    new-instance v1, Lcom/samsung/scloud/b/g;

    invoke-direct {v1}, Lcom/samsung/scloud/b/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_7

    :cond_4
    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->c(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_5

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->b(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/h;

    invoke-virtual {v0}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.folder"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Lcom/google/api/a/a/a/h;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/e;

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/b/h;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/b/i;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public a()Lcom/samsung/scloud/b/j;
    .locals 3

    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/a/a/a;->d()Lcom/google/api/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/b;->a()Lcom/google/api/a/a/c;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Lcom/google/api/a/a/a/a;)Lcom/samsung/scloud/b/j;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "getUserInfo - getUserInfo failed"

    invoke-direct {p0, v0, v2}, Lcom/samsung/scloud/g;->a(Ljava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public b(ILjava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {v0}, Lcom/google/api/a/a/a;->c()Lcom/google/api/a/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/a/a/e;->a()Lcom/google/api/a/a/g;

    move-result-object v0

    if-le p1, v4, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/a/a/g;->a(Ljava/lang/Integer;)Lcom/google/api/a/a/g;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/api/a/a/g;->b(Ljava/lang/String;)Lcom/google/api/a/a/g;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/j;
    :try_end_1
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v4, :cond_3

    :cond_2
    move-object v0, v2

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWholeFileList - build request failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    throw v0

    :cond_3
    new-instance v1, Lcom/samsung/scloud/b/g;

    invoke-direct {v1}, Lcom/samsung/scloud/b/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_7

    :cond_4
    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->c(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_5

    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->b(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/a/a/a/h;

    invoke-virtual {v0}, Lcom/google/api/a/a/a/h;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.folder"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-direct {p0, v0}, Lcom/samsung/scloud/g;->a(Lcom/google/api/a/a/a/h;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/d;

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lcom/google/api/a/a/a/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/b/g;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v0, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/scloud/g;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lcom/samsung/scloud/g;->i:Lcom/google/api/a/a/a;

    invoke-virtual {v1}, Lcom/google/api/a/a/a;->c()Lcom/google/api/a/a/e;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/a/a/e;->a(Ljava/lang/String;)Lcom/google/api/a/a/h;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/g;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "delete - deleting file failed"

    invoke-direct {p0, v0, v1}, Lcom/samsung/scloud/g;->a(Ljava/lang/Exception;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public d(Ljava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public d()Z
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public e()V
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public f()V
    .locals 2

    const-class v0, Lcom/google/api/client/http/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    new-instance v1, Lcom/samsung/scloud/h;

    invoke-direct {v1, p0}, Lcom/samsung/scloud/h;-><init>(Lcom/samsung/scloud/g;)V

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/scloud/g;->m:Landroid/content/Context;

    const-string v1, "googleCredential"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accessToken"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    invoke-virtual {v0, p1}, Lcom/samsung/scloud/a/b/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    :cond_0
    return-void
.end method

.method public g()V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/scloud/g;->l:Ljava/util/concurrent/CountDownLatch;

    const-string v0, "SCLOUD_SDK"

    const-string v1, "Token expired !! Retry get access token..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/api/client/googleapis/a/a/a/a;

    iget-object v1, p0, Lcom/samsung/scloud/g;->m:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/api/client/googleapis/a/a/a/a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    invoke-virtual {v1}, Lcom/samsung/scloud/a/b/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/a/a/a/a;->b(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/samsung/scloud/g;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/scloud/g;->e:Lcom/samsung/scloud/a/b/a;

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/a/b/a;->c(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/a/a;

    iget-object v1, p0, Lcom/samsung/scloud/g;->m:Landroid/content/Context;

    const-string v2, "googleCredential"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "accessToken"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/samsung/scloud/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/client/googleapis/a/a/a/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "SCLOUD_SDK"

    const-string v1, "Account is null !"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "SCLOUD_SDK"

    const-string v4, "Before retry get access token ..."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/a/a/a/a;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "oauth2:https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/drive"

    new-instance v4, Lcom/samsung/scloud/i;

    invoke-direct {v4, p0}, Lcom/samsung/scloud/i;-><init>(Lcom/samsung/scloud/g;)V

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/g;->l:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    iget-boolean v0, p0, Lcom/samsung/scloud/g;->j:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v1, "server account info changed"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)Lcom/samsung/scloud/b/i;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method
