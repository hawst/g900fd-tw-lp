.class public Lcom/samsung/scloud/d;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/samsung/scloud/a;

.field private b:Lcom/dropbox/client2/l;

.field private c:Z

.field private d:Ljava/io/File;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/samsung/scloud/c/a;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/a;Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/c/a;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/d;->c:Z

    iput-object p2, p0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    iput-object p3, p0, Lcom/samsung/scloud/d;->e:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/samsung/scloud/d;->f:Z

    iput-object p5, p0, Lcom/samsung/scloud/d;->g:Ljava/lang/String;

    iput-object p6, p0, Lcom/samsung/scloud/d;->h:Lcom/samsung/scloud/c/a;

    return-void
.end method

.method static synthetic a(Lcom/samsung/scloud/d;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    return-object v0
.end method

.method static synthetic b(Lcom/samsung/scloud/d;)Lcom/samsung/scloud/c/a;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/d;->h:Lcom/samsung/scloud/c/a;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/samsung/scloud/b/d;
    .locals 18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    :try_start_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudInvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;->printStackTrace()V

    :cond_1
    new-instance v8, Lcom/samsung/scloud/e;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/scloud/e;-><init>(Lcom/samsung/scloud/d;)V

    const/4 v3, 0x0

    const/16 v16, 0x0

    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_0
    const/4 v2, 0x5

    move/from16 v0, v17

    if-ge v0, v2, :cond_a

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/scloud/d;->c:Z

    if-nez v2, :cond_2

    new-instance v2, Lcom/samsung/scloud/exception/SCloudCancelException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file upload cancelled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudCancelException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/scloud/d;->f:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-static {v2}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual/range {v3 .. v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Ljava/io/InputStream;JLcom/dropbox/client2/m;)Lcom/dropbox/client2/l;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/scloud/d;->c:Z

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    invoke-interface {v2}, Lcom/dropbox/client2/l;->a()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    invoke-interface {v2}, Lcom/dropbox/client2/l;->b()Lcom/dropbox/client2/i;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v3

    :goto_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->h:Lcom/samsung/scloud/c/a;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->h:Lcom/samsung/scloud/c/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    const-wide/16 v10, 0x64

    invoke-interface {v2, v4, v6, v10, v11}, Lcom/samsung/scloud/c/a;->a(Ljava/lang/String;IJ)V

    :cond_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-static {v2, v3}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/b/d;

    :goto_3
    return-object v2

    :cond_5
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-static {v2}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/d;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/scloud/d;->g:Ljava/lang/String;

    move-object v11, v5

    move-object v15, v8

    invoke-virtual/range {v9 .. v15}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Lcom/dropbox/client2/m;)Lcom/dropbox/client2/l;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/scloud/d;->c:Z

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    invoke-interface {v2}, Lcom/dropbox/client2/l;->a()V

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    invoke-interface {v2}, Lcom/dropbox/client2/l;->b()Lcom/dropbox/client2/i;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v3

    goto :goto_1

    :catch_1
    move-exception v2

    :goto_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    if-eqz v5, :cond_7

    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-static {v4, v2}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x4

    move/from16 v0, v17

    if-ge v0, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;I)V

    :goto_5
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    move-object/from16 v16, v3

    move-object v3, v5

    goto/16 :goto_0

    :catch_2
    move-exception v2

    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v3, "file close failed"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/d;->a:Lcom/samsung/scloud/a;

    invoke-static {v4, v2}, Lcom/samsung/scloud/a;->b(Lcom/samsung/scloud/a;Ljava/lang/Exception;)V

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    :catch_3
    move-exception v2

    move-object v5, v3

    move-object/from16 v3, v16

    goto :goto_4

    :catch_4
    move-exception v2

    move-object/from16 v3, v16

    goto :goto_4

    :cond_a
    move-object/from16 v3, v16

    goto/16 :goto_2
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/scloud/d;->c:Z

    iget-object v1, p0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/d;->b:Lcom/dropbox/client2/l;

    invoke-interface {v0}, Lcom/dropbox/client2/l;->a()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
