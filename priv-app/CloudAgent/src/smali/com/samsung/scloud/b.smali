.class public Lcom/samsung/scloud/b;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/samsung/scloud/a;

.field private b:Lcom/dropbox/client2/g;

.field private c:Z

.field private d:Ljava/io/File;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Lcom/samsung/scloud/c/a;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/a;Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/c/a;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/b;->c:Z

    iput-object p2, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    iput-object p3, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/samsung/scloud/b;->f:Z

    iput-object p5, p0, Lcom/samsung/scloud/b;->g:Lcom/samsung/scloud/c/a;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/m;)Lcom/dropbox/client2/f;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    invoke-static {v0}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/g;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    iget-boolean v0, p0, Lcom/samsung/scloud/b;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/scloud/b;->b()Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    invoke-virtual {v0, p3, p4}, Lcom/dropbox/client2/g;->a(Ljava/io/OutputStream;Lcom/dropbox/client2/m;)V

    iget-object v0, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    invoke-virtual {v0}, Lcom/dropbox/client2/g;->a()Lcom/dropbox/client2/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/scloud/b;)Lcom/samsung/scloud/c/a;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b;->g:Lcom/samsung/scloud/c/a;

    return-object v0
.end method

.method static synthetic b(Lcom/samsung/scloud/b;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 10

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/scloud/b;->f:Z

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_2
    iget-object v5, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    const/4 v0, 0x0

    move v3, v0

    move-object v2, v4

    :goto_0
    const/4 v0, 0x5

    if-ge v3, v0, :cond_5

    iget-boolean v0, p0, Lcom/samsung/scloud/b;->c:Z

    if-nez v0, :cond_3

    new-instance v0, Lcom/samsung/scloud/exception/SCloudCancelException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file download cancelled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudCancelException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v0, p0, Lcom/samsung/scloud/b;->e:Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v6, Lcom/samsung/scloud/c;

    invoke-direct {v6, p0, v5}, Lcom/samsung/scloud/c;-><init>(Lcom/samsung/scloud/b;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2, v1, v6}, Lcom/samsung/scloud/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/m;)Lcom/dropbox/client2/f;

    iget-object v0, p0, Lcom/samsung/scloud/b;->g:Lcom/samsung/scloud/c/a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/scloud/b;->g:Lcom/samsung/scloud/c/a;

    iget-object v2, p0, Lcom/samsung/scloud/b;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x2

    const-wide/16 v8, 0x64

    invoke-interface {v0, v2, v6, v8, v9}, Lcom/samsung/scloud/c/a;->a(Ljava/lang/String;IJ)V

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    return-void

    :catch_0
    move-exception v0

    :goto_1
    iput-object v4, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    if-eqz v1, :cond_6

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_6
    iget-object v2, p0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    invoke-static {v2, v0}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    if-ge v3, v2, :cond_7

    iget-object v0, p0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    invoke-static {v0, v3}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;I)V

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object v2, v1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v1, "file close failed"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v2, p0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    invoke-static {v2, v0}, Lcom/samsung/scloud/a;->b(Lcom/samsung/scloud/a;Ljava/lang/Exception;)V

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public b()Z
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/scloud/b;->c:Z

    iget-object v1, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/b;->b:Lcom/dropbox/client2/g;

    invoke-virtual {v1}, Lcom/dropbox/client2/g;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
