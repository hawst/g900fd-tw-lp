.class Lcom/samsung/scloud/c;
.super Lcom/dropbox/client2/m;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/samsung/scloud/b;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/b;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/c;->b:Lcom/samsung/scloud/b;

    iput-object p2, p0, Lcom/samsung/scloud/c;->a:Ljava/lang/String;

    invoke-direct {p0}, Lcom/dropbox/client2/m;-><init>()V

    return-void
.end method


# virtual methods
.method public a(JJ)V
    .locals 7

    const-wide/16 v4, 0x64

    iget-object v0, p0, Lcom/samsung/scloud/c;->b:Lcom/samsung/scloud/b;

    iget-object v0, v0, Lcom/samsung/scloud/b;->a:Lcom/samsung/scloud/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-long v2, p1, v4

    div-long/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/scloud/a;->a(Lcom/samsung/scloud/a;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/scloud/c;->b:Lcom/samsung/scloud/b;

    invoke-static {v0}, Lcom/samsung/scloud/b;->a(Lcom/samsung/scloud/b;)Lcom/samsung/scloud/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/c;->b:Lcom/samsung/scloud/b;

    invoke-static {v0}, Lcom/samsung/scloud/b;->a(Lcom/samsung/scloud/b;)Lcom/samsung/scloud/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/c;->b:Lcom/samsung/scloud/b;

    invoke-static {v1}, Lcom/samsung/scloud/b;->b(Lcom/samsung/scloud/b;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    mul-long/2addr v4, p1

    div-long/2addr v4, p3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/samsung/scloud/c/a;->a(Ljava/lang/String;IJ)V

    :cond_0
    return-void
.end method
