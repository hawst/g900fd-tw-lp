.class public abstract Lcom/samsung/scloud/b/f;
.super Ljava/lang/Object;


# static fields
.field public static l:Ljava/lang/String;

.field public static m:Ljava/lang/String;

.field public static n:Ljava/lang/String;

.field public static o:Ljava/lang/String;

.field public static p:Ljava/lang/String;

.field public static q:Ljava/lang/String;

.field public static r:Ljava/lang/String;


# instance fields
.field protected A:Z

.field protected B:Z

.field protected C:J

.field protected D:Ljava/lang/String;

.field protected E:Ljava/lang/String;

.field protected F:J

.field protected G:Ljava/util/List;

.field protected H:Ljava/lang/String;

.field protected I:Ljava/lang/String;

.field protected J:Ljava/lang/String;

.field protected K:Ljava/util/List;

.field protected L:Ljava/lang/String;

.field protected M:Ljava/lang/String;

.field protected N:Ljava/lang/String;

.field protected O:Ljava/lang/String;

.field protected P:Ljava/lang/String;

.field protected Q:Ljava/lang/String;

.field protected R:Z

.field private a:Ljava/lang/String;

.field protected s:Ljava/lang/String;

.field protected t:Ljava/lang/String;

.field protected u:J

.field protected v:J

.field protected w:J

.field protected x:Ljava/lang/String;

.field protected y:Z

.field protected z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "folder"

    sput-object v0, Lcom/samsung/scloud/b/f;->l:Ljava/lang/String;

    const-string v0, "file"

    sput-object v0, Lcom/samsung/scloud/b/f;->m:Ljava/lang/String;

    const-string v0, "googleapp"

    sput-object v0, Lcom/samsung/scloud/b/f;->n:Ljava/lang/String;

    const-string v0, "unknown"

    sput-object v0, Lcom/samsung/scloud/b/f;->o:Ljava/lang/String;

    const-string v0, "owner"

    sput-object v0, Lcom/samsung/scloud/b/f;->p:Ljava/lang/String;

    const-string v0, "reader"

    sput-object v0, Lcom/samsung/scloud/b/f;->q:Ljava/lang/String;

    const-string v0, "writer"

    sput-object v0, Lcom/samsung/scloud/b/f;->r:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->s:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->t:Ljava/lang/String;

    iput-wide v4, p0, Lcom/samsung/scloud/b/f;->u:J

    iput-wide v4, p0, Lcom/samsung/scloud/b/f;->v:J

    iput-wide v4, p0, Lcom/samsung/scloud/b/f;->w:J

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->x:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/samsung/scloud/b/f;->y:Z

    iput-boolean v2, p0, Lcom/samsung/scloud/b/f;->z:Z

    iput-boolean v2, p0, Lcom/samsung/scloud/b/f;->A:Z

    iput-boolean v2, p0, Lcom/samsung/scloud/b/f;->B:Z

    iput-wide v4, p0, Lcom/samsung/scloud/b/f;->C:J

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->E:Ljava/lang/String;

    iput-wide v4, p0, Lcom/samsung/scloud/b/f;->F:J

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->G:Ljava/util/List;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->H:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->I:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->J:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->K:Ljava/util/List;

    sget-object v0, Lcom/samsung/scloud/b/f;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/b/f;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->L:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->M:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->N:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->O:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->P:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/f;->Q:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/samsung/scloud/b/f;->R:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->G:Ljava/util/List;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/scloud/b/f;->R:Z

    return-void
.end method

.method public b(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/f;->F:J

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->K:Ljava/util/List;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/scloud/b/f;->z:Z

    return-void
.end method

.method public c(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/f;->w:J

    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/scloud/b/f;->A:Z

    return-void
.end method

.method public d(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/f;->u:J

    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/scloud/b/f;->y:Z

    return-void
.end method

.method public e(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/f;->v:J

    return-void
.end method

.method public e()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/samsung/scloud/b/f;->R:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/scloud/b/f;->i()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->E:Ljava/lang/String;

    return-object v0
.end method

.method public f(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/f;->C:J

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->J:Ljava/lang/String;

    return-void
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/scloud/b/f;->z:Z

    return v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->K:Ljava/util/List;

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->E:Ljava/lang/String;

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->P:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->I:Ljava/lang/String;

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->s:Ljava/lang/String;

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->O:Ljava/lang/String;

    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->t:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/b/f;->L:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/scloud/b/f;->L:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->P:Ljava/lang/String;

    return-void
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/f;->u:J

    return-wide v0
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->H:Ljava/lang/String;

    return-void
.end method

.method public m()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/f;->v:J

    return-wide v0
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->s:Ljava/lang/String;

    return-void
.end method

.method public n(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->t:Ljava/lang/String;

    return-void
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/scloud/b/f;->y:Z

    return v0
.end method

.method public o()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/f;->C:J

    return-wide v0
.end method

.method public o(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->x:Ljava/lang/String;

    return-void
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/f;->L:Ljava/lang/String;

    return-object v0
.end method

.method public p(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->L:Ljava/lang/String;

    return-void
.end method

.method public q(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/f;->a:Ljava/lang/String;

    return-void
.end method
