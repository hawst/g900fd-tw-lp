.class public final Lcom/samsung/scloud/b/j;
.super Ljava/lang/Object;


# instance fields
.field protected a:J

.field protected b:Ljava/lang/String;

.field protected c:I

.field protected d:I

.field protected e:J

.field protected f:J

.field protected g:J

.field protected h:Ljava/lang/String;

.field protected i:Ljava/lang/String;

.field protected j:Ljava/lang/String;

.field protected k:J

.field protected l:J

.field protected m:J

.field protected n:J

.field protected o:J

.field protected p:J

.field protected q:J

.field protected r:J

.field protected s:J

.field protected t:J

.field protected u:Ljava/lang/String;

.field protected v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/scloud/b/j;->b:Ljava/lang/String;

    iput v4, p0, Lcom/samsung/scloud/b/j;->c:I

    iput v4, p0, Lcom/samsung/scloud/b/j;->d:I

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->e:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->f:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->g:J

    iput-object v1, p0, Lcom/samsung/scloud/b/j;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/j;->i:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/j;->j:Ljava/lang/String;

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->k:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->l:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->m:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->n:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->o:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->p:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->q:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->r:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->s:J

    iput-wide v2, p0, Lcom/samsung/scloud/b/j;->t:J

    iput-object v1, p0, Lcom/samsung/scloud/b/j;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/scloud/b/j;->v:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/j;->u:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->k:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/j;->v:Ljava/lang/String;

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/j;->k:J

    return-wide v0
.end method

.method public b(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->l:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/j;->u:Ljava/lang/String;

    return-void
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/j;->l:J

    return-wide v0
.end method

.method public c(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->m:J

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/j;->h:Ljava/lang/String;

    return-void
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/j;->n:J

    return-wide v0
.end method

.method public d(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->n:J

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/j;->i:Ljava/lang/String;

    return-void
.end method

.method public e(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->o:J

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/b/j;->j:Ljava/lang/String;

    return-void
.end method

.method public f(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->p:J

    return-void
.end method

.method public g(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->q:J

    return-void
.end method

.method public h(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->r:J

    return-void
.end method

.method public i(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->s:J

    return-void
.end method

.method public j(J)V
    .locals 1

    iput-wide p1, p0, Lcom/samsung/scloud/b/j;->a:J

    return-void
.end method
