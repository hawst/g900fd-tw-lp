.class public Lcom/samsung/scloud/b/c;
.super Ljava/lang/Object;


# instance fields
.field protected a:Ljava/util/List;

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/scloud/b/c;->b:J

    iput v3, p0, Lcom/samsung/scloud/b/c;->c:I

    iput v3, p0, Lcom/samsung/scloud/b/c;->d:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/scloud/b/c;->e:I

    iput-object v2, p0, Lcom/samsung/scloud/b/c;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/scloud/b/c;->g:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/scloud/b/c;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/scloud/b/c;->b:J

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/scloud/b/c;->c:I

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/samsung/scloud/b/c;->c:I

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/scloud/b/c;->d:I

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/samsung/scloud/b/c;->d:I

    return v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/scloud/b/c;->e:I

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/samsung/scloud/b/c;->e:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/b/c;->g:Ljava/lang/String;

    return-object v0
.end method
