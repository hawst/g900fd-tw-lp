.class public Lcom/samsung/scloud/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/scloud/j;


# static fields
.field private static e:Lcom/samsung/scloud/a;


# instance fields
.field private a:Lcom/dropbox/client2/SamsungDropboxAPI;

.field private b:Lcom/dropbox/client2/session/AccessTokenPair;

.field private c:Landroid/content/Context;

.field private d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    iput-object v0, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    iput-object v0, p0, Lcom/samsung/scloud/a;->c:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/a;->d:Z

    iput-object p1, p0, Lcom/samsung/scloud/a;->c:Landroid/content/Context;

    new-instance v0, Lcom/dropbox/client2/session/AppKeyPair;

    sget-object v1, Lcom/samsung/scloud/a/a/a;->a:Ljava/lang/String;

    sget-object v2, Lcom/samsung/scloud/a/a/a;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/dropbox/client2/session/AppKeyPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/dropbox/client2/android/a;

    sget-object v2, Lcom/dropbox/client2/session/Session$AccessType;->DROPBOX:Lcom/dropbox/client2/session/Session$AccessType;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/client2/android/a;-><init>(Lcom/dropbox/client2/session/AppKeyPair;Lcom/dropbox/client2/session/Session$AccessType;)V

    new-instance v0, Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-direct {v0, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;-><init>(Lcom/dropbox/client2/session/Session;)V

    iput-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    return-void
.end method

.method private a(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_1024x768:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_1
    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_320x240:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_480x320:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_640x480:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->BESTFIT_960x640:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    if-ne p1, v0, :cond_6

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_256x256:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_6
    const/4 v0, 0x7

    if-ne p1, v0, :cond_7

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_128x128:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_7
    const/16 v0, 0x8

    if-ne p1, v0, :cond_8

    sget-object v0, Lcom/dropbox/client2/DropboxAPI$ThumbSize;->ICON_64x64:Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    goto :goto_0

    :cond_8
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v1, "wrong thumbnail size"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/samsung/scloud/a;)Lcom/dropbox/client2/SamsungDropboxAPI;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/y;
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v5

    check-cast v5, Lcom/dropbox/client2/session/a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/samsung_thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Lcom/dropbox/client2/session/a;->e()Lcom/dropbox/client2/session/Session$AccessType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    aput-object p2, v4, v3

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p3}, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-virtual {v5}, Lcom/dropbox/client2/session/a;->f()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    sget-object v0, Lcom/dropbox/client2/RESTUtility$RequestMethod;->GET:Lcom/dropbox/client2/RESTUtility$RequestMethod;

    invoke-virtual {v5}, Lcom/dropbox/client2/session/a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static/range {v0 .. v5}, Lcom/dropbox/client2/RESTUtility;->b(Lcom/dropbox/client2/RESTUtility$RequestMethod;Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/String;Lcom/dropbox/client2/session/Session;)Lcom/dropbox/client2/k;

    move-result-object v0

    new-instance v1, Lcom/dropbox/client2/y;

    iget-object v2, v0, Lcom/dropbox/client2/k;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v0, v0, Lcom/dropbox/client2/k;->b:Lorg/apache/http/HttpResponse;

    invoke-direct {v1, v2, v0}, Lcom/dropbox/client2/y;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/samsung/scloud/a;
    .locals 2

    const-class v1, Lcom/samsung/scloud/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/scloud/a;->e:Lcom/samsung/scloud/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/scloud/a;->e:Lcom/samsung/scloud/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/samsung/scloud/a;

    invoke-direct {v0, p0}, Lcom/samsung/scloud/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/scloud/a;->e:Lcom/samsung/scloud/a;

    sget-object v0, Lcom/samsung/scloud/a;->e:Lcom/samsung/scloud/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/b/e;
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/samsung/scloud/b/e;

    invoke-direct {v1}, Lcom/samsung/scloud/b/e;-><init>()V

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/e;->n(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/e;->m(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->count:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/e;->a(J)V

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->modified:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/e;->e(J)V

    iget-object v0, p1, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;->cover:Lcom/dropbox/client2/i;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/e;->a(Lcom/samsung/scloud/b/d;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;
    .locals 4

    iget-boolean v0, p1, Lcom/dropbox/client2/i;->d:Z

    if-eqz v0, :cond_0

    new-instance v1, Lcom/samsung/scloud/b/e;

    invoke-direct {v1}, Lcom/samsung/scloud/b/e;-><init>()V

    sget-object v0, Lcom/samsung/scloud/b/f;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p1, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->m(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/dropbox/client2/i;->a:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->f(J)V

    iget-boolean v0, p1, Lcom/dropbox/client2/i;->m:Z

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->d(Z)V

    iget-object v0, p1, Lcom/dropbox/client2/i;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->e(J)V

    iget-object v0, p1, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->p(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->n(Ljava/lang/String;)V

    iget-boolean v0, p1, Lcom/dropbox/client2/i;->l:Z

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->a(Z)V

    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V

    sget-object v0, Lcom/samsung/scloud/b/f;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/i;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->e(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->c(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/i;->j:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->d(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/i;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->d(J)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/i;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/dropbox/client2/s;)Lcom/samsung/scloud/b/f;
    .locals 6

    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/scloud/b/f;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->q(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->c(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->j:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->d(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->e(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v0, v0, Lcom/dropbox/client2/i;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->d(J)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/d;->d(J)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->a(Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    iget-object v2, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v2, v2, Lcom/dropbox/client2/i;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->b(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v0, v0, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->m(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-wide v2, v0, Lcom/dropbox/client2/i;->a:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->f(J)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-boolean v0, v0, Lcom/dropbox/client2/i;->m:Z

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->d(Z)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v0, v0, Lcom/dropbox/client2/i;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/client2/RESTUtility;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/scloud/b/f;->e(J)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v0, v0, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->p(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-object v0, v0, Lcom/dropbox/client2/i;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->n(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/dropbox/client2/s;->a:Lcom/dropbox/client2/i;

    iget-boolean v0, v0, Lcom/dropbox/client2/i;->l:Z

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->a(Z)V

    :cond_0
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    if-eqz v0, :cond_7

    new-instance v2, Lcom/samsung/scloud/b/a;

    invoke-direct {v2}, Lcom/samsung/scloud/b/a;-><init>()V

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/b/a;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/b/a;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/b/a;->b(J)V

    :cond_3
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/b/a;->d(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/samsung/scloud/b/a;->c(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/dropbox/client2/s;->b:Lcom/dropbox/client2/u;

    iget-object v0, v0, Lcom/dropbox/client2/u;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/b/a;->a(J)V

    :cond_6
    move-object v0, v1

    check-cast v0, Lcom/samsung/scloud/b/d;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/d;->a(Lcom/samsung/scloud/b/a;)V

    :cond_7
    return-object v1
.end method

.method static synthetic a(Lcom/samsung/scloud/a;Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/scloud/a;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/a;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/scloud/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/a;->i(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/scloud/a;Ljava/lang/Exception;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Exception;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/dropbox/client2/exception/DropboxServerException;

    iget v2, p1, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :cond_1
    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxSSLException;

    if-nez v2, :cond_0

    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    if-nez v2, :cond_0

    instance-of v2, p1, Lcom/dropbox/client2/exception/DropboxIOException;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private b(I)V
    .locals 4

    if-ltz p1, :cond_0

    const/4 v0, 0x5

    if-le p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v0, 0x3e8

    const/4 v2, 0x1

    rem-int/lit8 v3, p1, 0x5

    shl-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    mul-long/2addr v0, v2

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doBackOff ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] sleep(ms)="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->i(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/scloud/a;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    return-void
.end method

.method private b(Ljava/lang/Exception;)V
    .locals 4

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDropboxException of [callerMethodName] - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->i(Ljava/lang/String;)V

    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxUnlinkedException;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>()V

    throw v0

    :cond_0
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/dropbox/client2/exception/DropboxServerException;

    iget v0, v0, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "server file not exists["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1
    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_2
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_3
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_4
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_5
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxIOException;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxFileSizeException;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxLocalStorageFullException;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxParseException;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxSSLException;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    instance-of v0, p1, Lcom/dropbox/client2/exception/DropboxPartialFileException;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_1
        0x194 -> :sswitch_0
        0x1f4 -> :sswitch_3
        0x1f6 -> :sswitch_4
        0x1f7 -> :sswitch_5
        0x1fb -> :sswitch_2
    .end sparse-switch
.end method

.method private c(I)I
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    const/16 v0, 0xb4

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    if-ne p1, v1, :cond_3

    const/16 v0, 0x5a

    goto :goto_0

    :cond_3
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    const/16 v0, 0x10e

    goto :goto_0
.end method

.method private f()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    const-class v0, Landroid/os/Debug;

    const-string v3, "isProductShip"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    move v2, v0

    :goto_1
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private i(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/scloud/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCLOUD_SDK"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private j(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private k(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/c/a;)Lcom/samsung/scloud/b/d;
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-ne p4, v1, :cond_4

    sget-object v1, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->PNG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    move-object v3, v1

    :goto_0
    invoke-direct {p0, p3}, Lcom/samsung/scloud/a;->a(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    move-result-object v6

    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    const/4 v1, 0x0

    invoke-direct {v7, p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    move v5, v2

    move-object v2, v4

    :goto_1
    const/4 v1, 0x5

    if-ge v5, v1, :cond_b

    :try_start_1
    new-instance v4, Lcom/samsung/scloud/f;

    invoke-direct {v4, p5, v8}, Lcom/samsung/scloud/f;-><init>(Lcom/samsung/scloud/c/a;Ljava/lang/String;)V

    if-eqz v6, :cond_6

    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1, p2, v6, v3}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/y;

    move-result-object v1

    :goto_2
    invoke-virtual {v1, v7, v4}, Lcom/dropbox/client2/y;->a(Ljava/io/OutputStream;Lcom/dropbox/client2/m;)V

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v4

    if-eqz v4, :cond_c

    new-instance v4, Lcom/samsung/scloud/b/c;

    invoke-direct {v4}, Lcom/samsung/scloud/b/c;-><init>()V

    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v9

    iget-object v9, v9, Lcom/dropbox/client2/w;->b:Ljava/lang/Integer;

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v9

    iget-object v9, v9, Lcom/dropbox/client2/w;->b:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v4, v9}, Lcom/samsung/scloud/b/c;->a(I)V

    :cond_0
    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v9

    iget-object v9, v9, Lcom/dropbox/client2/w;->c:Ljava/lang/Integer;

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v9

    iget-object v9, v9, Lcom/dropbox/client2/w;->c:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v4, v9}, Lcom/samsung/scloud/b/c;->b(I)V

    :cond_1
    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v9

    iget-object v9, v9, Lcom/dropbox/client2/w;->d:Ljava/lang/Integer;

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Lcom/dropbox/client2/y;->b()Lcom/dropbox/client2/w;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/client2/w;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->c(I)I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/samsung/scloud/b/c;->c(I)V

    :cond_2
    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1, v4}, Lcom/samsung/scloud/b/d;->a(Lcom/samsung/scloud/b/c;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_3
    if-eqz v7, :cond_3

    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_4
    return-object v1

    :cond_4
    if-nez p4, :cond_5

    sget-object v1, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->JPEG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    move-object v3, v1

    goto/16 :goto_0

    :cond_5
    new-instance v1, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v2, "wrong thumbnail type"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v1

    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    :try_start_4
    const-string v1, "2048x1536_bestfit"

    invoke-direct {p0, p2, v1, v3}, Lcom/samsung/scloud/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/y;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    goto/16 :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :catch_2
    move-exception v1

    move-object v4, v2

    move-object v2, v1

    :goto_5
    :try_start_5
    instance-of v1, v2, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v1, :cond_8

    move-object v0, v2

    check-cast v0, Lcom/dropbox/client2/exception/DropboxServerException;

    move-object v1, v0

    iget v1, v1, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v9, 0x19f

    if-ne v1, v9, :cond_8

    new-instance v1, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v2, "no thumbnail"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v1

    if-eqz v7, :cond_7

    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_7
    :goto_6
    throw v1

    :cond_8
    :try_start_7
    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x4

    if-ge v5, v1, :cond_a

    invoke-direct {p0, v5}, Lcom/samsung/scloud/a;->b(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v7, :cond_9

    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_9
    :goto_7
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v2, v4

    goto/16 :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :cond_a
    :try_start_9
    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v7, :cond_9

    :try_start_a
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_7

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    :cond_b
    move-object v1, v2

    goto :goto_4

    :catch_6
    move-exception v2

    move-object v4, v1

    goto :goto_5

    :cond_c
    move-object v1, v2

    goto/16 :goto_3
.end method

.method public a(Ljava/lang/String;)Lcom/samsung/scloud/b/d;
    .locals 8

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    move v7, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v7, v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Lcom/dropbox/client2/i;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/b/d;

    :goto_2
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    if-ge v7, v1, :cond_2

    invoke-direct {p0, v7}, Lcom/samsung/scloud/a;->b(I)V

    :goto_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_3

    :cond_3
    move-object v0, v6

    goto :goto_2

    :cond_4
    move-object v0, v6

    goto :goto_1
.end method

.method public a(ILjava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->d()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_3

    new-instance v1, Lcom/samsung/scloud/b/g;

    invoke-direct {v1}, Lcom/samsung/scloud/b/g;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/SamsungDropboxAPI$AlbumMetadata;)Lcom/samsung/scloud/b/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/samsung/scloud/b/g;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_3

    :cond_2
    move-object v0, v1

    :cond_3
    return-object v0

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/b/h;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->b(Ljava/lang/String;)Lcom/dropbox/client2/h;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_2

    new-instance v0, Lcom/samsung/scloud/b/h;

    invoke-direct {v0}, Lcom/samsung/scloud/b/h;-><init>()V

    iget-object v2, v1, Lcom/dropbox/client2/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/h;->a(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/dropbox/client2/h;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/h;->a(J)V

    :cond_2
    return-object v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    if-ge v2, v3, :cond_3

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/b/i;
    .locals 5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    const/4 v1, 0x0

    new-instance v3, Lcom/samsung/scloud/b/i;

    invoke-direct {v3}, Lcom/samsung/scloud/b/i;-><init>()V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/r;
    :try_end_0
    .catch Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/dropbox/client2/r;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/samsung/scloud/b/i;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/dropbox/client2/r;->b:Ljava/util/Date;

    invoke-virtual {v3, v0}, Lcom/samsung/scloud/b/i;->a(Ljava/util/Date;)V

    :cond_2
    return-object v3

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v4, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI$UnableToTranscodeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    if-ge v2, v4, :cond_3

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public a()Lcom/samsung/scloud/b/j;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->b()Lcom/dropbox/client2/DropboxAPI$Account;
    :try_end_0
    .catch Lcom/dropbox/client2/exception/DropboxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    if-nez v1, :cond_1

    :goto_2
    return-object v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_3

    :cond_1
    new-instance v0, Lcom/samsung/scloud/b/j;

    invoke-direct {v0}, Lcom/samsung/scloud/b/j;-><init>()V

    iget-object v2, v1, Lcom/dropbox/client2/DropboxAPI$Account;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/scloud/b/j;->d(Ljava/lang/String;)V

    iget-wide v2, v1, Lcom/dropbox/client2/DropboxAPI$Account;->quota:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/j;->a(J)V

    iget-wide v2, v1, Lcom/dropbox/client2/DropboxAPI$Account;->quotaNormal:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/j;->b(J)V

    iget-wide v2, v1, Lcom/dropbox/client2/DropboxAPI$Account;->quotaShared:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/j;->d(J)V

    iget-wide v2, v1, Lcom/dropbox/client2/DropboxAPI$Account;->uid:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/scloud/b/j;->j(J)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1, p2, p3}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_2
    const/4 v2, 0x0

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v3, v0, :cond_3

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    return-void

    :catch_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    if-ge v3, v2, :cond_5

    invoke-direct {p0, v3}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object v2, v1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v1, "file close failed"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v1, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Lcom/dropbox/client2/SamsungDropboxAPI$Hook;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    return v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public b(ILjava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 1

    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    iget-object v1, v1, Lcom/dropbox/client2/session/AccessTokenPair;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    iget-object v1, v1, Lcom/dropbox/client2/session/AccessTokenPair;->secret:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/a;

    invoke-virtual {v0, p1}, Lcom/dropbox/client2/android/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v0, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    move v3, v1

    :goto_0
    const/4 v2, 0x5

    if-ge v3, v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v2, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return v0

    :catch_0
    move-exception v2

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x4

    if-ge v3, v4, :cond_2

    invoke-direct {p0, v3}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 6

    const/4 v1, 0x0

    new-instance v3, Lcom/samsung/scloud/b/g;

    invoke-direct {v3}, Lcom/samsung/scloud/b/g;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->d(Ljava/lang/String;)Lcom/dropbox/client2/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/dropbox/client2/e;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->a(Ljava/lang/String;)V

    iget-boolean v2, v0, Lcom/dropbox/client2/e;->d:Z

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->a(Z)V

    iget-boolean v2, v0, Lcom/dropbox/client2/e;->a:Z

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->b(Z)V

    iget-object v0, v0, Lcom/dropbox/client2/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/c;

    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    if-nez v5, :cond_1

    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V

    iget-object v5, v0, Lcom/dropbox/client2/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/b/f;->m(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/dropbox/client2/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->p(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->d(Z)V

    move-object v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x4

    if-ge v2, v5, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_4

    :cond_1
    iget-object v5, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    if-eqz v5, :cond_4

    iget-object v0, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/client2/s;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/s;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    goto :goto_3

    :cond_2
    invoke-virtual {v3, v4}, Lcom/samsung/scloud/b/g;->a(Ljava/util/List;)V

    :cond_3
    return-object v3

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/a;

    invoke-virtual {v0}, Lcom/dropbox/client2/android/a;->b()Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/session/a;

    invoke-virtual {v0}, Lcom/dropbox/client2/session/a;->d()Lcom/dropbox/client2/session/AccessTokenPair;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    return-void
.end method

.method public d(Ljava/lang/String;)Lcom/samsung/scloud/b/g;
    .locals 6

    const/4 v1, 0x0

    new-instance v3, Lcom/samsung/scloud/b/g;

    invoke-direct {v3}, Lcom/samsung/scloud/b/g;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->c(Ljava/lang/String;)Lcom/dropbox/client2/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/dropbox/client2/e;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->a(Ljava/lang/String;)V

    iget-boolean v2, v0, Lcom/dropbox/client2/e;->d:Z

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->a(Z)V

    iget-boolean v2, v0, Lcom/dropbox/client2/e;->a:Z

    invoke-virtual {v3, v2}, Lcom/samsung/scloud/b/g;->b(Z)V

    iget-object v0, v0, Lcom/dropbox/client2/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/c;

    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    if-nez v5, :cond_1

    new-instance v1, Lcom/samsung/scloud/b/d;

    invoke-direct {v1}, Lcom/samsung/scloud/b/d;-><init>()V

    iget-object v5, v0, Lcom/dropbox/client2/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/b/f;->m(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/dropbox/client2/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->p(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/b/f;->d(Z)V

    move-object v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x4

    if-ge v2, v5, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_4

    :cond_1
    iget-object v5, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    if-eqz v5, :cond_4

    iget-object v0, v0, Lcom/dropbox/client2/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/dropbox/client2/i;

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Lcom/dropbox/client2/i;)Lcom/samsung/scloud/b/f;

    move-result-object v0

    goto :goto_3

    :cond_2
    invoke-virtual {v3, v4}, Lcom/samsung/scloud/b/g;->a(Ljava/util/List;)V

    :cond_3
    return-object v3

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/android/a;

    invoke-virtual {v0}, Lcom/dropbox/client2/android/a;->a()Z

    move-result v0

    return v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    :goto_0
    const/4 v1, 0x5

    if-ge v2, v1, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Z)Lcom/dropbox/client2/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_2

    iget-object v0, v1, Lcom/dropbox/client2/h;->a:Ljava/lang/String;

    :cond_2
    return-object v0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    if-ge v2, v3, :cond_3

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    const-string v2, "Loading Access TOKEN_KEY"

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->i(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    new-instance v2, Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-direct {v2, v1, v0}, Lcom/dropbox/client2/session/AccessTokenPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0}, Lcom/dropbox/client2/SamsungDropboxAPI;->a()Lcom/dropbox/client2/session/Session;

    move-result-object v0

    check-cast v0, Lcom/dropbox/client2/session/a;

    iget-object v1, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-virtual {v0, v1}, Lcom/dropbox/client2/session/a;->a(Lcom/dropbox/client2/session/AccessTokenPair;)V

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/scloud/a;->d:Z

    if-nez v2, :cond_2

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    new-instance v2, Lcom/dropbox/client2/session/AccessTokenPair;

    invoke-direct {v2, v1, v0}, Lcom/dropbox/client2/session/AccessTokenPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/scloud/a;->b:Lcom/dropbox/client2/session/AccessTokenPair;

    goto :goto_0

    :cond_2
    const-string v0, "Token is null !"

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v1, v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    invoke-virtual {v0, p1}, Lcom/dropbox/client2/SamsungDropboxAPI;->f(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h(Ljava/lang/String;)Lcom/samsung/scloud/b/i;
    .locals 5

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    :cond_1
    const/4 v1, 0x0

    new-instance v3, Lcom/samsung/scloud/b/i;

    invoke-direct {v3}, Lcom/samsung/scloud/b/i;-><init>()V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v2, v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/a;->a:Lcom/dropbox/client2/SamsungDropboxAPI;

    const/4 v4, 0x1

    invoke-virtual {v0, p1, v4}, Lcom/dropbox/client2/SamsungDropboxAPI;->a(Ljava/lang/String;Z)Lcom/dropbox/client2/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/dropbox/client2/h;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/samsung/scloud/b/i;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/dropbox/client2/h;->b:Ljava/util/Date;

    invoke-virtual {v3, v0}, Lcom/samsung/scloud/b/i;->a(Ljava/util/Date;)V

    :cond_2
    return-object v3

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->a(Ljava/lang/Exception;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    if-ge v2, v4, :cond_3

    invoke-direct {p0, v2}, Lcom/samsung/scloud/a;->b(I)V

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/scloud/a;->b(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
