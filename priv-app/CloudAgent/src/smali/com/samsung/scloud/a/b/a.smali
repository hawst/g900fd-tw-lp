.class public Lcom/samsung/scloud/a/b/a;
.super Lcom/google/api/client/googleapis/auth/a/a;


# instance fields
.field b:Lcom/google/api/client/http/f;


# direct methods
.method protected constructor <init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/j;)V
    .locals 4

    invoke-direct/range {p0 .. p12}, Lcom/google/api/client/googleapis/auth/a/a;-><init>(Lcom/google/api/client/auth/oauth2/d;Lcom/google/api/client/http/v;Lcom/google/api/client/json/d;Ljava/lang/String;Lcom/google/api/client/http/k;Lcom/google/api/client/http/s;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/j;)V

    invoke-static {}, Lcom/google/api/client/http/f;->d()Lcom/google/api/client/http/g;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/g;->a(I)Lcom/google/api/client/http/g;

    move-result-object v0

    const v1, 0x2bf20

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/g;->c(I)Lcom/google/api/client/http/g;

    move-result-object v0

    const/16 v1, 0x1b58

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/g;->b(I)Lcom/google/api/client/http/g;

    move-result-object v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/http/g;->b(D)Lcom/google/api/client/http/g;

    move-result-object v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/http/g;->a(D)Lcom/google/api/client/http/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/g;->a()Lcom/google/api/client/http/f;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/a/b/a;->b:Lcom/google/api/client/http/f;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/api/client/http/p;Lcom/google/api/client/http/t;Z)Z
    .locals 3

    if-nez p2, :cond_0

    const-string v0, "GoogleDrive"

    const-string v1, "[GoogleCredentialEx]handleResponse(UnsuccessfulResponseHandler) response=null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "GoogleDrive"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GoogleCredentialEx]handleResponse(UnsuccessfulResponseHandler) response code="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/api/client/http/t;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " headers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/api/client/http/t;->b()Lcom/google/api/client/http/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/api/client/http/l;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Lcom/google/api/client/googleapis/auth/a/a;->a(Lcom/google/api/client/http/p;Lcom/google/api/client/http/t;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lcom/google/api/client/http/p;)V
    .locals 2

    const v1, 0xea60

    invoke-super {p0, p1}, Lcom/google/api/client/googleapis/auth/a/a;->b(Lcom/google/api/client/http/p;)V

    iget-object v0, p0, Lcom/samsung/scloud/a/b/a;->b:Lcom/google/api/client/http/f;

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/p;->a(Lcom/google/api/client/http/c;)Lcom/google/api/client/http/p;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/p;->c(I)Lcom/google/api/client/http/p;

    invoke-virtual {p1, v1}, Lcom/google/api/client/http/p;->a(I)Lcom/google/api/client/http/p;

    invoke-virtual {p1, v1}, Lcom/google/api/client/http/p;->b(I)Lcom/google/api/client/http/p;

    const-string v0, "GoogleDrive"

    const-string v1, "[GoogleCredentialEx]Request initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
