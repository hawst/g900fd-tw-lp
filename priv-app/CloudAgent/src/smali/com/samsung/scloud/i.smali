.class Lcom/samsung/scloud/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/samsung/scloud/g;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/g;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/scloud/g;->j:Z

    const-string v0, "SCLOUD_SDK"

    const-string v1, "Account info changed ..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    invoke-static {v0}, Lcom/samsung/scloud/g;->a(Lcom/samsung/scloud/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_1
    return-void

    :cond_1
    const-string v1, "authtoken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/scloud/g;->j:Z

    iget-object v1, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/scloud/g;->f(Ljava/lang/String;)V

    const-string v0, "SCLOUD_SDK"

    const-string v1, "after retry get access token ..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SCLOUD_SDK"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/samsung/scloud/i;->a:Lcom/samsung/scloud/g;

    invoke-static {v0}, Lcom/samsung/scloud/g;->a(Lcom/samsung/scloud/g;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_1
.end method
