.class public Lcom/samsung/scloud/exception/SCloudIOException;
.super Lcom/samsung/scloud/exception/SCloudException;


# static fields
.field public static final FILE_CLOSE_FAILED:Ljava/lang/String; = "file close failed"

.field public static final NO_THUMBNAIL:Ljava/lang/String; = "no thumbnail"

.field public static final SERVER_FILE_NOT_EXISTS:Ljava/lang/String; = "server file not exists"

.field public static final SERVER_FOLDER_ALREADY_EXISTS:Ljava/lang/String; = "server folder already exists"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getExceptionCause()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
