.class public Lcom/samsung/scloud/exception/SCloudCancelException;
.super Lcom/samsung/scloud/exception/SCloudException;


# static fields
.field public static final FILE_DOWNLOAD_CANCELLED:Ljava/lang/String; = "file download cancelled"

.field public static final FILE_UPLOAD_CANCELLED:Ljava/lang/String; = "file upload cancelled"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    return-void
.end method
