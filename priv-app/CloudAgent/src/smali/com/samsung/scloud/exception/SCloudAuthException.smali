.class public Lcom/samsung/scloud/exception/SCloudAuthException;
.super Lcom/samsung/scloud/exception/SCloudException;


# static fields
.field public static final SERVER_ACCOUNT_INFO_CHANGED:Ljava/lang/String; = "server account info changed"

.field public static final SERVER_TOKEN_EXPIRED:Ljava/lang/String; = "server token expired"

.field public static final SERVER_TOKEN_INVALID:Ljava/lang/String; = "server token invalid"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getExceptionCause()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
