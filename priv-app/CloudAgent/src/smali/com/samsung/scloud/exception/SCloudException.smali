.class public Lcom/samsung/scloud/exception/SCloudException;
.super Ljava/lang/Exception;


# static fields
.field private static final serialVersionUID:J = 0x21bcf0a6afa131d1L


# instance fields
.field cause:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    iput-object p1, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getExceptionCause()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    goto :goto_0
.end method

.method public setExceptionCause(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    goto :goto_0
.end method
