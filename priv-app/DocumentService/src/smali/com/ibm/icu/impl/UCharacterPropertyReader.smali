.class final Lcom/ibm/icu/impl/UCharacterPropertyReader;
.super Ljava/lang/Object;
.source "UCharacterPropertyReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# static fields
.field private static final DATA_FORMAT_ID_:[B

.field private static final DATA_FORMAT_VERSION_:[B

.field private static final INDEX_SIZE_:I = 0x10


# instance fields
.field private m_additionalColumnsCount_:I

.field private m_additionalOffset_:I

.field private m_additionalVectorsOffset_:I

.field private m_caseOffset_:I

.field private m_dataInputStream_:Ljava/io/DataInputStream;

.field private m_exceptionOffset_:I

.field private m_propertyOffset_:I

.field private m_reservedOffset_:I

.field private m_unicodeVersion_:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 152
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_ID_:[B

    .line 158
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_VERSION_:[B

    return-void

    .line 152
    :array_0
    .array-data 1
        0x55t
        0x50t
        0x72t
        0x6ft
    .end array-data

    .line 158
    :array_1
    .array-data 1
        0x5t
        0x0t
        0x5t
        0x2t
    .end array-data
.end method

.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    sget-object v0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_ID_:[B

    invoke-static {p1, v0, p0}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_unicodeVersion_:[B

    .line 53
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    .line 54
    return-void
.end method


# virtual methods
.method public isDataVersionAcceptable([B)Z
    .locals 5
    .param p1, "version"    # [B

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 36
    aget-byte v1, p1, v0

    sget-object v2, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_VERSION_:[B

    aget-byte v2, v2, v0

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v3

    sget-object v2, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_VERSION_:[B

    aget-byte v2, v2, v3

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v4

    sget-object v2, Lcom/ibm/icu/impl/UCharacterPropertyReader;->DATA_FORMAT_VERSION_:[B

    aget-byte v2, v2, v4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected read(Lcom/ibm/icu/impl/UCharacterProperty;)V
    .locals 8
    .param p1, "ucharppty"    # Lcom/ibm/icu/impl/UCharacterProperty;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 67
    const/16 v0, 0x10

    .line 68
    .local v0, "count":I
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_propertyOffset_:I

    .line 69
    add-int/lit8 v0, v0, -0x1

    .line 70
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_exceptionOffset_:I

    .line 71
    add-int/lit8 v0, v0, -0x1

    .line 72
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_caseOffset_:I

    .line 73
    add-int/lit8 v0, v0, -0x1

    .line 74
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalOffset_:I

    .line 75
    add-int/lit8 v0, v0, -0x1

    .line 76
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalVectorsOffset_:I

    .line 77
    add-int/lit8 v0, v0, -0x1

    .line 78
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalColumnsCount_:I

    .line 79
    add-int/lit8 v0, v0, -0x1

    .line 80
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_reservedOffset_:I

    .line 81
    add-int/lit8 v0, v0, -0x1

    .line 82
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 83
    add-int/lit8 v0, v0, -0x3

    .line 84
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_maxBlockScriptValue_:I

    .line 85
    add-int/lit8 v0, v0, -0x1

    .line 86
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iput v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_maxJTGValue_:I

    .line 87
    add-int/lit8 v0, v0, -0x1

    .line 88
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 92
    new-instance v3, Lcom/ibm/icu/impl/CharTrie;

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-direct {v3, v4, v5}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    .line 95
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_exceptionOffset_:I

    iget v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_propertyOffset_:I

    sub-int v2, v3, v4

    .line 96
    .local v2, "size":I
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    mul-int/lit8 v4, v2, 0x4

    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 99
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_caseOffset_:I

    iget v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_exceptionOffset_:I

    sub-int v2, v3, v4

    .line 100
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    mul-int/lit8 v4, v2, 0x4

    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 103
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalOffset_:I

    iget v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_caseOffset_:I

    sub-int/2addr v3, v4

    shl-int/lit8 v2, v3, 0x1

    .line 104
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    mul-int/lit8 v4, v2, 0x2

    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 106
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalColumnsCount_:I

    if-lez v3, :cond_0

    .line 108
    new-instance v3, Lcom/ibm/icu/impl/CharTrie;

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-direct {v3, v4, v5}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalTrie_:Lcom/ibm/icu/impl/CharTrie;

    .line 111
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_reservedOffset_:I

    iget v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalVectorsOffset_:I

    sub-int v2, v3, v4

    .line 112
    new-array v3, v2, [I

    iput-object v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalVectors_:[I

    .line 113
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 114
    iget-object v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalVectors_:[I

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    aput v4, v3, v1

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    .end local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->close()V

    .line 119
    iget v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_additionalColumnsCount_:I

    iput v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalColumnsCount_:I

    .line 120
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_unicodeVersion_:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_unicodeVersion_:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_unicodeVersion_:[B

    const/4 v6, 0x2

    aget-byte v5, v5, v6

    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterPropertyReader;->m_unicodeVersion_:[B

    const/4 v7, 0x3

    aget-byte v6, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v3

    iput-object v3, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_unicodeVersion_:Lcom/ibm/icu/util/VersionInfo;

    .line 123
    return-void
.end method
