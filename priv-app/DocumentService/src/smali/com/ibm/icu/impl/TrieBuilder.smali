.class public Lcom/ibm/icu/impl/TrieBuilder;
.super Ljava/lang/Object;
.source "TrieBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;
    }
.end annotation


# static fields
.field protected static final BMP_INDEX_LENGTH_:I = 0x800

.field public static final DATA_BLOCK_LENGTH:I = 0x20

.field protected static final DATA_GRANULARITY_:I = 0x4

.field protected static final INDEX_SHIFT_:I = 0x2

.field protected static final MASK_:I = 0x1f

.field private static final MAX_BUILD_TIME_DATA_LENGTH_:I = 0x110420

.field protected static final MAX_DATA_LENGTH_:I = 0x40000

.field protected static final MAX_INDEX_LENGTH_:I = 0x8800

.field protected static final OPTIONS_DATA_IS_32_BIT_:I = 0x100

.field protected static final OPTIONS_INDEX_SHIFT_:I = 0x4

.field protected static final OPTIONS_LATIN1_IS_LINEAR_:I = 0x200

.field protected static final SHIFT_:I = 0x5

.field protected static final SURROGATE_BLOCK_COUNT_:I = 0x20


# instance fields
.field protected m_dataCapacity_:I

.field protected m_dataLength_:I

.field protected m_indexLength_:I

.field protected m_index_:[I

.field protected m_isCompacted_:Z

.field protected m_isLatin1Linear_:Z

.field protected m_map_:[I


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x8800

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    .line 176
    const v0, 0x8821

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    .line 177
    iput-boolean v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_isLatin1Linear_:Z

    .line 178
    iput-boolean v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_isCompacted_:Z

    .line 179
    iput v2, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_indexLength_:I

    .line 180
    return-void
.end method

.method protected constructor <init>(Lcom/ibm/icu/impl/TrieBuilder;)V
    .locals 4
    .param p1, "table"    # Lcom/ibm/icu/impl/TrieBuilder;

    .prologue
    const/4 v3, 0x0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    const v0, 0x8800

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    .line 185
    iget v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_indexLength_:I

    iput v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_indexLength_:I

    .line 186
    iget-object v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    iget v2, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_indexLength_:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 187
    iget v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_dataCapacity_:I

    iput v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_dataCapacity_:I

    .line 188
    iget v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_dataLength_:I

    iput v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_dataLength_:I

    .line 189
    iget-object v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    .line 190
    iget-object v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    iget-object v2, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 191
    iget-boolean v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_isLatin1Linear_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_isLatin1Linear_:Z

    .line 192
    iget-boolean v0, p1, Lcom/ibm/icu/impl/TrieBuilder;->m_isCompacted_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_isCompacted_:Z

    .line 193
    return-void
.end method

.method protected static final equal_int([IIII)Z
    .locals 2
    .param p0, "array"    # [I
    .param p1, "start1"    # I
    .param p2, "start2"    # I
    .param p3, "length"    # I

    .prologue
    .line 201
    :goto_0
    if-lez p3, :cond_0

    aget v0, p0, p1

    aget v1, p0, p2

    if-ne v0, v1, :cond_0

    .line 202
    add-int/lit8 p1, p1, 0x1

    .line 203
    add-int/lit8 p2, p2, 0x1

    .line 204
    add-int/lit8 p3, p3, -0x1

    .line 205
    goto :goto_0

    .line 206
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected static final findSameIndexBlock([III)I
    .locals 2
    .param p0, "index"    # [I
    .param p1, "indexLength"    # I
    .param p2, "otherBlock"    # I

    .prologue
    .line 241
    const/16 v0, 0x800

    .local v0, "block":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 243
    const/16 v1, 0x20

    invoke-static {p0, v0, p2, v1}, Lcom/ibm/icu/impl/TrieBuilder;->equal_int([IIII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    .end local v0    # "block":I
    :goto_1
    return v0

    .line 242
    .restart local v0    # "block":I
    :cond_0
    add-int/lit8 v0, v0, 0x20

    goto :goto_0

    :cond_1
    move v0, p1

    .line 247
    goto :goto_1
.end method


# virtual methods
.method protected findUnusedBlocks()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 220
    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    const/16 v2, 0xff

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_indexLength_:I

    if-ge v0, v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    iget-object v2, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    shr-int/lit8 v2, v2, 0x5

    aput v3, v1, v2

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_map_:[I

    aput v3, v1, v3

    .line 229
    return-void
.end method

.method public isInZeroBlock(I)Z
    .locals 3
    .param p1, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 83
    iget-boolean v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_isCompacted_:Z

    if-nez v1, :cond_0

    const v1, 0x10ffff

    if-gt p1, v1, :cond_0

    if-gez p1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/impl/TrieBuilder;->m_index_:[I

    shr-int/lit8 v2, p1, 0x5

    aget v1, v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
