.class Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;
.super Ljava/lang/Object;
.source "UPropertyAliases.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UPropertyAliases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NameToEnum"
.end annotation


# instance fields
.field enumArray:[I

.field nameArray:[S

.field private final this$0:Lcom/ibm/icu/impl/UPropertyAliases;


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/UPropertyAliases;Lcom/ibm/icu/impl/UPropertyAliases$Builder;)V
    .locals 4
    .param p2, "b"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->this$0:Lcom/ibm/icu/impl/UPropertyAliases;

    .line 372
    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readInt()I

    move-result v0

    .line 373
    .local v0, "count":I
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->enumArray:[I

    .line 374
    new-array v2, v0, [S

    iput-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->nameArray:[S

    .line 375
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 376
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->enumArray:[I

    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readInt()I

    move-result v3

    aput v3, v2, v1

    .line 375
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 378
    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 379
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->nameArray:[S

    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readShort()S

    move-result v3

    invoke-static {p2, v3}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$400(Lcom/ibm/icu/impl/UPropertyAliases$Builder;S)S

    move-result v3

    aput-short v3, v2, v1

    .line 378
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 381
    :cond_1
    return-void
.end method


# virtual methods
.method getEnum(Ljava/lang/String;)I
    .locals 5
    .param p1, "nameProbe"    # Ljava/lang/String;

    .prologue
    .line 360
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->nameArray:[S

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 361
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->this$0:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-static {v2}, Lcom/ibm/icu/impl/UPropertyAliases;->access$300(Lcom/ibm/icu/impl/UPropertyAliases;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->nameArray:[S

    aget-short v3, v3, v1

    aget-object v2, v2, v3

    invoke-static {p1, v2}, Lcom/ibm/icu/impl/UPropertyAliases;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 363
    .local v0, "c":I
    if-lez v0, :cond_0

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    :cond_0
    if-gez v0, :cond_2

    .line 367
    .end local v0    # "c":I
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Invalid name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 365
    .restart local v0    # "c":I
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->enumArray:[I

    aget v2, v2, v1

    return v2
.end method
