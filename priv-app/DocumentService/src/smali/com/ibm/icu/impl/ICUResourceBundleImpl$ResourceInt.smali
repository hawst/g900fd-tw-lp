.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceInt"
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 112
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 113
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->key:Ljava/lang/String;

    .line 114
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->resource:J

    .line 115
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->resPath:Ljava/lang/String;

    .line 116
    return-void
.end method


# virtual methods
.method public getInt()I
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->resource:J

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->RES_GET_INT(J)I

    move-result v0

    return v0
.end method

.method public getUInt()I
    .locals 4

    .prologue
    .line 108
    iget-wide v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->resource:J

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;->RES_GET_UINT(J)J

    move-result-wide v0

    .line 109
    .local v0, "ret":J
    long-to-int v2, v0

    return v2
.end method
