.class Lcom/ibm/icu/impl/UPropertyAliases$Builder;
.super Lcom/ibm/icu/impl/ICUBinaryStream;
.source "UPropertyAliases.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UPropertyAliases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private nameGroup_map:[S

.field private stringPool_map:[S

.field private valueMap_map:[S


# direct methods
.method public constructor <init>([B)V
    .locals 0
    .param p1, "raw"    # [B

    .prologue
    .line 497
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/ICUBinaryStream;-><init>([B)V

    .line 498
    return-void
.end method

.method static access$000(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V
    .locals 0
    .param p0, "x0"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    .param p1, "x1"    # [S

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroupOffsetToIndex([S)V

    return-void
.end method

.method static access$100(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V
    .locals 0
    .param p0, "x0"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    .param p1, "x1"    # [S

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMapOffsetToIndex([S)V

    return-void
.end method

.method static access$200(Lcom/ibm/icu/impl/UPropertyAliases$Builder;)[S
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;

    .prologue
    .line 484
    iget-object v0, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMap_map:[S

    return-object v0
.end method

.method static access$400(Lcom/ibm/icu/impl/UPropertyAliases$Builder;S)S
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    .param p1, "x1"    # S

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringOffsetToIndex(S)S

    move-result v0

    return v0
.end method

.method private nameGroupOffsetToIndex(S)S
    .locals 4
    .param p1, "offset"    # S

    .prologue
    .line 634
    const/4 v0, 0x0

    .local v0, "i":S
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroup_map:[S

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 635
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroup_map:[S

    aget-short v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 636
    return v0

    .line 634
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0

    .line 639
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Can\'t map name group offset "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " to index"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private nameGroupOffsetToIndex([S)V
    .locals 2
    .param p1, "array"    # [S

    .prologue
    .line 649
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 650
    aget-short v1, p1, v0

    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroupOffsetToIndex(S)S

    move-result v1

    aput-short v1, p1, v0

    .line 649
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 652
    :cond_0
    return-void
.end method

.method private stringOffsetToIndex(S)S
    .locals 5
    .param p1, "offset"    # S

    .prologue
    .line 582
    move v1, p1

    .line 583
    .local v1, "probe":I
    if-gez v1, :cond_0

    neg-int v1, v1

    .line 584
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringPool_map:[S

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 585
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringPool_map:[S

    aget-short v2, v2, v0

    if-ne v2, v1, :cond_2

    .line 586
    if-gez p1, :cond_1

    neg-int v0, v0

    .end local v0    # "i":I
    :cond_1
    int-to-short v2, v0

    return v2

    .line 584
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 589
    :cond_3
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Can\'t map string pool offset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, " to index"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private valueMapOffsetToIndex(S)S
    .locals 4
    .param p1, "offset"    # S

    .prologue
    .line 609
    const/4 v0, 0x0

    .local v0, "i":S
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMap_map:[S

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 610
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMap_map:[S

    aget-short v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 611
    return v0

    .line 609
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0

    .line 614
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Can\'t map value map offset "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " to index"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private valueMapOffsetToIndex([S)V
    .locals 2
    .param p1, "array"    # [S

    .prologue
    .line 624
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 625
    aget-short v1, p1, v0

    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMapOffsetToIndex(S)S

    move-result v1

    aput-short v1, p1, v0

    .line 624
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 627
    :cond_0
    return-void
.end method


# virtual methods
.method public readNameGroupPool(SS)[S
    .locals 6
    .param p1, "offset"    # S
    .param p2, "count"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 559
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 560
    move v2, p1

    .line 561
    .local v2, "pos":S
    new-array v1, p2, [S

    .line 562
    .local v1, "nameGroupPool":[S
    new-array v3, p2, [S

    iput-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroup_map:[S

    .line 563
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 564
    iget-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->nameGroup_map:[S

    aput-short v2, v3, v0

    .line 565
    invoke-virtual {p0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readShort()S

    move-result v3

    invoke-direct {p0, v3}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringOffsetToIndex(S)S

    move-result v3

    aput-short v3, v1, v0

    .line 566
    add-int/lit8 v3, v2, 0x2

    int-to-short v2, v3

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    :cond_0
    invoke-static {}, Lcom/ibm/icu/impl/UPropertyAliases;->access$500()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 569
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "read nameGroupPool x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/4 v5, 0x0

    aget-short v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/4 v5, 0x1

    aget-short v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/4 v5, 0x2

    aget-short v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ",..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 574
    :cond_1
    return-object v1
.end method

.method public readStringPool(SS)[Ljava/lang/String;
    .locals 8
    .param p1, "offset"    # S
    .param p2, "count"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 518
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 522
    add-int/lit8 v5, p2, 0x1

    new-array v4, v5, [Ljava/lang/String;

    .line 523
    .local v4, "stringPool":[Ljava/lang/String;
    add-int/lit8 v5, p2, 0x1

    new-array v5, v5, [S

    iput-object v5, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringPool_map:[S

    .line 524
    move v3, p1

    .line 525
    .local v3, "pos":S
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 526
    .local v0, "buf":Ljava/lang/StringBuffer;
    iget-object v5, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringPool_map:[S

    aput-short v6, v5, v6

    .line 527
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-gt v2, p2, :cond_1

    .line 528
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 531
    :goto_1
    invoke-virtual {p0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readUnsignedByte()I

    move-result v5

    int-to-char v1, v5

    .line 532
    .local v1, "c":C
    if-nez v1, :cond_0

    .line 535
    iget-object v5, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->stringPool_map:[S

    aput-short v3, v5, v2

    .line 536
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 537
    aget-object v5, v4, v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v3

    int-to-short v3, v5

    .line 527
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 533
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 539
    .end local v1    # "c":C
    :cond_1
    invoke-static {}, Lcom/ibm/icu/impl/UPropertyAliases;->access$500()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 540
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "read stringPool x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/4 v7, 0x2

    aget-object v7, v4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/4 v7, 0x3

    aget-object v7, v4, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ",..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 545
    :cond_2
    return-object v4
.end method

.method public setupValueMap_map(SS)V
    .locals 3
    .param p1, "offset"    # S
    .param p2, "count"    # S

    .prologue
    .line 505
    new-array v1, p2, [S

    iput-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMap_map:[S

    .line 506
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 508
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->valueMap_map:[S

    mul-int/lit8 v2, v0, 0x6

    add-int/2addr v2, p1

    int-to-short v2, v2

    aput-short v2, v1, v0

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 510
    :cond_0
    return-void
.end method
