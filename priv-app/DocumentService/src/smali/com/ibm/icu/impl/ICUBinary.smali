.class public final Lcom/ibm/icu/impl/ICUBinary;
.super Ljava/lang/Object;
.source "ICUBinary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ICUBinary$Authenticate;
    }
.end annotation


# static fields
.field private static final BIG_ENDIAN_:B = 0x1t

.field private static final CHAR_SET_:B = 0x0t

.field private static final CHAR_SIZE_:B = 0x2t

.field private static final HEADER_AUTHENTICATION_FAILED_:Ljava/lang/String; = "ICU data file error: Header authentication failed, please check if you have a valid ICU data file"

.field private static final MAGIC1:B = -0x26t

.field private static final MAGIC2:B = 0x27t

.field private static final MAGIC_NUMBER_AUTHENTICATION_FAILED_:Ljava/lang/String; = "ICU data file error: Not an ICU data file"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static final readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B
    .locals 13
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "dataFormatIDExpected"    # [B
    .param p2, "authenticate"    # Lcom/ibm/icu/impl/ICUBinary$Authenticate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x4

    .line 86
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 87
    .local v6, "input":Ljava/io/DataInputStream;
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    .line 88
    .local v5, "headersize":C
    const/4 v9, 0x2

    .line 90
    .local v9, "readcount":I
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v7

    .line 91
    .local v7, "magic1":B
    add-int/lit8 v9, v9, 0x1

    .line 92
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v8

    .line 93
    .local v8, "magic2":B
    add-int/lit8 v9, v9, 0x1

    .line 94
    const/16 v11, -0x26

    if-ne v7, v11, :cond_0

    const/16 v11, 0x27

    if-eq v8, v11, :cond_1

    .line 95
    :cond_0
    new-instance v11, Ljava/io/IOException;

    const-string/jumbo v12, "ICU data file error: Not an ICU data file"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 98
    :cond_1
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readChar()C

    .line 99
    add-int/lit8 v9, v9, 0x2

    .line 100
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readChar()C

    .line 101
    add-int/lit8 v9, v9, 0x2

    .line 102
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    .line 103
    .local v0, "bigendian":B
    add-int/lit8 v9, v9, 0x1

    .line 104
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v1

    .line 105
    .local v1, "charset":B
    add-int/lit8 v9, v9, 0x1

    .line 106
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 107
    .local v2, "charsize":B
    add-int/lit8 v9, v9, 0x1

    .line 108
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    .line 109
    add-int/lit8 v9, v9, 0x1

    .line 111
    new-array v3, v12, [B

    .line 112
    .local v3, "dataFormatID":[B
    invoke-virtual {v6, v3}, Ljava/io/DataInputStream;->readFully([B)V

    .line 113
    add-int/lit8 v9, v9, 0x4

    .line 114
    new-array v4, v12, [B

    .line 115
    .local v4, "dataVersion":[B
    invoke-virtual {v6, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 116
    add-int/lit8 v9, v9, 0x4

    .line 117
    new-array v10, v12, [B

    .line 118
    .local v10, "unicodeVersion":[B
    invoke-virtual {v6, v10}, Ljava/io/DataInputStream;->readFully([B)V

    .line 119
    add-int/lit8 v9, v9, 0x4

    .line 120
    if-ge v5, v9, :cond_2

    .line 121
    new-instance v11, Ljava/io/IOException;

    const-string/jumbo v12, "Internal Error: Header size error"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 123
    :cond_2
    add-int/lit8 v11, v5, -0x18

    invoke-virtual {v6, v11}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 125
    const/4 v11, 0x1

    if-ne v0, v11, :cond_3

    if-nez v1, :cond_3

    const/4 v11, 0x2

    if-ne v2, v11, :cond_3

    invoke-static {p1, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz p2, :cond_4

    invoke-interface {p2, v4}, Lcom/ibm/icu/impl/ICUBinary$Authenticate;->isDataVersionAcceptable([B)Z

    move-result v11

    if-nez v11, :cond_4

    .line 130
    :cond_3
    new-instance v11, Ljava/io/IOException;

    const-string/jumbo v12, "ICU data file error: Header authentication failed, please check if you have a valid ICU data file"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 132
    :cond_4
    return-object v10
.end method
