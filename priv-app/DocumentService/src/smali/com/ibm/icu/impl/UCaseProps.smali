.class public final Lcom/ibm/icu/impl/UCaseProps;
.super Ljava/lang/Object;
.source "UCaseProps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/UCaseProps$1;,
        Lcom/ibm/icu/impl/UCaseProps$ContextIterator;,
        Lcom/ibm/icu/impl/UCaseProps$IsAcceptable;
    }
.end annotation


# static fields
.field private static final ABOVE:I = 0x20

.field private static final CASE_IGNORABLE:I = 0x40

.field private static final CLOSURE_MAX_LENGTH:I = 0xf

.field private static final DATA_FILE_NAME:Ljava/lang/String; = "ucase.icu"

.field private static final DATA_NAME:Ljava/lang/String; = "ucase"

.field private static final DATA_TYPE:Ljava/lang/String; = "icu"

.field private static final DELTA_SHIFT:I = 0x6

.field private static final DOT_MASK:I = 0x30

.field private static final EXCEPTION:I = 0x8

.field private static final EXC_CLOSURE:I = 0x6

.field private static final EXC_CONDITIONAL_FOLD:I = 0x8000

.field private static final EXC_CONDITIONAL_SPECIAL:I = 0x4000

.field private static final EXC_DOT_SHIFT:I = 0x8

.field private static final EXC_DOUBLE_SLOTS:I = 0x100

.field private static final EXC_FOLD:I = 0x1

.field private static final EXC_FULL_MAPPINGS:I = 0x7

.field private static final EXC_LOWER:I = 0x0

.field private static final EXC_SHIFT:I = 0x4

.field private static final EXC_TITLE:I = 0x3

.field private static final EXC_UPPER:I = 0x2

.field private static final FMT:[B

.field private static final FOLD_CASE_OPTIONS_MASK:I = 0xff

.field private static final FULL_LOWER:I = 0xf

.field private static final IX_EXC_LENGTH:I = 0x3

.field private static final IX_INDEX_TOP:I = 0x0

.field private static final IX_TOP:I = 0x10

.field private static final IX_UNFOLD_LENGTH:I = 0x4

.field private static final LOC_LITHUANIAN:I = 0x3

.field private static final LOC_ROOT:I = 0x1

.field private static final LOC_TURKISH:I = 0x2

.field private static final LOC_UNKNOWN:I = 0x0

.field public static final LOWER:I = 0x1

.field public static final MAX_STRING_LENGTH:I = 0x1f

.field public static final NONE:I = 0x0

.field private static final OTHER_ACCENT:I = 0x30

.field private static final SENSITIVE:I = 0x4

.field private static final SOFT_DOTTED:I = 0x10

.field public static final TITLE:I = 0x3

.field public static final TYPE_MASK:I = 0x3

.field private static final UNFOLD_ROWS:I = 0x0

.field private static final UNFOLD_ROW_WIDTH:I = 0x1

.field private static final UNFOLD_STRING_WIDTH:I = 0x2

.field public static final UPPER:I = 0x2

.field private static final flagsOffset:[B

.field private static gCsp:Lcom/ibm/icu/impl/UCaseProps; = null

.field private static gCspDummy:Lcom/ibm/icu/impl/UCaseProps; = null

.field private static final iDot:Ljava/lang/String; = "i\u0307"

.field private static final iDotAcute:Ljava/lang/String; = "i\u0307\u0301"

.field private static final iDotGrave:Ljava/lang/String; = "i\u0307\u0300"

.field private static final iDotTilde:Ljava/lang/String; = "i\u0307\u0303"

.field private static final iOgonekDot:Ljava/lang/String; = "\u012f\u0307"

.field private static final jDot:Ljava/lang/String; = "j\u0307"


# instance fields
.field private exceptions:[C

.field private indexes:[I

.field private trie:Lcom/ibm/icu/impl/CharTrie;

.field private unfold:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    .line 108
    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCspDummy:Lcom/ibm/icu/impl/UCaseProps;

    .line 160
    const/16 v0, 0x100

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->flagsOffset:[B

    .line 1303
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->FMT:[B

    return-void

    .line 160
    nop

    :array_0
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x2t
        0x1t
        0x2t
        0x2t
        0x3t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x5t
        0x6t
        0x6t
        0x7t
        0x6t
        0x7t
        0x7t
        0x8t
    .end array-data

    .line 1303
    :array_1
    .array-data 1
        0x63t
        0x41t
        0x53t
        0x45t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string/jumbo v2, "data/icudt40b/ucase.icu"

    invoke-static {v2}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 41
    .local v1, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 42
    .local v0, "b":Ljava/io/BufferedInputStream;
    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/UCaseProps;->readData(Ljava/io/InputStream;)V

    .line 43
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 44
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 45
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "makeDummy"    # Z

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    .line 112
    iget-object v0, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    aput v1, v0, v2

    .line 113
    new-instance v0, Lcom/ibm/icu/impl/CharTrie;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/ibm/icu/impl/CharTrie;-><init>(IILcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    .line 114
    return-void
.end method

.method private static final getCaseLocale(Lcom/ibm/icu/util/ULocale;[I)I
    .locals 5
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "locCache"    # [I

    .prologue
    const/4 v4, 0x0

    .line 669
    if-eqz p1, :cond_0

    aget v1, p1, v4

    .local v1, "result":I
    if-eqz v1, :cond_0

    move v2, v1

    .line 685
    .end local v1    # "result":I
    .local v2, "result":I
    :goto_0
    return v2

    .line 673
    .end local v2    # "result":I
    :cond_0
    const/4 v1, 0x1

    .line 675
    .restart local v1    # "result":I
    invoke-virtual {p0}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 676
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v3, "tr"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "tur"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "az"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "aze"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 677
    :cond_1
    const/4 v1, 0x2

    .line 682
    :cond_2
    :goto_1
    if-eqz p1, :cond_3

    .line 683
    aput v1, p1, v4

    :cond_3
    move v2, v1

    .line 685
    .end local v1    # "result":I
    .restart local v2    # "result":I
    goto :goto_0

    .line 678
    .end local v2    # "result":I
    .restart local v1    # "result":I
    :cond_4
    const-string/jumbo v3, "lt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "lit"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 679
    :cond_5
    const/4 v1, 0x3

    goto :goto_1
.end method

.method private static final getDelta(I)I
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 1344
    int-to-short v0, p0

    shr-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public static final declared-synchronized getDummy()Lcom/ibm/icu/impl/UCaseProps;
    .locals 3

    .prologue
    .line 123
    const-class v1, Lcom/ibm/icu/impl/UCaseProps;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCspDummy:Lcom/ibm/icu/impl/UCaseProps;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/ibm/icu/impl/UCaseProps;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/ibm/icu/impl/UCaseProps;-><init>(Z)V

    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCspDummy:Lcom/ibm/icu/impl/UCaseProps;

    .line 126
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCspDummy:Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final getExceptionsOffset(I)I
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 152
    shr-int/lit8 v0, p0, 0x4

    return v0
.end method

.method public static final declared-synchronized getSingleton()Lcom/ibm/icu/impl/UCaseProps;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    const-class v1, Lcom/ibm/icu/impl/UCaseProps;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/ibm/icu/impl/UCaseProps;

    invoke-direct {v0}, Lcom/ibm/icu/impl/UCaseProps;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    .line 104
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/UCaseProps;->gCsp:Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final getSlotValue(III)I
    .locals 4
    .param p1, "excWord"    # I
    .param p2, "index"    # I
    .param p3, "excOffset"    # I

    .prologue
    .line 211
    and-int/lit16 v2, p1, 0x100

    if-nez v2, :cond_0

    .line 212
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/UCaseProps;->slotOffset(II)B

    move-result v2

    add-int/2addr p3, v2

    .line 213
    iget-object v2, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    aget-char v1, v2, p3

    .line 219
    .local v1, "value":I
    :goto_0
    return v1

    .line 215
    .end local v1    # "value":I
    :cond_0
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/UCaseProps;->slotOffset(II)B

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr p3, v2

    .line 216
    iget-object v2, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v0, p3, 0x1

    .end local p3    # "excOffset":I
    .local v0, "excOffset":I
    aget-char v1, v2, p3

    .line 217
    .restart local v1    # "value":I
    shl-int/lit8 v2, v1, 0x10

    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    aget-char v3, v3, v0

    or-int v1, v2, v3

    move p3, v0

    .end local v0    # "excOffset":I
    .restart local p3    # "excOffset":I
    goto :goto_0
.end method

.method private final getSlotValueAndOffset(III)J
    .locals 8
    .param p1, "excWord"    # I
    .param p2, "index"    # I
    .param p3, "excOffset"    # I

    .prologue
    .line 197
    and-int/lit16 v1, p1, 0x100

    if-nez v1, :cond_0

    .line 198
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/UCaseProps;->slotOffset(II)B

    move-result v1

    add-int/2addr p3, v1

    .line 199
    iget-object v1, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    aget-char v1, v1, p3

    int-to-long v2, v1

    .line 205
    .local v2, "value":J
    :goto_0
    int-to-long v4, p3

    const/16 v1, 0x20

    shl-long/2addr v4, v1

    or-long/2addr v4, v2

    return-wide v4

    .line 201
    .end local v2    # "value":J
    :cond_0
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/UCaseProps;->slotOffset(II)B

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr p3, v1

    .line 202
    iget-object v1, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v0, p3, 0x1

    .end local p3    # "excOffset":I
    .local v0, "excOffset":I
    aget-char v1, v1, p3

    int-to-long v2, v1

    .line 203
    .restart local v2    # "value":J
    const/16 v1, 0x10

    shl-long v4, v2, v1

    iget-object v1, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    aget-char v1, v1, v0

    int-to-long v6, v1

    or-long v2, v4, v6

    move p3, v0

    .end local v0    # "excOffset":I
    .restart local p3    # "excOffset":I
    goto :goto_0
.end method

.method private static final getTypeFromProps(I)I
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 1325
    and-int/lit8 v0, p0, 0x3

    return v0
.end method

.method private static final hasSlot(II)Z
    .locals 2
    .param p0, "flags"    # I
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x1

    .line 180
    shl-int v1, v0, p1

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final isFollowedByCasedLetter(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;I)Z
    .locals 5
    .param p1, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;
    .param p2, "dir"    # I

    .prologue
    const/4 v2, 0x0

    .line 693
    if-nez p1, :cond_1

    .line 708
    :cond_0
    :goto_0
    return v2

    .line 697
    :cond_1
    invoke-interface {p1, p2}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->reset(I)V

    :cond_2
    :goto_1
    invoke-interface {p1}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->next()I

    move-result v0

    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 698
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v1

    .line 699
    .local v1, "props":I
    invoke-static {v1}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v3

    if-eqz v3, :cond_3

    .line 700
    const/4 v2, 0x1

    goto :goto_0

    .line 701
    :cond_3
    const/16 v3, 0x307

    if-eq v0, v3, :cond_2

    and-int/lit8 v3, v1, 0x48

    const/16 v4, 0x40

    if-ne v3, v4, :cond_0

    goto :goto_1
.end method

.method private final isFollowedByDotAbove(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z
    .locals 5
    .param p1, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 814
    if-nez p1, :cond_1

    .line 828
    :cond_0
    :goto_0
    return v2

    .line 818
    :cond_1
    invoke-interface {p1, v3}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->reset(I)V

    :cond_2
    invoke-interface {p1}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->next()I

    move-result v0

    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 819
    const/16 v4, 0x307

    if-ne v0, v4, :cond_3

    move v2, v3

    .line 820
    goto :goto_0

    .line 822
    :cond_3
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/UCaseProps;->getDotType(I)I

    move-result v1

    .line 823
    .local v1, "dotType":I
    const/16 v4, 0x30

    if-eq v1, v4, :cond_2

    goto :goto_0
.end method

.method private final isFollowedByMoreAbove(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z
    .locals 5
    .param p1, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 793
    if-nez p1, :cond_1

    .line 806
    :cond_0
    :goto_0
    return v2

    .line 797
    :cond_1
    invoke-interface {p1, v3}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->reset(I)V

    :cond_2
    invoke-interface {p1}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->next()I

    move-result v0

    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 798
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/UCaseProps;->getDotType(I)I

    move-result v1

    .line 799
    .local v1, "dotType":I
    const/16 v4, 0x20

    if-ne v1, v4, :cond_3

    move v2, v3

    .line 800
    goto :goto_0

    .line 801
    :cond_3
    const/16 v4, 0x30

    if-eq v1, v4, :cond_2

    goto :goto_0
.end method

.method private final isPrecededBySoftDotted(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z
    .locals 4
    .param p1, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;

    .prologue
    const/4 v2, 0x0

    .line 716
    if-nez p1, :cond_1

    .line 729
    :cond_0
    :goto_0
    return v2

    .line 720
    :cond_1
    const/4 v3, -0x1

    invoke-interface {p1, v3}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->reset(I)V

    :cond_2
    invoke-interface {p1}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->next()I

    move-result v0

    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 721
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/UCaseProps;->getDotType(I)I

    move-result v1

    .line 722
    .local v1, "dotType":I
    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 723
    const/4 v2, 0x1

    goto :goto_0

    .line 724
    :cond_3
    const/16 v3, 0x30

    if-eq v1, v3, :cond_2

    goto :goto_0
.end method

.method private final isPrecededBy_I(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z
    .locals 4
    .param p1, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;

    .prologue
    const/4 v2, 0x0

    .line 771
    if-nez p1, :cond_1

    .line 785
    :cond_0
    :goto_0
    return v2

    .line 775
    :cond_1
    const/4 v3, -0x1

    invoke-interface {p1, v3}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->reset(I)V

    :cond_2
    invoke-interface {p1}, Lcom/ibm/icu/impl/UCaseProps$ContextIterator;->next()I

    move-result v0

    .local v0, "c":I
    if-ltz v0, :cond_0

    .line 776
    const/16 v3, 0x49

    if-ne v0, v3, :cond_3

    .line 777
    const/4 v2, 0x1

    goto :goto_0

    .line 779
    :cond_3
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/UCaseProps;->getDotType(I)I

    move-result v1

    .line 780
    .local v1, "dotType":I
    const/16 v3, 0x30

    if-eq v1, v3, :cond_2

    goto :goto_0
.end method

.method private static final propsHasException(I)Z
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 156
    and-int/lit8 v0, p0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final readData(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 48
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 51
    .local v2, "inputStream":Ljava/io/DataInputStream;
    sget-object v3, Lcom/ibm/icu/impl/UCaseProps;->FMT:[B

    new-instance v4, Lcom/ibm/icu/impl/UCaseProps$IsAcceptable;

    invoke-direct {v4, p0, v5}, Lcom/ibm/icu/impl/UCaseProps$IsAcceptable;-><init>(Lcom/ibm/icu/impl/UCaseProps;Lcom/ibm/icu/impl/UCaseProps$1;)V

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    .line 55
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 56
    .local v0, "count":I
    if-gez v0, :cond_0

    .line 57
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "indexes[0] too small in ucase.icu"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 59
    :cond_0
    new-array v3, v0, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    .line 61
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    const/4 v4, 0x0

    aput v0, v3, v4

    .line 62
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 63
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    aput v4, v3, v1

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_1
    new-instance v3, Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v3, v2, v5}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    .line 70
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    const/4 v4, 0x3

    aget v0, v3, v4

    .line 71
    if-lez v0, :cond_2

    .line 72
    new-array v3, v0, [C

    iput-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    .line 73
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    .line 74
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readChar()C

    move-result v4

    aput-char v4, v3, v1

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->indexes:[I

    const/4 v4, 0x4

    aget v0, v3, v4

    .line 80
    if-lez v0, :cond_3

    .line 81
    new-array v3, v0, [C

    iput-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    .line 82
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_3

    .line 83
    iget-object v3, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readChar()C

    move-result v4

    aput-char v4, v3, v1

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 86
    :cond_3
    return-void
.end method

.method private static final slotOffset(II)B
    .locals 2
    .param p0, "flags"    # I
    .param p1, "index"    # I

    .prologue
    .line 183
    sget-object v0, Lcom/ibm/icu/impl/UCaseProps;->flagsOffset:[B

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p0

    aget-byte v0, v0, v1

    return v0
.end method

.method private final strcmpMax(Ljava/lang/String;II)I
    .locals 7
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "unfoldOffset"    # I
    .param p3, "max"    # I

    .prologue
    .line 403
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 404
    .local v4, "length":I
    sub-int/2addr p3, v4

    .line 405
    const/4 v2, 0x0

    .line 407
    .local v2, "i1":I
    :goto_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i1":I
    .local v3, "i1":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 408
    .local v0, "c1":I
    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    add-int/lit8 v5, p2, 0x1

    .end local p2    # "unfoldOffset":I
    .local v5, "unfoldOffset":I
    aget-char v1, v6, p2

    .line 409
    .local v1, "c2":I
    if-nez v1, :cond_0

    .line 410
    const/4 v6, 0x1

    .line 422
    :goto_1
    return v6

    .line 412
    :cond_0
    sub-int/2addr v0, v1

    .line 413
    if-eqz v0, :cond_1

    move v6, v0

    .line 414
    goto :goto_1

    .line 416
    :cond_1
    add-int/lit8 v4, v4, -0x1

    if-gtz v4, :cond_4

    .line 419
    if-eqz p3, :cond_2

    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    aget-char v6, v6, v5

    if-nez v6, :cond_3

    .line 420
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 422
    :cond_3
    neg-int v6, p3

    goto :goto_1

    :cond_4
    move v2, v3

    .end local v3    # "i1":I
    .restart local v2    # "i1":I
    move p2, v5

    .end local v5    # "unfoldOffset":I
    .restart local p2    # "unfoldOffset":I
    goto :goto_0
.end method

.method private final toUpperOrTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[IZ)I
    .locals 16
    .param p1, "c"    # I
    .param p2, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;
    .param p3, "out"    # Ljava/lang/StringBuffer;
    .param p4, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p5, "locCache"    # [I
    .param p6, "upperNotTitle"    # Z

    .prologue
    .line 1010
    move/from16 v10, p1

    .line 1011
    .local v10, "result":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v9

    .line 1012
    .local v9, "props":I
    invoke-static {v9}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v11

    if-nez v11, :cond_1

    .line 1013
    invoke-static {v9}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v11

    const/4 v14, 0x1

    if-ne v11, v14, :cond_0

    .line 1014
    invoke-static {v9}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v11

    add-int v10, p1, v11

    .line 1095
    :cond_0
    :goto_0
    move/from16 v0, p1

    if-ne v10, v0, :cond_9

    xor-int/lit8 v11, v10, -0x1

    :goto_1
    move v6, v11

    :goto_2
    return v6

    .line 1017
    :cond_1
    invoke-static {v9}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v2

    .line 1018
    .local v2, "excOffset":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "excOffset":I
    .local v3, "excOffset":I
    aget-char v5, v11, v2

    .line 1021
    .local v5, "excWord":I
    move v4, v3

    .line 1023
    .local v4, "excOffset2":I
    and-int/lit16 v11, v5, 0x4000

    if-eqz v11, :cond_5

    .line 1025
    invoke-static/range {p4 .. p5}, Lcom/ibm/icu/impl/UCaseProps;->getCaseLocale(Lcom/ibm/icu/util/ULocale;[I)I

    move-result v8

    .line 1027
    .local v8, "loc":I
    const/4 v11, 0x2

    if-ne v8, v11, :cond_2

    const/16 v11, 0x69

    move/from16 v0, p1

    if-ne v0, v11, :cond_2

    .line 1039
    const/16 v6, 0x130

    goto :goto_2

    .line 1040
    :cond_2
    const/4 v11, 0x3

    if-ne v8, v11, :cond_3

    const/16 v11, 0x307

    move/from16 v0, p1

    if-ne v0, v11, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/UCaseProps;->isPrecededBySoftDotted(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1050
    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1084
    .end local v3    # "excOffset":I
    .end local v8    # "loc":I
    .restart local v2    # "excOffset":I
    :cond_4
    :goto_3
    if-nez p6, :cond_7

    const/4 v11, 0x3

    invoke-static {v5, v11}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1085
    const/4 v7, 0x3

    .line 1092
    .local v7, "index":I
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7, v4}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result v10

    goto :goto_0

    .line 1054
    .end local v2    # "excOffset":I
    .end local v7    # "index":I
    .restart local v3    # "excOffset":I
    :cond_5
    const/4 v11, 0x7

    invoke-static {v5, v11}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1055
    const/4 v11, 0x7

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v11, v3}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValueAndOffset(III)J

    move-result-wide v12

    .line 1056
    .local v12, "value":J
    long-to-int v11, v12

    const v14, 0xffff

    and-int v6, v11, v14

    .line 1059
    .local v6, "full":I
    const/16 v11, 0x20

    shr-long v14, v12, v11

    long-to-int v11, v14

    add-int/lit8 v2, v11, 0x1

    .line 1062
    .end local v3    # "excOffset":I
    .restart local v2    # "excOffset":I
    and-int/lit8 v11, v6, 0xf

    add-int/2addr v2, v11

    .line 1063
    shr-int/lit8 v6, v6, 0x4

    .line 1064
    and-int/lit8 v11, v6, 0xf

    add-int/2addr v2, v11

    .line 1065
    shr-int/lit8 v6, v6, 0x4

    .line 1067
    if-eqz p6, :cond_6

    .line 1068
    and-int/lit8 v6, v6, 0xf

    .line 1075
    :goto_5
    if-eqz v6, :cond_4

    .line 1077
    new-instance v11, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    invoke-direct {v11, v14, v2, v6}, Ljava/lang/String;-><init>([CII)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 1071
    :cond_6
    and-int/lit8 v11, v6, 0xf

    add-int/2addr v2, v11

    .line 1072
    shr-int/lit8 v11, v6, 0x4

    and-int/lit8 v6, v11, 0xf

    goto :goto_5

    .line 1086
    .end local v6    # "full":I
    .end local v12    # "value":J
    :cond_7
    const/4 v11, 0x2

    invoke-static {v5, v11}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1088
    const/4 v7, 0x2

    .line 1089
    .restart local v7    # "index":I
    goto :goto_4

    .line 1090
    .end local v7    # "index":I
    :cond_8
    xor-int/lit8 v6, p1, -0x1

    goto/16 :goto_2

    .end local v2    # "excOffset":I
    .end local v4    # "excOffset2":I
    .end local v5    # "excWord":I
    :cond_9
    move v11, v10

    .line 1095
    goto/16 :goto_1

    .restart local v3    # "excOffset":I
    .restart local v4    # "excOffset2":I
    .restart local v5    # "excWord":I
    :cond_a
    move v2, v3

    .end local v3    # "excOffset":I
    .restart local v2    # "excOffset":I
    goto :goto_3
.end method


# virtual methods
.method public final addCaseClosure(ILcom/ibm/icu/text/UnicodeSet;)V
    .locals 18
    .param p1, "c"    # I
    .param p2, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 296
    sparse-switch p1, :sswitch_data_0

    .line 316
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v12

    .line 317
    .local v12, "props":I
    invoke-static {v12}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v13

    if-nez v13, :cond_1

    .line 318
    invoke-static {v12}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v13

    if-eqz v13, :cond_0

    .line 320
    invoke-static {v12}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v4

    .line 321
    .local v4, "delta":I
    if-eqz v4, :cond_0

    .line 322
    add-int v13, p1, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 394
    .end local v4    # "delta":I
    .end local v12    # "props":I
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 299
    :sswitch_1
    const/16 v13, 0x69

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 302
    :sswitch_2
    const/16 v13, 0x49

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 306
    :sswitch_3
    const-string/jumbo v13, "i\u0307"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 330
    .restart local v12    # "props":I
    :cond_1
    invoke-static {v12}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v5

    .line 332
    .local v5, "excOffset":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "excOffset":I
    .local v6, "excOffset":I
    aget-char v8, v13, v5

    .line 335
    .local v8, "excWord":I
    move v7, v6

    .line 338
    .local v7, "excOffset0":I
    const/4 v10, 0x0

    .local v10, "index":I
    move v5, v6

    .end local v6    # "excOffset":I
    .restart local v5    # "excOffset":I
    :goto_1
    const/4 v13, 0x3

    if-gt v10, v13, :cond_3

    .line 339
    invoke-static {v8, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 340
    move v5, v7

    .line 341
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v10, v5}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result p1

    .line 342
    move-object/from16 v0, p2

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 338
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 347
    :cond_3
    const/4 v13, 0x6

    invoke-static {v8, v13}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 348
    move v5, v7

    .line 349
    const/4 v13, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v13, v5}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValueAndOffset(III)J

    move-result-wide v14

    .line 350
    .local v14, "value":J
    long-to-int v13, v14

    and-int/lit8 v2, v13, 0xf

    .line 351
    .local v2, "closureLength":I
    const/16 v13, 0x20

    shr-long v16, v14, v13

    move-wide/from16 v0, v16

    long-to-int v13, v0

    add-int/lit8 v3, v13, 0x1

    .line 358
    .end local v14    # "value":J
    .local v3, "closureOffset":I
    :goto_2
    const/4 v13, 0x7

    invoke-static {v8, v13}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 359
    move v5, v7

    .line 360
    const/4 v13, 0x7

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v13, v5}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValueAndOffset(III)J

    move-result-wide v14

    .line 361
    .restart local v14    # "value":J
    long-to-int v9, v14

    .line 364
    .local v9, "fullLength":I
    const/16 v13, 0x20

    shr-long v16, v14, v13

    move-wide/from16 v0, v16

    long-to-int v13, v0

    add-int/lit8 v5, v13, 0x1

    .line 366
    const v13, 0xffff

    and-int/2addr v9, v13

    .line 369
    and-int/lit8 v13, v9, 0xf

    add-int/2addr v5, v13

    .line 370
    shr-int/lit8 v9, v9, 0x4

    .line 373
    and-int/lit8 v11, v9, 0xf

    .line 374
    .local v11, "length":I
    if-eqz v11, :cond_4

    .line 375
    new-instance v13, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v13, v0, v5, v11}, Ljava/lang/String;-><init>([CII)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 376
    add-int/2addr v5, v11

    .line 380
    :cond_4
    shr-int/lit8 v9, v9, 0x4

    .line 381
    and-int/lit8 v13, v9, 0xf

    add-int/2addr v5, v13

    .line 382
    shr-int/lit8 v9, v9, 0x4

    .line 383
    add-int/2addr v5, v9

    .line 385
    move v3, v5

    .line 389
    .end local v9    # "fullLength":I
    .end local v11    # "length":I
    .end local v14    # "value":J
    :cond_5
    const/4 v10, 0x0

    :goto_3
    if-ge v10, v2, :cond_0

    .line 390
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v13, v3, v0, v10}, Lcom/ibm/icu/text/UTF16;->charAt([CIII)I

    move-result p1

    .line 391
    move-object/from16 v0, p2

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 389
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v13

    add-int/2addr v10, v13

    goto :goto_3

    .line 353
    .end local v2    # "closureLength":I
    .end local v3    # "closureOffset":I
    :cond_6
    const/4 v2, 0x0

    .line 354
    .restart local v2    # "closureLength":I
    const/4 v3, 0x0

    .restart local v3    # "closureOffset":I
    goto :goto_2

    .line 296
    :sswitch_data_0
    .sparse-switch
        0x49 -> :sswitch_1
        0x69 -> :sswitch_2
        0x130 -> :sswitch_3
        0x131 -> :sswitch_0
    .end sparse-switch
.end method

.method public final addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 3
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 133
    new-instance v1, Lcom/ibm/icu/impl/TrieIterator;

    iget-object v2, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v1, v2}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 134
    .local v1, "iter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v0, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v0}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 136
    .local v0, "element":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    invoke-virtual {v1, v0}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    iget v2, v0, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 148
    :cond_0
    return-void
.end method

.method public final addStringCaseClosure(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 12
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 440
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    if-eqz v10, :cond_0

    if-nez p1, :cond_1

    .line 441
    :cond_0
    const/4 v10, 0x0

    .line 490
    :goto_0
    return v10

    .line 443
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 444
    .local v2, "length":I
    const/4 v10, 0x1

    if-gt v2, v10, :cond_2

    .line 452
    const/4 v10, 0x0

    goto :goto_0

    .line 455
    :cond_2
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    const/4 v11, 0x0

    aget-char v8, v10, v11

    .line 456
    .local v8, "unfoldRows":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    const/4 v11, 0x1

    aget-char v7, v10, v11

    .line 457
    .local v7, "unfoldRowWidth":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    const/4 v11, 0x2

    aget-char v9, v10, v11

    .line 460
    .local v9, "unfoldStringWidth":I
    if-le v2, v9, :cond_3

    .line 462
    const/4 v10, 0x0

    goto :goto_0

    .line 466
    :cond_3
    const/4 v5, 0x0

    .line 467
    .local v5, "start":I
    move v3, v8

    .line 468
    .local v3, "limit":I
    :goto_1
    if-ge v5, v3, :cond_7

    .line 469
    add-int v10, v5, v3

    div-int/lit8 v1, v10, 0x2

    .line 470
    .local v1, "i":I
    add-int/lit8 v10, v1, 0x1

    mul-int v6, v10, v7

    .line 471
    .local v6, "unfoldOffset":I
    invoke-direct {p0, p1, v6, v9}, Lcom/ibm/icu/impl/UCaseProps;->strcmpMax(Ljava/lang/String;II)I

    move-result v4

    .line 473
    .local v4, "result":I
    if-nez v4, :cond_5

    .line 477
    move v1, v9

    :goto_2
    if-ge v1, v7, :cond_4

    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    add-int v11, v6, v1

    aget-char v10, v10, v11

    if-eqz v10, :cond_4

    .line 478
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    iget-object v11, p0, Lcom/ibm/icu/impl/UCaseProps;->unfold:[C

    array-length v11, v11

    invoke-static {v10, v6, v11, v1}, Lcom/ibm/icu/text/UTF16;->charAt([CIII)I

    move-result v0

    .line 479
    .local v0, "c":I
    invoke-virtual {p2, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 480
    invoke-virtual {p0, v0, p2}, Lcom/ibm/icu/impl/UCaseProps;->addCaseClosure(ILcom/ibm/icu/text/UnicodeSet;)V

    .line 477
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v10

    add-int/2addr v1, v10

    goto :goto_2

    .line 482
    .end local v0    # "c":I
    :cond_4
    const/4 v10, 0x1

    goto :goto_0

    .line 483
    :cond_5
    if-gez v4, :cond_6

    .line 484
    move v3, v1

    .line 485
    goto :goto_1

    .line 486
    :cond_6
    add-int/lit8 v5, v1, 0x1

    .line 488
    goto :goto_1

    .line 490
    .end local v1    # "i":I
    .end local v4    # "result":I
    .end local v6    # "unfoldOffset":I
    :cond_7
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public final fold(II)I
    .locals 9
    .param p1, "c"    # I
    .param p2, "options"    # I

    .prologue
    const/16 v8, 0x130

    const/16 v5, 0x69

    const/16 v7, 0x49

    .line 1160
    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v4

    .line 1161
    .local v4, "props":I
    invoke-static {v4}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1162
    invoke-static {v4}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_0

    .line 1163
    invoke-static {v4}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v5

    add-int/2addr p1, v5

    :cond_0
    :goto_0
    move v5, p1

    .line 1200
    :cond_1
    :goto_1
    return v5

    .line 1166
    :cond_2
    invoke-static {v4}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v0

    .line 1167
    .local v0, "excOffset":I
    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "excOffset":I
    .local v1, "excOffset":I
    aget-char v2, v6, v0

    .line 1169
    .local v2, "excWord":I
    const v6, 0x8000

    and-int/2addr v6, v2

    if-eqz v6, :cond_5

    .line 1171
    and-int/lit16 v6, p2, 0xff

    if-nez v6, :cond_3

    .line 1173
    if-eq p1, v7, :cond_1

    .line 1176
    if-ne p1, v8, :cond_5

    move v5, p1

    .line 1178
    goto :goto_1

    .line 1182
    :cond_3
    if-ne p1, v7, :cond_4

    .line 1184
    const/16 v5, 0x131

    goto :goto_1

    .line 1185
    :cond_4
    if-eq p1, v8, :cond_1

    .line 1191
    :cond_5
    const/4 v5, 0x1

    invoke-static {v2, v5}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1192
    const/4 v3, 0x1

    .line 1198
    .local v3, "index":I
    :goto_2
    invoke-direct {p0, v2, v3, v1}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result p1

    goto :goto_0

    .line 1193
    .end local v3    # "index":I
    :cond_6
    const/4 v5, 0x0

    invoke-static {v2, v5}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1194
    const/4 v3, 0x0

    .line 1195
    .restart local v3    # "index":I
    goto :goto_2

    .end local v3    # "index":I
    :cond_7
    move v5, p1

    .line 1196
    goto :goto_1
.end method

.method public final getDotType(I)I
    .locals 3
    .param p1, "c"    # I

    .prologue
    .line 516
    iget-object v1, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 517
    .local v0, "props":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 518
    and-int/lit8 v1, v0, 0x30

    .line 520
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    invoke-static {v0}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v2

    aget-char v1, v1, v2

    shr-int/lit8 v1, v1, 0x8

    and-int/lit8 v1, v1, 0x30

    goto :goto_0
.end method

.method public final getType(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 495
    iget-object v0, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v0

    return v0
.end method

.method public final getTypeOrIgnorable(I)I
    .locals 4
    .param p1, "c"    # I

    .prologue
    .line 500
    iget-object v2, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 501
    .local v0, "props":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v1

    .line 502
    .local v1, "type":I
    if-eqz v1, :cond_0

    .line 510
    .end local v1    # "type":I
    :goto_0
    return v1

    .line 504
    .restart local v1    # "type":I
    :cond_0
    const/16 v2, 0x307

    if-eq p1, v2, :cond_1

    and-int/lit8 v2, v0, 0x48

    const/16 v3, 0x40

    if-ne v2, v3, :cond_2

    .line 508
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 510
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final isCaseSensitive(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 529
    iget-object v0, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSoftDotted(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 525
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UCaseProps;->getDotType(I)I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toFullFolding(ILjava/lang/StringBuffer;I)I
    .locals 12
    .param p1, "c"    # I
    .param p2, "out"    # Ljava/lang/StringBuffer;
    .param p3, "options"    # I

    .prologue
    .line 1222
    move v7, p1

    .line 1223
    .local v7, "result":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v10, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v6

    .line 1224
    .local v6, "props":I
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 1225
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    .line 1226
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v10

    add-int v7, p1, v10

    .line 1287
    :cond_0
    :goto_0
    if-ne v7, p1, :cond_a

    xor-int/lit8 v10, v7, -0x1

    :goto_1
    move v4, v10

    :goto_2
    return v4

    .line 1229
    :cond_1
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v0

    .line 1230
    .local v0, "excOffset":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "excOffset":I
    .local v1, "excOffset":I
    aget-char v3, v10, v0

    .line 1233
    .local v3, "excWord":I
    move v2, v1

    .line 1235
    .local v2, "excOffset2":I
    const v10, 0x8000

    and-int/2addr v10, v3

    if-eqz v10, :cond_5

    .line 1237
    and-int/lit16 v10, p3, 0xff

    if-nez v10, :cond_3

    .line 1239
    const/16 v10, 0x49

    if-ne p1, v10, :cond_2

    .line 1241
    const/16 v4, 0x69

    goto :goto_2

    .line 1242
    :cond_2
    const/16 v10, 0x130

    if-ne p1, v10, :cond_6

    .line 1244
    const-string/jumbo v10, "i\u0307"

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1245
    const/4 v4, 0x2

    goto :goto_2

    .line 1249
    :cond_3
    const/16 v10, 0x49

    if-ne p1, v10, :cond_4

    .line 1251
    const/16 v4, 0x131

    goto :goto_2

    .line 1252
    :cond_4
    const/16 v10, 0x130

    if-ne p1, v10, :cond_6

    .line 1254
    const/16 v4, 0x69

    goto :goto_2

    .line 1257
    :cond_5
    const/4 v10, 0x7

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1258
    const/4 v10, 0x7

    invoke-direct {p0, v3, v10, v1}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValueAndOffset(III)J

    move-result-wide v8

    .line 1259
    .local v8, "value":J
    long-to-int v10, v8

    const v11, 0xffff

    and-int v4, v10, v11

    .line 1262
    .local v4, "full":I
    const/16 v10, 0x20

    shr-long v10, v8, v10

    long-to-int v10, v10

    add-int/lit8 v0, v10, 0x1

    .line 1265
    .end local v1    # "excOffset":I
    .restart local v0    # "excOffset":I
    and-int/lit8 v10, v4, 0xf

    add-int/2addr v0, v10

    .line 1266
    shr-int/lit8 v10, v4, 0x4

    and-int/lit8 v4, v10, 0xf

    .line 1268
    if-eqz v4, :cond_7

    .line 1270
    new-instance v10, Ljava/lang/String;

    iget-object v11, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    invoke-direct {v10, v11, v0, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .end local v0    # "excOffset":I
    .end local v4    # "full":I
    .end local v8    # "value":J
    .restart local v1    # "excOffset":I
    :cond_6
    move v0, v1

    .line 1277
    .end local v1    # "excOffset":I
    .restart local v0    # "excOffset":I
    :cond_7
    const/4 v10, 0x1

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1278
    const/4 v5, 0x1

    .line 1284
    .local v5, "index":I
    :goto_3
    invoke-direct {p0, v3, v5, v2}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result v7

    goto :goto_0

    .line 1279
    .end local v5    # "index":I
    :cond_8
    const/4 v10, 0x0

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1280
    const/4 v5, 0x0

    .line 1281
    .restart local v5    # "index":I
    goto :goto_3

    .line 1282
    .end local v5    # "index":I
    :cond_9
    xor-int/lit8 v4, p1, -0x1

    goto :goto_2

    .end local v0    # "excOffset":I
    .end local v2    # "excOffset2":I
    .end local v3    # "excWord":I
    :cond_a
    move v10, v7

    .line 1287
    goto :goto_1
.end method

.method public final toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I
    .locals 12
    .param p1, "c"    # I
    .param p2, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;
    .param p3, "out"    # Ljava/lang/StringBuffer;
    .param p4, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p5, "locCache"    # [I

    .prologue
    .line 862
    move v7, p1

    .line 863
    .local v7, "result":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v10, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v6

    .line 864
    .local v6, "props":I
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 865
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    .line 866
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v10

    add-int v7, p1, v10

    .line 999
    :cond_0
    :goto_0
    if-ne v7, p1, :cond_c

    xor-int/lit8 v10, v7, -0x1

    :goto_1
    move v4, v10

    :goto_2
    return v4

    .line 869
    :cond_1
    invoke-static {v6}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v0

    .line 870
    .local v0, "excOffset":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "excOffset":I
    .local v1, "excOffset":I
    aget-char v3, v10, v0

    .line 873
    .local v3, "excWord":I
    move v2, v1

    .line 875
    .local v2, "excOffset2":I
    and-int/lit16 v10, v3, 0x4000

    if-eqz v10, :cond_a

    .line 877
    invoke-static/range {p4 .. p5}, Lcom/ibm/icu/impl/UCaseProps;->getCaseLocale(Lcom/ibm/icu/util/ULocale;[I)I

    move-result v5

    .line 885
    .local v5, "loc":I
    const/4 v10, 0x3

    if-ne v5, v10, :cond_5

    const/16 v10, 0x49

    if-eq p1, v10, :cond_2

    const/16 v10, 0x4a

    if-eq p1, v10, :cond_2

    const/16 v10, 0x12e

    if-ne p1, v10, :cond_3

    :cond_2
    invoke-direct {p0, p2}, Lcom/ibm/icu/impl/UCaseProps;->isFollowedByMoreAbove(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z

    move-result v10

    if-nez v10, :cond_4

    :cond_3
    const/16 v10, 0xcc

    if-eq p1, v10, :cond_4

    const/16 v10, 0xcd

    if-eq p1, v10, :cond_4

    const/16 v10, 0x128

    if-ne p1, v10, :cond_5

    .line 908
    :cond_4
    sparse-switch p1, :sswitch_data_0

    .line 928
    const/4 v4, 0x0

    goto :goto_2

    .line 910
    :sswitch_0
    const-string/jumbo v10, "i\u0307"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 911
    const/4 v4, 0x2

    goto :goto_2

    .line 913
    :sswitch_1
    const-string/jumbo v10, "j\u0307"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 914
    const/4 v4, 0x2

    goto :goto_2

    .line 916
    :sswitch_2
    const-string/jumbo v10, "\u012f\u0307"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 917
    const/4 v4, 0x2

    goto :goto_2

    .line 919
    :sswitch_3
    const-string/jumbo v10, "i\u0307\u0300"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 920
    const/4 v4, 0x3

    goto :goto_2

    .line 922
    :sswitch_4
    const-string/jumbo v10, "i\u0307\u0301"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 923
    const/4 v4, 0x3

    goto :goto_2

    .line 925
    :sswitch_5
    const-string/jumbo v10, "i\u0307\u0303"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 926
    const/4 v4, 0x3

    goto :goto_2

    .line 931
    :cond_5
    const/4 v10, 0x2

    if-ne v5, v10, :cond_6

    const/16 v10, 0x130

    if-ne p1, v10, :cond_6

    .line 939
    const/16 v4, 0x69

    goto :goto_2

    .line 940
    :cond_6
    const/4 v10, 0x2

    if-ne v5, v10, :cond_7

    const/16 v10, 0x307

    if-ne p1, v10, :cond_7

    invoke-direct {p0, p2}, Lcom/ibm/icu/impl/UCaseProps;->isPrecededBy_I(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 948
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 949
    :cond_7
    const/4 v10, 0x2

    if-ne v5, v10, :cond_8

    const/16 v10, 0x49

    if-ne p1, v10, :cond_8

    invoke-direct {p0, p2}, Lcom/ibm/icu/impl/UCaseProps;->isFollowedByDotAbove(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 956
    const/16 v4, 0x131

    goto/16 :goto_2

    .line 957
    :cond_8
    const/16 v10, 0x130

    if-ne p1, v10, :cond_9

    .line 963
    const-string/jumbo v10, "i\u0307"

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 964
    const/4 v4, 0x2

    goto/16 :goto_2

    .line 965
    :cond_9
    const/16 v10, 0x3a3

    if-ne p1, v10, :cond_b

    const/4 v10, 0x1

    invoke-direct {p0, p2, v10}, Lcom/ibm/icu/impl/UCaseProps;->isFollowedByCasedLetter(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;I)Z

    move-result v10

    if-nez v10, :cond_b

    const/4 v10, -0x1

    invoke-direct {p0, p2, v10}, Lcom/ibm/icu/impl/UCaseProps;->isFollowedByCasedLetter(Lcom/ibm/icu/impl/UCaseProps$ContextIterator;I)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 975
    const/16 v4, 0x3c2

    goto/16 :goto_2

    .line 979
    .end local v5    # "loc":I
    :cond_a
    const/4 v10, 0x7

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 980
    const/4 v10, 0x7

    invoke-direct {p0, v3, v10, v1}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValueAndOffset(III)J

    move-result-wide v8

    .line 981
    .local v8, "value":J
    long-to-int v10, v8

    and-int/lit8 v4, v10, 0xf

    .line 982
    .local v4, "full":I
    if-eqz v4, :cond_b

    .line 984
    const/16 v10, 0x20

    shr-long v10, v8, v10

    long-to-int v10, v10

    add-int/lit8 v0, v10, 0x1

    .line 987
    .end local v1    # "excOffset":I
    .restart local v0    # "excOffset":I
    new-instance v10, Ljava/lang/String;

    iget-object v11, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    invoke-direct {v10, v11, v0, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 994
    .end local v0    # "excOffset":I
    .end local v4    # "full":I
    .end local v8    # "value":J
    .restart local v1    # "excOffset":I
    :cond_b
    const/4 v10, 0x0

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 995
    const/4 v10, 0x0

    invoke-direct {p0, v3, v10, v2}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result v7

    goto/16 :goto_0

    .end local v1    # "excOffset":I
    .end local v2    # "excOffset2":I
    .end local v3    # "excWord":I
    :cond_c
    move v10, v7

    .line 999
    goto/16 :goto_1

    .line 908
    :sswitch_data_0
    .sparse-switch
        0x49 -> :sswitch_0
        0x4a -> :sswitch_1
        0xcc -> :sswitch_3
        0xcd -> :sswitch_4
        0x128 -> :sswitch_5
        0x12e -> :sswitch_2
    .end sparse-switch
.end method

.method public final toFullTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I
    .locals 7
    .param p1, "c"    # I
    .param p2, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;
    .param p3, "out"    # Ljava/lang/StringBuffer;
    .param p4, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p5, "locCache"    # [I

    .prologue
    .line 1107
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/impl/UCaseProps;->toUpperOrTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[IZ)I

    move-result v0

    return v0
.end method

.method public final toFullUpper(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I
    .locals 7
    .param p1, "c"    # I
    .param p2, "iter"    # Lcom/ibm/icu/impl/UCaseProps$ContextIterator;
    .param p3, "out"    # Ljava/lang/StringBuffer;
    .param p4, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p5, "locCache"    # [I

    .prologue
    .line 1101
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/impl/UCaseProps;->toUpperOrTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[IZ)I

    move-result v0

    return v0
.end method

.method public final tolower(I)I
    .locals 6
    .param p1, "c"    # I

    .prologue
    const/4 v5, 0x0

    .line 225
    iget-object v4, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v3

    .line 226
    .local v3, "props":I
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 227
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_0

    .line 228
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v4

    add-int/2addr p1, v4

    .line 237
    :cond_0
    :goto_0
    return p1

    .line 231
    :cond_1
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v0

    .line 232
    .local v0, "excOffset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "excOffset":I
    .local v1, "excOffset":I
    aget-char v2, v4, v0

    .line 233
    .local v2, "excWord":I
    invoke-static {v2, v5}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 234
    invoke-direct {p0, v2, v5, v1}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result p1

    goto :goto_0
.end method

.method public final totitle(I)I
    .locals 8
    .param p1, "c"    # I

    .prologue
    .line 257
    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v5

    .line 258
    .local v5, "props":I
    invoke-static {v5}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 259
    invoke-static {v5}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 260
    invoke-static {v5}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v6

    add-int/2addr p1, v6

    :cond_0
    :goto_0
    move v0, p1

    .line 275
    .end local p1    # "c":I
    .local v0, "c":I
    :goto_1
    return v0

    .line 263
    .end local v0    # "c":I
    .restart local p1    # "c":I
    :cond_1
    invoke-static {v5}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v1

    .line 264
    .local v1, "excOffset":I
    iget-object v6, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "excOffset":I
    .local v2, "excOffset":I
    aget-char v3, v6, v1

    .line 266
    .local v3, "excWord":I
    const/4 v6, 0x3

    invoke-static {v3, v6}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 267
    const/4 v4, 0x3

    .line 273
    .local v4, "index":I
    :goto_2
    invoke-direct {p0, v3, v4, v2}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result p1

    goto :goto_0

    .line 268
    .end local v4    # "index":I
    :cond_2
    const/4 v6, 0x2

    invoke-static {v3, v6}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 269
    const/4 v4, 0x2

    .line 270
    .restart local v4    # "index":I
    goto :goto_2

    .end local v4    # "index":I
    :cond_3
    move v0, p1

    .line 271
    .end local p1    # "c":I
    .restart local v0    # "c":I
    goto :goto_1
.end method

.method public final toupper(I)I
    .locals 6
    .param p1, "c"    # I

    .prologue
    const/4 v5, 0x2

    .line 241
    iget-object v4, p0, Lcom/ibm/icu/impl/UCaseProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v3

    .line 242
    .local v3, "props":I
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->propsHasException(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 243
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getTypeFromProps(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 244
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getDelta(I)I

    move-result v4

    add-int/2addr p1, v4

    .line 253
    :cond_0
    :goto_0
    return p1

    .line 247
    :cond_1
    invoke-static {v3}, Lcom/ibm/icu/impl/UCaseProps;->getExceptionsOffset(I)I

    move-result v0

    .line 248
    .local v0, "excOffset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCaseProps;->exceptions:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "excOffset":I
    .local v1, "excOffset":I
    aget-char v2, v4, v0

    .line 249
    .local v2, "excWord":I
    invoke-static {v2, v5}, Lcom/ibm/icu/impl/UCaseProps;->hasSlot(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 250
    invoke-direct {p0, v2, v5, v1}, Lcom/ibm/icu/impl/UCaseProps;->getSlotValue(III)I

    move-result p1

    goto :goto_0
.end method
