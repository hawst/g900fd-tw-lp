.class public Lcom/ibm/icu/impl/ICURWLock;
.super Ljava/lang/Object;
.source "ICURWLock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ICURWLock$1;,
        Lcom/ibm/icu/impl/ICURWLock$Stats;
    }
.end annotation


# static fields
.field private static final NOTIFY_NONE:I = 0x0

.field private static final NOTIFY_READERS:I = 0x2

.field private static final NOTIFY_WRITERS:I = 0x1


# instance fields
.field private rc:I

.field private readLock:Ljava/lang/Object;

.field private stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

.field private wrc:I

.field private writeLock:Ljava/lang/Object;

.field private wwc:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->readLock:Ljava/lang/Object;

    .line 40
    new-instance v0, Lcom/ibm/icu/impl/ICURWLock$Stats;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/ICURWLock$Stats;-><init>(Lcom/ibm/icu/impl/ICURWLock$1;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    .line 45
    return-void
.end method

.method private declared-synchronized finishRead()Z
    .locals 2

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-lez v0, :cond_1

    .line 153
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "no current reader to release"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized finishWrite()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 188
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-gez v1, :cond_2

    .line 189
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    .line 190
    iget v1, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_1

    .line 191
    const/4 v0, 0x1

    .line 195
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 192
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/ibm/icu/impl/ICURWLock;->wrc:I

    if-lez v1, :cond_0

    .line 193
    const/4 v0, 0x2

    goto :goto_0

    .line 198
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "no current writer to release"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getRead()Z
    .locals 1

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I

    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->gotRead()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 139
    :goto_0
    monitor-exit p0

    return v0

    .line 138
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wrc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wrc:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    const/4 v0, 0x0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getWrite()Z
    .locals 1

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-nez v0, :cond_0

    .line 168
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->gotWrite()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 171
    :goto_0
    monitor-exit p0

    return v0

    .line 170
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized gotRead()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 126
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    .line 127
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_rc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_rc:I

    .line 129
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_mrc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_mrc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return v2

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized gotWrite()Z
    .locals 2

    .prologue
    .line 159
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    .line 160
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wc:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized retryRead()Z
    .locals 2

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wrc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wrc:I

    .line 144
    :cond_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I

    if-nez v0, :cond_1

    .line 145
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wrc:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wrc:I

    .line 146
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->gotRead()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 148
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized retryWrite()Z
    .locals 2

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wwc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/ibm/icu/impl/ICURWLock$Stats;->_wwc:I

    .line 176
    :cond_0
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->rc:I

    if-nez v0, :cond_1

    .line 177
    iget v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICURWLock;->wwc:I

    .line 178
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->gotWrite()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 180
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public acquireRead()V
    .locals 2

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->getRead()Z

    move-result v0

    if-nez v0, :cond_1

    .line 215
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->readLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :try_start_1
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->readLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 217
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    :try_start_2
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->retryRead()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    :cond_1
    return-void

    .line 217
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 222
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public acquireWrite()V
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->getWrite()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :try_start_1
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 261
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 262
    :try_start_2
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->retryWrite()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    :cond_1
    return-void

    .line 261
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 266
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized clearStats()Lcom/ibm/icu/impl/ICURWLock$Stats;
    .locals 2

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    .line 112
    .local v0, "result":Lcom/ibm/icu/impl/ICURWLock$Stats;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    monitor-exit p0

    return-object v0

    .line 111
    .end local v0    # "result":Lcom/ibm/icu/impl/ICURWLock$Stats;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getStats()Lcom/ibm/icu/impl/ICURWLock$Stats;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/ibm/icu/impl/ICURWLock$Stats;

    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/impl/ICURWLock$Stats;-><init>(Lcom/ibm/icu/impl/ICURWLock$Stats;Lcom/ibm/icu/impl/ICURWLock$1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public releaseRead()V
    .locals 2

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->finishRead()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 240
    monitor-exit v1

    .line 242
    :cond_0
    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public releaseWrite()V
    .locals 2

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICURWLock;->finishWrite()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 296
    :goto_0
    return-void

    .line 284
    :pswitch_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->writeLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 286
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 289
    :pswitch_1
    iget-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->readLock:Ljava/lang/Object;

    monitor-enter v1

    .line 290
    :try_start_1
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->readLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 291
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 282
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized resetStats()Lcom/ibm/icu/impl/ICURWLock$Stats;
    .locals 3

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;

    .line 103
    .local v0, "result":Lcom/ibm/icu/impl/ICURWLock$Stats;
    new-instance v1, Lcom/ibm/icu/impl/ICURWLock$Stats;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/ibm/icu/impl/ICURWLock$Stats;-><init>(Lcom/ibm/icu/impl/ICURWLock$1;)V

    iput-object v1, p0, Lcom/ibm/icu/impl/ICURWLock;->stats:Lcom/ibm/icu/impl/ICURWLock$Stats;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-object v0

    .line 102
    .end local v0    # "result":Lcom/ibm/icu/impl/ICURWLock$Stats;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
