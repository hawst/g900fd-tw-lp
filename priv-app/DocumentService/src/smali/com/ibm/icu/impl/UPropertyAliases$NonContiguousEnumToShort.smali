.class Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;
.super Ljava/lang/Object;
.source "UPropertyAliases.java"

# interfaces
.implements Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UPropertyAliases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NonContiguousEnumToShort"
.end annotation


# instance fields
.field enumArray:[I

.field offsetArray:[S


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V
    .locals 4
    .param p1, "s"    # Lcom/ibm/icu/impl/ICUBinaryStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readInt()I

    move-result v0

    .line 341
    .local v0, "count":I
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->enumArray:[I

    .line 342
    new-array v2, v0, [S

    iput-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    .line 343
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 344
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->enumArray:[I

    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readInt()I

    move-result v3

    aput v3, v2, v1

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 346
    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 347
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readShort()S

    move-result v3

    aput-short v3, v2, v1

    .line 346
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 349
    :cond_1
    return-void
.end method


# virtual methods
.method public getShort(I)S
    .locals 3
    .param p1, "enumProbe"    # I

    .prologue
    .line 330
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->enumArray:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 331
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->enumArray:[I

    aget v1, v1, v0

    if-ge v1, p1, :cond_0

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->enumArray:[I

    aget v1, v1, v0

    if-le v1, p1, :cond_2

    .line 335
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Invalid enum"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    aget-short v1, v1, v0

    return v1
.end method
