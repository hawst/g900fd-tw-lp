.class final Lcom/ibm/icu/impl/NormalizerDataReader;
.super Ljava/lang/Object;
.source "NormalizerDataReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# static fields
.field private static final DATA_FORMAT_ID:[B

.field private static final DATA_FORMAT_VERSION:[B

.field private static final debug:Z


# instance fields
.field private dataInputStream:Ljava/io/DataInputStream;

.field private unicodeVersion:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 300
    const-string/jumbo v0, "NormalizerDataReader"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/NormalizerDataReader;->debug:Z

    .line 422
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_ID:[B

    .line 424
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_VERSION:[B

    return-void

    .line 422
    nop

    :array_0
    .array-data 1
        0x4et
        0x6ft
        0x72t
        0x6dt
    .end array-data

    .line 424
    :array_1
    .array-data 1
        0x2t
        0x2t
        0x5t
        0x2t
    .end array-data
.end method

.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    sget-boolean v0, Lcom/ibm/icu/impl/NormalizerDataReader;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes in inputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 311
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_ID:[B

    invoke-static {p1, v0, p0}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->unicodeVersion:[B

    .line 313
    sget-boolean v0, Lcom/ibm/icu/impl/NormalizerDataReader;->debug:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes left in inputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 315
    :cond_1
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    .line 317
    sget-boolean v0, Lcom/ibm/icu/impl/NormalizerDataReader;->debug:Z

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes left in dataInputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 318
    :cond_2
    return-void
.end method


# virtual methods
.method public getDataFormatVersion()[B
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_VERSION:[B

    return-object v0
.end method

.method public getUnicodeVersion()[B
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->unicodeVersion:[B

    return-object v0
.end method

.method public isDataVersionAcceptable([B)Z
    .locals 5
    .param p1, "version"    # [B

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 399
    aget-byte v1, p1, v0

    sget-object v2, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v0

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v3

    sget-object v2, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v3

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v4

    sget-object v2, Lcom/ibm/icu/impl/NormalizerDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected read([B[B[B[C[C[Ljava/lang/Object;)V
    .locals 9
    .param p1, "normBytes"    # [B
    .param p2, "fcdBytes"    # [B
    .param p3, "auxBytes"    # [B
    .param p4, "extraData"    # [C
    .param p5, "combiningTable"    # [C
    .param p6, "canonStartSets"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 347
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5, p1}, Ljava/io/DataInputStream;->readFully([B)V

    .line 352
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p4

    if-ge v2, v5, :cond_0

    .line 353
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, p4, v2

    .line 352
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 357
    :cond_0
    const/4 v2, 0x0

    :goto_1
    array-length v5, p5

    if-ge v2, v5, :cond_1

    .line 358
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, p5, v2

    .line 357
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 362
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5, p2}, Ljava/io/DataInputStream;->readFully([B)V

    .line 365
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5, p3}, Ljava/io/DataInputStream;->readFully([B)V

    .line 368
    const/16 v5, 0x20

    new-array v1, v5, [I

    .line 370
    .local v1, "canonStartSetsIndexes":[I
    const/4 v2, 0x0

    :goto_2
    array-length v5, v1

    if-ge v2, v5, :cond_2

    .line 371
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput v5, v1, v2

    .line 370
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 374
    :cond_2
    aget v5, v1, v6

    add-int/lit8 v5, v5, -0x20

    new-array v3, v5, [C

    .line 376
    .local v3, "startSets":[C
    const/4 v2, 0x0

    :goto_3
    array-length v5, v3

    if-ge v2, v5, :cond_3

    .line 377
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, v3, v2

    .line 376
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 379
    :cond_3
    aget v5, v1, v7

    new-array v0, v5, [C

    .line 380
    .local v0, "bmpTable":[C
    const/4 v2, 0x0

    :goto_4
    array-length v5, v0

    if-ge v2, v5, :cond_4

    .line 381
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, v0, v2

    .line 380
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 383
    :cond_4
    aget v5, v1, v8

    new-array v4, v5, [C

    .line 384
    .local v4, "suppTable":[C
    const/4 v2, 0x0

    :goto_5
    array-length v5, v4

    if-ge v2, v5, :cond_5

    .line 385
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, v4, v2

    .line 384
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 387
    :cond_5
    aput-object v1, p6, v6

    .line 388
    aput-object v3, p6, v7

    .line 389
    aput-object v0, p6, v8

    .line 390
    const/4 v5, 0x3

    aput-object v4, p6, v5

    .line 391
    return-void
.end method

.method protected readIndexes(I)[I
    .locals 3
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    new-array v1, p1, [I

    .line 325
    .local v1, "indexes":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 326
    iget-object v2, p0, Lcom/ibm/icu/impl/NormalizerDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    aput v2, v1, v0

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    :cond_0
    return-object v1
.end method
