.class public Lcom/ibm/icu/impl/IntTrieBuilder;
.super Lcom/ibm/icu/impl/TrieBuilder;
.source "IntTrieBuilder.java"


# instance fields
.field protected m_data_:[I

.field protected m_initialValue_:I

.field private m_leadUnitValue_:I


# direct methods
.method public constructor <init>(Lcom/ibm/icu/impl/IntTrieBuilder;)V
    .locals 4
    .param p1, "table"    # Lcom/ibm/icu/impl/IntTrieBuilder;

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/TrieBuilder;-><init>(Lcom/ibm/icu/impl/TrieBuilder;)V

    .line 44
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataCapacity_:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    .line 45
    iget-object v0, p1, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    iget v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    iget v0, p1, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    iput v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    .line 47
    iget v0, p1, Lcom/ibm/icu/impl/IntTrieBuilder;->m_leadUnitValue_:I

    iput v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_leadUnitValue_:I

    .line 48
    return-void
.end method

.method public constructor <init>([IIIIZ)V
    .locals 6
    .param p1, "aliasdata"    # [I
    .param p2, "maxdatalength"    # I
    .param p3, "initialvalue"    # I
    .param p4, "leadunitvalue"    # I
    .param p5, "latin1linear"    # Z

    .prologue
    const/4 v5, 0x0

    .line 61
    invoke-direct {p0}, Lcom/ibm/icu/impl/TrieBuilder;-><init>()V

    .line 62
    const/16 v3, 0x20

    if-lt p2, v3, :cond_0

    if-eqz p5, :cond_1

    const/16 v3, 0x400

    if-ge p2, v3, :cond_1

    .line 64
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Argument maxdatalength is too small"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 68
    :cond_1
    if-eqz p1, :cond_3

    .line 69
    iput-object p1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    .line 76
    :goto_0
    const/16 v2, 0x20

    .line 78
    .local v2, "j":I
    if-eqz p5, :cond_2

    .line 83
    const/4 v0, 0x0

    .line 87
    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput v2, v3, v0

    .line 88
    add-int/lit8 v2, v2, 0x20

    .line 89
    const/16 v3, 0x8

    if-lt v1, v3, :cond_4

    .line 92
    .end local v1    # "i":I
    :cond_2
    iput v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    .line 94
    iget-object v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    iget v4, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    invoke-static {v3, v5, v4, p3}, Ljava/util/Arrays;->fill([IIII)V

    .line 95
    iput p3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    .line 96
    iput p4, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_leadUnitValue_:I

    .line 97
    iput p2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataCapacity_:I

    .line 98
    iput-boolean p5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isLatin1Linear_:Z

    .line 99
    iput-boolean v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    .line 100
    return-void

    .line 72
    .end local v2    # "j":I
    :cond_3
    new-array v3, p2, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    goto :goto_0

    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_4
    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1
.end method

.method private allocDataBlock()I
    .locals 3

    .prologue
    .line 503
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    .line 504
    .local v0, "newBlock":I
    add-int/lit8 v1, v0, 0x20

    .line 505
    .local v1, "newTop":I
    iget v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataCapacity_:I

    if-le v1, v2, :cond_0

    .line 507
    const/4 v0, -0x1

    .line 510
    .end local v0    # "newBlock":I
    :goto_0
    return v0

    .line 509
    .restart local v0    # "newBlock":I
    :cond_0
    iput v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    goto :goto_0
.end method

.method private compact(Z)V
    .locals 9
    .param p1, "overlap"    # Z

    .prologue
    .line 554
    iget-boolean v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-eqz v6, :cond_0

    .line 633
    :goto_0
    return-void

    .line 560
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/impl/IntTrieBuilder;->findUnusedBlocks()V

    .line 564
    const/16 v3, 0x20

    .line 565
    .local v3, "overlapStart":I
    iget-boolean v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isLatin1Linear_:Z

    if-eqz v6, :cond_1

    .line 566
    add-int/lit16 v3, v3, 0x100

    .line 569
    :cond_1
    const/16 v1, 0x20

    .line 571
    .local v1, "newStart":I
    move v4, v1

    .local v4, "start":I
    :goto_1
    iget v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    if-ge v4, v6, :cond_9

    .line 576
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    ushr-int/lit8 v7, v4, 0x5

    aget v6, v6, v7

    if-gez v6, :cond_2

    .line 578
    add-int/lit8 v4, v4, 0x20

    .line 580
    goto :goto_1

    .line 583
    :cond_2
    if-lt v4, v3, :cond_4

    .line 584
    iget-object v7, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    if-eqz p1, :cond_3

    const/4 v6, 0x4

    :goto_2
    invoke-static {v7, v1, v4, v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->findSameDataBlock([IIII)I

    move-result v0

    .line 586
    .local v0, "i":I
    if-ltz v0, :cond_4

    .line 589
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    ushr-int/lit8 v7, v4, 0x5

    aput v0, v6, v7

    .line 591
    add-int/lit8 v4, v4, 0x20

    .line 593
    goto :goto_1

    .line 584
    .end local v0    # "i":I
    :cond_3
    const/16 v6, 0x20

    goto :goto_2

    .line 598
    :cond_4
    if-eqz p1, :cond_5

    if-lt v4, v3, :cond_5

    .line 600
    const/16 v0, 0x1c

    .line 601
    .restart local v0    # "i":I
    :goto_3
    if-lez v0, :cond_6

    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    sub-int v7, v1, v0

    invoke-static {v6, v7, v4, v0}, Lcom/ibm/icu/impl/IntTrieBuilder;->equal_int([IIII)Z

    move-result v6

    if-nez v6, :cond_6

    .line 602
    add-int/lit8 v0, v0, -0x4

    goto :goto_3

    .line 604
    .end local v0    # "i":I
    :cond_5
    const/4 v0, 0x0

    .line 606
    .restart local v0    # "i":I
    :cond_6
    if-lez v0, :cond_7

    .line 608
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    ushr-int/lit8 v7, v4, 0x5

    sub-int v8, v1, v0

    aput v8, v6, v7

    .line 610
    add-int/2addr v4, v0

    .line 611
    rsub-int/lit8 v0, v0, 0x20

    move v5, v4

    .end local v4    # "start":I
    .local v5, "start":I
    move v2, v1

    .end local v1    # "newStart":I
    .local v2, "newStart":I
    :goto_4
    if-lez v0, :cond_b

    .line 612
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "newStart":I
    .restart local v1    # "newStart":I
    iget-object v7, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "start":I
    .restart local v4    # "start":I
    aget v7, v7, v5

    aput v7, v6, v2

    .line 611
    add-int/lit8 v0, v0, -0x1

    move v5, v4

    .end local v4    # "start":I
    .restart local v5    # "start":I
    move v2, v1

    .end local v1    # "newStart":I
    .restart local v2    # "newStart":I
    goto :goto_4

    .line 615
    .end local v2    # "newStart":I
    .end local v5    # "start":I
    .restart local v1    # "newStart":I
    .restart local v4    # "start":I
    :cond_7
    if-ge v1, v4, :cond_8

    .line 617
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    ushr-int/lit8 v7, v4, 0x5

    aput v1, v6, v7

    .line 618
    const/16 v0, 0x20

    move v5, v4

    .end local v4    # "start":I
    .restart local v5    # "start":I
    move v2, v1

    .end local v1    # "newStart":I
    .restart local v2    # "newStart":I
    :goto_5
    if-lez v0, :cond_b

    .line 619
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "newStart":I
    .restart local v1    # "newStart":I
    iget-object v7, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "start":I
    .restart local v4    # "start":I
    aget v7, v7, v5

    aput v7, v6, v2

    .line 618
    add-int/lit8 v0, v0, -0x1

    move v5, v4

    .end local v4    # "start":I
    .restart local v5    # "start":I
    move v2, v1

    .end local v1    # "newStart":I
    .restart local v2    # "newStart":I
    goto :goto_5

    .line 623
    .end local v2    # "newStart":I
    .end local v5    # "start":I
    .restart local v1    # "newStart":I
    .restart local v4    # "start":I
    :cond_8
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    ushr-int/lit8 v7, v4, 0x5

    aput v4, v6, v7

    .line 624
    add-int/lit8 v1, v1, 0x20

    .line 625
    move v4, v1

    .line 627
    goto/16 :goto_1

    .line 629
    .end local v0    # "i":I
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_6
    iget v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    if-ge v0, v6, :cond_a

    .line 630
    iget-object v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    iget-object v7, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_map_:[I

    iget-object v8, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aget v8, v8, v0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    ushr-int/lit8 v8, v8, 0x5

    aget v7, v7, v8

    aput v7, v6, v0

    .line 629
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 632
    :cond_a
    iput v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    goto/16 :goto_0

    .end local v1    # "newStart":I
    .end local v4    # "start":I
    .restart local v2    # "newStart":I
    .restart local v5    # "start":I
    :cond_b
    move v4, v5

    .end local v5    # "start":I
    .restart local v4    # "start":I
    move v1, v2

    .end local v2    # "newStart":I
    .restart local v1    # "newStart":I
    goto/16 :goto_1
.end method

.method private fillBlock(IIIIZ)V
    .locals 3
    .param p1, "block"    # I
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "value"    # I
    .param p5, "overwrite"    # Z

    .prologue
    .line 774
    add-int/2addr p3, p1

    .line 775
    add-int/2addr p1, p2

    .line 776
    if-eqz p5, :cond_0

    move v0, p1

    .line 777
    .end local p1    # "block":I
    .local v0, "block":I
    :goto_0
    if-ge v0, p3, :cond_2

    .line 778
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    add-int/lit8 p1, v0, 0x1

    .end local v0    # "block":I
    .restart local p1    # "block":I
    aput p4, v1, v0

    move v0, p1

    .line 779
    .end local p1    # "block":I
    .restart local v0    # "block":I
    goto :goto_0

    .line 782
    .end local v0    # "block":I
    .restart local p1    # "block":I
    :cond_0
    :goto_1
    if-ge p1, p3, :cond_3

    .line 783
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    aget v1, v1, p1

    iget v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    if-ne v1, v2, :cond_1

    .line 784
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    aput p4, v1, p1

    .line 786
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 787
    goto :goto_1

    .end local p1    # "block":I
    .restart local v0    # "block":I
    :cond_2
    move p1, v0

    .line 789
    .end local v0    # "block":I
    .restart local p1    # "block":I
    :cond_3
    return-void
.end method

.method private static final findSameDataBlock([IIII)I
    .locals 2
    .param p0, "data"    # [I
    .param p1, "dataLength"    # I
    .param p2, "otherBlock"    # I
    .param p3, "step"    # I

    .prologue
    .line 646
    add-int/lit8 p1, p1, -0x20

    .line 648
    const/4 v0, 0x0

    .local v0, "block":I
    :goto_0
    if-gt v0, p1, :cond_1

    .line 649
    const/16 v1, 0x20

    invoke-static {p0, v0, p2, v1}, Lcom/ibm/icu/impl/IntTrieBuilder;->equal_int([IIII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653
    .end local v0    # "block":I
    :goto_1
    return v0

    .line 648
    .restart local v0    # "block":I
    :cond_0
    add-int/2addr v0, p3

    goto :goto_0

    .line 653
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private final fold(Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;)V
    .locals 12
    .param p1, "manipulate"    # Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;

    .prologue
    const/16 v11, 0x800

    const/4 v2, 0x0

    const/16 v3, 0x20

    .line 669
    new-array v9, v3, [I

    .line 670
    .local v9, "leadIndexes":[I
    iget-object v7, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    .line 672
    .local v7, "index":[I
    const/16 v0, 0x6c0

    invoke-static {v7, v0, v9, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 682
    const/4 v1, 0x0

    .line 683
    .local v1, "block":I
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_leadUnitValue_:I

    iget v4, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    if-ne v0, v4, :cond_0

    .line 698
    :goto_0
    const/16 v6, 0x6c0

    .local v6, "c":I
    :goto_1
    const/16 v0, 0x6e0

    if-ge v6, v0, :cond_2

    .line 699
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aput v1, v0, v6

    .line 698
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 689
    .end local v6    # "c":I
    :cond_0
    invoke-direct {p0}, Lcom/ibm/icu/impl/IntTrieBuilder;->allocDataBlock()I

    move-result v1

    .line 690
    if-gez v1, :cond_1

    .line 692
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Internal error: Out of memory space"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 694
    :cond_1
    iget v4, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_leadUnitValue_:I

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    .line 696
    neg-int v1, v1

    goto :goto_0

    .line 709
    .restart local v6    # "c":I
    :cond_2
    const/16 v8, 0x800

    .line 711
    .local v8, "indexLength":I
    const/high16 v6, 0x10000

    :goto_2
    const/high16 v0, 0x110000

    if-ge v6, v0, :cond_6

    .line 712
    shr-int/lit8 v0, v6, 0x5

    aget v0, v7, v0

    if-eqz v0, :cond_5

    .line 714
    and-int/lit16 v6, v6, -0x400

    .line 716
    shr-int/lit8 v0, v6, 0x5

    invoke-static {v7, v8, v0}, Lcom/ibm/icu/impl/IntTrieBuilder;->findSameIndexBlock([III)I

    move-result v1

    .line 722
    add-int/lit8 v0, v1, 0x20

    invoke-interface {p1, v6, v0}, Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;->getFoldedValue(II)I

    move-result v10

    .line 724
    .local v10, "value":I
    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v0

    if-eq v10, v0, :cond_4

    .line 725
    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v0

    invoke-virtual {p0, v0, v10}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 727
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string/jumbo v2, "Data table overflow"

    invoke-direct {v0, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_3
    if-ne v1, v8, :cond_4

    .line 734
    shr-int/lit8 v0, v6, 0x5

    invoke-static {v7, v0, v7, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 736
    add-int/lit8 v8, v8, 0x20

    .line 739
    :cond_4
    add-int/lit16 v6, v6, 0x400

    .line 740
    goto :goto_2

    .line 742
    .end local v10    # "value":I
    :cond_5
    add-int/lit8 v6, v6, 0x20

    .line 744
    goto :goto_2

    .line 754
    :cond_6
    const v0, 0x8800

    if-lt v8, v0, :cond_7

    .line 755
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string/jumbo v2, "Index table overflow"

    invoke-direct {v0, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 759
    :cond_7
    const/16 v0, 0x820

    add-int/lit16 v4, v8, -0x800

    invoke-static {v7, v11, v7, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 762
    invoke-static {v9, v2, v7, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 764
    add-int/lit8 v8, v8, 0x20

    .line 765
    iput v8, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    .line 766
    return-void
.end method

.method private getDataBlock(I)I
    .locals 6
    .param p1, "ch"    # I

    .prologue
    .line 520
    shr-int/lit8 p1, p1, 0x5

    .line 521
    iget-object v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aget v0, v2, p1

    .line 522
    .local v0, "indexValue":I
    if-lez v0, :cond_0

    .line 537
    .end local v0    # "indexValue":I
    :goto_0
    return v0

    .line 527
    .restart local v0    # "indexValue":I
    :cond_0
    invoke-direct {p0}, Lcom/ibm/icu/impl/IntTrieBuilder;->allocDataBlock()I

    move-result v1

    .line 528
    .local v1, "newBlock":I
    if-gez v1, :cond_1

    .line 530
    const/4 v0, -0x1

    goto :goto_0

    .line 532
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aput v1, v2, p1

    .line 535
    iget-object v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v4, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    const/16 v5, 0x80

    invoke-static {v2, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v1

    .line 537
    goto :goto_0
.end method


# virtual methods
.method public getValue(I)I
    .locals 4
    .param p1, "ch"    # I

    .prologue
    .line 170
    iget-boolean v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v1, :cond_0

    const v1, 0x10ffff

    if-gt p1, v1, :cond_0

    if-gez p1, :cond_1

    .line 171
    :cond_0
    const/4 v1, 0x0

    .line 175
    :goto_0
    return v1

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    shr-int/lit8 v2, p1, 0x5

    aget v0, v1, v2

    .line 175
    .local v0, "block":I
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    and-int/lit8 v3, p1, 0x1f

    add-int/2addr v2, v3

    aget v1, v1, v2

    goto :goto_0
.end method

.method public getValue(I[Z)I
    .locals 5
    .param p1, "ch"    # I
    .param p2, "inBlockZero"    # [Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 188
    iget-boolean v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v3, :cond_0

    const v3, 0x10ffff

    if-gt p1, v3, :cond_0

    if-gez p1, :cond_2

    .line 189
    :cond_0
    if-eqz p2, :cond_1

    .line 190
    aput-boolean v1, p2, v2

    .line 199
    :cond_1
    :goto_0
    return v2

    .line 195
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    shr-int/lit8 v4, p1, 0x5

    aget v0, v3, v4

    .line 196
    .local v0, "block":I
    if-eqz p2, :cond_3

    .line 197
    if-nez v0, :cond_4

    :goto_1
    aput-boolean v1, p2, v2

    .line 199
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    and-int/lit8 v3, p1, 0x1f

    add-int/2addr v2, v3

    aget v2, v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    .line 197
    goto :goto_1
.end method

.method public serialize(Ljava/io/OutputStream;ZLcom/ibm/icu/impl/TrieBuilder$DataManipulate;)I
    .locals 7
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "reduceTo16Bits"    # Z
    .param p3, "datamanipulate"    # Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 294
    if-nez p3, :cond_0

    .line 295
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "Parameters can not be null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 300
    :cond_0
    iget-boolean v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v5, :cond_1

    .line 302
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/ibm/icu/impl/IntTrieBuilder;->compact(Z)V

    .line 304
    invoke-direct {p0, p3}, Lcom/ibm/icu/impl/IntTrieBuilder;->fold(Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;)V

    .line 306
    invoke-direct {p0, v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->compact(Z)V

    .line 307
    iput-boolean v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    .line 312
    :cond_1
    if-eqz p2, :cond_2

    .line 313
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    iget v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    add-int v2, v5, v6

    .line 317
    .local v2, "length":I
    :goto_0
    const/high16 v5, 0x40000

    if-lt v2, v5, :cond_3

    .line 318
    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string/jumbo v6, "Data length too small"

    invoke-direct {v5, v6}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 315
    .end local v2    # "length":I
    :cond_2
    iget v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    .restart local v2    # "length":I
    goto :goto_0

    .line 326
    :cond_3
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    mul-int/lit8 v5, v5, 0x2

    add-int/lit8 v2, v5, 0x10

    .line 327
    if-eqz p2, :cond_5

    .line 328
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    .line 333
    :goto_1
    if-nez p1, :cond_6

    .line 379
    :cond_4
    return v2

    .line 330
    :cond_5
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    mul-int/lit8 v5, v5, 0x4

    add-int/2addr v2, v5

    goto :goto_1

    .line 338
    :cond_6
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 339
    .local v0, "dos":Ljava/io/DataOutputStream;
    const v5, 0x54726965

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 341
    const/16 v3, 0x25

    .line 342
    .local v3, "options":I
    if-nez p2, :cond_7

    .line 343
    or-int/lit16 v3, v3, 0x100

    .line 345
    :cond_7
    iget-boolean v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isLatin1Linear_:Z

    if-eqz v5, :cond_8

    .line 346
    or-int/lit16 v3, v3, 0x200

    .line 348
    :cond_8
    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 350
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 351
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 354
    if-eqz p2, :cond_a

    .line 356
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    if-ge v1, v5, :cond_9

    .line 357
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aget v5, v5, v1

    iget v6, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    add-int/2addr v5, v6

    ushr-int/lit8 v4, v5, 0x2

    .line 358
    .local v4, "v":I
    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeChar(I)V

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 362
    .end local v4    # "v":I
    :cond_9
    const/4 v1, 0x0

    :goto_3
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    if-ge v1, v5, :cond_4

    .line 363
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    aget v5, v5, v1

    const v6, 0xffff

    and-int v4, v5, v6

    .line 364
    .restart local v4    # "v":I
    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeChar(I)V

    .line 362
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 368
    .end local v1    # "i":I
    .end local v4    # "v":I
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    if-ge v1, v5, :cond_b

    .line 369
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aget v5, v5, v1

    ushr-int/lit8 v4, v5, 0x2

    .line 370
    .restart local v4    # "v":I
    invoke-virtual {v0, v4}, Ljava/io/DataOutputStream;->writeChar(I)V

    .line 368
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 374
    .end local v4    # "v":I
    :cond_b
    const/4 v1, 0x0

    :goto_5
    iget v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    if-ge v1, v5, :cond_4

    .line 375
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    aget v5, v5, v1

    invoke-virtual {v0, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 374
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method

.method public serialize(Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;Lcom/ibm/icu/impl/Trie$DataManipulate;)Lcom/ibm/icu/impl/IntTrie;
    .locals 7
    .param p1, "datamanipulate"    # Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;
    .param p2, "triedatamanipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 235
    if-nez p1, :cond_0

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Parameters can not be null"

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    iget-boolean v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v0, :cond_1

    .line 242
    invoke-direct {p0, v5}, Lcom/ibm/icu/impl/IntTrieBuilder;->compact(Z)V

    .line 244
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->fold(Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;)V

    .line 246
    invoke-direct {p0, v3}, Lcom/ibm/icu/impl/IntTrieBuilder;->compact(Z)V

    .line 247
    iput-boolean v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    .line 250
    :cond_1
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    const/high16 v3, 0x40000

    if-lt v0, v3, :cond_2

    .line 251
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string/jumbo v3, "Data length too small"

    invoke-direct {v0, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_2
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    new-array v1, v0, [C

    .line 255
    .local v1, "index":[C
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    new-array v2, v0, [I

    .line 258
    .local v2, "data":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_indexLength_:I

    if-ge v6, v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    aget v0, v0, v6

    ushr-int/lit8 v0, v0, 0x2

    int-to-char v0, v0

    aput-char v0, v1, v6

    .line 258
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    iget v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_dataLength_:I

    invoke-static {v0, v5, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 264
    const/16 v4, 0x25

    .line 265
    .local v4, "options":I
    or-int/lit16 v4, v4, 0x100

    .line 266
    iget-boolean v0, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isLatin1Linear_:Z

    if-eqz v0, :cond_4

    .line 267
    or-int/lit16 v4, v4, 0x200

    .line 269
    :cond_4
    new-instance v0, Lcom/ibm/icu/impl/IntTrie;

    iget v3, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/IntTrie;-><init>([C[IIILcom/ibm/icu/impl/Trie$DataManipulate;)V

    return-object v0
.end method

.method public setRange(IIIZ)Z
    .locals 14
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "value"    # I
    .param p4, "overwrite"    # Z

    .prologue
    .line 404
    iget-boolean v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v1, :cond_0

    if-ltz p1, :cond_0

    const v1, 0x10ffff

    if-gt p1, v1, :cond_0

    if-ltz p2, :cond_0

    const/high16 v1, 0x110000

    move/from16 v0, p2

    if-gt v0, v1, :cond_0

    move/from16 v0, p2

    if-le p1, v0, :cond_1

    .line 407
    :cond_0
    const/4 v1, 0x0

    .line 487
    :goto_0
    return v1

    .line 410
    :cond_1
    move/from16 v0, p2

    if-ne p1, v0, :cond_2

    .line 411
    const/4 v1, 0x1

    goto :goto_0

    .line 414
    :cond_2
    and-int/lit8 v1, p1, 0x1f

    if-eqz v1, :cond_4

    .line 416
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getDataBlock(I)I

    move-result v2

    .line 417
    .local v2, "block":I
    if-gez v2, :cond_3

    .line 418
    const/4 v1, 0x0

    goto :goto_0

    .line 421
    :cond_3
    add-int/lit8 v1, p1, 0x20

    and-int/lit8 v11, v1, -0x20

    .line 422
    .local v11, "nextStart":I
    move/from16 v0, p2

    if-gt v11, v0, :cond_5

    .line 423
    and-int/lit8 v3, p1, 0x1f

    const/16 v4, 0x20

    move-object v1, p0

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    .line 425
    move p1, v11

    .line 435
    .end local v2    # "block":I
    .end local v11    # "nextStart":I
    :cond_4
    and-int/lit8 v13, p2, 0x1f

    .line 438
    .local v13, "rest":I
    and-int/lit8 p2, p2, -0x20

    .line 441
    const/4 v4, 0x0

    .line 442
    .local v4, "repeatBlock":I
    iget v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_initialValue_:I

    move/from16 v0, p3

    if-ne v0, v1, :cond_6

    move v12, v4

    .line 448
    .end local v4    # "repeatBlock":I
    .local v12, "repeatBlock":I
    :goto_1
    move/from16 v0, p2

    if-ge p1, v0, :cond_b

    .line 450
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    shr-int/lit8 v3, p1, 0x5

    aget v2, v1, v3

    .line 451
    .restart local v2    # "block":I
    if-lez v2, :cond_7

    .line 453
    const/4 v3, 0x0

    const/16 v4, 0x20

    move-object v1, p0

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    move v4, v12

    .line 475
    .end local v12    # "repeatBlock":I
    .restart local v4    # "repeatBlock":I
    :goto_2
    add-int/lit8 p1, p1, 0x20

    move v12, v4

    .line 476
    .end local v4    # "repeatBlock":I
    .restart local v12    # "repeatBlock":I
    goto :goto_1

    .line 428
    .end local v12    # "repeatBlock":I
    .end local v13    # "rest":I
    .restart local v11    # "nextStart":I
    :cond_5
    and-int/lit8 v3, p1, 0x1f

    and-int/lit8 v4, p2, 0x1f

    move-object v1, p0

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    .line 430
    const/4 v1, 0x1

    goto :goto_0

    .line 446
    .end local v2    # "block":I
    .end local v11    # "nextStart":I
    .restart local v4    # "repeatBlock":I
    .restart local v13    # "rest":I
    :cond_6
    const/4 v4, -0x1

    move v12, v4

    .end local v4    # "repeatBlock":I
    .restart local v12    # "repeatBlock":I
    goto :goto_1

    .line 455
    .restart local v2    # "block":I
    :cond_7
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    neg-int v3, v2

    aget v1, v1, v3

    move/from16 v0, p3

    if-eq v1, v0, :cond_e

    if-eqz v2, :cond_8

    if-eqz p4, :cond_e

    .line 458
    :cond_8
    if-ltz v12, :cond_9

    .line 459
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    shr-int/lit8 v3, p1, 0x5

    neg-int v5, v12

    aput v5, v1, v3

    move v4, v12

    .line 460
    .end local v12    # "repeatBlock":I
    .restart local v4    # "repeatBlock":I
    goto :goto_2

    .line 463
    .end local v4    # "repeatBlock":I
    .restart local v12    # "repeatBlock":I
    :cond_9
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getDataBlock(I)I

    move-result v4

    .line 464
    .end local v12    # "repeatBlock":I
    .restart local v4    # "repeatBlock":I
    if-gez v4, :cond_a

    .line 465
    const/4 v1, 0x0

    goto :goto_0

    .line 470
    :cond_a
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_index_:[I

    shr-int/lit8 v3, p1, 0x5

    neg-int v5, v4

    aput v5, v1, v3

    .line 471
    const/4 v5, 0x0

    const/16 v6, 0x20

    const/4 v8, 0x1

    move-object v3, p0

    move/from16 v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    goto :goto_2

    .line 478
    .end local v2    # "block":I
    .end local v4    # "repeatBlock":I
    .restart local v12    # "repeatBlock":I
    :cond_b
    if-lez v13, :cond_d

    .line 480
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getDataBlock(I)I

    move-result v2

    .line 481
    .restart local v2    # "block":I
    if-gez v2, :cond_c

    .line 482
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 484
    :cond_c
    const/4 v7, 0x0

    move-object v5, p0

    move v6, v2

    move v8, v13

    move/from16 v9, p3

    move/from16 v10, p4

    invoke-direct/range {v5 .. v10}, Lcom/ibm/icu/impl/IntTrieBuilder;->fillBlock(IIIIZ)V

    .line 487
    .end local v2    # "block":I
    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_0

    .restart local v2    # "block":I
    :cond_e
    move v4, v12

    .end local v12    # "repeatBlock":I
    .restart local v4    # "repeatBlock":I
    goto :goto_2
.end method

.method public setValue(II)Z
    .locals 3
    .param p1, "ch"    # I
    .param p2, "value"    # I

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-boolean v2, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_isCompacted_:Z

    if-nez v2, :cond_0

    const v2, 0x10ffff

    if-gt p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v1

    .line 217
    :cond_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getDataBlock(I)I

    move-result v0

    .line 218
    .local v0, "block":I
    if-ltz v0, :cond_0

    .line 222
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrieBuilder;->m_data_:[I

    and-int/lit8 v2, p1, 0x1f

    add-int/2addr v2, v0

    aput p2, v1, v2

    .line 223
    const/4 v1, 0x1

    goto :goto_0
.end method
