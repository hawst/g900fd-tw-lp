.class public Lcom/ibm/icu/impl/TimeZoneAdapter;
.super Ljava/util/TimeZone;
.source "TimeZoneAdapter.java"


# static fields
.field static final serialVersionUID:J = -0x1c4fcadfcee9017dL


# instance fields
.field private zone:Lcom/ibm/icu/util/TimeZone;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/util/TimeZone;)V
    .locals 1
    .param p1, "zone"    # Lcom/ibm/icu/util/TimeZone;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/util/TimeZone;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    .line 60
    invoke-virtual {p1}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Ljava/util/TimeZone;->setID(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public static wrap(Lcom/ibm/icu/util/TimeZone;)Ljava/util/TimeZone;
    .locals 1
    .param p0, "tz"    # Lcom/ibm/icu/util/TimeZone;

    .prologue
    .line 45
    new-instance v0, Lcom/ibm/icu/impl/TimeZoneAdapter;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/TimeZoneAdapter;-><init>(Lcom/ibm/icu/util/TimeZone;)V

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 119
    new-instance v1, Lcom/ibm/icu/impl/TimeZoneAdapter;

    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/util/TimeZone;

    invoke-direct {v1, v0}, Lcom/ibm/icu/impl/TimeZoneAdapter;-><init>(Lcom/ibm/icu/util/TimeZone;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 133
    instance-of v0, p1, Lcom/ibm/icu/impl/TimeZoneAdapter;

    if-eqz v0, :cond_0

    .line 134
    check-cast p1, Lcom/ibm/icu/impl/TimeZoneAdapter;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object p1, p1, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/TimeZone;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getOffset(IIIIII)I
    .locals 7
    .param p1, "era"    # I
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I
    .param p5, "dayOfWeek"    # I
    .param p6, "millis"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/util/TimeZone;->getOffset(IIIIII)I

    move-result v0

    return v0
.end method

.method public getRawOffset()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getRawOffset()I

    move-result v0

    return v0
.end method

.method public hasSameRules(Ljava/util/TimeZone;)Z
    .locals 2
    .param p1, "other"    # Ljava/util/TimeZone;

    .prologue
    .line 75
    instance-of v0, p1, Lcom/ibm/icu/impl/TimeZoneAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    check-cast p1, Lcom/ibm/icu/impl/TimeZoneAdapter;

    .end local p1    # "other":Ljava/util/TimeZone;
    iget-object v1, p1, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/TimeZone;->hasSameRules(Lcom/ibm/icu/util/TimeZone;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized hashCode()I
    .locals 1

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->hashCode()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public inDaylightTime(Ljava/util/Date;)Z
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-super {p0, p1}, Ljava/util/TimeZone;->setID(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/TimeZone;->setID(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public setRawOffset(I)V
    .locals 1
    .param p1, "offsetMillis"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/TimeZone;->setRawOffset(I)V

    .line 99
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "TimeZoneAdapter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unwrap()Lcom/ibm/icu/util/TimeZone;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    return-object v0
.end method

.method public useDaylightTime()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/ibm/icu/impl/TimeZoneAdapter;->zone:Lcom/ibm/icu/util/TimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->useDaylightTime()Z

    move-result v0

    return v0
.end method
