.class public final Lcom/ibm/icu/impl/UBiDiProps;
.super Ljava/lang/Object;
.source "UBiDiProps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/UBiDiProps$1;,
        Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;
    }
.end annotation


# static fields
.field private static final BIDI_CONTROL_SHIFT:I = 0xb

.field private static final CLASS_MASK:I = 0x1f

.field private static final DATA_FILE_NAME:Ljava/lang/String; = "ubidi.icu"

.field private static final DATA_NAME:Ljava/lang/String; = "ubidi"

.field private static final DATA_TYPE:Ljava/lang/String; = "icu"

.field private static final ESC_MIRROR_DELTA:I = -0x4

.field private static final FMT:[B

.field private static final IS_MIRRORED_SHIFT:I = 0xc

.field private static final IX_INDEX_TOP:I = 0x0

.field private static final IX_JG_LIMIT:I = 0x5

.field private static final IX_JG_START:I = 0x4

.field private static final IX_MAX_VALUES:I = 0xf

.field private static final IX_MIRROR_LENGTH:I = 0x3

.field private static final IX_TOP:I = 0x10

.field private static final JOIN_CONTROL_SHIFT:I = 0xa

.field private static final JT_MASK:I = 0xe0

.field private static final JT_SHIFT:I = 0x5

.field private static final MAX_JG_MASK:I = 0xff0000

.field private static final MAX_JG_SHIFT:I = 0x10

.field private static final MIRROR_DELTA_SHIFT:I = 0xd

.field private static final MIRROR_INDEX_SHIFT:I = 0x15

.field private static gBdp:Lcom/ibm/icu/impl/UBiDiProps;

.field private static gBdpDummy:Lcom/ibm/icu/impl/UBiDiProps;


# instance fields
.field private indexes:[I

.field private jgArray:[B

.field private mirrors:[I

.field private trie:Lcom/ibm/icu/impl/CharTrie;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    sput-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    .line 106
    sput-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdpDummy:Lcom/ibm/icu/impl/UBiDiProps;

    .line 269
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/UBiDiProps;->FMT:[B

    return-void

    :array_0
    .array-data 1
        0x42t
        0x69t
        0x44t
        0x69t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string/jumbo v2, "data/icudt40b/ubidi.icu"

    invoke-static {v2}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 40
    .local v1, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 41
    .local v0, "b":Ljava/io/BufferedInputStream;
    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/UBiDiProps;->readData(Ljava/io/InputStream;)V

    .line 42
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 43
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 45
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 3
    .param p1, "makeDummy"    # Z

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    .line 110
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    aput v1, v0, v2

    .line 111
    new-instance v0, Lcom/ibm/icu/impl/CharTrie;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/ibm/icu/impl/CharTrie;-><init>(IILcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    .line 112
    return-void
.end method

.method private static final getClassFromProps(I)I
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 304
    and-int/lit8 v0, p0, 0x1f

    return v0
.end method

.method public static final declared-synchronized getDummy()Lcom/ibm/icu/impl/UBiDiProps;
    .locals 3

    .prologue
    .line 121
    const-class v1, Lcom/ibm/icu/impl/UBiDiProps;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdpDummy:Lcom/ibm/icu/impl/UBiDiProps;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/ibm/icu/impl/UBiDiProps;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/ibm/icu/impl/UBiDiProps;-><init>(Z)V

    sput-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdpDummy:Lcom/ibm/icu/impl/UBiDiProps;

    .line 124
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdpDummy:Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final getFlagFromProps(II)Z
    .locals 1
    .param p0, "props"    # I
    .param p1, "shift"    # I

    .prologue
    .line 307
    shr-int v0, p0, p1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final getMirrorCodePoint(I)I
    .locals 1
    .param p0, "m"    # I

    .prologue
    .line 321
    const v0, 0x1fffff

    and-int/2addr v0, p0

    return v0
.end method

.method private static final getMirrorIndex(I)I
    .locals 1
    .param p0, "m"    # I

    .prologue
    .line 324
    ushr-int/lit8 v0, p0, 0x15

    return v0
.end method

.method public static final declared-synchronized getSingleton()Lcom/ibm/icu/impl/UBiDiProps;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    const-class v1, Lcom/ibm/icu/impl/UBiDiProps;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/ibm/icu/impl/UBiDiProps;

    invoke-direct {v0}, Lcom/ibm/icu/impl/UBiDiProps;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    .line 102
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/UBiDiProps;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private readData(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 48
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 51
    .local v2, "inputStream":Ljava/io/DataInputStream;
    sget-object v3, Lcom/ibm/icu/impl/UBiDiProps;->FMT:[B

    new-instance v4, Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;

    invoke-direct {v4, p0, v5}, Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;-><init>(Lcom/ibm/icu/impl/UBiDiProps;Lcom/ibm/icu/impl/UBiDiProps$1;)V

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    .line 55
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 56
    .local v0, "count":I
    if-gez v0, :cond_0

    .line 57
    new-instance v3, Ljava/io/IOException;

    const-string/jumbo v4, "indexes[0] too small in ubidi.icu"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 59
    :cond_0
    new-array v3, v0, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    .line 61
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v4, 0x0

    aput v0, v3, v4

    .line 62
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 63
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    aput v4, v3, v1

    .line 62
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_1
    new-instance v3, Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v3, v2, v5}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    .line 70
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v4, 0x3

    aget v0, v3, v4

    .line 71
    if-lez v0, :cond_2

    .line 72
    new-array v3, v0, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->mirrors:[I

    .line 73
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    .line 74
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->mirrors:[I

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    aput v4, v3, v1

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v4, 0x5

    aget v3, v3, v4

    iget-object v4, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v5, 0x4

    aget v4, v4, v5

    sub-int v0, v3, v4

    .line 80
    new-array v3, v0, [B

    iput-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->jgArray:[B

    .line 81
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_3

    .line 82
    iget-object v3, p0, Lcom/ibm/icu/impl/UBiDiProps;->jgArray:[B

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    aput-byte v4, v3, v1

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 84
    :cond_3
    return-void
.end method


# virtual methods
.method public final addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 11
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 136
    new-instance v3, Lcom/ibm/icu/impl/TrieIterator;

    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v3, v9}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 137
    .local v3, "iter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v1, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v1}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 139
    .local v1, "element":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    invoke-virtual {v3, v1}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 140
    iget v9, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p1, v9}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 144
    :cond_0
    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v10, 0x3

    aget v5, v9, v10

    .line 145
    .local v5, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_1

    .line 146
    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->mirrors:[I

    aget v9, v9, v2

    invoke-static {v9}, Lcom/ibm/icu/impl/UBiDiProps;->getMirrorCodePoint(I)I

    move-result v0

    .line 147
    .local v0, "c":I
    add-int/lit8 v9, v0, 0x1

    invoke-virtual {p1, v0, v9}, Lcom/ibm/icu/text/UnicodeSet;->add(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 145
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 151
    .end local v0    # "c":I
    :cond_1
    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v10, 0x4

    aget v8, v9, v10

    .line 152
    .local v8, "start":I
    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v10, 0x5

    aget v6, v9, v10

    .line 153
    .local v6, "limit":I
    sub-int v5, v6, v8

    .line 154
    const/4 v7, 0x0

    .line 155
    .local v7, "prev":B
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_3

    .line 156
    iget-object v9, p0, Lcom/ibm/icu/impl/UBiDiProps;->jgArray:[B

    aget-byte v4, v9, v2

    .line 157
    .local v4, "jg":B
    if-eq v4, v7, :cond_2

    .line 158
    invoke-virtual {p1, v8}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 159
    move v7, v4

    .line 161
    :cond_2
    add-int/lit8 v8, v8, 0x1

    .line 155
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 163
    .end local v4    # "jg":B
    :cond_3
    if-eqz v7, :cond_4

    .line 165
    invoke-virtual {p1, v6}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 171
    :cond_4
    return-void
.end method

.method public final getClass(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/impl/UBiDiProps;->getClassFromProps(I)I

    move-result v0

    return v0
.end method

.method public final getJoiningGroup(I)I
    .locals 4
    .param p1, "c"    # I

    .prologue
    .line 247
    iget-object v2, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v3, 0x4

    aget v1, v2, v3

    .line 248
    .local v1, "start":I
    iget-object v2, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v3, 0x5

    aget v0, v2, v3

    .line 249
    .local v0, "limit":I
    if-gt v1, p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 250
    iget-object v2, p0, Lcom/ibm/icu/impl/UBiDiProps;->jgArray:[B

    sub-int v3, p1, v1

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    .line 252
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getJoiningType(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 241
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    and-int/lit16 v0, v0, 0xe0

    shr-int/lit8 v0, v0, 0x5

    return v0
.end method

.method public final getMaxValue(I)I
    .locals 3
    .param p1, "which"    # I

    .prologue
    .line 178
    iget-object v1, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/16 v2, 0xf

    aget v0, v1, v2

    .line 179
    .local v0, "max":I
    sparse-switch p1, :sswitch_data_0

    .line 187
    const/4 v1, -0x1

    :goto_0
    return v1

    .line 181
    :sswitch_0
    and-int/lit8 v1, v0, 0x1f

    goto :goto_0

    .line 183
    :sswitch_1
    const/high16 v1, 0xff0000

    and-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x10

    goto :goto_0

    .line 185
    :sswitch_2
    and-int/lit16 v1, v0, 0xe0

    shr-int/lit8 v1, v1, 0x5

    goto :goto_0

    .line 179
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x1006 -> :sswitch_1
        0x1007 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getMirror(I)I
    .locals 8
    .param p1, "c"    # I

    .prologue
    .line 203
    iget-object v6, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v5

    .line 204
    .local v5, "props":I
    int-to-short v6, v5

    shr-int/lit8 v1, v6, 0xd

    .line 205
    .local v1, "delta":I
    const/4 v6, -0x4

    if-eq v1, v6, :cond_1

    .line 206
    add-int/2addr p1, v1

    .line 228
    .end local p1    # "c":I
    :cond_0
    :goto_0
    return p1

    .line 213
    .restart local p1    # "c":I
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/impl/UBiDiProps;->indexes:[I

    const/4 v7, 0x3

    aget v3, v6, v7

    .line 216
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_0

    .line 217
    iget-object v6, p0, Lcom/ibm/icu/impl/UBiDiProps;->mirrors:[I

    aget v4, v6, v2

    .line 218
    .local v4, "m":I
    invoke-static {v4}, Lcom/ibm/icu/impl/UBiDiProps;->getMirrorCodePoint(I)I

    move-result v0

    .line 219
    .local v0, "c2":I
    if-ne p1, v0, :cond_2

    .line 221
    iget-object v6, p0, Lcom/ibm/icu/impl/UBiDiProps;->mirrors:[I

    invoke-static {v4}, Lcom/ibm/icu/impl/UBiDiProps;->getMirrorIndex(I)I

    move-result v7

    aget v6, v6, v7

    invoke-static {v6}, Lcom/ibm/icu/impl/UBiDiProps;->getMirrorCodePoint(I)I

    move-result p1

    goto :goto_0

    .line 222
    :cond_2
    if-lt p1, v0, :cond_0

    .line 216
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public final isBidiControl(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UBiDiProps;->getFlagFromProps(II)Z

    move-result v0

    return v0
.end method

.method public final isJoinControl(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 237
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UBiDiProps;->getFlagFromProps(II)Z

    move-result v0

    return v0
.end method

.method public final isMirrored(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lcom/ibm/icu/impl/UBiDiProps;->trie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UBiDiProps;->getFlagFromProps(II)Z

    move-result v0

    return v0
.end method
