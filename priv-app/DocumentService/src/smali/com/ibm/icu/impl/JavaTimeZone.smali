.class public Lcom/ibm/icu/impl/JavaTimeZone;
.super Lcom/ibm/icu/util/TimeZone;
.source "JavaTimeZone.java"


# static fields
.field private static final AVAILABLESET:Ljava/util/TreeSet;

.field private static final serialVersionUID:J = 0x60d4e0281a0a2e14L


# instance fields
.field private transient javacal:Ljava/util/Calendar;

.field private javatz:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    sput-object v2, Lcom/ibm/icu/impl/JavaTimeZone;->AVAILABLESET:Ljava/util/TreeSet;

    .line 38
    invoke-static {}, Ljava/util/TimeZone;->getAvailableIDs()[Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "availableIds":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 40
    sget-object v2, Lcom/ibm/icu/impl/JavaTimeZone;->AVAILABLESET:Ljava/util/TreeSet;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/ibm/icu/util/TimeZone;-><init>()V

    .line 48
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 49
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/JavaTimeZone;->setID(Ljava/lang/String;)V

    .line 50
    new-instance v0, Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 12
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 57
    invoke-direct {p0}, Lcom/ibm/icu/util/TimeZone;-><init>()V

    .line 58
    sget-object v4, Lcom/ibm/icu/impl/JavaTimeZone;->AVAILABLESET:Ljava/util/TreeSet;

    invoke-virtual {v4, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 61
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    if-nez v4, :cond_1

    .line 63
    new-array v2, v5, [Z

    .line 64
    .local v2, "isSystemID":[Z
    invoke-static {p1, v2}, Lcom/ibm/icu/util/TimeZone;->getCanonicalID(Ljava/lang/String;[Z)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "canonicalID":Ljava/lang/String;
    aget-boolean v4, v2, v6

    if-eqz v4, :cond_1

    sget-object v4, Lcom/ibm/icu/impl/JavaTimeZone;->AVAILABLESET:Ljava/util/TreeSet;

    invoke-virtual {v4, v0}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 66
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 70
    .end local v0    # "canonicalID":Ljava/lang/String;
    .end local v2    # "isSystemID":[Z
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    if-nez v4, :cond_2

    .line 71
    const/4 v4, 0x4

    new-array v1, v4, [I

    .line 72
    .local v1, "fields":[I
    invoke-static {p1, v1}, Lcom/ibm/icu/impl/ZoneMeta;->parseCustomID(Ljava/lang/String;[I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 75
    aget v7, v1, v5

    aget v8, v1, v10

    aget v9, v1, v11

    aget v4, v1, v6

    if-gez v4, :cond_4

    move v4, v5

    :goto_0
    invoke-static {v7, v8, v9, v4}, Lcom/ibm/icu/impl/ZoneMeta;->formatCustomID(IIIZ)Ljava/lang/String;

    move-result-object p1

    .line 76
    aget v4, v1, v6

    aget v5, v1, v5

    mul-int/lit8 v5, v5, 0x3c

    aget v6, v1, v10

    add-int/2addr v5, v6

    mul-int/lit8 v5, v5, 0x3c

    aget v6, v1, v11

    add-int/2addr v5, v6

    mul-int/2addr v4, v5

    mul-int/lit16 v3, v4, 0x3e8

    .line 77
    .local v3, "offset":I
    new-instance v4, Ljava/util/SimpleTimeZone;

    invoke-direct {v4, v3, p1}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    iput-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 80
    .end local v1    # "fields":[I
    .end local v3    # "offset":I
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    if-nez v4, :cond_3

    .line 82
    const-string/jumbo p1, "GMT"

    .line 83
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 85
    :cond_3
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/JavaTimeZone;->setID(Ljava/lang/String;)V

    .line 86
    new-instance v4, Ljava/util/GregorianCalendar;

    iget-object v5, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-direct {v4, v5}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    .line 87
    return-void

    .restart local v1    # "fields":[I
    :cond_4
    move v4, v6

    .line 75
    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1, "s"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 215
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 216
    new-instance v0, Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    .line 217
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 202
    invoke-super {p0}, Lcom/ibm/icu/util/TimeZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/JavaTimeZone;

    .line 203
    .local v0, "other":Lcom/ibm/icu/impl/JavaTimeZone;
    iget-object v1, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/util/TimeZone;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    iput-object v1, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    .line 204
    return-object v0
.end method

.method public getDSTSavings()I
    .locals 6

    .prologue
    .line 181
    invoke-super {p0}, Lcom/ibm/icu/util/TimeZone;->getDSTSavings()I

    move-result v2

    .line 184
    .local v2, "dstSavings":I
    const/4 v4, 0x0

    :try_start_0
    new-array v0, v4, [Ljava/lang/Object;

    .line 185
    .local v0, "args":[Ljava/lang/Object;
    const/4 v4, 0x0

    new-array v1, v4, [Ljava/lang/Class;

    .line 186
    .local v1, "argtypes":[Ljava/lang/Class;
    iget-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string/jumbo v5, "getDSTSavings"

    invoke-virtual {v4, v5, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 187
    .local v3, "m":Ljava/lang/reflect/Method;
    iget-object v4, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 191
    .end local v0    # "args":[Ljava/lang/Object;
    .end local v1    # "argtypes":[Ljava/lang/Class;
    .end local v3    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 188
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public getOffset(IIIIII)I
    .locals 7
    .param p1, "era"    # I
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I
    .param p5, "dayOfWeek"    # I
    .param p6, "milliseconds"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v0

    return v0
.end method

.method public getOffset(JZ[I)V
    .locals 23
    .param p1, "date"    # J
    .param p3, "local"    # Z
    .param p4, "offsets"    # [I

    .prologue
    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 101
    if-eqz p3, :cond_3

    .line 102
    const/4 v2, 0x6

    :try_start_0
    new-array v12, v2, [I

    .line 103
    .local v12, "fields":[I
    move-wide/from16 v0, p1

    invoke-static {v0, v1, v12}, Lcom/ibm/icu/impl/Grego;->timeToFields(J[I)[I

    .line 105
    const/4 v2, 0x5

    aget v18, v12, v2

    .line 106
    .local v18, "tmp":I
    move/from16 v0, v18

    rem-int/lit16 v14, v0, 0x3e8

    .line 107
    .local v14, "mil":I
    move/from16 v0, v18

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v18, v0

    .line 108
    rem-int/lit8 v8, v18, 0x3c

    .line 109
    .local v8, "sec":I
    div-int/lit8 v18, v18, 0x3c

    .line 110
    rem-int/lit8 v7, v18, 0x3c

    .line 111
    .local v7, "min":I
    div-int/lit8 v6, v18, 0x3c

    .line 112
    .local v6, "hour":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clear()V

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/4 v3, 0x0

    aget v3, v12, v3

    const/4 v4, 0x1

    aget v4, v12, v4

    const/4 v5, 0x2

    aget v5, v12, v5

    invoke-virtual/range {v2 .. v8}, Ljava/util/Calendar;->set(IIIIII)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v3, 0xe

    invoke-virtual {v2, v3, v14}, Ljava/util/Calendar;->set(II)V

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 118
    .local v11, "doy1":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v13

    .line 119
    .local v13, "hour1":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v16

    .line 120
    .local v16, "min1":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v17

    .line 121
    .local v17, "sec1":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 123
    .local v15, "mil1":I
    const/4 v2, 0x4

    aget v2, v12, v2

    if-ne v2, v11, :cond_0

    if-ne v6, v13, :cond_0

    move/from16 v0, v16

    if-ne v7, v0, :cond_0

    move/from16 v0, v17

    if-ne v8, v0, :cond_0

    if-eq v14, v15, :cond_1

    .line 127
    :cond_0
    const/4 v2, 0x4

    aget v2, v12, v2

    sub-int v2, v11, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    const/4 v9, 0x1

    .line 128
    .local v9, "dayDelta":I
    :goto_0
    mul-int/lit8 v2, v9, 0x18

    add-int/2addr v2, v13

    sub-int/2addr v2, v6

    mul-int/lit8 v2, v2, 0x3c

    add-int v2, v2, v16

    sub-int/2addr v2, v7

    mul-int/lit8 v2, v2, 0x3c

    add-int v2, v2, v17

    sub-int/2addr v2, v8

    mul-int/lit16 v2, v2, 0x3e8

    add-int/2addr v2, v15

    sub-int v10, v2, v14

    .line 134
    .local v10, "delta":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    int-to-long v0, v10

    move-wide/from16 v20, v0

    sub-long v4, v4, v20

    const-wide/16 v20, 0x1

    sub-long v4, v4, v20

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 144
    .end local v6    # "hour":I
    .end local v7    # "min":I
    .end local v8    # "sec":I
    .end local v9    # "dayDelta":I
    .end local v10    # "delta":I
    .end local v11    # "doy1":I
    .end local v12    # "fields":[I
    .end local v13    # "hour1":I
    .end local v14    # "mil":I
    .end local v15    # "mil1":I
    .end local v16    # "min1":I
    .end local v17    # "sec1":I
    .end local v18    # "tmp":I
    :cond_1
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, p4, v2

    .line 145
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aput v3, p4, v2

    .line 146
    monitor-exit v19

    .line 147
    return-void

    .line 127
    .restart local v6    # "hour":I
    .restart local v7    # "min":I
    .restart local v8    # "sec":I
    .restart local v11    # "doy1":I
    .restart local v12    # "fields":[I
    .restart local v13    # "hour1":I
    .restart local v14    # "mil":I
    .restart local v15    # "mil1":I
    .restart local v16    # "min1":I
    .restart local v17    # "sec1":I
    .restart local v18    # "tmp":I
    :cond_2
    const/4 v2, 0x4

    aget v2, v12, v2

    sub-int v9, v11, v2

    goto :goto_0

    .line 141
    .end local v6    # "hour":I
    .end local v7    # "min":I
    .end local v8    # "sec":I
    .end local v11    # "doy1":I
    .end local v12    # "fields":[I
    .end local v13    # "hour1":I
    .end local v14    # "mil":I
    .end local v15    # "mil1":I
    .end local v16    # "min1":I
    .end local v17    # "sec1":I
    .end local v18    # "tmp":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/JavaTimeZone;->javacal:Ljava/util/Calendar;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_1

    .line 146
    :catchall_0
    move-exception v2

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getRawOffset()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 211
    invoke-super {p0}, Lcom/ibm/icu/util/TimeZone;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public inDaylightTime(Ljava/util/Date;)Z
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v0, p1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public setRawOffset(I)V
    .locals 1
    .param p1, "offsetMillis"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v0, p1}, Ljava/util/TimeZone;->setRawOffset(I)V

    .line 168
    return-void
.end method

.method public unwrap()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    return-object v0
.end method

.method public useDaylightTime()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/ibm/icu/impl/JavaTimeZone;->javatz:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v0

    return v0
.end method
