.class public final Lcom/ibm/icu/impl/USerializedSet;
.super Ljava/lang/Object;
.source "USerializedSet.java"


# instance fields
.field private array:[C

.field private arrayOffset:I

.field private bmpLength:I

.field private length:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    const/16 v0, 0x8

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    return-void
.end method


# virtual methods
.method public final contains(I)Z
    .locals 7
    .param p1, "c"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 152
    const v5, 0x10ffff

    if-le p1, v5, :cond_1

    move v3, v4

    .line 170
    :cond_0
    :goto_0
    return v3

    .line 156
    :cond_1
    const v5, 0xffff

    if-gt p1, v5, :cond_3

    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v5, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    if-ge v1, v5, :cond_2

    int-to-char v5, p1

    iget-object v6, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aget-char v6, v6, v1

    if-lt v5, v6, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 160
    :cond_2
    and-int/lit8 v5, v1, 0x1

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0

    .line 164
    .end local v1    # "i":I
    :cond_3
    shr-int/lit8 v5, p1, 0x10

    int-to-char v0, v5

    .local v0, "high":C
    int-to-char v2, p1

    .line 165
    .local v2, "low":C
    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 166
    .restart local v1    # "i":I
    :goto_2
    iget v5, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    if-ge v1, v5, :cond_5

    iget-object v5, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aget-char v5, v5, v1

    if-gt v0, v5, :cond_4

    iget-object v5, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aget-char v5, v5, v1

    if-ne v0, v5, :cond_5

    iget-object v5, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int/lit8 v6, v1, 0x1

    aget-char v5, v5, v6

    if-lt v2, v5, :cond_5

    .line 167
    :cond_4
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 170
    :cond_5
    iget v5, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    add-int/2addr v5, v1

    and-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public final countRanges()I
    .locals 3

    .prologue
    .line 182
    iget v0, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final getRange(I[I)Z
    .locals 8
    .param p1, "rangeIndex"    # I
    .param p2, "range"    # [I

    .prologue
    const/high16 v7, 0x110000

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 102
    if-gez p1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v2

    .line 105
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    if-nez v4, :cond_2

    .line 106
    const/16 v4, 0x8

    new-array v4, v4, [C

    iput-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    .line 108
    :cond_2
    if-eqz p2, :cond_3

    array-length v4, p2

    const/4 v5, 0x2

    if-ge v4, v5, :cond_4

    .line 109
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 111
    :cond_4
    mul-int/lit8 p1, p1, 0x2

    .line 112
    iget v4, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    if-ge p1, v4, :cond_7

    .line 113
    iget-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int/lit8 v1, p1, 0x1

    .end local p1    # "rangeIndex":I
    .local v1, "rangeIndex":I
    aget-char v4, v4, p1

    aput v4, p2, v2

    .line 114
    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    if-ge v1, v2, :cond_5

    .line 115
    iget-object v2, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aget-char v2, v2, v1

    aput v2, p2, v3

    .line 121
    :goto_1
    aget v2, p2, v3

    add-int/lit8 v2, v2, -0x1

    aput v2, p2, v3

    move p1, v1

    .end local v1    # "rangeIndex":I
    .restart local p1    # "rangeIndex":I
    move v2, v3

    .line 122
    goto :goto_0

    .line 116
    .end local p1    # "rangeIndex":I
    .restart local v1    # "rangeIndex":I
    :cond_5
    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    if-ge v1, v2, :cond_6

    .line 117
    iget-object v2, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aget-char v2, v2, v1

    shl-int/lit8 v2, v2, 0x10

    iget-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int/lit8 v5, v1, 0x1

    aget-char v4, v4, v5

    or-int/2addr v2, v4

    aput v2, p2, v3

    goto :goto_1

    .line 119
    :cond_6
    aput v7, p2, v3

    goto :goto_1

    .line 124
    .end local v1    # "rangeIndex":I
    .restart local p1    # "rangeIndex":I
    :cond_7
    iget v4, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    sub-int/2addr p1, v4

    .line 125
    mul-int/lit8 p1, p1, 0x2

    .line 126
    iget v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    iget v5, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 127
    iget v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    if-ge p1, v4, :cond_0

    .line 128
    iget v4, p0, Lcom/ibm/icu/impl/USerializedSet;->arrayOffset:I

    iget v5, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    add-int v0, v4, v5

    .line 129
    .local v0, "offset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int v5, v0, p1

    aget-char v4, v4, v5

    shl-int/lit8 v4, v4, 0x10

    iget-object v5, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int v6, v0, p1

    add-int/lit8 v6, v6, 0x1

    aget-char v5, v5, v6

    or-int/2addr v4, v5

    aput v4, p2, v2

    .line 130
    add-int/lit8 p1, p1, 0x2

    .line 131
    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    if-ge p1, v2, :cond_8

    .line 132
    iget-object v2, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int v4, v0, p1

    aget-char v2, v2, v4

    shl-int/lit8 v2, v2, 0x10

    iget-object v4, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int v5, v0, p1

    add-int/lit8 v5, v5, 0x1

    aget-char v4, v4, v5

    or-int/2addr v2, v4

    aput v2, p2, v3

    .line 136
    :goto_2
    aget v2, p2, v3

    add-int/lit8 v2, v2, -0x1

    aput v2, p2, v3

    move v2, v3

    .line 137
    goto/16 :goto_0

    .line 134
    :cond_8
    aput v7, p2, v3

    goto :goto_2
.end method

.method public final getSet([CI)Z
    .locals 5
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I

    .prologue
    const/4 v4, 0x0

    .line 30
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    .line 31
    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->arrayOffset:I

    .line 33
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "srcStart":I
    .local v0, "srcStart":I
    aget-char v1, p1, p2

    iput v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 36
    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-lez v1, :cond_1

    .line 38
    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    and-int/lit16 v1, v1, 0x7fff

    iput v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 39
    array-length v1, p1

    add-int/lit8 v2, v0, 0x1

    iget v3, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_0

    .line 40
    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 41
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 43
    :cond_0
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "srcStart":I
    .restart local p2    # "srcStart":I
    aget-char v1, p1, v0

    iput v1, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 52
    :goto_0
    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    .line 53
    iget-object v1, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    invoke-static {p1, p2, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    const/4 v1, 0x1

    return v1

    .line 46
    .end local p2    # "srcStart":I
    .restart local v0    # "srcStart":I
    :cond_1
    array-length v1, p1

    iget v2, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_2

    .line 47
    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 48
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 50
    :cond_2
    iget v1, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    iput v1, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    move p2, v0

    .end local v0    # "srcStart":I
    .restart local p2    # "srcStart":I
    goto :goto_0
.end method

.method public final setToOne(I)V
    .locals 7
    .param p1, "c"    # I

    .prologue
    const/4 v6, 0x3

    const v5, 0xffff

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    const v0, 0x10ffff

    if-ge v0, p1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 67
    :cond_0
    if-ge p1, v5, :cond_1

    .line 68
    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 69
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    int-to-char v1, p1

    aput-char v1, v0, v2

    .line 70
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    add-int/lit8 v1, p1, 0x1

    int-to-char v1, v1

    aput-char v1, v0, v3

    goto :goto_0

    .line 71
    :cond_1
    if-ne p1, v5, :cond_2

    .line 72
    iput v3, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 73
    iput v6, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 74
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aput-char v5, v0, v2

    .line 75
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aput-char v3, v0, v3

    .line 76
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aput-char v2, v0, v4

    goto :goto_0

    .line 77
    :cond_2
    const v0, 0x10ffff

    if-ge p1, v0, :cond_3

    .line 78
    iput v2, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 79
    const/4 v0, 0x4

    iput v0, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 80
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    shr-int/lit8 v1, p1, 0x10

    int-to-char v1, v1

    aput-char v1, v0, v2

    .line 81
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    int-to-char v1, p1

    aput-char v1, v0, v3

    .line 82
    add-int/lit8 p1, p1, 0x1

    .line 83
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    shr-int/lit8 v1, p1, 0x10

    int-to-char v1, v1

    aput-char v1, v0, v4

    .line 84
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    int-to-char v1, p1

    aput-char v1, v0, v6

    goto :goto_0

    .line 86
    :cond_3
    iput v2, p0, Lcom/ibm/icu/impl/USerializedSet;->bmpLength:I

    .line 87
    iput v4, p0, Lcom/ibm/icu/impl/USerializedSet;->length:I

    .line 88
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    const/16 v1, 0x10

    aput-char v1, v0, v2

    .line 89
    iget-object v0, p0, Lcom/ibm/icu/impl/USerializedSet;->array:[C

    aput-char v5, v0, v3

    goto :goto_0
.end method
