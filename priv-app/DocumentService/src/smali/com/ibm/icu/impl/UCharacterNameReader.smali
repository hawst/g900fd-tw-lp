.class final Lcom/ibm/icu/impl/UCharacterNameReader;
.super Ljava/lang/Object;
.source "UCharacterNameReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# static fields
.field private static final ALG_INFO_SIZE_:I = 0xc

.field private static final DATA_FORMAT_ID_:[B

.field private static final DATA_FORMAT_VERSION_:[B

.field private static final GROUP_INFO_SIZE_:I = 0x3


# instance fields
.field private m_algnamesindex_:I

.field private m_dataInputStream_:Ljava/io/DataInputStream;

.field private m_groupindex_:I

.field private m_groupstringindex_:I

.field private m_tokenstringindex_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 153
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_VERSION_:[B

    .line 155
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_ID_:[B

    return-void

    .line 153
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 155
    :array_1
    .array-data 1
        0x75t
        0x6et
        0x61t
        0x6dt
    .end array-data
.end method

.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-object v0, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_ID_:[B

    invoke-static {p1, v0, p0}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    .line 47
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    .line 48
    return-void
.end method

.method private readAlg()Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    new-instance v6, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    invoke-direct {v6}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;-><init>()V

    .line 169
    .local v6, "result":Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 170
    .local v5, "rangestart":I
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 171
    .local v4, "rangeend":I
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readByte()B

    move-result v9

    .line 172
    .local v9, "type":B
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readByte()B

    move-result v10

    .line 173
    .local v10, "variant":B
    invoke-virtual {v6, v5, v4, v9, v10}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->setInfo(IIBB)Z

    move-result v11

    if-nez v11, :cond_1

    .line 174
    const/4 v6, 0x0

    .line 207
    .end local v6    # "result":Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    :cond_0
    :goto_0
    return-object v6

    .line 177
    .restart local v6    # "result":Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    :cond_1
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readChar()C

    move-result v7

    .line 178
    .local v7, "size":I
    const/4 v11, 0x1

    if-ne v9, v11, :cond_3

    .line 180
    new-array v1, v10, [C

    .line 181
    .local v1, "factor":[C
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, v10, :cond_2

    .line 182
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readChar()C

    move-result v11

    aput-char v11, v1, v2

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 185
    :cond_2
    invoke-virtual {v6, v1}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->setFactor([C)Z

    .line 186
    shl-int/lit8 v11, v10, 0x1

    sub-int/2addr v7, v11

    .line 189
    .end local v1    # "factor":[C
    .end local v2    # "j":I
    :cond_3
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 190
    .local v3, "prefix":Ljava/lang/StringBuffer;
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readByte()B

    move-result v11

    and-int/lit16 v11, v11, 0xff

    int-to-char v0, v11

    .line 191
    .local v0, "c":C
    :goto_2
    if-eqz v0, :cond_4

    .line 193
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 194
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11}, Ljava/io/DataInputStream;->readByte()B

    move-result v11

    and-int/lit16 v11, v11, 0xff

    int-to-char v0, v11

    .line 195
    goto :goto_2

    .line 197
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->setPrefix(Ljava/lang/String;)Z

    .line 199
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0xc

    add-int/lit8 v11, v11, 0x1

    sub-int/2addr v7, v11

    .line 201
    if-lez v7, :cond_0

    .line 203
    new-array v8, v7, [B

    .line 204
    .local v8, "string":[B
    iget-object v11, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v11, v8}, Ljava/io/DataInputStream;->readFully([B)V

    .line 205
    invoke-virtual {v6, v8}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->setFactorString([B)Z

    goto :goto_0
.end method


# virtual methods
.method protected authenticate([B[B)Z
    .locals 1
    .param p1, "dataformatid"    # [B
    .param p2, "dataformatversion"    # [B

    .prologue
    .line 118
    sget-object v0, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_ID_:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_VERSION_:[B

    invoke-static {v0, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataVersionAcceptable([B)Z
    .locals 3
    .param p1, "version"    # [B

    .prologue
    const/4 v0, 0x0

    .line 33
    aget-byte v1, p1, v0

    sget-object v2, Lcom/ibm/icu/impl/UCharacterNameReader;->DATA_FORMAT_VERSION_:[B

    aget-byte v2, v2, v0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected read(Lcom/ibm/icu/impl/UCharacterName;)V
    .locals 11
    .param p1, "data"    # Lcom/ibm/icu/impl/UCharacterName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    iput v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_tokenstringindex_:I

    .line 63
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    iput v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_groupindex_:I

    .line 64
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    iput v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_groupstringindex_:I

    .line 65
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    iput v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_algnamesindex_:I

    .line 68
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readChar()C

    move-result v2

    .line 69
    .local v2, "count":I
    new-array v7, v2, [C

    .line 70
    .local v7, "token":[C
    const/4 v5, 0x0

    .local v5, "i":C
    :goto_0
    if-ge v5, v2, :cond_0

    .line 71
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readChar()C

    move-result v9

    aput-char v9, v7, v5

    .line 70
    add-int/lit8 v9, v5, 0x1

    int-to-char v5, v9

    goto :goto_0

    .line 73
    :cond_0
    iget v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_groupindex_:I

    iget v10, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_tokenstringindex_:I

    sub-int v6, v9, v10

    .line 74
    .local v6, "size":I
    new-array v8, v6, [B

    .line 75
    .local v8, "tokenstr":[B
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9, v8}, Ljava/io/DataInputStream;->readFully([B)V

    .line 76
    invoke-virtual {p1, v7, v8}, Lcom/ibm/icu/impl/UCharacterName;->setToken([C[B)Z

    .line 79
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readChar()C

    move-result v2

    .line 80
    const/4 v9, 0x3

    invoke-virtual {p1, v2, v9}, Lcom/ibm/icu/impl/UCharacterName;->setGroupCountSize(II)Z

    .line 81
    mul-int/lit8 v2, v2, 0x3

    .line 82
    new-array v3, v2, [C

    .line 83
    .local v3, "group":[C
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v2, :cond_1

    .line 84
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readChar()C

    move-result v9

    aput-char v9, v3, v5

    .line 83
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 87
    :cond_1
    iget v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_algnamesindex_:I

    iget v10, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_groupstringindex_:I

    sub-int v6, v9, v10

    .line 88
    new-array v4, v6, [B

    .line 89
    .local v4, "groupstring":[B
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 91
    invoke-virtual {p1, v3, v4}, Lcom/ibm/icu/impl/UCharacterName;->setGroup([C[B)Z

    .line 93
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterNameReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v9}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 94
    new-array v0, v2, [Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    .line 97
    .local v0, "alg":[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v2, :cond_3

    .line 99
    invoke-direct {p0}, Lcom/ibm/icu/impl/UCharacterNameReader;->readAlg()Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    move-result-object v1

    .line 100
    .local v1, "an":Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    if-nez v1, :cond_2

    .line 101
    new-instance v9, Ljava/io/IOException;

    const-string/jumbo v10, "unames.icu read error: Algorithmic names creation error"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 103
    :cond_2
    aput-object v1, v0, v5

    .line 97
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 105
    .end local v1    # "an":Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    :cond_3
    invoke-virtual {p1, v0}, Lcom/ibm/icu/impl/UCharacterName;->setAlgorithm([Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)Z

    .line 106
    return-void
.end method
