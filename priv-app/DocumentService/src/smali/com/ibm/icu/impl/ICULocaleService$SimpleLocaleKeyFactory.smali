.class public Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;
.super Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;
.source "ICULocaleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICULocaleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleLocaleKeyFactory"
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final kind:I

.field private final obj:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)V
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "kind"    # I
    .param p4, "visible"    # Z

    .prologue
    .line 478
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;-><init>(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZLjava/lang/String;)V

    .line 479
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZLjava/lang/String;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "kind"    # I
    .param p4, "visible"    # Z
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 482
    invoke-direct {p0, p4, p5}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;-><init>(ZLjava/lang/String;)V

    .line 484
    iput-object p1, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->obj:Ljava/lang/Object;

    .line 485
    invoke-virtual {p2}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    .line 486
    iput p3, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->kind:I

    .line 487
    return-void
.end method


# virtual methods
.method public create(Lcom/ibm/icu/impl/ICUService$Key;Lcom/ibm/icu/impl/ICUService;)Ljava/lang/Object;
    .locals 4
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "service"    # Lcom/ibm/icu/impl/ICUService;

    .prologue
    .line 493
    move-object v1, p1

    check-cast v1, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    .line 494
    .local v1, "lkey":Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    iget v2, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->kind:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->kind:I

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 495
    :cond_0
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID()Ljava/lang/String;

    move-result-object v0

    .line 496
    .local v0, "keyID":Ljava/lang/String;
    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 497
    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->obj:Ljava/lang/Object;

    .line 500
    .end local v0    # "keyID":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected isSupportedID(Ljava/lang/String;)Z
    .locals 1
    .param p1, "idToCheck"    # Ljava/lang/String;

    .prologue
    .line 504
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 516
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-super {p0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 517
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string/jumbo v1, ", id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 518
    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 519
    const-string/jumbo v1, ", kind: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 520
    iget v1, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->kind:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 521
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateVisibleIDs(Ljava/util/Map;)V
    .locals 1
    .param p1, "result"    # Ljava/util/Map;

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->visible:Z

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    :goto_0
    return-void

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;->id:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
