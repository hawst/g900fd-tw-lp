.class final Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
.super Ljava/lang/Object;
.source "ICUResourceBundle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AvailEntry"
.end annotation


# instance fields
.field private fullNameSet:Ljava/util/Set;

.field private locales:[Ljava/util/Locale;

.field private nameSet:Ljava/util/Set;

.field private prefix:Ljava/lang/String;

.field private ulocales:[Lcom/ibm/icu/util/ULocale;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->prefix:Ljava/lang/String;

    .line 602
    return-void
.end method


# virtual methods
.method getFullLocaleNameSet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->fullNameSet:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->prefix:Ljava/lang/String;

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->access$300(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->fullNameSet:Ljava/util/Set;

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->fullNameSet:Ljava/util/Set;

    return-object v0
.end method

.method getLocaleList()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->locales:[Ljava/util/Locale;

    if-nez v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->prefix:Ljava/lang/String;

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->access$100(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->locales:[Ljava/util/Locale;

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->locales:[Ljava/util/Locale;

    return-object v0
.end method

.method getLocaleNameSet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->nameSet:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->prefix:Ljava/lang/String;

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->access$200(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->nameSet:Ljava/util/Set;

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->nameSet:Ljava/util/Set;

    return-object v0
.end method

.method getULocaleList()[Lcom/ibm/icu/util/ULocale;
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->ulocales:[Lcom/ibm/icu/util/ULocale;

    if-nez v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->prefix:Ljava/lang/String;

    sget-object v1, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->access$000(Ljava/lang/String;Ljava/lang/ClassLoader;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->ulocales:[Lcom/ibm/icu/util/ULocale;

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->ulocales:[Lcom/ibm/icu/util/ULocale;

    return-object v0
.end method
