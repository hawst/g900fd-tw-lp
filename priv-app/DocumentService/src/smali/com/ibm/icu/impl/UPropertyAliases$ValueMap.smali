.class Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;
.super Ljava/lang/Object;
.source "UPropertyAliases.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UPropertyAliases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValueMap"
.end annotation


# instance fields
.field enumToName:Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;

.field nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

.field private final this$0:Lcom/ibm/icu/impl/UPropertyAliases;


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/UPropertyAliases;Lcom/ibm/icu/impl/UPropertyAliases$Builder;)V
    .locals 5
    .param p2, "b"    # Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->this$0:Lcom/ibm/icu/impl/UPropertyAliases;

    .line 266
    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readShort()S

    move-result v0

    .line 267
    .local v0, "enumToName_offset":S
    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readShort()S

    move-result v2

    .line 268
    .local v2, "ncEnumToName_offset":S
    invoke-virtual {p2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readShort()S

    move-result v1

    .line 269
    .local v1, "nameToEnum_offset":S
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p2, v0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 271
    new-instance v3, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;

    invoke-direct {v3, p2}, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;-><init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V

    .line 272
    .local v3, "x":Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;
    iget-object v4, v3, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->offsetArray:[S

    invoke-static {p2, v4}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$000(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V

    .line 273
    iput-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;

    .line 280
    .end local v3    # "x":Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;
    :goto_0
    invoke-virtual {p2, v1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 281
    new-instance v4, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    invoke-direct {v4, p1, p2}, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;-><init>(Lcom/ibm/icu/impl/UPropertyAliases;Lcom/ibm/icu/impl/UPropertyAliases$Builder;)V

    iput-object v4, p0, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    .line 282
    return-void

    .line 275
    :cond_0
    invoke-virtual {p2, v2}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 276
    new-instance v3, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    invoke-direct {v3, p2}, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;-><init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V

    .line 277
    .local v3, "x":Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;
    iget-object v4, v3, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    invoke-static {p2, v4}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$000(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V

    .line 278
    iput-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;

    goto :goto_0
.end method
