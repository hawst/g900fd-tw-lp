.class Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;
.super Ljava/lang/Object;
.source "UPropertyAliases.java"

# interfaces
.implements Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UPropertyAliases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContiguousEnumToShort"
.end annotation


# instance fields
.field enumLimit:I

.field enumStart:I

.field offsetArray:[S


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V
    .locals 4
    .param p1, "s"    # Lcom/ibm/icu/impl/ICUBinaryStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumStart:I

    .line 312
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumLimit:I

    .line 313
    iget v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumLimit:I

    iget v3, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumStart:I

    sub-int v0, v2, v3

    .line 314
    .local v0, "count":I
    new-array v2, v0, [S

    iput-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->offsetArray:[S

    .line 315
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 316
    iget-object v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->offsetArray:[S

    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->readShort()S

    move-result v3

    aput-short v3, v2, v1

    .line 315
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 318
    :cond_0
    return-void
.end method


# virtual methods
.method public getShort(I)S
    .locals 3
    .param p1, "enumProbe"    # I

    .prologue
    .line 302
    iget v0, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumLimit:I

    if-lt p1, v0, :cond_1

    .line 303
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid enum. enumStart = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " enumLimit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " enumProbe = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->offsetArray:[S

    iget v1, p0, Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;->enumStart:I

    sub-int v1, p1, v1

    aget-short v0, v0, v1

    return v0
.end method
