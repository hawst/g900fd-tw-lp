.class public Lcom/ibm/icu/impl/ICULocaleService;
.super Lcom/ibm/icu/impl/ICUService;
.source "ICULocaleService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ICULocaleService$ICUResourceBundleFactory;,
        Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;,
        Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;,
        Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    }
.end annotation


# instance fields
.field private fallbackLocale:Lcom/ibm/icu/util/ULocale;

.field private fallbackLocaleName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUService;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/ICUService;-><init>(Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public createKey(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/impl/ICUService$Key;
    .locals 1
    .param p1, "l"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->validateFallbackLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->createWithCanonical(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;I)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    move-result-object v0

    return-object v0
.end method

.method public createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->validateFallbackLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->createWithCanonicalFallback(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    move-result-object v0

    return-object v0
.end method

.method public createKey(Ljava/lang/String;I)Lcom/ibm/icu/impl/ICUService$Key;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "kind"    # I

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->validateFallbackLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->createWithCanonicalFallback(Ljava/lang/String;Ljava/lang/String;I)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;
    .locals 2
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 40
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/ibm/icu/util/ULocale;I)Ljava/lang/Object;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;
    .locals 7
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I
    .param p3, "actualReturn"    # [Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v6, 0x0

    .line 66
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/impl/ICULocaleService;->createKey(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v0

    .line 67
    .local v0, "key":Lcom/ibm/icu/impl/ICUService$Key;
    if-nez p3, :cond_1

    .line 68
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/ICULocaleService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;)Ljava/lang/Object;

    move-result-object v2

    .line 80
    :cond_0
    :goto_0
    return-object v2

    .line 71
    :cond_1
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/lang/String;

    .line 72
    .local v3, "temp":[Ljava/lang/String;
    invoke-virtual {p0, v0, v3}, Lcom/ibm/icu/impl/ICULocaleService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 73
    .local v2, "result":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 74
    aget-object v4, v3, v6

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 75
    .local v1, "n":I
    if-ltz v1, :cond_2

    .line 76
    aget-object v4, v3, v6

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 78
    :cond_2
    new-instance v4, Lcom/ibm/icu/util/ULocale;

    aget-object v5, v3, v6

    invoke-direct {v4, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    aput-object v4, p3, v6

    goto :goto_0
.end method

.method public get(Lcom/ibm/icu/util/ULocale;[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actualReturn"    # [Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 56
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableLocales()[Ljava/util/Locale;
    .locals 7

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->getVisibleIDs()Ljava/util/Set;

    move-result-object v5

    .line 126
    .local v5, "visIDs":Ljava/util/Set;
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 127
    .local v0, "iter":Ljava/util/Iterator;
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v6

    new-array v2, v6, [Ljava/util/Locale;

    .line 128
    .local v2, "locales":[Ljava/util/Locale;
    const/4 v3, 0x0

    .line 129
    .local v3, "n":I
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 130
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/ibm/icu/impl/LocaleUtility;->getLocaleFromName(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 131
    .local v1, "loc":Ljava/util/Locale;
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "n":I
    .local v4, "n":I
    aput-object v1, v2, v3

    move v3, v4

    .line 132
    .end local v4    # "n":I
    .restart local v3    # "n":I
    goto :goto_0

    .line 133
    .end local v1    # "loc":Ljava/util/Locale;
    :cond_0
    return-object v2
.end method

.method public getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 7

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->getVisibleIDs()Ljava/util/Set;

    move-result-object v4

    .line 142
    .local v4, "visIDs":Ljava/util/Set;
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 143
    .local v0, "iter":Ljava/util/Iterator;
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v1, v5, [Lcom/ibm/icu/util/ULocale;

    .line 144
    .local v1, "locales":[Lcom/ibm/icu/util/ULocale;
    const/4 v2, 0x0

    .line 145
    .local v2, "n":I
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 146
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "n":I
    .local v3, "n":I
    new-instance v6, Lcom/ibm/icu/util/ULocale;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v6, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    aput-object v6, v1, v2

    move v2, v3

    .line 147
    .end local v3    # "n":I
    .restart local v2    # "n":I
    goto :goto_0

    .line 148
    :cond_0
    return-object v1
.end method

.method public registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 89
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/ibm/icu/impl/ICULocaleService;->registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method public registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "kind"    # I

    .prologue
    .line 107
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/ibm/icu/impl/ICULocaleService;->registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method public registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "kind"    # I
    .param p4, "visible"    # Z

    .prologue
    .line 115
    new-instance v0, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/ibm/icu/impl/ICULocaleService$SimpleLocaleKeyFactory;-><init>(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)V

    .line 116
    .local v0, "factory":Lcom/ibm/icu/impl/ICUService$Factory;
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/ICULocaleService;->registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v1

    return-object v1
.end method

.method public registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;Z)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "visible"    # Z

    .prologue
    .line 98
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/ibm/icu/impl/ICULocaleService;->registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;IZ)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method public validateFallbackLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 590
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 591
    .local v0, "loc":Lcom/ibm/icu/util/ULocale;
    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService;->fallbackLocale:Lcom/ibm/icu/util/ULocale;

    if-eq v0, v1, :cond_1

    .line 592
    monitor-enter p0

    .line 593
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService;->fallbackLocale:Lcom/ibm/icu/util/ULocale;

    if-eq v0, v1, :cond_0

    .line 594
    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService;->fallbackLocale:Lcom/ibm/icu/util/ULocale;

    .line 595
    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService;->fallbackLocaleName:Ljava/lang/String;

    .line 596
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService;->clearServiceCache()V

    .line 598
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService;->fallbackLocaleName:Ljava/lang/String;

    return-object v1

    .line 598
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
