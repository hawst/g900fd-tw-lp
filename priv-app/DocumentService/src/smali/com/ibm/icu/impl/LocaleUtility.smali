.class public Lcom/ibm/icu/impl/LocaleUtility;
.super Ljava/lang/Object;
.source "LocaleUtility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fallback(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 6
    .param p0, "loc"    # Ljava/util/Locale;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 118
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 121
    .local v1, "parts":[Ljava/lang/String;
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 122
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    .line 127
    :cond_0
    if-gez v0, :cond_2

    .line 128
    const/4 v2, 0x0

    .line 130
    :goto_1
    return-object v2

    .line 121
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 130
    :cond_2
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v1, v3

    aget-object v4, v1, v4

    aget-object v5, v1, v5

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getLocaleFromName(Ljava/lang/String;)Ljava/util/Locale;
    .locals 7
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x5f

    .line 24
    const-string/jumbo v3, ""

    .line 25
    .local v3, "language":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 26
    .local v0, "country":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 28
    .local v4, "variant":Ljava/lang/String;
    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 29
    .local v1, "i1":I
    if-gez v1, :cond_0

    .line 30
    move-object v3, p0

    .line 43
    :goto_0
    new-instance v5, Ljava/util/Locale;

    invoke-direct {v5, v3, v0, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 32
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 33
    add-int/lit8 v1, v1, 0x1

    .line 34
    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 35
    .local v2, "i2":I
    if-gez v2, :cond_1

    .line 36
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 39
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static isFallbackOf(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "parent"    # Ljava/lang/String;
    .param p1, "child"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 56
    .local v0, "i":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v0, v2, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5f

    if-ne v2, v3, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isFallbackOf(Ljava/util/Locale;Ljava/util/Locale;)Z
    .locals 2
    .param p0, "parent"    # Ljava/util/Locale;
    .param p1, "child"    # Ljava/util/Locale;

    .prologue
    .line 66
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/LocaleUtility;->isFallbackOf(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
