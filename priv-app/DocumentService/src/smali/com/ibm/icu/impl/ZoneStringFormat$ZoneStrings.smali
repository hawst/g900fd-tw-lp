.class Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
.super Ljava/lang/Object;
.source "ZoneStringFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ZoneStringFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ZoneStrings"
.end annotation


# instance fields
.field private commonlyUsed:Z

.field private genericPartialLocationStrings:[[Ljava/lang/String;

.field private strings:[Ljava/lang/String;


# direct methods
.method private constructor <init>([Ljava/lang/String;Z[[Ljava/lang/String;)V
    .locals 5
    .param p1, "zstrarray"    # [Ljava/lang/String;
    .param p2, "commonlyUsed"    # Z
    .param p3, "genericPartialLocationStrings"    # [[Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 698
    if-eqz p1, :cond_2

    .line 699
    const/4 v1, -0x1

    .line 700
    .local v1, "lastIdx":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 701
    aget-object v2, p1, v0

    if-eqz v2, :cond_0

    .line 702
    move v1, v0

    .line 700
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 705
    :cond_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 706
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->strings:[Ljava/lang/String;

    .line 707
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->strings:[Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    invoke-static {p1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 710
    .end local v0    # "i":I
    .end local v1    # "lastIdx":I
    :cond_2
    iput-boolean p2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->commonlyUsed:Z

    .line 711
    iput-object p3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    .line 712
    return-void
.end method

.method constructor <init>([Ljava/lang/String;Z[[Ljava/lang/String;Lcom/ibm/icu/impl/ZoneStringFormat$1;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/String;
    .param p2, "x1"    # Z
    .param p3, "x2"    # [[Ljava/lang/String;
    .param p4, "x3"    # Lcom/ibm/icu/impl/ZoneStringFormat$1;

    .prologue
    .line 692
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;-><init>([Ljava/lang/String;Z[[Ljava/lang/String;)V

    return-void
.end method

.method static access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .param p1, "x1"    # I

    .prologue
    .line 692
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .prologue
    .line 692
    invoke-direct {p0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->isShortFormatCommonlyUsed()Z

    move-result v0

    return v0
.end method

.method static access$400(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 692
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->getGenericPartialLocationString(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGenericPartialLocationString(Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 4
    .param p1, "mzid"    # Ljava/lang/String;
    .param p2, "isShort"    # Z
    .param p3, "commonlyUsedOnly"    # Z

    .prologue
    .line 726
    const/4 v1, 0x0

    .line 727
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 728
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 729
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 730
    if-eqz p2, :cond_2

    .line 731
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x3

    aget-object v2, v2, v3

    if-eqz v2, :cond_1

    .line 732
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x2

    aget-object v1, v2, v3

    .line 741
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return-object v1

    .line 735
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->genericPartialLocationStrings:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x1

    aget-object v1, v2, v3

    .line 737
    goto :goto_1

    .line 728
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "typeIdx"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->strings:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->strings:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 716
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->strings:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 718
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isShortFormatCommonlyUsed()Z
    .locals 1

    .prologue
    .line 722
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->commonlyUsed:Z

    return v0
.end method
