.class public Lcom/ibm/icu/impl/RelativeDateFormat;
.super Lcom/ibm/icu/text/DateFormat;
.source "RelativeDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0xfb59e5457c57c3bL


# instance fields
.field private fCombinedFormat:Lcom/ibm/icu/text/MessageFormat;

.field private fDateFormat:Lcom/ibm/icu/text/DateFormat;

.field fDateStyle:I

.field private transient fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

.field fLocale:Lcom/ibm/icu/util/ULocale;

.field private fTimeFormat:Lcom/ibm/icu/text/DateFormat;

.field fTimeStyle:I


# direct methods
.method public constructor <init>(IILcom/ibm/icu/util/ULocale;)V
    .locals 4
    .param p1, "timeStyle"    # I
    .param p2, "dateStyle"    # I
    .param p3, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Lcom/ibm/icu/text/DateFormat;-><init>()V

    .line 134
    iput-object v2, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    .line 55
    iput-object p3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fLocale:Lcom/ibm/icu/util/ULocale;

    .line 56
    iput p1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeStyle:I

    .line 57
    iput p2, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateStyle:I

    .line 59
    iget v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateStyle:I

    if-eq v1, v3, :cond_0

    .line 60
    iget v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateStyle:I

    and-int/lit16 v0, v1, -0x81

    .line 61
    .local v0, "newStyle":I
    invoke-static {v0, p3}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateFormat:Lcom/ibm/icu/text/DateFormat;

    .line 65
    .end local v0    # "newStyle":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeStyle:I

    if-eq v1, v3, :cond_1

    .line 66
    iget v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeStyle:I

    and-int/lit16 v0, v1, -0x81

    .line 67
    .restart local v0    # "newStyle":I
    invoke-static {v0, p3}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeFormat:Lcom/ibm/icu/text/DateFormat;

    .line 72
    .end local v0    # "newStyle":I
    :goto_1
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fLocale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {p0, v2, v1}, Lcom/ibm/icu/impl/RelativeDateFormat;->initializeCalendar(Lcom/ibm/icu/util/TimeZone;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;

    .line 73
    invoke-direct {p0}, Lcom/ibm/icu/impl/RelativeDateFormat;->loadDates()V

    .line 74
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    iget-object v2, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fLocale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/impl/RelativeDateFormat;->initializeCombinedFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;

    .line 75
    return-void

    .line 63
    :cond_0
    iput-object v2, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateFormat:Lcom/ibm/icu/text/DateFormat;

    goto :goto_0

    .line 69
    :cond_1
    iput-object v2, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeFormat:Lcom/ibm/icu/text/DateFormat;

    goto :goto_1
.end method

.method private static dayDifference(Lcom/ibm/icu/util/Calendar;)I
    .locals 7
    .param p0, "until"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    const/16 v6, 0x14

    .line 192
    invoke-virtual {p0}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/util/Calendar;

    .line 193
    .local v1, "nowCal":Lcom/ibm/icu/util/Calendar;
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 194
    .local v2, "nowDate":Ljava/util/Date;
    invoke-virtual {v1}, Lcom/ibm/icu/util/Calendar;->clear()V

    .line 195
    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 196
    invoke-virtual {p0, v6}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v6}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v4

    sub-int v0, v3, v4

    .line 197
    .local v0, "dayDiff":I
    return v0
.end method

.method private getStringForDay(I)Ljava/lang/String;
    .locals 2
    .param p1, "day"    # I

    .prologue
    .line 143
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    if-nez v1, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/ibm/icu/impl/RelativeDateFormat;->loadDates()V

    .line 146
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 147
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    if-ne v1, p1, :cond_1

    .line 148
    iget-object v1, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->string:Ljava/lang/String;

    .line 151
    :goto_1
    return-object v1

    .line 146
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private initializeCalendar(Lcom/ibm/icu/util/TimeZone;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;
    .locals 1
    .param p1, "zone"    # Lcom/ibm/icu/util/TimeZone;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    if-nez v0, :cond_0

    .line 209
    if-nez p1, :cond_1

    .line 210
    invoke-static {p2}, Lcom/ibm/icu/util/Calendar;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    .line 215
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    return-object v0

    .line 212
    :cond_1
    invoke-static {p1, p2}, Lcom/ibm/icu/util/Calendar;->getInstance(Lcom/ibm/icu/util/TimeZone;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    goto :goto_0
.end method

.method private initializeCombinedFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;
    .locals 5
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 219
    const-string/jumbo v1, "{1} {0}"

    .line 221
    .local v1, "pattern":Ljava/lang/String;
    :try_start_0
    new-instance v0, Lcom/ibm/icu/impl/CalendarData;

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p2, v3}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 222
    .local v0, "calData":Lcom/ibm/icu/impl/CalendarData;
    const-string/jumbo v3, "DateTimePatterns"

    invoke-virtual {v0, v3}, Lcom/ibm/icu/impl/CalendarData;->get(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "patterns":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v3, v2

    const/16 v4, 0x9

    if-lt v3, v4, :cond_0

    .line 224
    const/16 v3, 0x8

    aget-object v1, v2, v3
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v0    # "calData":Lcom/ibm/icu/impl/CalendarData;
    .end local v2    # "patterns":[Ljava/lang/String;
    :cond_0
    :goto_0
    new-instance v3, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v3, v1, p2}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    iput-object v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fCombinedFormat:Lcom/ibm/icu/text/MessageFormat;

    .line 230
    iget-object v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fCombinedFormat:Lcom/ibm/icu/text/MessageFormat;

    return-object v3

    .line 226
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private declared-synchronized loadDates()V
    .locals 11

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/ibm/icu/impl/CalendarData;

    iget-object v8, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fLocale:Lcom/ibm/icu/util/ULocale;

    iget-object v9, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {v9}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v8, v9}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 159
    .local v0, "calData":Lcom/ibm/icu/impl/CalendarData;
    const-string/jumbo v8, "fields"

    const-string/jumbo v9, "day"

    const-string/jumbo v10, "relative"

    invoke-virtual {v0, v8, v9, v10}, Lcom/ibm/icu/impl/CalendarData;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v5

    .line 161
    .local v5, "rb":Lcom/ibm/icu/util/UResourceBundle;
    new-instance v1, Ljava/util/TreeSet;

    new-instance v8, Lcom/ibm/icu/impl/RelativeDateFormat$1;

    invoke-direct {v8, p0}, Lcom/ibm/icu/impl/RelativeDateFormat$1;-><init>(Lcom/ibm/icu/impl/RelativeDateFormat;)V

    invoke-direct {v1, v8}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 176
    .local v1, "datesSet":Ljava/util/Set;
    invoke-virtual {v5}, Lcom/ibm/icu/util/UResourceBundle;->getIterator()Lcom/ibm/icu/util/UResourceBundleIterator;

    move-result-object v2

    .local v2, "i":Lcom/ibm/icu/util/UResourceBundleIterator;
    :goto_0
    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundleIterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 177
    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundleIterator;->next()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 179
    .local v4, "line":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "k":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v7

    .line 181
    .local v7, "v":Ljava/lang/String;
    new-instance v6, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    invoke-direct {v6, p0, v3, v7}, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;-><init>(Lcom/ibm/icu/impl/RelativeDateFormat;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .local v6, "rs":Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 158
    .end local v0    # "calData":Lcom/ibm/icu/impl/CalendarData;
    .end local v1    # "datesSet":Ljava/util/Set;
    .end local v2    # "i":Lcom/ibm/icu/util/UResourceBundleIterator;
    .end local v3    # "k":Ljava/lang/String;
    .end local v4    # "line":Lcom/ibm/icu/util/UResourceBundle;
    .end local v5    # "rb":Lcom/ibm/icu/util/UResourceBundle;
    .end local v6    # "rs":Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    .end local v7    # "v":Ljava/lang/String;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 184
    .restart local v0    # "calData":Lcom/ibm/icu/impl/CalendarData;
    .restart local v1    # "datesSet":Ljava/util/Set;
    .restart local v2    # "i":Lcom/ibm/icu/util/UResourceBundleIterator;
    .restart local v5    # "rb":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    const/4 v8, 0x0

    :try_start_1
    new-array v8, v8, [Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    iput-object v8, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    .line 185
    iget-object v8, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    invoke-interface {v1, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    check-cast v8, [Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    iput-object v8, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDates:[Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    const/4 v6, 0x0

    const/4 v4, -0x1

    .line 90
    const/4 v1, 0x0

    .line 91
    .local v1, "dayString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 92
    .local v2, "timeString":Ljava/lang/String;
    iget v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateStyle:I

    if-eq v3, v4, :cond_0

    .line 94
    invoke-static {p1}, Lcom/ibm/icu/impl/RelativeDateFormat;->dayDifference(Lcom/ibm/icu/util/Calendar;)I

    move-result v0

    .line 97
    .local v0, "dayDiff":I
    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/RelativeDateFormat;->getStringForDay(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    if-nez v1, :cond_0

    .line 101
    iget-object v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fDateFormat:Lcom/ibm/icu/text/DateFormat;

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .end local v0    # "dayDiff":I
    :cond_0
    iget v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeStyle:I

    if-eq v3, v4, :cond_1

    .line 105
    iget-object v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fTimeFormat:Lcom/ibm/icu/text/DateFormat;

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 108
    :cond_1
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 109
    iget-object v3, p0, Lcom/ibm/icu/impl/RelativeDateFormat;->fCombinedFormat:Lcom/ibm/icu/text/MessageFormat;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    const/4 v5, 0x1

    aput-object v2, v4, v5

    new-instance v5, Ljava/text/FieldPosition;

    invoke-direct {v5, v6}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v3, v4, p2, v5}, Lcom/ibm/icu/text/MessageFormat;->format([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object p2

    .line 116
    .end local p2    # "toAppendTo":Ljava/lang/StringBuffer;
    :cond_2
    :goto_0
    return-object p2

    .line 111
    .restart local p2    # "toAppendTo":Ljava/lang/StringBuffer;
    :cond_3
    if-eqz v1, :cond_4

    .line 112
    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 113
    :cond_4
    if-eqz v2, :cond_2

    .line 114
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public parse(Ljava/lang/String;Lcom/ibm/icu/util/Calendar;Ljava/text/ParsePosition;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 123
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Relative Date parse is not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
