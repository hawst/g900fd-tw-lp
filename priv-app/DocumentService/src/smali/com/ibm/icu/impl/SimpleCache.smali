.class public Lcom/ibm/icu/impl/SimpleCache;
.super Ljava/lang/Object;
.source "SimpleCache.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUCache;


# static fields
.field private static final DEFAULT_CAPACITY:I = 0x10


# instance fields
.field private cacheRef:Ljava/lang/ref/Reference;

.field private capacity:I

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/SimpleCache;->type:I

    .line 22
    const/16 v0, 0x10

    iput v0, p0, Lcom/ibm/icu/impl/SimpleCache;->capacity:I

    .line 25
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "cacheType"    # I

    .prologue
    .line 28
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>(II)V

    .line 29
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "cacheType"    # I
    .param p2, "initialCapacity"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/SimpleCache;->type:I

    .line 22
    const/16 v0, 0x10

    iput v0, p0, Lcom/ibm/icu/impl/SimpleCache;->capacity:I

    .line 32
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 33
    iput p1, p0, Lcom/ibm/icu/impl/SimpleCache;->type:I

    .line 35
    :cond_0
    if-lez p2, :cond_1

    .line 36
    iput p2, p0, Lcom/ibm/icu/impl/SimpleCache;->capacity:I

    .line 38
    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 71
    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 42
    .local v1, "ref":Ljava/lang/ref/Reference;
    if-eqz v1, :cond_0

    .line 43
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 44
    .local v0, "map":Ljava/util/Map;
    if-eqz v0, :cond_0

    .line 45
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 48
    .end local v0    # "map":Ljava/util/Map;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 52
    iget-object v1, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 53
    .local v1, "ref":Ljava/lang/ref/Reference;
    const/4 v0, 0x0

    .line 54
    .local v0, "map":Ljava/util/Map;
    if-eqz v1, :cond_0

    .line 55
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "map":Ljava/util/Map;
    check-cast v0, Ljava/util/Map;

    .line 57
    .restart local v0    # "map":Ljava/util/Map;
    :cond_0
    if-nez v0, :cond_1

    .line 58
    new-instance v2, Ljava/util/HashMap;

    iget v3, p0, Lcom/ibm/icu/impl/SimpleCache;->capacity:I

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 59
    iget v2, p0, Lcom/ibm/icu/impl/SimpleCache;->type:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 60
    new-instance v1, Ljava/lang/ref/WeakReference;

    .end local v1    # "ref":Ljava/lang/ref/Reference;
    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 64
    .restart local v1    # "ref":Ljava/lang/ref/Reference;
    :goto_0
    iput-object v1, p0, Lcom/ibm/icu/impl/SimpleCache;->cacheRef:Ljava/lang/ref/Reference;

    .line 66
    :cond_1
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void

    .line 62
    :cond_2
    new-instance v1, Ljava/lang/ref/SoftReference;

    .end local v1    # "ref":Ljava/lang/ref/Reference;
    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .restart local v1    # "ref":Ljava/lang/ref/Reference;
    goto :goto_0
.end method
