.class public Lcom/ibm/icu/impl/ImplicitCEGenerator;
.super Ljava/lang/Object;
.source "ImplicitCEGenerator.java"


# static fields
.field public static final CJK_A_BASE:I = 0x3400

.field public static final CJK_A_LIMIT:I = 0x4dc0

.field public static final CJK_BASE:I = 0x4e00

.field public static final CJK_B_BASE:I = 0x20000

.field public static final CJK_B_LIMIT:I = 0x2a6e0

.field public static final CJK_COMPAT_USED_BASE:I = 0xfa0e

.field public static final CJK_COMPAT_USED_LIMIT:I = 0xfa30

.field public static final CJK_LIMIT:I = 0xa000

.field static final DEBUG:Z = false

.field static final MAX_INPUT:I = 0x220001

.field static NON_CJK_OFFSET:I = 0x0

.field static final bottomByte:J = 0xffL

.field static final fourBytes:J = 0xffffffffL

.field static final topByte:J = 0xff000000L


# instance fields
.field final3Count:I

.field final3Multiplier:I

.field final4Count:I

.field final4Multiplier:I

.field max3Trail:I

.field max4Primary:I

.field max4Trail:I

.field maxTrail:I

.field medialCount:I

.field min3Primary:I

.field min4Boundary:I

.field min4Primary:I

.field minTrail:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 324
    const/high16 v0, 0x110000

    sput v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 7
    .param p1, "minPrimary"    # I
    .param p2, "maxPrimary"    # I

    .prologue
    const/4 v5, 0x1

    .line 96
    const/4 v3, 0x4

    const/16 v4, 0xfe

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/impl/ImplicitCEGenerator;-><init>(IIIIII)V

    .line 97
    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 11
    .param p1, "minPrimary"    # I
    .param p2, "maxPrimary"    # I
    .param p3, "minTrail"    # I
    .param p4, "maxTrail"    # I
    .param p5, "gap3"    # I
    .param p6, "primaries3count"    # I

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    if-ltz p1, :cond_0

    if-ge p1, p2, :cond_0

    const/16 v9, 0xff

    if-le p2, v9, :cond_1

    .line 111
    :cond_0
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "bad lead bytes"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 113
    :cond_1
    if-ltz p3, :cond_2

    if-ge p3, p4, :cond_2

    const/16 v9, 0xff

    if-le p4, v9, :cond_3

    .line 114
    :cond_2
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "bad trail bytes"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 116
    :cond_3
    const/4 v9, 0x1

    move/from16 v0, p6

    if-ge v0, v9, :cond_4

    .line 117
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "bad three-byte primaries"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 120
    :cond_4
    iput p3, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    .line 121
    iput p4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->maxTrail:I

    .line 123
    iput p1, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min3Primary:I

    .line 124
    iput p2, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max4Primary:I

    .line 131
    add-int/lit8 v9, p5, 0x1

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    .line 132
    sub-int v9, p4, p3

    add-int/lit8 v9, v9, 0x1

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    div-int/2addr v9, v10

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    .line 133
    iget v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    add-int/lit8 v9, v9, -0x1

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    mul-int/2addr v9, v10

    add-int/2addr v9, p3

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max3Trail:I

    .line 136
    sub-int v9, p4, p3

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    .line 138
    iget v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    mul-int v7, v9, v10

    .line 141
    .local v7, "threeByteCount":I
    sub-int v9, p2, p1

    add-int/lit8 v6, v9, 0x1

    .line 142
    .local v6, "primariesAvailable":I
    sub-int v5, v6, p6

    .line 144
    .local v5, "primaries4count":I
    mul-int v2, p6, v7

    .line 145
    .local v2, "min3ByteCoverage":I
    add-int v9, p1, p6

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Primary:I

    .line 146
    iput v2, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Boundary:I

    .line 149
    const v9, 0x220001

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Boundary:I

    sub-int v8, v9, v10

    .line 150
    .local v8, "totalNeeded":I
    invoke-static {v8, v5}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->divideAndRoundUp(II)I

    move-result v4

    .line 153
    .local v4, "neededPerPrimaryByte":I
    iget v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v9, v10

    invoke-static {v4, v9}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->divideAndRoundUp(II)I

    move-result v3

    .line 156
    .local v3, "neededPerFinalByte":I
    sub-int v9, p4, p3

    add-int/lit8 v9, v9, -0x1

    div-int v1, v9, v3

    .line 158
    .local v1, "gap4":I
    const/4 v9, 0x1

    if-ge v1, v9, :cond_5

    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "must have larger gap4s"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 160
    :cond_5
    add-int/lit8 v9, v1, 0x1

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    .line 161
    iput v3, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    .line 162
    iget v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    add-int/lit8 v9, v9, -0x1

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    mul-int/2addr v9, v10

    add-int/2addr v9, p3

    iput v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max4Trail:I

    .line 164
    iget v9, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v9, v5

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v9, v10

    iget v10, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    mul-int/2addr v9, v10

    const v10, 0x220001

    if-ge v9, v10, :cond_6

    .line 165
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "internal error"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 174
    :cond_6
    return-void
.end method

.method public static divideAndRoundUp(II)I
    .locals 1
    .param p0, "a"    # I
    .param p1, "b"    # I

    .prologue
    .line 177
    add-int/lit8 v0, p0, -0x1

    div-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static swapCJK(I)I
    .locals 2
    .param p0, "i"    # I

    .prologue
    const v1, 0xfa0e

    .line 328
    const/16 v0, 0x4e00

    if-lt p0, v0, :cond_5

    .line 329
    const v0, 0xa000

    if-ge p0, v0, :cond_1

    add-int/lit16 p0, p0, -0x4e00

    .line 346
    .end local p0    # "i":I
    :cond_0
    :goto_0
    return p0

    .line 331
    .restart local p0    # "i":I
    :cond_1
    if-ge p0, v1, :cond_2

    sget v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    add-int/2addr p0, v0

    goto :goto_0

    .line 333
    :cond_2
    const v0, 0xfa30

    if-ge p0, v0, :cond_3

    sub-int v0, p0, v1

    add-int/lit16 p0, v0, 0x5200

    goto :goto_0

    .line 335
    :cond_3
    const/high16 v0, 0x20000

    if-ge p0, v0, :cond_4

    sget v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    add-int/2addr p0, v0

    goto :goto_0

    .line 337
    :cond_4
    const v0, 0x2a6e0

    if-lt p0, v0, :cond_0

    .line 339
    sget v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    add-int/2addr p0, v0

    goto :goto_0

    .line 341
    :cond_5
    const/16 v0, 0x3400

    if-ge p0, v0, :cond_6

    sget v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    add-int/2addr p0, v0

    goto :goto_0

    .line 343
    :cond_6
    const/16 v0, 0x4dc0

    if-ge p0, v0, :cond_7

    add-int/lit16 v0, p0, -0x3400

    add-int/lit16 v0, v0, 0x5200

    add-int/lit8 p0, v0, 0x22

    goto :goto_0

    .line 346
    :cond_7
    sget v0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    add-int/2addr p0, v0

    goto :goto_0
.end method


# virtual methods
.method public getCodePointFromRaw(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 365
    add-int/lit8 p1, p1, -0x1

    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "result":I
    sget v1, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    if-lt p1, v1, :cond_0

    .line 368
    sget v1, Lcom/ibm/icu/impl/ImplicitCEGenerator;->NON_CJK_OFFSET:I

    sub-int v0, p1, v1

    .line 383
    :goto_0
    return v0

    .line 369
    :cond_0
    const/high16 v1, 0x20000

    if-lt p1, v1, :cond_1

    .line 370
    move v0, p1

    .line 371
    goto :goto_0

    :cond_1
    const v1, 0x9fe2

    if-ge p1, v1, :cond_4

    .line 373
    const/16 v1, 0x5200

    if-ge p1, v1, :cond_2

    .line 374
    add-int/lit16 v0, p1, 0x4e00

    .line 375
    goto :goto_0

    :cond_2
    const/16 v1, 0x5222

    if-ge p1, v1, :cond_3

    .line 376
    const v1, 0xfa0e

    add-int/2addr v1, p1

    add-int/lit16 v0, v1, -0x5200

    .line 377
    goto :goto_0

    .line 378
    :cond_3
    add-int/lit16 v1, p1, 0x3400

    add-int/lit16 v1, v1, -0x5200

    add-int/lit8 v0, v1, -0x22

    .line 380
    goto :goto_0

    .line 381
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getGap3()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getGap4()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getImplicitFromCodePoint(I)I
    .locals 1
    .param p1, "cp"    # I

    .prologue
    .line 290
    invoke-static {p1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->swapCJK(I)I

    move-result v0

    add-int/lit8 p1, v0, 0x1

    .line 295
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromRaw(I)I

    move-result v0

    return v0
.end method

.method public getImplicitFromRaw(I)I
    .locals 8
    .param p1, "cp"    # I

    .prologue
    .line 233
    if-ltz p1, :cond_0

    const v4, 0x220001

    if-le p1, v4, :cond_1

    .line 234
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Code point out of range "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    int-to-long v6, p1

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/Utility;->hex(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 236
    :cond_1
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Boundary:I

    sub-int v0, p1, v4

    .line 237
    .local v0, "last0":I
    if-gez v0, :cond_3

    .line 238
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    div-int v1, p1, v4

    .line 239
    .local v1, "last1":I
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    rem-int v0, p1, v4

    .line 241
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    div-int v2, v1, v4

    .line 242
    .local v2, "last2":I
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    rem-int/2addr v1, v4

    .line 244
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    iget v5, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    mul-int/2addr v5, v0

    add-int v0, v4, v5

    .line 245
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    add-int/2addr v1, v4

    .line 246
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min3Primary:I

    add-int/2addr v2, v4

    .line 248
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Primary:I

    if-lt v2, v4, :cond_2

    .line 249
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "4-byte out of range: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    int-to-long v6, p1

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/Utility;->hex(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/Utility;->hex(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 253
    :cond_2
    shl-int/lit8 v4, v2, 0x18

    shl-int/lit8 v5, v1, 0x10

    add-int/2addr v4, v5

    shl-int/lit8 v5, v0, 0x8

    add-int/2addr v4, v5

    .line 274
    :goto_0
    return v4

    .line 255
    .end local v1    # "last1":I
    .end local v2    # "last2":I
    :cond_3
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    div-int v1, v0, v4

    .line 256
    .restart local v1    # "last1":I
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    rem-int/2addr v0, v4

    .line 258
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    div-int v2, v1, v4

    .line 259
    .restart local v2    # "last2":I
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    rem-int/2addr v1, v4

    .line 261
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    div-int v3, v2, v4

    .line 262
    .local v3, "last3":I
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    rem-int/2addr v2, v4

    .line 264
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    iget v5, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    mul-int/2addr v5, v0

    add-int v0, v4, v5

    .line 265
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    add-int/2addr v1, v4

    .line 266
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    add-int/2addr v2, v4

    .line 267
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Primary:I

    add-int/2addr v3, v4

    .line 269
    iget v4, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max4Primary:I

    if-le v3, v4, :cond_4

    .line 270
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "4-byte out of range: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    int-to-long v6, p1

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/Utility;->hex(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    int-to-long v6, v3

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/Utility;->hex(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 274
    :cond_4
    shl-int/lit8 v4, v3, 0x18

    shl-int/lit8 v5, v2, 0x10

    add-int/2addr v4, v5

    shl-int/lit8 v5, v1, 0x8

    add-int/2addr v4, v5

    add-int/2addr v4, v0

    goto :goto_0
.end method

.method public getMaxTrail()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->maxTrail:I

    return v0
.end method

.method public getMinTrail()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    return v0
.end method

.method public getRawFromCodePoint(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 387
    invoke-static {p1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->swapCJK(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getRawFromImplicit(I)I
    .locals 9
    .param p1, "implicit"    # I

    .prologue
    const/4 v6, -0x1

    .line 187
    and-int/lit16 v3, p1, 0xff

    .line 188
    .local v3, "b3":I
    shr-int/lit8 p1, p1, 0x8

    .line 189
    and-int/lit16 v2, p1, 0xff

    .line 190
    .local v2, "b2":I
    shr-int/lit8 p1, p1, 0x8

    .line 191
    and-int/lit16 v1, p1, 0xff

    .line 192
    .local v1, "b1":I
    shr-int/lit8 p1, p1, 0x8

    .line 193
    and-int/lit16 v0, p1, 0xff

    .line 196
    .local v0, "b0":I
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min3Primary:I

    if-lt v0, v7, :cond_0

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max4Primary:I

    if-gt v0, v7, :cond_0

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    if-lt v1, v7, :cond_0

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->maxTrail:I

    if-le v1, v7, :cond_2

    :cond_0
    move v5, v6

    .line 223
    :cond_1
    :goto_0
    return v5

    .line 199
    :cond_2
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    sub-int/2addr v1, v7

    .line 202
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Primary:I

    if-ge v0, v7, :cond_7

    .line 203
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    if-lt v2, v7, :cond_3

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max3Trail:I

    if-gt v2, v7, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    move v5, v6

    goto :goto_0

    .line 204
    :cond_4
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    sub-int/2addr v2, v7

    .line 205
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    rem-int v4, v2, v7

    .line 206
    .local v4, "remainder":I
    if-eqz v4, :cond_5

    move v5, v6

    goto :goto_0

    .line 207
    :cond_5
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min3Primary:I

    sub-int/2addr v0, v7

    .line 208
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Multiplier:I

    div-int/2addr v2, v7

    .line 209
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v7, v0

    add-int/2addr v7, v1

    iget v8, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final3Count:I

    mul-int/2addr v7, v8

    add-int v5, v7, v2

    .line 222
    .local v5, "result":I
    :goto_1
    if-ltz v5, :cond_6

    const v7, 0x220001

    if-le v5, v7, :cond_1

    :cond_6
    move v5, v6

    goto :goto_0

    .line 211
    .end local v4    # "remainder":I
    .end local v5    # "result":I
    :cond_7
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    if-lt v2, v7, :cond_8

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->maxTrail:I

    if-gt v2, v7, :cond_8

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    if-lt v3, v7, :cond_8

    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->max4Trail:I

    if-le v3, v7, :cond_9

    :cond_8
    move v5, v6

    .line 212
    goto :goto_0

    .line 213
    :cond_9
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    sub-int/2addr v2, v7

    .line 214
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->minTrail:I

    sub-int/2addr v3, v7

    .line 215
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    rem-int v4, v3, v7

    .line 216
    .restart local v4    # "remainder":I
    if-eqz v4, :cond_a

    move v5, v6

    goto :goto_0

    .line 217
    :cond_a
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Multiplier:I

    div-int/2addr v3, v7

    .line 218
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Primary:I

    sub-int/2addr v0, v7

    .line 219
    iget v7, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v7, v0

    add-int/2addr v7, v1

    iget v8, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->medialCount:I

    mul-int/2addr v7, v8

    add-int/2addr v7, v2

    iget v8, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->final4Count:I

    mul-int/2addr v7, v8

    add-int/2addr v7, v3

    iget v8, p0, Lcom/ibm/icu/impl/ImplicitCEGenerator;->min4Boundary:I

    add-int v5, v7, v8

    .restart local v5    # "result":I
    goto :goto_1
.end method
