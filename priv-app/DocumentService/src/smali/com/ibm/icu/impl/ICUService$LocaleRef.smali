.class Lcom/ibm/icu/impl/ICUService$LocaleRef;
.super Ljava/lang/Object;
.source "ICUService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocaleRef"
.end annotation


# instance fields
.field private com:Ljava/util/Comparator;

.field private final locale:Lcom/ibm/icu/util/ULocale;

.field private ref:Ljava/lang/ref/SoftReference;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)V
    .locals 1
    .param p1, "dnCache"    # Ljava/util/Map;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "com"    # Ljava/util/Comparator;

    .prologue
    .line 756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 757
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->locale:Lcom/ibm/icu/util/ULocale;

    .line 758
    iput-object p3, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->com:Ljava/util/Comparator;

    .line 759
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->ref:Ljava/lang/ref/SoftReference;

    .line 760
    return-void
.end method


# virtual methods
.method get(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)Ljava/util/SortedMap;
    .locals 2
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "comp"    # Ljava/util/Comparator;

    .prologue
    .line 764
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->ref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    .line 765
    .local v0, "m":Ljava/util/SortedMap;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->com:Ljava/util/Comparator;

    if-eq v1, p2, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->com:Ljava/util/Comparator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService$LocaleRef;->com:Ljava/util/Comparator;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 771
    .end local v0    # "m":Ljava/util/SortedMap;
    :cond_0
    :goto_0
    return-object v0

    .restart local v0    # "m":Ljava/util/SortedMap;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
