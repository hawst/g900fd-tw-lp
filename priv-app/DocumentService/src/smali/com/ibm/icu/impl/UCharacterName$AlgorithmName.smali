.class final Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
.super Ljava/lang/Object;
.source "UCharacterName.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UCharacterName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AlgorithmName"
.end annotation


# static fields
.field static final TYPE_0_:I = 0x0

.field static final TYPE_1_:I = 0x1


# instance fields
.field private m_factor_:[C

.field private m_factorstring_:[B

.field private m_prefix_:Ljava/lang/String;

.field private m_rangeend_:I

.field private m_rangestart_:I

.field private m_type_:B

.field private m_utilIntBuffer_:[I

.field private m_utilStringBuffer_:Ljava/lang/StringBuffer;

.field private m_variant_:B


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 871
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 875
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilIntBuffer_:[I

    .line 610
    return-void
.end method

.method static access$000(Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    .prologue
    .line 593
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    return v0
.end method

.method static access$100(Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    .prologue
    .line 593
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangeend_:I

    return v0
.end method

.method private compareFactorString([IILjava/lang/String;I)Z
    .locals 8
    .param p1, "index"    # [I
    .param p2, "length"    # I
    .param p3, "str"    # Ljava/lang/String;
    .param p4, "offset"    # I

    .prologue
    const/4 v5, 0x0

    .line 927
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    array-length v3, v6

    .line 928
    .local v3, "size":I
    if-eqz p1, :cond_0

    if-eq p2, v3, :cond_1

    .line 954
    :cond_0
    :goto_0
    return v5

    .line 931
    :cond_1
    const/4 v0, 0x0

    .line 932
    .local v0, "count":I
    move v4, p4

    .line 934
    .local v4, "strcount":I
    add-int/lit8 v3, v3, -0x1

    .line 935
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-gt v2, v3, :cond_3

    .line 937
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    aget-char v1, v6, v2

    .line 938
    .local v1, "factor":I
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    aget v7, p1, v2

    invoke-static {v6, v0, v7}, Lcom/ibm/icu/impl/UCharacterUtility;->skipNullTermByteSubString([BII)I

    move-result v0

    .line 940
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    invoke-static {p3, v6, v4, v0}, Lcom/ibm/icu/impl/UCharacterUtility;->compareNullTermByteSubString(Ljava/lang/String;[BII)I

    move-result v4

    .line 942
    if-ltz v4, :cond_0

    .line 946
    if-eq v2, v3, :cond_2

    .line 947
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    aget v7, p1, v2

    sub-int v7, v1, v7

    invoke-static {v6, v0, v7}, Lcom/ibm/icu/impl/UCharacterUtility;->skipNullTermByteSubString([BII)I

    move-result v0

    .line 935
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 951
    .end local v1    # "factor":I
    :cond_3
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v4, v6, :cond_0

    .line 954
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private getFactorString([II)Ljava/lang/String;
    .locals 8
    .param p1, "index"    # [I
    .param p2, "length"    # I

    .prologue
    .line 888
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    array-length v3, v4

    .line 889
    .local v3, "size":I
    if-eqz p1, :cond_0

    if-eq p2, v3, :cond_1

    .line 890
    :cond_0
    const/4 v4, 0x0

    .line 911
    :goto_0
    return-object v4

    .line 893
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v5

    .line 894
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 895
    const/4 v0, 0x0

    .line 897
    .local v0, "count":I
    add-int/lit8 v3, v3, -0x1

    .line 898
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-gt v2, v3, :cond_3

    .line 899
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    aget-char v1, v4, v2

    .line 900
    .local v1, "factor":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    aget v6, p1, v2

    invoke-static {v4, v0, v6}, Lcom/ibm/icu/impl/UCharacterUtility;->skipNullTermByteSubString([BII)I

    move-result v0

    .line 902
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    invoke-static {v4, v6, v0}, Lcom/ibm/icu/impl/UCharacterUtility;->getNullTermByteSubString(Ljava/lang/StringBuffer;[BI)I

    move-result v0

    .line 905
    if-eq v2, v3, :cond_2

    .line 906
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    aget v6, p1, v2

    sub-int v6, v1, v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v4, v0, v6}, Lcom/ibm/icu/impl/UCharacterUtility;->skipNullTermByteSubString([BII)I

    move-result v0

    .line 898
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 911
    .end local v1    # "factor":I
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    monitor-exit v5

    goto :goto_0

    .line 912
    .end local v0    # "count":I
    .end local v2    # "i":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method


# virtual methods
.method add([II)I
    .locals 9
    .param p1, "set"    # [I
    .param p2, "maxlength"    # I

    .prologue
    .line 810
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_prefix_:Ljava/lang/String;

    invoke-static {p1, v5}, Lcom/ibm/icu/impl/UCharacterName;->access$200([ILjava/lang/String;)I

    move-result v3

    .line 811
    .local v3, "length":I
    iget-byte v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_type_:B

    packed-switch v5, :pswitch_data_0

    .line 850
    :cond_0
    :goto_0
    if-le v3, p2, :cond_3

    .line 853
    .end local v3    # "length":I
    :goto_1
    return v3

    .line 815
    .restart local v3    # "length":I
    :pswitch_0
    iget-byte v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    add-int/2addr v3, v5

    .line 819
    goto :goto_0

    .line 825
    :pswitch_1
    iget-byte v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_2
    if-lez v2, :cond_0

    .line 827
    const/4 v4, 0x0

    .line 828
    .local v4, "maxfactorlength":I
    const/4 v0, 0x0

    .line 829
    .local v0, "count":I
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    aget-char v1, v5, v2

    .local v1, "factor":I
    :goto_3
    if-lez v1, :cond_2

    .line 830
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v6

    .line 831
    :try_start_0
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 833
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    invoke-static {v5, v7, v0}, Lcom/ibm/icu/impl/UCharacterUtility;->getNullTermByteSubString(Ljava/lang/StringBuffer;[BI)I

    move-result v0

    .line 837
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-static {p1, v5}, Lcom/ibm/icu/impl/UCharacterName;->access$300([ILjava/lang/StringBuffer;)I

    .line 838
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-le v5, v4, :cond_1

    .line 841
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 844
    :cond_1
    monitor-exit v6

    .line 829
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 844
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 846
    :cond_2
    add-int/2addr v3, v4

    .line 825
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .end local v0    # "count":I
    .end local v1    # "factor":I
    .end local v2    # "i":I
    .end local v4    # "maxfactorlength":I
    :cond_3
    move v3, p2

    .line 853
    goto :goto_1

    .line 811
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method appendName(ILjava/lang/StringBuffer;)V
    .locals 6
    .param p1, "ch"    # I
    .param p2, "str"    # Ljava/lang/StringBuffer;

    .prologue
    .line 697
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_prefix_:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 698
    iget-byte v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_type_:B

    packed-switch v4, :pswitch_data_0

    .line 731
    :goto_0
    return-void

    .line 702
    :pswitch_0
    iget-byte v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    invoke-static {p1, v4, p2}, Lcom/ibm/icu/impl/Utility;->hex(IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 706
    :pswitch_1
    iget v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    sub-int v3, p1, v4

    .line 707
    .local v3, "offset":I
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilIntBuffer_:[I

    .line 713
    .local v2, "indexes":[I
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilIntBuffer_:[I

    monitor-enter v5

    .line 714
    :try_start_0
    iget-byte v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_1
    if-lez v1, :cond_0

    .line 716
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    aget-char v4, v4, v1

    and-int/lit16 v0, v4, 0xff

    .line 717
    .local v0, "factor":I
    rem-int v4, v3, v0

    aput v4, v2, v1

    .line 718
    div-int/2addr v3, v0

    .line 714
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 724
    .end local v0    # "factor":I
    :cond_0
    const/4 v4, 0x0

    aput v3, v2, v4

    .line 727
    iget-byte v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    invoke-direct {p0, v2, v4}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->getFactorString([II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 728
    monitor-exit v5

    goto :goto_0

    .end local v1    # "i":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 698
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method contains(I)Z
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 685
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    if-gt v0, p1, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangeend_:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getChar(Ljava/lang/String;)I
    .locals 11
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v8, -0x1

    .line 739
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_prefix_:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v6

    .line 740
    .local v6, "prefixlen":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lt v9, v6, :cond_0

    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_prefix_:Ljava/lang/String;

    invoke-virtual {p1, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    move v7, v8

    .line 796
    :goto_0
    return v7

    .line 745
    :cond_1
    iget-byte v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_type_:B

    packed-switch v9, :pswitch_data_0

    :cond_2
    move v7, v8

    .line 796
    goto :goto_0

    .line 750
    :pswitch_0
    :try_start_0
    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x10

    invoke-static {v9, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    .line 753
    .local v7, "result":I
    iget v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    if-gt v9, v7, :cond_2

    iget v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangeend_:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-gt v7, v9, :cond_2

    goto :goto_0

    .line 757
    .end local v7    # "result":I
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/NumberFormatException;
    move v7, v8

    .line 759
    goto :goto_0

    .line 765
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :pswitch_1
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    .local v0, "ch":I
    :goto_1
    iget v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangeend_:I

    if-gt v0, v9, :cond_2

    .line 767
    iget v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    sub-int v5, v0, v9

    .line 768
    .local v5, "offset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilIntBuffer_:[I

    .line 774
    .local v4, "indexes":[I
    iget-object v9, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_utilIntBuffer_:[I

    monitor-enter v9

    .line 775
    :try_start_1
    iget-byte v10, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    add-int/lit8 v3, v10, -0x1

    .local v3, "i":I
    :goto_2
    if-lez v3, :cond_3

    .line 777
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    aget-char v10, v10, v3

    and-int/lit16 v2, v10, 0xff

    .line 778
    .local v2, "factor":I
    rem-int v10, v5, v2

    aput v10, v4, v3

    .line 779
    div-int/2addr v5, v2

    .line 775
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 785
    .end local v2    # "factor":I
    :cond_3
    const/4 v10, 0x0

    aput v5, v4, v10

    .line 788
    iget-byte v10, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    invoke-direct {p0, v4, v10, p1, v6}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->compareFactorString([IILjava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 790
    monitor-exit v9

    move v7, v0

    goto :goto_0

    .line 792
    :cond_4
    monitor-exit v9

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 792
    .end local v3    # "i":I
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 745
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setFactor([C)Z
    .locals 2
    .param p1, "factor"    # [C

    .prologue
    .line 645
    array-length v0, p1

    iget-byte v1, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    if-ne v0, v1, :cond_0

    .line 646
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factor_:[C

    .line 647
    const/4 v0, 0x1

    .line 649
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setFactorString([B)Z
    .locals 1
    .param p1, "string"    # [B

    .prologue
    .line 675
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_factorstring_:[B

    .line 676
    const/4 v0, 0x1

    return v0
.end method

.method setInfo(IIBB)Z
    .locals 2
    .param p1, "rangestart"    # I
    .param p2, "rangeend"    # I
    .param p3, "type"    # B
    .param p4, "variant"    # B

    .prologue
    const/4 v0, 0x1

    .line 626
    if-ltz p1, :cond_1

    if-gt p1, p2, :cond_1

    const v1, 0x10ffff

    if-gt p2, v1, :cond_1

    if-eqz p3, :cond_0

    if-ne p3, v0, :cond_1

    .line 629
    :cond_0
    iput p1, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangestart_:I

    .line 630
    iput p2, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_rangeend_:I

    .line 631
    iput-byte p3, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_type_:B

    .line 632
    iput-byte p4, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_variant_:B

    .line 635
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setPrefix(Ljava/lang/String;)Z
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 659
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 660
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->m_prefix_:Ljava/lang/String;

    .line 661
    const/4 v0, 0x1

    .line 663
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
