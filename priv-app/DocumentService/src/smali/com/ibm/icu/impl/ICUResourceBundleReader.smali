.class public final Lcom/ibm/icu/impl/ICUResourceBundleReader;
.super Ljava/lang/Object;
.source "ICUResourceBundleReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# static fields
.field private static final DATA_FORMAT_ID:[B

.field private static final DEBUG:Z = false

.field private static final ICU_RESOURCE_SUFFIX:Ljava/lang/String; = ".res"

.field private static final URES_ATT_NO_FALLBACK:I = 0x1

.field private static final URES_INDEX_ATTRIBUTES:I = 0x5

.field private static final URES_INDEX_BUNDLE_TOP:I = 0x3

.field private static final URES_INDEX_LENGTH:I


# instance fields
.field private data:[B

.field private dataVersion:[B

.field private indexes:[I

.field private noFallback:Z

.field private rootRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->DATA_FORMAT_ID:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x52t
        0x65t
        0x73t
        0x42t
    .end array-data
.end method

.method private constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "resolvedName"    # Ljava/lang/String;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 173
    .local v0, "bs":Ljava/io/BufferedInputStream;
    :try_start_0
    sget-object v2, Lcom/ibm/icu/impl/ICUResourceBundleReader;->DATA_FORMAT_ID:[B

    invoke-static {v0, v2, p0}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->dataVersion:[B

    .line 177
    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->readData(Ljava/io/InputStream;)V

    .line 178
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    return-void

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "ex":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Data file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, " is corrupt - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static getFullName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeName"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x2f

    const/16 v2, 0x2e

    .line 255
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 256
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 257
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    .line 259
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 262
    :cond_2
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 263
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v3, :cond_3

    .line 264
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 266
    :cond_3
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 269
    :cond_4
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 270
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    .line 271
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 273
    :cond_5
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ".res"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static getReader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundleReader;
    .locals 3
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeName"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;

    .prologue
    .line 184
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getFullName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "resolvedName":Ljava/lang/String;
    invoke-static {p2, v1}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 187
    .local v2, "stream":Ljava/io/InputStream;
    if-nez v2, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 191
    :goto_0
    return-object v0

    .line 190
    :cond_0
    new-instance v0, Lcom/ibm/icu/impl/ICUResourceBundleReader;

    invoke-direct {v0, v2, v1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 191
    .local v0, "reader":Lcom/ibm/icu/impl/ICUResourceBundleReader;
    goto :goto_0
.end method

.method private readData(Ljava/io/InputStream;)V
    .locals 8
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x5

    const/4 v5, 0x0

    .line 204
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 217
    .local v0, "ds":Ljava/io/DataInputStream;
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->rootRes:I

    .line 220
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 221
    .local v2, "indexLength":I
    add-int/lit8 v4, v2, -0x1

    mul-int/lit8 v4, v4, 0x4

    invoke-virtual {v0, v4}, Ljava/io/DataInputStream;->mark(I)V

    .line 223
    new-array v4, v2, [I

    iput-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->indexes:[I

    .line 224
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->indexes:[I

    aput v2, v4, v5

    .line 226
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 227
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->indexes:[I

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v4, v1

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 232
    :cond_0
    if-le v2, v7, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->indexes:[I

    aget v4, v4, v7

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    iput-boolean v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->noFallback:Z

    .line 239
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->indexes:[I

    const/4 v6, 0x3

    aget v4, v4, v6

    mul-int/lit8 v3, v4, 0x4

    .line 242
    .local v3, "length":I
    new-array v4, v3, [B

    iput-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->data:[B

    .line 243
    iget v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->rootRes:I

    iget-object v6, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->data:[B

    invoke-static {v4, v6, v5}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->writeInt(I[BI)V

    .line 244
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->data:[B

    const/4 v5, 0x4

    invoke-static {v2, v4, v5}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->writeInt(I[BI)V

    .line 247
    invoke-virtual {v0}, Ljava/io/DataInputStream;->reset()V

    .line 248
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->data:[B

    const/16 v5, 0x8

    add-int/lit8 v6, v3, -0x8

    invoke-virtual {v0, v4, v5, v6}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 249
    return-void

    .end local v3    # "length":I
    :cond_1
    move v4, v5

    .line 232
    goto :goto_1
.end method

.method private static writeInt(I[BI)V
    .locals 2
    .param p0, "i"    # I
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 195
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "offset":I
    .local v0, "offset":I
    shr-int/lit8 v1, p0, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 196
    add-int/lit8 p2, v0, 0x1

    .end local v0    # "offset":I
    .restart local p2    # "offset":I
    shr-int/lit8 v1, p0, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 197
    add-int/lit8 v0, p2, 0x1

    .end local p2    # "offset":I
    .restart local v0    # "offset":I
    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 198
    int-to-byte v1, p0

    aput-byte v1, p1, v0

    .line 199
    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->data:[B

    return-object v0
.end method

.method public getNoFallback()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->noFallback:Z

    return v0
.end method

.method public getRootResource()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->rootRes:I

    return v0
.end method

.method public getVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->dataVersion:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->dataVersion:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    iget-object v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->dataVersion:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundleReader;->dataVersion:[B

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    return-object v0
.end method

.method public isDataVersionAcceptable([B)Z
    .locals 3
    .param p1, "version"    # [B

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 286
    aget-byte v2, p1, v1

    if-ne v2, v0, :cond_0

    aget-byte v2, p1, v0

    if-lt v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
