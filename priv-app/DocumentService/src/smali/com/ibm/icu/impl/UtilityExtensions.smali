.class public Lcom/ibm/icu/impl/UtilityExtensions;
.super Ljava/lang/Object;
.source "UtilityExtensions.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appendToRule(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeMatcher;ZLjava/lang/StringBuffer;)V
    .locals 2
    .param p0, "rule"    # Ljava/lang/StringBuffer;
    .param p1, "matcher"    # Lcom/ibm/icu/text/UnicodeMatcher;
    .param p2, "escapeUnprintable"    # Z
    .param p3, "quoteBuf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 43
    if-eqz p1, :cond_0

    .line 44
    invoke-interface {p1, p2}, Lcom/ibm/icu/text/UnicodeMatcher;->toPattern(Z)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2, p3}, Lcom/ibm/icu/impl/UtilityExtensions;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    .line 47
    :cond_0
    return-void
.end method

.method public static appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V
    .locals 2
    .param p0, "rule"    # Ljava/lang/StringBuffer;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isLiteral"    # Z
    .param p3, "escapeUnprintable"    # Z
    .param p4, "quoteBuf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 28
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 30
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {p0, v1, p2, p3, p4}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    return-void
.end method

.method public static formatInput(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Lcom/ibm/icu/text/Replaceable;
    .param p1, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 100
    check-cast p0, Lcom/ibm/icu/text/ReplaceableString;

    .end local p0    # "input":Lcom/ibm/icu/text/Replaceable;
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/UtilityExtensions;->formatInput(Lcom/ibm/icu/text/ReplaceableString;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatInput(Lcom/ibm/icu/text/ReplaceableString;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/String;
    .locals 2
    .param p0, "input"    # Lcom/ibm/icu/text/ReplaceableString;
    .param p1, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 56
    .local v0, "appendTo":Ljava/lang/StringBuffer;
    invoke-static {v0, p0, p1}, Lcom/ibm/icu/impl/UtilityExtensions;->formatInput(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/ReplaceableString;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/StringBuffer;

    .line 57
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/ibm/icu/impl/Utility;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static formatInput(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "appendTo"    # Ljava/lang/StringBuffer;
    .param p1, "input"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 109
    check-cast p1, Lcom/ibm/icu/text/ReplaceableString;

    .end local p1    # "input":Lcom/ibm/icu/text/Replaceable;
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/impl/UtilityExtensions;->formatInput(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/ReplaceableString;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static formatInput(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/ReplaceableString;Lcom/ibm/icu/text/Transliterator$Position;)Ljava/lang/StringBuffer;
    .locals 6
    .param p0, "appendTo"    # Ljava/lang/StringBuffer;
    .param p1, "input"    # Lcom/ibm/icu/text/ReplaceableString;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    const/16 v5, 0x7c

    .line 68
    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    if-ltz v3, :cond_0

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    if-gt v3, v4, :cond_0

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-gt v3, v4, :cond_0

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    if-gt v3, v4, :cond_0

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {p1}, Lcom/ibm/icu/text/ReplaceableString;->length()I

    move-result v4

    if-gt v3, v4, :cond_0

    .line 76
    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {p1, v3, v4}, Lcom/ibm/icu/text/ReplaceableString;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "b":Ljava/lang/String;
    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {p1, v3, v4}, Lcom/ibm/icu/text/ReplaceableString;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "c":Ljava/lang/String;
    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {p1, v3, v4}, Lcom/ibm/icu/text/ReplaceableString;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "d":Ljava/lang/String;
    const/16 v3, 0x7b

    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x7d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 92
    .end local v0    # "b":Ljava/lang/String;
    .end local v1    # "c":Ljava/lang/String;
    .end local v2    # "d":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 87
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "INVALID Position {cs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ", s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ", l="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ", cl="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "} on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
