.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceBinary"
.end annotation


# instance fields
.field private value:[B


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 95
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 96
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->resource:J

    .line 97
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->key:Ljava/lang/String;

    .line 98
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->resPath:Ljava/lang/String;

    .line 99
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->getValue()[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->value:[B

    .line 101
    return-void
.end method

.method private getValue()[B
    .locals 6

    .prologue
    .line 86
    iget-wide v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->resource:J

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->RES_GET_OFFSET(J)I

    move-result v3

    .line 87
    .local v3, "offset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->rawData:[B

    invoke-static {v4, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v2

    .line 88
    .local v2, "length":I
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->getIntOffset(I)I

    move-result v4

    add-int v0, v3, v4

    .line 89
    .local v0, "byteOffset":I
    new-array v1, v2, [B

    .line 91
    .local v1, "dst":[B
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->rawData:[B

    const/4 v5, 0x0

    invoke-static {v4, v0, v1, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    return-object v1
.end method


# virtual methods
.method public getBinary()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->value:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getBinary([B)[B
    .locals 1
    .param p1, "ba"    # [B

    .prologue
    .line 83
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;->value:[B

    return-object v0
.end method
