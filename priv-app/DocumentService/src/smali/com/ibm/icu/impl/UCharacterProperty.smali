.class public final Lcom/ibm/icu/impl/UCharacterProperty;
.super Ljava/lang/Object;
.source "UCharacterProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;
    }
.end annotation


# static fields
.field private static final AGE_SHIFT_:I = 0x18

.field private static final ALPHABETIC_PROPERTY_:I = 0x8

.field private static final ASCII_HEX_DIGIT_PROPERTY_:I = 0x7

.field private static final CGJ:I = 0x34f

.field private static final CR:I = 0xd

.field private static final DASH_PROPERTY_:I = 0x1

.field private static final DATA_BUFFER_SIZE_:I = 0x61a8

.field private static final DATA_FILE_NAME_:Ljava/lang/String; = "data/icudt40b/uprops.icu"

.field private static final DEFAULT_IGNORABLE_CODE_POINT_PROPERTY_:I = 0x13

.field private static final DEL:I = 0x7f

.field private static final DEPRECATED_PROPERTY_:I = 0x14

.field private static final DIACRITIC_PROPERTY_:I = 0xa

.field private static final EXTENDER_PROPERTY_:I = 0xb

.field private static final FIGURESP:I = 0x2007

.field private static final FIRST_NIBBLE_SHIFT_:I = 0x4

.field private static final GC_CC_MASK:I

.field private static final GC_CN_MASK:I

.field private static final GC_CS_MASK:I

.field private static final GC_ZL_MASK:I

.field private static final GC_ZP_MASK:I

.field private static final GC_ZS_MASK:I

.field private static final GC_Z_MASK:I

.field private static final GRAPHEME_BASE_PROPERTY_:I = 0x1a

.field private static final GRAPHEME_EXTEND_PROPERTY_:I = 0xd

.field private static final GRAPHEME_LINK_PROPERTY_:I = 0xe

.field private static final HAIRSP:I = 0x200a

.field private static final HEX_DIGIT_PROPERTY_:I = 0x6

.field private static final HYPHEN_PROPERTY_:I = 0x2

.field private static final IDEOGRAPHIC_PROPERTY_:I = 0x9

.field private static final IDS_BINARY_OPERATOR_PROPERTY_:I = 0xf

.field private static final IDS_TRINARY_OPERATOR_PROPERTY_:I = 0x10

.field private static final ID_CONTINUE_PROPERTY_:I = 0x19

.field private static final ID_START_PROPERTY_:I = 0x18

.field private static final INHSWAP:I = 0x206a

.field private static INSTANCE_:Lcom/ibm/icu/impl/UCharacterProperty; = null

.field private static final LAST_NIBBLE_MASK_:I = 0xf

.field public static final LATIN_CAPITAL_LETTER_I_WITH_DOT_ABOVE_:C = '\u0130'

.field public static final LATIN_SMALL_LETTER_DOTLESS_I_:C = '\u0131'

.field public static final LATIN_SMALL_LETTER_I_:C = 'i'

.field private static final LEAD_SURROGATE_SHIFT_:I = 0xa

.field private static final LOGICAL_ORDER_EXCEPTION_PROPERTY_:I = 0x15

.field private static final MATH_PROPERTY_:I = 0x5

.field static final MY_MASK:I = 0x1e

.field private static final NBSP:I = 0xa0

.field private static final NL:I = 0x85

.field private static final NNBSP:I = 0x202f

.field private static final NOMDIG:I = 0x206f

.field private static final NONCHARACTER_CODE_POINT_PROPERTY_:I = 0xc

.field public static final NT_COUNT:I = 0x6

.field public static final NT_FRACTION:I = 0x4

.field public static final NT_LARGE:I = 0x5

.field private static final PATTERN_SYNTAX:I = 0x1d

.field private static final PATTERN_WHITE_SPACE:I = 0x1e

.field private static final QUOTATION_MARK_PROPERTY_:I = 0x3

.field private static final RADICAL_PROPERTY_:I = 0x11

.field private static final RLM:I = 0x200f

.field public static final SRC_BIDI:I = 0x7

.field public static final SRC_CASE:I = 0x6

.field public static final SRC_CHAR:I = 0x1

.field public static final SRC_CHAR_AND_PROPSVEC:I = 0x8

.field public static final SRC_COUNT:I = 0x9

.field public static final SRC_HST:I = 0x3

.field public static final SRC_NAMES:I = 0x4

.field public static final SRC_NONE:I = 0x0

.field public static final SRC_NORM:I = 0x5

.field public static final SRC_PROPSVEC:I = 0x2

.field private static final SURROGATE_OFFSET_:I = -0x35fdc00

.field private static final S_TERM_PROPERTY_:I = 0x1b

.field private static final TAB:I = 0x9

.field private static final TERMINAL_PUNCTUATION_PROPERTY_:I = 0x4

.field public static final TYPE_MASK:I = 0x1f

.field private static final UNIFIED_IDEOGRAPH_PROPERTY_:I = 0x12

.field private static final UNSIGNED_INT_MASK:J = 0xffffffffL

.field private static final UNSIGNED_VALUE_MASK_AFTER_SHIFT_:I = 0xff

.field private static final U_A:I = 0x41

.field private static final U_F:I = 0x46

.field private static final U_FW_A:I = 0xff21

.field private static final U_FW_F:I = 0xff26

.field private static final U_FW_Z:I = 0xff3a

.field private static final U_FW_a:I = 0xff41

.field private static final U_FW_f:I = 0xff46

.field private static final U_FW_z:I = 0xff5a

.field private static final U_Z:I = 0x5a

.field private static final U_a:I = 0x61

.field private static final U_f:I = 0x66

.field private static final U_z:I = 0x7a

.field private static final VALUE_SHIFT_:I = 0x8

.field private static final VARIATION_SELECTOR_PROPERTY_:I = 0x1c

.field private static final WHITE_SPACE_PROPERTY_:I = 0x0

.field private static final WJ:I = 0x2060

.field private static final XID_CONTINUE_PROPERTY_:I = 0x17

.field private static final XID_START_PROPERTY_:I = 0x16

.field private static final ZWNBSP:I = 0xfeff


# instance fields
.field binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

.field m_additionalColumnsCount_:I

.field m_additionalTrie_:Lcom/ibm/icu/impl/CharTrie;

.field m_additionalVectors_:[I

.field m_maxBlockScriptValue_:I

.field m_maxJTGValue_:I

.field public m_trieData_:[C

.field public m_trieIndex_:[C

.field public m_trieInitialValue_:I

.field public m_trie_:Lcom/ibm/icu/impl/CharTrie;

.field public m_unicodeVersion_:Lcom/ibm/icu/util/VersionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CN_MASK:I

    .line 247
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CC_MASK:I

    .line 248
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CS_MASK:I

    .line 249
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZS_MASK:I

    .line 250
    const/16 v0, 0xd

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZL_MASK:I

    .line 251
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZP_MASK:I

    .line 253
    sget v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZS_MASK:I

    sget v1, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZL_MASK:I

    or-int/2addr v0, v1

    sget v1, Lcom/ibm/icu/impl/UCharacterProperty;->GC_ZP_MASK:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/impl/UCharacterProperty;->GC_Z_MASK:I

    .line 735
    const/4 v0, 0x0

    sput-object v0, Lcom/ibm/icu/impl/UCharacterProperty;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterProperty;

    return-void
.end method

.method private constructor <init>()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x7

    const/4 v12, 0x6

    const/4 v9, 0x5

    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    .line 850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    const/16 v3, 0x31

    new-array v3, v3, [Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const/4 v4, 0x0

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x100

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    new-instance v4, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x80

    invoke-direct {v4, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v4, v3, v8

    const/4 v4, 0x2

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v13, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/4 v4, 0x3

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v13, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/4 v4, 0x4

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x2

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    new-instance v4, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x80000

    invoke-direct {v4, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v4, v3, v9

    new-instance v4, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x100000

    invoke-direct {v4, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v4, v3, v12

    new-instance v4, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x400

    invoke-direct {v4, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v4, v3, v13

    const/16 v4, 0x8

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x800

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x9

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xa

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x4000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xb

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x2000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xc

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x4000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xd

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x40

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xe

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x4

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0xf

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x2000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x10

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x1000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x11

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x200

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x12

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x8000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x13

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x10000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x14

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v13, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x15

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x200000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x16

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v12, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x17

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x20

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x18

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x1000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x19

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x8

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1a

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x20000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1b

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v12, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1c

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x10

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1d

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x40000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1e

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v12, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x1f

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/16 v6, 0x1

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x20

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x800000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x21

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x400000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x22

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v12, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x23

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x8000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x24

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x10000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x25

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x26

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x27

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x28

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x29

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v9, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2a

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x20000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2b

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const-wide/32 v6, 0x40000000

    invoke-direct {v5, v8, v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2c

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    const/16 v6, 0x8

    invoke-direct {v5, v6, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2d

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v8, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2e

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v8, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x2f

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v8, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    const/16 v4, 0x30

    new-instance v5, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    invoke-direct {v5, v8, v10, v11}, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;-><init>(IJ)V

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterProperty;->binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    .line 852
    const-string/jumbo v3, "data/icudt40b/uprops.icu"

    invoke-static {v3}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 853
    .local v1, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v3, 0x61a8

    invoke-direct {v0, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 854
    .local v0, "b":Ljava/io/BufferedInputStream;
    new-instance v2, Lcom/ibm/icu/impl/UCharacterPropertyReader;

    invoke-direct {v2, v0}, Lcom/ibm/icu/impl/UCharacterPropertyReader;-><init>(Ljava/io/InputStream;)V

    .line 855
    .local v2, "reader":Lcom/ibm/icu/impl/UCharacterPropertyReader;
    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/UCharacterPropertyReader;->read(Lcom/ibm/icu/impl/UCharacterProperty;)V

    .line 856
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 858
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v3, p0}, Lcom/ibm/icu/impl/CharTrie;->putIndexData(Lcom/ibm/icu/impl/UCharacterProperty;)V

    .line 859
    return-void
.end method

.method public static getInstance()Lcom/ibm/icu/impl/UCharacterProperty;
    .locals 5

    .prologue
    .line 571
    sget-object v1, Lcom/ibm/icu/impl/UCharacterProperty;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterProperty;

    if-nez v1, :cond_0

    .line 573
    :try_start_0
    new-instance v1, Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-direct {v1}, Lcom/ibm/icu/impl/UCharacterProperty;-><init>()V

    sput-object v1, Lcom/ibm/icu/impl/UCharacterProperty;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterProperty;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 579
    :cond_0
    sget-object v1, Lcom/ibm/icu/impl/UCharacterProperty;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterProperty;

    return-object v1

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method

.method public static final getMask(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 702
    const/4 v0, 0x1

    shl-int/2addr v0, p0

    return v0
.end method

.method public static getRawSupplementary(CC)I
    .locals 2
    .param p0, "lead"    # C
    .param p1, "trail"    # C

    .prologue
    .line 562
    shl-int/lit8 v0, p0, 0xa

    add-int/2addr v0, p1

    const v1, -0x35fdc00

    add-int/2addr v0, v1

    return v0
.end method

.method public static getUnsignedValue(I)I
    .locals 1
    .param p0, "prop"    # I

    .prologue
    .line 189
    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static isRuleWhiteSpace(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 672
    const/16 v0, 0x9

    if-lt p0, v0, :cond_1

    const/16 v0, 0x2029

    if-gt p0, v0, :cond_1

    const/16 v0, 0xd

    if-le p0, v0, :cond_0

    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x85

    if-eq p0, v0, :cond_0

    const/16 v0, 0x200e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x200f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2028

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isgraphPOSIX(I)Z
    .locals 3
    .param p0, "c"    # I

    .prologue
    .line 265
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v0

    sget v1, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CC_MASK:I

    sget v2, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CS_MASK:I

    or-int/2addr v1, v2

    sget v2, Lcom/ibm/icu/impl/UCharacterProperty;->GC_CN_MASK:I

    or-int/2addr v1, v2

    sget v2, Lcom/ibm/icu/impl/UCharacterProperty;->GC_Z_MASK:I

    or-int/2addr v1, v2

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 961
    new-instance v0, Lcom/ibm/icu/impl/TrieIterator;

    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v0, v2}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 962
    .local v0, "propsIter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v1, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v1}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 963
    .local v1, "propsResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 964
    iget v2, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 970
    :cond_0
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 971
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 974
    const/16 v2, 0xe

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 975
    const/16 v2, 0x1c

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 976
    const/16 v2, 0x20

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 977
    const/16 v2, 0x85

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 978
    const/16 v2, 0x86

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 981
    const/16 v2, 0x7f

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 982
    const/16 v2, 0x200a

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 983
    const/16 v2, 0x2010

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 984
    const/16 v2, 0x206a

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 985
    const/16 v2, 0x2070

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 986
    const v2, 0xfeff

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 987
    const v2, 0xff00

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 990
    const/16 v2, 0xa0

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 991
    const/16 v2, 0xa1

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 992
    const/16 v2, 0x2007

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 993
    const/16 v2, 0x2008

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 994
    const/16 v2, 0x202f

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 995
    const/16 v2, 0x2030

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1000
    const/16 v2, 0x3007

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1001
    const/16 v2, 0x3008

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1002
    const/16 v2, 0x4e00

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1003
    const/16 v2, 0x4e01

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1004
    const/16 v2, 0x4e8c

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1005
    const/16 v2, 0x4e8d

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1006
    const/16 v2, 0x4e09

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1007
    const/16 v2, 0x4e0a

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1008
    const/16 v2, 0x56db

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1009
    const/16 v2, 0x56dc

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1010
    const/16 v2, 0x4e94

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1011
    const/16 v2, 0x4e95

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1012
    const/16 v2, 0x516d

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1013
    const/16 v2, 0x516e

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1014
    const/16 v2, 0x4e03

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1015
    const/16 v2, 0x4e04

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1016
    const/16 v2, 0x516b

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1017
    const/16 v2, 0x516c

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1018
    const/16 v2, 0x4e5d

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1019
    const/16 v2, 0x4e5e

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1022
    const/16 v2, 0x61

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1023
    const/16 v2, 0x7b

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1024
    const/16 v2, 0x41

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1025
    const/16 v2, 0x5b

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1026
    const v2, 0xff41

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1027
    const v2, 0xff5b

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1028
    const v2, 0xff21

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1029
    const v2, 0xff3b

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1032
    const/16 v2, 0x67

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1033
    const/16 v2, 0x47

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1034
    const v2, 0xff47

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1035
    const v2, 0xff27

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1038
    const/16 v2, 0x2060

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1039
    const v2, 0xfff0

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1040
    const v2, 0xfffc

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1041
    const/high16 v2, 0xe0000

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1042
    const v2, 0xe1000

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1045
    const/16 v2, 0x34f

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1046
    const/16 v2, 0x350

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1048
    return-object p1
.end method

.method public getAdditional(II)I
    .locals 2
    .param p1, "codepoint"    # I
    .param p2, "column"    # I

    .prologue
    .line 206
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 207
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->getProperty(I)I

    move-result v0

    .line 212
    :goto_0
    return v0

    .line 209
    :cond_0
    if-ltz p2, :cond_1

    iget v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalColumnsCount_:I

    if-lt p2, v0, :cond_2

    .line 210
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalVectors_:[I

    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalTrie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v1

    add-int/2addr v1, p2

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getAge(I)Lcom/ibm/icu/util/VersionInfo;
    .locals 4
    .param p1, "codepoint"    # I

    .prologue
    const/4 v3, 0x0

    .line 238
    invoke-virtual {p0, p1, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v1

    shr-int/lit8 v0, v1, 0x18

    .line 239
    .local v0, "version":I
    shr-int/lit8 v1, v0, 0x4

    and-int/lit8 v1, v1, 0xf

    and-int/lit8 v2, v0, 0xf

    invoke-static {v1, v2, v3, v3}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v1

    return-object v1
.end method

.method public getMaxValues(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 685
    packed-switch p1, :pswitch_data_0

    .line 691
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 687
    :pswitch_1
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_maxBlockScriptValue_:I

    goto :goto_0

    .line 689
    :pswitch_2
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_maxJTGValue_:I

    goto :goto_0

    .line 685
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final getProperty(I)I
    .locals 4
    .param p1, "ch"    # I

    .prologue
    const v2, 0xdbff

    .line 130
    const v1, 0xd800

    if-lt p1, v1, :cond_0

    if-le p1, v2, :cond_1

    const/high16 v1, 0x10000

    if-ge p1, v1, :cond_1

    .line 136
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieData_:[C

    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieIndex_:[C

    shr-int/lit8 v3, p1, 0x5

    aget-char v2, v2, v3

    shl-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, p1, 0x1f

    add-int/2addr v2, v3

    aget-char v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_0
    return v1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    iget v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieInitialValue_:I

    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    if-gt p1, v2, :cond_2

    .line 146
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieData_:[C

    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieIndex_:[C

    shr-int/lit8 v3, p1, 0x5

    add-int/lit16 v3, v3, 0x140

    aget-char v2, v2, v3

    shl-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, p1, 0x1f

    add-int/2addr v2, v3

    aget-char v1, v1, v2

    goto :goto_0

    .line 152
    :cond_2
    const v1, 0x10ffff

    if-gt p1, v1, :cond_3

    .line 156
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v2

    and-int/lit16 v3, p1, 0x3ff

    int-to-char v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/ibm/icu/impl/CharTrie;->getSurrogateValue(CC)C

    move-result v1

    goto :goto_0

    .line 165
    :cond_3
    iget v1, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieInitialValue_:I

    goto :goto_0
.end method

.method public final getSource(I)I
    .locals 6
    .param p1, "which"    # I

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x1

    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 476
    if-gez p1, :cond_1

    .line 548
    :cond_0
    :goto_0
    return v0

    .line 478
    :cond_1
    const/16 v4, 0x31

    if-ge p1, v4, :cond_3

    .line 479
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    aget-object v0, v0, p1

    iget-wide v2, v0, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;->mask:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    move v0, v1

    .line 480
    goto :goto_0

    .line 482
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;->column:I

    goto :goto_0

    .line 484
    :cond_3
    const/16 v4, 0x1000

    if-lt p1, v4, :cond_0

    .line 486
    const/16 v4, 0x1015

    if-ge p1, v4, :cond_4

    .line 487
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 510
    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 490
    goto :goto_0

    .line 493
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 502
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_4
    move v0, v3

    .line 507
    goto :goto_0

    .line 512
    :cond_4
    const/16 v4, 0x4000

    if-ge p1, v4, :cond_5

    .line 513
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 516
    goto :goto_0

    .line 521
    :cond_5
    const/16 v2, 0x400d

    if-ge p1, v2, :cond_0

    .line 522
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 524
    goto :goto_0

    :pswitch_6
    move v0, v3

    .line 527
    goto :goto_0

    .line 537
    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    .line 542
    :pswitch_8
    const/4 v0, 0x4

    goto :goto_0

    .line 487
    nop

    :pswitch_data_0
    .packed-switch 0x1000
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 513
    :sswitch_data_0
    .sparse-switch
        0x2000 -> :sswitch_0
        0x3000 -> :sswitch_0
    .end sparse-switch

    .line 522
    :pswitch_data_1
    .packed-switch 0x4000
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public hasBinaryProperty(II)Z
    .locals 10
    .param p1, "codepoint"    # I
    .param p2, "property"    # I

    .prologue
    .line 363
    if-ltz p2, :cond_0

    const/16 v6, 0x31

    if-gt v6, p2, :cond_1

    .line 365
    :cond_0
    const/4 v6, 0x0

    .line 472
    :goto_0
    return v6

    .line 367
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterProperty;->binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    aget-object v6, v6, p2

    iget-wide v4, v6, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;->mask:J

    .line 368
    .local v4, "mask":J
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterProperty;->binProps:[Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;

    aget-object v6, v6, p2

    iget v1, v6, Lcom/ibm/icu/impl/UCharacterProperty$BinaryProperties;->column:I

    .line 369
    .local v1, "column":I
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_3

    .line 371
    const-wide v6, 0xffffffffL

    invoke-virtual {p0, p1, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v8

    int-to-long v8, v8

    and-long/2addr v6, v8

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 373
    :cond_3
    const/4 v6, 0x6

    if-ne v1, v6, :cond_7

    .line 377
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 381
    .local v2, "csp":Lcom/ibm/icu/impl/UCaseProps;
    sparse-switch p2, :sswitch_data_0

    .line 472
    .end local v2    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    :cond_4
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v3

    .line 379
    .local v3, "e":Ljava/io/IOException;
    const/4 v6, 0x0

    goto :goto_0

    .line 383
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v2    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    :sswitch_0
    const/4 v6, 0x1

    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/UCaseProps;->getType(I)I

    move-result v7

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_0

    .line 385
    :sswitch_1
    const/4 v6, 0x2

    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/UCaseProps;->getType(I)I

    move-result v7

    if-ne v6, v7, :cond_6

    const/4 v6, 0x1

    goto :goto_0

    :cond_6
    const/4 v6, 0x0

    goto :goto_0

    .line 387
    :sswitch_2
    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/UCaseProps;->isSoftDotted(I)Z

    move-result v6

    goto :goto_0

    .line 389
    :sswitch_3
    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/UCaseProps;->isCaseSensitive(I)Z

    move-result v6

    goto :goto_0

    .line 393
    .end local v2    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    :cond_7
    const/4 v6, 0x5

    if-ne v1, v6, :cond_8

    .line 395
    sparse-switch p2, :sswitch_data_1

    goto :goto_1

    .line 397
    :sswitch_4
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->isFullCompositionExclusion(I)Z

    move-result v6

    goto :goto_0

    .line 399
    :sswitch_5
    sget-object v6, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;)Z

    move-result v6

    goto :goto_0

    .line 401
    :sswitch_6
    sget-object v6, Lcom/ibm/icu/text/Normalizer;->NFKD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;)Z

    move-result v6

    goto :goto_0

    .line 403
    :sswitch_7
    sget-object v6, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;)Z

    move-result v6

    goto :goto_0

    .line 405
    :sswitch_8
    sget-object v6, Lcom/ibm/icu/text/Normalizer;->NFKC:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;)Z

    move-result v6

    goto :goto_0

    .line 407
    :sswitch_9
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->isCanonSafeStart(I)Z

    move-result v6

    goto/16 :goto_0

    .line 411
    :cond_8
    const/4 v6, 0x7

    if-ne v1, v6, :cond_9

    .line 415
    :try_start_1
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 419
    .local v0, "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    sparse-switch p2, :sswitch_data_2

    goto :goto_1

    .line 423
    :sswitch_a
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/UBiDiProps;->isBidiControl(I)Z

    move-result v6

    goto/16 :goto_0

    .line 416
    .end local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    :catch_1
    move-exception v3

    .line 417
    .restart local v3    # "e":Ljava/io/IOException;
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 421
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    :sswitch_b
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/UBiDiProps;->isMirrored(I)Z

    move-result v6

    goto/16 :goto_0

    .line 425
    :sswitch_c
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/UBiDiProps;->isJoinControl(I)Z

    move-result v6

    goto/16 :goto_0

    .line 429
    .end local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    :cond_9
    const/4 v6, 0x1

    if-ne v1, v6, :cond_14

    .line 430
    packed-switch p2, :pswitch_data_0

    goto :goto_1

    .line 433
    :pswitch_0
    const/16 v6, 0x9f

    if-gt p1, v6, :cond_c

    .line 434
    const/16 v6, 0x9

    if-eq p1, v6, :cond_a

    const/16 v6, 0x20

    if-ne p1, v6, :cond_b

    :cond_a
    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 437
    :cond_c
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v6

    const/16 v7, 0xc

    if-ne v6, v7, :cond_d

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 440
    :pswitch_1
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterProperty;->isgraphPOSIX(I)Z

    move-result v6

    goto/16 :goto_0

    .line 448
    :pswitch_2
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v6

    const/16 v7, 0xc

    if-eq v6, v7, :cond_e

    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterProperty;->isgraphPOSIX(I)Z

    move-result v6

    if-eqz v6, :cond_f

    :cond_e
    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 451
    :pswitch_3
    const/16 v6, 0x66

    if-gt p1, v6, :cond_10

    const/16 v6, 0x41

    if-lt p1, v6, :cond_10

    const/16 v6, 0x46

    if-le p1, v6, :cond_11

    const/16 v6, 0x61

    if-ge p1, v6, :cond_11

    :cond_10
    const v6, 0xff21

    if-lt p1, v6, :cond_12

    const v6, 0xff46

    if-gt p1, v6, :cond_12

    const v6, 0xff26

    if-le p1, v6, :cond_11

    const v6, 0xff41

    if-lt p1, v6, :cond_12

    .line 455
    :cond_11
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 458
    :cond_12
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v6

    const/16 v7, 0x9

    if-ne v6, v7, :cond_13

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_13
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 462
    :cond_14
    const/16 v6, 0x8

    if-ne v1, v6, :cond_4

    .line 463
    packed-switch p2, :pswitch_data_1

    goto/16 :goto_1

    .line 465
    :pswitch_4
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->isUAlphabetic(I)Z

    move-result v6

    if-nez v6, :cond_15

    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->isDigit(I)Z

    move-result v6

    if-eqz v6, :cond_16

    :cond_15
    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_16
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_0
        0x1b -> :sswitch_2
        0x1e -> :sswitch_1
        0x22 -> :sswitch_3
    .end sparse-switch

    .line 395
    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_4
        0x25 -> :sswitch_5
        0x26 -> :sswitch_6
        0x27 -> :sswitch_7
        0x28 -> :sswitch_8
        0x29 -> :sswitch_9
    .end sparse-switch

    .line 419
    :sswitch_data_2
    .sparse-switch
        0x2 -> :sswitch_a
        0x3 -> :sswitch_b
        0x14 -> :sswitch_c
    .end sparse-switch

    .line 430
    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 463
    :pswitch_data_1
    .packed-switch 0x2c
        :pswitch_4
    .end packed-switch
.end method

.method public setIndexData(Lcom/ibm/icu/impl/CharTrie$FriendAgent;)V
    .locals 1
    .param p1, "friendagent"    # Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    .prologue
    .line 115
    invoke-virtual {p1}, Lcom/ibm/icu/impl/CharTrie$FriendAgent;->getPrivateIndex()[C

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieIndex_:[C

    .line 116
    invoke-virtual {p1}, Lcom/ibm/icu/impl/CharTrie$FriendAgent;->getPrivateData()[C

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieData_:[C

    .line 117
    invoke-virtual {p1}, Lcom/ibm/icu/impl/CharTrie$FriendAgent;->getPrivateInitialValue()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieInitialValue_:I

    .line 118
    return-void
.end method

.method public uhst_addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 5
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const/16 v4, 0x100b

    .line 921
    const/16 v3, 0x1100

    invoke-virtual {p1, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 922
    const/4 v1, 0x1

    .line 923
    .local v1, "value":I
    const/16 v0, 0x115a

    .local v0, "c":I
    :goto_0
    const/16 v3, 0x115f

    if-gt v0, v3, :cond_1

    .line 924
    invoke-static {v0, v4}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyValue(II)I

    move-result v2

    .line 925
    .local v2, "value2":I
    if-eq v1, v2, :cond_0

    .line 926
    move v1, v2

    .line 927
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 923
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 931
    .end local v2    # "value2":I
    :cond_1
    const/16 v3, 0x1160

    invoke-virtual {p1, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 932
    const/4 v1, 0x2

    .line 933
    const/16 v0, 0x11a3

    :goto_1
    const/16 v3, 0x11a7

    if-gt v0, v3, :cond_3

    .line 934
    invoke-static {v0, v4}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyValue(II)I

    move-result v2

    .line 935
    .restart local v2    # "value2":I
    if-eq v1, v2, :cond_2

    .line 936
    move v1, v2

    .line 937
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 933
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 941
    .end local v2    # "value2":I
    :cond_3
    const/16 v3, 0x11a8

    invoke-virtual {p1, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 942
    const/4 v1, 0x3

    .line 943
    const/16 v0, 0x11fa

    :goto_2
    const/16 v3, 0x11ff

    if-gt v0, v3, :cond_5

    .line 944
    invoke-static {v0, v4}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyValue(II)I

    move-result v2

    .line 945
    .restart local v2    # "value2":I
    if-eq v1, v2, :cond_4

    .line 946
    move v1, v2

    .line 947
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 943
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 952
    .end local v2    # "value2":I
    :cond_5
    const v0, 0xac00

    :goto_3
    const v3, 0xd7a4

    if-ge v0, v3, :cond_6

    .line 953
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 954
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 952
    add-int/lit8 v0, v0, 0x1c

    goto :goto_3

    .line 956
    :cond_6
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 957
    return-void
.end method

.method public upropsvec_addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 3
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1053
    iget v2, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalColumnsCount_:I

    if-lez v2, :cond_0

    .line 1055
    new-instance v0, Lcom/ibm/icu/impl/TrieIterator;

    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterProperty;->m_additionalTrie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v0, v2}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 1056
    .local v0, "propsVectorsIter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v1, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v1}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 1057
    .local v1, "propsVectorsResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1058
    iget v2, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 1061
    .end local v0    # "propsVectorsIter":Lcom/ibm/icu/impl/TrieIterator;
    .end local v1    # "propsVectorsResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :cond_0
    return-void
.end method
