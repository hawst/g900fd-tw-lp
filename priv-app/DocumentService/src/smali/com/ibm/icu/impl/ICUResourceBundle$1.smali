.class Lcom/ibm/icu/impl/ICUResourceBundle$1;
.super Ljava/lang/Object;
.source "ICUResourceBundle.java"

# interfaces
.implements Ljava/security/PrivilegedAction;


# instance fields
.field private final val$baseName:Ljava/lang/String;

.field private final val$root:Ljava/lang/ClassLoader;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$baseName:Ljava/lang/String;

    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$root:Ljava/lang/ClassLoader;

    return-void
.end method


# virtual methods
.method public run()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 519
    iget-object v8, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$baseName:Ljava/lang/String;

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$baseName:Ljava/lang/String;

    .line 525
    .local v0, "bn":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v8, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$root:Ljava/lang/ClassLoader;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, "res_index"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, ".txt"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 526
    .local v5, "s":Ljava/io/InputStream;
    if-eqz v5, :cond_1

    .line 527
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .local v4, "lst":Ljava/util/ArrayList;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    const-string/jumbo v9, "ASCII"

    invoke-direct {v8, v5, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 530
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 531
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, "#"

    invoke-virtual {v3, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 532
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 537
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "lst":Ljava/util/ArrayList;
    .end local v5    # "s":Ljava/io/InputStream;
    :catch_0
    move-exception v8

    .line 541
    :cond_1
    iget-object v8, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$root:Ljava/lang/ClassLoader;

    invoke-virtual {v8, v0}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v6

    .line 542
    .local v6, "url":Ljava/net/URL;
    invoke-static {v6}, Lcom/ibm/icu/impl/URLHandler;->get(Ljava/net/URL;)Lcom/ibm/icu/impl/URLHandler;

    move-result-object v2

    .line 543
    .local v2, "handler":Lcom/ibm/icu/impl/URLHandler;
    if-eqz v2, :cond_4

    .line 544
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 545
    .restart local v4    # "lst":Ljava/util/ArrayList;
    new-instance v7, Lcom/ibm/icu/impl/ICUResourceBundle$1$1;

    invoke-direct {v7, p0, v4}, Lcom/ibm/icu/impl/ICUResourceBundle$1$1;-><init>(Lcom/ibm/icu/impl/ICUResourceBundle$1;Ljava/util/ArrayList;)V

    .line 552
    .local v7, "v":Lcom/ibm/icu/impl/URLHandler$URLVisitor;
    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Lcom/ibm/icu/impl/URLHandler;->guide(Lcom/ibm/icu/impl/URLHandler$URLVisitor;Z)V

    .line 556
    .end local v2    # "handler":Lcom/ibm/icu/impl/URLHandler;
    .end local v4    # "lst":Ljava/util/ArrayList;
    .end local v6    # "url":Ljava/net/URL;
    .end local v7    # "v":Lcom/ibm/icu/impl/URLHandler$URLVisitor;
    :cond_2
    :goto_2
    return-object v4

    .line 519
    .end local v0    # "bn":Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v9, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1;->val$baseName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 556
    .restart local v0    # "bn":Ljava/lang/String;
    .restart local v2    # "handler":Lcom/ibm/icu/impl/URLHandler;
    .restart local v6    # "url":Ljava/net/URL;
    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method
