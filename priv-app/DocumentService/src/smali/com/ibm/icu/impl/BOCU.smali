.class public Lcom/ibm/icu/impl/BOCU;
.super Ljava/lang/Object;
.source "BOCU.java"


# static fields
.field private static final SLOPE_LEAD_2_:I = 0x2a

.field private static final SLOPE_LEAD_3_:I = 0x3

.field private static final SLOPE_MAX_:I = 0xff

.field private static final SLOPE_MIDDLE_:I = 0x81

.field private static final SLOPE_MIN_:I = 0x3

.field private static final SLOPE_REACH_NEG_1_:I = -0x50

.field private static final SLOPE_REACH_NEG_2_:I = -0x29ac

.field private static final SLOPE_REACH_NEG_3_:I = -0x2f112

.field private static final SLOPE_REACH_POS_1_:I = 0x50

.field private static final SLOPE_REACH_POS_2_:I = 0x29ab

.field private static final SLOPE_REACH_POS_3_:I = 0x2f111

.field private static final SLOPE_SINGLE_:I = 0x50

.field private static final SLOPE_START_NEG_2_:I = 0x31

.field private static final SLOPE_START_NEG_3_:I = 0x7

.field private static final SLOPE_START_POS_2_:I = 0xd2

.field private static final SLOPE_START_POS_3_:I = 0xfc

.field private static final SLOPE_TAIL_COUNT_:I = 0xfd


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    return-void
.end method

.method public static compress(Ljava/lang/String;[BI)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 100
    const/4 v2, 0x0

    .line 101
    .local v2, "prev":I
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v1

    .line 102
    .local v1, "iterator":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v0

    .line 103
    .local v0, "codepoint":I
    :goto_0
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 104
    const/16 v3, 0x4e00

    if-lt v2, v3, :cond_0

    const v3, 0xa000

    if-lt v2, v3, :cond_1

    .line 105
    :cond_0
    and-int/lit8 v3, v2, -0x80

    add-int/lit8 v2, v3, 0x50

    .line 113
    :goto_1
    sub-int v3, v0, v2

    invoke-static {v3, p1, p2}, Lcom/ibm/icu/impl/BOCU;->writeDiff(I[BI)I

    move-result p2

    .line 114
    move v2, v0

    .line 115
    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v0

    .line 116
    goto :goto_0

    .line 110
    :cond_1
    const/16 v2, 0x7654

    goto :goto_1

    .line 117
    :cond_2
    return p2
.end method

.method public static getCompressionLength(Ljava/lang/String;)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 128
    const/4 v2, 0x0

    .line 129
    .local v2, "prev":I
    const/4 v3, 0x0

    .line 130
    .local v3, "result":I
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v1

    .line 131
    .local v1, "iterator":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v0

    .line 132
    .local v0, "codepoint":I
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    .line 133
    const/16 v4, 0x4e00

    if-lt v2, v4, :cond_0

    const v4, 0xa000

    if-lt v2, v4, :cond_1

    .line 134
    :cond_0
    and-int/lit8 v4, v2, -0x80

    add-int/lit8 v2, v4, 0x50

    .line 142
    :goto_1
    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v0

    .line 143
    sub-int v4, v0, v2

    invoke-static {v4}, Lcom/ibm/icu/impl/BOCU;->lengthOfDiff(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 144
    move v2, v0

    .line 145
    goto :goto_0

    .line 139
    :cond_1
    const/16 v2, 0x7654

    goto :goto_1

    .line 146
    :cond_2
    return v3
.end method

.method private static final getNegDivMod(II)J
    .locals 8
    .param p0, "number"    # I
    .param p1, "factor"    # I

    .prologue
    .line 259
    rem-int v0, p0, p1

    .line 260
    .local v0, "modulo":I
    div-int v1, p0, p1

    int-to-long v2, v1

    .line 261
    .local v2, "result":J
    if-gez v0, :cond_0

    .line 262
    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    .line 263
    add-int/2addr v0, p1

    .line 265
    :cond_0
    const/16 v1, 0x20

    shl-long v4, v2, v1

    int-to-long v6, v0

    or-long/2addr v4, v6

    return-wide v4
.end method

.method private static final lengthOfDiff(I)I
    .locals 4
    .param p0, "diff"    # I

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 352
    const/16 v3, -0x50

    if-lt p0, v3, :cond_3

    .line 353
    const/16 v3, 0x50

    if-gt p0, v3, :cond_1

    .line 354
    const/4 v0, 0x1

    .line 374
    :cond_0
    :goto_0
    return v0

    .line 356
    :cond_1
    const/16 v3, 0x29ab

    if-le p0, v3, :cond_0

    .line 359
    const v0, 0x2f111

    if-gt p0, v0, :cond_2

    move v0, v1

    .line 360
    goto :goto_0

    :cond_2
    move v0, v2

    .line 363
    goto :goto_0

    .line 367
    :cond_3
    const/16 v3, -0x29ac

    if-ge p0, v3, :cond_0

    .line 370
    const v0, -0x2f112

    if-lt p0, v0, :cond_4

    move v0, v1

    .line 371
    goto :goto_0

    :cond_4
    move v0, v2

    .line 374
    goto :goto_0
.end method

.method private static final writeDiff(I[BI)I
    .locals 8
    .param p0, "diff"    # I
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    const/16 v7, 0xfd

    const/16 v6, 0x20

    .line 278
    const/16 v4, -0x50

    if-lt p0, v4, :cond_3

    .line 279
    const/16 v4, 0x50

    if-gt p0, v4, :cond_0

    .line 280
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "offset":I
    .local v3, "offset":I
    add-int/lit16 v4, p0, 0x81

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    move p2, v3

    .line 343
    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    :goto_0
    return p2

    .line 282
    :cond_0
    const/16 v4, 0x29ab

    if-gt p0, v4, :cond_1

    .line 283
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    div-int/lit16 v4, p0, 0xfd

    add-int/lit16 v4, v4, 0xd2

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    .line 285
    add-int/lit8 p2, v3, 0x1

    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    rem-int/lit16 v4, p0, 0xfd

    add-int/lit8 v4, v4, 0x3

    int-to-byte v4, v4

    aput-byte v4, p1, v3

    goto :goto_0

    .line 288
    :cond_1
    const v4, 0x2f111

    if-gt p0, v4, :cond_2

    .line 289
    add-int/lit8 v4, p2, 0x2

    rem-int/lit16 v5, p0, 0xfd

    add-int/lit8 v5, v5, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 291
    div-int/lit16 p0, p0, 0xfd

    .line 292
    add-int/lit8 v4, p2, 0x1

    rem-int/lit16 v5, p0, 0xfd

    add-int/lit8 v5, v5, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 294
    div-int/lit16 v4, p0, 0xfd

    add-int/lit16 v4, v4, 0xfc

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    .line 296
    add-int/lit8 p2, p2, 0x3

    .line 297
    goto :goto_0

    .line 299
    :cond_2
    add-int/lit8 v4, p2, 0x3

    rem-int/lit16 v5, p0, 0xfd

    add-int/lit8 v5, v5, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 301
    div-int/lit16 p0, p0, 0xfd

    .line 302
    rem-int/lit16 v4, p0, 0xfd

    add-int/lit8 v4, v4, 0x3

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    .line 304
    div-int/lit16 p0, p0, 0xfd

    .line 305
    add-int/lit8 v4, p2, 0x1

    rem-int/lit16 v5, p0, 0xfd

    add-int/lit8 v5, v5, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 307
    const/4 v4, -0x1

    aput-byte v4, p1, p2

    .line 308
    add-int/lit8 p2, p2, 0x4

    .line 310
    goto :goto_0

    .line 312
    :cond_3
    invoke-static {p0, v7}, Lcom/ibm/icu/impl/BOCU;->getNegDivMod(II)J

    move-result-wide v0

    .line 313
    .local v0, "division":J
    long-to-int v2, v0

    .line 314
    .local v2, "modulo":I
    const/16 v4, -0x29ac

    if-lt p0, v4, :cond_4

    .line 315
    shr-long v4, v0, v6

    long-to-int p0, v4

    .line 316
    add-int/lit8 v3, p2, 0x1

    .end local p2    # "offset":I
    .restart local v3    # "offset":I
    add-int/lit8 v4, p0, 0x31

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    .line 317
    add-int/lit8 p2, v3, 0x1

    .end local v3    # "offset":I
    .restart local p2    # "offset":I
    add-int/lit8 v4, v2, 0x3

    int-to-byte v4, v4

    aput-byte v4, p1, v3

    goto :goto_0

    .line 319
    :cond_4
    const v4, -0x2f112

    if-lt p0, v4, :cond_5

    .line 320
    add-int/lit8 v4, p2, 0x2

    add-int/lit8 v5, v2, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 321
    shr-long v4, v0, v6

    long-to-int p0, v4

    .line 322
    invoke-static {p0, v7}, Lcom/ibm/icu/impl/BOCU;->getNegDivMod(II)J

    move-result-wide v0

    .line 323
    long-to-int v2, v0

    .line 324
    shr-long v4, v0, v6

    long-to-int p0, v4

    .line 325
    add-int/lit8 v4, p2, 0x1

    add-int/lit8 v5, v2, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 326
    add-int/lit8 v4, p0, 0x7

    int-to-byte v4, v4

    aput-byte v4, p1, p2

    .line 327
    add-int/lit8 p2, p2, 0x3

    .line 328
    goto/16 :goto_0

    .line 330
    :cond_5
    add-int/lit8 v4, p2, 0x3

    add-int/lit8 v5, v2, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 331
    shr-long v4, v0, v6

    long-to-int p0, v4

    .line 332
    invoke-static {p0, v7}, Lcom/ibm/icu/impl/BOCU;->getNegDivMod(II)J

    move-result-wide v0

    .line 333
    long-to-int v2, v0

    .line 334
    shr-long v4, v0, v6

    long-to-int p0, v4

    .line 335
    add-int/lit8 v4, p2, 0x2

    add-int/lit8 v5, v2, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 336
    invoke-static {p0, v7}, Lcom/ibm/icu/impl/BOCU;->getNegDivMod(II)J

    move-result-wide v0

    .line 337
    long-to-int v2, v0

    .line 338
    add-int/lit8 v4, p2, 0x1

    add-int/lit8 v5, v2, 0x3

    int-to-byte v5, v5

    aput-byte v5, p1, v4

    .line 339
    const/4 v4, 0x3

    aput-byte v4, p1, p2

    .line 340
    add-int/lit8 p2, p2, 0x4

    goto/16 :goto_0
.end method
