.class public final Lcom/ibm/icu/impl/UPropertyAliases;
.super Ljava/lang/Object;
.source "UPropertyAliases.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/UPropertyAliases$Builder;,
        Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;,
        Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;,
        Lcom/ibm/icu/impl/UPropertyAliases$ContiguousEnumToShort;,
        Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;,
        Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;
    }
.end annotation


# static fields
.field private static final DATA_BUFFER_SIZE:I = 0x2000

.field private static final DATA_FILE_NAME:Ljava/lang/String; = "data/icudt40b/pnames.icu"

.field private static final DATA_FORMAT_ID:[B

.field private static final DATA_FORMAT_VERSION:B = 0x1t

.field private static DEBUG:Z


# instance fields
.field private enumToName:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

.field private enumToValue:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

.field private nameGroupPool:[S

.field private nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

.field private stringPool:[Ljava/lang/String;

.field private valueMapArray:[Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-string/jumbo v0, "pnames"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/UPropertyAliases;->DEBUG:Z

    .line 94
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/UPropertyAliases;->DATA_FORMAT_ID:[B

    return-void

    :array_0
    .array-data 1
        0x70t
        0x6et
        0x61t
        0x6dt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const-string/jumbo v18, "data/icudt40b/pnames.icu"

    invoke-static/range {v18 .. v18}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    .line 124
    .local v8, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v18, 0x2000

    move/from16 v0, v18

    invoke-direct {v2, v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 126
    .local v2, "b":Ljava/io/BufferedInputStream;
    sget-object v18, Lcom/ibm/icu/impl/UPropertyAliases;->DATA_FORMAT_ID:[B

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-static {v2, v0, v1}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    .line 127
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 131
    .local v4, "d":Ljava/io/DataInputStream;
    const/16 v18, 0x100

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/io/DataInputStream;->mark(I)V

    .line 133
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v5

    .line 134
    .local v5, "enumToName_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v11

    .line 135
    .local v11, "nameToEnum_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    .line 136
    .local v6, "enumToValue_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v15

    .line 137
    .local v15, "total_size":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v17

    .line 138
    .local v17, "valueMap_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v16

    .line 139
    .local v16, "valueMap_count":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v10

    .line 140
    .local v10, "nameGroupPool_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v9

    .line 141
    .local v9, "nameGroupPool_count":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v14

    .line 142
    .local v14, "stringPool_offset":S
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v13

    .line 144
    .local v13, "stringPool_count":S
    sget-boolean v18, Lcom/ibm/icu/impl/UPropertyAliases;->DEBUG:Z

    if-eqz v18, :cond_0

    .line 145
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v20, "enumToName_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "nameToEnum_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "enumToValue_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "total_size="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "valueMap_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "valueMap_count="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "nameGroupPool_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "nameGroupPool_count="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "stringPool_offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "stringPool_count="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 162
    :cond_0
    new-array v12, v15, [B

    .line 163
    .local v12, "raw":[B
    invoke-virtual {v4}, Ljava/io/DataInputStream;->reset()V

    .line 164
    invoke-virtual {v4, v12}, Ljava/io/DataInputStream;->readFully([B)V

    .line 165
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 167
    new-instance v3, Lcom/ibm/icu/impl/UPropertyAliases$Builder;

    invoke-direct {v3, v12}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;-><init>([B)V

    .line 169
    .local v3, "builder":Lcom/ibm/icu/impl/UPropertyAliases$Builder;
    invoke-virtual {v3, v14, v13}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readStringPool(SS)[Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->stringPool:[Ljava/lang/String;

    .line 172
    invoke-virtual {v3, v10, v9}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->readNameGroupPool(SS)[S

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->nameGroupPool:[S

    .line 175
    move/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v3, v0, v1}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->setupValueMap_map(SS)V

    .line 182
    invoke-virtual {v3, v5}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 183
    new-instance v18, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;-><init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/UPropertyAliases;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$000(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V

    .line 186
    invoke-virtual {v3, v11}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 187
    new-instance v18, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;-><init>(Lcom/ibm/icu/impl/UPropertyAliases;Lcom/ibm/icu/impl/UPropertyAliases$Builder;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    .line 189
    invoke-virtual {v3, v6}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 190
    new-instance v18, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;-><init>(Lcom/ibm/icu/impl/ICUBinaryStream;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->enumToValue:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/UPropertyAliases;->enumToValue:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->offsetArray:[S

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$100(Lcom/ibm/icu/impl/UPropertyAliases$Builder;[S)V

    .line 193
    move/from16 v0, v16

    new-array v0, v0, [Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/UPropertyAliases;->valueMapArray:[Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    .line 194
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v7, v0, :cond_1

    .line 196
    invoke-static {v3}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->access$200(Lcom/ibm/icu/impl/UPropertyAliases$Builder;)[S

    move-result-object v18

    aget-short v18, v18, v7

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->seek(I)V

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/UPropertyAliases;->valueMapArray:[Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    move-object/from16 v18, v0

    new-instance v19, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;-><init>(Lcom/ibm/icu/impl/UPropertyAliases;Lcom/ibm/icu/impl/UPropertyAliases$Builder;)V

    aput-object v19, v18, v7

    .line 194
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {v3}, Lcom/ibm/icu/impl/UPropertyAliases$Builder;->close()V

    .line 201
    return-void
.end method

.method static access$300(Lcom/ibm/icu/impl/UPropertyAliases;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/UPropertyAliases;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/ibm/icu/impl/UPropertyAliases;->stringPool:[Ljava/lang/String;

    return-object v0
.end method

.method static access$500()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/ibm/icu/impl/UPropertyAliases;->DEBUG:Z

    return v0
.end method

.method private chooseNameInGroup(SI)Ljava/lang/String;
    .locals 5
    .param p1, "nameGroupIndex"    # S
    .param p2, "nameChoice"    # I

    .prologue
    .line 446
    if-gez p2, :cond_0

    .line 447
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid name choice"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move v1, p2

    .end local p2    # "nameChoice":I
    .local v1, "nameChoice":I
    move v2, p1

    .line 449
    .end local p1    # "nameGroupIndex":S
    .local v2, "nameGroupIndex":S
    add-int/lit8 p2, v1, -0x1

    .end local v1    # "nameChoice":I
    .restart local p2    # "nameChoice":I
    if-lez v1, :cond_1

    .line 450
    iget-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases;->nameGroupPool:[S

    add-int/lit8 v4, v2, 0x1

    int-to-short p1, v4

    .end local v2    # "nameGroupIndex":S
    .restart local p1    # "nameGroupIndex":S
    aget-short v3, v3, v2

    if-gez v3, :cond_0

    .line 451
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid name choice"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 454
    .end local p1    # "nameGroupIndex":S
    .restart local v2    # "nameGroupIndex":S
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases;->nameGroupPool:[S

    aget-short v0, v3, v2

    .line 455
    .local v0, "a":S
    iget-object v3, p0, Lcom/ibm/icu/impl/UPropertyAliases;->stringPool:[Ljava/lang/String;

    if-gez v0, :cond_2

    neg-int v0, v0

    .end local v0    # "a":S
    :cond_2
    aget-object v3, v3, v0

    return-object v3
.end method

.method public static compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p0, "stra"    # Ljava/lang/String;
    .param p1, "strb"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 395
    const/4 v4, 0x0

    .local v4, "istra":I
    const/4 v5, 0x0

    .line 396
    .local v5, "istrb":I
    const/4 v0, 0x0

    .local v0, "cstra":I
    const/4 v1, 0x0

    .line 399
    .local v1, "cstrb":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 400
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 401
    sparse-switch v0, :sswitch_data_0

    .line 410
    :cond_0
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v5, v9, :cond_1

    .line 411
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 412
    sparse-switch v1, :sswitch_data_1

    .line 422
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v4, v9, :cond_2

    move v2, v7

    .line 423
    .local v2, "endstra":Z
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v5, v9, :cond_3

    move v3, v7

    .line 424
    .local v3, "endstrb":Z
    :goto_3
    if-eqz v2, :cond_6

    .line 425
    if-eqz v3, :cond_4

    .line 433
    :goto_4
    return v8

    .line 404
    .end local v2    # "endstra":Z
    .end local v3    # "endstrb":Z
    :sswitch_0
    add-int/lit8 v4, v4, 0x1

    .line 405
    goto :goto_0

    .line 415
    :sswitch_1
    add-int/lit8 v5, v5, 0x1

    .line 416
    goto :goto_1

    :cond_2
    move v2, v8

    .line 422
    goto :goto_2

    .restart local v2    # "endstra":Z
    :cond_3
    move v3, v8

    .line 423
    goto :goto_3

    .line 426
    .restart local v3    # "endstrb":Z
    :cond_4
    const/4 v0, 0x0

    .line 431
    :cond_5
    :goto_5
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v9

    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v10

    sub-int v6, v9, v10

    .line 432
    .local v6, "rc":I
    if-eqz v6, :cond_7

    move v8, v6

    .line 433
    goto :goto_4

    .line 427
    .end local v6    # "rc":I
    :cond_6
    if-eqz v3, :cond_5

    .line 428
    const/4 v1, 0x0

    goto :goto_5

    .line 436
    .restart local v6    # "rc":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    .line 437
    add-int/lit8 v5, v5, 0x1

    .line 438
    goto :goto_0

    .line 401
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x2d -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch

    .line 412
    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xb -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x2d -> :sswitch_1
        0x5f -> :sswitch_1
    .end sparse-switch
.end method

.method private getValueMap(I)Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;
    .locals 2
    .param p1, "property"    # I

    .prologue
    .line 462
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases;->enumToValue:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->getShort(I)S

    move-result v0

    .line 463
    .local v0, "valueMapIndex":I
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases;->valueMapArray:[Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    aget-object v1, v1, v0

    return-object v1
.end method


# virtual methods
.method public getPropertyEnum(Ljava/lang/String;)I
    .locals 1
    .param p1, "propertyAlias"    # Ljava/lang/String;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/ibm/icu/impl/UPropertyAliases;->nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->getEnum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPropertyName(II)Ljava/lang/String;
    .locals 2
    .param p1, "property"    # I
    .param p2, "nameChoice"    # I

    .prologue
    .line 213
    iget-object v1, p0, Lcom/ibm/icu/impl/UPropertyAliases;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/UPropertyAliases$NonContiguousEnumToShort;->getShort(I)S

    move-result v0

    .line 214
    .local v0, "nameGroupIndex":S
    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/impl/UPropertyAliases;->chooseNameInGroup(SI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPropertyValueEnum(ILjava/lang/String;)I
    .locals 2
    .param p1, "property"    # I
    .param p2, "valueAlias"    # Ljava/lang/String;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases;->getValueMap(I)Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    move-result-object v0

    .line 244
    .local v0, "vm":Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;
    iget-object v1, v0, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->nameToEnum:Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;

    invoke-virtual {v1, p2}, Lcom/ibm/icu/impl/UPropertyAliases$NameToEnum;->getEnum(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getPropertyValueName(III)Ljava/lang/String;
    .locals 3
    .param p1, "property"    # I
    .param p2, "value"    # I
    .param p3, "nameChoice"    # I

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases;->getValueMap(I)Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;

    move-result-object v1

    .line 233
    .local v1, "vm":Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;
    iget-object v2, v1, Lcom/ibm/icu/impl/UPropertyAliases$ValueMap;->enumToName:Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;

    invoke-interface {v2, p2}, Lcom/ibm/icu/impl/UPropertyAliases$EnumToShort;->getShort(I)S

    move-result v0

    .line 234
    .local v0, "nameGroupIndex":S
    invoke-direct {p0, v0, p3}, Lcom/ibm/icu/impl/UPropertyAliases;->chooseNameInGroup(SI)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public isDataVersionAcceptable([B)Z
    .locals 3
    .param p1, "version"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 473
    aget-byte v2, p1, v1

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
