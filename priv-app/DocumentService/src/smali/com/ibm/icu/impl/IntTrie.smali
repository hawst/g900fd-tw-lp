.class public Lcom/ibm/icu/impl/IntTrie;
.super Lcom/ibm/icu/impl/Trie;
.source "IntTrie.java"


# instance fields
.field private m_data_:[I

.field private m_initialValue_:I


# direct methods
.method public constructor <init>(IILcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 7
    .param p1, "initialValue"    # I
    .param p2, "leadUnitValue"    # I
    .param p3, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;

    .prologue
    .line 61
    const/16 v5, 0x820

    new-array v5, v5, [C

    const/16 v6, 0x200

    invoke-direct {p0, v5, v6, p3}, Lcom/ibm/icu/impl/Trie;-><init>([CILcom/ibm/icu/impl/Trie$DataManipulate;)V

    .line 69
    const/16 v3, 0x100

    .local v3, "latin1Length":I
    move v1, v3

    .line 70
    .local v1, "dataLength":I
    if-eq p2, p1, :cond_0

    .line 71
    add-int/lit8 v1, v1, 0x20

    .line 73
    :cond_0
    new-array v5, v1, [I

    iput-object v5, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    .line 74
    iput v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataLength_:I

    .line 76
    iput p1, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    .line 83
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 84
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aput p1, v5, v2

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    :cond_1
    if-eq p2, p1, :cond_3

    .line 89
    const/16 v5, 0x40

    int-to-char v0, v5

    .line 90
    .local v0, "block":C
    const/16 v2, 0x6c0

    .line 91
    const/16 v4, 0x6e0

    .line 92
    .local v4, "limit":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 93
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrie;->m_index_:[C

    aput-char v0, v5, v2

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 97
    :cond_2
    add-int/lit8 v4, v3, 0x20

    .line 98
    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_3

    .line 99
    iget-object v5, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aput p2, v5, v2

    .line 98
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 102
    .end local v0    # "block":C
    .end local v4    # "limit":I
    :cond_3
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 2
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/Trie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    .line 40
    invoke-virtual {p0}, Lcom/ibm/icu/impl/IntTrie;->isIntTrie()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Data given does not belong to a int trie."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    return-void
.end method

.method constructor <init>([C[IIILcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 1
    .param p1, "index"    # [C
    .param p2, "data"    # [I
    .param p3, "initialvalue"    # I
    .param p4, "options"    # I
    .param p5, "datamanipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;

    .prologue
    .line 315
    invoke-direct {p0, p1, p4, p5}, Lcom/ibm/icu/impl/Trie;-><init>([CILcom/ibm/icu/impl/Trie$DataManipulate;)V

    .line 316
    iput-object p2, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    .line 317
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    array-length v0, v0

    iput v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataLength_:I

    .line 318
    iput p3, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    .line 319
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 222
    invoke-super {p0, p1}, Lcom/ibm/icu/impl/Trie;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 223
    .local v1, "result":Z
    if-eqz v1, :cond_0

    instance-of v3, p1, Lcom/ibm/icu/impl/IntTrie;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 224
    check-cast v0, Lcom/ibm/icu/impl/IntTrie;

    .line 225
    .local v0, "othertrie":Lcom/ibm/icu/impl/IntTrie;
    iget v3, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    iget v4, v0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    iget-object v4, v0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 231
    .end local v0    # "othertrie":Lcom/ibm/icu/impl/IntTrie;
    :cond_0
    :goto_0
    return v2

    .line 229
    .restart local v0    # "othertrie":Lcom/ibm/icu/impl/IntTrie;
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final getBMPValue(C)I
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 153
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/IntTrie;->getBMPOffset(C)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public final getCodePointValue(I)I
    .locals 3
    .param p1, "ch"    # I

    .prologue
    .line 118
    if-ltz p1, :cond_0

    const v1, 0xd800

    if-ge p1, v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_index_:[C

    shr-int/lit8 v2, p1, 0x5

    aget-char v1, v1, v2

    shl-int/lit8 v1, v1, 0x2

    and-int/lit8 v2, p1, 0x1f

    add-int v0, v1, v2

    .line 122
    .local v0, "offset":I
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aget v1, v1, v0

    .line 127
    :goto_0
    return v1

    .line 126
    .end local v0    # "offset":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/IntTrie;->getCodePointOffset(I)I

    move-result v0

    .line 127
    .restart local v0    # "offset":I
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aget v1, v1, v0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    goto :goto_0
.end method

.method protected final getInitialValue()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    return v0
.end method

.method public final getLatin1LinearValue(C)I
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 210
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    add-int/lit8 v1, p1, 0x20

    aget v0, v0, v1

    return v0
.end method

.method public final getLeadValue(C)I
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 141
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/IntTrie;->getLeadOffset(C)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method protected final getSurrogateOffset(CC)I
    .locals 3
    .param p1, "lead"    # C
    .param p2, "trail"    # C

    .prologue
    .line 264
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v1, :cond_0

    .line 265
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The field DataManipulate in this Trie is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v0

    .line 272
    .local v0, "offset":I
    if-lez v0, :cond_1

    .line 273
    and-int/lit16 v1, p2, 0x3ff

    int-to-char v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/impl/IntTrie;->getRawOffset(IC)I

    move-result v1

    .line 278
    :goto_0
    return v1

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public final getSurrogateValue(CC)I
    .locals 3
    .param p1, "lead"    # C
    .param p2, "trail"    # C

    .prologue
    .line 163
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 164
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Argument characters do not form a supplementary character"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 168
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/impl/IntTrie;->getSurrogateOffset(CC)I

    move-result v0

    .line 171
    .local v0, "offset":I
    if-lez v0, :cond_2

    .line 172
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aget v1, v1, v0

    .line 176
    :goto_0
    return v1

    :cond_2
    iget v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    goto :goto_0
.end method

.method public final getTrailValue(IC)I
    .locals 3
    .param p1, "leadvalue"    # I
    .param p2, "trail"    # C

    .prologue
    .line 189
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v1, :cond_0

    .line 190
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The field DataManipulate in this Trie is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    invoke-interface {v1, p1}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v0

    .line 194
    .local v0, "offset":I
    if-lez v0, :cond_1

    .line 195
    iget-object v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    and-int/lit16 v2, p2, 0x3ff

    int-to-char v2, v2

    invoke-virtual {p0, v0, v2}, Lcom/ibm/icu/impl/IntTrie;->getRawOffset(IC)I

    move-result v2

    aget v1, v1, v2

    .line 198
    :goto_0
    return v1

    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    goto :goto_0
.end method

.method protected final getValue(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 290
    iget-object v0, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    aget v0, v0, p1

    return v0
.end method

.method protected final unserialize(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-super {p0, p1}, Lcom/ibm/icu/impl/Trie;->unserialize(Ljava/io/InputStream;)V

    .line 248
    iget v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataLength_:I

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    .line 249
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 250
    .local v1, "input":Ljava/io/DataInputStream;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_dataLength_:I

    if-ge v0, v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    aput v3, v2, v0

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_data_:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    iput v2, p0, Lcom/ibm/icu/impl/IntTrie;->m_initialValue_:I

    .line 254
    return-void
.end method
