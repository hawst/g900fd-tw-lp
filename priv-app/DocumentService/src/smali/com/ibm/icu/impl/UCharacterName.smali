.class public final Lcom/ibm/icu/impl/UCharacterName;
.super Ljava/lang/Object;
.source "UCharacterName.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;
    }
.end annotation


# static fields
.field static final EXTENDED_CATEGORY_:I = 0x21

.field private static final GROUP_MASK_:I = 0x1f

.field private static final GROUP_SHIFT_:I = 0x5

.field private static INSTANCE_:Lcom/ibm/icu/impl/UCharacterName; = null

.field private static final LEAD_SURROGATE_:I = 0x1f

.field public static final LINES_PER_GROUP_:I = 0x20

.field private static final NAME_BUFFER_SIZE_:I = 0x186a0

.field private static final NAME_FILE_NAME_:Ljava/lang/String; = "data/icudt40b/unames.icu"

.field private static final NON_CHARACTER_:I = 0x1e

.field private static final OFFSET_HIGH_OFFSET_:I = 0x1

.field private static final OFFSET_LOW_OFFSET_:I = 0x2

.field private static final SINGLE_NIBBLE_MAX_:I = 0xb

.field private static final TRAIL_SURROGATE_:I = 0x20

.field private static final TYPE_NAMES_:[Ljava/lang/String;

.field private static final UNKNOWN_TYPE_NAME_:Ljava/lang/String; = "unknown"


# instance fields
.field private m_ISOCommentSet_:[I

.field private m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

.field public m_groupcount_:I

.field private m_groupinfo_:[C

.field private m_grouplengths_:[C

.field private m_groupoffsets_:[C

.field m_groupsize_:I

.field private m_groupstring_:[B

.field private m_maxISOCommentLength_:I

.field private m_maxNameLength_:I

.field private m_nameSet_:[I

.field private m_tokenstring_:[B

.field private m_tokentable_:[C

.field private m_utilIntBuffer_:[I

.field private m_utilStringBuffer_:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1118
    const/4 v0, 0x0

    sput-object v0, Lcom/ibm/icu/impl/UCharacterName;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterName;

    .line 1122
    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "unassigned"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "uppercase letter"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "lowercase letter"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "titlecase letter"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "modifier letter"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "other letter"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "non spacing mark"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "enclosing mark"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "combining spacing mark"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "decimal digit number"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "letter number"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "other number"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "space separator"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "line separator"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "paragraph separator"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "control"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "format"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "private use area"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "surrogate"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "dash punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "start punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "end punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "connector punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "other punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "math symbol"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "currency symbol"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "modifier symbol"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "other symbol"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "initial punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "final punctuation"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "noncharacter"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "lead surrogate"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "trail surrogate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x21

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    .line 963
    iput v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    .line 1045
    new-array v3, v5, [C

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupoffsets_:[C

    .line 1046
    new-array v3, v5, [C

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_grouplengths_:[C

    .line 1093
    new-array v3, v4, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    .line 1098
    new-array v3, v4, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_ISOCommentSet_:[I

    .line 1102
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 1106
    const/4 v3, 0x2

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilIntBuffer_:[I

    .line 1188
    const-string/jumbo v3, "data/icudt40b/unames.icu"

    invoke-static {v3}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 1189
    .local v1, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const v3, 0x186a0

    invoke-direct {v0, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 1190
    .local v0, "b":Ljava/io/BufferedInputStream;
    new-instance v2, Lcom/ibm/icu/impl/UCharacterNameReader;

    invoke-direct {v2, v0}, Lcom/ibm/icu/impl/UCharacterNameReader;-><init>(Ljava/io/InputStream;)V

    .line 1191
    .local v2, "reader":Lcom/ibm/icu/impl/UCharacterNameReader;
    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/UCharacterNameReader;->read(Lcom/ibm/icu/impl/UCharacterName;)V

    .line 1192
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 1193
    return-void
.end method

.method static access$200([ILjava/lang/String;)I
    .locals 1
    .param p0, "x0"    # [I
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/UCharacterName;->add([ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static access$300([ILjava/lang/StringBuffer;)I
    .locals 1
    .param p0, "x0"    # [I
    .param p1, "x1"    # Ljava/lang/StringBuffer;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/UCharacterName;->add([ILjava/lang/StringBuffer;)I

    move-result v0

    return v0
.end method

.method private static add([ILjava/lang/String;)I
    .locals 3
    .param p0, "set"    # [I
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1426
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1428
    .local v1, "result":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1429
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->add([IC)V

    .line 1428
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1431
    :cond_0
    return v1
.end method

.method private static add([ILjava/lang/StringBuffer;)I
    .locals 3
    .param p0, "set"    # [I
    .param p1, "str"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1442
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    .line 1444
    .local v1, "result":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1445
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->add([IC)V

    .line 1444
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1447
    :cond_0
    return v1
.end method

.method private static add([IC)V
    .locals 4
    .param p0, "set"    # [I
    .param p1, "ch"    # C

    .prologue
    .line 1403
    ushr-int/lit8 v0, p1, 0x5

    aget v1, p0, v0

    const/4 v2, 0x1

    and-int/lit8 v3, p1, 0x1f

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    aput v1, p0, v0

    .line 1404
    return-void
.end method

.method private addAlgorithmName(I)I
    .locals 4
    .param p1, "maxlength"    # I

    .prologue
    .line 1459
    const/4 v1, 0x0

    .line 1460
    .local v1, "result":I
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1461
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    invoke-virtual {v2, v3, p1}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->add([II)I

    move-result v1

    .line 1462
    if-le v1, p1, :cond_0

    .line 1463
    move p1, v1

    .line 1460
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1466
    :cond_1
    return p1
.end method

.method private addExtendedName(I)I
    .locals 4
    .param p1, "maxlength"    # I

    .prologue
    .line 1477
    sget-object v2, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1483
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    sget-object v3, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/UCharacterName;->add([ILjava/lang/String;)I

    move-result v2

    add-int/lit8 v1, v2, 0x9

    .line 1484
    .local v1, "length":I
    if-le v1, p1, :cond_0

    .line 1485
    move p1, v1

    .line 1477
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1488
    .end local v1    # "length":I
    :cond_1
    return p1
.end method

.method private addGroupName(I)V
    .locals 13
    .param p1, "maxlength"    # I

    .prologue
    const/16 v10, 0x22

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 1563
    const/4 v5, 0x0

    .line 1564
    .local v5, "maxisolength":I
    new-array v7, v10, [C

    .line 1565
    .local v7, "offsets":[C
    new-array v2, v10, [C

    .line 1566
    .local v2, "lengths":[C
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    array-length v10, v10

    new-array v9, v10, [B

    .line 1570
    .local v9, "tokenlengths":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    if-ge v0, v10, :cond_5

    .line 1571
    invoke-virtual {p0, v0, v7, v2}, Lcom/ibm/icu/impl/UCharacterName;->getGroupLengths(I[C[C)I

    move-result v6

    .line 1575
    .local v6, "offset":I
    const/4 v3, 0x0

    .local v3, "linenumber":I
    :goto_1
    const/16 v10, 0x20

    if-ge v3, v10, :cond_4

    .line 1577
    aget-char v10, v7, v3

    add-int v4, v6, v10

    .line 1578
    .local v4, "lineoffset":I
    aget-char v1, v2, v3

    .line 1579
    .local v1, "length":I
    if-nez v1, :cond_1

    .line 1576
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1584
    :cond_1
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    invoke-direct {p0, v4, v1, v9, v10}, Lcom/ibm/icu/impl/UCharacterName;->addGroupName(II[B[I)[I

    move-result-object v8

    .line 1586
    .local v8, "parsed":[I
    aget v10, v8, v12

    if-le v10, p1, :cond_2

    .line 1588
    aget p1, v8, v12

    .line 1590
    :cond_2
    aget v10, v8, v11

    add-int/2addr v4, v10

    .line 1591
    aget v10, v8, v11

    if-ge v10, v1, :cond_0

    .line 1595
    aget v10, v8, v11

    sub-int/2addr v1, v10

    .line 1597
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    invoke-direct {p0, v4, v1, v9, v10}, Lcom/ibm/icu/impl/UCharacterName;->addGroupName(II[B[I)[I

    move-result-object v8

    .line 1599
    aget v10, v8, v12

    if-le v10, p1, :cond_3

    .line 1601
    aget p1, v8, v12

    .line 1603
    :cond_3
    aget v10, v8, v11

    add-int/2addr v4, v10

    .line 1604
    aget v10, v8, v11

    if-ge v10, v1, :cond_0

    .line 1608
    aget v10, v8, v11

    sub-int/2addr v1, v10

    .line 1610
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_ISOCommentSet_:[I

    invoke-direct {p0, v4, v1, v9, v10}, Lcom/ibm/icu/impl/UCharacterName;->addGroupName(II[B[I)[I

    move-result-object v8

    .line 1612
    aget v10, v8, v11

    if-le v10, v5, :cond_0

    .line 1613
    move v5, v1

    goto :goto_2

    .line 1570
    .end local v1    # "length":I
    .end local v4    # "lineoffset":I
    .end local v8    # "parsed":[I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1619
    .end local v3    # "linenumber":I
    .end local v6    # "offset":I
    :cond_5
    iput v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxISOCommentLength_:I

    .line 1620
    iput p1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    .line 1621
    return-void
.end method

.method private addGroupName(II[B[I)[I
    .locals 10
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "tokenlength"    # [B
    .param p4, "set"    # [I

    .prologue
    const/4 v9, 0x0

    .line 1504
    const/4 v1, 0x0

    .line 1505
    .local v1, "resultnlength":I
    const/4 v2, 0x0

    .line 1506
    .local v2, "resultplength":I
    :goto_0
    if-ge v2, p2, :cond_0

    .line 1507
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v6, p1, v2

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    int-to-char v0, v5

    .line 1508
    .local v0, "b":C
    add-int/lit8 v2, v2, 0x1

    .line 1509
    const/16 v5, 0x3b

    if-ne v0, v5, :cond_1

    .line 1549
    .end local v0    # "b":C
    :cond_0
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilIntBuffer_:[I

    aput v1, v5, v9

    .line 1550
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilIntBuffer_:[I

    const/4 v6, 0x1

    aput v2, v5, v6

    .line 1551
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilIntBuffer_:[I

    return-object v5

    .line 1513
    .restart local v0    # "b":C
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    array-length v5, v5

    if-lt v0, v5, :cond_2

    .line 1514
    invoke-static {p4, v0}, Lcom/ibm/icu/impl/UCharacterName;->add([IC)V

    .line 1515
    add-int/lit8 v1, v1, 0x1

    .line 1516
    goto :goto_0

    .line 1518
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    and-int/lit16 v6, v0, 0xff

    aget-char v4, v5, v6

    .line 1519
    .local v4, "token":C
    const v5, 0xfffe

    if-ne v4, v5, :cond_3

    .line 1521
    shl-int/lit8 v5, v0, 0x8

    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v7, p1, v2

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    int-to-char v0, v5

    .line 1523
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    aget-char v4, v5, v0

    .line 1524
    add-int/lit8 v2, v2, 0x1

    .line 1526
    :cond_3
    const v5, 0xffff

    if-ne v4, v5, :cond_4

    .line 1527
    invoke-static {p4, v0}, Lcom/ibm/icu/impl/UCharacterName;->add([IC)V

    .line 1528
    add-int/lit8 v1, v1, 0x1

    .line 1529
    goto :goto_0

    .line 1533
    :cond_4
    aget-byte v3, p3, v0

    .line 1534
    .local v3, "tlength":B
    if-nez v3, :cond_5

    .line 1535
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v6

    .line 1536
    :try_start_0
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1538
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokenstring_:[B

    invoke-static {v5, v7, v4}, Lcom/ibm/icu/impl/UCharacterUtility;->getNullTermByteSubString(Ljava/lang/StringBuffer;[BI)I

    .line 1541
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-static {p4, v5}, Lcom/ibm/icu/impl/UCharacterName;->add([ILjava/lang/StringBuffer;)I

    move-result v5

    int-to-byte v3, v5

    .line 1542
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1543
    aput-byte v3, p3, v0

    .line 1545
    :cond_5
    add-int/2addr v1, v3

    goto :goto_0

    .line 1542
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method private static contains([IC)Z
    .locals 3
    .param p0, "set"    # [I
    .param p1, "ch"    # C

    .prologue
    const/4 v0, 0x1

    .line 1415
    ushr-int/lit8 v1, p1, 0x5

    aget v1, p0, v1

    and-int/lit8 v2, p1, 0x1f

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private convert([ILcom/ibm/icu/text/UnicodeSet;)V
    .locals 2
    .param p1, "set"    # [I
    .param p2, "uset"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1657
    invoke-virtual {p2}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 1658
    invoke-direct {p0}, Lcom/ibm/icu/impl/UCharacterName;->initNameSetsLengths()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1668
    :cond_0
    return-void

    .line 1663
    :cond_1
    const/16 v0, 0xff

    .local v0, "c":C
    :goto_0
    if-lez v0, :cond_0

    .line 1664
    invoke-static {p1, v0}, Lcom/ibm/icu/impl/UCharacterName;->contains([IC)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1665
    invoke-virtual {p2, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 1663
    :cond_2
    add-int/lit8 v1, v0, -0x1

    int-to-char v0, v1

    goto :goto_0
.end method

.method private getAlgName(II)Ljava/lang/String;
    .locals 5
    .param p1, "ch"    # I
    .param p2, "choice"    # I

    .prologue
    .line 1209
    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    .line 1211
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v2

    .line 1212
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1214
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "index":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1216
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->contains(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1217
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v1, v1, v0

    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p1, v3}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->appendName(ILjava/lang/StringBuffer;)V

    .line 1218
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    .line 1223
    .end local v0    # "index":I
    :goto_1
    return-object v1

    .line 1214
    .restart local v0    # "index":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1221
    :cond_1
    monitor-exit v2

    .line 1223
    .end local v0    # "index":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1221
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getCodepointMSB(I)I
    .locals 1
    .param p0, "codepoint"    # I

    .prologue
    .line 418
    shr-int/lit8 v0, p0, 0x5

    return v0
.end method

.method private static getExtendedChar(Ljava/lang/String;I)I
    .locals 10
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "choice"    # I

    .prologue
    const/4 v7, -0x1

    .line 1357
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x3c

    if-ne v8, v9, :cond_2

    .line 1358
    const/4 v8, 0x2

    if-ne p1, v8, :cond_1

    .line 1359
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v1, v8, -0x1

    .line 1360
    .local v1, "endIndex":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x3e

    if-ne v8, v9, :cond_1

    .line 1361
    const/16 v8, 0x2d

    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 1362
    .local v5, "startIndex":I
    if-ltz v5, :cond_1

    .line 1363
    add-int/lit8 v5, v5, 0x1

    .line 1364
    const/4 v4, -0x1

    .line 1366
    .local v4, "result":I
    :try_start_0
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1375
    const/4 v8, 0x1

    add-int/lit8 v9, v5, -0x1

    invoke-virtual {p0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1376
    .local v6, "type":Ljava/lang/String;
    sget-object v8, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    array-length v3, v8

    .line 1377
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1378
    sget-object v8, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    aget-object v8, v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_0

    .line 1379
    invoke-static {v4}, Lcom/ibm/icu/impl/UCharacterName;->getType(I)I

    move-result v8

    if-ne v8, v2, :cond_1

    .line 1390
    .end local v1    # "endIndex":I
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "result":I
    .end local v5    # "startIndex":I
    .end local v6    # "type":Ljava/lang/String;
    :goto_1
    return v4

    .line 1370
    .restart local v1    # "endIndex":I
    .restart local v4    # "result":I
    .restart local v5    # "startIndex":I
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NumberFormatException;
    move v4, v7

    .line 1371
    goto :goto_1

    .line 1377
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    .restart local v6    # "type":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v1    # "endIndex":I
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "result":I
    .end local v5    # "startIndex":I
    .end local v6    # "type":Ljava/lang/String;
    :cond_1
    move v4, v7

    .line 1388
    goto :goto_1

    .line 1390
    :cond_2
    const/4 v4, -0x2

    goto :goto_1
.end method

.method private getGroupChar(I[CLjava/lang/String;I)I
    .locals 14
    .param p1, "index"    # I
    .param p2, "length"    # [C
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "choice"    # I

    .prologue
    .line 1264
    const/4 v1, 0x0

    .line 1267
    .local v1, "b":B
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v4

    .line 1271
    .local v4, "namelen":I
    const/4 v8, 0x0

    .local v8, "result":I
    :goto_0
    const/16 v10, 0x20

    if-gt v8, v10, :cond_7

    .line 1272
    const/4 v5, 0x0

    .line 1273
    .local v5, "nindex":I
    aget-char v3, p2, v8

    .line 1275
    .local v3, "len":I
    const/4 v10, 0x1

    move/from16 v0, p4

    if-ne v0, v10, :cond_0

    .line 1276
    move v7, p1

    .line 1277
    .local v7, "oldindex":I
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    const/16 v11, 0x3b

    invoke-static {v10, p1, v3, v11}, Lcom/ibm/icu/impl/UCharacterUtility;->skipByteSubString([BIIB)I

    move-result v10

    add-int/2addr p1, v10

    .line 1279
    sub-int v10, p1, v7

    sub-int/2addr v3, v10

    .line 1284
    .end local v7    # "oldindex":I
    :cond_0
    const/4 v2, 0x0

    .local v2, "count":I
    move v6, v5

    .end local v5    # "nindex":I
    .local v6, "nindex":I
    :goto_1
    if-ge v2, v3, :cond_4

    const/4 v10, -0x1

    if-eq v6, v10, :cond_4

    if-ge v6, v4, :cond_4

    .line 1286
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v11, p1, v2

    aget-byte v1, v10, v11

    .line 1287
    add-int/lit8 v2, v2, 0x1

    .line 1289
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    array-length v10, v10

    if-lt v1, v10, :cond_1

    .line 1290
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "nindex":I
    .restart local v5    # "nindex":I
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    and-int/lit16 v11, v1, 0xff

    if-eq v10, v11, :cond_8

    .line 1291
    const/4 v5, -0x1

    move v6, v5

    .line 1292
    .end local v5    # "nindex":I
    .restart local v6    # "nindex":I
    goto :goto_1

    .line 1295
    :cond_1
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    and-int/lit16 v11, v1, 0xff

    aget-char v9, v10, v11

    .line 1296
    .local v9, "token":C
    const v10, 0xfffe

    if-ne v9, v10, :cond_2

    .line 1298
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    shl-int/lit8 v11, v1, 0x8

    iget-object v12, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v13, p1, v2

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    or-int/2addr v11, v12

    aget-char v9, v10, v11

    .line 1300
    add-int/lit8 v2, v2, 0x1

    .line 1302
    :cond_2
    const v10, 0xffff

    if-ne v9, v10, :cond_3

    .line 1303
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "nindex":I
    .restart local v5    # "nindex":I
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v10

    and-int/lit16 v11, v1, 0xff

    if-eq v10, v11, :cond_8

    .line 1304
    const/4 v5, -0x1

    move v6, v5

    .line 1305
    .end local v5    # "nindex":I
    .restart local v6    # "nindex":I
    goto :goto_1

    .line 1309
    :cond_3
    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokenstring_:[B

    move-object/from16 v0, p3

    invoke-static {v0, v10, v6, v9}, Lcom/ibm/icu/impl/UCharacterUtility;->compareNullTermByteSubString(Ljava/lang/String;[BII)I

    move-result v5

    .end local v6    # "nindex":I
    .restart local v5    # "nindex":I
    move v6, v5

    .line 1313
    .end local v5    # "nindex":I
    .restart local v6    # "nindex":I
    goto :goto_1

    .line 1315
    .end local v9    # "token":C
    :cond_4
    if-ne v4, v6, :cond_6

    if-eq v2, v3, :cond_5

    iget-object v10, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v11, p1, v2

    aget-byte v10, v10, v11

    const/16 v11, 0x3b

    if-ne v10, v11, :cond_6

    .line 1322
    .end local v2    # "count":I
    .end local v3    # "len":I
    .end local v6    # "nindex":I
    .end local v8    # "result":I
    :cond_5
    :goto_2
    return v8

    .line 1320
    .restart local v2    # "count":I
    .restart local v3    # "len":I
    .restart local v6    # "nindex":I
    .restart local v8    # "result":I
    :cond_6
    add-int/2addr p1, v3

    .line 1271
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 1322
    .end local v2    # "count":I
    .end local v3    # "len":I
    .end local v6    # "nindex":I
    :cond_7
    const/4 v8, -0x1

    goto :goto_2

    .restart local v2    # "count":I
    .restart local v3    # "len":I
    .restart local v5    # "nindex":I
    :cond_8
    move v6, v5

    .end local v5    # "nindex":I
    .restart local v6    # "nindex":I
    goto :goto_1
.end method

.method private declared-synchronized getGroupChar(Ljava/lang/String;I)I
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "choice"    # I

    .prologue
    const/4 v3, -0x1

    .line 1234
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    if-ge v0, v4, :cond_0

    .line 1237
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupoffsets_:[C

    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_grouplengths_:[C

    invoke-virtual {p0, v0, v4, v5}, Lcom/ibm/icu/impl/UCharacterName;->getGroupLengths(I[C[C)I

    move-result v2

    .line 1241
    .local v2, "startgpstrindex":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_grouplengths_:[C

    invoke-direct {p0, v2, v4, p1, p2}, Lcom/ibm/icu/impl/UCharacterName;->getGroupChar(I[CLjava/lang/String;I)I

    move-result v1

    .line 1243
    .local v1, "result":I
    if-eq v1, v3, :cond_1

    .line 1244
    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    iget v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    mul-int/2addr v4, v0

    aget-char v3, v3, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    shl-int/lit8 v3, v3, 0x5

    or-int/2addr v3, v1

    .line 1248
    .end local v1    # "result":I
    .end local v2    # "startgpstrindex":I
    :cond_0
    monitor-exit p0

    return v3

    .line 1234
    .restart local v1    # "result":I
    .restart local v2    # "startgpstrindex":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "result":I
    .end local v2    # "startgpstrindex":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public static getGroupLimit(I)I
    .locals 1
    .param p0, "msb"    # I

    .prologue
    .line 428
    shl-int/lit8 v0, p0, 0x5

    add-int/lit8 v0, v0, 0x20

    return v0
.end method

.method public static getGroupMin(I)I
    .locals 1
    .param p0, "msb"    # I

    .prologue
    .line 438
    shl-int/lit8 v0, p0, 0x5

    return v0
.end method

.method public static getGroupMinFromCodepoint(I)I
    .locals 1
    .param p0, "codepoint"    # I

    .prologue
    .line 459
    and-int/lit8 v0, p0, -0x20

    return v0
.end method

.method public static getGroupOffset(I)I
    .locals 1
    .param p0, "codepoint"    # I

    .prologue
    .line 448
    and-int/lit8 v0, p0, 0x1f

    return v0
.end method

.method public static getInstance()Lcom/ibm/icu/impl/UCharacterName;
    .locals 5

    .prologue
    .line 58
    sget-object v1, Lcom/ibm/icu/impl/UCharacterName;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v1, :cond_0

    .line 60
    :try_start_0
    new-instance v1, Lcom/ibm/icu/impl/UCharacterName;

    invoke-direct {v1}, Lcom/ibm/icu/impl/UCharacterName;-><init>()V

    sput-object v1, Lcom/ibm/icu/impl/UCharacterName;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 68
    :cond_0
    sget-object v1, Lcom/ibm/icu/impl/UCharacterName;->INSTANCE_:Lcom/ibm/icu/impl/UCharacterName;

    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/util/MissingResourceException;

    const-string/jumbo v2, "Could not construct UCharacterName. Missing unames.icu"

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 64
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method

.method private static getType(I)I
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 1332
    invoke-static {p0}, Lcom/ibm/icu/impl/UCharacterUtility;->isNonCharacter(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1334
    const/16 v0, 0x1e

    .line 1345
    :cond_0
    :goto_0
    return v0

    .line 1336
    :cond_1
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    .line 1337
    .local v0, "result":I
    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    .line 1338
    const v1, 0xdbff

    if-gt p0, v1, :cond_2

    .line 1339
    const/16 v0, 0x1f

    .line 1340
    goto :goto_0

    .line 1342
    :cond_2
    const/16 v0, 0x20

    goto :goto_0
.end method

.method private initNameSetsLengths()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1629
    iget v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    if-lez v2, :cond_0

    .line 1646
    :goto_0
    return v4

    .line 1633
    :cond_0
    const-string/jumbo v0, "0123456789ABCDEF<>-"

    .line 1636
    .local v0, "extra":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_1

    .line 1637
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/UCharacterName;->add([IC)V

    .line 1636
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1641
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->addAlgorithmName(I)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    .line 1643
    iget v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    invoke-direct {p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->addExtendedName(I)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    .line 1645
    iget v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    invoke-direct {p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->addGroupName(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAlgorithmEnd(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 489
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v0, v0, p1

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->access$100(Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)I

    move-result v0

    return v0
.end method

.method public getAlgorithmLength()I
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    array-length v0, v0

    return v0
.end method

.method public getAlgorithmName(II)Ljava/lang/String;
    .locals 5
    .param p1, "index"    # I
    .param p2, "codepoint"    # I

    .prologue
    .line 500
    const/4 v0, 0x0

    .line 501
    .local v0, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v2

    .line 502
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 503
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v1, v1, p1

    iget-object v3, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p2, v3}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->appendName(ILjava/lang/StringBuffer;)V

    .line 504
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 505
    monitor-exit v2

    .line 506
    return-object v0

    .line 505
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAlgorithmStart(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 479
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v0, v0, p1

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->access$000(Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)I

    move-result v0

    return v0
.end method

.method public getCharFromName(ILjava/lang/String;)I
    .locals 6
    .param p1, "choice"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 116
    const/4 v4, 0x3

    if-ge p1, v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v1, v3

    .line 155
    :cond_1
    :goto_0
    return v1

    .line 122
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/ibm/icu/impl/UCharacterName;->getExtendedChar(Ljava/lang/String;I)I

    move-result v1

    .line 123
    .local v1, "result":I
    if-ge v1, v3, :cond_1

    .line 127
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "upperCaseName":Ljava/lang/String;
    if-eq p1, v5, :cond_4

    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "count":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    if-eqz v4, :cond_3

    .line 134
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    array-length v0, v4

    .line 136
    :cond_3
    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_4

    .line 137
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    aget-object v4, v4, v0

    invoke-virtual {v4, v2}, Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;->getChar(Ljava/lang/String;)I

    move-result v1

    .line 138
    if-gez v1, :cond_1

    .line 136
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 144
    .end local v0    # "count":I
    :cond_4
    const/4 v4, 0x2

    if-ne p1, v4, :cond_5

    .line 145
    const/4 v4, 0x0

    invoke-direct {p0, v2, v4}, Lcom/ibm/icu/impl/UCharacterName;->getGroupChar(Ljava/lang/String;I)I

    move-result v1

    .line 147
    if-ne v1, v3, :cond_1

    .line 148
    invoke-direct {p0, v2, v5}, Lcom/ibm/icu/impl/UCharacterName;->getGroupChar(Ljava/lang/String;I)I

    move-result v1

    .line 150
    goto :goto_0

    .line 153
    :cond_5
    invoke-direct {p0, v2, p1}, Lcom/ibm/icu/impl/UCharacterName;->getGroupChar(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public getCharNameCharacters(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_nameSet_:[I

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/impl/UCharacterName;->convert([ILcom/ibm/icu/text/UnicodeSet;)V

    .line 574
    return-void
.end method

.method public getExtendedName(I)Ljava/lang/String;
    .locals 3
    .param p1, "ch"    # I

    .prologue
    .line 318
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 320
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterName;->getType(I)I

    move-result v1

    const/16 v2, 0xf

    if-ne v1, v2, :cond_0

    .line 321
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v0

    .line 324
    :cond_0
    if-nez v0, :cond_1

    .line 325
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UCharacterName;->getExtendedOr10Name(I)Ljava/lang/String;

    move-result-object v0

    .line 328
    :cond_1
    return-object v0
.end method

.method public getExtendedOr10Name(I)Ljava/lang/String;
    .locals 8
    .param p1, "ch"    # I

    .prologue
    .line 364
    const/4 v1, 0x0

    .line 365
    .local v1, "result":Ljava/lang/String;
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterName;->getType(I)I

    move-result v4

    const/16 v5, 0xf

    if-ne v4, v5, :cond_0

    .line 366
    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v1

    .line 369
    :cond_0
    if-nez v1, :cond_3

    .line 370
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterName;->getType(I)I

    move-result v2

    .line 373
    .local v2, "type":I
    sget-object v4, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_1

    .line 374
    const-string/jumbo v1, "unknown"

    .line 379
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v5

    .line 380
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 381
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/16 v6, 0x3c

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 382
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 383
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/16 v6, 0x2d

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 384
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "chStr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    rsub-int/lit8 v3, v4, 0x4

    .line 386
    .local v3, "zeros":I
    :goto_1
    if-lez v3, :cond_2

    .line 387
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/16 v6, 0x30

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    add-int/lit8 v3, v3, -0x1

    .line 389
    goto :goto_1

    .line 377
    .end local v0    # "chStr":Ljava/lang/String;
    .end local v3    # "zeros":I
    :cond_1
    sget-object v4, Lcom/ibm/icu/impl/UCharacterName;->TYPE_NAMES_:[Ljava/lang/String;

    aget-object v1, v4, v2

    goto :goto_0

    .line 390
    .restart local v0    # "chStr":Ljava/lang/String;
    .restart local v3    # "zeros":I
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/16 v6, 0x3e

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 392
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 393
    monitor-exit v5

    .line 395
    .end local v0    # "chStr":Ljava/lang/String;
    .end local v2    # "type":I
    .end local v3    # "zeros":I
    :cond_3
    return-object v1

    .line 393
    .restart local v2    # "type":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public getGroup(I)I
    .locals 5
    .param p1, "codepoint"    # I

    .prologue
    .line 338
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    .line 339
    .local v0, "endGroup":I
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterName;->getCodepointMSB(I)I

    move-result v2

    .line 340
    .local v2, "msb":I
    const/4 v3, 0x0

    .line 344
    .local v3, "result":I
    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-ge v3, v4, :cond_1

    .line 345
    add-int v4, v3, v0

    shr-int/lit8 v1, v4, 0x1

    .line 346
    .local v1, "gindex":I
    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/UCharacterName;->getGroupMSB(I)I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 347
    move v0, v1

    .line 348
    goto :goto_0

    .line 350
    :cond_0
    move v3, v1

    goto :goto_0

    .line 353
    .end local v1    # "gindex":I
    :cond_1
    return v3
.end method

.method public getGroupLengths(I[C[C)I
    .locals 12
    .param p1, "index"    # I
    .param p2, "offsets"    # [C
    .param p3, "lengths"    # [C

    .prologue
    const v11, 0xffff

    const/16 v10, 0x20

    const/4 v9, 0x0

    .line 178
    const v2, 0xffff

    .line 179
    .local v2, "length":C
    const/4 v0, 0x0

    .line 180
    .local v0, "b":B
    const/4 v3, 0x0

    .line 182
    .local v3, "n":B
    iget v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    mul-int/2addr p1, v6

    .line 183
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    add-int/lit8 v7, p1, 0x1

    aget-char v6, v6, v7

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    add-int/lit8 v8, p1, 0x2

    aget-char v7, v7, v8

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/UCharacterUtility;->toInt(CC)I

    move-result v5

    .line 187
    .local v5, "stringoffset":I
    aput-char v9, p2, v9

    .line 191
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v10, :cond_4

    .line 192
    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    aget-byte v0, v6, v5

    .line 193
    const/4 v4, 0x4

    .line 195
    .local v4, "shift":I
    :goto_1
    if-ltz v4, :cond_3

    .line 197
    shr-int v6, v0, v4

    and-int/lit8 v6, v6, 0xf

    int-to-byte v3, v6

    .line 198
    if-ne v2, v11, :cond_0

    const/16 v6, 0xb

    if-le v3, v6, :cond_0

    .line 199
    add-int/lit8 v6, v3, -0xc

    shl-int/lit8 v6, v6, 0x4

    int-to-char v2, v6

    .line 217
    :goto_2
    add-int/lit8 v4, v4, -0x4

    .line 218
    goto :goto_1

    .line 202
    :cond_0
    if-eq v2, v11, :cond_2

    .line 203
    or-int v6, v2, v3

    add-int/lit8 v6, v6, 0xc

    int-to-char v6, v6

    aput-char v6, p3, v1

    .line 209
    :goto_3
    if-ge v1, v10, :cond_1

    .line 210
    add-int/lit8 v6, v1, 0x1

    aget-char v7, p2, v1

    aget-char v8, p3, v1

    add-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, p2, v6

    .line 213
    :cond_1
    const v2, 0xffff

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 206
    :cond_2
    int-to-char v6, v3

    aput-char v6, p3, v1

    goto :goto_3

    .line 191
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 220
    .end local v4    # "shift":I
    :cond_4
    return v5
.end method

.method public getGroupMSB(I)I
    .locals 2
    .param p1, "gindex"    # I

    .prologue
    .line 405
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    if-lt p1, v0, :cond_0

    .line 406
    const/4 v0, -0x1

    .line 408
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    iget v1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    mul-int/2addr v1, p1

    aget-char v0, v0, v1

    goto :goto_0
.end method

.method public declared-synchronized getGroupName(II)Ljava/lang/String;
    .locals 6
    .param p1, "ch"    # I
    .param p2, "choice"    # I

    .prologue
    .line 517
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterName;->getCodepointMSB(I)I

    move-result v2

    .line 518
    .local v2, "msb":I
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UCharacterName;->getGroup(I)I

    move-result v0

    .line 521
    .local v0, "group":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    iget v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    mul-int/2addr v5, v0

    aget-char v4, v4, v5

    if-ne v2, v4, :cond_0

    .line 522
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupoffsets_:[C

    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_grouplengths_:[C

    invoke-virtual {p0, v0, v4, v5}, Lcom/ibm/icu/impl/UCharacterName;->getGroupLengths(I[C[C)I

    move-result v1

    .line 524
    .local v1, "index":I
    and-int/lit8 v3, p1, 0x1f

    .line 525
    .local v3, "offset":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupoffsets_:[C

    aget-char v4, v4, v3

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_grouplengths_:[C

    aget-char v5, v5, v3

    invoke-virtual {p0, v4, v5, p2}, Lcom/ibm/icu/impl/UCharacterName;->getGroupName(III)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 529
    .end local v1    # "index":I
    .end local v3    # "offset":I
    :goto_0
    monitor-exit p0

    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 517
    .end local v0    # "group":I
    .end local v2    # "msb":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public getGroupName(III)Ljava/lang/String;
    .locals 11
    .param p1, "index"    # I
    .param p2, "length"    # I
    .param p3, "choice"    # I

    .prologue
    const v10, 0xffff

    const/4 v5, 0x3

    const/16 v9, 0x3b

    .line 239
    const/4 v4, 0x1

    if-eq p3, v4, :cond_0

    if-ne p3, v5, :cond_2

    .line 241
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    array-length v4, v4

    if-ge v9, v4, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    aget-char v4, v4, v9

    if-ne v4, v10, :cond_4

    .line 243
    :cond_1
    move v2, p1

    .line 244
    .local v2, "oldindex":I
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    invoke-static {v4, p1, p2, v9}, Lcom/ibm/icu/impl/UCharacterUtility;->skipByteSubString([BIIB)I

    move-result v4

    add-int/2addr p1, v4

    .line 246
    sub-int v4, p1, v2

    sub-int/2addr p2, v4

    .line 247
    if-ne p3, v5, :cond_2

    .line 249
    move v2, p1

    .line 250
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    invoke-static {v4, p1, p2, v9}, Lcom/ibm/icu/impl/UCharacterUtility;->skipByteSubString([BIIB)I

    move-result v4

    add-int/2addr p1, v4

    .line 252
    sub-int v4, p1, v2

    sub-int/2addr p2, v4

    .line 263
    .end local v2    # "oldindex":I
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    monitor-enter v5

    .line 264
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 267
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p2, :cond_3

    .line 268
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v6, p1, v1

    aget-byte v0, v4, v6

    .line 269
    .local v0, "b":B
    add-int/lit8 v1, v1, 0x1

    .line 271
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    array-length v4, v4

    if-lt v0, v4, :cond_6

    .line 272
    if-ne v0, v9, :cond_5

    .line 306
    .end local v0    # "b":B
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_a

    .line 307
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    monitor-exit v5

    .line 310
    :goto_2
    return-object v4

    .line 259
    .end local v1    # "i":I
    :cond_4
    const/4 p2, 0x0

    goto :goto_0

    .line 275
    .restart local v0    # "b":B
    .restart local v1    # "i":I
    :cond_5
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 309
    .end local v0    # "b":B
    .end local v1    # "i":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 278
    .restart local v0    # "b":B
    .restart local v1    # "i":I
    :cond_6
    :try_start_1
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    and-int/lit16 v6, v0, 0xff

    aget-char v3, v4, v6

    .line 279
    .local v3, "token":C
    const v4, 0xfffe

    if-ne v3, v4, :cond_7

    .line 281
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    shl-int/lit8 v6, v0, 0x8

    iget-object v7, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    add-int v8, p1, v1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    aget-char v3, v4, v6

    .line 283
    add-int/lit8 v1, v1, 0x1

    .line 285
    :cond_7
    if-ne v3, v10, :cond_9

    .line 286
    if-ne v0, v9, :cond_8

    .line 290
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x2

    if-ne p3, v4, :cond_3

    goto :goto_1

    .line 297
    :cond_8
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    and-int/lit16 v6, v0, 0xff

    int-to-char v6, v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 300
    :cond_9
    iget-object v4, p0, Lcom/ibm/icu/impl/UCharacterName;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    iget-object v6, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokenstring_:[B

    invoke-static {v4, v6, v3}, Lcom/ibm/icu/impl/UCharacterUtility;->getNullTermByteSubString(Ljava/lang/StringBuffer;[BI)I

    goto :goto_1

    .line 309
    .end local v0    # "b":B
    .end local v3    # "token":C
    :cond_a
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public getISOCommentCharacters(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 584
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_ISOCommentSet_:[I

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/impl/UCharacterName;->convert([ILcom/ibm/icu/text/UnicodeSet;)V

    .line 585
    return-void
.end method

.method public getMaxCharNameLength()I
    .locals 1

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/ibm/icu/impl/UCharacterName;->initNameSetsLengths()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxNameLength_:I

    .line 545
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaxISOCommentLength()I
    .locals 1

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/ibm/icu/impl/UCharacterName;->initNameSetsLengths()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget v0, p0, Lcom/ibm/icu/impl/UCharacterName;->m_maxISOCommentLength_:I

    .line 561
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getName(II)Ljava/lang/String;
    .locals 2
    .param p1, "ch"    # I
    .param p2, "choice"    # I

    .prologue
    .line 85
    if-ltz p1, :cond_0

    const v1, 0x10ffff

    if-gt p1, v1, :cond_0

    const/4 v1, 0x3

    if-le p2, v1, :cond_2

    .line 87
    :cond_0
    const/4 v0, 0x0

    .line 103
    :cond_1
    :goto_0
    return-object v0

    .line 90
    :cond_2
    const/4 v0, 0x0

    .line 92
    .local v0, "result":Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/UCharacterName;->getAlgName(II)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 96
    :cond_3
    const/4 v1, 0x2

    if-ne p2, v1, :cond_4

    .line 97
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/UCharacterName;->getExtendedName(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    goto :goto_0

    .line 99
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/impl/UCharacterName;->getGroupName(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method setAlgorithm([Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;)Z
    .locals 1
    .param p1, "alg"    # [Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    .prologue
    .line 991
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    .line 992
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_algorithm_:[Lcom/ibm/icu/impl/UCharacterName$AlgorithmName;

    .line 993
    const/4 v0, 0x1

    .line 995
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setGroup([C[B)Z
    .locals 1
    .param p1, "group"    # [C
    .param p2, "groupstring"    # [B

    .prologue
    .line 1022
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 1024
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupinfo_:[C

    .line 1025
    iput-object p2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupstring_:[B

    .line 1026
    const/4 v0, 0x1

    .line 1028
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setGroupCountSize(II)Z
    .locals 1
    .param p1, "count"    # I
    .param p2, "size"    # I

    .prologue
    .line 1006
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 1007
    :cond_0
    const/4 v0, 0x0

    .line 1011
    :goto_0
    return v0

    .line 1009
    :cond_1
    iput p1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    .line 1010
    iput p2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_groupsize_:I

    .line 1011
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setToken([C[B)Z
    .locals 1
    .param p1, "token"    # [C
    .param p2, "tokenstring"    # [B

    .prologue
    .line 975
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 977
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokentable_:[C

    .line 978
    iput-object p2, p0, Lcom/ibm/icu/impl/UCharacterName;->m_tokenstring_:[B

    .line 979
    const/4 v0, 0x1

    .line 981
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
