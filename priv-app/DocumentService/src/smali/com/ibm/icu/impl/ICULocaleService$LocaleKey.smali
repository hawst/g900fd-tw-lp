.class public Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
.super Lcom/ibm/icu/impl/ICUService$Key;
.source "ICULocaleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICULocaleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocaleKey"
.end annotation


# static fields
.field public static final KIND_ANY:I = -0x1


# instance fields
.field private currentID:Ljava/lang/String;

.field private fallbackID:Ljava/lang/String;

.field private kind:I

.field private primaryID:Ljava/lang/String;

.field private varstart:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "primaryID"    # Ljava/lang/String;
    .param p2, "canonicalPrimaryID"    # Ljava/lang/String;
    .param p3, "canonicalFallbackID"    # Ljava/lang/String;
    .param p4, "kind"    # I

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/ICUService$Key;-><init>(Ljava/lang/String;)V

    .line 213
    iput p4, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind:I

    .line 214
    if-nez p2, :cond_0

    .line 215
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    const-string/jumbo v1, ""

    if-ne v0, v1, :cond_1

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    .line 230
    :goto_1
    iget v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    .line 231
    return-void

    .line 217
    :cond_0
    iput-object p2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    .line 218
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    goto :goto_0

    .line 223
    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 224
    :cond_2
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    goto :goto_1

    .line 226
    :cond_3
    iput-object p3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    goto :goto_1

    .line 230
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    const/4 v1, 0x0

    iget v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static createWithCanonical(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;I)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    .locals 2
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "canonicalFallbackID"    # Ljava/lang/String;
    .param p2, "kind"    # I

    .prologue
    .line 197
    if-nez p0, :cond_0

    .line 198
    const/4 v1, 0x0

    .line 201
    :goto_0
    return-object v1

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/util/ULocale;->getName()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "canonicalPrimaryID":Ljava/lang/String;
    new-instance v1, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    invoke-direct {v1, v0, v0, p1, p2}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static createWithCanonicalFallback(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    .locals 1
    .param p0, "primaryID"    # Ljava/lang/String;
    .param p1, "canonicalFallbackID"    # Ljava/lang/String;

    .prologue
    .line 176
    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->createWithCanonicalFallback(Ljava/lang/String;Ljava/lang/String;I)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    move-result-object v0

    return-object v0
.end method

.method public static createWithCanonicalFallback(Ljava/lang/String;Ljava/lang/String;I)Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    .locals 2
    .param p0, "primaryID"    # Ljava/lang/String;
    .param p1, "canonicalFallbackID"    # Ljava/lang/String;
    .param p2, "kind"    # I

    .prologue
    .line 183
    if-nez p0, :cond_0

    .line 184
    const/4 v1, 0x0

    .line 190
    :goto_0
    return-object v1

    .line 186
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 187
    const-string/jumbo p0, "root"

    .line 189
    :cond_1
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "canonicalPrimaryID":Ljava/lang/String;
    new-instance v1, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public canonicalID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    return-object v0
.end method

.method public canonicalLocale()Lcom/ibm/icu/util/ULocale;
    .locals 2

    .prologue
    .line 286
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public currentDescriptor()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 266
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID()Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "result":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 268
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 269
    .local v0, "buf":Ljava/lang/StringBuffer;
    iget v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind:I

    if-eq v2, v3, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->prefix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 272
    :cond_0
    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 273
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    iget v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    if-eq v2, v3, :cond_1

    .line 275
    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    iget v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    iget-object v4, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 277
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 279
    .end local v0    # "buf":Ljava/lang/StringBuffer;
    :cond_2
    return-object v1
.end method

.method public currentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    return-object v0
.end method

.method public currentLocale()Lcom/ibm/icu/util/ULocale;
    .locals 4

    .prologue
    .line 293
    iget v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 294
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    iget-object v1, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    .line 296
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->primaryID:Ljava/lang/String;

    iget v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->varstart:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public fallback()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x5f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310
    iget-object v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 311
    .local v0, "x":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 312
    :cond_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v4, :cond_0

    .line 314
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    .line 328
    :goto_0
    return v1

    .line 317
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 318
    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 319
    const-string/jumbo v2, "root"

    iput-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    .line 320
    iput-object v5, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    goto :goto_0

    .line 322
    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    iput-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    .line 323
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->fallbackID:Ljava/lang/String;

    goto :goto_0

    .line 327
    :cond_4
    iput-object v5, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->currentID:Ljava/lang/String;

    move v1, v2

    .line 328
    goto :goto_0
.end method

.method public isFallbackOf(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->canonicalID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/impl/LocaleUtility;->isFallbackOf(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public kind()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind:I

    return v0
.end method

.method public prefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 237
    iget v0, p0, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
