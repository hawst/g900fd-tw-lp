.class public final Lcom/ibm/icu/impl/StringPrepDataReader;
.super Ljava/lang/Object;
.source "StringPrepDataReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# static fields
.field private static final DATA_FORMAT_ID:[B

.field private static final DATA_FORMAT_VERSION:[B

.field private static final debug:Z


# instance fields
.field private dataInputStream:Ljava/io/DataInputStream;

.field private unicodeVersion:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 27
    const-string/jumbo v0, "NormalizerDataReader"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/StringPrepDataReader;->debug:Z

    .line 95
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_ID:[B

    .line 97
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_VERSION:[B

    return-void

    .line 95
    nop

    :array_0
    .array-data 1
        0x53t
        0x50t
        0x52t
        0x50t
    .end array-data

    .line 97
    :array_1
    .array-data 1
        0x3t
        0x2t
        0x5t
        0x2t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-boolean v0, Lcom/ibm/icu/impl/StringPrepDataReader;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes in inputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 38
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_ID:[B

    invoke-static {p1, v0, p0}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->unicodeVersion:[B

    .line 40
    sget-boolean v0, Lcom/ibm/icu/impl/StringPrepDataReader;->debug:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes left in inputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 42
    :cond_1
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->dataInputStream:Ljava/io/DataInputStream;

    .line 44
    sget-boolean v0, Lcom/ibm/icu/impl/StringPrepDataReader;->debug:Z

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bytes left in dataInputStream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 45
    :cond_2
    return-void
.end method


# virtual methods
.method public getDataFormatVersion()[B
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_VERSION:[B

    return-object v0
.end method

.method public getUnicodeVersion()[B
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->unicodeVersion:[B

    return-object v0
.end method

.method public isDataVersionAcceptable([B)Z
    .locals 5
    .param p1, "version"    # [B

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 65
    aget-byte v1, p1, v0

    sget-object v2, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v0

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v3

    sget-object v2, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v3

    if-ne v1, v2, :cond_0

    aget-byte v1, p1, v4

    sget-object v2, Lcom/ibm/icu/impl/StringPrepDataReader;->DATA_FORMAT_VERSION:[B

    aget-byte v2, v2, v4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public read([B[C)V
    .locals 2
    .param p1, "idnaBytes"    # [B
    .param p2, "mappingTable"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v1, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v1, p1}, Ljava/io/DataInputStream;->readFully([B)V

    .line 55
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readChar()C

    move-result v1

    aput-char v1, p2, v0

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public readIndexes(I)[I
    .locals 3
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    new-array v1, p1, [I

    .line 72
    .local v1, "indexes":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 73
    iget-object v2, p0, Lcom/ibm/icu/impl/StringPrepDataReader;->dataInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    aput v2, v1, v0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-object v1
.end method
