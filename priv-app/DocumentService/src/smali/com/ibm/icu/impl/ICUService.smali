.class public Lcom/ibm/icu/impl/ICUService;
.super Lcom/ibm/icu/impl/ICUNotifier;
.source "ICUService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ICUService$ServiceListener;,
        Lcom/ibm/icu/impl/ICUService$LocaleRef;,
        Lcom/ibm/icu/impl/ICUService$CacheEntry;,
        Lcom/ibm/icu/impl/ICUService$SimpleFactory;,
        Lcom/ibm/icu/impl/ICUService$Factory;,
        Lcom/ibm/icu/impl/ICUService$Key;
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field private cacheref:Ljava/lang/ref/SoftReference;

.field private defaultSize:I

.field private dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

.field private final factories:Ljava/util/List;

.field private final factoryLock:Lcom/ibm/icu/impl/ICURWLock;

.field private idref:Ljava/lang/ref/SoftReference;

.field protected final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    const-string/jumbo v0, "service"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUNotifier;-><init>()V

    .line 117
    new-instance v0, Lcom/ibm/icu/impl/ICURWLock;

    invoke-direct {v0}, Lcom/ibm/icu/impl/ICURWLock;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/ICUService;->defaultSize:I

    .line 101
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUNotifier;-><init>()V

    .line 117
    new-instance v0, Lcom/ibm/icu/impl/ICURWLock;

    invoke-direct {v0}, Lcom/ibm/icu/impl/ICURWLock;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/ICUService;->defaultSize:I

    .line 109
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    .line 110
    return-void
.end method

.method private getVisibleIDMap()Ljava/util/Map;
    .locals 7

    .prologue
    .line 589
    const/4 v1, 0x0

    .line 590
    .local v1, "idcache":Ljava/util/Map;
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;

    .line 591
    .local v4, "ref":Ljava/lang/ref/SoftReference;
    if-eqz v4, :cond_4

    .line 592
    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "idcache":Ljava/util/Map;
    check-cast v1, Ljava/util/Map;

    .restart local v1    # "idcache":Ljava/util/Map;
    move-object v2, v1

    .line 594
    .end local v1    # "idcache":Ljava/util/Map;
    .local v2, "idcache":Ljava/util/Map;
    :goto_0
    if-nez v2, :cond_3

    .line 595
    monitor-enter p0

    .line 596
    :try_start_0
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;

    if-eq v4, v5, :cond_0

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v5, :cond_2

    .line 600
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ICURWLock;->acquireRead()V

    .line 601
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 602
    .end local v2    # "idcache":Ljava/util/Map;
    .restart local v1    # "idcache":Ljava/util/Map;
    :try_start_2
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    iget-object v6, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v5, v6}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 603
    .local v3, "lIter":Ljava/util/ListIterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 604
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUService$Factory;

    .line 605
    .local v0, "f":Lcom/ibm/icu/impl/ICUService$Factory;
    invoke-interface {v0, v1}, Lcom/ibm/icu/impl/ICUService$Factory;->updateVisibleIDs(Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 611
    .end local v0    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    .end local v3    # "lIter":Ljava/util/ListIterator;
    :catchall_0
    move-exception v5

    :goto_2
    :try_start_3
    iget-object v6, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v6}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 612
    throw v5

    .line 620
    :catchall_1
    move-exception v5

    :goto_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 607
    .restart local v3    # "lIter":Ljava/util/ListIterator;
    :cond_1
    :try_start_4
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 608
    new-instance v5, Ljava/lang/ref/SoftReference;

    invoke-direct {v5, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v5, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 611
    :try_start_5
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 620
    .end local v3    # "lIter":Ljava/util/ListIterator;
    :goto_4
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v2, v1

    .line 621
    .end local v1    # "idcache":Ljava/util/Map;
    .restart local v2    # "idcache":Ljava/util/Map;
    goto :goto_0

    .line 617
    :cond_2
    :try_start_6
    iget-object v4, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;

    .line 618
    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v2    # "idcache":Ljava/util/Map;
    .restart local v1    # "idcache":Ljava/util/Map;
    goto :goto_4

    .line 623
    .end local v1    # "idcache":Ljava/util/Map;
    .restart local v2    # "idcache":Ljava/util/Map;
    :cond_3
    return-object v2

    .line 620
    :catchall_2
    move-exception v5

    move-object v1, v2

    .end local v2    # "idcache":Ljava/util/Map;
    .restart local v1    # "idcache":Ljava/util/Map;
    goto :goto_3

    .line 611
    .end local v1    # "idcache":Ljava/util/Map;
    .restart local v2    # "idcache":Ljava/util/Map;
    :catchall_3
    move-exception v5

    move-object v1, v2

    .end local v2    # "idcache":Ljava/util/Map;
    .restart local v1    # "idcache":Ljava/util/Map;
    goto :goto_2

    :cond_4
    move-object v2, v1

    .end local v1    # "idcache":Ljava/util/Map;
    .restart local v2    # "idcache":Ljava/util/Map;
    goto :goto_0
.end method


# virtual methods
.method protected acceptsListener(Ljava/util/EventListener;)Z
    .locals 1
    .param p1, "l"    # Ljava/util/EventListener;

    .prologue
    .line 953
    instance-of v0, p1, Lcom/ibm/icu/impl/ICUService$ServiceListener;

    return v0
.end method

.method protected clearCaches()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 921
    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->cacheref:Ljava/lang/ref/SoftReference;

    .line 922
    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->idref:Ljava/lang/ref/SoftReference;

    .line 923
    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    .line 924
    return-void
.end method

.method protected clearServiceCache()V
    .locals 1

    .prologue
    .line 933
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUService;->cacheref:Ljava/lang/ref/SoftReference;

    .line 934
    return-void
.end method

.method public createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 907
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/ibm/icu/impl/ICUService$Key;

    invoke-direct {v0, p1}, Lcom/ibm/icu/impl/ICUService$Key;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final factories()Ljava/util/List;
    .locals 2

    .prologue
    .line 783
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock;->acquireRead()V

    .line 784
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 788
    return-object v0

    .line 787
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 788
    throw v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "descriptor"    # Ljava/lang/String;

    .prologue
    .line 343
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/impl/ICUService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "descriptor"    # Ljava/lang/String;
    .param p2, "actualReturn"    # [Ljava/lang/String;

    .prologue
    .line 351
    if-nez p1, :cond_0

    .line 352
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "descriptor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/ibm/icu/impl/ICUService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 632
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/impl/ICUService;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 641
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUService;->getVisibleIDMap()Ljava/util/Map;

    move-result-object v2

    .line 642
    .local v2, "m":Ljava/util/Map;
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUService$Factory;

    .line 643
    .local v0, "f":Lcom/ibm/icu/impl/ICUService$Factory;
    if-eqz v0, :cond_0

    .line 644
    invoke-interface {v0, p1, p2}, Lcom/ibm/icu/impl/ICUService$Factory;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v3

    .line 655
    :goto_0
    return-object v3

    .line 647
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v1

    .line 648
    .local v1, "key":Lcom/ibm/icu/impl/ICUService$Key;
    :cond_1
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUService$Key;->fallback()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 649
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUService$Key;->currentID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    check-cast v0, Lcom/ibm/icu/impl/ICUService$Factory;

    .line 650
    .restart local v0    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    if-eqz v0, :cond_1

    .line 651
    invoke-interface {v0, p1, p2}, Lcom/ibm/icu/impl/ICUService$Factory;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 655
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getDisplayNames()Ljava/util/SortedMap;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 664
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 665
    .local v0, "locale":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {p0, v0, v1, v1}, Lcom/ibm/icu/impl/ICUService;->getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;Ljava/lang/String;)Ljava/util/SortedMap;

    move-result-object v1

    return-object v1
.end method

.method public getDisplayNames(Lcom/ibm/icu/util/ULocale;)Ljava/util/SortedMap;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x0

    .line 673
    invoke-virtual {p0, p1, v0, v0}, Lcom/ibm/icu/impl/ICUService;->getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;Ljava/lang/String;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/util/SortedMap;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "matchID"    # Ljava/lang/String;

    .prologue
    .line 689
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/ibm/icu/impl/ICUService;->getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;Ljava/lang/String;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)Ljava/util/SortedMap;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "com"    # Ljava/util/Comparator;

    .prologue
    .line 681
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ICUService;->getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;Ljava/lang/String;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayNames(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;Ljava/lang/String;)Ljava/util/SortedMap;
    .locals 12
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "com"    # Ljava/util/Comparator;
    .param p3, "matchID"    # Ljava/lang/String;

    .prologue
    .line 703
    const/4 v0, 0x0

    .line 704
    .local v0, "dncache":Ljava/util/SortedMap;
    iget-object v9, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    .line 706
    .local v9, "ref":Lcom/ibm/icu/impl/ICUService$LocaleRef;
    if-eqz v9, :cond_7

    .line 707
    invoke-virtual {v9, p1, p2}, Lcom/ibm/icu/impl/ICUService$LocaleRef;->get(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)Ljava/util/SortedMap;

    move-result-object v0

    move-object v1, v0

    .line 710
    .end local v0    # "dncache":Ljava/util/SortedMap;
    .local v1, "dncache":Ljava/util/SortedMap;
    :goto_0
    if-nez v1, :cond_3

    .line 711
    monitor-enter p0

    .line 712
    :try_start_0
    iget-object v11, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    if-eq v9, v11, :cond_0

    iget-object v11, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    if-nez v11, :cond_2

    .line 713
    :cond_0
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p2}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 715
    .end local v1    # "dncache":Ljava/util/SortedMap;
    .restart local v0    # "dncache":Ljava/util/SortedMap;
    :try_start_1
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUService;->getVisibleIDMap()Ljava/util/Map;

    move-result-object v7

    .line 716
    .local v7, "m":Ljava/util/Map;
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 717
    .local v3, "ei":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 718
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 719
    .local v2, "e":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 720
    .local v5, "id":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/impl/ICUService$Factory;

    .line 721
    .local v4, "f":Lcom/ibm/icu/impl/ICUService$Factory;
    invoke-interface {v4, v5, p1}, Lcom/ibm/icu/impl/ICUService$Factory;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v0, v11, v5}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 730
    .end local v2    # "e":Ljava/util/Map$Entry;
    .end local v3    # "ei":Ljava/util/Iterator;
    .end local v4    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    .end local v5    # "id":Ljava/lang/String;
    .end local v7    # "m":Ljava/util/Map;
    :catchall_0
    move-exception v11

    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v11

    .line 724
    .restart local v3    # "ei":Ljava/util/Iterator;
    .restart local v7    # "m":Ljava/util/Map;
    :cond_1
    :try_start_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    .line 725
    new-instance v11, Lcom/ibm/icu/impl/ICUService$LocaleRef;

    invoke-direct {v11, v0, p1, p2}, Lcom/ibm/icu/impl/ICUService$LocaleRef;-><init>(Ljava/util/Map;Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)V

    iput-object v11, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    .line 730
    .end local v3    # "ei":Ljava/util/Iterator;
    .end local v7    # "m":Ljava/util/Map;
    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 731
    .end local v0    # "dncache":Ljava/util/SortedMap;
    .restart local v1    # "dncache":Ljava/util/SortedMap;
    goto :goto_0

    .line 727
    :cond_2
    :try_start_3
    iget-object v9, p0, Lcom/ibm/icu/impl/ICUService;->dnref:Lcom/ibm/icu/impl/ICUService$LocaleRef;

    .line 728
    invoke-virtual {v9, p1, p2}, Lcom/ibm/icu/impl/ICUService$LocaleRef;->get(Lcom/ibm/icu/util/ULocale;Ljava/util/Comparator;)Ljava/util/SortedMap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .end local v1    # "dncache":Ljava/util/SortedMap;
    .restart local v0    # "dncache":Ljava/util/SortedMap;
    goto :goto_3

    .line 733
    .end local v0    # "dncache":Ljava/util/SortedMap;
    .restart local v1    # "dncache":Ljava/util/SortedMap;
    :cond_3
    invoke-virtual {p0, p3}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v8

    .line 734
    .local v8, "matchKey":Lcom/ibm/icu/impl/ICUService$Key;
    if-nez v8, :cond_4

    .line 746
    .end local v1    # "dncache":Ljava/util/SortedMap;
    :goto_4
    return-object v1

    .line 738
    .restart local v1    # "dncache":Ljava/util/SortedMap;
    :cond_4
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/SortedMap;)V

    .line 739
    .local v10, "result":Ljava/util/SortedMap;
    invoke-interface {v10}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 740
    .local v6, "iter":Ljava/util/Iterator;
    :cond_5
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 741
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 742
    .restart local v2    # "e":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v8, v11}, Lcom/ibm/icu/impl/ICUService$Key;->isFallbackOf(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 743
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .end local v2    # "e":Ljava/util/Map$Entry;
    :cond_6
    move-object v1, v10

    .line 746
    goto :goto_4

    .line 730
    .end local v6    # "iter":Ljava/util/Iterator;
    .end local v8    # "matchKey":Lcom/ibm/icu/impl/ICUService$Key;
    .end local v10    # "result":Ljava/util/SortedMap;
    :catchall_1
    move-exception v11

    move-object v0, v1

    .end local v1    # "dncache":Ljava/util/SortedMap;
    .restart local v0    # "dncache":Ljava/util/SortedMap;
    goto :goto_2

    :cond_7
    move-object v1, v0

    .end local v0    # "dncache":Ljava/util/SortedMap;
    .restart local v1    # "dncache":Ljava/util/SortedMap;
    goto/16 :goto_0
.end method

.method public getKey(Lcom/ibm/icu/impl/ICUService$Key;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;

    .prologue
    .line 361
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/impl/ICUService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "actualReturn"    # [Ljava/lang/String;

    .prologue
    .line 380
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ICUService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;Lcom/ibm/icu/impl/ICUService$Factory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;Lcom/ibm/icu/impl/ICUService$Factory;)Ljava/lang/Object;
    .locals 24
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "actualReturn"    # [Ljava/lang/String;
    .param p3, "factory"    # Lcom/ibm/icu/impl/ICUService$Factory;

    .prologue
    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-nez v21, :cond_0

    .line 388
    invoke-virtual/range {p0 .. p2}, Lcom/ibm/icu/impl/ICUService;->handleDefault(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    .line 523
    :goto_0
    return-object v21

    .line 391
    :cond_0
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_1

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "Service: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " key: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/ICUService$Key;->canonicalID()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 393
    :cond_1
    const/16 v17, 0x0

    .line 394
    .local v17, "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    if-eqz p1, :cond_1a

    .line 399
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/impl/ICURWLock;->acquireRead()V

    .line 401
    const/4 v4, 0x0

    .line 402
    .local v4, "cache":Ljava/util/Map;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/ibm/icu/impl/ICUService;->cacheref:Ljava/lang/ref/SoftReference;

    .line 403
    .local v7, "cref":Ljava/lang/ref/SoftReference;
    if-eqz v7, :cond_3

    .line 404
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_2

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "Service "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " ref exists"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 405
    :cond_2
    invoke-virtual {v7}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "cache":Ljava/util/Map;
    check-cast v4, Ljava/util/Map;

    .line 407
    .restart local v4    # "cache":Ljava/util/Map;
    :cond_3
    if-nez v4, :cond_5

    .line 408
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_4

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "Service "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " cache was empty"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 411
    :cond_4
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    invoke-static/range {v21 .. v21}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    .line 413
    new-instance v7, Ljava/lang/ref/SoftReference;

    .end local v7    # "cref":Ljava/lang/ref/SoftReference;
    invoke-direct {v7, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 416
    .restart local v7    # "cref":Ljava/lang/ref/SoftReference;
    :cond_5
    const/4 v8, 0x0

    .line 417
    .local v8, "currentDescriptor":Ljava/lang/String;
    const/4 v5, 0x0

    .line 418
    .local v5, "cacheDescriptorList":Ljava/util/ArrayList;
    const/16 v16, 0x0

    .line 420
    .local v16, "putInCache":Z
    const/4 v2, 0x0

    .line 422
    .local v2, "NDebug":I
    const/16 v20, 0x0

    .line 423
    .local v20, "startIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v15

    .line 424
    .local v15, "limit":I
    const/4 v6, 0x1

    .line 425
    .local v6, "cacheResult":Z
    if-eqz p3, :cond_13

    .line 426
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v15, :cond_6

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 428
    add-int/lit8 v20, v11, 0x1

    .line 432
    :cond_6
    if-nez v20, :cond_8

    .line 433
    new-instance v21, Ljava/lang/IllegalStateException;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "Factory "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "not registered with service: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    .end local v2    # "NDebug":I
    .end local v4    # "cache":Ljava/util/Map;
    .end local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    .end local v6    # "cacheResult":Z
    .end local v7    # "cref":Ljava/lang/ref/SoftReference;
    .end local v8    # "currentDescriptor":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v15    # "limit":I
    .end local v16    # "putInCache":Z
    .end local v20    # "startIndex":I
    :catchall_0
    move-exception v21

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 518
    throw v21

    .line 426
    .restart local v2    # "NDebug":I
    .restart local v4    # "cache":Ljava/util/Map;
    .restart local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    .restart local v6    # "cacheResult":Z
    .restart local v7    # "cref":Ljava/lang/ref/SoftReference;
    .restart local v8    # "currentDescriptor":Ljava/lang/String;
    .restart local v11    # "i":I
    .restart local v15    # "limit":I
    .restart local v16    # "putInCache":Z
    .restart local v20    # "startIndex":I
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 435
    :cond_8
    const/4 v6, 0x0

    move v3, v2

    .line 440
    .end local v2    # "NDebug":I
    .end local v11    # "i":I
    .local v3, "NDebug":I
    :goto_3
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/ICUService$Key;->currentDescriptor()Ljava/lang/String;

    move-result-object v8

    .line 441
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_1d

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "NDebug":I
    .restart local v2    # "NDebug":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "] looking for: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 442
    :goto_4
    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, v21

    check-cast v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;

    move-object/from16 v17, v0

    .line 443
    if-eqz v17, :cond_c

    .line 444
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_9

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " found with descriptor: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 482
    :cond_9
    :goto_5
    if-eqz v17, :cond_19

    .line 483
    if-eqz v16, :cond_15

    .line 484
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_a

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "caching \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 485
    :cond_a
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    if-eqz v5, :cond_14

    .line 487
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 488
    .local v14, "iter":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_14

    .line 489
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 490
    .local v9, "desc":Ljava/lang/String;
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_b

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " adding descriptor: \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "\' for actual: \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 492
    :cond_b
    move-object/from16 v0, v17

    invoke-interface {v4, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 447
    .end local v9    # "desc":Ljava/lang/String;
    .end local v14    # "iter":Ljava/util/Iterator;
    :cond_c
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_d

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "did not find: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " in cache"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 453
    :cond_d
    move/from16 v16, v6

    .line 456
    move/from16 v12, v20

    .local v12, "index":I
    move v13, v12

    .line 457
    .end local v12    # "index":I
    .local v13, "index":I
    :goto_7
    if-ge v13, v15, :cond_11

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    move-object/from16 v21, v0

    add-int/lit8 v12, v13, 0x1

    .end local v13    # "index":I
    .restart local v12    # "index":I
    move-object/from16 v0, v21

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/impl/ICUService$Factory;

    .line 459
    .local v10, "f":Lcom/ibm/icu/impl/ICUService$Factory;
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_e

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "trying factory["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    add-int/lit8 v23, v12, -0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 460
    :cond_e
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-interface {v10, v0, v1}, Lcom/ibm/icu/impl/ICUService$Factory;->create(Lcom/ibm/icu/impl/ICUService$Key;Lcom/ibm/icu/impl/ICUService;)Ljava/lang/Object;

    move-result-object v19

    .line 461
    .local v19, "service":Ljava/lang/Object;
    if-eqz v19, :cond_f

    .line 462
    new-instance v18, Lcom/ibm/icu/impl/ICUService$CacheEntry;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v8, v1}, Lcom/ibm/icu/impl/ICUService$CacheEntry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    .end local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .local v18, "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    :try_start_2
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_1c

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, " factory supported: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, ", caching"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v17, v18

    .end local v18    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    goto/16 :goto_5

    .line 466
    :cond_f
    :try_start_3
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_10

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "factory did not support: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_10
    move v13, v12

    .line 468
    .end local v12    # "index":I
    .restart local v13    # "index":I
    goto/16 :goto_7

    .line 475
    .end local v10    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    .end local v19    # "service":Ljava/lang/Object;
    :cond_11
    if-nez v5, :cond_12

    .line 476
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    const/16 v21, 0x5

    move/from16 v0, v21

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 478
    .restart local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    :cond_12
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/ICUService$Key;->fallback()Z

    move-result v21

    if-eqz v21, :cond_9

    .end local v13    # "index":I
    :cond_13
    move v3, v2

    .end local v2    # "NDebug":I
    .restart local v3    # "NDebug":I
    goto/16 :goto_3

    .line 499
    .end local v3    # "NDebug":I
    .restart local v2    # "NDebug":I
    :cond_14
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/ibm/icu/impl/ICUService;->cacheref:Ljava/lang/ref/SoftReference;

    .line 502
    :cond_15
    if-eqz p2, :cond_16

    .line 504
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-nez v21, :cond_18

    .line 505
    const/16 v21, 0x0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, p2, v21

    .line 511
    :cond_16
    :goto_8
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_17

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "found in service: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 513
    :cond_17
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->service:Ljava/lang/Object;

    move-object/from16 v21, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    goto/16 :goto_0

    .line 507
    :cond_18
    const/16 v21, 0x0

    :try_start_4
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService$CacheEntry;->actualDescriptor:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, p2, v21
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_8

    .line 517
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/impl/ICURWLock;->releaseRead()V

    .line 521
    .end local v2    # "NDebug":I
    .end local v4    # "cache":Ljava/util/Map;
    .end local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    .end local v6    # "cacheResult":Z
    .end local v7    # "cref":Ljava/lang/ref/SoftReference;
    .end local v8    # "currentDescriptor":Ljava/lang/String;
    .end local v15    # "limit":I
    .end local v16    # "putInCache":Z
    .end local v20    # "startIndex":I
    :cond_1a
    sget-boolean v21, Lcom/ibm/icu/impl/ICUService;->DEBUG:Z

    if-eqz v21, :cond_1b

    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "not found in service: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 523
    :cond_1b
    invoke-virtual/range {p0 .. p2}, Lcom/ibm/icu/impl/ICUService;->handleDefault(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    goto/16 :goto_0

    .line 517
    .end local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v2    # "NDebug":I
    .restart local v4    # "cache":Ljava/util/Map;
    .restart local v5    # "cacheDescriptorList":Ljava/util/ArrayList;
    .restart local v6    # "cacheResult":Z
    .restart local v7    # "cref":Ljava/lang/ref/SoftReference;
    .restart local v8    # "currentDescriptor":Ljava/lang/String;
    .restart local v10    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    .restart local v12    # "index":I
    .restart local v15    # "limit":I
    .restart local v16    # "putInCache":Z
    .restart local v18    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v19    # "service":Ljava/lang/Object;
    .restart local v20    # "startIndex":I
    :catchall_1
    move-exception v21

    move-object/from16 v17, v18

    .end local v18    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    goto/16 :goto_2

    .end local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v18    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    :cond_1c
    move-object/from16 v17, v18

    .end local v18    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    .restart local v17    # "result":Lcom/ibm/icu/impl/ICUService$CacheEntry;
    goto/16 :goto_5

    .end local v2    # "NDebug":I
    .end local v10    # "f":Lcom/ibm/icu/impl/ICUService$Factory;
    .end local v12    # "index":I
    .end local v19    # "service":Ljava/lang/Object;
    .restart local v3    # "NDebug":I
    :cond_1d
    move v2, v3

    .end local v3    # "NDebug":I
    .restart local v2    # "NDebug":I
    goto/16 :goto_4
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getVisibleIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/ICUService;->getVisibleIDs(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleIDs(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .param p1, "matchID"    # Ljava/lang/String;

    .prologue
    .line 567
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUService;->getVisibleIDMap()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 569
    .local v3, "result":Ljava/util/Set;
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v0

    .line 571
    .local v0, "fallbackKey":Lcom/ibm/icu/impl/ICUService$Key;
    if-eqz v0, :cond_2

    .line 572
    new-instance v4, Ljava/util/HashSet;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(I)V

    .line 573
    .local v4, "temp":Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 574
    .local v2, "iter":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 575
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 576
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUService$Key;->isFallbackOf(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 577
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 580
    .end local v1    # "id":Ljava/lang/String;
    :cond_1
    move-object v3, v4

    .line 582
    .end local v2    # "iter":Ljava/util/Iterator;
    .end local v4    # "temp":Ljava/util/Set;
    :cond_2
    return-object v3
.end method

.method protected handleDefault(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "actualIDReturn"    # [Ljava/lang/String;

    .prologue
    .line 544
    const/4 v0, 0x0

    return-object v0
.end method

.method public isDefault()Z
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/ibm/icu/impl/ICUService;->defaultSize:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected markDefault()V
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/ICUService;->defaultSize:I

    .line 899
    return-void
.end method

.method protected notifyListener(Ljava/util/EventListener;)V
    .locals 0
    .param p1, "l"    # Ljava/util/EventListener;

    .prologue
    .line 961
    check-cast p1, Lcom/ibm/icu/impl/ICUService$ServiceListener;

    .end local p1    # "l":Ljava/util/EventListener;
    invoke-interface {p1, p0}, Lcom/ibm/icu/impl/ICUService$ServiceListener;->serviceChanged(Lcom/ibm/icu/impl/ICUService;)V

    .line 962
    return-void
.end method

.method protected reInitializeFactories()V
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 883
    return-void
.end method

.method public final registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 2
    .param p1, "factory"    # Lcom/ibm/icu/impl/ICUService$Factory;

    .prologue
    .line 815
    if-nez p1, :cond_0

    .line 816
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 819
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock;->acquireWrite()V

    .line 820
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 821
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->clearCaches()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 824
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 826
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->notifyChanged()V

    .line 827
    return-object p1

    .line 824
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 825
    throw v0
.end method

.method public registerObject(Ljava/lang/Object;Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 796
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ICUService;->registerObject(Ljava/lang/Object;Ljava/lang/String;Z)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method public registerObject(Ljava/lang/Object;Ljava/lang/String;Z)Lcom/ibm/icu/impl/ICUService$Factory;
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "visible"    # Z

    .prologue
    .line 805
    invoke-virtual {p0, p2}, Lcom/ibm/icu/impl/ICUService;->createKey(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUService$Key;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUService$Key;->canonicalID()Ljava/lang/String;

    move-result-object v0

    .line 806
    .local v0, "canonicalID":Ljava/lang/String;
    new-instance v1, Lcom/ibm/icu/impl/ICUService$SimpleFactory;

    invoke-direct {v1, p1, v0, p3}, Lcom/ibm/icu/impl/ICUService$SimpleFactory;-><init>(Ljava/lang/Object;Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/ICUService;->registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v1

    return-object v1
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 864
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock;->acquireWrite()V

    .line 865
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->reInitializeFactories()V

    .line 866
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->clearCaches()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 869
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 871
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->notifyChanged()V

    .line 872
    return-void

    .line 869
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 870
    throw v0
.end method

.method public stats()Ljava/lang/String;
    .locals 2

    .prologue
    .line 969
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->resetStats()Lcom/ibm/icu/impl/ICURWLock$Stats;

    move-result-object v0

    .line 970
    .local v0, "stats":Lcom/ibm/icu/impl/ICURWLock$Stats;
    if-eqz v0, :cond_0

    .line 971
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICURWLock$Stats;->toString()Ljava/lang/String;

    move-result-object v1

    .line 973
    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, "no stats"

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 987
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unregisterFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Z
    .locals 3
    .param p1, "factory"    # Lcom/ibm/icu/impl/ICUService$Factory;

    .prologue
    .line 836
    if-nez p1, :cond_0

    .line 837
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 840
    :cond_0
    const/4 v0, 0x0

    .line 842
    .local v0, "result":Z
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->acquireWrite()V

    .line 843
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factories:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 844
    const/4 v0, 0x1

    .line 845
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->clearCaches()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 849
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 852
    if-eqz v0, :cond_2

    .line 853
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUService;->notifyChanged()V

    .line 855
    :cond_2
    return v0

    .line 849
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/ibm/icu/impl/ICUService;->factoryLock:Lcom/ibm/icu/impl/ICURWLock;

    invoke-virtual {v2}, Lcom/ibm/icu/impl/ICURWLock;->releaseWrite()V

    .line 850
    throw v1
.end method
