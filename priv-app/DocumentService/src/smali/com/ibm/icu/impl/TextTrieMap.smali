.class public Lcom/ibm/icu/impl/TextTrieMap;
.super Ljava/lang/Object;
.source "TextTrieMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/TextTrieMap$1;,
        Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;,
        Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;,
        Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    }
.end annotation


# instance fields
.field ignoreCase:Z

.field private root:Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "ignoreCase"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;-><init>(Lcom/ibm/icu/impl/TextTrieMap;I)V

    iput-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap;->root:Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    .line 28
    iput-boolean p1, p0, Lcom/ibm/icu/impl/TextTrieMap;->ignoreCase:Z

    .line 29
    return-void
.end method

.method static access$100(Lcom/ibm/icu/impl/TextTrieMap;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/TextTrieMap;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/TextTrieMap;->compare(II)Z

    move-result v0

    return v0
.end method

.method private compare(II)Z
    .locals 3
    .param p1, "ch1"    # I
    .param p2, "ch2"    # I

    .prologue
    const/4 v0, 0x1

    .line 131
    if-ne p1, p2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 134
    :cond_1
    iget-boolean v1, p0, Lcom/ibm/icu/impl/TextTrieMap;->ignoreCase:Z

    if-eqz v1, :cond_2

    .line 135
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v1

    invoke-static {p2}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 138
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(I)I

    move-result v1

    invoke-static {p2}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 142
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized find(Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;Ljava/lang/String;IILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V
    .locals 12
    .param p1, "node"    # Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "index"    # I
    .param p5, "handler"    # Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 101
    .local v11, "itr":Ljava/util/Iterator;
    if-eqz v11, :cond_1

    .line 102
    sub-int v1, p4, p3

    move-object/from16 v0, p5

    invoke-interface {v0, v1, v11}, Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;->handlePrefixMatch(ILjava/util/Iterator;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 121
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 106
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    move/from16 v0, p4

    if-ge v0, v1, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->getChildNodes()Ljava/util/List;

    move-result-object v9

    .line 108
    .local v9, "childNodes":Ljava/util/List;
    if-eqz v9, :cond_0

    .line 111
    move/from16 v0, p4

    invoke-static {p2, v0}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v7

    .line 112
    .local v7, "ch":I
    invoke-static {v7}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v8

    .line 113
    .local v8, "chLen":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v10, v1, :cond_0

    .line 114
    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    .line 115
    .local v2, "child":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    invoke-virtual {v2}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->getCharacter()I

    move-result v1

    invoke-direct {p0, v7, v1}, Lcom/ibm/icu/impl/TextTrieMap;->compare(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    add-int v5, p4, v8

    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/TextTrieMap;->find(Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;Ljava/lang/String;IILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 100
    .end local v2    # "child":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    .end local v7    # "ch":I
    .end local v8    # "chLen":I
    .end local v9    # "childNodes":Ljava/util/List;
    .end local v10    # "i":I
    .end local v11    # "itr":Ljava/util/Iterator;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 113
    .restart local v2    # "child":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    .restart local v7    # "ch":I
    .restart local v8    # "chLen":I
    .restart local v9    # "childNodes":Ljava/util/List;
    .restart local v10    # "i":I
    .restart local v11    # "itr":Ljava/util/Iterator;
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method


# virtual methods
.method public find(Ljava/lang/String;ILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "handler"    # Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/ibm/icu/impl/TextTrieMap;->root:Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/TextTrieMap;->find(Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;Ljava/lang/String;IILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V

    .line 85
    return-void
.end method

.method public find(Ljava/lang/String;Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "handler"    # Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/ibm/icu/impl/TextTrieMap;->find(Ljava/lang/String;ILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V

    .line 81
    return-void
.end method

.method public get(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/impl/TextTrieMap;->get(Ljava/lang/String;I)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 74
    new-instance v0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;-><init>(Lcom/ibm/icu/impl/TextTrieMap$1;)V

    .line 75
    .local v0, "handler":Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;
    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/TextTrieMap;->find(Ljava/lang/String;ILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V

    .line 76
    invoke-virtual {v0}, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->getMatches()Ljava/util/Iterator;

    move-result-object v1

    return-object v1
.end method

.method public declared-synchronized put(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 38
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/ibm/icu/impl/TextTrieMap;->root:Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    .line 39
    .local v2, "node":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 40
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 41
    .local v0, "ch":I
    invoke-virtual {v2, v0}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->addChildNode(I)Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    move-result-object v2

    .line 42
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 43
    add-int/lit8 v1, v1, 0x1

    .line 39
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "ch":I
    :cond_1
    invoke-virtual {v2, p2}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->addObject(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    monitor-exit p0

    return-void

    .line 38
    .end local v1    # "i":I
    .end local v2    # "node":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
