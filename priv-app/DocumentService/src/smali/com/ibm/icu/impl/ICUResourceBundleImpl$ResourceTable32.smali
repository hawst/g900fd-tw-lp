.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceTable32"
.end annotation


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 8
    .param p1, "reader"    # Lcom/ibm/icu/impl/ICUResourceBundleReader;
    .param p2, "baseName"    # Ljava/lang/String;
    .param p3, "localeID"    # Ljava/lang/String;
    .param p4, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    const/4 v2, 0x0

    .line 303
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 305
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getData()[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    .line 306
    const-wide v0, 0xffffffffL

    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getRootResource()I

    move-result v3

    int-to-long v4, v3

    and-long/2addr v0, v4

    iput-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rootResource:J

    .line 307
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getNoFallback()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->noFallback:Z

    .line 308
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->baseName:Ljava/lang/String;

    .line 309
    iput-object p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->localeID:Ljava/lang/String;

    .line 310
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, p3}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 311
    iput-object p4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->loader:Ljava/lang/ClassLoader;

    .line 312
    const-string/jumbo v3, ""

    iget-wide v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rootResource:J

    iget-boolean v7, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->isTopLevel:Z

    move-object v1, p0

    move-object v6, v2

    invoke-virtual/range {v1 .. v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 313
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 9
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 301
    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 302
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p6, "isTopLevel"    # Z

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 328
    invoke-virtual/range {p0 .. p6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 329
    return-void
.end method

.method private countItems()I
    .locals 4

    .prologue
    .line 296
    iget-wide v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resource:J

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->RES_GET_OFFSET(J)I

    move-result v0

    .line 297
    .local v0, "offset":I
    iget-object v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    invoke-static {v2, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v1

    .line 298
    .local v1, "value":I
    return v1
.end method


# virtual methods
.method public getOffset(II)I
    .locals 2
    .param p1, "currentOffset"    # I
    .param p2, "index"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    invoke-static {p2}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v1

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v0

    return v0
.end method

.method protected handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 14
    .param p1, "index"    # I
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "isAlias"    # [Z

    .prologue
    .line 276
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    if-gtz v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    .line 279
    :cond_0
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    if-le p1, v0, :cond_1

    .line 280
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 282
    :cond_1
    iget-wide v6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resource:J

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->RES_GET_OFFSET(J)I

    move-result v11

    .line 285
    .local v11, "offset":I
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v0

    add-int/2addr v0, v11

    invoke-static {p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v5

    add-int v10, v0, v5

    .line 287
    .local v10, "currentOffset":I
    const/4 v0, 0x0

    invoke-virtual {p0, v10, v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getOffset(II)I

    move-result v9

    .line 288
    .local v9, "betterOffset":I
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    invoke-static {v0, v9}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->RES_GET_KEY([BI)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "itemKey":Ljava/lang/String;
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v0

    add-int/2addr v10, v0

    .line 290
    const-wide v6, 0xffffffffL

    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    invoke-static {v0, v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v0

    int-to-long v12, v0

    and-long v2, v6, v12

    .line 291
    .local v2, "resOffset":J
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->isTopLevel:Z

    const/4 v5, 0x1

    if-ne v0, v5, :cond_2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .local v4, "path":Ljava/lang/String;
    :goto_1
    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object v7, p0

    move-object/from16 v8, p4

    .line 293
    invoke-virtual/range {v0 .. v8}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    goto :goto_0

    .line 291
    .end local v4    # "path":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resPath:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method protected handleGetImpl(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[I[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 11
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "index"    # [I
    .param p5, "isAlias"    # [Z

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resource:J

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->RES_GET_OFFSET(J)I

    move-result v10

    .line 254
    .local v10, "offset":I
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v0

    add-int v9, v10, v0

    .line 257
    .local v9, "currentOffset":I
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    invoke-virtual {p0, v1, v9, p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->findKey(IILcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)I

    move-result v1

    aput v1, p4, v0

    .line 258
    const/4 v0, 0x0

    aget v0, p4, v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 259
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not find resource "

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->baseName:Ljava/lang/String;

    iget-object v6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->localeID:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getFullName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v0

    const/4 v1, 0x0

    aget v1, p4, v1

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->getIntOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v9, v0

    .line 265
    const-wide v0, 0xffffffffL

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->rawData:[B

    invoke-static {v5, v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v5

    int-to-long v6, v5

    and-long v2, v0, v6

    .line 266
    .local v2, "resOffset":J
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->isTopLevel:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    move-object v4, p1

    .local v4, "path":Ljava/lang/String;
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p0

    move-object/from16 v8, p5

    .line 268
    invoke-virtual/range {v0 .. v8}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    return-object v0

    .line 266
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V
    .locals 1
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "resourcePath"    # Ljava/lang/String;
    .param p3, "resOffset"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p6, "topLevel"    # Z

    .prologue
    .line 316
    if-eqz p5, :cond_0

    .line 317
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 319
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->key:Ljava/lang/String;

    .line 320
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resource:J

    .line 321
    iput-boolean p6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->isTopLevel:Z

    .line 322
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->countItems()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->size:I

    .line 323
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->resPath:Ljava/lang/String;

    .line 324
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;->createLookupCache()V

    .line 325
    return-void
.end method
