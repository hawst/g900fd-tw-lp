.class public abstract Lcom/ibm/icu/impl/Trie;
.super Ljava/lang/Object;
.source "Trie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/Trie$1;,
        Lcom/ibm/icu/impl/Trie$DefaultGetFoldingOffset;,
        Lcom/ibm/icu/impl/Trie$DataManipulate;
    }
.end annotation


# static fields
.field protected static final BMP_INDEX_LENGTH:I = 0x800

.field protected static final DATA_BLOCK_LENGTH:I = 0x20

.field protected static final HEADER_LENGTH_:I = 0x10

.field protected static final HEADER_OPTIONS_DATA_IS_32_BIT_:I = 0x100

.field protected static final HEADER_OPTIONS_INDEX_SHIFT_:I = 0x4

.field protected static final HEADER_OPTIONS_LATIN1_IS_LINEAR_MASK_:I = 0x200

.field private static final HEADER_OPTIONS_SHIFT_MASK_:I = 0xf

.field protected static final HEADER_SIGNATURE_:I = 0x54726965

.field protected static final INDEX_STAGE_1_SHIFT_:I = 0x5

.field protected static final INDEX_STAGE_2_SHIFT_:I = 0x2

.field protected static final INDEX_STAGE_3_MASK_:I = 0x1f

.field protected static final LEAD_INDEX_OFFSET_:I = 0x140

.field protected static final SURROGATE_BLOCK_BITS:I = 0x5

.field protected static final SURROGATE_BLOCK_COUNT:I = 0x20

.field protected static final SURROGATE_MASK_:I = 0x3ff


# instance fields
.field protected m_dataLength_:I

.field protected m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

.field protected m_dataOffset_:I

.field protected m_index_:[C

.field private m_isLatin1Linear_:Z

.field private m_options_:I


# direct methods
.method protected constructor <init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 148
    .local v0, "input":Ljava/io/DataInputStream;
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 149
    .local v1, "signature":I
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    .line 151
    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/Trie;->checkHeader(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 152
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "ICU data file error: Trie header authentication failed, please check if you have the most updated ICU data file"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 155
    :cond_0
    if-eqz p2, :cond_1

    .line 156
    iput-object p2, p0, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    .line 160
    :goto_0
    iget v2, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/ibm/icu/impl/Trie;->m_isLatin1Linear_:Z

    .line 162
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    .line 163
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/Trie;->m_dataLength_:I

    .line 164
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/Trie;->unserialize(Ljava/io/InputStream;)V

    .line 165
    return-void

    .line 158
    :cond_1
    new-instance v2, Lcom/ibm/icu/impl/Trie$DefaultGetFoldingOffset;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/ibm/icu/impl/Trie$DefaultGetFoldingOffset;-><init>(Lcom/ibm/icu/impl/Trie$1;)V

    iput-object v2, p0, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    goto :goto_0

    .line 160
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected constructor <init>([CILcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 2
    .param p1, "index"    # [C
    .param p2, "options"    # I
    .param p3, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput p2, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    .line 177
    if-eqz p3, :cond_0

    .line 178
    iput-object p3, p0, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    .line 182
    :goto_0
    iget v0, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/ibm/icu/impl/Trie;->m_isLatin1Linear_:Z

    .line 184
    iput-object p1, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    .line 185
    iget-object v0, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    array-length v0, v0

    iput v0, p0, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    .line 186
    return-void

    .line 180
    :cond_0
    new-instance v0, Lcom/ibm/icu/impl/Trie$DefaultGetFoldingOffset;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/Trie$DefaultGetFoldingOffset;-><init>(Lcom/ibm/icu/impl/Trie$1;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    goto :goto_0

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final checkHeader(I)Z
    .locals 3
    .param p1, "signature"    # I

    .prologue
    const/4 v0, 0x0

    .line 446
    const v1, 0x54726965

    if-eq p1, v1, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    and-int/lit8 v1, v1, 0xf

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    shr-int/lit8 v1, v1, 0x4

    and-int/lit8 v1, v1, 0xf

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 457
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    if-ne p1, p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 101
    :cond_1
    instance-of v3, p1, Lcom/ibm/icu/impl/Trie;

    if-nez v3, :cond_2

    move v1, v2

    .line 102
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 104
    check-cast v0, Lcom/ibm/icu/impl/Trie;

    .line 105
    .local v0, "othertrie":Lcom/ibm/icu/impl/Trie;
    iget-boolean v3, p0, Lcom/ibm/icu/impl/Trie;->m_isLatin1Linear_:Z

    iget-boolean v4, v0, Lcom/ibm/icu/impl/Trie;->m_isLatin1Linear_:Z

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    iget v4, v0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/ibm/icu/impl/Trie;->m_dataLength_:I

    iget v4, v0, Lcom/ibm/icu/impl/Trie;->m_dataLength_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    iget-object v4, v0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected final getBMPOffset(C)I
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 307
    const v0, 0xd800

    if-lt p1, v0, :cond_0

    const v0, 0xdbff

    if-gt p1, v0, :cond_0

    const/16 v0, 0x140

    invoke-virtual {p0, v0, p1}, Lcom/ibm/icu/impl/Trie;->getRawOffset(IC)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/ibm/icu/impl/Trie;->getRawOffset(IC)I

    move-result v0

    goto :goto_0
.end method

.method protected final getCodePointOffset(I)I
    .locals 2
    .param p1, "ch"    # I

    .prologue
    const/4 v0, -0x1

    .line 338
    if-gez p1, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v0

    .line 340
    :cond_1
    const v1, 0xd800

    if-ge p1, v1, :cond_2

    .line 342
    const/4 v0, 0x0

    int-to-char v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/impl/Trie;->getRawOffset(IC)I

    move-result v0

    goto :goto_0

    .line 343
    :cond_2
    const/high16 v1, 0x10000

    if-ge p1, v1, :cond_3

    .line 345
    int-to-char v0, p1

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/Trie;->getBMPOffset(C)I

    move-result v0

    goto :goto_0

    .line 346
    :cond_3
    const v1, 0x10ffff

    if-gt p1, v1, :cond_0

    .line 349
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v0

    and-int/lit16 v1, p1, 0x3ff

    int-to-char v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/impl/Trie;->getSurrogateOffset(CC)I

    move-result v0

    goto :goto_0
.end method

.method protected abstract getInitialValue()I
.end method

.method protected final getLeadOffset(C)I
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/ibm/icu/impl/Trie;->getRawOffset(IC)I

    move-result v0

    return v0
.end method

.method protected final getRawOffset(IC)I
    .locals 2
    .param p1, "offset"    # I
    .param p2, "ch"    # C

    .prologue
    .line 294
    iget-object v0, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    shr-int/lit8 v1, p2, 0x5

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    shl-int/lit8 v0, v0, 0x2

    and-int/lit8 v1, p2, 0x1f

    add-int/2addr v0, v1

    return v0
.end method

.method public getSerializedDataSize()I
    .locals 2

    .prologue
    .line 121
    const/16 v0, 0x10

    .line 122
    .local v0, "result":I
    iget v1, p0, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 123
    invoke-virtual {p0}, Lcom/ibm/icu/impl/Trie;->isCharTrie()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    iget v1, p0, Lcom/ibm/icu/impl/Trie;->m_dataLength_:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/impl/Trie;->isIntTrie()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget v1, p0, Lcom/ibm/icu/impl/Trie;->m_dataLength_:I

    shl-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected abstract getSurrogateOffset(CC)I
.end method

.method protected abstract getValue(I)I
.end method

.method protected final isCharTrie()Z
    .locals 1

    .prologue
    .line 388
    iget v0, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    and-int/lit16 v0, v0, 0x100

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isIntTrie()Z
    .locals 1

    .prologue
    .line 379
    iget v0, p0, Lcom/ibm/icu/impl/Trie;->m_options_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLatin1Linear()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/ibm/icu/impl/Trie;->m_isLatin1Linear_:Z

    return v0
.end method

.method protected unserialize(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    iget v2, p0, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    new-array v2, v2, [C

    iput-object v2, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    .line 367
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 368
    .local v1, "input":Ljava/io/DataInputStream;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    if-ge v0, v2, :cond_0

    .line 369
    iget-object v2, p0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readChar()C

    move-result v3

    aput-char v3, v2, v0

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371
    :cond_0
    return-void
.end method
