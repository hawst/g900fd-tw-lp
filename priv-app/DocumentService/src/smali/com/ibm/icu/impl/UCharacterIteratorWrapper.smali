.class public Lcom/ibm/icu/impl/UCharacterIteratorWrapper;
.super Ljava/lang/Object;
.source "UCharacterIteratorWrapper.java"

# interfaces
.implements Ljava/text/CharacterIterator;


# instance fields
.field private iterator:Lcom/ibm/icu/text/UCharacterIterator;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/UCharacterIterator;)V
    .locals 0
    .param p1, "iter"    # Lcom/ibm/icu/text/UCharacterIterator;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    .line 23
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 132
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;

    .line 133
    .local v1, "result":Lcom/ibm/icu/impl/UCharacterIteratorWrapper;
    iget-object v2, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/UCharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/UCharacterIterator;

    iput-object v2, v1, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v1    # "result":Lcom/ibm/icu/impl/UCharacterIteratorWrapper;
    :goto_0
    return-object v1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public current()C
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public first()C
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->setToStart()V

    .line 37
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public getBeginIndex()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v0

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    return v0
.end method

.method public last()C
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->setToLimit()V

    .line 48
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public next()C
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 72
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public previous()C
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method public setIndex(I)C
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 97
    iget-object v0, p0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;->iterator:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    int-to-char v0, v0

    return v0
.end method
