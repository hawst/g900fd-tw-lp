.class Lcom/ibm/icu/impl/ICUBinaryStream;
.super Ljava/io/DataInputStream;
.source "ICUBinaryStream.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 0
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "size"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 35
    invoke-virtual {p0, p2}, Lcom/ibm/icu/impl/ICUBinaryStream;->mark(I)V

    .line 36
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "raw"    # [B

    .prologue
    .line 42
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/impl/ICUBinaryStream;-><init>(Ljava/io/InputStream;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public seek(I)V
    .locals 4
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUBinaryStream;->reset()V

    .line 52
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUBinaryStream;->skipBytes(I)I

    move-result v0

    .line 53
    .local v0, "actual":I
    if-eq v0, p1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Skip("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, ") only skipped "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 58
    :cond_0
    return-void
.end method
