.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceIntVector"
.end annotation


# instance fields
.field private value:[I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 153
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 154
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->key:Ljava/lang/String;

    .line 155
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->resource:J

    .line 156
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->size:I

    .line 157
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->resPath:Ljava/lang/String;

    .line 158
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->getValue()[I

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->value:[I

    .line 159
    return-void
.end method

.method private getValue()[I
    .locals 8

    .prologue
    .line 139
    iget-wide v6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->resource:J

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->RES_GET_OFFSET(J)I

    move-result v3

    .line 140
    .local v3, "offset":I
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->rawData:[B

    invoke-static {v5, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v2

    .line 141
    .local v2, "length":I
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->getIntOffset(I)I

    move-result v5

    add-int v1, v3, v5

    .line 142
    .local v1, "intOffset":I
    new-array v4, v2, [I

    .line 147
    .local v4, "val":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 148
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->rawData:[B

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->getIntOffset(I)I

    move-result v6

    add-int/2addr v6, v1

    invoke-static {v5, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v5

    aput v5, v4, v0

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_0
    return-object v4
.end method


# virtual methods
.method public getIntVector()[I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;->value:[I

    return-object v0
.end method
