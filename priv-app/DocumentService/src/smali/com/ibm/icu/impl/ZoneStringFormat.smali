.class public Lcom/ibm/icu/impl/ZoneStringFormat;
.super Ljava/lang/Object;
.source "ZoneStringFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ZoneStringFormat$1;,
        Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;,
        Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;,
        Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    }
.end annotation


# static fields
.field private static final DAYLIGHT_LONG:I = 0x20

.field private static final DAYLIGHT_SHORT:I = 0x40

.field private static final DST_CHECK_RANGE:J = 0x3b3922000L

.field private static final GENERIC_LONG:I = 0x2

.field private static final GENERIC_SHORT:I = 0x4

.field private static final INDEXMAP:[I

.field private static final LOCATION:I = 0x1

.field private static final NAMETYPEMAP:[I

.field private static final RESKEY_COMMONLY_USED:Ljava/lang/String; = "cu"

.field private static final RESKEY_EXEMPLAR_CITY:Ljava/lang/String; = "ec"

.field private static final RESKEY_LONG_DAYLIGHT:Ljava/lang/String; = "ld"

.field private static final RESKEY_LONG_GENERIC:Ljava/lang/String; = "lg"

.field private static final RESKEY_LONG_STANDARD:Ljava/lang/String; = "ls"

.field private static final RESKEY_SHORT_DAYLIGHT:Ljava/lang/String; = "sd"

.field private static final RESKEY_SHORT_GENERIC:Ljava/lang/String; = "sg"

.field private static final RESKEY_SHORT_STANDARD:Ljava/lang/String; = "ss"

.field private static final STANDARD_LONG:I = 0x8

.field private static final STANDARD_SHORT:I = 0x10

.field private static TZFORMAT_CACHE:Lcom/ibm/icu/impl/ICUCache; = null

.field private static final ZSIDX_LOCATION:I = 0x0

.field private static final ZSIDX_LONG_DAYLIGHT:I = 0x3

.field private static final ZSIDX_LONG_GENERIC:I = 0x5

.field private static final ZSIDX_LONG_STANDARD:I = 0x1

.field private static final ZSIDX_MAX:I = 0x7

.field private static final ZSIDX_SHORT_DAYLIGHT:I = 0x4

.field private static final ZSIDX_SHORT_GENERIC:I = 0x6

.field private static final ZSIDX_SHORT_STANDARD:I = 0x2


# instance fields
.field private locale:Lcom/ibm/icu/util/ULocale;

.field private mzidToStrings:Ljava/util/Map;

.field private transient region:Ljava/lang/String;

.field private tzidToStrings:Ljava/util/Map;

.field private zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 412
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/ZoneStringFormat;->TZFORMAT_CACHE:Lcom/ibm/icu/impl/ICUCache;

    .line 831
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/impl/ZoneStringFormat;->INDEXMAP:[I

    .line 857
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/impl/ZoneStringFormat;->NAMETYPEMAP:[I

    return-void

    .line 831
    nop

    :array_0
    .array-data 4
        -0x1
        0x1
        0x2
        0x3
        0x4
        0x0
        0x5
        0x6
    .end array-data

    .line 857
    :array_1
    .array-data 4
        0x1
        0x8
        0x10
        0x20
        0x40
        0x2
        0x4
    .end array-data
.end method

.method protected constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 45
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 214
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 215
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/ZoneStringFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 216
    new-instance v41, Ljava/util/HashMap;

    invoke-direct/range {v41 .. v41}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    .line 217
    new-instance v41, Ljava/util/HashMap;

    invoke-direct/range {v41 .. v41}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    .line 218
    new-instance v41, Lcom/ibm/icu/impl/TextTrieMap;

    const/16 v42, 0x1

    invoke-direct/range {v41 .. v42}, Lcom/ibm/icu/impl/TextTrieMap;-><init>(Z)V

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    .line 220
    const/16 v37, 0x0

    .line 222
    .local v37, "zoneStringsBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    :try_start_0
    const-string/jumbo v41, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    check-cast v6, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 223
    .local v6, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v41, "zoneStrings"

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v37

    .line 231
    .end local v6    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_0
    invoke-static {}, Lcom/ibm/icu/util/TimeZone;->getAvailableIDs()[Ljava/lang/String;

    move-result-object v35

    .line 232
    .local v35, "zoneIDs":[Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getFallbackFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v11

    .line 233
    .local v11, "fallbackFmt":Lcom/ibm/icu/text/MessageFormat;
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getRegionFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v30

    .line 235
    .local v30, "regionFmt":Lcom/ibm/icu/text/MessageFormat;
    const/16 v41, 0x7

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v39, v0

    .line 236
    .local v39, "zstrarray":[Ljava/lang/String;
    const/16 v41, 0x7

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    .line 237
    .local v26, "mzstrarray":[Ljava/lang/String;
    const/16 v41, 0xa

    const/16 v42, 0x4

    filled-new-array/range {v41 .. v42}, [I

    move-result-object v41

    const-class v42, Ljava/lang/String;

    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [[Ljava/lang/String;

    .line 239
    .local v20, "mzPartialLoc":[[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    if-ge v13, v0, :cond_14

    .line 241
    aget-object v41, v35, v13

    invoke-static/range {v41 .. v41}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalSystemID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 242
    .local v34, "tzid":Ljava/lang/String;
    if-eqz v34, :cond_0

    aget-object v41, v35, v13

    move-object/from16 v0, v41

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-nez v41, :cond_1

    .line 239
    :cond_0
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 246
    :cond_1
    const/16 v41, 0x2f

    const/16 v42, 0x3a

    move-object/from16 v0, v34

    move/from16 v1, v41

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v36

    .line 247
    .local v36, "zoneKey":Ljava/lang/String;
    const/16 v41, 0x1

    const-string/jumbo v42, "ls"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 248
    const/16 v41, 0x2

    const-string/jumbo v42, "ss"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 249
    const/16 v41, 0x3

    const-string/jumbo v42, "ld"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 250
    const/16 v41, 0x4

    const-string/jumbo v42, "sd"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 251
    const/16 v41, 0x5

    const-string/jumbo v42, "lg"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 252
    const/16 v41, 0x6

    const-string/jumbo v42, "sg"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 255
    invoke-static/range {v34 .. v34}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 256
    .local v10, "countryCode":Ljava/lang/String;
    const/4 v9, 0x0

    .line 257
    .local v9, "country":Ljava/lang/String;
    const/4 v7, 0x0

    .line 258
    .local v7, "city":Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 259
    const-string/jumbo v41, "ec"

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 260
    if-nez v7, :cond_2

    .line 261
    const/16 v41, 0x2f

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v41

    add-int/lit8 v41, v41, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v41

    const/16 v42, 0x5f

    const/16 v43, 0x20

    invoke-virtual/range {v41 .. v43}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    .line 263
    :cond_2
    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getLocalizedCountry(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v9

    .line 264
    invoke-static/range {v34 .. v34}, Lcom/ibm/icu/impl/ZoneMeta;->getSingleCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    if-eqz v41, :cond_5

    .line 266
    const/16 v41, 0x0

    const/16 v42, 0x1

    move/from16 v0, v42

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    aput-object v9, v42, v43

    move-object/from16 v0, v30

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    .line 274
    :goto_2
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->isCommonlyUsed(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)Z

    move-result v8

    .line 277
    .local v8, "commonlyUsed":Z
    const/16 v21, 0x0

    .line 278
    .local v21, "mzPartialLocIdx":I
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getOlsonToMetaMap()Ljava/util/Map;

    move-result-object v27

    .line 279
    .local v27, "olsonToMeta":Ljava/util/Map;
    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/List;

    .line 280
    .local v18, "metazoneMappings":Ljava/util/List;
    if-eqz v18, :cond_e

    .line 281
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 282
    .local v14, "it":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-eqz v41, :cond_e

    .line 283
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;

    .line 284
    .local v25, "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    move-object/from16 v41, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-interface/range {v41 .. v42}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 285
    .local v22, "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-nez v22, :cond_7

    .line 287
    new-instance v41, Ljava/lang/StringBuffer;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v42, "meta:"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v41

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    .line 288
    .local v24, "mzkey":Ljava/lang/String;
    move-object/from16 v0, v37

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->isCommonlyUsed(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)Z

    move-result v19

    .line 289
    .local v19, "mzCommonlyUsed":Z
    const/16 v41, 0x1

    const-string/jumbo v42, "ls"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 290
    const/16 v41, 0x2

    const-string/jumbo v42, "ss"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 291
    const/16 v41, 0x3

    const-string/jumbo v42, "ld"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 292
    const/16 v41, 0x4

    const-string/jumbo v42, "sd"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 293
    const/16 v41, 0x5

    const-string/jumbo v42, "lg"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 294
    const/16 v41, 0x6

    const-string/jumbo v42, "sg"

    move-object/from16 v0, v37

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v26, v41

    .line 295
    const/16 v41, 0x0

    const/16 v42, 0x0

    aput-object v42, v26, v41

    .line 296
    new-instance v22, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .end local v22    # "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    const/16 v41, 0x0

    check-cast v41, [[Ljava/lang/String;

    const/16 v42, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move/from16 v2, v19

    move-object/from16 v3, v41

    move-object/from16 v4, v42

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;-><init>([Ljava/lang/String;Z[[Ljava/lang/String;Lcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 297
    .restart local v22    # "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    move-object/from16 v41, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v42, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v41, v0

    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getRegion()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v41 .. v42}, Lcom/ibm/icu/impl/ZoneMeta;->getZoneIdByMetazone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 301
    .local v29, "preferredIdForLocale":Ljava/lang/String;
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_4
    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    if-ge v15, v0, :cond_7

    .line 302
    aget-object v41, v26, v15

    if-eqz v41, :cond_4

    .line 303
    invoke-static {v15}, Lcom/ibm/icu/impl/ZoneStringFormat;->getNameType(I)I

    move-result v33

    .line 304
    .local v33, "type":I
    new-instance v38, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    aget-object v41, v26, v15

    const/16 v42, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v29

    move-object/from16 v2, v41

    move/from16 v3, v33

    move-object/from16 v4, v42

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 305
    .local v38, "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    move-object/from16 v41, v0

    aget-object v42, v26, v15

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/TextTrieMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 301
    .end local v33    # "type":I
    .end local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 268
    .end local v8    # "commonlyUsed":Z
    .end local v14    # "it":Ljava/util/Iterator;
    .end local v15    # "j":I
    .end local v18    # "metazoneMappings":Ljava/util/List;
    .end local v19    # "mzCommonlyUsed":Z
    .end local v21    # "mzPartialLocIdx":I
    .end local v22    # "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .end local v24    # "mzkey":Ljava/lang/String;
    .end local v25    # "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    .end local v27    # "olsonToMeta":Ljava/util/Map;
    .end local v29    # "preferredIdForLocale":Ljava/lang/String;
    :cond_5
    const/16 v41, 0x0

    const/16 v42, 0x2

    move/from16 v0, v42

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    aput-object v7, v42, v43

    const/16 v43, 0x1

    aput-object v9, v42, v43

    move-object/from16 v0, v42

    invoke-virtual {v11, v0}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    aput-object v42, v39, v41

    goto/16 :goto_2

    .line 271
    :cond_6
    const/16 v41, 0x0

    const/16 v42, 0x0

    aput-object v42, v39, v41

    goto/16 :goto_2

    .line 310
    .restart local v8    # "commonlyUsed":Z
    .restart local v14    # "it":Ljava/util/Iterator;
    .restart local v18    # "metazoneMappings":Ljava/util/List;
    .restart local v21    # "mzPartialLocIdx":I
    .restart local v22    # "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .restart local v25    # "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    .restart local v27    # "olsonToMeta":Ljava/util/Map;
    :cond_7
    const/16 v41, 0x5

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v16

    .line 311
    .local v16, "lg":Ljava/lang/String;
    const/16 v41, 0x6

    move-object/from16 v0, v22

    move/from16 v1, v41

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v31

    .line 312
    .local v31, "sg":Ljava/lang/String;
    if-nez v16, :cond_8

    if-eqz v31, :cond_3

    .line 313
    :cond_8
    const/4 v5, 0x1

    .line 314
    .local v5, "addMzPartialLocationNames":Z
    const/4 v15, 0x0

    .restart local v15    # "j":I
    :goto_5
    move/from16 v0, v21

    if-ge v15, v0, :cond_9

    .line 315
    aget-object v41, v20, v15

    const/16 v42, 0x0

    aget-object v41, v41, v42

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_c

    .line 317
    const/4 v5, 0x0

    .line 321
    :cond_9
    if-eqz v5, :cond_3

    .line 322
    const/16 v17, 0x0

    .line 324
    .local v17, "locationPart":Ljava/lang/String;
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    invoke-static {v0, v10}, Lcom/ibm/icu/impl/ZoneMeta;->getZoneIdByMetazone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 325
    .local v28, "preferredID":Ljava/lang/String;
    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_d

    .line 327
    move-object/from16 v17, v9

    .line 332
    :goto_6
    aget-object v41, v20, v21

    const/16 v42, 0x0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    move-object/from16 v43, v0

    aput-object v43, v41, v42

    .line 333
    aget-object v41, v20, v21

    const/16 v42, 0x1

    const/16 v43, 0x0

    aput-object v43, v41, v42

    .line 334
    aget-object v41, v20, v21

    const/16 v42, 0x2

    const/16 v43, 0x0

    aput-object v43, v41, v42

    .line 335
    aget-object v41, v20, v21

    const/16 v42, 0x3

    const/16 v43, 0x0

    aput-object v43, v41, v42

    .line 336
    if-eqz v17, :cond_b

    .line 337
    if-eqz v16, :cond_a

    .line 338
    aget-object v41, v20, v21

    const/16 v42, 0x1

    const/16 v43, 0x2

    move/from16 v0, v43

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    aput-object v17, v43, v44

    const/16 v44, 0x1

    aput-object v16, v43, v44

    move-object/from16 v0, v43

    invoke-virtual {v11, v0}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    aput-object v43, v41, v42

    .line 340
    :cond_a
    if-eqz v31, :cond_b

    .line 341
    aget-object v41, v20, v21

    const/16 v42, 0x2

    const/16 v43, 0x2

    move/from16 v0, v43

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v43, v0

    const/16 v44, 0x0

    aput-object v17, v43, v44

    const/16 v44, 0x1

    aput-object v31, v43, v44

    move-object/from16 v0, v43

    invoke-virtual {v11, v0}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    aput-object v43, v41, v42

    .line 342
    invoke-static/range {v22 .. v22}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z

    move-result v32

    .line 343
    .local v32, "shortMzCommonlyUsed":Z
    if-eqz v32, :cond_b

    .line 344
    aget-object v41, v20, v21

    const/16 v42, 0x3

    const-string/jumbo v43, "1"

    aput-object v43, v41, v42

    .line 348
    .end local v32    # "shortMzCommonlyUsed":Z
    :cond_b
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    .line 314
    .end local v17    # "locationPart":Ljava/lang/String;
    .end local v28    # "preferredID":Ljava/lang/String;
    :cond_c
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_5

    .line 330
    .restart local v17    # "locationPart":Ljava/lang/String;
    .restart local v28    # "preferredID":Ljava/lang/String;
    :cond_d
    move-object/from16 v17, v7

    goto :goto_6

    .line 353
    .end local v5    # "addMzPartialLocationNames":Z
    .end local v14    # "it":Ljava/util/Iterator;
    .end local v15    # "j":I
    .end local v16    # "lg":Ljava/lang/String;
    .end local v17    # "locationPart":Ljava/lang/String;
    .end local v22    # "mzStrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .end local v25    # "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    .end local v28    # "preferredID":Ljava/lang/String;
    .end local v31    # "sg":Ljava/lang/String;
    :cond_e
    const/4 v12, 0x0

    check-cast v12, [[Ljava/lang/String;

    .line 354
    .local v12, "genericPartialLocationNames":[[Ljava/lang/String;
    if-eqz v21, :cond_f

    .line 356
    move/from16 v0, v21

    new-array v12, v0, [[Ljava/lang/String;

    .line 357
    const/16 v23, 0x0

    .local v23, "mzi":I
    :goto_7
    move/from16 v0, v23

    move/from16 v1, v21

    if-ge v0, v1, :cond_f

    .line 358
    aget-object v41, v20, v23

    invoke-virtual/range {v41 .. v41}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, [Ljava/lang/String;

    check-cast v41, [Ljava/lang/String;

    aput-object v41, v12, v23

    .line 357
    add-int/lit8 v23, v23, 0x1

    goto :goto_7

    .line 362
    .end local v23    # "mzi":I
    :cond_f
    new-instance v40, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    const/16 v41, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v8, v12, v2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;-><init>([Ljava/lang/String;Z[[Ljava/lang/String;Lcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 363
    .local v40, "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v34

    move-object/from16 v2, v40

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    if-eqz v39, :cond_11

    .line 367
    const/4 v15, 0x0

    .restart local v15    # "j":I
    :goto_8
    move-object/from16 v0, v39

    array-length v0, v0

    move/from16 v41, v0

    move/from16 v0, v41

    if-ge v15, v0, :cond_11

    .line 368
    aget-object v41, v39, v15

    if-eqz v41, :cond_10

    .line 369
    invoke-static {v15}, Lcom/ibm/icu/impl/ZoneStringFormat;->getNameType(I)I

    move-result v33

    .line 370
    .restart local v33    # "type":I
    new-instance v38, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    aget-object v41, v39, v15

    const/16 v42, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v34

    move-object/from16 v2, v41

    move/from16 v3, v33

    move-object/from16 v4, v42

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 371
    .restart local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    move-object/from16 v41, v0

    aget-object v42, v39, v15

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/TextTrieMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 367
    .end local v33    # "type":I
    .end local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_10
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 375
    .end local v15    # "j":I
    :cond_11
    if-eqz v12, :cond_0

    .line 376
    const/4 v15, 0x0

    .restart local v15    # "j":I
    :goto_9
    array-length v0, v12

    move/from16 v41, v0

    move/from16 v0, v41

    if-ge v15, v0, :cond_0

    .line 378
    aget-object v41, v12, v15

    const/16 v42, 0x1

    aget-object v41, v41, v42

    if-eqz v41, :cond_12

    .line 379
    new-instance v38, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    aget-object v41, v12, v15

    const/16 v42, 0x1

    aget-object v41, v41, v42

    const/16 v42, 0x2

    const/16 v43, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v34

    move-object/from16 v2, v41

    move/from16 v3, v42

    move-object/from16 v4, v43

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 380
    .restart local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    move-object/from16 v41, v0

    aget-object v42, v12, v15

    const/16 v43, 0x1

    aget-object v42, v42, v43

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/TextTrieMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 382
    .end local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_12
    aget-object v41, v12, v15

    const/16 v42, 0x2

    aget-object v41, v41, v42

    if-eqz v41, :cond_13

    .line 383
    new-instance v38, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    aget-object v41, v12, v15

    const/16 v42, 0x1

    aget-object v41, v41, v42

    const/16 v42, 0x4

    const/16 v43, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v34

    move-object/from16 v2, v41

    move/from16 v3, v42

    move-object/from16 v4, v43

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 384
    .restart local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    move-object/from16 v41, v0

    aget-object v42, v12, v15

    const/16 v43, 0x2

    aget-object v42, v42, v43

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/TextTrieMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 376
    .end local v38    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_13
    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    .line 389
    .end local v7    # "city":Ljava/lang/String;
    .end local v8    # "commonlyUsed":Z
    .end local v9    # "country":Ljava/lang/String;
    .end local v10    # "countryCode":Ljava/lang/String;
    .end local v12    # "genericPartialLocationNames":[[Ljava/lang/String;
    .end local v15    # "j":I
    .end local v18    # "metazoneMappings":Ljava/util/List;
    .end local v21    # "mzPartialLocIdx":I
    .end local v27    # "olsonToMeta":Ljava/util/Map;
    .end local v34    # "tzid":Ljava/lang/String;
    .end local v36    # "zoneKey":Ljava/lang/String;
    .end local v40    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_14
    return-void

    .line 224
    .end local v11    # "fallbackFmt":Lcom/ibm/icu/text/MessageFormat;
    .end local v13    # "i":I
    .end local v20    # "mzPartialLoc":[[Ljava/lang/String;
    .end local v26    # "mzstrarray":[Ljava/lang/String;
    .end local v30    # "regionFmt":Lcom/ibm/icu/text/MessageFormat;
    .end local v35    # "zoneIDs":[Ljava/lang/String;
    .end local v39    # "zstrarray":[Ljava/lang/String;
    :catch_0
    move-exception v41

    goto/16 :goto_0
.end method

.method public constructor <init>([[Ljava/lang/String;)V
    .locals 12
    .param p1, "zoneStrings"    # [[Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v9, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iput-object v8, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    .line 39
    new-instance v8, Lcom/ibm/icu/impl/TextTrieMap;

    invoke-direct {v8, v11}, Lcom/ibm/icu/impl/TextTrieMap;-><init>(Z)V

    iput-object v8, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v8, p1

    if-ge v0, v8, :cond_2

    .line 41
    aget-object v8, p1, v0

    const/4 v10, 0x0

    aget-object v5, v8, v10

    .line 42
    .local v5, "tzid":Ljava/lang/String;
    const/4 v8, 0x7

    new-array v2, v8, [Ljava/lang/String;

    .line 43
    .local v2, "names":[Ljava/lang/String;
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_1
    aget-object v8, p1, v0

    array-length v8, v8

    if-ge v1, v8, :cond_1

    .line 44
    aget-object v8, p1, v0

    aget-object v8, v8, v1

    if-eqz v8, :cond_0

    .line 45
    invoke-static {v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getNameTypeIndex(I)I

    move-result v4

    .line 46
    .local v4, "typeIdx":I
    const/4 v8, -0x1

    if-eq v4, v8, :cond_0

    .line 47
    aget-object v8, p1, v0

    aget-object v8, v8, v1

    aput-object v8, v2, v4

    .line 50
    invoke-static {v4}, Lcom/ibm/icu/impl/ZoneStringFormat;->getNameType(I)I

    move-result v3

    .line 51
    .local v3, "type":I
    new-instance v6, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    aget-object v8, p1, v0

    aget-object v8, v8, v1

    invoke-direct {v6, v5, v8, v3, v9}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 52
    .local v6, "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    iget-object v8, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    aget-object v10, p1, v0

    aget-object v10, v10, v1

    invoke-virtual {v8, v10, v6}, Lcom/ibm/icu/impl/TextTrieMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    .end local v3    # "type":I
    .end local v4    # "typeIdx":I
    .end local v6    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    :cond_1
    new-instance v7, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    move-object v8, v9

    check-cast v8, [[Ljava/lang/String;

    invoke-direct {v7, v2, v11, v8, v9}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;-><init>([Ljava/lang/String;Z[[Ljava/lang/String;Lcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 58
    .local v7, "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    iget-object v8, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v8, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    .end local v1    # "j":I
    .end local v2    # "names":[Ljava/lang/String;
    .end local v5    # "tzid":Ljava/lang/String;
    .end local v7    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_2
    return-void
.end method

.method private find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "types"    # I

    .prologue
    .line 904
    const/4 v4, 0x0

    .line 905
    .local v4, "result":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    new-instance v1, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;-><init>(Lcom/ibm/icu/impl/ZoneStringFormat$1;)V

    .line 906
    .local v1, "handler":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;
    iget-object v6, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->zoneStringsTrie:Lcom/ibm/icu/impl/TextTrieMap;

    invoke-virtual {v6, p1, p2, v1}, Lcom/ibm/icu/impl/TextTrieMap;->find(Ljava/lang/String;ILcom/ibm/icu/impl/TextTrieMap$ResultHandler;)V

    .line 907
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->getMatchedZoneStrings()Ljava/util/List;

    move-result-object v3

    .line 908
    .local v3, "list":Ljava/util/List;
    const/4 v0, 0x0

    .line 909
    .local v0, "fallback":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 910
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 911
    .local v2, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 912
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    .line 913
    .local v5, "tmp":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    invoke-static {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->access$600(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;)I

    move-result v6

    and-int/2addr v6, p3

    if-eqz v6, :cond_3

    .line 914
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 915
    :cond_1
    move-object v4, v5

    .line 916
    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 921
    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isGeneric()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isGeneric()Z

    move-result v6

    if-nez v6, :cond_0

    .line 922
    move-object v4, v5

    .line 923
    goto :goto_0

    .line 925
    :cond_3
    if-nez v4, :cond_0

    .line 926
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v6, v7, :cond_5

    .line 927
    :cond_4
    move-object v0, v5

    .line 928
    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 929
    invoke-virtual {v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isGeneric()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isGeneric()Z

    move-result v6

    if-nez v6, :cond_0

    .line 930
    move-object v0, v5

    goto :goto_0

    .line 936
    .end local v2    # "it":Ljava/util/Iterator;
    .end local v5    # "tmp":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_6
    if-nez v4, :cond_7

    if-eqz v0, :cond_7

    .line 937
    move-object v4, v0

    .line 939
    :cond_7
    return-object v4
.end method

.method private static getFallbackFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;
    .locals 2
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 809
    const-string/jumbo v1, "fallbackFormat"

    invoke-static {p0, v1}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 810
    .local v0, "fallbackPattern":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 811
    const-string/jumbo v0, "{1} ({0})"

    .line 813
    :cond_0
    new-instance v1, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v1, v0, p0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    return-object v1
.end method

.method private getGenericPartialLocationString(Ljava/lang/String;ZJZ)Ljava/lang/String;
    .locals 5
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "isShort"    # Z
    .param p3, "date"    # J
    .param p5, "commonlyUsedOnly"    # Z

    .prologue
    .line 647
    const/4 v1, 0x0

    .line 648
    .local v1, "result":Ljava/lang/String;
    invoke-static {p1, p3, p4}, Lcom/ibm/icu/impl/ZoneMeta;->getMetazoneID(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 649
    .local v0, "mzid":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 650
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 651
    .local v2, "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-eqz v2, :cond_0

    .line 652
    invoke-static {v2, v0, p2, p5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$400(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    .line 655
    .end local v2    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_0
    return-object v1
.end method

.method private getGenericString(Lcom/ibm/icu/util/Calendar;ZZ)Ljava/lang/String;
    .locals 33
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "isShort"    # Z
    .param p3, "commonlyUsedOnly"    # Z

    .prologue
    .line 523
    const/16 v28, 0x0

    .line 524
    .local v28, "result":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v30

    .line 525
    .local v30, "tz":Lcom/ibm/icu/util/TimeZone;
    invoke-virtual/range {v30 .. v30}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    .line 526
    .local v6, "tzid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 527
    .local v32, "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-nez v32, :cond_0

    .line 529
    invoke-static {v6}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalSystemID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 530
    .local v19, "canonicalID":Ljava/lang/String;
    if-eqz v19, :cond_0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 533
    move-object/from16 v6, v19

    .line 534
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    .end local v32    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    check-cast v32, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 537
    .end local v19    # "canonicalID":Ljava/lang/String;
    .restart local v32    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_0
    if-eqz v32, :cond_2

    .line 538
    if-eqz p2, :cond_a

    .line 539
    if-eqz p3, :cond_1

    invoke-static/range {v32 .. v32}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 540
    :cond_1
    const/4 v5, 0x6

    move-object/from16 v0, v32

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v28

    .line 546
    :cond_2
    :goto_0
    if-nez v28, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    if-eqz v5, :cond_8

    .line 548
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 549
    .local v8, "time":J
    invoke-static {v6, v8, v9}, Lcom/ibm/icu/impl/ZoneMeta;->getMetazoneID(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v21

    .line 550
    .local v21, "mzid":Ljava/lang/String;
    if-eqz v21, :cond_8

    .line 551
    const/16 v31, 0x0

    .line 552
    .local v31, "useStandard":Z
    const/16 v5, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v5

    if-nez v5, :cond_3

    .line 553
    const/16 v31, 0x1

    .line 555
    move-object/from16 v0, v30

    instance-of v5, v0, Lcom/ibm/icu/util/BasicTimeZone;

    if-eqz v5, :cond_c

    move-object/from16 v18, v30

    .line 556
    check-cast v18, Lcom/ibm/icu/util/BasicTimeZone;

    .line 557
    .local v18, "btz":Lcom/ibm/icu/util/BasicTimeZone;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9, v5}, Lcom/ibm/icu/util/BasicTimeZone;->getPreviousTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v17

    .line 558
    .local v17, "before":Lcom/ibm/icu/util/TimeZoneTransition;
    if-eqz v17, :cond_b

    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v10

    sub-long v10, v8, v10

    const-wide v12, 0x3b3922000L

    cmp-long v5, v10, v12

    if-gez v5, :cond_b

    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/TimeZoneTransition;->getFrom()Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v5

    if-eqz v5, :cond_b

    .line 561
    const/16 v31, 0x0

    .line 585
    .end local v17    # "before":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v18    # "btz":Lcom/ibm/icu/util/BasicTimeZone;
    :cond_3
    :goto_1
    if-eqz v31, :cond_4

    .line 586
    if-eqz p2, :cond_e

    const/4 v7, 0x2

    :goto_2
    move-object/from16 v5, p0

    move/from16 v10, p3

    invoke-direct/range {v5 .. v10}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v28

    .line 594
    if-eqz v28, :cond_4

    .line 595
    if-eqz p2, :cond_f

    const/4 v7, 0x6

    :goto_3
    move-object/from16 v5, p0

    move/from16 v10, p3

    invoke-direct/range {v5 .. v10}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v20

    .line 597
    .local v20, "genericNonLocation":Ljava/lang/String;
    if-eqz v20, :cond_4

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 598
    const/16 v28, 0x0

    .line 602
    .end local v20    # "genericNonLocation":Ljava/lang/String;
    :cond_4
    if-nez v28, :cond_8

    .line 603
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    move-object/from16 v0, v21

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 604
    .local v22, "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-eqz v22, :cond_6

    .line 605
    if-eqz p2, :cond_10

    .line 606
    if-eqz p3, :cond_5

    invoke-static/range {v22 .. v22}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 607
    :cond_5
    const/4 v5, 0x6

    move-object/from16 v0, v22

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v28

    .line 613
    :cond_6
    :goto_4
    if-eqz v28, :cond_8

    .line 615
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getRegion()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/ZoneMeta;->getZoneIdByMetazone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 616
    .local v24, "preferredId":Ljava/lang/String;
    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 618
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v27

    .line 619
    .local v27, "raw":I
    const/16 v5, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v29

    .line 620
    .local v29, "sav":I
    invoke-static/range {v24 .. v24}, Lcom/ibm/icu/util/TimeZone;->getTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;

    move-result-object v26

    .line 621
    .local v26, "preferredZone":Lcom/ibm/icu/util/TimeZone;
    const/4 v5, 0x2

    new-array v0, v5, [I

    move-object/from16 v25, v0

    .line 626
    .local v25, "preferredOffsets":[I
    move/from16 v0, v27

    int-to-long v10, v0

    add-long/2addr v10, v8

    move/from16 v0, v29

    int-to-long v12, v0

    add-long/2addr v10, v12

    const/4 v5, 0x1

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v10, v11, v5, v1}, Lcom/ibm/icu/util/TimeZone;->getOffset(JZ[I)V

    .line 627
    const/4 v5, 0x0

    aget v5, v25, v5

    move/from16 v0, v27

    if-ne v0, v5, :cond_7

    const/4 v5, 0x1

    aget v5, v25, v5

    move/from16 v0, v29

    if-eq v0, v5, :cond_8

    .line 629
    :cond_7
    move-object/from16 v0, v32

    move-object/from16 v1, v21

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$400(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v28

    .line 636
    .end local v8    # "time":J
    .end local v21    # "mzid":Ljava/lang/String;
    .end local v22    # "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    .end local v24    # "preferredId":Ljava/lang/String;
    .end local v25    # "preferredOffsets":[I
    .end local v26    # "preferredZone":Lcom/ibm/icu/util/TimeZone;
    .end local v27    # "raw":I
    .end local v29    # "sav":I
    .end local v31    # "useStandard":Z
    :cond_8
    if-nez v28, :cond_9

    .line 638
    const/4 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    const/16 v16, 0x0

    move-object/from16 v11, p0

    move-object v12, v6

    invoke-direct/range {v11 .. v16}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v28

    .line 640
    :cond_9
    return-object v28

    .line 543
    :cond_a
    const/4 v5, 0x5

    move-object/from16 v0, v32

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v28

    goto/16 :goto_0

    .line 563
    .restart local v8    # "time":J
    .restart local v17    # "before":Lcom/ibm/icu/util/TimeZoneTransition;
    .restart local v18    # "btz":Lcom/ibm/icu/util/BasicTimeZone;
    .restart local v21    # "mzid":Ljava/lang/String;
    .restart local v31    # "useStandard":Z
    :cond_b
    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9, v5}, Lcom/ibm/icu/util/BasicTimeZone;->getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v4

    .line 564
    .local v4, "after":Lcom/ibm/icu/util/TimeZoneTransition;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v10

    sub-long/2addr v10, v8

    const-wide v12, 0x3b3922000L

    cmp-long v5, v10, v12

    if-gez v5, :cond_3

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneTransition;->getTo()Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v5

    if-eqz v5, :cond_3

    .line 567
    const/16 v31, 0x0

    goto/16 :goto_1

    .line 573
    .end local v4    # "after":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v17    # "before":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v18    # "btz":Lcom/ibm/icu/util/BasicTimeZone;
    :cond_c
    const/4 v5, 0x2

    new-array v0, v5, [I

    move-object/from16 v23, v0

    .line 574
    .local v23, "offsets":[I
    const-wide v10, 0x3b3922000L

    sub-long v10, v8, v10

    const/4 v5, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v11, v5, v1}, Lcom/ibm/icu/util/TimeZone;->getOffset(JZ[I)V

    .line 575
    const/4 v5, 0x1

    aget v5, v23, v5

    if-eqz v5, :cond_d

    .line 576
    const/16 v31, 0x0

    .line 577
    goto/16 :goto_1

    .line 578
    :cond_d
    const-wide v10, 0x3b3922000L

    add-long/2addr v10, v8

    const/4 v5, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v11, v5, v1}, Lcom/ibm/icu/util/TimeZone;->getOffset(JZ[I)V

    .line 579
    const/4 v5, 0x1

    aget v5, v23, v5

    if-eqz v5, :cond_3

    .line 580
    const/16 v31, 0x0

    goto/16 :goto_1

    .line 586
    .end local v23    # "offsets":[I
    :cond_e
    const/4 v7, 0x1

    goto/16 :goto_2

    .line 595
    :cond_f
    const/4 v7, 0x5

    goto/16 :goto_3

    .line 610
    .restart local v22    # "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_10
    const/4 v5, 0x5

    move-object/from16 v0, v22

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v28

    goto/16 :goto_4
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/impl/ZoneStringFormat;
    .locals 2
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 68
    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->TZFORMAT_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v1, p0}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 69
    .local v0, "tzf":Lcom/ibm/icu/impl/ZoneStringFormat;
    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/ibm/icu/impl/ZoneStringFormat;

    .end local v0    # "tzf":Lcom/ibm/icu/impl/ZoneStringFormat;
    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/ZoneStringFormat;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 71
    .restart local v0    # "tzf":Lcom/ibm/icu/impl/ZoneStringFormat;
    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->TZFORMAT_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v1, p0, v0}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 73
    :cond_0
    return-object v0
.end method

.method private static getLocalizedCountry(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 5
    .param p0, "countryCode"    # Ljava/lang/String;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 782
    const/4 v0, 0x0

    .line 783
    .local v0, "countryStr":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 784
    const-string/jumbo v3, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v3, p1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 793
    .local v1, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    .line 794
    .local v2, "rbloc":Lcom/ibm/icu/util/ULocale;
    sget-object v3, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 795
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "xx_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/ibm/icu/util/ULocale;->getDisplayCountry(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    .line 798
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 799
    :cond_1
    move-object v0, p0

    .line 802
    .end local v1    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v2    # "rbloc":Lcom/ibm/icu/util/ULocale;
    :cond_2
    return-object v0
.end method

.method private static getNameType(I)I
    .locals 2
    .param p0, "typeIdx"    # I

    .prologue
    .line 868
    const/4 v0, -0x1

    .line 869
    .local v0, "type":I
    if-ltz p0, :cond_0

    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->NAMETYPEMAP:[I

    array-length v1, v1

    if-ge p0, v1, :cond_0

    .line 870
    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->NAMETYPEMAP:[I

    aget v0, v1, p0

    .line 872
    :cond_0
    return v0
.end method

.method private static getNameTypeIndex(I)I
    .locals 2
    .param p0, "i"    # I

    .prologue
    .line 847
    const/4 v0, -0x1

    .line 848
    .local v0, "idx":I
    const/4 v1, 0x1

    if-lt p0, v1, :cond_0

    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->INDEXMAP:[I

    array-length v1, v1

    if-ge p0, v1, :cond_0

    .line 849
    sget-object v1, Lcom/ibm/icu/impl/ZoneStringFormat;->INDEXMAP:[I

    aget v0, v1, p0

    .line 851
    :cond_0
    return v0
.end method

.method private getRegion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 879
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 880
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->locale:Lcom/ibm/icu/util/ULocale;

    if-eqz v1, :cond_1

    .line 881
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v1}, Lcom/ibm/icu/util/ULocale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    .line 882
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 883
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v1}, Lcom/ibm/icu/util/ULocale;->addLikelySubtag(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 884
    .local v0, "tmp":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    .line 890
    .end local v0    # "tmp":Lcom/ibm/icu/util/ULocale;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    return-object v1

    .line 887
    :cond_1
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->region:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getRegionFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MessageFormat;
    .locals 2
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 820
    const-string/jumbo v1, "regionFormat"

    invoke-static {p0, v1}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 821
    .local v0, "regionPattern":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 822
    const-string/jumbo v0, "{0}"

    .line 824
    :cond_0
    new-instance v1, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v1, v0, p0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    return-object v1
.end method

.method private getString(Ljava/lang/String;IJZ)Ljava/lang/String;
    .locals 7
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "typeIdx"    # I
    .param p3, "date"    # J
    .param p5, "commonlyUsedOnly"    # Z

    .prologue
    .line 449
    const/4 v3, 0x0

    .line 450
    .local v3, "result":Ljava/lang/String;
    iget-object v5, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 451
    .local v4, "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-nez v4, :cond_0

    .line 453
    invoke-static {p1}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalSystemID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 454
    .local v0, "canonicalID":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 457
    move-object p1, v0

    .line 458
    iget-object v5, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    check-cast v4, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 461
    .end local v0    # "canonicalID":Ljava/lang/String;
    .restart local v4    # "zstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_0
    if-eqz v4, :cond_1

    .line 462
    packed-switch p2, :pswitch_data_0

    .line 478
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    iget-object v5, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    if-eqz v5, :cond_2

    if-eqz p2, :cond_2

    .line 480
    invoke-static {p1, p3, p4}, Lcom/ibm/icu/impl/ZoneMeta;->getMetazoneID(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    .line 481
    .local v1, "mzid":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 482
    iget-object v5, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->mzidToStrings:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;

    .line 483
    .local v2, "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    if-eqz v2, :cond_2

    .line 484
    packed-switch p2, :pswitch_data_1

    .line 501
    .end local v1    # "mzid":Ljava/lang/String;
    .end local v2    # "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :cond_2
    :goto_1
    return-object v3

    .line 467
    :pswitch_0
    invoke-static {v4, p2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v3

    .line 468
    goto :goto_0

    .line 472
    :pswitch_1
    if-eqz p5, :cond_3

    invoke-static {v4}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 473
    :cond_3
    invoke-static {v4, p2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 488
    .restart local v1    # "mzid":Ljava/lang/String;
    .restart local v2    # "mzstrings":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;
    :pswitch_2
    invoke-static {v2, p2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v3

    .line 489
    goto :goto_1

    .line 493
    :pswitch_3
    if-eqz p5, :cond_4

    invoke-static {v2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$300(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 494
    :cond_4
    invoke-static {v2, p2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;->access$200(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStrings;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 484
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getZoneStringFromBundle(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 749
    const/4 v0, 0x0

    .line 750
    .local v0, "zstring":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 752
    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 757
    :cond_0
    :goto_0
    return-object v0

    .line 753
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getZoneStrings(J)[[Ljava/lang/String;
    .locals 9
    .param p1, "date"    # J

    .prologue
    const/4 v8, 0x0

    .line 669
    iget-object v5, p0, Lcom/ibm/icu/impl/ZoneStringFormat;->tzidToStrings:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 670
    .local v3, "tzids":Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v5

    const/16 v6, 0x8

    filled-new-array {v5, v6}, [I

    move-result-object v5

    const-class v6, Ljava/lang/String;

    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Ljava/lang/String;

    .line 671
    .local v4, "zoneStrings":[[Ljava/lang/String;
    const/4 v0, 0x0

    .line 672
    .local v0, "idx":I
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 673
    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 674
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 675
    .local v2, "tzid":Ljava/lang/String;
    aget-object v5, v4, v0

    aput-object v2, v5, v8

    .line 676
    aget-object v5, v4, v0

    const/4 v6, 0x1

    invoke-virtual {p0, v2, p1, p2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getLongStandard(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 677
    aget-object v5, v4, v0

    const/4 v6, 0x2

    invoke-virtual {p0, v2, p1, p2, v8}, Lcom/ibm/icu/impl/ZoneStringFormat;->getShortStandard(Ljava/lang/String;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 678
    aget-object v5, v4, v0

    const/4 v6, 0x3

    invoke-virtual {p0, v2, p1, p2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getLongDaylight(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 679
    aget-object v5, v4, v0

    const/4 v6, 0x4

    invoke-virtual {p0, v2, p1, p2, v8}, Lcom/ibm/icu/impl/ZoneStringFormat;->getShortDaylight(Ljava/lang/String;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 680
    aget-object v5, v4, v0

    const/4 v6, 0x5

    invoke-virtual {p0, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 681
    aget-object v5, v4, v0

    const/4 v6, 0x6

    invoke-virtual {p0, v2, p1, p2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getLongGenericNonLocation(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 682
    aget-object v5, v4, v0

    const/4 v6, 0x7

    invoke-virtual {p0, v2, p1, p2, v8}, Lcom/ibm/icu/impl/ZoneStringFormat;->getShortGenericNonLocation(Ljava/lang/String;JZ)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 683
    add-int/lit8 v0, v0, 0x1

    .line 684
    goto :goto_0

    .line 685
    .end local v2    # "tzid":Ljava/lang/String;
    :cond_0
    return-object v4
.end method

.method private static isCommonlyUsed(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)Z
    .locals 5
    .param p0, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 764
    const/4 v0, 0x0

    .line 765
    .local v0, "commonlyUsed":Z
    if-eqz p0, :cond_0

    .line 767
    :try_start_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "cu"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v1

    .line 768
    .local v1, "cuRes":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1}, Lcom/ibm/icu/util/UResourceBundle;->getInt()I
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 769
    .local v2, "cuValue":I
    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 774
    .end local v1    # "cuRes":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "cuValue":I
    :cond_0
    :goto_0
    return v0

    .line 769
    .restart local v1    # "cuRes":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v2    # "cuValue":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 770
    .end local v1    # "cuRes":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "cuValue":I
    :catch_0
    move-exception v3

    goto :goto_0
.end method


# virtual methods
.method public findGenericLocation(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v0

    return-object v0
.end method

.method public findGenericLong(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 159
    const/16 v0, 0xb

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v0

    return-object v0
.end method

.method public findGenericShort(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 163
    const/16 v0, 0x15

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v0

    return-object v0
.end method

.method public findSpecificLong(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 151
    const/16 v0, 0x28

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v0

    return-object v0
.end method

.method public findSpecificShort(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 155
    const/16 v0, 0x50

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->find(Ljava/lang/String;II)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGenericLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "tzid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 204
    const-wide/16 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move v6, v3

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenericLocationString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;
    .locals 7
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object v1, p0

    move v6, v3

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenericLongString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;
    .locals 1
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0, p1, v0, v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericString(Lcom/ibm/icu/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenericShortString(Lcom/ibm/icu/util/Calendar;Z)Ljava/lang/String;
    .locals 1
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "commonlyUsedOnly"    # Z

    .prologue
    .line 100
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericString(Lcom/ibm/icu/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongDaylight(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J

    .prologue
    .line 176
    const/4 v3, 0x3

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongGenericNonLocation(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J

    .prologue
    .line 180
    const/4 v3, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongGenericPartialLocation(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J

    .prologue
    const/4 v3, 0x0

    .line 184
    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, v3

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericPartialLocationString(Ljava/lang/String;ZJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongStandard(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J

    .prologue
    .line 172
    const/4 v3, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortDaylight(Ljava/lang/String;JZ)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J
    .param p4, "commonlyUsedOnly"    # Z

    .prologue
    .line 192
    const/4 v3, 0x4

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortGenericNonLocation(Ljava/lang/String;JZ)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J
    .param p4, "commonlyUsedOnly"    # Z

    .prologue
    .line 196
    const/4 v3, 0x6

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortGenericPartialLocation(Ljava/lang/String;JZ)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J
    .param p4, "commonlyUsedOnly"    # Z

    .prologue
    .line 200
    const/4 v3, 0x1

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericPartialLocationString(Ljava/lang/String;ZJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortStandard(Ljava/lang/String;JZ)Ljava/lang/String;
    .locals 8
    .param p1, "tzid"    # Ljava/lang/String;
    .param p2, "date"    # J
    .param p4, "commonlyUsedOnly"    # Z

    .prologue
    .line 188
    const/4 v3, 0x2

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpecificLongString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;
    .locals 7
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    const/4 v6, 0x0

    .line 82
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSpecificShortString(Lcom/ibm/icu/util/Calendar;Z)Ljava/lang/String;
    .locals 7
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "commonlyUsedOnly"    # Z

    .prologue
    .line 89
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ZoneStringFormat;->getString(Ljava/lang/String;IJZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoneStrings()[[Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStrings(J)[[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
