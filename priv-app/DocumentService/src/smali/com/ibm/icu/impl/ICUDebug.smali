.class public final Lcom/ibm/icu/impl/ICUDebug;
.super Ljava/lang/Object;
.source "ICUDebug.java"


# static fields
.field private static debug:Z

.field private static help:Z

.field public static final isJDK14OrHigher:Z

.field public static final javaVersion:Lcom/ibm/icu/util/VersionInfo;

.field public static final javaVersionString:Ljava/lang/String;

.field private static params:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 15
    :try_start_0
    const-string/jumbo v1, "ICUDebug"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    sput-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    .line 21
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    const-string/jumbo v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    const-string/jumbo v4, "help"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_3

    :cond_0
    move v1, v2

    :goto_2
    sput-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->help:Z

    .line 24
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    if-eqz v1, :cond_1

    .line 25
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "\nICUDebug="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sget-object v5, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 29
    :cond_1
    const-string/jumbo v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/ICUDebug;->javaVersionString:Ljava/lang/String;

    .line 69
    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->javaVersionString:Ljava/lang/String;

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUDebug;->getInstanceLenient(Ljava/lang/String;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/ICUDebug;->javaVersion:Lcom/ibm/icu/util/VersionInfo;

    .line 71
    const-string/jumbo v1, "1.4.0"

    invoke-static {v1}, Lcom/ibm/icu/util/VersionInfo;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    .line 73
    .local v0, "java14Version":Lcom/ibm/icu/util/VersionInfo;
    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->javaVersion:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v1, v0}, Lcom/ibm/icu/util/VersionInfo;->compareTo(Lcom/ibm/icu/util/VersionInfo;)I

    move-result v1

    if-ltz v1, :cond_4

    :goto_3
    sput-boolean v2, Lcom/ibm/icu/impl/ICUDebug;->isJDK14OrHigher:Z

    .line 74
    return-void

    .end local v0    # "java14Version":Lcom/ibm/icu/util/VersionInfo;
    :cond_2
    move v1, v3

    .line 20
    goto :goto_1

    :cond_3
    move v1, v3

    .line 21
    goto :goto_2

    .restart local v0    # "java14Version":Lcom/ibm/icu/util/VersionInfo;
    :cond_4
    move v2, v3

    .line 73
    goto :goto_3

    .line 17
    .end local v0    # "java14Version":Lcom/ibm/icu/util/VersionInfo;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enabled()Z
    .locals 1

    .prologue
    .line 77
    sget-boolean v0, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    return v0
.end method

.method public static enabled(Ljava/lang/String;)Z
    .locals 4
    .param p0, "arg"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 81
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    if-eqz v1, :cond_1

    .line 82
    sget-object v1, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 83
    .local v0, "result":Z
    :cond_0
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->help:Z

    if-eqz v1, :cond_1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "\nICUDebug.enabled("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, ") = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 86
    .end local v0    # "result":Z
    :cond_1
    return v0
.end method

.method public static getInstanceLenient(Ljava/lang/String;)Lcom/ibm/icu/util/VersionInfo;
    .locals 11
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0x2e

    .line 39
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 40
    .local v1, "chars":[C
    const/4 v4, 0x0

    .local v4, "r":I
    const/4 v7, 0x0

    .local v7, "w":I
    const/4 v2, 0x0

    .line 41
    .local v2, "count":I
    const/4 v3, 0x0

    .line 42
    .local v3, "numeric":Z
    :goto_0
    array-length v9, v1

    if-ge v4, v9, :cond_1

    .line 43
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "r":I
    .local v5, "r":I
    aget-char v0, v1, v4

    .line 44
    .local v0, "c":C
    const/16 v9, 0x30

    if-lt v0, v9, :cond_0

    const/16 v9, 0x39

    if-le v0, v9, :cond_4

    .line 45
    :cond_0
    if-eqz v3, :cond_3

    .line 46
    const/4 v9, 0x3

    if-ne v2, v9, :cond_2

    move v4, v5

    .line 59
    .end local v0    # "c":C
    .end local v5    # "r":I
    .restart local v4    # "r":I
    :cond_1
    :goto_1
    if-lez v7, :cond_5

    add-int/lit8 v9, v7, -0x1

    aget-char v9, v1, v9

    if-ne v9, v10, :cond_5

    .line 60
    add-int/lit8 v7, v7, -0x1

    .line 61
    goto :goto_1

    .line 50
    .end local v4    # "r":I
    .restart local v0    # "c":C
    .restart local v5    # "r":I
    :cond_2
    const/4 v3, 0x0

    .line 51
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "w":I
    .local v8, "w":I
    aput-char v10, v1, v7

    .line 52
    add-int/lit8 v2, v2, 0x1

    move v7, v8

    .end local v8    # "w":I
    .restart local v7    # "w":I
    :cond_3
    :goto_2
    move v4, v5

    .line 58
    .end local v5    # "r":I
    .restart local v4    # "r":I
    goto :goto_0

    .line 55
    .end local v4    # "r":I
    .restart local v5    # "r":I
    :cond_4
    const/4 v3, 0x1

    .line 56
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "w":I
    .restart local v8    # "w":I
    aput-char v0, v1, v7

    move v7, v8

    .end local v8    # "w":I
    .restart local v7    # "w":I
    goto :goto_2

    .line 63
    .end local v0    # "c":C
    .end local v5    # "r":I
    .restart local v4    # "r":I
    :cond_5
    new-instance v6, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v6, v1, v9, v7}, Ljava/lang/String;-><init>([CII)V

    .line 65
    .local v6, "vs":Ljava/lang/String;
    invoke-static {v6}, Lcom/ibm/icu/util/VersionInfo;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v9

    return-object v9
.end method

.method public static value(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "arg"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 90
    const-string/jumbo v2, "false"

    .line 91
    .local v2, "result":Ljava/lang/String;
    sget-boolean v3, Lcom/ibm/icu/impl/ICUDebug;->debug:Z

    if-eqz v3, :cond_2

    .line 92
    sget-object v3, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 93
    .local v0, "index":I
    if-eq v0, v5, :cond_1

    .line 94
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v0, v3

    .line 95
    sget-object v3, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v0, :cond_3

    sget-object v3, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3d

    if-ne v3, v4, :cond_3

    .line 96
    add-int/lit8 v0, v0, 0x1

    .line 97
    sget-object v3, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    const-string/jumbo v4, ","

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 98
    .local v1, "limit":I
    sget-object v3, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    if-ne v1, v5, :cond_0

    sget-object v4, Lcom/ibm/icu/impl/ICUDebug;->params:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .end local v1    # "limit":I
    :cond_0
    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 104
    :cond_1
    :goto_0
    sget-boolean v3, Lcom/ibm/icu/impl/ICUDebug;->help:Z

    if-eqz v3, :cond_2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "\nICUDebug.value("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ") = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 106
    .end local v0    # "index":I
    :cond_2
    return-object v2

    .line 100
    .restart local v0    # "index":I
    :cond_3
    const-string/jumbo v2, "true"

    goto :goto_0
.end method
