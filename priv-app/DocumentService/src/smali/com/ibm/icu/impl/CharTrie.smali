.class public Lcom/ibm/icu/impl/CharTrie;
.super Lcom/ibm/icu/impl/Trie;
.source "CharTrie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/CharTrie$FriendAgent;
    }
.end annotation


# instance fields
.field private m_data_:[C

.field private m_friendAgent_:Lcom/ibm/icu/impl/CharTrie$FriendAgent;

.field private m_initialValue_:C


# direct methods
.method public constructor <init>(IILcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 7
    .param p1, "initialValue"    # I
    .param p2, "leadUnitValue"    # I
    .param p3, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;

    .prologue
    .line 66
    const/16 v5, 0x820

    new-array v5, v5, [C

    const/16 v6, 0x200

    invoke-direct {p0, v5, v6, p3}, Lcom/ibm/icu/impl/Trie;-><init>([CILcom/ibm/icu/impl/Trie$DataManipulate;)V

    .line 74
    const/16 v3, 0x100

    .local v3, "latin1Length":I
    move v1, v3

    .line 75
    .local v1, "dataLength":I
    if-eq p2, p1, :cond_0

    .line 76
    add-int/lit8 v1, v1, 0x20

    .line 78
    :cond_0
    new-array v5, v1, [C

    iput-object v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    .line 79
    iput v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataLength_:I

    .line 81
    int-to-char v5, p1

    iput-char v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    .line 88
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 89
    iget-object v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    int-to-char v6, p1

    aput-char v6, v5, v2

    .line 88
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 92
    :cond_1
    if-eq p2, p1, :cond_3

    .line 94
    const/16 v5, 0x40

    int-to-char v0, v5

    .line 95
    .local v0, "block":C
    const/16 v2, 0x6c0

    .line 96
    const/16 v4, 0x6e0

    .line 97
    .local v4, "limit":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 98
    iget-object v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_index_:[C

    aput-char v0, v5, v2

    .line 97
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    :cond_2
    add-int/lit8 v4, v3, 0x20

    .line 103
    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_3

    .line 104
    iget-object v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    int-to-char v6, p2

    aput-char v6, v5, v2

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 108
    .end local v0    # "block":C
    .end local v4    # "limit":I
    :cond_3
    new-instance v5, Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    invoke-direct {v5, p0}, Lcom/ibm/icu/impl/CharTrie$FriendAgent;-><init>(Lcom/ibm/icu/impl/CharTrie;)V

    iput-object v5, p0, Lcom/ibm/icu/impl/CharTrie;->m_friendAgent_:Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V
    .locals 2
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "dataManipulate"    # Lcom/ibm/icu/impl/Trie$DataManipulate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/Trie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    .line 44
    invoke-virtual {p0}, Lcom/ibm/icu/impl/CharTrie;->isCharTrie()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Data given does not belong to a char trie."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    new-instance v0, Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/CharTrie$FriendAgent;-><init>(Lcom/ibm/icu/impl/CharTrie;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_friendAgent_:Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    .line 49
    return-void
.end method

.method static access$000(Lcom/ibm/icu/impl/CharTrie;)[C
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/CharTrie;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    return-object v0
.end method

.method static access$100(Lcom/ibm/icu/impl/CharTrie;)C
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/CharTrie;

    .prologue
    .line 25
    iget-char v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 265
    invoke-super {p0, p1}, Lcom/ibm/icu/impl/Trie;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 266
    .local v1, "result":Z
    if-eqz v1, :cond_0

    instance-of v3, p1, Lcom/ibm/icu/impl/CharTrie;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 267
    check-cast v0, Lcom/ibm/icu/impl/CharTrie;

    .line 268
    .local v0, "othertrie":Lcom/ibm/icu/impl/CharTrie;
    iget-char v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    iget-char v4, v0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    .line 270
    .end local v0    # "othertrie":Lcom/ibm/icu/impl/CharTrie;
    :cond_0
    return v2
.end method

.method public final getBMPValue(C)C
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 204
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/CharTrie;->getBMPOffset(C)I

    move-result v1

    aget-char v0, v0, v1

    return v0
.end method

.method public final getCodePointValue(I)C
    .locals 3
    .param p1, "ch"    # I

    .prologue
    .line 166
    if-ltz p1, :cond_0

    const v1, 0xd800

    if-ge p1, v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_index_:[C

    shr-int/lit8 v2, p1, 0x5

    aget-char v1, v1, v2

    shl-int/lit8 v1, v1, 0x2

    and-int/lit8 v2, p1, 0x1f

    add-int v0, v1, v2

    .line 170
    .local v0, "offset":I
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    aget-char v1, v1, v0

    .line 178
    :goto_0
    return v1

    .line 174
    .end local v0    # "offset":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointOffset(I)I

    move-result v0

    .line 178
    .restart local v0    # "offset":I
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    aget-char v1, v1, v0

    goto :goto_0

    :cond_1
    iget-char v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    goto :goto_0
.end method

.method protected final getInitialValue()I
    .locals 1

    .prologue
    .line 339
    iget-char v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    return v0
.end method

.method public final getLatin1LinearValue(C)C
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 253
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    iget v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataOffset_:I

    add-int/lit8 v1, v1, 0x20

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public final getLeadValue(C)C
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 192
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/CharTrie;->getLeadOffset(C)I

    move-result v1

    aget-char v0, v0, v1

    return v0
.end method

.method protected final getSurrogateOffset(CC)I
    .locals 3
    .param p1, "lead"    # C
    .param p2, "trail"    # C

    .prologue
    .line 303
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v1, :cond_0

    .line 304
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The field DataManipulate in this Trie is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 309
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/CharTrie;->getLeadValue(C)C

    move-result v2

    invoke-interface {v1, v2}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v0

    .line 312
    .local v0, "offset":I
    if-lez v0, :cond_1

    .line 313
    and-int/lit16 v1, p2, 0x3ff

    int-to-char v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/impl/CharTrie;->getRawOffset(IC)I

    move-result v1

    .line 318
    :goto_0
    return v1

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public final getSurrogateValue(CC)C
    .locals 2
    .param p1, "lead"    # C
    .param p2, "trail"    # C

    .prologue
    .line 214
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/impl/CharTrie;->getSurrogateOffset(CC)I

    move-result v0

    .line 215
    .local v0, "offset":I
    if-lez v0, :cond_0

    .line 216
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    aget-char v1, v1, v0

    .line 218
    :goto_0
    return v1

    :cond_0
    iget-char v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    goto :goto_0
.end method

.method public final getTrailValue(IC)C
    .locals 3
    .param p1, "leadvalue"    # I
    .param p2, "trail"    # C

    .prologue
    .line 232
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v1, :cond_0

    .line 233
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "The field DataManipulate in this Trie is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    invoke-interface {v1, p1}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v0

    .line 237
    .local v0, "offset":I
    if-lez v0, :cond_1

    .line 238
    iget-object v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    and-int/lit16 v2, p2, 0x3ff

    int-to-char v2, v2

    invoke-virtual {p0, v0, v2}, Lcom/ibm/icu/impl/CharTrie;->getRawOffset(IC)I

    move-result v2

    aget-char v1, v1, v2

    .line 241
    :goto_0
    return v1

    :cond_1
    iget-char v1, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    goto :goto_0
.end method

.method protected final getValue(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 330
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public putIndexData(Lcom/ibm/icu/impl/UCharacterProperty;)V
    .locals 1
    .param p1, "friend"    # Lcom/ibm/icu/impl/UCharacterProperty;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/ibm/icu/impl/CharTrie;->m_friendAgent_:Lcom/ibm/icu/impl/CharTrie$FriendAgent;

    invoke-virtual {p1, v0}, Lcom/ibm/icu/impl/UCharacterProperty;->setIndexData(Lcom/ibm/icu/impl/CharTrie$FriendAgent;)V

    .line 152
    return-void
.end method

.method protected final unserialize(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 286
    .local v2, "input":Ljava/io/DataInputStream;
    iget v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataOffset_:I

    iget v4, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataLength_:I

    add-int v1, v3, v4

    .line 287
    .local v1, "indexDataLength":I
    new-array v3, v1, [C

    iput-object v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_index_:[C

    .line 288
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 289
    iget-object v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_index_:[C

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readChar()C

    move-result v4

    aput-char v4, v3, v0

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 291
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_index_:[C

    iput-object v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    .line 292
    iget-object v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_data_:[C

    iget v4, p0, Lcom/ibm/icu/impl/CharTrie;->m_dataOffset_:I

    aget-char v3, v3, v4

    iput-char v3, p0, Lcom/ibm/icu/impl/CharTrie;->m_initialValue_:C

    .line 293
    return-void
.end method
