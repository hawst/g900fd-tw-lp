.class public Lcom/ibm/icu/impl/PluralRulesLoader;
.super Ljava/lang/Object;
.source "PluralRulesLoader.java"


# static fields
.field public static final loader:Lcom/ibm/icu/impl/PluralRulesLoader;


# instance fields
.field private localeIdToRulesId:Ljava/util/Map;

.field private final rulesIdToRules:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/ibm/icu/impl/PluralRulesLoader;

    invoke-direct {v0}, Lcom/ibm/icu/impl/PluralRulesLoader;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/PluralRulesLoader;->loader:Lcom/ibm/icu/impl/PluralRulesLoader;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->rulesIdToRules:Ljava/util/Map;

    .line 33
    return-void
.end method

.method private getLocaleIdToRulesIdMap()Ljava/util/Map;
    .locals 8

    .prologue
    .line 55
    iget-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->localeIdToRulesId:Ljava/util/Map;

    if-nez v7, :cond_0

    .line 57
    :try_start_0
    invoke-virtual {p0}, Lcom/ibm/icu/impl/PluralRulesLoader;->getPluralBundle()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    .line 58
    .local v5, "pluralb":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v7, "locales"

    invoke-virtual {v5, v7}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 59
    .local v4, "localeb":Lcom/ibm/icu/util/UResourceBundle;
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iput-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->localeIdToRulesId:Ljava/util/Map;

    .line 60
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 61
    invoke-virtual {v4, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 62
    .local v0, "b":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "id":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v6

    .line 64
    .local v6, "value":Ljava/lang/String;
    iget-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->localeIdToRulesId:Ljava/util/Map;

    invoke-interface {v7, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    .end local v0    # "b":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "i":I
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "localeb":Lcom/ibm/icu/util/UResourceBundle;
    .end local v5    # "pluralb":Lcom/ibm/icu/util/UResourceBundle;
    .end local v6    # "value":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/util/MissingResourceException;
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->localeIdToRulesId:Ljava/util/Map;

    .line 71
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :cond_0
    iget-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->localeIdToRulesId:Ljava/util/Map;

    return-object v7
.end method


# virtual methods
.method public forLocale(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/PluralRules;
    .locals 3
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/PluralRulesLoader;->getRulesIdForLocale(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "rulesId":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 145
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/PluralRules;->DEFAULT:Lcom/ibm/icu/text/PluralRules;

    .line 151
    :cond_1
    :goto_0
    return-object v0

    .line 147
    :cond_2
    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/PluralRulesLoader;->getRulesForRulesId(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;

    move-result-object v0

    .line 148
    .local v0, "rules":Lcom/ibm/icu/text/PluralRules;
    if-nez v0, :cond_1

    .line 149
    sget-object v0, Lcom/ibm/icu/text/PluralRules;->DEFAULT:Lcom/ibm/icu/text/PluralRules;

    goto :goto_0
.end method

.method public getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 6

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/ibm/icu/impl/PluralRulesLoader;->getLocaleIdToRulesIdMap()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 41
    .local v1, "keys":Ljava/util/Set;
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    new-array v2, v5, [Lcom/ibm/icu/util/ULocale;

    .line 42
    .local v2, "locales":[Lcom/ibm/icu/util/ULocale;
    const/4 v3, 0x0

    .line 43
    .local v3, "n":I
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iter":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 44
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "n":I
    .local v4, "n":I
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lcom/ibm/icu/util/ULocale;->createCanonical(Ljava/lang/String;)Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    aput-object v5, v2, v3

    move v3, v4

    .line 45
    .end local v4    # "n":I
    .restart local v3    # "n":I
    goto :goto_0

    .line 46
    :cond_0
    return-object v2
.end method

.method public getPluralBundle()Lcom/ibm/icu/util/UResourceBundle;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 130
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v1, "plurals"

    sget-object v2, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    return-object v0
.end method

.method public getRulesForRulesId(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;
    .locals 8
    .param p1, "rulesId"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->rulesIdToRules:Ljava/util/Map;

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/PluralRules;

    .line 99
    .local v3, "rules":Lcom/ibm/icu/text/PluralRules;
    if-nez v3, :cond_2

    .line 101
    :try_start_0
    invoke-virtual {p0}, Lcom/ibm/icu/impl/PluralRulesLoader;->getPluralBundle()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 102
    .local v2, "pluralb":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v7, "rules"

    invoke-virtual {v2, v7}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 103
    .local v4, "rulesb":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v4, p1}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    .line 105
    .local v6, "setb":Lcom/ibm/icu/util/UResourceBundle;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 106
    .local v5, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v6}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 107
    invoke-virtual {v6, v1}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 108
    .local v0, "b":Lcom/ibm/icu/util/UResourceBundle;
    if-lez v1, :cond_0

    .line 109
    const-string/jumbo v7, "; "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    :cond_0
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 112
    const-string/jumbo v7, ": "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "b":Lcom/ibm/icu/util/UResourceBundle;
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/ibm/icu/text/PluralRules;->parseDescription(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 119
    .end local v1    # "i":I
    .end local v2    # "pluralb":Lcom/ibm/icu/util/UResourceBundle;
    .end local v4    # "rulesb":Lcom/ibm/icu/util/UResourceBundle;
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    .end local v6    # "setb":Lcom/ibm/icu/util/UResourceBundle;
    :goto_1
    iget-object v7, p0, Lcom/ibm/icu/impl/PluralRulesLoader;->rulesIdToRules:Ljava/util/Map;

    invoke-interface {v7, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    :cond_2
    return-object v3

    .line 117
    :catch_0
    move-exception v7

    goto :goto_1

    .line 116
    :catch_1
    move-exception v7

    goto :goto_1
.end method

.method public getRulesIdForLocale(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 5
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/ibm/icu/impl/PluralRulesLoader;->getLocaleIdToRulesIdMap()Ljava/util/Map;

    move-result-object v0

    .line 81
    .local v0, "idMap":Ljava/util/Map;
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/ibm/icu/util/ULocale;->canonicalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "localeId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 83
    .local v3, "rulesId":Ljava/lang/String;
    :goto_0
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "rulesId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .restart local v3    # "rulesId":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 84
    const-string/jumbo v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 85
    .local v1, "ix":I
    const/4 v4, -0x1

    if-ne v1, v4, :cond_1

    .line 90
    .end local v1    # "ix":I
    :cond_0
    return-object v3

    .line 88
    .restart local v1    # "ix":I
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 89
    goto :goto_0
.end method
