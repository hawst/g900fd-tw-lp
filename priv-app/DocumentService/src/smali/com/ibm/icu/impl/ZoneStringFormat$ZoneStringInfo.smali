.class public Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
.super Ljava/lang/Object;
.source "ZoneStringFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ZoneStringFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ZoneStringInfo"
.end annotation


# instance fields
.field private id:Ljava/lang/String;

.field private str:Ljava/lang/String;

.field private type:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->id:Ljava/lang/String;

    .line 115
    iput-object p2, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->str:Ljava/lang/String;

    .line 116
    iput p3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    .line 117
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/ibm/icu/impl/ZoneStringFormat$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/ibm/icu/impl/ZoneStringFormat$1;

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static access$600(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getType()I

    move-result v0

    return v0
.end method

.method private getType()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    return v0
.end method


# virtual methods
.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->str:Ljava/lang/String;

    return-object v0
.end method

.method public isDaylight()Z
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGeneric()Z
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isStandard()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isDaylight()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStandard()Z
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->type:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 129
    :cond_0
    const/4 v0, 0x1

    .line 131
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
