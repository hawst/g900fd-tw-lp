.class public Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
.super Ljava/lang/Object;
.source "RelativeDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/RelativeDateFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "URelativeString"
.end annotation


# instance fields
.field public offset:I

.field public string:Ljava/lang/String;

.field private final this$0:Lcom/ibm/icu/impl/RelativeDateFormat;


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/RelativeDateFormat;ILjava/lang/String;)V
    .locals 0
    .param p2, "offset"    # I
    .param p3, "string"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->this$0:Lcom/ibm/icu/impl/RelativeDateFormat;

    .line 36
    iput p2, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    .line 37
    iput-object p3, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->string:Ljava/lang/String;

    .line 38
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/impl/RelativeDateFormat;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "offset"    # Ljava/lang/String;
    .param p3, "string"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->this$0:Lcom/ibm/icu/impl/RelativeDateFormat;

    .line 40
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    .line 41
    iput-object p3, p0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->string:Ljava/lang/String;

    .line 42
    return-void
.end method
