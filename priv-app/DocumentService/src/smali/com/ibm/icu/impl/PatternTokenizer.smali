.class public Lcom/ibm/icu/impl/PatternTokenizer;
.super Ljava/lang/Object;
.source "PatternTokenizer.java"


# static fields
.field private static final AFTER_QUOTE:I = -0x1

.field public static final BACK_SLASH:C = '\\'

.field public static final BROKEN_ESCAPE:I = 0x4

.field public static final BROKEN_QUOTE:I = 0x3

.field public static final DONE:I = 0x0

.field private static final HEX:I = 0x4

.field private static IN_QUOTE:I = 0x0

.field public static final LITERAL:I = 0x2

.field private static final NONE:I = 0x0

.field private static final NORMAL_QUOTE:I = 0x2

.field private static NO_QUOTE:I = 0x0

.field public static final SINGLE_QUOTE:C = '\''

.field private static final SLASH_START:I = 0x3

.field private static final START_QUOTE:I = 0x1

.field public static final SYNTAX:I = 0x1

.field public static final UNKNOWN:I = 0x5


# instance fields
.field private escapeCharacters:Lcom/ibm/icu/text/UnicodeSet;

.field private extraQuotingCharacters:Lcom/ibm/icu/text/UnicodeSet;

.field private ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

.field private limit:I

.field private transient needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

.field private pattern:Ljava/lang/String;

.field private start:I

.field private syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

.field private usingQuote:Z

.field private usingSlash:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, -0x1

    sput v0, Lcom/ibm/icu/impl/PatternTokenizer;->NO_QUOTE:I

    const/4 v0, -0x2

    sput v0, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 26
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 27
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->extraQuotingCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 28
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->escapeCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 29
    iput-boolean v1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    .line 30
    iput-boolean v1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    return-void
.end method

.method private appendEscaped(Ljava/lang/StringBuffer;I)V
    .locals 2
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "cp"    # I

    .prologue
    .line 238
    const v0, 0xffff

    if-gt p2, v0, :cond_0

    .line 239
    const-string/jumbo v0, "\\u"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {p2, v1}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    const-string/jumbo v0, "\\U"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p2, v1}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method


# virtual methods
.method public getEscapeCharacters()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->escapeCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method public getExtraQuotingCharacters()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->extraQuotingCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method public getIgnorableCharacters()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    return v0
.end method

.method public getSyntaxCharacters()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method public isUsingQuote()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    return v0
.end method

.method public isUsingSlash()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    return v0
.end method

.method public next(Ljava/lang/StringBuffer;)I
    .locals 10
    .param p1, "buffer"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v9, 0x5c

    .line 269
    iget v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    iget v8, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    if-lt v7, v8, :cond_0

    const/4 v6, 0x0

    .line 398
    :goto_0
    return v6

    .line 270
    :cond_0
    const/4 v6, 0x5

    .line 271
    .local v6, "status":I
    const/4 v4, 0x5

    .line 272
    .local v4, "lastQuote":I
    const/4 v5, 0x0

    .line 273
    .local v5, "quoteStatus":I
    const/4 v1, 0x0

    .line 274
    .local v1, "hexCount":I
    const/4 v2, 0x0

    .line 277
    .local v2, "hexValue":I
    iget v3, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .local v3, "i":I
    :goto_1
    iget v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    if-ge v3, v7, :cond_b

    .line 278
    iget-object v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->pattern:Ljava/lang/String;

    invoke-static {v7, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 280
    .local v0, "cp":I
    packed-switch v5, :pswitch_data_0

    .line 354
    :goto_2
    :pswitch_0
    iget-object v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v7, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 277
    :cond_1
    :goto_3
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v3, v7

    goto :goto_1

    .line 282
    :pswitch_1
    sparse-switch v0, :sswitch_data_0

    .line 294
    iget-boolean v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    if-eqz v7, :cond_2

    .line 295
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 296
    const/4 v5, 0x0

    .line 297
    goto :goto_3

    .line 284
    :sswitch_0
    const/4 v5, 0x4

    .line 285
    const/4 v1, 0x4

    .line 286
    const/4 v2, 0x0

    .line 287
    goto :goto_3

    .line 289
    :sswitch_1
    const/4 v5, 0x4

    .line 290
    const/16 v1, 0x8

    .line 291
    const/4 v2, 0x0

    .line 292
    goto :goto_3

    .line 299
    :cond_2
    invoke-virtual {p1, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 300
    const/4 v5, 0x0

    .line 303
    goto :goto_2

    .line 305
    :pswitch_2
    shl-int/lit8 v2, v2, 0x4

    .line 306
    add-int/2addr v2, v0

    .line 307
    sparse-switch v0, :sswitch_data_1

    .line 315
    iput v3, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 316
    const/4 v6, 0x4

    goto :goto_0

    .line 309
    :sswitch_2
    add-int/lit8 v2, v2, -0x30

    .line 318
    :goto_4
    add-int/lit8 v1, v1, -0x1

    .line 319
    if-nez v1, :cond_1

    .line 320
    const/4 v5, 0x0

    .line 321
    invoke-static {p1, v2}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 311
    :sswitch_3
    add-int/lit8 v2, v2, -0x57

    goto :goto_4

    .line 313
    :sswitch_4
    add-int/lit8 v2, v2, -0x37

    goto :goto_4

    .line 327
    :pswitch_3
    if-ne v0, v4, :cond_3

    .line 328
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 329
    const/4 v5, 0x2

    .line 330
    goto :goto_3

    .line 332
    :cond_3
    const/4 v5, 0x0

    .line 333
    goto :goto_2

    .line 336
    :pswitch_4
    if-ne v0, v4, :cond_4

    .line 337
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 338
    const/4 v5, 0x0

    .line 339
    goto :goto_3

    .line 342
    :cond_4
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 343
    const/4 v5, 0x2

    .line 344
    goto :goto_3

    .line 346
    :pswitch_5
    if-ne v0, v4, :cond_5

    .line 347
    const/4 v5, -0x1

    .line 348
    goto :goto_3

    .line 350
    :cond_5
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 358
    :cond_6
    iget-object v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v7, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 359
    const/4 v7, 0x5

    if-ne v6, v7, :cond_7

    .line 360
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 361
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v7, v3

    iput v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 362
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 364
    :cond_7
    iput v3, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    goto/16 :goto_0

    .line 369
    :cond_8
    const/4 v6, 0x2

    .line 370
    if-ne v0, v9, :cond_9

    .line 371
    const/4 v5, 0x3

    .line 372
    goto :goto_3

    .line 373
    :cond_9
    iget-boolean v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    if-eqz v7, :cond_a

    const/16 v7, 0x27

    if-ne v0, v7, :cond_a

    .line 374
    move v4, v0

    .line 375
    const/4 v5, 0x1

    .line 376
    goto/16 :goto_3

    .line 379
    :cond_a
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 382
    .end local v0    # "cp":I
    :cond_b
    iget v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    iput v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 383
    packed-switch v5, :pswitch_data_1

    goto/16 :goto_0

    .line 395
    :pswitch_6
    const/4 v6, 0x3

    goto/16 :goto_0

    .line 385
    :pswitch_7
    const/4 v6, 0x4

    .line 386
    goto/16 :goto_0

    .line 388
    :pswitch_8
    iget-boolean v7, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    if-eqz v7, :cond_c

    .line 389
    const/4 v6, 0x4

    .line 390
    goto/16 :goto_0

    .line 391
    :cond_c
    invoke-virtual {p1, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 282
    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_1
        0x75 -> :sswitch_0
    .end sparse-switch

    .line 307
    :sswitch_data_1
    .sparse-switch
        0x30 -> :sswitch_2
        0x31 -> :sswitch_2
        0x32 -> :sswitch_2
        0x33 -> :sswitch_2
        0x34 -> :sswitch_2
        0x35 -> :sswitch_2
        0x36 -> :sswitch_2
        0x37 -> :sswitch_2
        0x38 -> :sswitch_2
        0x39 -> :sswitch_2
        0x41 -> :sswitch_4
        0x42 -> :sswitch_4
        0x43 -> :sswitch_4
        0x44 -> :sswitch_4
        0x45 -> :sswitch_4
        0x46 -> :sswitch_4
        0x61 -> :sswitch_3
        0x62 -> :sswitch_3
        0x63 -> :sswitch_3
        0x64 -> :sswitch_3
        0x65 -> :sswitch_3
        0x66 -> :sswitch_3
    .end sparse-switch

    .line 383
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public normalize()Ljava/lang/String;
    .locals 5

    .prologue
    .line 246
    iget v1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 247
    .local v1, "oldStart":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 248
    .local v2, "result":Ljava/lang/StringBuffer;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 250
    .local v0, "buffer":Ljava/lang/StringBuffer;
    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 251
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->next(Ljava/lang/StringBuffer;)I

    move-result v3

    .line 252
    .local v3, "status":I
    if-nez v3, :cond_0

    .line 253
    iput v1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 254
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 256
    :cond_0
    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 257
    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->quoteLiteral(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 259
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public quoteLiteral(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 161
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->quoteLiteral(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public quoteLiteral(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x5c

    const/16 v6, 0x27

    .line 171
    iget-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    if-nez v4, :cond_1

    .line 172
    new-instance v4, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v4}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    iget-object v5, p0, Lcom/ibm/icu/impl/PatternTokenizer;->syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/impl/PatternTokenizer;->ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/impl/PatternTokenizer;->extraQuotingCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 173
    iget-boolean v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v7}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 174
    :cond_0
    iget-boolean v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v6}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 176
    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 177
    .local v3, "result":Ljava/lang/StringBuffer;
    sget v2, Lcom/ibm/icu/impl/PatternTokenizer;->NO_QUOTE:I

    .line 179
    .local v2, "quotedChar":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_b

    .line 180
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 181
    .local v0, "cp":I
    iget-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->escapeCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 183
    sget v4, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    if-ne v2, v4, :cond_2

    .line 184
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 185
    sget v2, Lcom/ibm/icu/impl/PatternTokenizer;->NO_QUOTE:I

    .line 187
    :cond_2
    invoke-direct {p0, v3, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->appendEscaped(Ljava/lang/StringBuffer;I)V

    .line 179
    :cond_3
    :goto_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0

    .line 191
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 193
    sget v4, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    if-ne v2, v4, :cond_5

    .line 194
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 195
    iget-boolean v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    if-eqz v4, :cond_3

    if-ne v0, v6, :cond_3

    .line 196
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 201
    :cond_5
    iget-boolean v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    if-eqz v4, :cond_6

    .line 202
    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 203
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 206
    :cond_6
    iget-boolean v4, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    if-eqz v4, :cond_8

    .line 207
    if-ne v0, v6, :cond_7

    .line 208
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 209
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 212
    :cond_7
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 213
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 214
    sget v2, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    .line 215
    goto :goto_1

    .line 218
    :cond_8
    invoke-direct {p0, v3, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->appendEscaped(Ljava/lang/StringBuffer;I)V

    goto :goto_1

    .line 223
    :cond_9
    sget v4, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    if-ne v2, v4, :cond_a

    .line 224
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 225
    sget v2, Lcom/ibm/icu/impl/PatternTokenizer;->NO_QUOTE:I

    .line 227
    :cond_a
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 231
    .end local v0    # "cp":I
    :cond_b
    sget v4, Lcom/ibm/icu/impl/PatternTokenizer;->IN_QUOTE:I

    if-ne v2, v4, :cond_c

    .line 232
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 234
    :cond_c
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public setEscapeCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "escapeCharacters"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->escapeCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 90
    return-object p0
.end method

.method public setExtraQuotingCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "syntaxCharacters"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 75
    invoke-virtual {p1}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->extraQuotingCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 77
    return-object p0
.end method

.method public setIgnorableCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "ignorableCharacters"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->ignorableCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 51
    return-object p0
.end method

.method public setLimit(I)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 0
    .param p1, "limit"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    .line 121
    return-object p0
.end method

.method public setPattern(Ljava/lang/CharSequence;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "pattern"    # Ljava/lang/CharSequence;

    .prologue
    .line 137
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->setPattern(Ljava/lang/String;)Lcom/ibm/icu/impl/PatternTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public setPattern(Ljava/lang/String;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 142
    if-nez p1, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Inconsistent arguments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 146
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->limit:I

    .line 147
    iput-object p1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->pattern:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public setStart(I)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 0
    .param p1, "start"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->start:I

    .line 128
    return-object p0
.end method

.method public setSyntaxCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "syntaxCharacters"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 65
    invoke-virtual {p1}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->syntaxCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 67
    return-object p0
.end method

.method public setUsingQuote(Z)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "usingQuote"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingQuote:Z

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 98
    return-object p0
.end method

.method public setUsingSlash(Z)Lcom/ibm/icu/impl/PatternTokenizer;
    .locals 1
    .param p1, "usingSlash"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/ibm/icu/impl/PatternTokenizer;->usingSlash:Z

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/PatternTokenizer;->needingQuoteCharacters:Lcom/ibm/icu/text/UnicodeSet;

    .line 106
    return-object p0
.end method
