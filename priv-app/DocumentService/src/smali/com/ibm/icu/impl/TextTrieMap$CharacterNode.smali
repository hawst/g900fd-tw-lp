.class Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
.super Ljava/lang/Object;
.source "TextTrieMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/TextTrieMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CharacterNode"
.end annotation


# instance fields
.field character:I

.field children:Ljava/util/List;

.field objlist:Ljava/util/List;

.field private final this$0:Lcom/ibm/icu/impl/TextTrieMap;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/impl/TextTrieMap;I)V
    .locals 0
    .param p2, "ch"    # I

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->this$0:Lcom/ibm/icu/impl/TextTrieMap;

    .line 165
    iput p2, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->character:I

    .line 166
    return-void
.end method


# virtual methods
.method public addChildNode(I)Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    .locals 6
    .param p1, "ch"    # I

    .prologue
    .line 213
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    if-nez v4, :cond_0

    .line 214
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    .line 215
    new-instance v2, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->this$0:Lcom/ibm/icu/impl/TextTrieMap;

    invoke-direct {v2, v4, p1}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;-><init>(Lcom/ibm/icu/impl/TextTrieMap;I)V

    .line 216
    .local v2, "newNode":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .end local v2    # "newNode":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    :goto_0
    return-object v2

    .line 219
    :cond_0
    const/4 v3, 0x0

    .line 220
    .local v3, "node":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 221
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    .line 222
    .local v0, "cur":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->this$0:Lcom/ibm/icu/impl/TextTrieMap;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->getCharacter()I

    move-result v5

    invoke-static {v4, p1, v5}, Lcom/ibm/icu/impl/TextTrieMap;->access$100(Lcom/ibm/icu/impl/TextTrieMap;II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 223
    move-object v3, v0

    .line 227
    .end local v0    # "cur":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    :cond_1
    if-nez v3, :cond_2

    .line 228
    new-instance v3, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;

    .end local v3    # "node":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->this$0:Lcom/ibm/icu/impl/TextTrieMap;

    invoke-direct {v3, v4, p1}, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;-><init>(Lcom/ibm/icu/impl/TextTrieMap;I)V

    .line 229
    .restart local v3    # "node":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    iget-object v4, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object v2, v3

    .line 231
    goto :goto_0

    .line 220
    .restart local v0    # "cur":Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public addObject(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->objlist:Ljava/util/List;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->objlist:Ljava/util/List;

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->objlist:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public getCharacter()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->character:I

    return v0
.end method

.method public getChildNodes()Ljava/util/List;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->children:Ljava/util/List;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->objlist:Ljava/util/List;

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 200
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$CharacterNode;->objlist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method
