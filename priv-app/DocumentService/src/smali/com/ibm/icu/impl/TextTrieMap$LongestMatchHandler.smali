.class Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;
.super Ljava/lang/Object;
.source "TextTrieMap.java"

# interfaces
.implements Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/TextTrieMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LongestMatchHandler"
.end annotation


# instance fields
.field private length:I

.field private matches:Ljava/util/Iterator;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->matches:Ljava/util/Iterator;

    .line 261
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->length:I

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/impl/TextTrieMap$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/impl/TextTrieMap$1;

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public getMatches()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->matches:Ljava/util/Iterator;

    return-object v0
.end method

.method public handlePrefixMatch(ILjava/util/Iterator;)Z
    .locals 1
    .param p1, "matchLength"    # I
    .param p2, "values"    # Ljava/util/Iterator;

    .prologue
    .line 264
    iget v0, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->length:I

    if-le p1, v0, :cond_0

    .line 265
    iput p1, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->length:I

    .line 266
    iput-object p2, p0, Lcom/ibm/icu/impl/TextTrieMap$LongestMatchHandler;->matches:Ljava/util/Iterator;

    .line 268
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
