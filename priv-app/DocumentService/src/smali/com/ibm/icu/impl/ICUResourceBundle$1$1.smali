.class Lcom/ibm/icu/impl/ICUResourceBundle$1$1;
.super Ljava/lang/Object;
.source "ICUResourceBundle.java"

# interfaces
.implements Lcom/ibm/icu/impl/URLHandler$URLVisitor;


# instance fields
.field private final this$0:Lcom/ibm/icu/impl/ICUResourceBundle$1;

.field private final val$lst:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/ICUResourceBundle$1;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1$1;->this$0:Lcom/ibm/icu/impl/ICUResourceBundle$1;

    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1$1;->val$lst:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public visit(Ljava/lang/String;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 547
    const-string/jumbo v0, ".res"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "res_index.res"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle$1$1;->val$lst:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    :cond_0
    return-void
.end method
