.class public final Lcom/ibm/icu/impl/UCharArrayIterator;
.super Lcom/ibm/icu/text/UCharacterIterator;
.source "UCharArrayIterator.java"


# instance fields
.field private final limit:I

.field private pos:I

.field private final start:I

.field private final text:[C


# direct methods
.method public constructor <init>([CII)V
    .locals 3
    .param p1, "text"    # [C
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/ibm/icu/text/UCharacterIterator;-><init>()V

    .line 25
    if-ltz p2, :cond_0

    array-length v0, p1

    if-gt p3, v0, :cond_0

    if-le p2, p3, :cond_1

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "start: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " or limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " out of range [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iput-object p1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->text:[C

    .line 31
    iput p2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    .line 32
    iput p3, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    .line 34
    iput p2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    .line 35
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79
    :try_start_0
    invoke-super {p0}, Lcom/ibm/icu/text/UCharacterIterator;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 81
    :goto_0
    return-object v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public current()I
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->text:[C

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    aget-char v0, v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getIndex()I
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLength()I
    .locals 2

    .prologue
    .line 42
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getText([CI)I
    .locals 3
    .param p1, "fillIn"    # [C
    .param p2, "offset"    # I

    .prologue
    .line 67
    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    iget v2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    sub-int v0, v1, v2

    .line 68
    .local v0, "len":I
    iget-object v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->text:[C

    iget v2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    return v0
.end method

.method public next()I
    .locals 3

    .prologue
    .line 50
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->text:[C

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    aget-char v0, v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public previous()I
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->text:[C

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    aget-char v0, v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setIndex(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 58
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    iget v1, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " out of range [0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->limit:I

    iget v3, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    iget v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->start:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/ibm/icu/impl/UCharArrayIterator;->pos:I

    .line 64
    return-void
.end method
