.class Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;
.super Ljava/lang/Object;
.source "ZoneStringFormat.java"

# interfaces
.implements Lcom/ibm/icu/impl/TextTrieMap$ResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ZoneStringFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ZoneStringSearchResultHandler"
.end annotation


# instance fields
.field private resultList:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/impl/ZoneStringFormat$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/impl/ZoneStringFormat$1;

    .prologue
    .line 942
    invoke-direct {p0}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;-><init>()V

    return-void
.end method


# virtual methods
.method getMatchedZoneStrings()Ljava/util/List;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 975
    :cond_0
    const/4 v0, 0x0

    .line 977
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public handlePrefixMatch(ILjava/util/Iterator;)Z
    .locals 5
    .param p1, "matchLength"    # I
    .param p2, "values"    # Ljava/util/Iterator;

    .prologue
    .line 947
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 948
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    .line 950
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 951
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    .line 952
    .local v2, "zsitem":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    if-nez v2, :cond_2

    .line 970
    .end local v2    # "zsitem":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 955
    .restart local v2    # "zsitem":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_2
    const/4 v0, 0x0

    .line 956
    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 957
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    .line 958
    .local v1, "tmp":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    invoke-static {v2}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->access$600(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;)I

    move-result v3

    invoke-static {v1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->access$600(Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;)I

    move-result v4

    if-ne v3, v4, :cond_4

    .line 959
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le p1, v3, :cond_3

    .line 960
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 965
    .end local v1    # "tmp":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 967
    iget-object v3, p0, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringSearchResultHandler;->resultList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 956
    .restart local v1    # "tmp":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
