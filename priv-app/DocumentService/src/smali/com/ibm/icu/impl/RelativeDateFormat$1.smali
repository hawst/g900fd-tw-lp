.class Lcom/ibm/icu/impl/RelativeDateFormat$1;
.super Ljava/lang/Object;
.source "RelativeDateFormat.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final this$0:Lcom/ibm/icu/impl/RelativeDateFormat;


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/RelativeDateFormat;)V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/RelativeDateFormat$1;->this$0:Lcom/ibm/icu/impl/RelativeDateFormat;

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4
    .param p1, "o1"    # Ljava/lang/Object;
    .param p2, "o2"    # Ljava/lang/Object;

    .prologue
    .line 163
    move-object v0, p1

    check-cast v0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    .local v0, "r1":Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    move-object v1, p2

    .line 164
    check-cast v1, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;

    .line 166
    .local v1, "r2":Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;
    iget v2, v0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    iget v3, v1, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    if-ne v2, v3, :cond_0

    .line 167
    const/4 v2, 0x0

    .line 171
    :goto_0
    return v2

    .line 168
    :cond_0
    iget v2, v0, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    iget v3, v1, Lcom/ibm/icu/impl/RelativeDateFormat$URelativeString;->offset:I

    if-ge v2, v3, :cond_1

    .line 169
    const/4 v2, -0x1

    goto :goto_0

    .line 171
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
