.class Lcom/ibm/icu/impl/ICUData$2;
.super Ljava/lang/Object;
.source "ICUData.java"

# interfaces
.implements Ljava/security/PrivilegedAction;


# instance fields
.field private final val$resourceName:Ljava/lang/String;

.field private final val$root:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/ICUData$2;->val$root:Ljava/lang/Class;

    iput-object p2, p0, Lcom/ibm/icu/impl/ICUData$2;->val$resourceName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUData$2;->val$root:Ljava/lang/Class;

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUData$2;->val$resourceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
