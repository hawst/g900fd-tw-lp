.class public final Lcom/ibm/icu/impl/ZoneMeta;
.super Ljava/lang/Object;
.source "ZoneMeta.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ZoneMeta$1;,
        Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;,
        Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    }
.end annotation


# static fields
.field private static final ASSERT:Z = false

.field private static final DEF_FALLBACK_FORMAT:Ljava/lang/String; = "{1} ({0})"

.field private static final DEF_REGION_FORMAT:Ljava/lang/String; = "{0}"

.field private static final EMPTY:[Ljava/lang/String;

.field public static final FALLBACK_FORMAT:Ljava/lang/String; = "fallbackFormat"

.field public static final FORWARD_SLASH:Ljava/lang/String; = "/"

.field public static final GMT:Ljava/lang/String; = "gmtFormat"

.field public static final HOUR:Ljava/lang/String; = "hourFormat"

.field private static META_TO_OLSON_REF:Ljava/lang/ref/SoftReference; = null

.field private static OLSON_TO_META_REF:Ljava/lang/ref/SoftReference; = null

.field static OLSON_ZONE_COUNT:I = 0x0

.field static OLSON_ZONE_START:I = 0x0

.field public static final REGION_FORMAT:Ljava/lang/String; = "regionFormat"

.field public static final ZONE_STRINGS:Ljava/lang/String; = "zoneStrings"

.field private static canonicalMap:Ljava/util/Map; = null

.field static class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class; = null

.field private static final kCUSTOM_TZ_PREFIX:Ljava/lang/String; = "GMT"

.field private static final kGMT_ID:Ljava/lang/String; = "GMT"

.field private static final kMAX_CUSTOM_HOUR:I = 0x17

.field private static final kMAX_CUSTOM_MIN:I = 0x3b

.field private static final kMAX_CUSTOM_SEC:I = 0x3b

.field private static final kNAMES:Ljava/lang/String; = "Names"

.field private static final kREGIONS:Ljava/lang/String; = "Regions"

.field private static final kZONES:Ljava/lang/String; = "Zones"

.field private static multiZoneTerritories:Ljava/util/Set;

.field private static zoneCache:Lcom/ibm/icu/impl/ICUCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 302
    sput-object v0, Lcom/ibm/icu/impl/ZoneMeta;->canonicalMap:Ljava/util/Map;

    .line 303
    sput-object v0, Lcom/ibm/icu/impl/ZoneMeta;->multiZoneTerritories:Ljava/util/Set;

    .line 445
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;

    .line 533
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/ZoneMeta;->zoneCache:Lcom/ibm/icu/impl/ICUCache;

    .line 541
    const/4 v0, -0x1

    sput v0, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I

    .line 542
    sput v1, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 837
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 293
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static declared-synchronized countEquivalentIDs(Ljava/lang/String;)I
    .locals 6
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 142
    const-class v5, Lcom/ibm/icu/impl/ZoneMeta;

    monitor-enter v5

    :try_start_0
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->openOlsonResource(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 143
    .local v1, "res":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v2

    .line 144
    .local v2, "size":I
    const/4 v4, 0x4

    if-eq v2, v4, :cond_0

    const/4 v4, 0x6

    if-ne v2, v4, :cond_1

    .line 145
    :cond_0
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v1, v4}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 147
    .local v0, "r":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v3

    .line 148
    .local v3, "v":[I
    array-length v4, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v0    # "r":Lcom/ibm/icu/util/UResourceBundle;
    .end local v3    # "v":[I
    :goto_0
    monitor-exit v5

    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 142
    .end local v1    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "size":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method private static createOlsonToMetaMap()Ljava/util/Map;
    .locals 20

    .prologue
    .line 865
    const/4 v12, 0x0

    .line 866
    .local v12, "olsonToMeta":Ljava/util/HashMap;
    const/4 v6, 0x0

    .line 868
    .local v6, "metazoneMappingsBundle":Lcom/ibm/icu/util/UResourceBundle;
    :try_start_0
    const-string/jumbo v17, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v18, "metazoneInfo"

    invoke-static/range {v17 .. v18}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 869
    .local v2, "bundle":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v17, "metazoneMappings"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v6

    .line 873
    .end local v2    # "bundle":Lcom/ibm/icu/util/UResourceBundle;
    :goto_0
    if-eqz v6, :cond_5

    .line 874
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getAvailableIDs()[Ljava/lang/String;

    move-result-object v14

    .line 875
    .local v14, "tzids":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v0, v14

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v4, v0, :cond_5

    .line 877
    aget-object v17, v14, v4

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/util/TimeZone;->getCanonicalID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 878
    .local v3, "canonicalID":Ljava/lang/String;
    if-eqz v3, :cond_0

    aget-object v17, v14, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 875
    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 881
    :cond_1
    aget-object v17, v14, v4

    const/16 v18, 0x2f

    const/16 v19, 0x3a

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v15

    .line 883
    .local v15, "tzkey":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v6, v15}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v16

    .line 884
    .local v16, "zoneBundle":Lcom/ibm/icu/util/UResourceBundle;
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_1

    .line 885
    .local v8, "mzMappings":Ljava/util/LinkedList;
    const/4 v5, 0x0

    .line 887
    .local v5, "idx":I
    :goto_3
    :try_start_2
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v18, "mz"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v7

    .line 888
    .local v7, "mz":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v7}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v10

    .line 889
    .local v10, "mzstr":[Ljava/lang/String;
    if-eqz v10, :cond_2

    array-length v0, v10

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 885
    .end local v7    # "mz":Lcom/ibm/icu/util/UResourceBundle;
    .end local v10    # "mzstr":[Ljava/lang/String;
    :cond_2
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 892
    .restart local v7    # "mz":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v10    # "mzstr":[Ljava/lang/String;
    :cond_3
    new-instance v9, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;

    invoke-direct {v9}, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;-><init>()V

    .line 893
    .local v9, "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    const/16 v17, 0x0

    aget-object v17, v10, v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v9, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    .line 894
    const/16 v17, 0x1

    aget-object v17, v10, v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/impl/ZoneMeta;->parseDate(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v9, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->from:J

    .line 895
    const/16 v17, 0x2

    aget-object v17, v10, v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/impl/ZoneMeta;->parseDate(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v9, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->to:J

    .line 898
    invoke-virtual {v8, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 899
    .end local v7    # "mz":Lcom/ibm/icu/util/UResourceBundle;
    .end local v9    # "mzmap":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    .end local v10    # "mzstr":[Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 906
    .local v11, "nomz":Ljava/util/MissingResourceException;
    :try_start_3
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v17

    if-eqz v17, :cond_0

    .line 908
    if-nez v12, :cond_4

    .line 909
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .end local v12    # "olsonToMeta":Ljava/util/HashMap;
    .local v13, "olsonToMeta":Ljava/util/HashMap;
    move-object v12, v13

    .line 911
    .end local v13    # "olsonToMeta":Ljava/util/HashMap;
    .restart local v12    # "olsonToMeta":Ljava/util/HashMap;
    :cond_4
    aget-object v17, v14, v4

    move-object/from16 v0, v17

    invoke-virtual {v12, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/MissingResourceException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    .line 913
    .end local v5    # "idx":I
    .end local v8    # "mzMappings":Ljava/util/LinkedList;
    .end local v11    # "nomz":Ljava/util/MissingResourceException;
    .end local v16    # "zoneBundle":Lcom/ibm/icu/util/UResourceBundle;
    :catch_1
    move-exception v17

    goto/16 :goto_2

    .line 918
    .end local v3    # "canonicalID":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v14    # "tzids":[Ljava/lang/String;
    .end local v15    # "tzkey":Ljava/lang/String;
    :cond_5
    return-object v12

    .line 902
    .restart local v3    # "canonicalID":Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v5    # "idx":I
    .restart local v8    # "mzMappings":Ljava/util/LinkedList;
    .restart local v14    # "tzids":[Ljava/lang/String;
    .restart local v15    # "tzkey":Ljava/lang/String;
    .restart local v16    # "zoneBundle":Lcom/ibm/icu/util/UResourceBundle;
    :catch_2
    move-exception v17

    goto :goto_4

    .line 870
    .end local v3    # "canonicalID":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "idx":I
    .end local v8    # "mzMappings":Ljava/util/LinkedList;
    .end local v14    # "tzids":[Ljava/lang/String;
    .end local v15    # "tzkey":Ljava/lang/String;
    .end local v16    # "zoneBundle":Lcom/ibm/icu/util/UResourceBundle;
    :catch_3
    move-exception v17

    goto/16 :goto_0
.end method

.method private static findInStringArray(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)I
    .locals 8
    .param p0, "array"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 499
    const/4 v4, 0x0

    .line 500
    .local v4, "start":I
    invoke-virtual {p0}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v1

    .line 502
    .local v1, "limit":I
    const/4 v5, 0x0

    .line 503
    .local v5, "u":Ljava/lang/String;
    const v0, 0x7fffffff

    .line 504
    .local v0, "lastMid":I
    const/4 v7, 0x1

    if-ge v1, v7, :cond_1

    move v2, v6

    .line 526
    :goto_0
    return v2

    .line 520
    .local v2, "mid":I
    .local v3, "r":I
    :cond_0
    if-gez v3, :cond_4

    .line 521
    move v1, v2

    .line 508
    .end local v2    # "mid":I
    .end local v3    # "r":I
    :cond_1
    :goto_1
    add-int v7, v4, v1

    div-int/lit8 v2, v7, 0x2

    .line 509
    .restart local v2    # "mid":I
    if-ne v0, v2, :cond_3

    :cond_2
    move v2, v6

    .line 526
    goto :goto_0

    .line 512
    :cond_3
    move v0, v2

    .line 513
    invoke-virtual {p0, v2}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 514
    if-eqz v5, :cond_2

    .line 517
    invoke-virtual {p1, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    .line 518
    .restart local v3    # "r":I
    if-nez v3, :cond_0

    goto :goto_0

    .line 523
    :cond_4
    move v4, v2

    goto :goto_1
.end method

.method static formatCustomID(IIIZ)Ljava/lang/String;
    .locals 4
    .param p0, "hour"    # I
    .param p1, "min"    # I
    .param p2, "sec"    # I
    .param p3, "negative"    # Z

    .prologue
    const/16 v3, 0x30

    const/16 v2, 0xa

    .line 800
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "GMT"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 801
    .local v0, "zid":Ljava/lang/StringBuffer;
    if-nez p0, :cond_0

    if-eqz p1, :cond_4

    .line 802
    :cond_0
    if-eqz p3, :cond_5

    .line 803
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 808
    :goto_0
    if-ge p0, v2, :cond_1

    .line 809
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 811
    :cond_1
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 812
    if-ge p1, v2, :cond_2

    .line 813
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 815
    :cond_2
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 817
    if-eqz p2, :cond_4

    .line 819
    if-ge p2, v2, :cond_3

    .line 820
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 822
    :cond_3
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 825
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 805
    :cond_5
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static declared-synchronized getAvailableIDs()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 84
    const-class v3, Lcom/ibm/icu/impl/ZoneMeta;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getOlsonMeta()Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    sget-object v2, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    .local v1, "top":Lcom/ibm/icu/util/UResourceBundle;
    :goto_0
    monitor-exit v3

    return-object v2

    .line 88
    .end local v1    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    :try_start_1
    const-string/jumbo v2, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v4, "zoneinfo"

    sget-object v5, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v2, v4, v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 89
    .restart local v1    # "top":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v2, "Names"

    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 90
    .local v0, "names":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 91
    .end local v0    # "names":Lcom/ibm/icu/util/UResourceBundle;
    :catch_0
    move-exception v2

    .line 94
    :try_start_2
    sget-object v2, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized getAvailableIDs(I)[Ljava/lang/String;
    .locals 7
    .param p0, "offset"    # I

    .prologue
    .line 97
    const-class v6, Lcom/ibm/icu/impl/ZoneMeta;

    monitor-enter v6

    :try_start_0
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 98
    .local v3, "vector":Ljava/util/Vector;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v5, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_COUNT:I

    if-ge v0, v5, :cond_1

    .line 100
    invoke-static {v0}, Lcom/ibm/icu/impl/ZoneMeta;->getID(I)Ljava/lang/String;

    move-result-object v2

    .local v2, "unistr":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 102
    invoke-static {v2}, Lcom/ibm/icu/util/TimeZone;->getTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;

    move-result-object v4

    .line 105
    .local v4, "z":Lcom/ibm/icu/util/TimeZone;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZone;->getRawOffset()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 107
    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 98
    .end local v4    # "z":Lcom/ibm/icu/util/TimeZone;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    .end local v2    # "unistr":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3}, Ljava/util/Vector;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 112
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v1, v5, [Ljava/lang/String;

    .line 113
    .local v1, "strings":[Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    check-cast v5, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    .end local v1    # "strings":[Ljava/lang/String;
    :goto_1
    monitor-exit v6

    return-object v5

    :cond_2
    :try_start_1
    sget-object v5, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 97
    .end local v0    # "i":I
    .end local v3    # "vector":Ljava/util/Vector;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static declared-synchronized getAvailableIDs(Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p0, "country"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v8, Lcom/ibm/icu/impl/ZoneMeta;

    monitor-enter v8

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getOlsonMeta()Z

    move-result v7

    if-nez v7, :cond_1

    .line 62
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_0
    :goto_0
    monitor-exit v8

    return-object v3

    .line 65
    :cond_1
    :try_start_1
    const-string/jumbo v7, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v9, "zoneinfo"

    sget-object v10, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v7, v9, v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 66
    .local v5, "top":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v7, "Regions"

    invoke-virtual {v5, v7}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 67
    .local v2, "regions":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v7, "Names"

    invoke-virtual {v5, v7}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 68
    .local v1, "names":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v2, p0}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 69
    .local v4, "temp":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v6

    .line 71
    .local v6, "vector":[I
    array-length v7, v6

    new-array v3, v7, [Ljava/lang/String;

    .line 72
    .local v3, "ret":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v7, v6

    if-ge v0, v7, :cond_0

    .line 75
    aget v7, v6, v0

    invoke-virtual {v1, v7}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v0
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78
    .end local v0    # "i":I
    .end local v1    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "regions":Lcom/ibm/icu/util/UResourceBundle;
    .end local v3    # "ret":[Ljava/lang/String;
    .end local v4    # "temp":Lcom/ibm/icu/util/UResourceBundle;
    .end local v5    # "top":Lcom/ibm/icu/util/UResourceBundle;
    .end local v6    # "vector":[I
    :catch_0
    move-exception v7

    .line 81
    :try_start_2
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->EMPTY:[Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method

.method public static getCanonicalCountry(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tzid"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalInfo(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "info":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 324
    const/4 v1, 0x1

    aget-object v1, v0, v1

    .line 326
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getCanonicalInfo(Ljava/lang/String;)[Ljava/lang/String;
    .locals 30
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 199
    if-eqz p0, :cond_0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v26

    if-nez v26, :cond_1

    .line 200
    :cond_0
    const/16 v26, 0x0

    .line 299
    :goto_0
    return-object v26

    .line 202
    :cond_1
    sget-object v26, Lcom/ibm/icu/impl/ZoneMeta;->canonicalMap:Ljava/util/Map;

    if-nez v26, :cond_a

    .line 203
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 204
    .local v11, "m":Ljava/util/Map;
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 206
    .local v17, "s":Ljava/util/Set;
    :try_start_0
    const-string/jumbo v26, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v27, "supplementalData"

    sget-object v28, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static/range {v26 .. v28}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v18

    .line 208
    .local v18, "supplementalDataBundle":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v26, "zoneFormatting"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v24

    .line 209
    .local v24, "zoneFormatting":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/util/UResourceBundle;->getIterator()Lcom/ibm/icu/util/UResourceBundleIterator;

    move-result-object v9

    .line 211
    .local v9, "it":Lcom/ibm/icu/util/UResourceBundleIterator;
    :cond_2
    :goto_1
    invoke-virtual {v9}, Lcom/ibm/icu/util/UResourceBundleIterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_4

    .line 212
    invoke-virtual {v9}, Lcom/ibm/icu/util/UResourceBundleIterator;->next()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v19

    .line 213
    .local v19, "temp":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/util/UResourceBundle;->getType()I

    move-result v15

    .line 215
    .local v15, "resourceType":I
    sparse-switch v15, :sswitch_data_0

    goto :goto_1

    .line 217
    :sswitch_0
    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v26, 0x0

    const-string/jumbo v27, ""

    aput-object v27, v16, v26

    const/16 v26, 0x1

    const-string/jumbo v27, ""

    aput-object v27, v16, v26

    .line 218
    .local v16, "result":[Ljava/lang/String;
    move-object/from16 v25, v19

    .line 219
    .local v25, "zoneInfo":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual/range {v25 .. v25}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x3a

    const/16 v28, 0x2f

    invoke-virtual/range {v26 .. v28}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "canonicalID":Ljava/lang/String;
    const-string/jumbo v26, "territory"

    invoke-virtual/range {v25 .. v26}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v20

    .line 221
    .local v20, "territory":Ljava/lang/String;
    const/16 v26, 0x0

    aput-object v5, v16, v26

    .line 222
    const-string/jumbo v26, "001"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 223
    const/16 v26, 0x1

    const/16 v27, 0x0

    aput-object v27, v16, v26

    .line 228
    :goto_2
    move-object/from16 v0, v16

    invoke-interface {v11, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :try_start_1
    const-string/jumbo v26, "aliases"

    invoke-virtual/range {v25 .. v26}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .line 231
    .local v3, "aliasBundle":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "aliases":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    array-length v0, v4

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v7, v0, :cond_2

    .line 233
    aget-object v26, v4, v7

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-interface {v11, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_2

    .line 232
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 226
    .end local v3    # "aliasBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v4    # "aliases":[Ljava/lang/String;
    .end local v7    # "i":I
    :cond_3
    const/16 v26, 0x1

    :try_start_2
    aput-object v20, v16, v26
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 247
    .end local v5    # "canonicalID":Ljava/lang/String;
    .end local v9    # "it":Lcom/ibm/icu/util/UResourceBundleIterator;
    .end local v15    # "resourceType":I
    .end local v16    # "result":[Ljava/lang/String;
    .end local v18    # "supplementalDataBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v19    # "temp":Lcom/ibm/icu/util/UResourceBundle;
    .end local v20    # "territory":Ljava/lang/String;
    .end local v24    # "zoneFormatting":Lcom/ibm/icu/util/UResourceBundle;
    .end local v25    # "zoneInfo":Lcom/ibm/icu/util/UResourceBundle;
    :catch_0
    move-exception v26

    .line 256
    :cond_4
    :try_start_3
    const-string/jumbo v26, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v27, "zoneinfo"

    sget-object v28, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static/range {v26 .. v28}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v23

    check-cast v23, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 258
    .local v23, "top":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v26, "Names"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v13

    .line 259
    .local v13, "names":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v13}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v8

    .line 260
    .local v8, "ids":[Ljava/lang/String;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    array-length v0, v8

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v7, v0, :cond_9

    .line 261
    aget-object v26, v8, v7

    move-object/from16 v0, v26

    invoke-interface {v11, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/util/MissingResourceException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v26

    if-eqz v26, :cond_5

    .line 260
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 240
    .end local v7    # "i":I
    .end local v8    # "ids":[Ljava/lang/String;
    .end local v13    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .end local v23    # "top":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v9    # "it":Lcom/ibm/icu/util/UResourceBundleIterator;
    .restart local v15    # "resourceType":I
    .restart local v18    # "supplementalDataBundle":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v19    # "temp":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v24    # "zoneFormatting":Lcom/ibm/icu/util/UResourceBundle;
    :sswitch_1
    :try_start_4
    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v21

    .line 241
    .local v21, "territoryList":[Ljava/lang/String;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_6
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v7, v0, :cond_2

    .line 242
    aget-object v26, v21, v7

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/util/MissingResourceException; {:try_start_4 .. :try_end_4} :catch_0

    .line 241
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 267
    .end local v9    # "it":Lcom/ibm/icu/util/UResourceBundleIterator;
    .end local v15    # "resourceType":I
    .end local v18    # "supplementalDataBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v19    # "temp":Lcom/ibm/icu/util/UResourceBundle;
    .end local v21    # "territoryList":[Ljava/lang/String;
    .end local v24    # "zoneFormatting":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v8    # "ids":[Ljava/lang/String;
    .restart local v13    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v23    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :cond_5
    const/16 v22, 0x0

    .line 268
    .local v22, "tmpinfo":[Ljava/lang/String;
    :try_start_5
    aget-object v26, v8, v7

    invoke-static/range {v26 .. v26}, Lcom/ibm/icu/util/TimeZone;->countEquivalentIDs(Ljava/lang/String;)I

    move-result v12

    .line 269
    .local v12, "nTzdataEquivalent":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_7
    if-ge v10, v12, :cond_8

    .line 270
    aget-object v26, v8, v7

    move-object/from16 v0, v26

    invoke-static {v0, v10}, Lcom/ibm/icu/util/TimeZone;->getEquivalentID(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, "alias":Ljava/lang/String;
    aget-object v26, v8, v7

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 269
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 274
    :cond_7
    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [Ljava/lang/String;

    move-object/from16 v0, v26

    check-cast v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    .line 275
    if-eqz v22, :cond_6

    .line 279
    .end local v2    # "alias":Ljava/lang/String;
    :cond_8
    if-nez v22, :cond_c

    .line 281
    aget-object v26, v8, v7

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ZoneMeta;->getZoneByName(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    .line 282
    .local v14, "res":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v14}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_b

    invoke-virtual {v14}, Lcom/ibm/icu/util/UResourceBundle;->getInt()I

    move-result v26

    aget-object v6, v8, v26

    .line 283
    .local v6, "derefID":Ljava/lang/String;
    :goto_8
    aget-object v26, v8, v7

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v6, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput-object v29, v27, v28

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-interface {v11, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/util/MissingResourceException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_5

    .line 289
    .end local v6    # "derefID":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "ids":[Ljava/lang/String;
    .end local v10    # "j":I
    .end local v12    # "nTzdataEquivalent":I
    .end local v13    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .end local v14    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .end local v22    # "tmpinfo":[Ljava/lang/String;
    .end local v23    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :catch_1
    move-exception v26

    .line 293
    :cond_9
    sget-object v26, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    if-nez v26, :cond_d

    const-string/jumbo v26, "com.ibm.icu.impl.ZoneMeta"

    invoke-static/range {v26 .. v26}, Lcom/ibm/icu/impl/ZoneMeta;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v26

    sput-object v26, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    :goto_9
    monitor-enter v26

    .line 294
    :try_start_6
    sput-object v11, Lcom/ibm/icu/impl/ZoneMeta;->canonicalMap:Ljava/util/Map;

    .line 295
    sput-object v17, Lcom/ibm/icu/impl/ZoneMeta;->multiZoneTerritories:Ljava/util/Set;

    .line 296
    monitor-exit v26
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 299
    .end local v11    # "m":Ljava/util/Map;
    .end local v17    # "s":Ljava/util/Set;
    :cond_a
    sget-object v26, Lcom/ibm/icu/impl/ZoneMeta;->canonicalMap:Ljava/util/Map;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [Ljava/lang/String;

    check-cast v26, [Ljava/lang/String;

    goto/16 :goto_0

    .line 282
    .restart local v7    # "i":I
    .restart local v8    # "ids":[Ljava/lang/String;
    .restart local v10    # "j":I
    .restart local v11    # "m":Ljava/util/Map;
    .restart local v12    # "nTzdataEquivalent":I
    .restart local v13    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v14    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v17    # "s":Ljava/util/Set;
    .restart local v22    # "tmpinfo":[Ljava/lang/String;
    .restart local v23    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :cond_b
    :try_start_7
    aget-object v6, v8, v7

    goto :goto_8

    .line 286
    .end local v14    # "res":Lcom/ibm/icu/util/UResourceBundle;
    :cond_c
    aget-object v26, v8, v7

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-interface {v11, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/util/MissingResourceException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_5

    .line 293
    .end local v7    # "i":I
    .end local v8    # "ids":[Ljava/lang/String;
    .end local v10    # "j":I
    .end local v12    # "nTzdataEquivalent":I
    .end local v13    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .end local v22    # "tmpinfo":[Ljava/lang/String;
    .end local v23    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :cond_d
    sget-object v26, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    goto :goto_9

    .line 296
    :catchall_0
    move-exception v27

    :try_start_8
    monitor-exit v26
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v27

    .line 235
    .restart local v5    # "canonicalID":Ljava/lang/String;
    .restart local v9    # "it":Lcom/ibm/icu/util/UResourceBundleIterator;
    .restart local v15    # "resourceType":I
    .restart local v16    # "result":[Ljava/lang/String;
    .restart local v18    # "supplementalDataBundle":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v19    # "temp":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v20    # "territory":Ljava/lang/String;
    .restart local v24    # "zoneFormatting":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v25    # "zoneInfo":Lcom/ibm/icu/util/UResourceBundle;
    :catch_2
    move-exception v26

    goto/16 :goto_1

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getCanonicalSystemID(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tzid"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalInfo(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "info":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 312
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 314
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getCustomID(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 633
    const/4 v3, 0x4

    new-array v0, v3, [I

    .line 634
    .local v0, "fields":[I
    invoke-static {p0, v0}, Lcom/ibm/icu/impl/ZoneMeta;->parseCustomID(Ljava/lang/String;[I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 635
    aget v3, v0, v1

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v5, v0, v5

    aget v6, v0, v2

    if-gez v6, :cond_0

    :goto_0
    invoke-static {v3, v4, v5, v1}, Lcom/ibm/icu/impl/ZoneMeta;->formatCustomID(IIIZ)Ljava/lang/String;

    move-result-object v1

    .line 637
    :goto_1
    return-object v1

    :cond_0
    move v1, v2

    .line 635
    goto :goto_0

    .line 637
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getCustomTimeZone(I)Lcom/ibm/icu/util/TimeZone;
    .locals 8
    .param p0, "offset"    # I

    .prologue
    .line 770
    const/4 v3, 0x0

    .line 771
    .local v3, "negative":Z
    move v5, p0

    .line 772
    .local v5, "tmp":I
    if-gez p0, :cond_0

    .line 773
    const/4 v3, 0x1

    .line 774
    neg-int v5, p0

    .line 779
    :cond_0
    rem-int/lit16 v1, v5, 0x3e8

    .line 783
    .local v1, "millis":I
    div-int/lit16 v5, v5, 0x3e8

    .line 784
    rem-int/lit8 v4, v5, 0x3c

    .line 785
    .local v4, "sec":I
    div-int/lit8 v5, v5, 0x3c

    .line 786
    rem-int/lit8 v2, v5, 0x3c

    .line 787
    .local v2, "min":I
    div-int/lit8 v0, v5, 0x3c

    .line 790
    .local v0, "hour":I
    invoke-static {v0, v2, v4, v3}, Lcom/ibm/icu/impl/ZoneMeta;->formatCustomID(IIIZ)Ljava/lang/String;

    move-result-object v6

    .line 792
    .local v6, "zid":Ljava/lang/String;
    new-instance v7, Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-direct {v7, p0, v6}, Lcom/ibm/icu/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    return-object v7
.end method

.method public static getCustomTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;
    .locals 11
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 616
    const/4 v3, 0x4

    new-array v0, v3, [I

    .line 617
    .local v0, "fields":[I
    invoke-static {p0, v0}, Lcom/ibm/icu/impl/ZoneMeta;->parseCustomID(Ljava/lang/String;[I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 618
    aget v6, v0, v4

    aget v7, v0, v9

    aget v8, v0, v10

    aget v3, v0, v5

    if-gez v3, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v6, v7, v8, v3}, Lcom/ibm/icu/impl/ZoneMeta;->formatCustomID(IIIZ)Ljava/lang/String;

    move-result-object v2

    .line 619
    .local v2, "zid":Ljava/lang/String;
    aget v3, v0, v5

    aget v4, v0, v4

    mul-int/lit8 v4, v4, 0x3c

    aget v5, v0, v9

    add-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x3c

    aget v5, v0, v10

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    mul-int/lit16 v1, v3, 0x3e8

    .line 620
    .local v1, "offset":I
    new-instance v3, Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-direct {v3, v1, v2}, Lcom/ibm/icu/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    .line 622
    .end local v1    # "offset":I
    .end local v2    # "zid":Ljava/lang/String;
    :goto_1
    return-object v3

    :cond_0
    move v3, v5

    .line 618
    goto :goto_0

    .line 622
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized getEquivalentID(Ljava/lang/String;I)Ljava/lang/String;
    .locals 13
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "index"    # I

    .prologue
    .line 172
    const-class v10, Lcom/ibm/icu/impl/ZoneMeta;

    monitor-enter v10

    :try_start_0
    const-string/jumbo v4, ""

    .line 173
    .local v4, "result":Ljava/lang/String;
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->openOlsonResource(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .line 174
    .local v3, "res":Lcom/ibm/icu/util/UResourceBundle;
    if-eqz v3, :cond_2

    .line 175
    const/4 v8, -0x1

    .line 176
    .local v8, "zone":I
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v5

    .line 177
    .local v5, "size":I
    const/4 v9, 0x4

    if-eq v5, v9, :cond_0

    const/4 v9, 0x6

    if-ne v5, v9, :cond_1

    .line 178
    :cond_0
    add-int/lit8 v9, v5, -0x1

    invoke-virtual {v3, v9}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 179
    .local v2, "r":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v7

    .line 180
    .local v7, "v":[I
    if-ltz p1, :cond_1

    array-length v9, v7

    if-ge p1, v9, :cond_1

    .line 181
    aget v8, v7, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    .end local v2    # "r":Lcom/ibm/icu/util/UResourceBundle;
    .end local v7    # "v":[I
    :cond_1
    if-ltz v8, :cond_2

    .line 186
    :try_start_1
    const-string/jumbo v9, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v11, "zoneinfo"

    sget-object v12, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v9, v11, v12}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    .line 188
    .local v6, "top":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v9, "Names"

    invoke-virtual {v6, v9}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 189
    .local v0, "ares":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0, v8}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 195
    .end local v0    # "ares":Lcom/ibm/icu/util/UResourceBundle;
    .end local v5    # "size":I
    .end local v6    # "top":Lcom/ibm/icu/util/UResourceBundle;
    .end local v8    # "zone":I
    :cond_2
    :goto_0
    monitor-exit v10

    return-object v4

    .line 190
    .restart local v5    # "size":I
    .restart local v8    # "zone":I
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/util/MissingResourceException;
    :try_start_2
    const-string/jumbo v4, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 172
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .end local v3    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .end local v4    # "result":Ljava/lang/String;
    .end local v5    # "size":I
    .end local v8    # "zone":I
    :catchall_0
    move-exception v9

    monitor-exit v10

    throw v9
.end method

.method public static getGMT()Lcom/ibm/icu/util/TimeZone;
    .locals 3

    .prologue
    .line 598
    new-instance v0, Lcom/ibm/icu/util/SimpleTimeZone;

    const/4 v1, 0x0

    const-string/jumbo v2, "GMT"

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    .line 599
    .local v0, "z":Lcom/ibm/icu/util/TimeZone;
    const-string/jumbo v1, "GMT"

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/TimeZone;->setID(Ljava/lang/String;)V

    .line 600
    return-object v0
.end method

.method private static getID(I)Ljava/lang/String;
    .locals 5
    .param p0, "i"    # I

    .prologue
    .line 119
    :try_start_0
    const-string/jumbo v2, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v3, "zoneinfo"

    sget-object v4, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 120
    .local v1, "top":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v2, "Names"

    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 121
    .local v0, "names":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0, p0}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 125
    .end local v0    # "names":Lcom/ibm/icu/util/UResourceBundle;
    .end local v1    # "top":Lcom/ibm/icu/util/UResourceBundle;
    :goto_0
    return-object v2

    .line 122
    :catch_0
    move-exception v2

    .line 125
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLocationFormat(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 13
    .param p0, "tzid"    # Ljava/lang/String;
    .param p1, "city"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 348
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalInfo(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 349
    .local v3, "info":[Ljava/lang/String;
    if-nez v3, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-object v8

    .line 353
    :cond_1
    aget-object v1, v3, v11

    .line 354
    .local v1, "country_code":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 359
    .local v0, "country":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 361
    :try_start_0
    const-string/jumbo v8, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v8, p2}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 370
    .local v5, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v6

    .line 371
    .local v6, "rbloc":Lcom/ibm/icu/util/ULocale;
    sget-object v8, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v6, v8}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v6}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 372
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v9, "xx_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p2}, Lcom/ibm/icu/util/ULocale;->getDisplayCountry(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 378
    .end local v5    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v6    # "rbloc":Lcom/ibm/icu/util/ULocale;
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_4

    .line 379
    :cond_3
    move-object v0, v1

    .line 385
    :cond_4
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getSingleCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 386
    const-string/jumbo v8, "regionFormat"

    invoke-static {p2, v8}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 387
    .local v7, "regPat":Ljava/lang/String;
    if-nez v7, :cond_5

    .line 388
    const-string/jumbo v7, "{0}"

    .line 390
    :cond_5
    new-instance v4, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v4, v7}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 391
    .local v4, "mf":Lcom/ibm/icu/text/MessageFormat;
    new-array v8, v11, [Ljava/lang/Object;

    aput-object v0, v8, v12

    invoke-virtual {v4, v8}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 394
    .end local v4    # "mf":Lcom/ibm/icu/text/MessageFormat;
    .end local v7    # "regPat":Ljava/lang/String;
    :cond_6
    if-nez p1, :cond_7

    .line 395
    const/16 v8, 0x2f

    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x5f

    const/16 v10, 0x20

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 398
    :cond_7
    const-string/jumbo v8, "fallbackFormat"

    invoke-static {p2, v8}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 399
    .local v2, "flbPat":Ljava/lang/String;
    if-nez v2, :cond_8

    .line 400
    const-string/jumbo v2, "{1} ({0})"

    .line 402
    :cond_8
    new-instance v4, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v4, v2}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 404
    .restart local v4    # "mf":Lcom/ibm/icu/text/MessageFormat;
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v12

    aput-object v0, v8, v11

    invoke-virtual {v4, v8}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 375
    .end local v2    # "flbPat":Ljava/lang/String;
    .end local v4    # "mf":Lcom/ibm/icu/text/MessageFormat;
    :catch_0
    move-exception v8

    goto :goto_1
.end method

.method private static getMetaToOlsonMap()Ljava/util/Map;
    .locals 17

    .prologue
    .line 948
    const/4 v3, 0x0

    .line 949
    .local v3, "metaToOlson":Ljava/util/HashMap;
    sget-object v14, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    if-nez v14, :cond_2

    const-string/jumbo v14, "com.ibm.icu.impl.ZoneMeta"

    invoke-static {v14}, Lcom/ibm/icu/impl/ZoneMeta;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    sput-object v14, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    move-object v15, v14

    :goto_0
    monitor-enter v15

    .line 950
    :try_start_0
    sget-object v14, Lcom/ibm/icu/impl/ZoneMeta;->META_TO_OLSON_REF:Ljava/lang/ref/SoftReference;

    if-eqz v14, :cond_5

    .line 951
    sget-object v14, Lcom/ibm/icu/impl/ZoneMeta;->META_TO_OLSON_REF:Ljava/lang/ref/SoftReference;

    invoke-virtual {v14}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Ljava/util/HashMap;

    move-object v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v3

    .line 953
    .end local v3    # "metaToOlson":Ljava/util/HashMap;
    .local v4, "metaToOlson":Ljava/util/HashMap;
    :goto_1
    if-nez v4, :cond_4

    .line 954
    :try_start_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 955
    .end local v4    # "metaToOlson":Ljava/util/HashMap;
    .restart local v3    # "metaToOlson":Ljava/util/HashMap;
    const/4 v5, 0x0

    .line 957
    .local v5, "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    :try_start_2
    const-string/jumbo v14, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v16, "supplementalData"

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v10

    .line 959
    .local v10, "supplementalBundle":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v14, "mapTimezones"

    invoke-virtual {v10, v14}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 960
    .local v1, "mapTimezonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v14, "metazones"

    invoke-virtual {v1, v14}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 964
    .end local v1    # "mapTimezonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v10    # "supplementalBundle":Lcom/ibm/icu/util/UResourceBundle;
    :goto_2
    if-eqz v5, :cond_3

    .line 965
    :try_start_3
    invoke-virtual {v5}, Lcom/ibm/icu/util/UResourceBundle;->getKeys()Ljava/util/Enumeration;

    move-result-object v6

    .line 966
    .local v6, "mzenum":Ljava/util/Enumeration;
    :cond_0
    :goto_3
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 967
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 968
    .local v8, "mzkey":Ljava/lang/String;
    const-string/jumbo v14, "meta:"

    invoke-virtual {v8, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v14

    if-eqz v14, :cond_0

    .line 971
    const/4 v13, 0x0

    .line 973
    .local v13, "tzid":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v5, v8}, Lcom/ibm/icu/util/UResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/util/MissingResourceException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v13

    .line 977
    :goto_4
    if-eqz v13, :cond_0

    .line 978
    const/16 v14, 0x5f

    :try_start_5
    invoke-virtual {v8, v14}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    .line 979
    .local v12, "territoryIdx":I
    if-lez v12, :cond_0

    .line 980
    const/4 v14, 0x5

    invoke-virtual {v8, v14, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 981
    .local v7, "mzid":Ljava/lang/String;
    add-int/lit8 v14, v12, 0x1

    invoke-virtual {v8, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 982
    .local v11, "territory":Ljava/lang/String;
    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 983
    .local v2, "mappings":Ljava/util/List;
    if-nez v2, :cond_1

    .line 984
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "mappings":Ljava/util/List;
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 985
    .restart local v2    # "mappings":Ljava/util/List;
    invoke-virtual {v3, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 987
    :cond_1
    new-instance v9, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;

    const/4 v14, 0x0

    invoke-direct {v9, v14}, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;-><init>(Lcom/ibm/icu/impl/ZoneMeta$1;)V

    .line 988
    .local v9, "olsonmap":Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;
    iput-object v13, v9, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->id:Ljava/lang/String;

    .line 989
    iput-object v11, v9, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->territory:Ljava/lang/String;

    .line 990
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 997
    .end local v2    # "mappings":Ljava/util/List;
    .end local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v6    # "mzenum":Ljava/util/Enumeration;
    .end local v7    # "mzid":Ljava/lang/String;
    .end local v8    # "mzkey":Ljava/lang/String;
    .end local v9    # "olsonmap":Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;
    .end local v11    # "territory":Ljava/lang/String;
    .end local v12    # "territoryIdx":I
    .end local v13    # "tzid":Ljava/lang/String;
    :catchall_0
    move-exception v14

    :goto_5
    monitor-exit v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v14

    .line 949
    :cond_2
    sget-object v14, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    move-object v15, v14

    goto/16 :goto_0

    .line 995
    .restart local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    :cond_3
    :try_start_6
    new-instance v14, Ljava/lang/ref/SoftReference;

    invoke-direct {v14, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v14, Lcom/ibm/icu/impl/ZoneMeta;->META_TO_OLSON_REF:Ljava/lang/ref/SoftReference;

    .line 997
    .end local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    :goto_6
    monitor-exit v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 998
    return-object v3

    .line 974
    .restart local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v6    # "mzenum":Ljava/util/Enumeration;
    .restart local v8    # "mzkey":Ljava/lang/String;
    .restart local v13    # "tzid":Ljava/lang/String;
    :catch_0
    move-exception v14

    goto :goto_4

    .line 997
    .end local v3    # "metaToOlson":Ljava/util/HashMap;
    .end local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    .end local v6    # "mzenum":Ljava/util/Enumeration;
    .end local v8    # "mzkey":Ljava/lang/String;
    .end local v13    # "tzid":Ljava/lang/String;
    .restart local v4    # "metaToOlson":Ljava/util/HashMap;
    :catchall_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "metaToOlson":Ljava/util/HashMap;
    .restart local v3    # "metaToOlson":Ljava/util/HashMap;
    goto :goto_5

    .line 961
    .restart local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    :catch_1
    move-exception v14

    goto :goto_2

    .end local v3    # "metaToOlson":Ljava/util/HashMap;
    .end local v5    # "metazonesBundle":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v4    # "metaToOlson":Ljava/util/HashMap;
    :cond_4
    move-object v3, v4

    .end local v4    # "metaToOlson":Ljava/util/HashMap;
    .restart local v3    # "metaToOlson":Ljava/util/HashMap;
    goto :goto_6

    :cond_5
    move-object v4, v3

    .end local v3    # "metaToOlson":Ljava/util/HashMap;
    .restart local v4    # "metaToOlson":Ljava/util/HashMap;
    goto/16 :goto_1
.end method

.method public static getMetazoneID(Ljava/lang/String;J)Ljava/lang/String;
    .locals 9
    .param p0, "olsonID"    # Ljava/lang/String;
    .param p1, "date"    # J

    .prologue
    .line 925
    const/4 v3, 0x0

    .line 926
    .local v3, "mzid":Ljava/lang/String;
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getOlsonToMetaMap()Ljava/util/Map;

    move-result-object v5

    .line 927
    .local v5, "olsonToMeta":Ljava/util/Map;
    invoke-interface {v5, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 928
    .local v2, "mappings":Ljava/util/List;
    if-nez v2, :cond_0

    .line 930
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalSystemID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 931
    .local v0, "canonicalID":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 932
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mappings":Ljava/util/List;
    check-cast v2, Ljava/util/List;

    .line 935
    .end local v0    # "canonicalID":Ljava/lang/String;
    .restart local v2    # "mappings":Ljava/util/List;
    :cond_0
    if-eqz v2, :cond_1

    .line 936
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 937
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;

    .line 938
    .local v4, "mzm":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    iget-wide v6, v4, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->from:J

    cmp-long v6, p1, v6

    if-ltz v6, :cond_2

    iget-wide v6, v4, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->to:J

    cmp-long v6, p1, v6

    if-gez v6, :cond_2

    .line 939
    iget-object v3, v4, Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;->mzid:Ljava/lang/String;

    .line 944
    .end local v1    # "i":I
    .end local v4    # "mzm":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    :cond_1
    return-object v3

    .line 936
    .restart local v1    # "i":I
    .restart local v4    # "mzm":Lcom/ibm/icu/impl/ZoneMeta$OlsonToMetaMappingEntry;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static getOlsonMeta()Z
    .locals 4

    .prologue
    .line 565
    sget v1, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I

    if-gez v1, :cond_0

    .line 567
    :try_start_0
    const-string/jumbo v1, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v2, "zoneinfo"

    sget-object v3, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 568
    .local v0, "top":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-static {v0}, Lcom/ibm/icu/impl/ZoneMeta;->getOlsonMeta(Lcom/ibm/icu/impl/ICUResourceBundle;)Z
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    :cond_0
    :goto_0
    sget v1, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I

    if-ltz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 569
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static getOlsonMeta(Lcom/ibm/icu/impl/ICUResourceBundle;)Z
    .locals 3
    .param p0, "top"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    const/4 v1, 0x0

    .line 549
    sget v2, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I

    if-gez v2, :cond_0

    if-eqz p0, :cond_0

    .line 551
    :try_start_0
    const-string/jumbo v2, "Zones"

    invoke-virtual {p0, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 552
    .local v0, "res":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v2

    sput v2, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_COUNT:I

    .line 553
    const/4 v2, 0x0

    sput v2, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    .end local v0    # "res":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    :goto_0
    sget v2, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_ZONE_START:I

    if-ltz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 554
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method static getOlsonToMetaMap()Ljava/util/Map;
    .locals 5

    .prologue
    .line 843
    const/4 v1, 0x0

    .line 844
    .local v1, "olsonToMeta":Ljava/util/Map;
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    if-nez v3, :cond_3

    const-string/jumbo v3, "com.ibm.icu.impl.ZoneMeta"

    invoke-static {v3}, Lcom/ibm/icu/impl/ZoneMeta;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    move-object v4, v3

    :goto_0
    monitor-enter v4

    .line 845
    :try_start_0
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_TO_META_REF:Ljava/lang/ref/SoftReference;

    if-eqz v3, :cond_0

    .line 846
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_TO_META_REF:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/HashMap;

    move-object v1, v0

    .line 848
    :cond_0
    if-nez v1, :cond_2

    .line 849
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->createOlsonToMetaMap()Ljava/util/Map;

    move-result-object v1

    .line 850
    if-nez v1, :cond_1

    .line 852
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .end local v1    # "olsonToMeta":Ljava/util/Map;
    .local v2, "olsonToMeta":Ljava/util/Map;
    move-object v1, v2

    .line 854
    .end local v2    # "olsonToMeta":Ljava/util/Map;
    .restart local v1    # "olsonToMeta":Ljava/util/Map;
    :cond_1
    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v3, Lcom/ibm/icu/impl/ZoneMeta;->OLSON_TO_META_REF:Ljava/lang/ref/SoftReference;

    .line 856
    :cond_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 857
    return-object v1

    .line 844
    :cond_3
    sget-object v3, Lcom/ibm/icu/impl/ZoneMeta;->class$com$ibm$icu$impl$ZoneMeta:Ljava/lang/Class;

    move-object v4, v3

    goto :goto_0

    .line 856
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public static getSingleCountry(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "tzid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 336
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->getCanonicalInfo(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "info":[Ljava/lang/String;
    if-eqz v0, :cond_0

    aget-object v1, v0, v3

    if-eqz v1, :cond_0

    sget-object v1, Lcom/ibm/icu/impl/ZoneMeta;->multiZoneTerritories:Ljava/util/Set;

    aget-object v2, v0, v3

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    aget-object v1, v0, v3

    .line 340
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getSystemTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;
    .locals 8
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 582
    sget-object v5, Lcom/ibm/icu/impl/ZoneMeta;->zoneCache:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v5, p0}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/util/TimeZone;

    .line 583
    .local v3, "z":Lcom/ibm/icu/util/TimeZone;
    if-nez v3, :cond_0

    .line 585
    :try_start_0
    const-string/jumbo v5, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v6, "zoneinfo"

    sget-object v7, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v5, v6, v7}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 586
    .local v2, "top":Lcom/ibm/icu/util/UResourceBundle;
    invoke-static {p0}, Lcom/ibm/icu/impl/ZoneMeta;->openOlsonResource(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 587
    .local v1, "res":Lcom/ibm/icu/util/UResourceBundle;
    new-instance v4, Lcom/ibm/icu/impl/OlsonTimeZone;

    invoke-direct {v4, v2, v1}, Lcom/ibm/icu/impl/OlsonTimeZone;-><init>(Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    .end local v3    # "z":Lcom/ibm/icu/util/TimeZone;
    .local v4, "z":Lcom/ibm/icu/util/TimeZone;
    :try_start_1
    invoke-virtual {v4, p0}, Lcom/ibm/icu/util/TimeZone;->setID(Ljava/lang/String;)V

    .line 589
    sget-object v5, Lcom/ibm/icu/impl/ZoneMeta;->zoneCache:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v5, p0, v4}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 594
    .end local v1    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "top":Lcom/ibm/icu/util/UResourceBundle;
    .end local v4    # "z":Lcom/ibm/icu/util/TimeZone;
    .restart local v3    # "z":Lcom/ibm/icu/util/TimeZone;
    :cond_0
    invoke-virtual {v3}, Lcom/ibm/icu/util/TimeZone;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/util/TimeZone;

    :goto_0
    return-object v5

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "ex":Ljava/lang/Exception;
    :goto_1
    const/4 v5, 0x0

    goto :goto_0

    .line 590
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v3    # "z":Lcom/ibm/icu/util/TimeZone;
    .restart local v1    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v2    # "top":Lcom/ibm/icu/util/UResourceBundle;
    .restart local v4    # "z":Lcom/ibm/icu/util/TimeZone;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "z":Lcom/ibm/icu/util/TimeZone;
    .restart local v3    # "z":Lcom/ibm/icu/util/TimeZone;
    goto :goto_1
.end method

.method public static getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 423
    const/4 v2, 0x0

    .line 425
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 426
    .local v0, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "zoneStrings/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 430
    .end local v0    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_0
    return-object v2

    .line 427
    :catch_0
    move-exception v1

    .line 428
    .local v1, "e":Ljava/util/MissingResourceException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getZoneByName(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 5
    .param p0, "top"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 482
    const-string/jumbo v2, "Names"

    invoke-virtual {p0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 485
    .local v1, "tmp":Lcom/ibm/icu/util/UResourceBundle;
    invoke-static {v1, p1}, Lcom/ibm/icu/impl/ZoneMeta;->findInStringArray(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)I

    move-result v0

    .line 487
    .local v0, "idx":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 489
    new-instance v2, Ljava/util/MissingResourceException;

    const-string/jumbo v3, "Names"

    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .end local v1    # "tmp":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getResPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 493
    .restart local v1    # "tmp":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    const-string/jumbo v2, "Zones"

    invoke-virtual {p0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 494
    invoke-virtual {v1, v0}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 496
    return-object v1
.end method

.method public static getZoneIdByMetazone(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "metazoneID"    # Ljava/lang/String;
    .param p1, "region"    # Ljava/lang/String;

    .prologue
    .line 1005
    const/4 v4, 0x0

    .line 1006
    .local v4, "tzid":Ljava/lang/String;
    invoke-static {}, Lcom/ibm/icu/impl/ZoneMeta;->getMetaToOlsonMap()Ljava/util/Map;

    move-result-object v2

    .line 1007
    .local v2, "metaToOlson":Ljava/util/Map;
    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1008
    .local v1, "mappings":Ljava/util/List;
    if-eqz v1, :cond_0

    .line 1009
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 1010
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;

    .line 1011
    .local v3, "olsonmap":Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;
    iget-object v5, v3, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->territory:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1012
    iget-object v4, v3, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->id:Ljava/lang/String;

    .line 1019
    .end local v0    # "i":I
    .end local v3    # "olsonmap":Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;
    :cond_0
    return-object v4

    .line 1014
    .restart local v0    # "i":I
    .restart local v3    # "olsonmap":Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;
    :cond_1
    iget-object v5, v3, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->territory:Ljava/lang/String;

    const-string/jumbo v6, "001"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1015
    iget-object v4, v3, Lcom/ibm/icu/impl/ZoneMeta$MetaToOlsonMappingEntry;->id:Ljava/lang/String;

    .line 1009
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static openOlsonResource(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 9
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 457
    const/4 v4, 0x0

    .line 459
    .local v4, "res":Lcom/ibm/icu/util/UResourceBundle;
    :try_start_0
    const-string/jumbo v6, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v7, "zoneinfo"

    sget-object v8, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v6, v7, v8}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 460
    .local v5, "top":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-static {v5, p0}, Lcom/ibm/icu/impl/ZoneMeta;->getZoneByName(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 463
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_0

    .line 464
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getInt()I

    move-result v6

    add-int/lit8 v2, v6, 0x0

    .line 465
    .local v2, "deref":I
    const-string/jumbo v6, "Zones"

    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 466
    .local v1, "ares":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    move-object v4, v0
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    .end local v1    # "ares":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "deref":I
    .end local v5    # "top":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_0
    :goto_0
    return-object v4

    .line 468
    :catch_0
    move-exception v3

    .line 469
    .local v3, "e":Ljava/util/MissingResourceException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method static parseCustomID(Ljava/lang/String;[I)Z
    .locals 13
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "fields"    # [I

    .prologue
    .line 651
    const/4 v5, 0x0

    .line 652
    .local v5, "numberFormat":Lcom/ibm/icu/text/NumberFormat;
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 654
    .local v1, "idUppercase":Ljava/lang/String;
    if-eqz p0, :cond_11

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v11

    const-string/jumbo v12, "GMT"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-le v11, v12, :cond_11

    const-string/jumbo v11, "GMT"

    invoke-virtual {v1, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 656
    new-instance v7, Ljava/text/ParsePosition;

    const-string/jumbo v11, "GMT"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-direct {v7, v11}, Ljava/text/ParsePosition;-><init>(I)V

    .line 657
    .local v7, "pos":Ljava/text/ParsePosition;
    const/4 v9, 0x1

    .line 658
    .local v9, "sign":I
    const/4 v0, 0x0

    .line 659
    .local v0, "hour":I
    const/4 v3, 0x0

    .line 660
    .local v3, "min":I
    const/4 v8, 0x0

    .line 662
    .local v8, "sec":I
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0x2d

    if-ne v11, v12, :cond_1

    .line 663
    const/4 v9, -0x1

    .line 667
    :cond_0
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 669
    invoke-static {}, Lcom/ibm/icu/text/NumberFormat;->getInstance()Lcom/ibm/icu/text/NumberFormat;

    move-result-object v5

    .line 670
    const/4 v11, 0x1

    invoke-virtual {v5, v11}, Lcom/ibm/icu/text/NumberFormat;->setParseIntegerOnly(Z)V

    .line 673
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v10

    .line 675
    .local v10, "start":I
    invoke-virtual {v5, p0, v7}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v4

    .line 676
    .local v4, "n":Ljava/lang/Number;
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    if-ne v11, v10, :cond_2

    .line 677
    const/4 v11, 0x0

    .line 761
    .end local v0    # "hour":I
    .end local v3    # "min":I
    .end local v4    # "n":Ljava/lang/Number;
    .end local v7    # "pos":Ljava/text/ParsePosition;
    .end local v8    # "sec":I
    .end local v9    # "sign":I
    .end local v10    # "start":I
    :goto_0
    return v11

    .line 664
    .restart local v0    # "hour":I
    .restart local v3    # "min":I
    .restart local v7    # "pos":Ljava/text/ParsePosition;
    .restart local v8    # "sec":I
    .restart local v9    # "sign":I
    :cond_1
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0x2b

    if-eq v11, v12, :cond_0

    .line 665
    const/4 v11, 0x0

    goto :goto_0

    .line 679
    .restart local v4    # "n":Ljava/lang/Number;
    .restart local v10    # "start":I
    :cond_2
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 681
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_e

    .line 682
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    sub-int/2addr v11, v10

    const/4 v12, 0x2

    if-gt v11, v12, :cond_3

    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0x3a

    if-eq v11, v12, :cond_4

    .line 684
    :cond_3
    const/4 v11, 0x0

    goto :goto_0

    .line 687
    :cond_4
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 688
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 689
    .local v6, "oldPos":I
    invoke-virtual {v5, p0, v7}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v4

    .line 690
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    sub-int/2addr v11, v6

    const/4 v12, 0x2

    if-eq v11, v12, :cond_5

    .line 692
    const/4 v11, 0x0

    goto :goto_0

    .line 694
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 695
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_9

    .line 696
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0x3a

    if-eq v11, v12, :cond_6

    .line 697
    const/4 v11, 0x0

    goto :goto_0

    .line 700
    :cond_6
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 701
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 702
    invoke-virtual {v5, p0, v7}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v4

    .line 703
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    if-ne v11, v12, :cond_7

    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    sub-int/2addr v11, v6

    const/4 v12, 0x2

    if-eq v11, v12, :cond_8

    .line 705
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 707
    :cond_8
    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v8

    .line 743
    .end local v6    # "oldPos":I
    :cond_9
    :goto_1
    :pswitch_0
    const/16 v11, 0x17

    if-gt v0, v11, :cond_11

    const/16 v11, 0x3b

    if-gt v3, v11, :cond_11

    const/16 v11, 0x3b

    if-gt v8, v11, :cond_11

    .line 744
    if-eqz p1, :cond_d

    .line 745
    array-length v11, p1

    const/4 v12, 0x1

    if-lt v11, v12, :cond_a

    .line 746
    const/4 v11, 0x0

    aput v9, p1, v11

    .line 748
    :cond_a
    array-length v11, p1

    const/4 v12, 0x2

    if-lt v11, v12, :cond_b

    .line 749
    const/4 v11, 0x1

    aput v0, p1, v11

    .line 751
    :cond_b
    array-length v11, p1

    const/4 v12, 0x3

    if-lt v11, v12, :cond_c

    .line 752
    const/4 v11, 0x2

    aput v3, p1, v11

    .line 754
    :cond_c
    array-length v11, p1

    const/4 v12, 0x4

    if-lt v11, v12, :cond_d

    .line 755
    const/4 v11, 0x3

    aput v8, p1, v11

    .line 758
    :cond_d
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 719
    :cond_e
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    sub-int v2, v11, v10

    .line 720
    .local v2, "length":I
    if-lez v2, :cond_f

    const/4 v11, 0x6

    if-ge v11, v2, :cond_10

    .line 722
    :cond_f
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 724
    :cond_10
    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 731
    :pswitch_1
    rem-int/lit8 v3, v0, 0x64

    .line 732
    div-int/lit8 v0, v0, 0x64

    .line 733
    goto :goto_1

    .line 736
    :pswitch_2
    rem-int/lit8 v8, v0, 0x64

    .line 737
    div-int/lit8 v11, v0, 0x64

    rem-int/lit8 v3, v11, 0x64

    .line 738
    div-int/lit16 v0, v0, 0x2710

    goto :goto_1

    .line 761
    .end local v0    # "hour":I
    .end local v2    # "length":I
    .end local v3    # "min":I
    .end local v4    # "n":Ljava/lang/Number;
    .end local v7    # "pos":Ljava/text/ParsePosition;
    .end local v8    # "sec":I
    .end local v9    # "sign":I
    .end local v10    # "start":I
    :cond_11
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 724
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static parseDate(Ljava/lang/String;)J
    .locals 14
    .param p0, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/16 v10, 0xa

    .line 1043
    const/4 v8, 0x0

    .local v8, "year":I
    const/4 v6, 0x0

    .local v6, "month":I
    const/4 v2, 0x0

    .local v2, "day":I
    const/4 v3, 0x0

    .local v3, "hour":I
    const/4 v5, 0x0

    .line 1048
    .local v5, "min":I
    const/4 v4, 0x0

    .local v4, "idx":I
    :goto_0
    const/4 v9, 0x3

    if-gt v4, v9, :cond_1

    .line 1049
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    add-int/lit8 v7, v9, -0x30

    .line 1050
    .local v7, "n":I
    if-ltz v7, :cond_0

    if-ge v7, v10, :cond_0

    .line 1051
    mul-int/lit8 v9, v8, 0xa

    add-int v8, v9, v7

    .line 1048
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1053
    :cond_0
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "Bad year"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1057
    .end local v7    # "n":I
    :cond_1
    const/4 v4, 0x5

    :goto_1
    const/4 v9, 0x6

    if-gt v4, v9, :cond_3

    .line 1058
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    add-int/lit8 v7, v9, -0x30

    .line 1059
    .restart local v7    # "n":I
    if-ltz v7, :cond_2

    if-ge v7, v10, :cond_2

    .line 1060
    mul-int/lit8 v9, v6, 0xa

    add-int v6, v9, v7

    .line 1057
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1062
    :cond_2
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "Bad month"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1066
    .end local v7    # "n":I
    :cond_3
    const/16 v4, 0x8

    :goto_2
    const/16 v9, 0x9

    if-gt v4, v9, :cond_5

    .line 1067
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    add-int/lit8 v7, v9, -0x30

    .line 1068
    .restart local v7    # "n":I
    if-ltz v7, :cond_4

    if-ge v7, v10, :cond_4

    .line 1069
    mul-int/lit8 v9, v2, 0xa

    add-int v2, v9, v7

    .line 1066
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1071
    :cond_4
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "Bad day"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1075
    .end local v7    # "n":I
    :cond_5
    const/16 v4, 0xb

    :goto_3
    const/16 v9, 0xc

    if-gt v4, v9, :cond_7

    .line 1076
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    add-int/lit8 v7, v9, -0x30

    .line 1077
    .restart local v7    # "n":I
    if-ltz v7, :cond_6

    if-ge v7, v10, :cond_6

    .line 1078
    mul-int/lit8 v9, v3, 0xa

    add-int v3, v9, v7

    .line 1075
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1080
    :cond_6
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "Bad hour"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1084
    .end local v7    # "n":I
    :cond_7
    const/16 v4, 0xe

    :goto_4
    const/16 v9, 0xf

    if-gt v4, v9, :cond_9

    .line 1085
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    add-int/lit8 v7, v9, -0x30

    .line 1086
    .restart local v7    # "n":I
    if-ltz v7, :cond_8

    if-ge v7, v10, :cond_8

    .line 1087
    mul-int/lit8 v9, v5, 0xa

    add-int v5, v9, v7

    .line 1084
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1089
    :cond_8
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v10, "Bad minute"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 1093
    .end local v7    # "n":I
    :cond_9
    add-int/lit8 v9, v6, -0x1

    invoke-static {v8, v9, v2}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v10

    const-wide/32 v12, 0x5265c00

    mul-long/2addr v10, v12

    const v9, 0x36ee80

    mul-int/2addr v9, v3

    int-to-long v12, v9

    add-long/2addr v10, v12

    const v9, 0xea60

    mul-int/2addr v9, v5

    int-to-long v12, v9

    add-long v0, v10, v12

    .line 1095
    .local v0, "date":J
    return-wide v0
.end method
