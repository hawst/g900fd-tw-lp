.class public final Lcom/ibm/icu/impl/DateNumberFormat;
.super Lcom/ibm/icu/text/NumberFormat;
.source "DateNumberFormat.java"


# static fields
.field private static CACHE:Lcom/ibm/icu/impl/SimpleCache; = null

.field private static final serialVersionUID:J = -0x57a5d92a02d4dc49L


# instance fields
.field private transient decimalBuf:[C

.field private maxIntDigits:I

.field private minIntDigits:I

.field private minusSign:C

.field private positiveOnly:Z

.field private zeroDigit:C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/impl/DateNumberFormat;->CACHE:Lcom/ibm/icu/impl/SimpleCache;

    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->positiveOnly:Z

    .line 34
    const/16 v0, 0x14

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    .line 42
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/DateNumberFormat;->initialize(Lcom/ibm/icu/util/ULocale;)V

    .line 43
    return-void
.end method

.method private initialize(Lcom/ibm/icu/util/ULocale;)V
    .locals 6
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    sget-object v3, Lcom/ibm/icu/impl/DateNumberFormat;->CACHE:Lcom/ibm/icu/impl/SimpleCache;

    invoke-virtual {v3, p1}, Lcom/ibm/icu/impl/SimpleCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [C

    move-object v0, v3

    check-cast v0, [C

    .line 53
    .local v0, "elems":[C
    if-nez v0, :cond_0

    .line 55
    const-string/jumbo v3, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v3, p1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 56
    .local v2, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v3, "NumberElements"

    invoke-virtual {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "numberElements":[Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [C

    .line 58
    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, v0, v4

    .line 59
    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, v0, v5

    .line 60
    sget-object v3, Lcom/ibm/icu/impl/DateNumberFormat;->CACHE:Lcom/ibm/icu/impl/SimpleCache;

    invoke-virtual {v3, p1, v0}, Lcom/ibm/icu/impl/SimpleCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62
    .end local v1    # "numberElements":[Ljava/lang/String;
    .end local v2    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_0
    aget-char v3, v0, v4

    iput-char v3, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    .line 63
    aget-char v3, v0, v5

    iput-char v3, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minusSign:C

    .line 64
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 209
    const/16 v0, 0x14

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    .line 210
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 195
    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Lcom/ibm/icu/text/NumberFormat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, Lcom/ibm/icu/impl/DateNumberFormat;

    if-nez v2, :cond_1

    .line 199
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 198
    check-cast v0, Lcom/ibm/icu/impl/DateNumberFormat;

    .line 199
    .local v0, "other":Lcom/ibm/icu/impl/DateNumberFormat;
    iget v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    iget v3, v0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minIntDigits:I

    iget v3, v0, Lcom/ibm/icu/impl/DateNumberFormat;->minIntDigits:I

    if-ne v2, v3, :cond_0

    iget-char v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    iget-char v3, v0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    if-ne v2, v3, :cond_0

    iget-char v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minusSign:C

    iget-char v3, v0, Lcom/ibm/icu/impl/DateNumberFormat;->minusSign:C

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->positiveOnly:Z

    iget-boolean v3, v0, Lcom/ibm/icu/impl/DateNumberFormat;->positiveOnly:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # D
    .param p3, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 97
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "StringBuffer format(double, StringBuffer, FieldPostion) is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 9
    .param p1, "numberL"    # J
    .param p3, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;

    .prologue
    const/4 v8, 0x0

    .line 103
    const-wide/16 v6, 0x0

    cmp-long v5, p1, v6

    if-gez v5, :cond_0

    .line 105
    iget-char v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minusSign:C

    invoke-virtual {p3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 111
    :cond_0
    long-to-int v3, p1

    .line 113
    .local v3, "number":I
    iget-object v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    array-length v5, v5

    iget v6, p0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    if-ge v5, v6, :cond_2

    iget-object v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    array-length v2, v5

    .line 114
    .local v2, "limit":I
    :goto_0
    add-int/lit8 v0, v2, -0x1

    .line 116
    .local v0, "index":I
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    rem-int/lit8 v6, v3, 0xa

    iget-char v7, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    add-int/2addr v6, v7

    int-to-char v6, v6

    aput-char v6, v5, v0

    .line 117
    div-int/lit8 v3, v3, 0xa

    .line 118
    if-eqz v0, :cond_1

    if-nez v3, :cond_3

    .line 123
    :cond_1
    iget v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minIntDigits:I

    sub-int v6, v2, v0

    sub-int v4, v5, v6

    .line 124
    .local v4, "padding":I
    :goto_2
    if-lez v4, :cond_4

    .line 125
    iget-object v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    add-int/lit8 v0, v0, -0x1

    iget-char v6, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    aput-char v6, v5, v0

    .line 124
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 113
    .end local v0    # "index":I
    .end local v2    # "limit":I
    .end local v4    # "padding":I
    :cond_2
    iget v2, p0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    goto :goto_0

    .line 121
    .restart local v0    # "index":I
    .restart local v2    # "limit":I
    :cond_3
    add-int/lit8 v0, v0, -0x1

    .line 122
    goto :goto_1

    .line 127
    .restart local v4    # "padding":I
    :cond_4
    sub-int v1, v2, v0

    .line 128
    .local v1, "length":I
    iget-object v5, p0, Lcom/ibm/icu/impl/DateNumberFormat;->decimalBuf:[C

    invoke-virtual {p3, v5, v0, v1}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {p4, v8}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 130
    invoke-virtual {p4}, Ljava/text/FieldPosition;->getField()I

    move-result v5

    if-nez v5, :cond_5

    .line 131
    invoke-virtual {p4, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 135
    :goto_3
    return-object p3

    .line 133
    :cond_5
    invoke-virtual {p4, v8}, Ljava/text/FieldPosition;->setEndIndex(I)V

    goto :goto_3
.end method

.method public format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 153
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "StringBuffer format(BigDecimal, StringBuffer, FieldPostion) is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # Ljava/math/BigDecimal;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 147
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "StringBuffer format(BigDecimal, StringBuffer, FieldPostion) is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # Ljava/math/BigInteger;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "StringBuffer format(BigInteger, StringBuffer, FieldPostion) is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMaximumIntegerDigits()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    return v0
.end method

.method public getMinimumIntegerDigits()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minIntDigits:I

    return v0
.end method

.method public getZeroDigit()C
    .locals 1

    .prologue
    .line 88
    iget-char v0, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    return v0
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;
    .locals 16
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;

    .prologue
    .line 160
    const-wide/16 v6, 0x0

    .line 161
    .local v6, "num":J
    const/4 v10, 0x0

    .line 162
    .local v10, "sawNumber":Z
    const/4 v5, 0x0

    .line 163
    .local v5, "negative":Z
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    .line 164
    .local v2, "base":I
    const/4 v8, 0x0

    .line 165
    .local v8, "offset":I
    :goto_0
    add-int v11, v2, v8

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_0

    .line 166
    add-int v11, v2, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 167
    .local v3, "ch":C
    if-nez v8, :cond_4

    move-object/from16 v0, p0

    iget-char v11, v0, Lcom/ibm/icu/impl/DateNumberFormat;->minusSign:C

    if-ne v3, v11, :cond_4

    .line 168
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/ibm/icu/impl/DateNumberFormat;->positiveOnly:Z

    if-eqz v11, :cond_3

    .line 185
    .end local v3    # "ch":C
    :cond_0
    const/4 v9, 0x0

    .line 186
    .local v9, "result":Ljava/lang/Number;
    if-eqz v10, :cond_2

    .line 187
    if-eqz v5, :cond_1

    const-wide/16 v12, -0x1

    mul-long/2addr v6, v12

    .line 188
    :cond_1
    new-instance v9, Ljava/lang/Long;

    .end local v9    # "result":Ljava/lang/Number;
    invoke-direct {v9, v6, v7}, Ljava/lang/Long;-><init>(J)V

    .line 189
    .restart local v9    # "result":Ljava/lang/Number;
    add-int v11, v2, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 191
    :cond_2
    return-object v9

    .line 171
    .end local v9    # "result":Ljava/lang/Number;
    .restart local v3    # "ch":C
    :cond_3
    const/4 v5, 0x1

    .line 165
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 173
    :cond_4
    move-object/from16 v0, p0

    iget-char v11, v0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    sub-int v4, v3, v11

    .line 174
    .local v4, "digit":I
    if-ltz v4, :cond_5

    const/16 v11, 0x9

    if-ge v11, v4, :cond_6

    .line 175
    :cond_5
    invoke-static {v3}, Lcom/ibm/icu/lang/UCharacter;->digit(I)I

    move-result v4

    .line 177
    :cond_6
    if-ltz v4, :cond_0

    const/16 v11, 0x9

    if-gt v4, v11, :cond_0

    .line 178
    const/4 v10, 0x1

    .line 179
    const-wide/16 v12, 0xa

    mul-long/2addr v12, v6

    int-to-long v14, v4

    add-long v6, v12, v14

    goto :goto_1
.end method

.method public setMaximumIntegerDigits(I)V
    .locals 0
    .param p1, "newValue"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/ibm/icu/impl/DateNumberFormat;->maxIntDigits:I

    .line 68
    return-void
.end method

.method public setMinimumIntegerDigits(I)V
    .locals 0
    .param p1, "newValue"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/ibm/icu/impl/DateNumberFormat;->minIntDigits:I

    .line 76
    return-void
.end method

.method public setParsePositiveOnly(Z)V
    .locals 0
    .param p1, "isPositiveOnly"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/ibm/icu/impl/DateNumberFormat;->positiveOnly:Z

    .line 85
    return-void
.end method

.method public setZeroDigit(C)V
    .locals 0
    .param p1, "zero"    # C

    .prologue
    .line 92
    iput-char p1, p0, Lcom/ibm/icu/impl/DateNumberFormat;->zeroDigit:C

    .line 93
    return-void
.end method
