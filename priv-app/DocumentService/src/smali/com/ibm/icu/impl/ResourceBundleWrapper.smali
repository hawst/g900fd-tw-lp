.class public Lcom/ibm/icu/impl/ResourceBundleWrapper;
.super Lcom/ibm/icu/util/UResourceBundle;
.source "ResourceBundleWrapper.java"


# static fields
.field private static final DEBUG:Z


# instance fields
.field private baseName:Ljava/lang/String;

.field private bundle:Ljava/util/ResourceBundle;

.field private keys:Ljava/util/Vector;

.field private localeID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "resourceBundleWrapper"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->DEBUG:Z

    return-void
.end method

.method private constructor <init>(Ljava/util/ResourceBundle;)V
    .locals 1
    .param p1, "bundle"    # Ljava/util/ResourceBundle;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/ibm/icu/util/UResourceBundle;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->bundle:Ljava/util/ResourceBundle;

    .line 27
    iput-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->localeID:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->baseName:Ljava/lang/String;

    .line 29
    iput-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->keys:Ljava/util/Vector;

    .line 33
    iput-object p1, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->bundle:Ljava/util/ResourceBundle;

    .line 34
    return-void
.end method

.method public static getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 6
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeID"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;
    .param p3, "disableFallback"    # Z

    .prologue
    .line 101
    invoke-static {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 102
    .local v0, "b":Lcom/ibm/icu/util/UResourceBundle;
    if-nez v0, :cond_1

    .line 103
    const-string/jumbo v1, "_"

    .line 104
    .local v1, "separator":Ljava/lang/String;
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    .line 105
    const-string/jumbo v1, "/"

    .line 107
    :cond_0
    new-instance v2, Ljava/util/MissingResourceException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Could not find the bundle "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 109
    .end local v1    # "separator":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private initKeysVector()V
    .locals 4

    .prologue
    .line 66
    move-object v0, p0

    .line 67
    .local v0, "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->keys:Ljava/util/Vector;

    .line 68
    :goto_0
    if-eqz v0, :cond_2

    .line 69
    iget-object v3, v0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v3}, Ljava/util/ResourceBundle;->getKeys()Ljava/util/Enumeration;

    move-result-object v1

    .line 70
    .local v1, "e":Ljava/util/Enumeration;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 71
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 72
    .local v2, "elem":Ljava/lang/String;
    iget-object v3, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->keys:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 73
    iget-object v3, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->keys:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    .end local v2    # "elem":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .end local v0    # "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v0, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 77
    .restart local v0    # "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    goto :goto_0

    .line 78
    .end local v1    # "e":Ljava/util/Enumeration;
    :cond_2
    return-void
.end method

.method protected static declared-synchronized instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 22
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeID"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;
    .param p3, "disableFallback"    # Z

    .prologue
    .line 114
    const-class v19, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    monitor-enter v19

    if-nez p2, :cond_0

    .line 116
    :try_start_0
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    .line 118
    :cond_0
    move-object/from16 v6, p2

    .line 119
    .local v6, "cl":Ljava/lang/ClassLoader;
    move-object/from16 v13, p0

    .line 120
    .local v13, "name":Ljava/lang/String;
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v8

    .line 121
    .local v8, "defaultLocale":Lcom/ibm/icu/util/ULocale;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_1

    .line 122
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v20, "_"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    .line 125
    :cond_1
    invoke-static {v6, v13, v8}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->loadFromCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 126
    .local v3, "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-nez v3, :cond_5

    .line 127
    const/4 v14, 0x0

    .line 128
    .local v14, "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    const/16 v18, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    .line 130
    .local v11, "i":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v11, v0, :cond_7

    .line 131
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 132
    .local v12, "locName":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v20, "_"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v6, v0, v8}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->loadFromCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v14, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 133
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-nez v14, :cond_2

    .line 134
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v12, v6, v1}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v14, Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 143
    .end local v12    # "locName":Ljava/lang/String;
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_2
    :goto_0
    :try_start_1
    invoke-virtual {v6, v13}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 144
    .local v7, "cls":Ljava/lang/Class;
    invoke-virtual {v7}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ResourceBundle;

    .line 145
    .local v5, "bx":Ljava/util/ResourceBundle;
    new-instance v4, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    invoke-direct {v4, v5}, Lcom/ibm/icu/impl/ResourceBundleWrapper;-><init>(Ljava/util/ResourceBundle;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 146
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .local v4, "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-eqz v14, :cond_3

    .line 147
    :try_start_2
    invoke-virtual {v4, v14}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->setParent(Ljava/util/ResourceBundle;)V

    .line 149
    :cond_3
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/ibm/icu/impl/ResourceBundleWrapper;->baseName:Ljava/lang/String;

    .line 150
    move-object/from16 v0, p1

    iput-object v0, v4, Lcom/ibm/icu/impl/ResourceBundleWrapper;->localeID:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v3, v4

    .line 209
    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v5    # "bx":Ljava/util/ResourceBundle;
    .end local v7    # "cls":Ljava/lang/Class;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_4
    :goto_1
    :try_start_3
    invoke-static {v6, v13, v8, v3}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->addToCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/UResourceBundle;)V

    .line 211
    .end local v11    # "i":I
    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_5
    if-eqz v3, :cond_b

    .line 212
    invoke-direct {v3}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->initKeysVector()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 217
    :cond_6
    :goto_2
    monitor-exit v19

    return-object v3

    .line 136
    .restart local v11    # "i":I
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_7
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_2

    .line 137
    move-object/from16 v0, p0

    invoke-static {v6, v0, v8}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->loadFromCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v14, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 138
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-nez v14, :cond_2

    .line 139
    const-string/jumbo v18, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v6, v2}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v14, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    goto :goto_0

    .line 152
    :catch_0
    move-exception v10

    move-object v4, v3

    .line 154
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .local v10, "e":Ljava/lang/ClassNotFoundException;
    :goto_3
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v20, 0x2e

    const/16 v21, 0x2f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v20, ".properties"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    .line 155
    .local v15, "resName":Ljava/lang/String;
    new-instance v18, Lcom/ibm/icu/impl/ResourceBundleWrapper$1;

    move-object/from16 v0, v18

    invoke-direct {v0, v6, v15}, Lcom/ibm/icu/impl/ResourceBundleWrapper$1;-><init>(Ljava/lang/ClassLoader;Ljava/lang/String;)V

    invoke-static/range {v18 .. v18}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/io/InputStream;

    .line 166
    .local v16, "stream":Ljava/io/InputStream;
    if-eqz v16, :cond_c

    .line 168
    new-instance v17, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 170
    .end local v16    # "stream":Ljava/io/InputStream;
    .local v17, "stream":Ljava/io/InputStream;
    :try_start_5
    new-instance v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    new-instance v18, Ljava/util/PropertyResourceBundle;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/util/PropertyResourceBundle;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Lcom/ibm/icu/impl/ResourceBundleWrapper;-><init>(Ljava/util/ResourceBundle;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 171
    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-eqz v14, :cond_8

    .line 172
    :try_start_6
    invoke-virtual {v3, v14}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->setParent(Ljava/util/ResourceBundle;)V

    .line 174
    :cond_8
    move-object/from16 v0, p0

    iput-object v0, v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;->baseName:Ljava/lang/String;

    .line 175
    move-object/from16 v0, p1

    iput-object v0, v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;->localeID:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 180
    :try_start_7
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object/from16 v16, v17

    .line 189
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v16    # "stream":Ljava/io/InputStream;
    :goto_4
    if-nez v3, :cond_9

    .line 190
    :try_start_8
    invoke-virtual {v8}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v9

    .line 191
    .local v9, "defaultName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_9

    const/16 v18, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    if-gez v18, :cond_9

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v18

    const/16 v20, -0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    .line 192
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v20, "_"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v6, v0, v8}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->loadFromCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 193
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    if-nez v3, :cond_9

    .line 194
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v9, v6, v1}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v3, Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 199
    .end local v9    # "defaultName":Ljava/lang/String;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_9
    if-nez v3, :cond_4

    .line 200
    move-object v3, v14

    goto/16 :goto_1

    .line 181
    .end local v16    # "stream":Ljava/io/InputStream;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catch_1
    move-exception v18

    move-object/from16 v16, v17

    .line 184
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v16    # "stream":Ljava/io/InputStream;
    goto :goto_4

    .line 176
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v16    # "stream":Ljava/io/InputStream;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catch_2
    move-exception v18

    move-object v3, v4

    .line 180
    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :goto_5
    :try_start_9
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-object/from16 v16, v17

    .line 183
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v16    # "stream":Ljava/io/InputStream;
    goto :goto_4

    .line 181
    .end local v16    # "stream":Ljava/io/InputStream;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catch_3
    move-exception v18

    move-object/from16 v16, v17

    .line 184
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v16    # "stream":Ljava/io/InputStream;
    goto :goto_4

    .line 179
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v16    # "stream":Ljava/io/InputStream;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catchall_0
    move-exception v18

    move-object v3, v4

    .line 180
    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :goto_6
    :try_start_a
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 184
    :goto_7
    :try_start_b
    throw v18
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 114
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v6    # "cl":Ljava/lang/ClassLoader;
    .end local v8    # "defaultLocale":Lcom/ibm/icu/util/ULocale;
    .end local v10    # "e":Ljava/lang/ClassNotFoundException;
    .end local v11    # "i":I
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v15    # "resName":Ljava/lang/String;
    .end local v17    # "stream":Ljava/io/InputStream;
    :catchall_1
    move-exception v18

    monitor-exit v19

    throw v18

    .line 202
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v6    # "cl":Ljava/lang/ClassLoader;
    .restart local v8    # "defaultLocale":Lcom/ibm/icu/util/ULocale;
    .restart local v11    # "i":I
    .restart local v13    # "name":Ljava/lang/String;
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :catch_4
    move-exception v10

    .line 203
    .local v10, "e":Ljava/lang/Exception;
    :goto_8
    :try_start_c
    sget-boolean v18, Lcom/ibm/icu/impl/ResourceBundleWrapper;->DEBUG:Z

    if-eqz v18, :cond_a

    .line 204
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v20, "failure"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 205
    :cond_a
    sget-boolean v18, Lcom/ibm/icu/impl/ResourceBundleWrapper;->DEBUG:Z

    if-eqz v18, :cond_4

    .line 206
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 214
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "i":I
    .end local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    :cond_b
    sget-boolean v18, Lcom/ibm/icu/impl/ResourceBundleWrapper;->DEBUG:Z

    if-eqz v18, :cond_6

    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v21, "Returning null for "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string/jumbo v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_2

    .line 181
    .local v10, "e":Ljava/lang/ClassNotFoundException;
    .restart local v11    # "i":I
    .restart local v14    # "parent":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v15    # "resName":Ljava/lang/String;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catch_5
    move-exception v20

    goto :goto_7

    .line 202
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v10    # "e":Ljava/lang/ClassNotFoundException;
    .end local v15    # "resName":Ljava/lang/String;
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v5    # "bx":Ljava/util/ResourceBundle;
    .restart local v7    # "cls":Ljava/lang/Class;
    :catch_6
    move-exception v10

    move-object v3, v4

    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    goto :goto_8

    .line 179
    .end local v5    # "bx":Ljava/util/ResourceBundle;
    .end local v7    # "cls":Ljava/lang/Class;
    .restart local v10    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v15    # "resName":Ljava/lang/String;
    .restart local v17    # "stream":Ljava/io/InputStream;
    :catchall_2
    move-exception v18

    goto :goto_6

    .line 176
    :catch_7
    move-exception v18

    goto :goto_5

    .line 152
    .end local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .end local v10    # "e":Ljava/lang/ClassNotFoundException;
    .end local v15    # "resName":Ljava/lang/String;
    .end local v17    # "stream":Ljava/io/InputStream;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v5    # "bx":Ljava/util/ResourceBundle;
    .restart local v7    # "cls":Ljava/lang/Class;
    :catch_8
    move-exception v10

    goto/16 :goto_3

    .end local v5    # "bx":Ljava/util/ResourceBundle;
    .end local v7    # "cls":Ljava/lang/Class;
    .restart local v10    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v15    # "resName":Ljava/lang/String;
    .restart local v16    # "stream":Ljava/io/InputStream;
    :cond_c
    move-object v3, v4

    .end local v4    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    goto/16 :goto_4
.end method


# virtual methods
.method protected getBaseName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeys()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->keys:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method protected getLocaleID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->localeID:Ljava/lang/String;

    return-object v0
.end method

.method public getParent()Lcom/ibm/icu/util/UResourceBundle;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->parent:Ljava/util/ResourceBundle;

    check-cast v0, Lcom/ibm/icu/util/UResourceBundle;

    return-object v0
.end method

.method public getULocale()Lcom/ibm/icu/util/ULocale;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    iget-object v1, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->localeID:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected handleGetObject(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "aKey"    # Ljava/lang/String;

    .prologue
    .line 41
    move-object v0, p0

    .line 42
    .local v0, "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    const/4 v2, 0x0

    .line 43
    .local v2, "obj":Ljava/lang/Object;
    :goto_0
    if-eqz v0, :cond_0

    .line 45
    :try_start_0
    iget-object v3, v0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->bundle:Ljava/util/ResourceBundle;

    invoke-virtual {v3, p1}, Ljava/util/ResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 51
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_0
    if-nez v2, :cond_1

    .line 52
    new-instance v3, Ljava/util/MissingResourceException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Can\'t find resource for bundle "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/impl/ResourceBundleWrapper;->baseName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ", key "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 47
    .restart local v2    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 48
    .local v1, "ex":Ljava/util/MissingResourceException;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ResourceBundleWrapper;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .end local v0    # "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    check-cast v0, Lcom/ibm/icu/impl/ResourceBundleWrapper;

    .line 50
    .restart local v0    # "current":Lcom/ibm/icu/impl/ResourceBundleWrapper;
    goto :goto_0

    .line 58
    .end local v1    # "ex":Ljava/util/MissingResourceException;
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_1
    return-object v2
.end method

.method protected setLoadingStatus(I)V
    .locals 0
    .param p1, "newStatus"    # I

    .prologue
    .line 38
    return-void
.end method
