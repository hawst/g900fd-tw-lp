.class public Lcom/ibm/icu/impl/SortedSetRelation;
.super Ljava/lang/Object;
.source "SortedSetRelation.java"


# static fields
.field public static final A:I = 0x6

.field public static final ADDALL:I = 0x7

.field public static final ANY:I = 0x7

.field public static final A_AND_B:I = 0x2

.field public static final A_NOT_B:I = 0x4

.field public static final B:I = 0x3

.field public static final B_NOT_A:I = 0x1

.field public static final B_REMOVEALL:I = 0x1

.field public static final COMPLEMENTALL:I = 0x5

.field public static final CONTAINS:I = 0x6

.field public static final DISJOINT:I = 0x5

.field public static final EQUALS:I = 0x2

.field public static final ISCONTAINED:I = 0x3

.field public static final NONE:I = 0x0

.field public static final NO_A:I = 0x1

.field public static final NO_B:I = 0x4

.field public static final REMOVEALL:I = 0x4

.field public static final RETAINALL:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doOperation(Ljava/util/SortedSet;ILjava/util/SortedSet;)Ljava/util/SortedSet;
    .locals 4
    .param p0, "a"    # Ljava/util/SortedSet;
    .param p1, "relation"    # I
    .param p2, "b"    # Ljava/util/SortedSet;

    .prologue
    .line 143
    packed-switch p1, :pswitch_data_0

    .line 177
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Relation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " out of range"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :pswitch_0
    invoke-interface {p0, p2}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    .line 175
    :goto_0
    :pswitch_1
    return-object p0

    .line 150
    :pswitch_2
    invoke-interface {p0}, Ljava/util/SortedSet;->clear()V

    .line 151
    invoke-interface {p0, p2}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 154
    :pswitch_3
    invoke-interface {p0, p2}, Ljava/util/SortedSet;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 157
    :pswitch_4
    invoke-interface {p0, p2}, Ljava/util/SortedSet;->retainAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 162
    :pswitch_5
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p2}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    .line 163
    .local v0, "temp":Ljava/util/TreeSet;
    invoke-virtual {v0, p0}, Ljava/util/TreeSet;->removeAll(Ljava/util/Collection;)Z

    .line 164
    invoke-interface {p0, p2}, Ljava/util/SortedSet;->removeAll(Ljava/util/Collection;)Z

    .line 165
    invoke-interface {p0, v0}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 168
    .end local v0    # "temp":Ljava/util/TreeSet;
    :pswitch_6
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p2}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    .line 169
    .restart local v0    # "temp":Ljava/util/TreeSet;
    invoke-virtual {v0, p0}, Ljava/util/TreeSet;->removeAll(Ljava/util/Collection;)Z

    .line 170
    invoke-interface {p0}, Ljava/util/SortedSet;->clear()V

    .line 171
    invoke-interface {p0, v0}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 174
    .end local v0    # "temp":Ljava/util/TreeSet;
    :pswitch_7
    invoke-interface {p0}, Ljava/util/SortedSet;->clear()V

    goto :goto_0

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static hasRelation(Ljava/util/SortedSet;ILjava/util/SortedSet;)Z
    .locals 12
    .param p0, "a"    # Ljava/util/SortedSet;
    .param p1, "allow"    # I
    .param p2, "b"    # Ljava/util/SortedSet;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 71
    if-ltz p1, :cond_0

    const/4 v10, 0x7

    if-le p1, v10, :cond_1

    .line 72
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "Relation "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, " out of range"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 78
    :cond_1
    and-int/lit8 v10, p1, 0x4

    if-eqz v10, :cond_4

    move v3, v9

    .line 79
    .local v3, "anb":Z
    :goto_0
    and-int/lit8 v10, p1, 0x2

    if-eqz v10, :cond_5

    move v1, v9

    .line 80
    .local v1, "ab":Z
    :goto_1
    and-int/lit8 v10, p1, 0x1

    if-eqz v10, :cond_6

    move v6, v9

    .line 83
    .local v6, "bna":Z
    :goto_2
    packed-switch p1, :pswitch_data_0

    .line 90
    :cond_2
    :pswitch_0
    invoke-interface {p0}, Ljava/util/SortedSet;->size()I

    move-result v10

    if-nez v10, :cond_7

    .line 91
    invoke-interface {p2}, Ljava/util/SortedSet;->size()I

    move-result v8

    if-nez v8, :cond_3

    move v6, v9

    .line 125
    .end local v6    # "bna":Z
    :cond_3
    :goto_3
    return v6

    .end local v1    # "ab":Z
    .end local v3    # "anb":Z
    :cond_4
    move v3, v8

    .line 78
    goto :goto_0

    .restart local v3    # "anb":Z
    :cond_5
    move v1, v8

    .line 79
    goto :goto_1

    .restart local v1    # "ab":Z
    :cond_6
    move v6, v8

    .line 80
    goto :goto_2

    .line 84
    .restart local v6    # "bna":Z
    :pswitch_1
    invoke-interface {p0}, Ljava/util/SortedSet;->size()I

    move-result v10

    invoke-interface {p2}, Ljava/util/SortedSet;->size()I

    move-result v11

    if-ge v10, v11, :cond_2

    move v6, v8

    goto :goto_3

    .line 85
    :pswitch_2
    invoke-interface {p0}, Ljava/util/SortedSet;->size()I

    move-result v10

    invoke-interface {p2}, Ljava/util/SortedSet;->size()I

    move-result v11

    if-le v10, v11, :cond_2

    move v6, v8

    goto :goto_3

    .line 86
    :pswitch_3
    invoke-interface {p0}, Ljava/util/SortedSet;->size()I

    move-result v10

    invoke-interface {p2}, Ljava/util/SortedSet;->size()I

    move-result v11

    if-eq v10, v11, :cond_2

    move v6, v8

    goto :goto_3

    .line 93
    :cond_7
    invoke-interface {p2}, Ljava/util/SortedSet;->size()I

    move-result v10

    if-nez v10, :cond_8

    move v6, v3

    .line 94
    goto :goto_3

    .line 98
    :cond_8
    invoke-interface {p0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 99
    .local v2, "ait":Ljava/util/Iterator;
    invoke-interface {p2}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 101
    .local v5, "bit":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 102
    .local v0, "aa":Ljava/lang/Comparable;
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Comparable;

    .line 105
    .local v4, "bb":Ljava/lang/Comparable;
    :goto_4
    invoke-interface {v0, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v7

    .line 106
    .local v7, "comp":I
    if-nez v7, :cond_c

    .line 107
    if-nez v1, :cond_9

    move v6, v8

    goto :goto_3

    .line 108
    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_a

    .line 109
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    move v6, v9

    goto :goto_3

    .line 111
    :cond_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_b

    move v6, v3

    .line 112
    goto :goto_3

    .line 114
    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "aa":Ljava/lang/Comparable;
    check-cast v0, Ljava/lang/Comparable;

    .line 115
    .restart local v0    # "aa":Ljava/lang/Comparable;
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "bb":Ljava/lang/Comparable;
    check-cast v4, Ljava/lang/Comparable;

    .line 116
    .restart local v4    # "bb":Ljava/lang/Comparable;
    goto :goto_4

    :cond_c
    if-gez v7, :cond_e

    .line 117
    if-nez v3, :cond_d

    move v6, v8

    goto :goto_3

    .line 118
    :cond_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 121
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "aa":Ljava/lang/Comparable;
    check-cast v0, Ljava/lang/Comparable;

    .line 122
    .restart local v0    # "aa":Ljava/lang/Comparable;
    goto :goto_4

    .line 123
    :cond_e
    if-nez v6, :cond_f

    move v6, v8

    goto/16 :goto_3

    .line 124
    :cond_f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_10

    move v6, v3

    .line 125
    goto/16 :goto_3

    .line 127
    :cond_10
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "bb":Ljava/lang/Comparable;
    check-cast v4, Ljava/lang/Comparable;

    .restart local v4    # "bb":Ljava/lang/Comparable;
    goto :goto_4

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
