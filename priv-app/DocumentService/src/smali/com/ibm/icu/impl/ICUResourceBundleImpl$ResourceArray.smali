.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceArray"
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 69
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 70
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->resource:J

    .line 71
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->key:Ljava/lang/String;

    .line 72
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->countItems()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->size:I

    .line 73
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->resPath:Ljava/lang/String;

    .line 74
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->createLookupCache()V

    .line 75
    return-void
.end method

.method private countItems()I
    .locals 4

    .prologue
    .line 64
    iget-wide v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->resource:J

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->RES_GET_OFFSET(J)I

    move-result v0

    .line 65
    .local v0, "offset":I
    iget-object v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->rawData:[B

    invoke-static {v2, v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->getInt([BI)I

    move-result v1

    .line 66
    .local v1, "value":I
    return v1
.end method


# virtual methods
.method public getStringArray()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->handleGetStringArray()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 11
    .param p1, "index"    # I
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "isAlias"    # [Z

    .prologue
    .line 53
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->size:I

    if-le p1, v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 56
    :cond_0
    iget-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->resource:J

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->RES_GET_OFFSET(J)I

    move-result v10

    .line 57
    .local v10, "offset":I
    add-int/lit8 v0, p1, 0x1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->getIntOffset(I)I

    move-result v0

    add-int v9, v10, v0

    .line 58
    .local v9, "itemOffset":I
    const-wide v0, 0xffffffffL

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->rawData:[B

    invoke-static {v5, v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v5

    int-to-long v6, v5

    and-long v2, v0, v6

    .line 59
    .local v2, "itemResource":J
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->isTopLevel:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "path":Ljava/lang/String;
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p0

    move-object v8, p4

    invoke-virtual/range {v0 .. v8}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    return-object v0

    .line 59
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->resPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method protected handleGetImpl(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[I[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 3
    .param p1, "indexStr"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "index"    # [I
    .param p5, "isAlias"    # [Z

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-static {p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->getIndex(Ljava/lang/String;)I

    move-result v0

    aput v0, p4, v2

    .line 45
    aget v0, p4, v2

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 46
    aget v0, p4, v2

    invoke-virtual {p0, v0, p2, p3, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    return-object v0

    .line 48
    :cond_0
    new-instance v0, Lcom/ibm/icu/util/UResourceTypeMismatchException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Could not get the correct value for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/UResourceTypeMismatchException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected handleGetStringArray()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 27
    iget v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->size:I

    new-array v3, v4, [Ljava/lang/String;

    .line 28
    .local v3, "strings":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;->getIterator()Lcom/ibm/icu/util/UResourceBundleIterator;

    move-result-object v2

    .line 29
    .local v2, "iter":Lcom/ibm/icu/util/UResourceBundleIterator;
    const/4 v0, 0x0

    .line 30
    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundleIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 31
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundleIterator;->next()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    move v0, v1

    .line 32
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 33
    :cond_0
    return-object v3
.end method
