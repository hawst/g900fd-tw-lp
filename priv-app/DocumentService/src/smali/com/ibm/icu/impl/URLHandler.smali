.class public abstract Lcom/ibm/icu/impl/URLHandler;
.super Ljava/lang/Object;
.source "URLHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/URLHandler$URLVisitor;,
        Lcom/ibm/icu/impl/URLHandler$JarURLHandler;,
        Lcom/ibm/icu/impl/URLHandler$FileURLHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field public static final PROPNAME:Ljava/lang/String; = "urlhandler.props"

.field static class$com$ibm$icu$impl$URLHandler:Ljava/lang/Class;

.field static class$java$net$URL:Ljava/lang/Class;

.field private static final handlers:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    .line 29
    const-string/jumbo v13, "URLHandler"

    invoke-static {v13}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v13

    sput-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    .line 32
    const/4 v3, 0x0

    .line 35
    .local v3, "h":Ljava/util/Map;
    :try_start_0
    sget-object v13, Lcom/ibm/icu/impl/URLHandler;->class$com$ibm$icu$impl$URLHandler:Ljava/lang/Class;

    if-nez v13, :cond_2

    const-string/jumbo v13, "com.ibm.icu.impl.URLHandler"

    invoke-static {v13}, Lcom/ibm/icu/impl/URLHandler;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    sput-object v13, Lcom/ibm/icu/impl/URLHandler;->class$com$ibm$icu$impl$URLHandler:Ljava/lang/Class;

    :goto_0
    const-string/jumbo v14, "urlhandler.props"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 37
    .local v5, "is":Ljava/io/InputStream;
    if-nez v5, :cond_0

    .line 38
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    const-string/jumbo v14, "urlhandler.props"

    invoke-virtual {v13, v14}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 41
    :cond_0
    if-eqz v5, :cond_5

    .line 42
    const/4 v13, 0x1

    new-array v10, v13, [Ljava/lang/Class;

    const/4 v14, 0x0

    sget-object v13, Lcom/ibm/icu/impl/URLHandler;->class$java$net$URL:Ljava/lang/Class;

    if-nez v13, :cond_3

    const-string/jumbo v13, "java.net.URL"

    invoke-static {v13}, Lcom/ibm/icu/impl/URLHandler;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    sput-object v13, Lcom/ibm/icu/impl/URLHandler;->class$java$net$URL:Ljava/lang/Class;

    :goto_1
    aput-object v13, v10, v14

    .line 43
    .local v10, "params":[Ljava/lang/Class;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-direct {v13, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 45
    .local v0, "br":Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .local v8, "line":Ljava/lang/String;
    move-object v4, v3

    .end local v3    # "h":Ljava/util/Map;
    .local v4, "h":Ljava/util/Map;
    :goto_2
    if-eqz v8, :cond_8

    .line 46
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 48
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_9

    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Ljava/lang/String;->charAt(I)C
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4

    move-result v13

    const/16 v14, 0x23

    if-ne v13, v14, :cond_4

    move-object v3, v4

    .line 45
    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    :cond_1
    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    move-object v4, v3

    .end local v3    # "h":Ljava/util/Map;
    .restart local v4    # "h":Ljava/util/Map;
    goto :goto_2

    .line 35
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "h":Ljava/util/Map;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v8    # "line":Ljava/lang/String;
    .end local v10    # "params":[Ljava/lang/Class;
    .restart local v3    # "h":Ljava/util/Map;
    :cond_2
    sget-object v13, Lcom/ibm/icu/impl/URLHandler;->class$com$ibm$icu$impl$URLHandler:Ljava/lang/Class;

    goto :goto_0

    .line 42
    .restart local v5    # "is":Ljava/io/InputStream;
    :cond_3
    sget-object v13, Lcom/ibm/icu/impl/URLHandler;->class$java$net$URL:Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 52
    .end local v3    # "h":Ljava/util/Map;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "h":Ljava/util/Map;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v10    # "params":[Ljava/lang/Class;
    :cond_4
    const/16 v13, 0x3d

    :try_start_3
    invoke-virtual {v8, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 54
    .local v6, "ix":I
    const/4 v13, -0x1

    if-ne v6, v13, :cond_6

    .line 55
    sget-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v13, :cond_8

    sget-object v13, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v15, "bad urlhandler line: \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string/jumbo v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    move-object v3, v4

    .line 87
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "h":Ljava/util/Map;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "ix":I
    .end local v8    # "line":Ljava/lang/String;
    .end local v10    # "params":[Ljava/lang/Class;
    .restart local v3    # "h":Ljava/util/Map;
    :cond_5
    :goto_4
    sput-object v3, Lcom/ibm/icu/impl/URLHandler;->handlers:Ljava/util/Map;

    .line 88
    return-void

    .line 59
    .end local v3    # "h":Ljava/util/Map;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "h":Ljava/util/Map;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "ix":I
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v10    # "params":[Ljava/lang/Class;
    :cond_6
    const/4 v13, 0x0

    :try_start_4
    invoke-virtual {v8, v13, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 60
    .local v7, "key":Ljava/lang/String;
    add-int/lit8 v13, v6, 0x1

    invoke-virtual {v8, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v12

    .line 63
    .local v12, "value":Ljava/lang/String;
    :try_start_5
    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 64
    .local v1, "cl":Ljava/lang/Class;
    const-string/jumbo v13, "get"

    invoke-virtual {v1, v13, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    .line 66
    .local v9, "m":Ljava/lang/reflect/Method;
    if-nez v4, :cond_7

    .line 67
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    .line 70
    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    :goto_5
    :try_start_6
    invoke-interface {v3, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 72
    :catch_0
    move-exception v2

    .line 73
    .end local v1    # "cl":Ljava/lang/Class;
    .end local v9    # "m":Ljava/lang/reflect/Method;
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    :goto_6
    :try_start_7
    sget-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v13, :cond_1

    sget-object v13, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v13, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 83
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "ix":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v8    # "line":Ljava/lang/String;
    .end local v10    # "params":[Ljava/lang/Class;
    .end local v12    # "value":Ljava/lang/String;
    :catch_1
    move-exception v11

    .line 84
    .local v11, "t":Ljava/lang/Throwable;
    :goto_7
    sget-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v13, :cond_5

    sget-object v13, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v13, v11}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_4

    .line 75
    .end local v3    # "h":Ljava/util/Map;
    .end local v11    # "t":Ljava/lang/Throwable;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "h":Ljava/util/Map;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "ix":I
    .restart local v7    # "key":Ljava/lang/String;
    .restart local v8    # "line":Ljava/lang/String;
    .restart local v10    # "params":[Ljava/lang/Class;
    .restart local v12    # "value":Ljava/lang/String;
    :catch_2
    move-exception v2

    move-object v3, v4

    .line 76
    .end local v4    # "h":Ljava/util/Map;
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    .restart local v3    # "h":Ljava/util/Map;
    :goto_8
    :try_start_8
    sget-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v13, :cond_1

    sget-object v13, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v13, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 78
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .end local v3    # "h":Ljava/util/Map;
    .restart local v4    # "h":Ljava/util/Map;
    :catch_3
    move-exception v2

    move-object v3, v4

    .line 79
    .end local v4    # "h":Ljava/util/Map;
    .local v2, "e":Ljava/lang/SecurityException;
    .restart local v3    # "h":Ljava/util/Map;
    :goto_9
    sget-boolean v13, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v13, :cond_1

    sget-object v13, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v13, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_3

    .line 83
    .end local v2    # "e":Ljava/lang/SecurityException;
    .end local v3    # "h":Ljava/util/Map;
    .end local v6    # "ix":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v12    # "value":Ljava/lang/String;
    .restart local v4    # "h":Ljava/util/Map;
    :catch_4
    move-exception v11

    move-object v3, v4

    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    goto :goto_7

    .line 78
    .restart local v1    # "cl":Ljava/lang/Class;
    .restart local v6    # "ix":I
    .restart local v7    # "key":Ljava/lang/String;
    .restart local v9    # "m":Ljava/lang/reflect/Method;
    .restart local v12    # "value":Ljava/lang/String;
    :catch_5
    move-exception v2

    goto :goto_9

    .line 75
    :catch_6
    move-exception v2

    goto :goto_8

    .line 72
    .end local v1    # "cl":Ljava/lang/Class;
    .end local v3    # "h":Ljava/util/Map;
    .end local v9    # "m":Ljava/lang/reflect/Method;
    .restart local v4    # "h":Ljava/util/Map;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    goto :goto_6

    .end local v3    # "h":Ljava/util/Map;
    .restart local v1    # "cl":Ljava/lang/Class;
    .restart local v4    # "h":Ljava/util/Map;
    .restart local v9    # "m":Ljava/lang/reflect/Method;
    :cond_7
    move-object v3, v4

    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    goto :goto_5

    .end local v1    # "cl":Ljava/lang/Class;
    .end local v3    # "h":Ljava/util/Map;
    .end local v6    # "ix":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v9    # "m":Ljava/lang/reflect/Method;
    .end local v12    # "value":Ljava/lang/String;
    .restart local v4    # "h":Ljava/util/Map;
    :cond_8
    move-object v3, v4

    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    goto :goto_4

    .end local v3    # "h":Ljava/util/Map;
    .restart local v4    # "h":Ljava/util/Map;
    :cond_9
    move-object v3, v4

    .end local v4    # "h":Ljava/util/Map;
    .restart local v3    # "h":Ljava/util/Map;
    goto/16 :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    return-void
.end method

.method static access$000()Z
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    return v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 35
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static get(Ljava/net/URL;)Lcom/ibm/icu/impl/URLHandler;
    .locals 7
    .param p0, "url"    # Ljava/net/URL;

    .prologue
    const/4 v1, 0x0

    .line 91
    if-nez p0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-object v1

    .line 95
    :cond_1
    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "protocol":Ljava/lang/String;
    sget-object v4, Lcom/ibm/icu/impl/URLHandler;->handlers:Ljava/util/Map;

    if-eqz v4, :cond_2

    .line 98
    sget-object v4, Lcom/ibm/icu/impl/URLHandler;->handlers:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    .line 100
    .local v2, "m":Ljava/lang/reflect/Method;
    if-eqz v2, :cond_2

    .line 102
    const/4 v4, 0x0

    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/URLHandler;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 104
    .local v1, "handler":Lcom/ibm/icu/impl/URLHandler;
    if-nez v1, :cond_0

    .line 120
    .end local v1    # "handler":Lcom/ibm/icu/impl/URLHandler;
    .end local v2    # "m":Ljava/lang/reflect/Method;
    :cond_2
    :goto_1
    invoke-static {p0}, Lcom/ibm/icu/impl/URLHandler;->getDefault(Ljava/net/URL;)Lcom/ibm/icu/impl/URLHandler;

    move-result-object v1

    goto :goto_0

    .line 108
    .restart local v2    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-boolean v4, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v4, :cond_2

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1

    .line 111
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-boolean v4, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v4, :cond_2

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1

    .line 114
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-boolean v4, Lcom/ibm/icu/impl/URLHandler;->DEBUG:Z

    if-eqz v4, :cond_2

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected static getDefault(Ljava/net/URL;)Lcom/ibm/icu/impl/URLHandler;
    .locals 2
    .param p0, "url"    # Ljava/net/URL;

    .prologue
    .line 124
    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "protocol":Ljava/lang/String;
    const-string/jumbo v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    new-instance v1, Lcom/ibm/icu/impl/URLHandler$FileURLHandler;

    invoke-direct {v1, p0}, Lcom/ibm/icu/impl/URLHandler$FileURLHandler;-><init>(Ljava/net/URL;)V

    .line 131
    :goto_0
    return-object v1

    .line 128
    :cond_0
    const-string/jumbo v1, "jar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    new-instance v1, Lcom/ibm/icu/impl/URLHandler$JarURLHandler;

    invoke-direct {v1, p0}, Lcom/ibm/icu/impl/URLHandler$JarURLHandler;-><init>(Ljava/net/URL;)V

    goto :goto_0

    .line 131
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public guide(Lcom/ibm/icu/impl/URLHandler$URLVisitor;Z)V
    .locals 1
    .param p1, "visitor"    # Lcom/ibm/icu/impl/URLHandler$URLVisitor;
    .param p2, "recurse"    # Z

    .prologue
    .line 234
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/impl/URLHandler;->guide(Lcom/ibm/icu/impl/URLHandler$URLVisitor;ZZ)V

    .line 235
    return-void
.end method

.method public abstract guide(Lcom/ibm/icu/impl/URLHandler$URLVisitor;ZZ)V
.end method
