.class public Lcom/ibm/icu/impl/TrieIterator;
.super Ljava/lang/Object;
.source "TrieIterator.java"

# interfaces
.implements Lcom/ibm/icu/util/RangeValueIterator;


# static fields
.field private static final BMP_INDEX_LENGTH_:I = 0x800

.field private static final DATA_BLOCK_LENGTH_:I = 0x20

.field private static final LEAD_SURROGATE_MIN_VALUE_:I = 0xd800

.field private static final TRAIL_SURROGATE_COUNT_:I = 0x400

.field private static final TRAIL_SURROGATE_INDEX_BLOCK_LENGTH_:I = 0x20

.field private static final TRAIL_SURROGATE_MIN_VALUE_:I = 0xdc00


# instance fields
.field private m_currentCodepoint_:I

.field private m_initialValue_:I

.field private m_nextBlockIndex_:I

.field private m_nextBlock_:I

.field private m_nextCodepoint_:I

.field private m_nextIndex_:I

.field private m_nextTrailIndexOffset_:I

.field private m_nextValue_:I

.field private m_trie_:Lcom/ibm/icu/impl/Trie;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/impl/Trie;)V
    .locals 2
    .param p1, "trie"    # Lcom/ibm/icu/impl/Trie;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Argument trie cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    .line 101
    iget-object v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/Trie;->getInitialValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/TrieIterator;->extract(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    .line 102
    invoke-virtual {p0}, Lcom/ibm/icu/impl/TrieIterator;->reset()V

    .line 103
    return-void
.end method

.method private final calculateNextBMPElement(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z
    .locals 6
    .param p1, "element"    # Lcom/ibm/icu/util/RangeValueIterator$Element;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 195
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 196
    .local v0, "currentBlock":I
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 197
    .local v1, "currentValue":I
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    .line 198
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 199
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 200
    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/TrieIterator;->checkBlockDetail(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 201
    iget v3, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v3, v4, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 231
    :goto_0
    return v2

    .line 207
    :cond_0
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    const/high16 v5, 0x10000

    if-ge v4, v5, :cond_3

    .line 208
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 212
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    const v5, 0xd800

    if-ne v4, v5, :cond_2

    .line 215
    const/16 v4, 0x800

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 222
    :cond_1
    :goto_1
    iput v3, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 223
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->checkBlock(II)Z

    move-result v4

    if-nez v4, :cond_0

    .line 224
    iget v3, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v3, v4, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    goto :goto_0

    .line 217
    :cond_2
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    const v5, 0xdc00

    if-ne v4, v5, :cond_1

    .line 219
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    shr-int/lit8 v4, v4, 0x5

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    goto :goto_1

    .line 229
    :cond_3
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 230
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    move v2, v3

    .line 231
    goto :goto_0
.end method

.method private final calculateNextSupplementaryElement(Lcom/ibm/icu/util/RangeValueIterator$Element;)V
    .locals 9
    .param p1, "element"    # Lcom/ibm/icu/util/RangeValueIterator$Element;

    .prologue
    const v8, 0xdc00

    const/4 v7, 0x0

    .line 253
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 254
    .local v1, "currentValue":I
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 255
    .local v0, "currentBlock":I
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 256
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 258
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v4

    if-eq v4, v8, :cond_1

    .line 262
    invoke-direct {p0}, Lcom/ibm/icu/impl/TrieIterator;->checkNullNextTrailIndex()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/TrieIterator;->checkBlockDetail(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 263
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 265
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    .line 343
    :goto_0
    return-void

    .line 269
    :cond_0
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 270
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    .line 271
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->checkTrailBlock(II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 272
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 274
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    goto :goto_0

    .line 278
    :cond_1
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v3

    .line 280
    .local v3, "nextLead":I
    :goto_1
    if-ge v3, v8, :cond_8

    .line 282
    iget-object v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v4, v4, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    shr-int/lit8 v5, v3, 0x5

    aget-char v4, v4, v5

    shl-int/lit8 v2, v4, 0x2

    .line 285
    .local v2, "leadBlock":I
    iget-object v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget v4, v4, Lcom/ibm/icu/impl/Trie;->m_dataOffset_:I

    if-ne v2, v4, :cond_3

    .line 287
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    if-eq v1, v4, :cond_2

    .line 288
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 289
    iput v7, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 290
    iput v7, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 291
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 293
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    goto :goto_0

    .line 297
    :cond_2
    add-int/lit8 v3, v3, 0x20

    .line 303
    int-to-char v4, v3

    invoke-static {v4, v8}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    goto :goto_1

    .line 308
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v4, v4, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v4, :cond_4

    .line 309
    new-instance v4, Ljava/lang/NullPointerException;

    const-string/jumbo v5, "The field DataManipulate in this Trie is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 313
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v4, v4, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    iget-object v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    and-int/lit8 v6, v3, 0x1f

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/Trie;->getValue(I)I

    move-result v5

    invoke-interface {v4, v5}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 316
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    if-gtz v4, :cond_7

    .line 318
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    if-eq v1, v4, :cond_5

    .line 319
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 320
    iput v7, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 321
    iput v7, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 322
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 324
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    goto/16 :goto_0

    .line 327
    :cond_5
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 337
    :cond_6
    add-int/lit8 v3, v3, 0x1

    .line 338
    goto :goto_1

    .line 329
    :cond_7
    iput v7, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    .line 330
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->checkTrailBlock(II)Z

    move-result v4

    if-nez v4, :cond_6

    .line 331
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    iget v5, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    .line 333
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iput v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    goto/16 :goto_0

    .line 341
    .end local v2    # "leadBlock":I
    :cond_8
    iget v4, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    const/high16 v5, 0x110000

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/ibm/icu/impl/TrieIterator;->setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V

    goto/16 :goto_0
.end method

.method private final checkBlock(II)Z
    .locals 3
    .param p1, "currentBlock"    # I
    .param p2, "currentValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 385
    iget-object v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v1, v1, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    aget-char v1, v1, v2

    shl-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 387
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    if-ne v1, p1, :cond_1

    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    sub-int/2addr v1, v2

    const/16 v2, 0x20

    if-lt v1, v2, :cond_1

    .line 391
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 407
    :cond_0
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 393
    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    if-nez v1, :cond_3

    .line 395
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    if-eq p2, v1, :cond_2

    .line 396
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    iput v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 397
    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    goto :goto_1

    .line 400
    :cond_2
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    goto :goto_0

    .line 403
    :cond_3
    invoke-direct {p0, p2}, Lcom/ibm/icu/impl/TrieIterator;->checkBlockDetail(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1
.end method

.method private final checkBlockDetail(I)Z
    .locals 3
    .param p1, "currentValue"    # I

    .prologue
    .line 359
    :goto_0
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    const/16 v1, 0x20

    if-ge v0, v1, :cond_1

    .line 360
    iget-object v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/Trie;->getValue(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/TrieIterator;->extract(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 362
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    if-eq v0, p1, :cond_0

    .line 363
    const/4 v0, 0x0

    .line 368
    :goto_1
    return v0

    .line 365
    :cond_0
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 366
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    goto :goto_0

    .line 368
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private final checkNullNextTrailIndex()Z
    .locals 5

    .prologue
    .line 451
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    if-gtz v2, :cond_1

    .line 452
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    add-int/lit16 v2, v2, 0x3ff

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 453
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v1

    .line 454
    .local v1, "nextLead":I
    iget-object v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v2, v2, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    shr-int/lit8 v3, v1, 0x5

    aget-char v2, v2, v3

    shl-int/lit8 v0, v2, 0x2

    .line 457
    .local v0, "leadBlock":I
    iget-object v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v2, v2, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    if-nez v2, :cond_0

    .line 458
    new-instance v2, Ljava/lang/NullPointerException;

    const-string/jumbo v3, "The field DataManipulate in this Trie is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 461
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v2, v2, Lcom/ibm/icu/impl/Trie;->m_dataManipulate_:Lcom/ibm/icu/impl/Trie$DataManipulate;

    iget-object v3, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    and-int/lit8 v4, v1, 0x1f

    add-int/2addr v4, v0

    invoke-virtual {v3, v4}, Lcom/ibm/icu/impl/Trie;->getValue(I)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/ibm/icu/impl/Trie$DataManipulate;->getFoldingOffset(I)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 464
    iget v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 465
    const/16 v2, 0x20

    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 466
    const/4 v2, 0x1

    .line 468
    .end local v0    # "leadBlock":I
    .end local v1    # "nextLead":I
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private final checkTrailBlock(II)Z
    .locals 3
    .param p1, "currentBlock"    # I
    .param p2, "currentValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 426
    :goto_0
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    const/16 v2, 0x20

    if-ge v1, v2, :cond_1

    .line 429
    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 431
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/TrieIterator;->checkBlock(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 437
    :goto_1
    return v0

    .line 434
    :cond_0
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    .line 435
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    goto :goto_0

    .line 437
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private final setResult(Lcom/ibm/icu/util/RangeValueIterator$Element;III)V
    .locals 0
    .param p1, "element"    # Lcom/ibm/icu/util/RangeValueIterator$Element;
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "value"    # I

    .prologue
    .line 177
    iput p2, p1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    .line 178
    iput p3, p1, Lcom/ibm/icu/util/RangeValueIterator$Element;->limit:I

    .line 179
    iput p4, p1, Lcom/ibm/icu/util/RangeValueIterator$Element;->value:I

    .line 180
    return-void
.end method


# virtual methods
.method protected extract(I)I
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 162
    return p1
.end method

.method public final next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z
    .locals 3
    .param p1, "element"    # Lcom/ibm/icu/util/RangeValueIterator$Element;

    .prologue
    const/4 v0, 0x1

    .line 119
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    const v2, 0x10ffff

    if-le v1, v2, :cond_1

    .line 120
    const/4 v0, 0x0

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    const/high16 v2, 0x10000

    if-ge v1, v2, :cond_2

    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/TrieIterator;->calculateNextBMPElement(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    :cond_2
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/TrieIterator;->calculateNextSupplementaryElement(Lcom/ibm/icu/util/RangeValueIterator$Element;)V

    goto :goto_0
.end method

.method public final reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_currentCodepoint_:I

    .line 136
    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextCodepoint_:I

    .line 137
    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextIndex_:I

    .line 138
    iget-object v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget-object v0, v0, Lcom/ibm/icu/impl/Trie;->m_index_:[C

    aget-char v0, v0, v2

    shl-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    .line 139
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    if-nez v0, :cond_0

    .line 140
    iget v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_initialValue_:I

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    .line 145
    :goto_0
    iput v2, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlockIndex_:I

    .line 146
    const/16 v0, 0x20

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextTrailIndexOffset_:I

    .line 147
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_trie_:Lcom/ibm/icu/impl/Trie;

    iget v1, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextBlock_:I

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/Trie;->getValue(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/impl/TrieIterator;->extract(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/TrieIterator;->m_nextValue_:I

    goto :goto_0
.end method
