.class public final Lcom/ibm/icu/impl/ICUData;
.super Ljava/lang/Object;
.source "ICUData.java"


# static fields
.field static class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 33
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static exists(Ljava/lang/String;)Z
    .locals 2
    .param p0, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 30
    .local v0, "i":Ljava/net/URL;
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 31
    new-instance v1, Lcom/ibm/icu/impl/ICUData$1;

    invoke-direct {v1, p0}, Lcom/ibm/icu/impl/ICUData$1;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "i":Ljava/net/URL;
    check-cast v0, Ljava/net/URL;

    .line 39
    .restart local v0    # "i":Ljava/net/URL;
    :goto_0
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 37
    :cond_0
    sget-object v1, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string/jumbo v1, "com.ibm.icu.impl.ICUData"

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUData;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    :goto_2
    invoke-virtual {v1, p0}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    goto :goto_2

    .line 39
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getRequiredStream(Ljava/lang/Class;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "root"    # Ljava/lang/Class;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 111
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public static getRequiredStream(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "loader"    # Ljava/lang/ClassLoader;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/ClassLoader;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public static getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .param p0, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 97
    sget-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.impl.ICUData"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUData;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    :goto_0
    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    goto :goto_0
.end method

.method public static getStream(Ljava/lang/Class;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "root"    # Ljava/lang/Class;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method private static getStream(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/io/InputStream;
    .locals 4
    .param p0, "root"    # Ljava/lang/Class;
    .param p1, "resourceName"    # Ljava/lang/String;
    .param p2, "required"    # Z

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 45
    .local v0, "i":Ljava/io/InputStream;
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 46
    new-instance v1, Lcom/ibm/icu/impl/ICUData$2;

    invoke-direct {v1, p0, p1}, Lcom/ibm/icu/impl/ICUData$2;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-static {v1}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "i":Ljava/io/InputStream;
    check-cast v0, Ljava/io/InputStream;

    .line 55
    .restart local v0    # "i":Ljava/io/InputStream;
    :goto_0
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 56
    new-instance v1, Ljava/util/MissingResourceException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "could not locate data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 52
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_1
    return-object v0
.end method

.method public static getStream(Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p0, "loader"    # Ljava/lang/ClassLoader;
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/ClassLoader;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method private static getStream(Ljava/lang/ClassLoader;Ljava/lang/String;Z)Ljava/io/InputStream;
    .locals 4
    .param p0, "loader"    # Ljava/lang/ClassLoader;
    .param p1, "resourceName"    # Ljava/lang/String;
    .param p2, "required"    # Z

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 63
    .local v0, "i":Ljava/io/InputStream;
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 64
    new-instance v1, Lcom/ibm/icu/impl/ICUData$3;

    invoke-direct {v1, p0, p1}, Lcom/ibm/icu/impl/ICUData$3;-><init>(Ljava/lang/ClassLoader;Ljava/lang/String;)V

    invoke-static {v1}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "i":Ljava/io/InputStream;
    check-cast v0, Ljava/io/InputStream;

    .line 72
    .restart local v0    # "i":Ljava/io/InputStream;
    :goto_0
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 73
    new-instance v1, Ljava/util/MissingResourceException;

    const-string/jumbo v2, "could not locate data"

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_1
    return-object v0
.end method

.method public static getStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .param p0, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 90
    sget-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.impl.ICUData"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUData;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/Class;Ljava/lang/String;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/ICUData;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    goto :goto_0
.end method
