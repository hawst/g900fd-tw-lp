.class public final Lcom/ibm/icu/impl/NormalizerImpl;
.super Ljava/lang/Object;
.source "NormalizerImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/NormalizerImpl$1;,
        Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;,
        Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;,
        Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;,
        Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;,
        Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;
    }
.end annotation


# static fields
.field private static final AUX_COMP_EX_MASK:I = 0x400

.field private static final AUX_COMP_EX_SHIFT:I = 0xa

.field private static final AUX_FNC_MASK:I = 0x3ff

.field private static final AUX_MAX_FNC:I = 0x400

.field private static final AUX_NFC_SKIPPABLE_F_SHIFT:I = 0xc

.field private static final AUX_NFC_SKIP_F_MASK:J = 0x1000L

.field private static final AUX_UNSAFE_MASK:I = 0x800

.field private static final AUX_UNSAFE_SHIFT:I = 0xb

.field public static final BEFORE_PRI_29:I = 0x100

.field private static final BMP_INDEX_LENGTH:I = 0x800

.field static final CANON_SET_BMP_IS_INDEX:I = 0x4000

.field static final CANON_SET_BMP_MASK:I = 0xc000

.field static final CANON_SET_BMP_TABLE_INDEX:I = 0x2

.field static final CANON_SET_INDICIES_INDEX:I = 0x0

.field static final CANON_SET_MAX_CANON_SETS:I = 0x4000

.field static final CANON_SET_START_SETS_INDEX:I = 0x1

.field static final CANON_SET_SUPP_TABLE_INDEX:I = 0x3

.field public static final CC_MASK:I = 0xff00

.field private static final CC_SHIFT:I = 0x8

.field public static final COMBINES_ANY:I = 0xc0

.field private static final COMBINES_BACK:I = 0x80

.field private static final COMBINES_FWD:I = 0x40

.field public static final COMPARE_EQUIV:I = 0x80000

.field private static final DATA_BUFFER_SIZE:I = 0x61a8

.field private static final DATA_FILE_NAME:Ljava/lang/String; = "data/icudt40b/unorm.icu"

.field private static final DECOMP_FLAG_LENGTH_HAS_CC:I = 0x80

.field private static final DECOMP_LENGTH_MASK:I = 0x7f

.field private static final EXTRA_SHIFT:I = 0x10

.field public static final HANGUL_BASE:I = 0xac00

.field public static final HANGUL_COUNT:I = 0x2ba4

.field static final IMPL:Lcom/ibm/icu/impl/NormalizerImpl;

.field static final INDEX_AUX_TRIE_SIZE:I = 0xb

.field static final INDEX_CANON_SET_COUNT:I = 0xc

.field static final INDEX_CHAR_COUNT:I = 0x1

.field static final INDEX_COMBINE_BACK_COUNT:I = 0x5

.field static final INDEX_COMBINE_BOTH_COUNT:I = 0x4

.field static final INDEX_COMBINE_DATA_COUNT:I = 0x2

.field static final INDEX_COMBINE_FWD_COUNT:I = 0x3

.field static final INDEX_FCD_TRIE_SIZE:I = 0xa

.field public static final INDEX_MIN_NFC_NO_MAYBE:I = 0x6

.field public static final INDEX_MIN_NFD_NO_MAYBE:I = 0x8

.field public static final INDEX_MIN_NFKC_NO_MAYBE:I = 0x7

.field public static final INDEX_MIN_NFKD_NO_MAYBE:I = 0x9

.field static final INDEX_TOP:I = 0x20

.field static final INDEX_TRIE_SIZE:I = 0x0

.field public static final JAMO_L_BASE:I = 0x1100

.field public static final JAMO_L_COUNT:I = 0x13

.field public static final JAMO_T_BASE:I = 0x11a7

.field public static final JAMO_T_COUNT:I = 0x1c

.field public static final JAMO_V_BASE:I = 0x1161

.field public static final JAMO_V_COUNT:I = 0x15

.field private static final JAMO_V_TOP:J = 0xfff30000L

.field private static final MAX_BUFFER_SIZE:I = 0x14

.field private static final MIN_HANGUL:J = 0xfff00000L

.field private static final MIN_SPECIAL:J = 0xfc000000L

.field public static final MIN_WITH_LEAD_CC:I = 0x300

.field private static final NX_CJK_COMPAT:I = 0x2

.field private static final NX_HANGUL:I = 0x1

.field public static final OPTIONS_COMPAT:I = 0x1000

.field public static final OPTIONS_COMPOSE_CONTIGUOUS:I = 0x2000

.field private static final OPTIONS_NX_MASK:I = 0x1f

.field public static final OPTIONS_SETS_MASK:I = 0xff

.field private static final OPTIONS_UNICODE_MASK:I = 0xe0

.field public static final QC_ANY_MAYBE:I = 0x30

.field public static final QC_ANY_NO:I = 0xf

.field public static final QC_MASK:I = 0x3f

.field public static final QC_MAYBE:I = 0x10

.field public static final QC_NFC:I = 0x11

.field public static final QC_NFD:I = 0x4

.field public static final QC_NFKC:I = 0x22

.field public static final QC_NFKD:I = 0x8

.field static final SET_INDEX_CANON_BMP_TABLE_LENGTH:I = 0x1

.field static final SET_INDEX_CANON_SETS_LENGTH:I = 0x0

.field static final SET_INDEX_CANON_SUPP_TABLE_LENGTH:I = 0x2

.field static final SET_INDEX_TOP:I = 0x20

.field private static final SURROGATES_TOP:J = 0xfff00000L

.field private static final SURROGATE_BLOCK_BITS:I = 0x5

.field static final UNSIGNED_BYTE_MASK:I = 0xff

.field static final UNSIGNED_INT_MASK:J = 0xffffffffL

.field private static auxTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;

.field private static canonStartSets:[Ljava/lang/Object;

.field private static combiningTable:[C

.field private static extraData:[C

.field private static fcdTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;

.field private static indexes:[I

.field private static isDataLoaded:Z

.field private static isFormatVersion_2_1:Z

.field private static isFormatVersion_2_2:Z

.field private static normTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;

.field private static final nxCache:[Lcom/ibm/icu/text/UnicodeSet;

.field private static unicodeVersion:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 35
    :try_start_0
    new-instance v1, Lcom/ibm/icu/impl/NormalizerImpl;

    invoke-direct {v1}, Lcom/ibm/icu/impl/NormalizerImpl;-><init>()V

    sput-object v1, Lcom/ibm/icu/impl/NormalizerImpl;->IMPL:Lcom/ibm/icu/impl/NormalizerImpl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3680
    const/16 v1, 0x100

    new-array v1, v1, [Lcom/ibm/icu/text/UnicodeSet;

    sput-object v1, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    sget-boolean v4, Lcom/ibm/icu/impl/NormalizerImpl;->isDataLoaded:Z

    if-nez v4, :cond_3

    .line 286
    const-string/jumbo v4, "data/icudt40b/unorm.icu"

    invoke-static {v4}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v11

    .line 287
    .local v11, "i":Ljava/io/InputStream;
    new-instance v7, Ljava/io/BufferedInputStream;

    const/16 v4, 0x61a8

    invoke-direct {v7, v11, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 288
    .local v7, "b":Ljava/io/BufferedInputStream;
    new-instance v0, Lcom/ibm/icu/impl/NormalizerDataReader;

    invoke-direct {v0, v7}, Lcom/ibm/icu/impl/NormalizerDataReader;-><init>(Ljava/io/InputStream;)V

    .line 291
    .local v0, "reader":Lcom/ibm/icu/impl/NormalizerDataReader;
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/NormalizerDataReader;->readIndexes(I)[I

    move-result-object v4

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    .line 293
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    aget v4, v4, v12

    new-array v1, v4, [B

    .line 295
    .local v1, "normBytes":[B
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    aget v8, v4, v14

    .line 296
    .local v8, "combiningTableTop":I
    new-array v4, v8, [C

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->combiningTable:[C

    .line 298
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    aget v9, v4, v13

    .line 299
    .local v9, "extraDataTop":I
    new-array v4, v9, [C

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    .line 301
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v5, 0xa

    aget v4, v4, v5

    new-array v2, v4, [B

    .line 302
    .local v2, "fcdBytes":[B
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v5, 0xb

    aget v4, v4, v5

    new-array v3, v4, [B

    .line 303
    .local v3, "auxBytes":[B
    const/16 v4, 0x4000

    new-array v4, v4, [Ljava/lang/Object;

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    .line 305
    new-instance v4, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;

    invoke-direct {v4}, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;-><init>()V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->fcdTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;

    .line 306
    new-instance v4, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;

    invoke-direct {v4}, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;-><init>()V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->normTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;

    .line 307
    new-instance v4, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;

    invoke-direct {v4}, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;-><init>()V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->auxTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;

    .line 310
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    sget-object v5, Lcom/ibm/icu/impl/NormalizerImpl;->combiningTable:[C

    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerDataReader;->read([B[B[B[C[C[Ljava/lang/Object;)V

    .line 313
    new-instance v4, Lcom/ibm/icu/impl/IntTrie;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->normTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;

    invoke-direct {v4, v5, v6}, Lcom/ibm/icu/impl/IntTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    .line 314
    new-instance v4, Lcom/ibm/icu/impl/CharTrie;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->fcdTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;

    invoke-direct {v4, v5, v6}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    .line 315
    new-instance v4, Lcom/ibm/icu/impl/CharTrie;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->auxTrieImpl:Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;

    invoke-direct {v4, v5, v6}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    .line 319
    sput-boolean v13, Lcom/ibm/icu/impl/NormalizerImpl;->isDataLoaded:Z

    .line 322
    invoke-virtual {v0}, Lcom/ibm/icu/impl/NormalizerDataReader;->getDataFormatVersion()[B

    move-result-object v10

    .line 324
    .local v10, "formatVersion":[B
    aget-byte v4, v10, v12

    if-gt v4, v14, :cond_0

    aget-byte v4, v10, v12

    if-ne v4, v14, :cond_4

    aget-byte v4, v10, v13

    if-lt v4, v13, :cond_4

    :cond_0
    move v4, v13

    :goto_0
    sput-boolean v4, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_1:Z

    .line 328
    aget-byte v4, v10, v12

    if-gt v4, v14, :cond_1

    aget-byte v4, v10, v12

    if-ne v4, v14, :cond_2

    aget-byte v4, v10, v13

    if-lt v4, v14, :cond_2

    :cond_1
    move v12, v13

    :cond_2
    sput-boolean v12, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_2:Z

    .line 332
    invoke-virtual {v0}, Lcom/ibm/icu/impl/NormalizerDataReader;->getUnicodeVersion()[B

    move-result-object v4

    sput-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->unicodeVersion:[B

    .line 333
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 335
    .end local v0    # "reader":Lcom/ibm/icu/impl/NormalizerDataReader;
    .end local v1    # "normBytes":[B
    .end local v2    # "fcdBytes":[B
    .end local v3    # "auxBytes":[B
    .end local v7    # "b":Ljava/io/BufferedInputStream;
    .end local v8    # "combiningTableTop":I
    .end local v9    # "extraDataTop":I
    .end local v10    # "formatVersion":[B
    .end local v11    # "i":Ljava/io/InputStream;
    :cond_3
    return-void

    .restart local v0    # "reader":Lcom/ibm/icu/impl/NormalizerDataReader;
    .restart local v1    # "normBytes":[B
    .restart local v2    # "fcdBytes":[B
    .restart local v3    # "auxBytes":[B
    .restart local v7    # "b":Ljava/io/BufferedInputStream;
    .restart local v8    # "combiningTableTop":I
    .restart local v9    # "extraDataTop":I
    .restart local v10    # "formatVersion":[B
    .restart local v11    # "i":Ljava/io/InputStream;
    :cond_4
    move v4, v12

    .line 324
    goto :goto_0
.end method

.method public static addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 9
    .param p0, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const v8, 0xd7a4

    .line 2815
    new-instance v5, Lcom/ibm/icu/impl/TrieIterator;

    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    invoke-direct {v5, v7}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 2816
    .local v5, "normIter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v6, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v6}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 2818
    .local v6, "normResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2819
    iget v7, v6, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 2823
    :cond_0
    new-instance v3, Lcom/ibm/icu/impl/TrieIterator;

    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v3, v7}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 2824
    .local v3, "fcdIter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v4, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v4}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 2826
    .local v4, "fcdResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_1
    invoke-virtual {v3, v4}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2827
    iget v7, v4, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1

    .line 2830
    :cond_1
    sget-boolean v7, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_1:Z

    if-eqz v7, :cond_2

    .line 2832
    new-instance v0, Lcom/ibm/icu/impl/TrieIterator;

    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {v0, v7}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 2833
    .local v0, "auxIter":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v1, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v1}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 2834
    .local v1, "auxResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_2
    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2835
    iget v7, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_2

    .line 2839
    .end local v0    # "auxIter":Lcom/ibm/icu/impl/TrieIterator;
    .end local v1    # "auxResult":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :cond_2
    const v2, 0xac00

    .local v2, "c":I
    :goto_3
    if-ge v2, v8, :cond_3

    .line 2840
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 2841
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 2839
    add-int/lit8 v2, v2, 0x1c

    goto :goto_3

    .line 2843
    :cond_3
    invoke-virtual {p0, v8}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 2844
    return-object p0
.end method

.method public static checkFCD([CIILcom/ibm/icu/text/UnicodeSet;)Z
    .locals 10
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 852
    const/4 v7, 0x0

    .line 853
    .local v7, "prevCC":I
    move v4, p1

    .local v4, "i":I
    move v6, p2

    .local v6, "length":I
    move v5, v4

    .line 857
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    if-ne v5, v6, :cond_0

    .line 858
    const/4 v8, 0x1

    move v4, v5

    .line 912
    .end local v5    # "i":I
    .restart local v4    # "i":I
    :goto_1
    return v8

    .line 859
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :cond_0
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-char v0, p0, v5

    .local v0, "c":C
    const/16 v8, 0x300

    if-ge v0, v8, :cond_1

    .line 860
    neg-int v7, v0

    move v5, v4

    .line 861
    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .end local v5    # "i":I
    .restart local v4    # "i":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v3

    .local v3, "fcd16":C
    if-nez v3, :cond_2

    .line 862
    const/4 v7, 0x0

    move v5, v4

    .line 863
    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 869
    .end local v5    # "i":I
    .restart local v4    # "i":I
    :cond_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 871
    if-eq v4, v6, :cond_3

    aget-char v1, p0, v4

    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 872
    add-int/lit8 v4, v4, 0x1

    .line 873
    invoke-static {v3, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v3

    .line 882
    :goto_2
    invoke-static {p3, v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 883
    const/4 v7, 0x0

    move v5, v4

    .line 884
    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 875
    .end local v1    # "c2":C
    .end local v5    # "i":I
    .restart local v4    # "i":I
    :cond_3
    const/4 v1, 0x0

    .line 876
    .restart local v1    # "c2":C
    const/4 v3, 0x0

    .line 878
    goto :goto_2

    .line 879
    .end local v1    # "c2":C
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "c2":C
    goto :goto_2

    .line 895
    :cond_5
    shr-int/lit8 v2, v3, 0x8

    .line 896
    .local v2, "cc":I
    if-eqz v2, :cond_8

    .line 897
    if-gez v7, :cond_6

    .line 901
    neg-int v8, v7

    invoke-static {p3, v8}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v8

    if-nez v8, :cond_7

    .line 902
    sget-object v8, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    neg-int v9, v7

    int-to-char v9, v9

    invoke-virtual {v8, v9}, Lcom/ibm/icu/impl/CharTrie;->getBMPValue(C)C

    move-result v8

    and-int/lit16 v7, v8, 0xff

    .line 911
    :cond_6
    :goto_3
    if-ge v2, v7, :cond_8

    .line 912
    const/4 v8, 0x0

    goto :goto_1

    .line 906
    :cond_7
    const/4 v7, 0x0

    goto :goto_3

    .line 915
    :cond_8
    and-int/lit16 v7, v3, 0xff

    move v5, v4

    .line 916
    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0
.end method

.method public static cmpEquivFold(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 7
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 3067
    const/high16 v0, 0x10000

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    .line 3068
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpSimpleEquivFold(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 3071
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move v4, v1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpEquivFold([CII[CIII)I

    move-result v0

    goto :goto_0
.end method

.method public static cmpEquivFold([CII[CIII)I
    .locals 28
    .param p0, "s1"    # [C
    .param p1, "s1Start"    # I
    .param p2, "s1Limit"    # I
    .param p3, "s2"    # [C
    .param p4, "s2Start"    # I
    .param p5, "s2Limit"    # I
    .param p6, "options"    # I

    .prologue
    .line 3166
    move-object/from16 v6, p0

    .line 3167
    .local v6, "cSource1":[C
    move-object/from16 v7, p3

    .line 3172
    .local v7, "cSource2":[C
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    move-object/from16 v21, v0

    const/16 v25, 0x0

    new-instance v26, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    aput-object v26, v21, v25

    const/16 v25, 0x1

    new-instance v26, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    aput-object v26, v21, v25

    .line 3176
    .local v21, "stack1":[Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    move-object/from16 v22, v0

    const/16 v25, 0x0

    new-instance v26, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    aput-object v26, v22, v25

    const/16 v25, 0x1

    new-instance v26, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;

    const/16 v27, 0x0

    invoke-direct/range {v26 .. v27}, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    aput-object v26, v22, v25

    .line 3182
    .local v22, "stack2":[Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;
    const/16 v25, 0x8

    move/from16 v0, v25

    new-array v10, v0, [C

    .line 3183
    .local v10, "decomp1":[C
    const/16 v25, 0x8

    move/from16 v0, v25

    new-array v11, v0, [C

    .line 3186
    .local v11, "decomp2":[C
    const/16 v25, 0x20

    move/from16 v0, v25

    new-array v12, v0, [C

    .line 3187
    .local v12, "fold1":[C
    const/16 v25, 0x20

    move/from16 v0, v25

    new-array v13, v0, [C

    .line 3205
    .local v13, "fold2":[C
    move/from16 v23, p1

    .line 3206
    .local v23, "start1":I
    move/from16 v19, p2

    .line 3208
    .local v19, "limit1":I
    move/from16 v24, p4

    .line 3209
    .local v24, "start2":I
    move/from16 v20, p5

    .line 3211
    .local v20, "limit2":I
    const/16 v17, 0x0

    .local v17, "level2":I
    move/from16 v15, v17

    .line 3212
    .local v15, "level1":I
    const/4 v5, -0x1

    .local v5, "c2":I
    move v4, v5

    .line 3213
    .local v4, "c1":I
    const/4 v9, -0x1

    .local v9, "cp2":I
    move v8, v9

    .line 3219
    .local v8, "cp1":I
    :goto_0
    if-gez v4, :cond_0

    .line 3222
    :goto_1
    move/from16 v0, p1

    move/from16 v1, v19

    if-lt v0, v1, :cond_2

    .line 3223
    if-nez v15, :cond_3

    .line 3224
    const/4 v4, -0x1

    .line 3244
    :cond_0
    :goto_2
    if-gez v5, :cond_1

    .line 3247
    :goto_3
    move/from16 v0, p4

    move/from16 v1, v20

    if-lt v0, v1, :cond_4

    .line 3248
    if-nez v17, :cond_5

    .line 3249
    const/4 v5, -0x1

    .line 3272
    :cond_1
    :goto_4
    if-ne v4, v5, :cond_7

    .line 3273
    if-gez v4, :cond_6

    .line 3274
    const/16 v25, 0x0

    .line 3541
    :goto_5
    return v25

    .line 3228
    :cond_2
    aget-char v4, v6, p1

    .line 3229
    add-int/lit8 p1, p1, 0x1

    .line 3230
    goto :goto_2

    .line 3235
    :cond_3
    add-int/lit8 v15, v15, -0x1

    .line 3236
    aget-object v25, v21, v15

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    move/from16 v23, v0

    .line 3237
    const/16 v25, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_3

    .line 3238
    aget-object v25, v21, v15

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    move/from16 p1, v0

    .line 3239
    aget-object v25, v21, v15

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    move/from16 v19, v0

    .line 3240
    aget-object v25, v21, v15

    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3241
    goto :goto_1

    .line 3253
    :cond_4
    aget-char v5, v7, p4

    .line 3254
    add-int/lit8 p4, p4, 0x1

    .line 3255
    goto :goto_4

    .line 3260
    :cond_5
    add-int/lit8 v17, v17, -0x1

    .line 3261
    aget-object v25, v22, v17

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    move/from16 v24, v0

    .line 3262
    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_5

    .line 3263
    aget-object v25, v22, v17

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    move/from16 p4, v0

    .line 3264
    aget-object v25, v22, v17

    move-object/from16 v0, v25

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    move/from16 v20, v0

    .line 3265
    aget-object v25, v22, v17

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3266
    goto :goto_3

    .line 3276
    :cond_6
    const/4 v5, -0x1

    move v4, v5

    .line 3277
    goto :goto_0

    .line 3278
    :cond_7
    if-gez v4, :cond_8

    .line 3279
    const/16 v25, -0x1

    goto :goto_5

    .line 3280
    :cond_8
    if-gez v5, :cond_9

    .line 3281
    const/16 v25, 0x1

    goto :goto_5

    .line 3287
    :cond_9
    move v8, v4

    .line 3288
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 3291
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 3292
    move/from16 v0, p1

    move/from16 v1, v19

    if-eq v0, v1, :cond_a

    aget-char v3, v6, p1

    .local v3, "c":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 3296
    int-to-char v0, v4

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-static {v0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v8

    .line 3306
    .end local v3    # "c":C
    :cond_a
    :goto_6
    move v9, v5

    .line 3307
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 3310
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 3311
    move/from16 v0, p4

    move/from16 v1, v20

    if-eq v0, v1, :cond_b

    aget-char v3, v7, p4

    .restart local v3    # "c":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 3315
    int-to-char v0, v5

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-static {v0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v9

    .line 3328
    .end local v3    # "c":C
    :cond_b
    :goto_7
    const/16 v25, 0x2

    move/from16 v0, v25

    if-ge v15, v0, :cond_10

    const/high16 v25, 0x10000

    and-int v25, v25, p6

    if-eqz v25, :cond_10

    const/16 v25, 0x0

    const/16 v26, 0x20

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, p6

    invoke-static {v8, v12, v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->foldCase(I[CIII)I

    move-result v14

    .local v14, "length":I
    if-ltz v14, :cond_10

    .line 3332
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 3333
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 3336
    add-int/lit8 p1, p1, 0x1

    .line 3349
    :cond_c
    :goto_8
    const/16 v25, 0x0

    aget-object v25, v21, v25

    move/from16 v0, v23

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    .line 3350
    const/16 v25, 0x0

    aget-object v25, v21, v25

    move/from16 v0, p1

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    .line 3351
    const/16 v25, 0x0

    aget-object v25, v21, v25

    move/from16 v0, v19

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    .line 3352
    const/16 v25, 0x0

    aget-object v25, v21, v25

    move-object/from16 v0, v25

    iput-object v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3353
    add-int/lit8 v15, v15, 0x1

    .line 3355
    move-object v6, v12

    .line 3356
    const/16 p1, 0x0

    move/from16 v23, p1

    .line 3357
    move/from16 v19, v14

    .line 3360
    const/4 v4, -0x1

    .line 3361
    goto/16 :goto_0

    .line 3299
    .end local v14    # "length":I
    :cond_d
    add-int/lit8 v25, p1, -0x2

    move/from16 v0, v23

    move/from16 v1, v25

    if-gt v0, v1, :cond_a

    add-int/lit8 v25, p1, -0x2

    aget-char v3, v6, v25

    .restart local v3    # "c":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 3302
    int-to-char v0, v4

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-static {v3, v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v8

    goto/16 :goto_6

    .line 3318
    .end local v3    # "c":C
    :cond_e
    add-int/lit8 v25, p4, -0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-gt v0, v1, :cond_b

    add-int/lit8 v25, p4, -0x2

    aget-char v3, v7, v25

    .restart local v3    # "c":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 3321
    int-to-char v0, v5

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-static {v3, v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v9

    goto/16 :goto_7

    .line 3343
    .end local v3    # "c":C
    .restart local v14    # "length":I
    :cond_f
    add-int/lit8 p4, p4, -0x1

    .line 3344
    add-int/lit8 v25, p4, -0x1

    aget-char v5, v7, v25

    goto :goto_8

    .line 3364
    .end local v14    # "length":I
    :cond_10
    const/16 v25, 0x2

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_13

    const/high16 v25, 0x10000

    and-int v25, v25, p6

    if-eqz v25, :cond_13

    const/16 v25, 0x0

    const/16 v26, 0x20

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, p6

    invoke-static {v9, v13, v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->foldCase(I[CIII)I

    move-result v14

    .restart local v14    # "length":I
    if-ltz v14, :cond_13

    .line 3368
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 3369
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 3372
    add-int/lit8 p4, p4, 0x1

    .line 3385
    :cond_11
    :goto_9
    const/16 v25, 0x0

    aget-object v25, v22, v25

    move/from16 v0, v24

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    .line 3386
    const/16 v25, 0x0

    aget-object v25, v22, v25

    move/from16 v0, p4

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    .line 3387
    const/16 v25, 0x0

    aget-object v25, v22, v25

    move/from16 v0, v20

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    .line 3388
    const/16 v25, 0x0

    aget-object v25, v22, v25

    move-object/from16 v0, v25

    iput-object v7, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3389
    add-int/lit8 v17, v17, 0x1

    .line 3391
    move-object v7, v13

    .line 3392
    const/16 p4, 0x0

    move/from16 v24, p4

    .line 3393
    move/from16 v20, v14

    .line 3396
    const/4 v5, -0x1

    .line 3397
    goto/16 :goto_0

    .line 3379
    :cond_12
    add-int/lit8 p1, p1, -0x1

    .line 3380
    add-int/lit8 v25, p1, -0x1

    aget-char v4, v6, v25

    goto :goto_9

    .line 3400
    .end local v14    # "length":I
    :cond_13
    const/16 v25, 0x2

    move/from16 v0, v25

    if-ge v15, v0, :cond_17

    const/high16 v25, 0x80000

    and-int v25, v25, p6

    if-eqz v25, :cond_17

    invoke-static {v8, v10}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(I[C)I

    move-result v14

    .restart local v14    # "length":I
    if-eqz v14, :cond_17

    .line 3404
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 3405
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 3408
    add-int/lit8 p1, p1, 0x1

    .line 3421
    :cond_14
    :goto_a
    aget-object v25, v21, v15

    move/from16 v0, v23

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    .line 3422
    aget-object v25, v21, v15

    move/from16 v0, p1

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    .line 3423
    aget-object v25, v21, v15

    move/from16 v0, v19

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    .line 3424
    aget-object v25, v21, v15

    move-object/from16 v0, v25

    iput-object v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3425
    add-int/lit8 v15, v15, 0x1

    .line 3428
    move-object v6, v10

    .line 3429
    const/16 p1, 0x0

    move/from16 v23, p1

    .line 3430
    move/from16 v19, v14

    .line 3433
    const/16 v25, 0x2

    move/from16 v0, v25

    if-ge v15, v0, :cond_15

    .line 3434
    add-int/lit8 v16, v15, 0x1

    .end local v15    # "level1":I
    .local v16, "level1":I
    aget-object v25, v21, v15

    const/16 v26, -0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    move/from16 v15, v16

    .line 3437
    .end local v16    # "level1":I
    .restart local v15    # "level1":I
    :cond_15
    const/4 v4, -0x1

    .line 3438
    goto/16 :goto_0

    .line 3415
    :cond_16
    add-int/lit8 p4, p4, -0x1

    .line 3416
    add-int/lit8 v25, p4, -0x1

    aget-char v5, v7, v25

    goto :goto_a

    .line 3441
    .end local v14    # "length":I
    :cond_17
    const/16 v25, 0x2

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_1b

    const/high16 v25, 0x80000

    and-int v25, v25, p6

    if-eqz v25, :cond_1b

    invoke-static {v9, v11}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(I[C)I

    move-result v14

    .restart local v14    # "length":I
    if-eqz v14, :cond_1b

    .line 3445
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_18

    .line 3446
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_1a

    .line 3449
    add-int/lit8 p4, p4, 0x1

    .line 3462
    :cond_18
    :goto_b
    aget-object v25, v22, v17

    move/from16 v0, v24

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    .line 3463
    aget-object v25, v22, v17

    move/from16 v0, p4

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->s:I

    .line 3464
    aget-object v25, v22, v17

    move/from16 v0, v20

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->limit:I

    .line 3465
    aget-object v25, v22, v17

    move-object/from16 v0, v25

    iput-object v7, v0, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->source:[C

    .line 3466
    add-int/lit8 v17, v17, 0x1

    .line 3469
    move-object v7, v11

    .line 3470
    const/16 p4, 0x0

    move/from16 v24, p4

    .line 3471
    move/from16 v20, v14

    .line 3474
    const/16 v25, 0x2

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_19

    .line 3475
    add-int/lit8 v18, v17, 0x1

    .end local v17    # "level2":I
    .local v18, "level2":I
    aget-object v25, v22, v17

    const/16 v26, -0x1

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$CmpEquivLevel;->start:I

    move/from16 v17, v18

    .line 3479
    .end local v18    # "level2":I
    .restart local v17    # "level2":I
    :cond_19
    const/4 v5, -0x1

    .line 3480
    goto/16 :goto_0

    .line 3456
    :cond_1a
    add-int/lit8 p1, p1, -0x1

    .line 3457
    add-int/lit8 v25, p1, -0x1

    aget-char v4, v6, v25

    goto :goto_b

    .line 3499
    .end local v14    # "length":I
    :cond_1b
    const v25, 0xd800

    move/from16 v0, v25

    if-lt v4, v0, :cond_1f

    const v25, 0xd800

    move/from16 v0, v25

    if-lt v5, v0, :cond_1f

    const v25, 0x8000

    and-int v25, v25, p6

    if-eqz v25, :cond_1f

    .line 3504
    const v25, 0xdbff

    move/from16 v0, v25

    if-gt v4, v0, :cond_1c

    move/from16 v0, p1

    move/from16 v1, v19

    if-eq v0, v1, :cond_1c

    aget-char v25, v6, p1

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-nez v25, :cond_1d

    :cond_1c
    int-to-char v0, v4

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_20

    add-int/lit8 v25, p1, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_20

    add-int/lit8 v25, p1, -0x2

    aget-char v25, v6, v25

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_20

    .line 3522
    :cond_1d
    :goto_c
    const v25, 0xdbff

    move/from16 v0, v25

    if-gt v5, v0, :cond_1e

    move/from16 v0, p4

    move/from16 v1, v20

    if-eq v0, v1, :cond_1e

    aget-char v25, v7, p4

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-nez v25, :cond_1f

    :cond_1e
    int-to-char v0, v5

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_21

    add-int/lit8 v25, p4, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_21

    add-int/lit8 v25, p4, -0x2

    aget-char v25, v7, v25

    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v25

    if-eqz v25, :cond_21

    .line 3541
    :cond_1f
    :goto_d
    sub-int v25, v4, v5

    goto/16 :goto_5

    .line 3519
    :cond_20
    add-int/lit16 v4, v4, -0x2800

    goto :goto_c

    .line 3537
    :cond_21
    add-int/lit16 v5, v5, -0x2800

    goto :goto_d
.end method

.method private static cmpSimpleEquivFold(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 12
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 3079
    const/4 v7, 0x0

    .line 3080
    .local v7, "cmp":I
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v4, 0x0

    .line 3081
    .local v4, "j":I
    const/4 v8, 0x0

    .line 3082
    .local v8, "foldS1":Ljava/lang/String;
    const/4 v9, 0x0

    .line 3083
    .local v9, "foldS2":Ljava/lang/String;
    const/4 v10, 0x1

    .line 3084
    .local v10, "offset1":I
    const/4 v11, 0x1

    .line 3085
    .local v11, "offset2":I
    :goto_0
    add-int v0, v1, v10

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v0, v2, :cond_b

    add-int v0, v4, v11

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v0, v2, :cond_b

    .line 3086
    if-nez v7, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v0, v2, :cond_a

    .line 3087
    :cond_0
    if-lez v1, :cond_2

    if-lez v4, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3091
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .end local v1    # "i":I
    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .end local v4    # "j":I
    move-result v5

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpEquivFold([CII[CIII)I

    move-result v0

    .line 3144
    :goto_1
    return v0

    .line 3095
    .restart local v1    # "i":I
    .restart local v4    # "j":I
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3097
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpEquivFold([CII[CIII)I

    move-result v0

    goto :goto_1

    .line 3102
    :cond_4
    if-lez v10, :cond_5

    .line 3103
    add-int v0, v1, v10

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 3105
    :cond_5
    if-lez v11, :cond_6

    .line 3106
    add-int v0, v4, v11

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    .line 3108
    :cond_6
    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    .line 3109
    if-nez v7, :cond_7

    .line 3110
    invoke-static {v1, v10}, Lcom/ibm/icu/impl/NormalizerImpl;->moveToNext(II)I

    move-result v1

    .line 3111
    invoke-static {v4, v11}, Lcom/ibm/icu/impl/NormalizerImpl;->moveToNext(II)I

    move-result v4

    .line 3112
    const/4 v11, 0x1

    move v10, v11

    .line 3113
    goto/16 :goto_0

    .line 3116
    :cond_7
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v0, v2, :cond_8

    move v0, v7

    .line 3117
    goto :goto_1

    .line 3119
    :cond_8
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_9

    .line 3120
    add-int/lit8 v10, v10, 0x1

    .line 3121
    const/4 v11, 0x0

    .line 3122
    goto/16 :goto_0

    .line 3124
    :cond_9
    const/4 v10, 0x0

    .line 3125
    add-int/lit8 v11, v11, 0x1

    .line 3127
    goto/16 :goto_0

    .line 3129
    :cond_a
    add-int/lit8 v1, v1, 0x1

    .line 3130
    add-int/lit8 v4, v4, 0x1

    .line 3131
    goto/16 :goto_0

    .line 3132
    :cond_b
    if-eqz v7, :cond_c

    move v0, v7

    .line 3133
    goto :goto_1

    .line 3135
    :cond_c
    add-int v0, v1, v10

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v0, v2, :cond_e

    .line 3136
    add-int v0, v4, v11

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v0, v2, :cond_d

    .line 3137
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 3140
    :cond_d
    const/4 v0, -0x1

    goto/16 :goto_1

    .line 3144
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_1
.end method

.method private static combine([CII[I)I
    .locals 12
    .param p0, "table"    # [C
    .param p1, "tableStart"    # I
    .param p2, "combineBackIndex"    # I
    .param p3, "outValues"    # [I

    .prologue
    const-wide v10, 0xffffffffL

    const v8, 0x8000

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1447
    array-length v4, p3

    if-ge v4, v5, :cond_1

    .line 1448
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 1457
    .end local p1    # "tableStart":I
    .local v0, "key":I
    .local v1, "tableStart":I
    :cond_0
    aget-char v4, p0, v1

    and-int/2addr v4, v8

    if-eqz v4, :cond_2

    move v4, v5

    :goto_0
    add-int p1, v1, v4

    .line 1453
    .end local v0    # "key":I
    .end local v1    # "tableStart":I
    .restart local p1    # "tableStart":I
    :cond_1
    add-int/lit8 v1, p1, 0x1

    .end local p1    # "tableStart":I
    .restart local v1    # "tableStart":I
    aget-char v0, p0, p1

    .line 1454
    .restart local v0    # "key":I
    if-lt v0, p2, :cond_0

    .line 1461
    and-int/lit16 v4, v0, 0x7fff

    if-ne v4, p2, :cond_5

    .line 1463
    aget-char v2, p0, v1

    .line 1466
    .local v2, "value":I
    and-int/lit16 v4, v2, 0x2000

    add-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    and-long/2addr v4, v10

    long-to-int v0, v4

    .line 1471
    and-int v4, v2, v8

    if-eqz v4, :cond_4

    .line 1472
    and-int/lit16 v4, v2, 0x4000

    if-eqz v4, :cond_3

    .line 1474
    and-int/lit16 v4, v2, 0x3ff

    const v5, 0xd800

    or-int/2addr v4, v5

    int-to-long v4, v4

    and-long/2addr v4, v10

    long-to-int v2, v4

    .line 1475
    add-int/lit8 v4, v1, 0x1

    aget-char v3, p0, v4

    .line 1486
    .local v3, "value2":I
    :goto_1
    aput v2, p3, v7

    .line 1487
    aput v3, p3, v6

    move v4, v0

    .line 1491
    .end local v2    # "value":I
    .end local v3    # "value2":I
    :goto_2
    return v4

    :cond_2
    move v4, v6

    .line 1457
    goto :goto_0

    .line 1478
    .restart local v2    # "value":I
    :cond_3
    add-int/lit8 v4, v1, 0x1

    aget-char v2, p0, v4

    .line 1479
    const/4 v3, 0x0

    .line 1481
    .restart local v3    # "value2":I
    goto :goto_1

    .line 1483
    .end local v3    # "value2":I
    :cond_4
    and-int/lit16 v2, v2, 0x1fff

    .line 1484
    const/4 v3, 0x0

    .restart local v3    # "value2":I
    goto :goto_1

    .end local v2    # "value":I
    .end local v3    # "value2":I
    :cond_5
    move v4, v7

    .line 1491
    goto :goto_2
.end method

.method public static compose([CII[CIIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 40
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "options"    # I
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1972
    const/4 v6, 0x1

    new-array v11, v6, [I

    .line 1973
    .local v11, "ioIndex":[I
    move/from16 v23, p4

    .line 1974
    .local v23, "destIndex":I
    move/from16 v13, p1

    .line 1976
    .local v13, "srcIndex":I
    move/from16 v0, p6

    and-int/lit16 v6, v0, 0x1000

    if-eqz v6, :cond_1

    .line 1977
    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/4 v10, 0x7

    aget v6, v6, v10

    int-to-char v0, v6

    move/from16 v17, v0

    .line 1978
    .local v17, "minNoMaybe":C
    const/16 v35, 0x22

    .line 2007
    .local v35, "qcMask":I
    :goto_0
    move/from16 v19, v13

    .line 2009
    .local v19, "prevStarter":I
    const v6, 0xff00

    or-int v27, v6, v35

    .line 2010
    .local v27, "ccOrQCMask":I
    const/16 v21, 0x0

    .line 2011
    .local v21, "reorderStartIndex":I
    const/16 v33, 0x0

    .line 2014
    .local v33, "prevCC":I
    const-wide/16 v8, 0x0

    .line 2015
    .local v8, "norm32":J
    const/4 v7, 0x0

    .line 2020
    .local v7, "c":C
    :goto_1
    move/from16 v34, v13

    .line 2022
    .local v34, "prevSrc":I
    :goto_2
    move/from16 v0, p2

    if-eq v13, v0, :cond_2

    aget-char v7, p0, v13

    move/from16 v0, v17

    if-lt v7, v0, :cond_0

    invoke-static {v7}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v8

    move/from16 v0, v27

    int-to-long v14, v0

    and-long/2addr v14, v8

    const-wide/16 v38, 0x0

    cmp-long v6, v14, v38

    if-nez v6, :cond_2

    .line 2024
    :cond_0
    const/16 v33, 0x0

    .line 2025
    add-int/lit8 v13, v13, 0x1

    .line 2026
    goto :goto_2

    .line 1980
    .end local v7    # "c":C
    .end local v8    # "norm32":J
    .end local v17    # "minNoMaybe":C
    .end local v19    # "prevStarter":I
    .end local v21    # "reorderStartIndex":I
    .end local v27    # "ccOrQCMask":I
    .end local v33    # "prevCC":I
    .end local v34    # "prevSrc":I
    .end local v35    # "qcMask":I
    :cond_1
    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/4 v10, 0x6

    aget v6, v6, v10

    int-to-char v0, v6

    move/from16 v17, v0

    .line 1981
    .restart local v17    # "minNoMaybe":C
    const/16 v35, 0x11

    .restart local v35    # "qcMask":I
    goto :goto_0

    .line 2030
    .restart local v7    # "c":C
    .restart local v8    # "norm32":J
    .restart local v19    # "prevStarter":I
    .restart local v21    # "reorderStartIndex":I
    .restart local v27    # "ccOrQCMask":I
    .restart local v33    # "prevCC":I
    .restart local v34    # "prevSrc":I
    :cond_2
    move/from16 v0, v34

    if-eq v13, v0, :cond_15

    .line 2031
    sub-int v31, v13, v34

    .line 2032
    .local v31, "length":I
    add-int v6, v23, v31

    move/from16 v0, p5

    if-gt v6, v0, :cond_3

    .line 2033
    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, p3

    move/from16 v3, v23

    move/from16 v4, v31

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2035
    :cond_3
    add-int v23, v23, v31

    .line 2036
    move/from16 v21, v23

    .line 2040
    add-int/lit8 v19, v13, -0x1

    .line 2041
    aget-char v6, p0, v19

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_4

    move/from16 v0, v34

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    add-int/lit8 v6, v19, -0x1

    aget-char v6, p0, v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2044
    add-int/lit8 v19, v19, -0x1

    .line 2047
    :cond_4
    move/from16 v34, v13

    move/from16 v28, v23

    .end local v23    # "destIndex":I
    .local v28, "destIndex":I
    move/from16 v36, v21

    .line 2051
    .end local v21    # "reorderStartIndex":I
    .end local v31    # "length":I
    .local v36, "reorderStartIndex":I
    :goto_3
    move/from16 v0, p2

    if-ne v13, v0, :cond_5

    move/from16 v23, v28

    .line 2233
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    :goto_4
    sub-int v6, v23, p4

    return v6

    .line 2056
    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    :cond_5
    add-int/lit8 v13, v13, 0x1

    .line 2088
    invoke-static {v8, v9}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2095
    const/16 v26, 0x0

    .local v26, "cc":I
    move/from16 v33, v26

    .line 2096
    move/from16 v21, v28

    .line 2097
    .end local v36    # "reorderStartIndex":I
    .restart local v21    # "reorderStartIndex":I
    const/4 v6, 0x0

    aput v13, v11, v6

    .line 2098
    if-lez v28, :cond_8

    add-int/lit8 v6, v34, -0x1

    aget-char v6, p0, v6

    move/from16 v0, p6

    and-int/lit16 v10, v0, 0x1000

    if-eqz v10, :cond_6

    const/4 v13, 0x1

    .end local v13    # "srcIndex":I
    :goto_5
    move/from16 v0, v28

    move/from16 v1, p5

    if-gt v0, v1, :cond_7

    add-int/lit8 v15, v28, -0x1

    :goto_6
    move-object/from16 v10, p0

    move/from16 v12, p2

    move-object/from16 v14, p3

    move-object/from16 v16, p7

    invoke-static/range {v6 .. v16}, Lcom/ibm/icu/impl/NormalizerImpl;->composeHangul(CCJ[C[IIZ[CILcom/ibm/icu/text/UnicodeSet;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2105
    const/4 v6, 0x0

    aget v13, v11, v6

    .line 2106
    .restart local v13    # "srcIndex":I
    move/from16 v19, v13

    move/from16 v23, v28

    .line 2107
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    goto/16 :goto_1

    .line 2098
    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    :cond_6
    const/4 v13, 0x0

    goto :goto_5

    .end local v13    # "srcIndex":I
    :cond_7
    const/4 v15, 0x0

    goto :goto_6

    .line 2110
    :cond_8
    const/4 v6, 0x0

    aget v13, v11, v6

    .line 2114
    .restart local v13    # "srcIndex":I
    const/16 v25, 0x0

    .line 2115
    .local v25, "c2":C
    const/16 v31, 0x1

    .line 2116
    .restart local v31    # "length":I
    move/from16 v19, v34

    .line 2209
    :goto_7
    add-int v6, v28, v31

    move/from16 v0, p5

    if-gt v6, v0, :cond_14

    .line 2210
    if-eqz v26, :cond_12

    move/from16 v0, v26

    move/from16 v1, v33

    if-ge v0, v1, :cond_12

    .line 2213
    move/from16 v22, v28

    .line 2214
    .local v22, "reorderSplit":I
    add-int v23, v28, v31

    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    move-object/from16 v20, p3

    move/from16 v24, v7

    .line 2215
    invoke-static/range {v20 .. v26}, Lcom/ibm/icu/impl/NormalizerImpl;->insertOrdered([CIIICCI)I

    move-result v33

    .line 2217
    goto/16 :goto_1

    .line 2118
    .end local v21    # "reorderStartIndex":I
    .end local v22    # "reorderSplit":I
    .end local v23    # "destIndex":I
    .end local v25    # "c2":C
    .end local v26    # "cc":I
    .end local v31    # "length":I
    .restart local v28    # "destIndex":I
    .restart local v36    # "reorderStartIndex":I
    :cond_9
    invoke-static {v8, v9}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2119
    const/16 v25, 0x0

    .line 2120
    .restart local v25    # "c2":C
    const/16 v31, 0x1

    .line 2135
    .restart local v31    # "length":I
    :goto_8
    new-instance v18, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 2138
    .local v18, "args":Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    move-object/from16 v0, p7

    move/from16 v1, v25

    invoke-static {v0, v7, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2140
    const/16 v26, 0x0

    .restart local v26    # "cc":I
    move/from16 v21, v36

    .line 2141
    .end local v36    # "reorderStartIndex":I
    .restart local v21    # "reorderStartIndex":I
    goto :goto_7

    .line 2123
    .end local v18    # "args":Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    .end local v21    # "reorderStartIndex":I
    .end local v25    # "c2":C
    .end local v26    # "cc":I
    .end local v31    # "length":I
    .restart local v36    # "reorderStartIndex":I
    :cond_a
    move/from16 v0, p2

    if-eq v13, v0, :cond_b

    aget-char v25, p0, v13

    .restart local v25    # "c2":C
    invoke-static/range {v25 .. v25}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2125
    add-int/lit8 v13, v13, 0x1

    .line 2126
    const/16 v31, 0x2

    .line 2127
    .restart local v31    # "length":I
    move/from16 v0, v25

    invoke-static {v8, v9, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v8

    .line 2128
    goto :goto_8

    .line 2130
    .end local v25    # "c2":C
    .end local v31    # "length":I
    :cond_b
    const/16 v25, 0x0

    .line 2131
    .restart local v25    # "c2":C
    const/16 v31, 0x1

    .line 2132
    .restart local v31    # "length":I
    const-wide/16 v8, 0x0

    goto :goto_8

    .line 2141
    .restart local v18    # "args":Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    :cond_c
    move/from16 v0, v35

    int-to-long v14, v0

    and-long/2addr v14, v8

    const-wide/16 v38, 0x0

    cmp-long v6, v14, v38

    if-nez v6, :cond_d

    .line 2142
    const-wide/16 v14, 0xff

    const/16 v6, 0x8

    shr-long v38, v8, v6

    and-long v14, v14, v38

    long-to-int v0, v14

    move/from16 v26, v0

    .restart local v26    # "cc":I
    move/from16 v21, v36

    .line 2143
    .end local v36    # "reorderStartIndex":I
    .restart local v21    # "reorderStartIndex":I
    goto :goto_7

    .line 2159
    .end local v21    # "reorderStartIndex":I
    .end local v26    # "cc":I
    .restart local v36    # "reorderStartIndex":I
    :cond_d
    shl-int/lit8 v6, v35, 0x2

    and-int/lit8 v16, v6, 0xf

    .line 2165
    .local v16, "decompQCMask":I
    const v6, 0xff00

    or-int v6, v6, v35

    move/from16 v0, v16

    invoke-static {v8, v9, v6, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->isTrueStarter(JII)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2166
    move/from16 v19, v34

    :goto_9
    move-object/from16 v12, p0

    move/from16 v14, p2

    move/from16 v15, v35

    .line 2173
    invoke-static/range {v12 .. v17}, Lcom/ibm/icu/impl/NormalizerImpl;->findNextStarter([CIIIIC)I

    move-result v13

    .line 2176
    move/from16 v0, v33

    move-object/from16 v1, v18

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->prevCC:I

    .line 2178
    move/from16 v0, v31

    move-object/from16 v1, v18

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    move-object/from16 v20, p0

    move/from16 v21, v13

    move/from16 v22, p2

    move/from16 v23, p6

    move-object/from16 v24, p7

    .line 2179
    invoke-static/range {v18 .. v24}, Lcom/ibm/icu/impl/NormalizerImpl;->composePart(Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;I[CIIILcom/ibm/icu/text/UnicodeSet;)[C

    move-result-object v32

    .line 2181
    .local v32, "p":[C
    if-nez v32, :cond_f

    move/from16 v23, v28

    .line 2183
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    goto/16 :goto_4

    .line 2169
    .end local v23    # "destIndex":I
    .end local v32    # "p":[C
    .restart local v28    # "destIndex":I
    :cond_e
    sub-int v6, v34, v19

    sub-int v23, v28, v6

    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    move/from16 v28, v23

    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    goto :goto_9

    .line 2186
    .restart local v32    # "p":[C
    :cond_f
    move-object/from16 v0, v18

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->prevCC:I

    move/from16 v33, v0

    .line 2187
    move-object/from16 v0, v18

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    move/from16 v31, v0

    .line 2191
    move-object/from16 v0, v18

    iget v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    add-int v6, v6, v28

    move/from16 v0, p5

    if-gt v6, v0, :cond_10

    .line 2192
    const/16 v29, 0x0

    .local v29, "i":I
    move/from16 v23, v28

    .line 2193
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    :goto_a
    move-object/from16 v0, v18

    iget v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    move/from16 v0, v29

    if-ge v0, v6, :cond_11

    .line 2194
    add-int/lit8 v28, v23, 0x1

    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    add-int/lit8 v30, v29, 0x1

    .end local v29    # "i":I
    .local v30, "i":I
    aget-char v6, v32, v29

    aput-char v6, p3, v23

    .line 2195
    add-int/lit8 v31, v31, -0x1

    move/from16 v29, v30

    .end local v30    # "i":I
    .restart local v29    # "i":I
    move/from16 v23, v28

    .line 2196
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    goto :goto_a

    .line 2200
    .end local v23    # "destIndex":I
    .end local v29    # "i":I
    .restart local v28    # "destIndex":I
    :cond_10
    add-int v23, v28, v31

    .line 2203
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    :cond_11
    move/from16 v19, v13

    move/from16 v21, v36

    .line 2204
    .end local v36    # "reorderStartIndex":I
    .restart local v21    # "reorderStartIndex":I
    goto/16 :goto_1

    .line 2219
    .end local v16    # "decompQCMask":I
    .end local v18    # "args":Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    .end local v23    # "destIndex":I
    .end local v32    # "p":[C
    .restart local v26    # "cc":I
    .restart local v28    # "destIndex":I
    :cond_12
    add-int/lit8 v23, v28, 0x1

    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    aput-char v7, p3, v28

    .line 2220
    if-eqz v25, :cond_13

    .line 2221
    add-int/lit8 v28, v23, 0x1

    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    aput-char v25, p3, v23

    move/from16 v23, v28

    .line 2223
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    :cond_13
    move/from16 v33, v26

    .line 2225
    goto/16 :goto_1

    .line 2228
    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    :cond_14
    add-int v23, v28, v31

    .line 2229
    .end local v28    # "destIndex":I
    .restart local v23    # "destIndex":I
    move/from16 v33, v26

    .line 2231
    goto/16 :goto_1

    .end local v25    # "c2":C
    .end local v26    # "cc":I
    .end local v31    # "length":I
    :cond_15
    move/from16 v28, v23

    .end local v23    # "destIndex":I
    .restart local v28    # "destIndex":I
    move/from16 v36, v21

    .end local v21    # "reorderStartIndex":I
    .restart local v36    # "reorderStartIndex":I
    goto/16 :goto_3
.end method

.method private static composeHangul(CCJ[C[IIZ[CILcom/ibm/icu/text/UnicodeSet;)Z
    .locals 12
    .param p0, "prev"    # C
    .param p1, "c"    # C
    .param p2, "norm32"    # J
    .param p4, "src"    # [C
    .param p5, "srcIndex"    # [I
    .param p6, "limit"    # I
    .param p7, "compat"    # Z
    .param p8, "dest"    # [C
    .param p9, "destIndex"    # I
    .param p10, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1896
    const/4 v7, 0x0

    aget v5, p5, v7

    .line 1897
    .local v5, "start":I
    invoke-static {p2, p3}, Lcom/ibm/icu/impl/NormalizerImpl;->isJamoVTNorm32JamoV(J)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1900
    add-int/lit16 v7, p0, -0x1100

    int-to-char p0, v7

    .line 1901
    const/16 v7, 0x13

    if-ge p0, v7, :cond_6

    .line 1902
    const v7, 0xac00

    mul-int/lit8 v8, p0, 0x15

    add-int/lit16 v9, p1, -0x1161

    add-int/2addr v8, v9

    mul-int/lit8 v8, v8, 0x1c

    add-int/2addr v7, v8

    int-to-char p1, v7

    .line 1907
    move/from16 v0, p6

    if-eq v5, v0, :cond_0

    .line 1910
    aget-char v3, p4, v5

    .line 1911
    .local v3, "next":C
    add-int/lit16 v7, v3, -0x11a7

    int-to-char v6, v7

    .local v6, "t":C
    const/16 v7, 0x1c

    if-ge v6, v7, :cond_2

    .line 1913
    add-int/lit8 v5, v5, 0x1

    .line 1914
    add-int v7, p1, v6

    int-to-char p1, v7

    .line 1933
    .end local v3    # "next":C
    .end local v6    # "t":C
    :cond_0
    :goto_0
    move-object/from16 v0, p10

    invoke-static {v0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1934
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->isHangulWithoutJamoT(C)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1935
    add-int/lit8 v5, v5, -0x1

    .line 1937
    :cond_1
    const/4 v7, 0x0

    .line 1954
    :goto_1
    return v7

    .line 1915
    .restart local v3    # "next":C
    .restart local v6    # "t":C
    :cond_2
    if-eqz p7, :cond_0

    .line 1918
    invoke-static {v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide p2

    .line 1919
    invoke-static {p2, p3}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v7

    if-eqz v7, :cond_0

    const-wide/16 v8, 0x8

    and-long/2addr v8, p2

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 1921
    new-instance v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v7, 0x0

    invoke-direct {v2, v7}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1922
    .local v2, "dcArgs":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    const/16 v7, 0x8

    invoke-static {p2, p3, v7, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v4

    .line 1923
    .local v4, "p":I
    iget v7, v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    aget-char v7, v7, v4

    add-int/lit16 v7, v7, -0x11a7

    int-to-char v6, v7

    const/16 v7, 0x1c

    if-ge v6, v7, :cond_0

    .line 1927
    add-int/lit8 v5, v5, 0x1

    .line 1928
    add-int v7, p1, v6

    int-to-char p1, v7

    goto :goto_0

    .line 1939
    .end local v2    # "dcArgs":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    .end local v3    # "next":C
    .end local v4    # "p":I
    .end local v6    # "t":C
    :cond_3
    aput-char p1, p8, p9

    .line 1940
    const/4 v7, 0x0

    aput v5, p5, v7

    .line 1941
    const/4 v7, 0x1

    goto :goto_1

    .line 1943
    :cond_4
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->isHangulWithoutJamoT(C)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1946
    add-int/lit16 v7, p1, -0x11a7

    add-int/2addr v7, p0

    int-to-char p1, v7

    .line 1947
    move-object/from16 v0, p10

    invoke-static {v0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1948
    const/4 v7, 0x0

    goto :goto_1

    .line 1950
    :cond_5
    aput-char p1, p8, p9

    .line 1951
    const/4 v7, 0x0

    aput v5, p5, v7

    .line 1952
    const/4 v7, 0x1

    goto :goto_1

    .line 1954
    :cond_6
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private static composePart(Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;I[CIIILcom/ibm/icu/text/UnicodeSet;)[C
    .locals 13
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    .param p1, "prevStarter"    # I
    .param p2, "src"    # [C
    .param p3, "start"    # I
    .param p4, "limit"    # I
    .param p5, "options"    # I
    .param p6, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1856
    move/from16 v0, p5

    and-int/lit16 v2, v0, 0x1000

    if-eqz v2, :cond_1

    const/4 v8, 0x1

    .line 1859
    .local v8, "compat":Z
    :goto_0
    const/4 v2, 0x1

    new-array v9, v2, [I

    .line 1860
    .local v9, "outTrailCC":[I
    sub-int v2, p4, p1

    mul-int/lit8 v2, v2, 0x14

    new-array v5, v2, [C

    .line 1863
    .local v5, "buffer":[C
    :goto_1
    const/4 v6, 0x0

    array-length v7, v5

    move-object v2, p2

    move v3, p1

    move/from16 v4, p3

    move-object/from16 v10, p6

    invoke-static/range {v2 .. v10}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    .line 1866
    iget v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    array-length v3, v5

    if-gt v2, v3, :cond_2

    .line 1874
    iget v12, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    .line 1876
    .local v12, "recomposeLimit":I
    iget v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 1877
    new-instance v11, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;

    const/4 v2, 0x0

    invoke-direct {v11, v2}, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1878
    .local v11, "rcArgs":Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;
    iput-object v5, v11, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    .line 1879
    const/4 v2, 0x0

    iput v2, v11, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1880
    iput v12, v11, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    .line 1881
    move/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v11, v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->recompose(Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;ILcom/ibm/icu/text/UnicodeSet;)C

    move-result v2

    iput v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->prevCC:I

    .line 1882
    iget v12, v11, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    .line 1886
    .end local v11    # "rcArgs":Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;
    :cond_0
    iput v12, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    .line 1887
    return-object v5

    .line 1856
    .end local v5    # "buffer":[C
    .end local v8    # "compat":Z
    .end local v9    # "outTrailCC":[I
    .end local v12    # "recomposeLimit":I
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 1869
    .restart local v5    # "buffer":[C
    .restart local v8    # "compat":Z
    .restart local v9    # "outTrailCC":[I
    :cond_2
    iget v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    new-array v5, v2, [C

    .line 1871
    goto :goto_1
.end method

.method private static decompose(I[C)I
    .locals 12
    .param p0, "c"    # I
    .param p1, "buffer"    # [C

    .prologue
    const/4 v6, 0x0

    .line 2999
    const/4 v3, 0x0

    .line 3000
    .local v3, "length":I
    const-wide v8, 0xffffffffL

    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v7, p0}, Lcom/ibm/icu/impl/IntTrie;->getCodePointValue(I)I

    move-result v7

    int-to-long v10, v7

    and-long v4, v8, v10

    .line 3001
    .local v4, "norm32":J
    const-wide/16 v8, 0x4

    and-long/2addr v8, v4

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 3002
    invoke-static {v4, v5}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 3006
    const v7, 0xac00

    sub-int/2addr p0, v7

    .line 3008
    rem-int/lit8 v7, p0, 0x1c

    int-to-char v1, v7

    .line 3009
    .local v1, "c2":C
    div-int/lit8 p0, p0, 0x1c

    .line 3010
    if-lez v1, :cond_1

    .line 3011
    const/4 v7, 0x2

    add-int/lit16 v8, v1, 0x11a7

    int-to-char v8, v8

    aput-char v8, p1, v7

    .line 3012
    const/4 v3, 0x3

    .line 3016
    :goto_0
    const/4 v7, 0x1

    rem-int/lit8 v8, p0, 0x15

    add-int/lit16 v8, v8, 0x1161

    int-to-char v8, v8

    aput-char v8, p1, v7

    .line 3017
    div-int/lit8 v7, p0, 0x15

    add-int/lit16 v7, v7, 0x1100

    int-to-char v7, v7

    aput-char v7, p1, v6

    move v6, v3

    .line 3027
    .end local v1    # "c2":C
    :cond_0
    :goto_1
    return v6

    .line 3014
    .restart local v1    # "c2":C
    :cond_1
    const/4 v3, 0x2

    goto :goto_0

    .line 3021
    .end local v1    # "c2":C
    :cond_2
    new-instance v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v7, 0x0

    invoke-direct {v0, v7}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 3022
    .local v0, "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    invoke-static {v4, v5, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JLcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v2

    .line 3023
    .local v2, "index":I
    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    iget v8, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    invoke-static {v7, v2, p1, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3024
    iget v6, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    goto :goto_1
.end method

.method private static decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I
    .locals 8
    .param p0, "norm32"    # J
    .param p2, "qcMask"    # I
    .param p3, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    .prologue
    .line 467
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getExtraDataIndex(J)I

    move-result v1

    .line 468
    .local v1, "p":I
    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "p":I
    .local v2, "p":I
    aget-char v3, v3, v1

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 470
    int-to-long v4, p2

    and-long/2addr v4, p0

    const-wide/16 v6, 0x8

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    iget v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    const/16 v4, 0x100

    if-lt v3, v4, :cond_1

    .line 472
    iget v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    shr-int/lit8 v3, v3, 0x7

    and-int/lit8 v3, v3, 0x1

    iget v4, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    and-int/lit8 v4, v4, 0x7f

    add-int/2addr v3, v4

    add-int v1, v2, v3

    .line 473
    .end local v2    # "p":I
    .restart local v1    # "p":I
    iget v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    shr-int/lit8 v3, v3, 0x8

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 476
    :goto_0
    iget v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    and-int/lit16 v3, v3, 0x80

    if-lez v3, :cond_0

    .line 478
    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "p":I
    .restart local v2    # "p":I
    aget-char v0, v3, v1

    .line 479
    .local v0, "bothCCs":C
    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    .line 480
    and-int/lit16 v3, v0, 0xff

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    move v1, v2

    .line 486
    .end local v0    # "bothCCs":C
    .end local v2    # "p":I
    .restart local v1    # "p":I
    :goto_1
    iget v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    and-int/lit8 v3, v3, 0x7f

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 487
    return v1

    .line 483
    :cond_0
    const/4 v3, 0x0

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    iput v3, p3, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    goto :goto_1

    .end local v1    # "p":I
    .restart local v2    # "p":I
    :cond_1
    move v1, v2

    .end local v2    # "p":I
    .restart local v1    # "p":I
    goto :goto_0
.end method

.method private static decompose(JLcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I
    .locals 4
    .param p0, "norm32"    # J
    .param p2, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    .prologue
    .line 498
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getExtraDataIndex(J)I

    move-result v1

    .line 499
    .local v1, "p":I
    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "p":I
    .local v2, "p":I
    aget-char v3, v3, v1

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 501
    iget v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    and-int/lit16 v3, v3, 0x80

    if-lez v3, :cond_0

    .line 503
    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "p":I
    .restart local v1    # "p":I
    aget-char v0, v3, v2

    .line 504
    .local v0, "bothCCs":C
    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    .line 505
    and-int/lit16 v3, v0, 0xff

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    .line 511
    .end local v0    # "bothCCs":C
    :goto_0
    iget v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    and-int/lit8 v3, v3, 0x7f

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 512
    return v1

    .line 508
    .end local v1    # "p":I
    .restart local v2    # "p":I
    :cond_0
    const/4 v3, 0x0

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    iput v3, p2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    move v1, v2

    .end local v2    # "p":I
    .restart local v1    # "p":I
    goto :goto_0
.end method

.method public static decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I
    .locals 34
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "compat"    # Z
    .param p7, "outTrailCC"    # [I
    .param p8, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1136
    const/4 v4, 0x3

    new-array v0, v4, [C

    move-object/from16 v18, v0

    .line 1145
    .local v18, "buffer":[C
    move/from16 v7, p4

    .line 1146
    .local v7, "destIndex":I
    move/from16 v29, p1

    .line 1147
    .local v29, "srcIndex":I
    if-nez p6, :cond_2

    .line 1148
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v11, 0x8

    aget v4, v4, v11

    int-to-char v0, v4

    move/from16 v22, v0

    .line 1149
    .local v22, "minNoMaybe":C
    const/16 v28, 0x4

    .line 1156
    .local v28, "qcMask":I
    :goto_0
    const v4, 0xff00

    or-int v19, v4, v28

    .line 1157
    .local v19, "ccOrQCMask":I
    const/4 v5, 0x0

    .line 1158
    .local v5, "reorderStartIndex":I
    const/16 v26, 0x0

    .line 1159
    .local v26, "prevCC":I
    const-wide/16 v24, 0x0

    .line 1160
    .local v24, "norm32":J
    const/4 v8, 0x0

    .line 1161
    .local v8, "c":C
    const/4 v15, 0x0

    .line 1163
    .local v15, "pStart":I
    const/16 v30, -0x1

    .local v30, "trailCC":I
    move/from16 v10, v30

    .line 1169
    .local v10, "cc":I
    :cond_0
    :goto_1
    move/from16 v27, v29

    .line 1171
    .local v27, "prevSrc":I
    :goto_2
    move/from16 v0, v29

    move/from16 v1, p2

    if-eq v0, v1, :cond_3

    aget-char v8, p0, v29

    move/from16 v0, v22

    if-lt v8, v0, :cond_1

    invoke-static {v8}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v24

    move/from16 v0, v19

    int-to-long v12, v0

    and-long v12, v12, v24

    const-wide/16 v32, 0x0

    cmp-long v4, v12, v32

    if-nez v4, :cond_3

    .line 1173
    :cond_1
    const/16 v26, 0x0

    .line 1174
    add-int/lit8 v29, v29, 0x1

    .line 1175
    goto :goto_2

    .line 1151
    .end local v5    # "reorderStartIndex":I
    .end local v8    # "c":C
    .end local v10    # "cc":I
    .end local v15    # "pStart":I
    .end local v19    # "ccOrQCMask":I
    .end local v22    # "minNoMaybe":C
    .end local v24    # "norm32":J
    .end local v26    # "prevCC":I
    .end local v27    # "prevSrc":I
    .end local v28    # "qcMask":I
    .end local v30    # "trailCC":I
    :cond_2
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v11, 0x9

    aget v4, v4, v11

    int-to-char v0, v4

    move/from16 v22, v0

    .line 1152
    .restart local v22    # "minNoMaybe":C
    const/16 v28, 0x8

    .restart local v28    # "qcMask":I
    goto :goto_0

    .line 1178
    .restart local v5    # "reorderStartIndex":I
    .restart local v8    # "c":C
    .restart local v10    # "cc":I
    .restart local v15    # "pStart":I
    .restart local v19    # "ccOrQCMask":I
    .restart local v24    # "norm32":J
    .restart local v26    # "prevCC":I
    .restart local v27    # "prevSrc":I
    .restart local v30    # "trailCC":I
    :cond_3
    move/from16 v0, v29

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    .line 1179
    sub-int v21, v29, v27

    .line 1180
    .local v21, "length":I
    add-int v4, v7, v21

    move/from16 v0, p5

    if-gt v4, v0, :cond_4

    .line 1181
    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, p3

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1184
    :cond_4
    add-int v7, v7, v21

    .line 1185
    move v5, v7

    move/from16 v20, v7

    .line 1189
    .end local v7    # "destIndex":I
    .end local v21    # "length":I
    .local v20, "destIndex":I
    :goto_3
    move/from16 v0, v29

    move/from16 v1, p2

    if-ne v0, v1, :cond_5

    .line 1337
    const/4 v4, 0x0

    aput v26, p7, v4

    .line 1339
    sub-int v4, v20, p4

    return v4

    .line 1194
    :cond_5
    add-int/lit8 v29, v29, 0x1

    .line 1213
    invoke-static/range {v24 .. v25}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1214
    move-object/from16 v0, p8

    invoke-static {v0, v8}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1215
    const/4 v9, 0x0

    .line 1216
    .local v9, "c2":C
    const/4 v14, 0x0

    .line 1217
    .local v14, "p":[C
    const/16 v21, 0x1

    .line 1289
    .restart local v21    # "length":I
    :cond_6
    :goto_4
    add-int v4, v20, v21

    move/from16 v0, p5

    if-gt v4, v0, :cond_12

    .line 1290
    move/from16 v6, v20

    .line 1291
    .local v6, "reorderSplit":I
    if-nez v14, :cond_10

    .line 1293
    if-eqz v10, :cond_f

    move/from16 v0, v26

    if-ge v10, v0, :cond_f

    .line 1297
    add-int v7, v20, v21

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    move-object/from16 v4, p3

    .line 1298
    invoke-static/range {v4 .. v10}, Lcom/ibm/icu/impl/NormalizerImpl;->insertOrdered([CIIICCI)I

    move-result v30

    .line 1331
    .end local v6    # "reorderSplit":I
    :cond_7
    :goto_5
    move/from16 v26, v30

    .line 1332
    if-nez v26, :cond_0

    .line 1333
    move v5, v7

    .line 1334
    goto/16 :goto_1

    .line 1220
    .end local v7    # "destIndex":I
    .end local v9    # "c2":C
    .end local v14    # "p":[C
    .end local v21    # "length":I
    .restart local v20    # "destIndex":I
    :cond_8
    move-object/from16 v14, v18

    .line 1221
    .restart local v14    # "p":[C
    const/4 v15, 0x0

    .line 1222
    const/16 v30, 0x0

    move/from16 v10, v30

    .line 1224
    const v4, 0xac00

    sub-int v4, v8, v4

    int-to-char v8, v4

    .line 1226
    rem-int/lit8 v4, v8, 0x1c

    int-to-char v9, v4

    .line 1227
    .restart local v9    # "c2":C
    div-int/lit8 v4, v8, 0x1c

    int-to-char v8, v4

    .line 1228
    if-lez v9, :cond_9

    .line 1229
    const/4 v4, 0x2

    add-int/lit16 v11, v9, 0x11a7

    int-to-char v11, v11

    aput-char v11, v18, v4

    .line 1230
    const/16 v21, 0x3

    .line 1235
    .restart local v21    # "length":I
    :goto_6
    const/4 v4, 0x1

    rem-int/lit8 v11, v8, 0x15

    add-int/lit16 v11, v11, 0x1161

    int-to-char v11, v11

    aput-char v11, v18, v4

    .line 1236
    const/4 v4, 0x0

    div-int/lit8 v11, v8, 0x15

    add-int/lit16 v11, v11, 0x1100

    int-to-char v11, v11

    aput-char v11, v18, v4

    goto :goto_4

    .line 1232
    .end local v21    # "length":I
    :cond_9
    const/16 v21, 0x2

    .restart local v21    # "length":I
    goto :goto_6

    .line 1239
    .end local v9    # "c2":C
    .end local v14    # "p":[C
    .end local v21    # "length":I
    :cond_a
    invoke-static/range {v24 .. v25}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1240
    const/4 v9, 0x0

    .line 1241
    .restart local v9    # "c2":C
    const/16 v21, 0x1

    .line 1257
    .restart local v21    # "length":I
    :goto_7
    move-object/from16 v0, p8

    invoke-static {v0, v8, v9}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1259
    const/16 v30, 0x0

    move/from16 v10, v30

    .line 1260
    const/4 v14, 0x0

    .line 1261
    .restart local v14    # "p":[C
    goto :goto_4

    .line 1244
    .end local v9    # "c2":C
    .end local v14    # "p":[C
    .end local v21    # "length":I
    :cond_b
    move/from16 v0, v29

    move/from16 v1, p2

    if-eq v0, v1, :cond_c

    aget-char v9, p0, v29

    .restart local v9    # "c2":C
    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1246
    add-int/lit8 v29, v29, 0x1

    .line 1247
    const/16 v21, 0x2

    .line 1248
    .restart local v21    # "length":I
    move-wide/from16 v0, v24

    invoke-static {v0, v1, v9}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v24

    .line 1249
    goto :goto_7

    .line 1250
    .end local v9    # "c2":C
    .end local v21    # "length":I
    :cond_c
    const/4 v9, 0x0

    .line 1251
    .restart local v9    # "c2":C
    const/16 v21, 0x1

    .line 1252
    .restart local v21    # "length":I
    const-wide/16 v24, 0x0

    goto :goto_7

    .line 1261
    :cond_d
    move/from16 v0, v28

    int-to-long v12, v0

    and-long v12, v12, v24

    const-wide/16 v32, 0x0

    cmp-long v4, v12, v32

    if-nez v4, :cond_e

    .line 1263
    const-wide/16 v12, 0xff

    const/16 v4, 0x8

    shr-long v32, v24, v4

    and-long v12, v12, v32

    long-to-int v0, v12

    move/from16 v30, v0

    move/from16 v10, v30

    .line 1264
    const/4 v14, 0x0

    .line 1265
    .restart local v14    # "p":[C
    const/4 v15, -0x1

    .line 1266
    goto/16 :goto_4

    .line 1267
    .end local v14    # "p":[C
    :cond_e
    new-instance v17, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1271
    .local v17, "arg":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    move-wide/from16 v0, v24

    move/from16 v2, v28

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v15

    .line 1272
    sget-object v14, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    .line 1273
    .restart local v14    # "p":[C
    move-object/from16 v0, v17

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    move/from16 v21, v0

    .line 1274
    move-object/from16 v0, v17

    iget v10, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    .line 1275
    move-object/from16 v0, v17

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    move/from16 v30, v0

    .line 1276
    const/4 v4, 0x1

    move/from16 v0, v21

    if-ne v0, v4, :cond_6

    .line 1278
    aget-char v8, v14, v15

    .line 1279
    const/4 v9, 0x0

    .line 1280
    const/4 v14, 0x0

    .line 1281
    const/4 v15, -0x1

    goto/16 :goto_4

    .line 1302
    .end local v17    # "arg":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    .restart local v6    # "reorderSplit":I
    :cond_f
    add-int/lit8 v7, v20, 0x1

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    aput-char v8, p3, v20

    .line 1303
    if-eqz v9, :cond_7

    .line 1304
    add-int/lit8 v20, v7, 0x1

    .end local v7    # "destIndex":I
    .restart local v20    # "destIndex":I
    aput-char v9, p3, v7

    move/from16 v7, v20

    .line 1305
    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    goto/16 :goto_5

    .line 1311
    .end local v7    # "destIndex":I
    .restart local v20    # "destIndex":I
    :cond_10
    if-eqz v10, :cond_13

    move/from16 v0, v26

    if-ge v10, v0, :cond_13

    .line 1315
    add-int v7, v20, v21

    .line 1316
    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    add-int v16, v15, v21

    move-object/from16 v11, p3

    move v12, v5

    move v13, v6

    invoke-static/range {v11 .. v16}, Lcom/ibm/icu/impl/NormalizerImpl;->mergeOrdered([CII[CII)I

    move-result v30

    .line 1318
    goto/16 :goto_5

    .end local v7    # "destIndex":I
    .end local v15    # "pStart":I
    .restart local v20    # "destIndex":I
    .local v23, "pStart":I
    :cond_11
    move/from16 v7, v20

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    move/from16 v15, v23

    .line 1321
    .end local v23    # "pStart":I
    .restart local v15    # "pStart":I
    :goto_8
    add-int/lit8 v20, v7, 0x1

    .end local v7    # "destIndex":I
    .restart local v20    # "destIndex":I
    add-int/lit8 v23, v15, 0x1

    .end local v15    # "pStart":I
    .restart local v23    # "pStart":I
    aget-char v4, v14, v15

    aput-char v4, p3, v7

    .line 1322
    add-int/lit8 v21, v21, -0x1

    if-gtz v21, :cond_11

    move/from16 v7, v20

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    move/from16 v15, v23

    .end local v23    # "pStart":I
    .restart local v15    # "pStart":I
    goto/16 :goto_5

    .line 1328
    .end local v6    # "reorderSplit":I
    .end local v7    # "destIndex":I
    .restart local v20    # "destIndex":I
    :cond_12
    add-int v7, v20, v21

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    goto/16 :goto_5

    .end local v7    # "destIndex":I
    .restart local v6    # "reorderSplit":I
    .restart local v20    # "destIndex":I
    :cond_13
    move/from16 v7, v20

    .end local v20    # "destIndex":I
    .restart local v7    # "destIndex":I
    goto :goto_8

    .end local v6    # "reorderSplit":I
    .end local v9    # "c2":C
    .end local v14    # "p":[C
    .end local v21    # "length":I
    :cond_14
    move/from16 v20, v7

    .end local v7    # "destIndex":I
    .restart local v20    # "destIndex":I
    goto/16 :goto_3
.end method

.method private static decomposeFCD([CII[C[ILcom/ibm/icu/text/UnicodeSet;)I
    .locals 24
    .param p0, "src"    # [C
    .param p1, "start"    # I
    .param p2, "decompLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destIndexArr"    # [I
    .param p5, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2296
    const/4 v11, 0x0

    .line 2297
    .local v11, "p":[C
    const/4 v12, -0x1

    .line 2303
    .local v12, "pStart":I
    new-instance v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v2, 0x0

    invoke-direct {v14, v2}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 2304
    .local v14, "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    const/4 v2, 0x0

    aget v5, p4, v2

    .line 2319
    .local v5, "destIndex":I
    move v3, v5

    .line 2320
    .local v3, "reorderStartIndex":I
    const/16 v19, 0x0

    .local v19, "prevCC":I
    move v15, v5

    .end local v5    # "destIndex":I
    .local v15, "destIndex":I
    move/from16 v20, p1

    .line 2322
    .end local p1    # "start":I
    .local v20, "start":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, p2

    if-ge v0, v1, :cond_a

    .line 2323
    add-int/lit8 p1, v20, 0x1

    .end local v20    # "start":I
    .restart local p1    # "start":I
    aget-char v6, p0, v20

    .line 2324
    .local v6, "c":C
    invoke-static {v6}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v16

    .line 2325
    .local v16, "norm32":J
    invoke-static/range {v16 .. v17}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2326
    const/4 v7, 0x0

    .line 2327
    .local v7, "c2":C
    const/4 v2, 0x1

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 2347
    :goto_1
    move-object/from16 v0, p5

    invoke-static {v0, v6, v7}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2349
    const/4 v2, 0x0

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    .line 2350
    const/4 v11, 0x0

    .line 2371
    :cond_0
    :goto_2
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int/2addr v2, v15

    move-object/from16 v0, p3

    array-length v8, v0

    if-gt v2, v8, :cond_9

    .line 2372
    move v4, v15

    .line 2373
    .local v4, "reorderSplit":I
    if-nez v11, :cond_7

    .line 2375
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    if-eqz v2, :cond_6

    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    move/from16 v0, v19

    if-ge v2, v0, :cond_6

    .line 2378
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int v5, v15, v2

    .line 2379
    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    iget v8, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    move-object/from16 v2, p3

    invoke-static/range {v2 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->insertOrdered([CIIICCI)I

    move-result v2

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    .line 2412
    .end local v4    # "reorderSplit":I
    :cond_1
    :goto_3
    iget v0, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    move/from16 v19, v0

    .line 2413
    if-nez v19, :cond_b

    .line 2414
    move v3, v5

    move v15, v5

    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    move/from16 v20, p1

    .line 2415
    .end local p1    # "start":I
    .restart local v20    # "start":I
    goto :goto_0

    .line 2335
    .end local v7    # "c2":C
    .end local v20    # "start":I
    .restart local p1    # "start":I
    :cond_2
    move/from16 v0, p1

    move/from16 v1, p2

    if-eq v0, v1, :cond_3

    aget-char v7, p0, p1

    .restart local v7    # "c2":C
    invoke-static {v7}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2336
    add-int/lit8 p1, p1, 0x1

    .line 2337
    const/4 v2, 0x2

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 2338
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v7}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v16

    .line 2339
    goto :goto_1

    .line 2340
    .end local v7    # "c2":C
    :cond_3
    const/4 v7, 0x0

    .line 2341
    .restart local v7    # "c2":C
    const/4 v2, 0x1

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    .line 2342
    const-wide/16 v16, 0x0

    goto :goto_1

    .line 2351
    :cond_4
    const-wide/16 v8, 0x4

    and-long v8, v8, v16

    const-wide/16 v22, 0x0

    cmp-long v2, v8, v22

    if-nez v2, :cond_5

    .line 2353
    const-wide/16 v8, 0xff

    const/16 v2, 0x8

    shr-long v22, v16, v2

    and-long v8, v8, v22

    long-to-int v2, v8

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    .line 2355
    const/4 v11, 0x0

    .line 2356
    goto :goto_2

    .line 2359
    :cond_5
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v14}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JLcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v12

    .line 2360
    sget-object v11, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    .line 2361
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_0

    .line 2363
    aget-char v6, v11, v12

    .line 2364
    const/4 v7, 0x0

    .line 2365
    const/4 v11, 0x0

    goto :goto_2

    .line 2384
    .restart local v4    # "reorderSplit":I
    :cond_6
    add-int/lit8 v5, v15, 0x1

    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    aput-char v6, p3, v15

    .line 2385
    if-eqz v7, :cond_1

    .line 2386
    add-int/lit8 v15, v5, 0x1

    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    aput-char v7, p3, v5

    move v5, v15

    .line 2387
    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    goto :goto_3

    .line 2392
    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    :cond_7
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    if-eqz v2, :cond_c

    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    move/from16 v0, v19

    if-ge v2, v0, :cond_c

    .line 2395
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int v5, v15, v2

    .line 2396
    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int v13, v12, v2

    move-object/from16 v8, p3

    move v9, v3

    move v10, v4

    invoke-static/range {v8 .. v13}, Lcom/ibm/icu/impl/NormalizerImpl;->mergeOrdered([CII[CII)I

    move-result v2

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->trailCC:I

    goto :goto_3

    .end local v5    # "destIndex":I
    .end local v12    # "pStart":I
    .restart local v15    # "destIndex":I
    .local v18, "pStart":I
    :cond_8
    move v5, v15

    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    move/from16 v12, v18

    .line 2402
    .end local v18    # "pStart":I
    .restart local v12    # "pStart":I
    :goto_4
    add-int/lit8 v15, v5, 0x1

    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    add-int/lit8 v18, v12, 0x1

    .end local v12    # "pStart":I
    .restart local v18    # "pStart":I
    aget-char v2, v11, v12

    aput-char v2, p3, v5

    .line 2403
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    if-gtz v2, :cond_8

    move v5, v15

    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    move/from16 v12, v18

    .end local v18    # "pStart":I
    .restart local v12    # "pStart":I
    goto/16 :goto_3

    .line 2409
    .end local v4    # "reorderSplit":I
    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    :cond_9
    iget v2, v14, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int v5, v15, v2

    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    goto/16 :goto_3

    .line 2417
    .end local v5    # "destIndex":I
    .end local v6    # "c":C
    .end local v7    # "c2":C
    .end local v16    # "norm32":J
    .end local p1    # "start":I
    .restart local v15    # "destIndex":I
    .restart local v20    # "start":I
    :cond_a
    const/4 v2, 0x0

    aput v15, p4, v2

    .line 2418
    return v19

    .end local v15    # "destIndex":I
    .end local v20    # "start":I
    .restart local v5    # "destIndex":I
    .restart local v6    # "c":C
    .restart local v7    # "c2":C
    .restart local v16    # "norm32":J
    .restart local p1    # "start":I
    :cond_b
    move v15, v5

    .end local v5    # "destIndex":I
    .restart local v15    # "destIndex":I
    move/from16 v20, p1

    .end local p1    # "start":I
    .restart local v20    # "start":I
    goto/16 :goto_0

    .end local v20    # "start":I
    .restart local v4    # "reorderSplit":I
    .restart local p1    # "start":I
    :cond_c
    move v5, v15

    .end local v15    # "destIndex":I
    .restart local v5    # "destIndex":I
    goto :goto_4
.end method

.method private static findNextStarter([CIIIIC)I
    .locals 14
    .param p0, "src"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "qcMask"    # I
    .param p4, "decompQCMask"    # I
    .param p5, "minNoMaybe"    # C

    .prologue
    .line 1790
    const v9, 0xff00

    or-int v4, v9, p3

    .line 1792
    .local v4, "ccOrQCMask":I
    new-instance v5, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v9, 0x0

    invoke-direct {v5, v9}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1795
    .local v5, "decompArgs":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    :goto_0
    move/from16 v0, p2

    if-ne p1, v0, :cond_1

    .line 1840
    :cond_0
    return p1

    .line 1798
    :cond_1
    aget-char v2, p0, p1

    .line 1799
    .local v2, "c":C
    move/from16 v0, p5

    if-lt v2, v0, :cond_0

    .line 1803
    invoke-static {v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v6

    .line 1804
    .local v6, "norm32":J
    int-to-long v10, v4

    and-long/2addr v10, v6

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 1808
    invoke-static {v6, v7}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32LeadSurrogate(J)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1810
    add-int/lit8 v9, p1, 0x1

    move/from16 v0, p2

    if-eq v9, v0, :cond_0

    add-int/lit8 v9, p1, 0x1

    aget-char v3, p0, v9

    .local v3, "c2":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1815
    invoke-static {v6, v7, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v6

    .line 1817
    int-to-long v10, v4

    and-long/2addr v10, v6

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 1825
    :goto_1
    move/from16 v0, p4

    int-to-long v10, v0

    and-long/2addr v10, v6

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_2

    .line 1828
    move/from16 v0, p4

    invoke-static {v6, v7, v0, v5}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v8

    .line 1832
    .local v8, "p":I
    iget v9, v5, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    if-nez v9, :cond_2

    sget-object v9, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    move/from16 v0, p3

    invoke-static {v9, v8, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32([CII)J

    move-result-wide v10

    move/from16 v0, p3

    int-to-long v12, v0

    and-long/2addr v10, v12

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 1837
    .end local v8    # "p":I
    :cond_2
    if-nez v3, :cond_4

    const/4 v9, 0x1

    :goto_2
    add-int/2addr p1, v9

    .line 1838
    goto :goto_0

    .line 1821
    .end local v3    # "c2":C
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "c2":C
    goto :goto_1

    .line 1837
    :cond_4
    const/4 v9, 0x2

    goto :goto_2
.end method

.method private static findPreviousStarter([CIIIIC)I
    .locals 5
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "current"    # I
    .param p3, "ccOrQCMask"    # I
    .param p4, "decompQCMask"    # I
    .param p5, "minNoMaybe"    # C

    .prologue
    .line 1764
    new-instance v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1765
    .local v0, "args":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    iput-object p0, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->src:[C

    .line 1766
    iput p1, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->start:I

    .line 1767
    iput p2, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 1769
    :cond_0
    iget v1, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->start:I

    iget v4, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    if-ge v1, v4, :cond_1

    .line 1770
    or-int v1, p3, p4

    invoke-static {v0, p5, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getPrevNorm32(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;II)J

    move-result-wide v2

    .line 1771
    .local v2, "norm32":J
    invoke-static {v2, v3, p3, p4}, Lcom/ibm/icu/impl/NormalizerImpl;->isTrueStarter(JII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775
    .end local v2    # "norm32":J
    :cond_1
    iget v1, v0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    return v1
.end method

.method private static findSafeFCD([CIIC)I
    .locals 4
    .param p0, "src"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "fcd16"    # C

    .prologue
    const/16 v3, 0xff

    .line 2254
    :goto_0
    and-int/lit16 v2, p3, 0xff

    if-nez v2, :cond_1

    .line 2288
    :cond_0
    return p1

    .line 2259
    :cond_1
    if-eq p1, p2, :cond_0

    .line 2262
    aget-char v0, p0, p1

    .line 2265
    .local v0, "c":C
    const/16 v2, 0x300

    if-lt v0, v2, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result p3

    if-eqz p3, :cond_0

    .line 2269
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2270
    if-le p3, v3, :cond_0

    .line 2273
    add-int/lit8 p1, p1, 0x1

    .line 2274
    goto :goto_0

    :cond_2
    add-int/lit8 v2, p1, 0x1

    if-eq v2, p2, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v1, p0, v2

    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2277
    invoke-static {p3, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result p3

    .line 2278
    if-le p3, v3, :cond_0

    .line 2281
    add-int/lit8 p1, p1, 0x2

    .line 2282
    goto :goto_0
.end method

.method private static foldCase(I[CIII)I
    .locals 5
    .param p0, "c"    # I
    .param p1, "dest"    # [C
    .param p2, "destStart"    # I
    .param p3, "destLimit"    # I
    .param p4, "options"    # I

    .prologue
    .line 3033
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 3034
    .local v3, "src":Ljava/lang/String;
    invoke-static {v3, p4}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 3035
    .local v1, "foldedStr":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 3036
    .local v0, "foldedC":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_1

    .line 3037
    if-ge p2, p3, :cond_0

    .line 3038
    aget-char v4, v0, v2

    aput-char v4, p1, p2

    .line 3042
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 3036
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3044
    :cond_1
    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v4

    if-ne p0, v4, :cond_2

    neg-int p2, p2

    .end local p2    # "destStart":I
    :cond_2
    return p2
.end method

.method public static getCanonStartSet(ILcom/ibm/icu/impl/USerializedSet;)Z
    .locals 20
    .param p0, "c"    # I
    .param p1, "fillSet"    # Lcom/ibm/icu/impl/USerializedSet;

    .prologue
    .line 2610
    if-eqz p1, :cond_9

    sget-object v17, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    if-eqz v17, :cond_9

    .line 2619
    const/4 v4, 0x0

    .line 2621
    .local v4, "i":I
    sget-object v17, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    const/16 v18, 0x0

    aget-object v17, v17, v18

    check-cast v17, [I

    move-object/from16 v5, v17

    check-cast v5, [I

    .line 2622
    .local v5, "idxs":[I
    sget-object v17, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    const/16 v18, 0x1

    aget-object v17, v17, v18

    check-cast v17, [C

    move-object/from16 v11, v17

    check-cast v11, [C

    .line 2624
    .local v11, "startSets":[C
    const v17, 0xffff

    move/from16 v0, p0

    move/from16 v1, v17

    if-gt v0, v1, :cond_3

    .line 2625
    sget-object v17, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    const/16 v18, 0x2

    aget-object v17, v17, v18

    check-cast v17, [C

    move-object/from16 v12, v17

    check-cast v12, [C

    .line 2626
    .local v12, "table":[C
    const/4 v10, 0x0

    .line 2627
    .local v10, "start":I
    array-length v7, v12

    .line 2630
    .local v7, "limit":I
    :goto_0
    add-int/lit8 v17, v7, -0x2

    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    .line 2631
    add-int v17, v10, v7

    div-int/lit8 v17, v17, 0x4

    mul-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    int-to-char v4, v0

    .line 2632
    aget-char v17, v12, v4

    move/from16 v0, p0

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 2633
    move v7, v4

    .line 2634
    goto :goto_0

    .line 2635
    :cond_0
    move v10, v4

    .line 2637
    goto :goto_0

    .line 2640
    :cond_1
    aget-char v17, v12, v10

    move/from16 v0, p0

    move/from16 v1, v17

    if-ne v0, v1, :cond_9

    .line 2641
    add-int/lit8 v17, v10, 0x1

    aget-char v4, v12, v17

    .line 2642
    const v17, 0xc000

    and-int v17, v17, v4

    const/16 v18, 0x4000

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 2645
    and-int/lit16 v4, v4, 0x3fff

    .line 2646
    array-length v0, v5

    move/from16 v17, v0

    sub-int v17, v4, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Lcom/ibm/icu/impl/USerializedSet;->getSet([CI)Z

    move-result v17

    .line 2720
    .end local v4    # "i":I
    .end local v5    # "idxs":[I
    .end local v7    # "limit":I
    .end local v10    # "start":I
    .end local v11    # "startSets":[C
    .end local v12    # "table":[C
    :goto_1
    return v17

    .line 2650
    .restart local v4    # "i":I
    .restart local v5    # "idxs":[I
    .restart local v7    # "limit":I
    .restart local v10    # "start":I
    .restart local v11    # "startSets":[C
    .restart local v12    # "table":[C
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/USerializedSet;->setToOne(I)V

    .line 2651
    const/16 v17, 0x1

    goto :goto_1

    .line 2655
    .end local v7    # "limit":I
    .end local v10    # "start":I
    .end local v12    # "table":[C
    :cond_3
    const/4 v6, 0x0

    .line 2657
    .local v6, "j":C
    sget-object v17, Lcom/ibm/icu/impl/NormalizerImpl;->canonStartSets:[Ljava/lang/Object;

    const/16 v18, 0x3

    aget-object v17, v17, v18

    check-cast v17, [C

    move-object/from16 v12, v17

    check-cast v12, [C

    .line 2658
    .restart local v12    # "table":[C
    const/4 v10, 0x0

    .line 2659
    .restart local v10    # "start":I
    array-length v7, v12

    .line 2661
    .restart local v7    # "limit":I
    shr-int/lit8 v17, p0, 0x10

    move/from16 v0, v17

    int-to-char v3, v0

    .line 2662
    .local v3, "high":C
    move/from16 v0, p0

    int-to-char v8, v0

    .line 2665
    .local v8, "low":C
    :cond_4
    :goto_2
    add-int/lit8 v17, v7, -0x3

    move/from16 v0, v17

    if-ge v10, v0, :cond_7

    .line 2667
    add-int v17, v10, v7

    div-int/lit8 v17, v17, 0x6

    mul-int/lit8 v17, v17, 0x3

    move/from16 v0, v17

    int-to-char v4, v0

    .line 2668
    aget-char v17, v12, v4

    and-int/lit8 v17, v17, 0x1f

    move/from16 v0, v17

    int-to-char v6, v0

    .line 2669
    add-int/lit8 v17, v4, 0x1

    aget-char v13, v12, v17

    .line 2670
    .local v13, "tableVal":I
    move v9, v8

    .line 2671
    .local v9, "lowInt":I
    if-lt v3, v6, :cond_5

    if-le v13, v9, :cond_6

    if-ne v3, v6, :cond_6

    .line 2672
    :cond_5
    move v7, v4

    .line 2681
    :goto_3
    invoke-static {}, Lcom/ibm/icu/impl/ICUDebug;->enabled()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 2682
    sget-object v17, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v19, "\t\t j = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-static {v6, v0}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v19, "\t i = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-static {v4, v0}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v19, "\t high = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-static {v3}, Lcom/ibm/icu/impl/Utility;->hex(C)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v19, "\t low = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-static {v9, v0}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string/jumbo v19, "\t table[i+1]: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-static {v13, v0}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2674
    :cond_6
    move v10, v4

    goto :goto_3

    .line 2693
    .end local v9    # "lowInt":I
    .end local v13    # "tableVal":I
    :cond_7
    aget-char v2, v12, v10

    .line 2696
    .local v2, "h":C
    add-int/lit8 v17, v10, 0x1

    aget-char v14, v12, v17

    .line 2697
    .local v14, "tableVal1":I
    move v9, v8

    .line 2699
    .restart local v9    # "lowInt":I
    and-int/lit8 v17, v2, 0x1f

    move/from16 v0, v17

    if-ne v3, v0, :cond_9

    if-ne v9, v14, :cond_9

    .line 2700
    add-int/lit8 v17, v10, 0x2

    aget-char v15, v12, v17

    .line 2701
    .local v15, "tableVal2":I
    move v4, v15

    .line 2702
    const v17, 0x8000

    and-int v17, v17, v2

    if-nez v17, :cond_8

    .line 2704
    array-length v0, v5

    move/from16 v17, v0

    sub-int v17, v4, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Lcom/ibm/icu/impl/USerializedSet;->getSet([CI)Z

    move-result v17

    goto/16 :goto_1

    .line 2711
    :cond_8
    and-int/lit16 v0, v2, 0x1f00

    move/from16 v17, v0

    shl-int/lit8 v16, v17, 0x8

    .line 2712
    .local v16, "temp":I
    or-int v4, v4, v16

    .line 2713
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/USerializedSet;->setToOne(I)V

    .line 2714
    const/16 v17, 0x1

    goto/16 :goto_1

    .line 2720
    .end local v2    # "h":C
    .end local v3    # "high":C
    .end local v4    # "i":I
    .end local v5    # "idxs":[I
    .end local v6    # "j":C
    .end local v7    # "limit":I
    .end local v8    # "low":C
    .end local v9    # "lowInt":I
    .end local v10    # "start":I
    .end local v11    # "startSets":[C
    .end local v12    # "table":[C
    .end local v14    # "tableVal1":I
    .end local v15    # "tableVal2":I
    .end local v16    # "temp":I
    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_1
.end method

.method public static getCombiningClass(I)I
    .locals 6
    .param p0, "c"    # I

    .prologue
    .line 2586
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(I)J

    move-result-wide v0

    .line 2587
    .local v0, "norm32":J
    const/16 v2, 0x8

    shr-long v2, v0, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-char v2, v2

    return v2
.end method

.method private static getCombiningIndexFromStarter(CC)I
    .locals 4
    .param p0, "c"    # C
    .param p1, "c2"    # C

    .prologue
    .line 1417
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v0

    .line 1418
    .local v0, "norm32":J
    if-eqz p1, :cond_0

    .line 1419
    invoke-static {v0, v1, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v0

    .line 1421
    :cond_0
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getExtraDataIndex(J)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-char v2, v2, v3

    return v2
.end method

.method public static getDecomposition(IZ[CII)I
    .locals 18
    .param p0, "c"    # I
    .param p1, "compat"    # Z
    .param p2, "dest"    # [C
    .param p3, "destStart"    # I
    .param p4, "destCapacity"    # I

    .prologue
    .line 1048
    const-wide v14, 0xffffffffL

    move/from16 v0, p0

    int-to-long v0, v0

    move-wide/from16 v16, v0

    and-long v14, v14, v16

    const-wide/32 v16, 0x10ffff

    cmp-long v13, v14, v16

    if-gtz v13, :cond_c

    .line 1055
    if-nez p1, :cond_2

    .line 1056
    sget-object v13, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v14, 0x8

    aget v7, v13, v14

    .line 1057
    .local v7, "minNoMaybe":I
    const/4 v12, 0x4

    .line 1063
    .local v12, "qcMask":I
    :goto_0
    move/from16 v0, p0

    if-ge v0, v7, :cond_3

    .line 1065
    if-lez p4, :cond_0

    .line 1066
    const/4 v13, 0x0

    move/from16 v0, p0

    int-to-char v14, v0

    aput-char v14, p2, v13

    .line 1068
    :cond_0
    const/4 v5, -0x1

    .line 1126
    .end local v7    # "minNoMaybe":I
    .end local v12    # "qcMask":I
    :cond_1
    :goto_1
    return v5

    .line 1059
    :cond_2
    sget-object v13, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    const/16 v14, 0x9

    aget v7, v13, v14

    .line 1060
    .restart local v7    # "minNoMaybe":I
    const/16 v12, 0x8

    .restart local v12    # "qcMask":I
    goto :goto_0

    .line 1072
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(I)J

    move-result-wide v8

    .line 1073
    .local v8, "norm32":J
    int-to-long v14, v12

    and-long/2addr v14, v8

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-nez v13, :cond_7

    .line 1075
    const v13, 0xffff

    move/from16 v0, p0

    if-gt v0, v13, :cond_5

    .line 1076
    if-lez p4, :cond_4

    .line 1077
    const/4 v13, 0x0

    move/from16 v0, p0

    int-to-char v14, v0

    aput-char v14, p2, v13

    .line 1079
    :cond_4
    const/4 v5, -0x1

    goto :goto_1

    .line 1081
    :cond_5
    const/4 v13, 0x2

    move/from16 v0, p4

    if-lt v0, v13, :cond_6

    .line 1082
    const/4 v13, 0x0

    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v14

    aput-char v14, p2, v13

    .line 1083
    const/4 v13, 0x1

    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v14

    aput-char v14, p2, v13

    .line 1085
    :cond_6
    const/4 v5, -0x2

    goto :goto_1

    .line 1087
    :cond_7
    invoke-static {v8, v9}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 1091
    const v13, 0xac00

    sub-int p0, p0, v13

    .line 1093
    rem-int/lit8 v13, p0, 0x1c

    int-to-char v3, v13

    .line 1094
    .local v3, "c2":C
    div-int/lit8 p0, p0, 0x1c

    .line 1095
    if-lez v3, :cond_9

    .line 1096
    const/4 v13, 0x3

    move/from16 v0, p4

    if-lt v0, v13, :cond_8

    .line 1097
    const/4 v13, 0x2

    add-int/lit16 v14, v3, 0x11a7

    int-to-char v14, v14

    aput-char v14, p2, v13

    .line 1099
    :cond_8
    const/4 v5, 0x3

    .line 1104
    .local v5, "length":I
    :goto_2
    const/4 v13, 0x2

    move/from16 v0, p4

    if-lt v0, v13, :cond_1

    .line 1105
    const/4 v13, 0x1

    rem-int/lit8 v14, p0, 0x15

    add-int/lit16 v14, v14, 0x1161

    int-to-char v14, v14

    aput-char v14, p2, v13

    .line 1106
    const/4 v13, 0x0

    div-int/lit8 v14, p0, 0x15

    add-int/lit16 v14, v14, 0x1100

    int-to-char v14, v14

    aput-char v14, p2, v13

    goto :goto_1

    .line 1101
    .end local v5    # "length":I
    :cond_9
    const/4 v5, 0x2

    .restart local v5    # "length":I
    goto :goto_2

    .line 1114
    .end local v3    # "c2":C
    .end local v5    # "length":I
    :cond_a
    new-instance v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v13, 0x0

    invoke-direct {v2, v13}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1116
    .local v2, "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    invoke-static {v8, v9, v12, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v10

    .line 1117
    .local v10, "p":I
    iget v13, v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    move/from16 v0, p4

    if-gt v13, v0, :cond_b

    .line 1118
    iget v13, v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    add-int v6, v10, v13

    .line 1120
    .local v6, "limit":I
    :goto_3
    add-int/lit8 v4, p3, 0x1

    .end local p3    # "destStart":I
    .local v4, "destStart":I
    sget-object v13, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v11, v10, 0x1

    .end local v10    # "p":I
    .local v11, "p":I
    aget-char v13, v13, v10

    aput-char v13, p2, p3

    .line 1121
    if-lt v11, v6, :cond_d

    move v10, v11

    .end local v11    # "p":I
    .restart local v10    # "p":I
    move/from16 p3, v4

    .line 1123
    .end local v4    # "destStart":I
    .end local v6    # "limit":I
    .restart local p3    # "destStart":I
    :cond_b
    iget v5, v2, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->length:I

    goto/16 :goto_1

    .line 1126
    .end local v2    # "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    .end local v7    # "minNoMaybe":I
    .end local v8    # "norm32":J
    .end local v10    # "p":I
    .end local v12    # "qcMask":I
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_1

    .end local p3    # "destStart":I
    .restart local v2    # "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    .restart local v4    # "destStart":I
    .restart local v6    # "limit":I
    .restart local v7    # "minNoMaybe":I
    .restart local v8    # "norm32":J
    .restart local v11    # "p":I
    .restart local v12    # "qcMask":I
    :cond_d
    move v10, v11

    .end local v11    # "p":I
    .restart local v10    # "p":I
    move/from16 p3, v4

    .end local v4    # "destStart":I
    .restart local p3    # "destStart":I
    goto :goto_3
.end method

.method private static getExtraDataIndex(J)I
    .locals 2
    .param p0, "norm32"    # J

    .prologue
    .line 450
    const/16 v0, 0x10

    shr-long v0, p0, v0

    long-to-int v0, v0

    return v0
.end method

.method public static getFCD16(C)C
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 436
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/CharTrie;->getLeadValue(C)C

    move-result v0

    return v0
.end method

.method public static getFCD16(I)I
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 446
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    return v0
.end method

.method public static getFCD16FromSurrogatePair(CC)C
    .locals 1
    .param p0, "fcd16"    # C
    .param p1, "c2"    # C

    .prologue
    .line 443
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/impl/CharTrie;->getTrailValue(IC)C

    move-result v0

    return v0
.end method

.method public static getFC_NFKC_Closure(I[C)I
    .locals 7
    .param p0, "c"    # I
    .param p1, "dest"    # [C

    .prologue
    const/4 v5, 0x0

    .line 2727
    if-nez p1, :cond_1

    .line 2728
    const/4 v1, 0x0

    .line 2733
    .local v1, "destCapacity":I
    :goto_0
    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v6, p0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 2735
    .local v0, "aux":I
    and-int/lit16 v0, v0, 0x3ff

    .line 2736
    if-eqz v0, :cond_3

    .line 2738
    move v2, v0

    .line 2741
    .local v2, "index":I
    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    aget-char v4, v6, v2

    .line 2742
    .local v4, "s":I
    const v6, 0xff00

    if-ge v4, v6, :cond_2

    .line 2744
    const/4 v3, 0x1

    .line 2749
    .local v3, "length":I
    :goto_1
    if-lez v3, :cond_0

    if-gt v3, v1, :cond_0

    .line 2750
    sget-object v6, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    invoke-static {v6, v2, p1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2754
    .end local v2    # "index":I
    .end local v3    # "length":I
    .end local v4    # "s":I
    :cond_0
    :goto_2
    return v3

    .line 2730
    .end local v0    # "aux":I
    .end local v1    # "destCapacity":I
    :cond_1
    array-length v1, p1

    .restart local v1    # "destCapacity":I
    goto :goto_0

    .line 2746
    .restart local v0    # "aux":I
    .restart local v2    # "index":I
    .restart local v4    # "s":I
    :cond_2
    and-int/lit16 v3, v4, 0xff

    .line 2747
    .restart local v3    # "length":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v2    # "index":I
    .end local v3    # "length":I
    .end local v4    # "s":I
    :cond_3
    move v3, v5

    .line 2754
    goto :goto_2
.end method

.method public static getFromIndexesArr(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 272
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl;->indexes:[I

    aget v0, v0, p0

    return v0
.end method

.method public static final getNX(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p0, "options"    # I

    .prologue
    .line 3835
    and-int/lit16 p0, p0, 0xff

    if-nez p0, :cond_0

    .line 3837
    const/4 v0, 0x0

    .line 3839
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    goto :goto_0
.end method

.method private static getNextCC(Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;)I
    .locals 8
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;

    .prologue
    const/4 v2, 0x0

    .line 532
    iget-object v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->source:[C

    iget v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    aget-char v3, v3, v4

    iput-char v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c:C

    .line 534
    iget-char v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c:C

    invoke-static {v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v0

    .line 535
    .local v0, "norm32":J
    const-wide/32 v4, 0xff00

    and-long/2addr v4, v0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 536
    iput-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    .line 553
    :goto_0
    return v2

    .line 539
    :cond_0
    invoke-static {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32LeadSurrogate(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 540
    iput-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    .line 553
    :goto_1
    const-wide/16 v2, 0xff

    const/16 v4, 0x8

    shr-long v4, v0, v4

    and-long/2addr v2, v4

    long-to-int v2, v2

    goto :goto_0

    .line 543
    :cond_1
    iget v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    iget v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->source:[C

    iget v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    aget-char v3, v3, v4

    iput-char v3, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 545
    iget v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    .line 546
    iget-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v0

    .line 547
    goto :goto_1

    .line 548
    :cond_2
    iput-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    goto :goto_0
.end method

.method private static getNextCombining(Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;ILcom/ibm/icu/text/UnicodeSet;)I
    .locals 12
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;
    .param p1, "limit"    # I
    .param p2, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const-wide/16 v10, 0xc0

    const/4 v4, 0x0

    .line 1360
    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->source:[C

    iget v6, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    aget-char v5, v5, v6

    iput-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    .line 1361
    iget-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    invoke-static {v5}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v2

    .line 1364
    .local v2, "norm32":J
    iput-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1365
    iput v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->combiningIndex:I

    .line 1366
    iput-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    .line 1368
    const-wide/32 v6, 0xffc0

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 1402
    :cond_0
    :goto_0
    return v4

    .line 1371
    :cond_1
    invoke-static {v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1390
    :goto_1
    iget-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    iget-char v6, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    invoke-static {p2, v5, v6}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1394
    const/16 v5, 0x8

    shr-long v6, v2, v5

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    long-to-int v5, v6

    int-to-char v5, v5

    iput-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    .line 1396
    and-long v6, v2, v10

    long-to-int v0, v6

    .line 1397
    .local v0, "combineFlags":I
    if-eqz v0, :cond_3

    .line 1398
    invoke-static {v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getExtraDataIndex(J)I

    move-result v1

    .line 1399
    .local v1, "index":I
    if-lez v1, :cond_2

    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    add-int/lit8 v5, v1, -0x1

    aget-char v4, v4, v5

    :cond_2
    iput v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->combiningIndex:I

    .end local v1    # "index":I
    :cond_3
    move v4, v0

    .line 1402
    goto :goto_0

    .line 1373
    .end local v0    # "combineFlags":I
    :cond_4
    invoke-static {v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1375
    const-wide v4, 0xffffffffL

    const-wide/32 v6, 0xfff0

    const/16 v8, 0x10

    shr-long v8, v2, v8

    or-long/2addr v6, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->combiningIndex:I

    .line 1377
    and-long v4, v2, v10

    long-to-int v4, v4

    goto :goto_0

    .line 1380
    :cond_5
    iget v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    if-eq v5, p1, :cond_6

    iget-object v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->source:[C

    iget v6, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    aget-char v5, v5, v6

    iput-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1382
    iget v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    .line 1383
    iget-char v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    invoke-static {v2, v3, v5}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v2

    .line 1384
    goto :goto_1

    .line 1385
    :cond_6
    iput-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    goto :goto_0
.end method

.method public static getNorm32(C)J
    .locals 4
    .param p0, "c"    # C

    .prologue
    .line 393
    const-wide v0, 0xffffffffL

    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    int-to-long v2, v2

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private static getNorm32(I)J
    .locals 4
    .param p0, "c"    # I

    .prologue
    .line 406
    const-wide v0, 0xffffffffL

    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/IntTrie;->getCodePointValue(I)I

    move-result v2

    int-to-long v2, v2

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private static getNorm32([CII)J
    .locals 6
    .param p0, "p"    # [C
    .param p1, "start"    # I
    .param p2, "mask"    # I

    .prologue
    .line 424
    aget-char v2, p0, p1

    invoke-static {v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v0

    .line 425
    .local v0, "norm32":J
    int-to-long v2, p2

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32LeadSurrogate(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 427
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v0

    .line 429
    :cond_0
    return-wide v0
.end method

.method public static getNorm32FromSurrogatePair(JC)J
    .locals 4
    .param p0, "norm32"    # J
    .param p2, "c2"    # C

    .prologue
    .line 402
    const-wide v0, 0xffffffffL

    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl$NormTrieImpl;->normTrie:Lcom/ibm/icu/impl/IntTrie;

    long-to-int v3, p0

    invoke-virtual {v2, v3, p2}, Lcom/ibm/icu/impl/IntTrie;->getTrailValue(IC)I

    move-result v2

    int-to-long v2, v2

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private static getPrevCC(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;)I
    .locals 5
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;

    .prologue
    .line 616
    const-wide/16 v0, 0xff

    const/16 v2, 0x300

    const v3, 0xff00

    invoke-static {p0, v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getPrevNorm32(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;II)J

    move-result-wide v2

    const/16 v4, 0x8

    shr-long/2addr v2, v4

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static getPrevNorm32(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;II)J
    .locals 7
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    .param p1, "minC"    # I
    .param p2, "mask"    # I

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 576
    iget-object v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->src:[C

    iget v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    aget-char v4, v4, v5

    iput-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    .line 577
    iput-char v6, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c2:C

    .line 582
    iget-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    if-ge v4, p1, :cond_1

    .line 606
    :cond_0
    :goto_0
    return-wide v2

    .line 584
    :cond_1
    iget-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v4

    if-nez v4, :cond_2

    .line 585
    iget-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    invoke-static {v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v2

    goto :goto_0

    .line 586
    :cond_2
    iget-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 589
    iget v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    iget v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->start:I

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->src:[C

    iget v5, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    add-int/lit8 v5, v5, -0x1

    aget-char v4, v4, v5

    iput-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c2:C

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 591
    iget v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 592
    iget-char v4, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c2:C

    invoke-static {v4}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v0

    .line 594
    .local v0, "norm32":J
    int-to-long v4, p2

    and-long/2addr v4, v0

    cmp-long v4, v4, v2

    if-eqz v4, :cond_0

    .line 601
    iget-char v2, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c:C

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v2

    goto :goto_0

    .line 605
    .end local v0    # "norm32":J
    :cond_3
    iput-char v6, p0, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->c2:C

    goto :goto_0
.end method

.method public static getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 5

    .prologue
    .line 432
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl;->unicodeVersion:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    sget-object v1, Lcom/ibm/icu/impl/NormalizerImpl;->unicodeVersion:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl;->unicodeVersion:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->unicodeVersion:[B

    const/4 v4, 0x3

    aget-byte v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    return-object v0
.end method

.method private static insertOrdered([CIIICCI)I
    .locals 7
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "current"    # I
    .param p3, "p"    # I
    .param p4, "c"    # C
    .param p5, "c2"    # C
    .param p6, "cc"    # I

    .prologue
    .line 698
    move v5, p6

    .line 700
    .local v5, "trailCC":I
    if-ge p1, p2, :cond_2

    if-eqz p6, :cond_2

    .line 702
    move v0, p2

    .local v0, "back":I
    move v1, p2

    .line 703
    .local v1, "preBack":I
    new-instance v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;

    const/4 v6, 0x0

    invoke-direct {v2, v6}, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 704
    .local v2, "prevArgs":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    iput p2, v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 705
    iput p1, v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->start:I

    .line 706
    iput-object p0, v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->src:[C

    .line 708
    invoke-static {v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getPrevCC(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;)I

    move-result v3

    .line 709
    .local v3, "prevCC":I
    iget v1, v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 711
    if-ge p6, v3, :cond_2

    .line 713
    move v5, v3

    .line 714
    move v0, v1

    .line 715
    :goto_0
    if-ge p1, v1, :cond_0

    .line 716
    invoke-static {v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getPrevCC(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;)I

    move-result v3

    .line 717
    iget v1, v2, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 718
    if-lt p6, v3, :cond_4

    .line 732
    :cond_0
    move v4, p3

    .line 734
    .local v4, "r":I
    :cond_1
    add-int/lit8 v4, v4, -0x1

    add-int/lit8 p2, p2, -0x1

    aget-char v6, p0, p2

    aput-char v6, p0, v4

    .line 735
    if-ne v0, p2, :cond_1

    .line 740
    .end local v0    # "back":I
    .end local v1    # "preBack":I
    .end local v2    # "prevArgs":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    .end local v3    # "prevCC":I
    .end local v4    # "r":I
    :cond_2
    aput-char p4, p0, p2

    .line 741
    if-eqz p5, :cond_3

    .line 742
    add-int/lit8 v6, p2, 0x1

    aput-char p5, p0, v6

    .line 746
    :cond_3
    return v5

    .line 721
    .restart local v0    # "back":I
    .restart local v1    # "preBack":I
    .restart local v2    # "prevArgs":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    .restart local v3    # "prevCC":I
    :cond_4
    move v0, v1

    .line 722
    goto :goto_0
.end method

.method private static final declared-synchronized internalGetNX(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p0, "options"    # I

    .prologue
    .line 3798
    const-class v3, Lcom/ibm/icu/impl/NormalizerImpl;

    monitor-enter v3

    and-int/lit16 p0, p0, 0xff

    .line 3800
    :try_start_0
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v2, v2, p0

    if-nez v2, :cond_6

    .line 3802
    const/4 v2, 0x1

    if-ne p0, v2, :cond_0

    .line 3803
    invoke-static {}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXHangul()Lcom/ibm/icu/text/UnicodeSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3831
    :goto_0
    monitor-exit v3

    return-object v2

    .line 3805
    :cond_0
    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    .line 3806
    :try_start_1
    invoke-static {}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXCJKCompat()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    goto :goto_0

    .line 3808
    :cond_1
    and-int/lit16 v2, p0, 0xe0

    if-eqz v2, :cond_2

    and-int/lit8 v2, p0, 0x1f

    if-nez v2, :cond_2

    .line 3809
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXUnicode(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    goto :goto_0

    .line 3816
    :cond_2
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 3819
    .local v1, "set":Lcom/ibm/icu/text/UnicodeSet;
    and-int/lit8 v2, p0, 0x1

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXHangul()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    .local v0, "other":Lcom/ibm/icu/text/UnicodeSet;
    if-eqz v0, :cond_3

    .line 3820
    invoke-virtual {v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3822
    .end local v0    # "other":Lcom/ibm/icu/text/UnicodeSet;
    :cond_3
    and-int/lit8 v2, p0, 0x2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXCJKCompat()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    .restart local v0    # "other":Lcom/ibm/icu/text/UnicodeSet;
    if-eqz v0, :cond_4

    .line 3823
    invoke-virtual {v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3825
    .end local v0    # "other":Lcom/ibm/icu/text/UnicodeSet;
    :cond_4
    and-int/lit16 v2, p0, 0xe0

    if-eqz v2, :cond_5

    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->internalGetNXUnicode(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    .restart local v0    # "other":Lcom/ibm/icu/text/UnicodeSet;
    if-eqz v0, :cond_5

    .line 3826
    invoke-virtual {v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3829
    .end local v0    # "other":Lcom/ibm/icu/text/UnicodeSet;
    :cond_5
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aput-object v1, v2, p0

    .line 3831
    .end local v1    # "set":Lcom/ibm/icu/text/UnicodeSet;
    :cond_6
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v2, v2, p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3798
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static final declared-synchronized internalGetNXCJKCompat()Lcom/ibm/icu/text/UnicodeSet;
    .locals 14

    .prologue
    .line 3737
    const-class v8, Lcom/ibm/icu/impl/NormalizerImpl;

    monitor-enter v8

    :try_start_0
    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v9, 0x2

    aget-object v7, v7, v9

    if-nez v7, :cond_3

    .line 3742
    new-instance v3, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v7, "[:Ideographic:]"

    invoke-direct {v3, v7}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    .line 3745
    .local v3, "set":Lcom/ibm/icu/text/UnicodeSet;
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 3748
    .local v1, "hasDecomp":Lcom/ibm/icu/text/UnicodeSet;
    new-instance v2, Lcom/ibm/icu/text/UnicodeSetIterator;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSetIterator;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 3752
    .local v2, "it":Lcom/ibm/icu/text/UnicodeSetIterator;
    :cond_0
    invoke-virtual {v2}, Lcom/ibm/icu/text/UnicodeSetIterator;->nextRange()Z

    move-result v7

    if-eqz v7, :cond_2

    iget v7, v2, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    sget v9, Lcom/ibm/icu/text/UnicodeSetIterator;->IS_STRING:I

    if-eq v7, v9, :cond_2

    .line 3753
    iget v6, v2, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    .line 3754
    .local v6, "start":I
    iget v0, v2, Lcom/ibm/icu/text/UnicodeSetIterator;->codepointEnd:I

    .line 3755
    .local v0, "end":I
    :goto_0
    if-gt v6, v0, :cond_0

    .line 3756
    invoke-static {v6}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(I)J

    move-result-wide v4

    .line 3757
    .local v4, "norm32":J
    const-wide/16 v10, 0x4

    and-long/2addr v10, v4

    const-wide/16 v12, 0x0

    cmp-long v7, v10, v12

    if-lez v7, :cond_1

    .line 3758
    invoke-virtual {v1, v6}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 3760
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 3761
    goto :goto_0

    .line 3765
    .end local v0    # "end":I
    .end local v4    # "norm32":J
    .end local v6    # "start":I
    :cond_2
    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v9, 0x2

    aput-object v1, v7, v9

    .line 3769
    :cond_3
    sget-object v7, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v9, 0x2

    aget-object v7, v7, v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return-object v7

    .line 3737
    .end local v1    # "hasDecomp":Lcom/ibm/icu/text/UnicodeSet;
    .end local v2    # "it":Lcom/ibm/icu/text/UnicodeSetIterator;
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method

.method private static final declared-synchronized internalGetNXHangul()Lcom/ibm/icu/text/UnicodeSet;
    .locals 6

    .prologue
    .line 3728
    const-class v1, Lcom/ibm/icu/impl/NormalizerImpl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    if-nez v0, :cond_0

    .line 3729
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v2, 0x1

    new-instance v3, Lcom/ibm/icu/text/UnicodeSet;

    const v4, 0xac00

    const v5, 0xd7a3

    invoke-direct {v3, v4, v5}, Lcom/ibm/icu/text/UnicodeSet;-><init>(II)V

    aput-object v3, v0, v2

    .line 3731
    :cond_0
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v2, 0x1

    aget-object v0, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 3728
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static final declared-synchronized internalGetNXUnicode(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p0, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 3773
    const-class v2, Lcom/ibm/icu/impl/NormalizerImpl;

    monitor-enter v2

    and-int/lit16 p0, p0, 0xe0

    .line 3774
    if-nez p0, :cond_0

    .line 3793
    :goto_0
    monitor-exit v2

    return-object v1

    .line 3778
    :cond_0
    :try_start_0
    sget-object v3, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v3, v3, p0

    if-nez v3, :cond_1

    .line 3780
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 3782
    .local v0, "set":Lcom/ibm/icu/text/UnicodeSet;
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 3784
    :pswitch_0
    const-string/jumbo v1, "[:^Age=3.2:]"

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3790
    sget-object v1, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aput-object v0, v1, p0

    .line 3793
    .end local v0    # "set":Lcom/ibm/icu/text/UnicodeSet;
    :cond_1
    sget-object v1, Lcom/ibm/icu/impl/NormalizerImpl;->nxCache:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v1, v1, p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3773
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 3782
    nop

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
    .end packed-switch
.end method

.method public static isCanonSafeStart(I)Z
    .locals 3
    .param p0, "c"    # I

    .prologue
    const/4 v1, 0x0

    .line 2600
    sget-boolean v2, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_1:Z

    if-eqz v2, :cond_0

    .line 2601
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 2602
    .local v0, "aux":I
    and-int/lit16 v2, v0, 0x800

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 2604
    .end local v0    # "aux":I
    :cond_0
    return v1
.end method

.method public static isFullCompositionExclusion(I)Z
    .locals 3
    .param p0, "c"    # I

    .prologue
    const/4 v1, 0x0

    .line 2591
    sget-boolean v2, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_1:Z

    if-eqz v2, :cond_0

    .line 2592
    sget-object v2, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v2, p0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 2593
    .local v0, "aux":I
    and-int/lit16 v2, v0, 0x400

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 2595
    .end local v0    # "aux":I
    :cond_0
    return v1
.end method

.method private static isHangulWithoutJamoT(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 353
    const v0, 0xac00

    sub-int v0, p0, v0

    int-to-char p0, v0

    .line 354
    const/16 v0, 0x2ba4

    if-ge p0, v0, :cond_0

    rem-int/lit8 v0, p0, 0x1c

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isJamoVTNorm32JamoV(J)Z
    .locals 2
    .param p0, "norm32"    # J

    .prologue
    .line 387
    const-wide v0, 0xfff30000L

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNFDSafe(JII)Z
    .locals 8
    .param p0, "norm32"    # J
    .param p2, "ccOrQCMask"    # I
    .param p3, "decompQCMask"    # I

    .prologue
    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    .line 627
    int-to-long v4, p2

    and-long/2addr v4, p0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 639
    :cond_0
    :goto_0
    return v1

    .line 632
    :cond_1
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32Regular(J)Z

    move-result v3

    if-eqz v3, :cond_2

    int-to-long v4, p3

    and-long/2addr v4, p0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 633
    new-instance v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 635
    .local v0, "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    invoke-static {p0, p1, p3, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    .line 636
    iget v3, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 639
    .end local v0    # "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    :cond_2
    const-wide/32 v4, 0xff00

    and-long/2addr v4, p0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;J)Z
    .locals 12
    .param p0, "c"    # I
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "mask"    # J

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2762
    const-wide v6, 0xffffffffL

    and-long/2addr p2, v6

    .line 2766
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(I)J

    move-result-wide v2

    .line 2768
    .local v2, "norm32":J
    and-long v6, v2, p2

    cmp-long v5, v6, v10

    if-eqz v5, :cond_1

    move v1, v4

    .line 2796
    :cond_0
    :goto_0
    return v1

    .line 2772
    :cond_1
    sget-object v5, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    if-eq p1, v5, :cond_0

    sget-object v5, Lcom/ibm/icu/text/Normalizer;->NFKD:Lcom/ibm/icu/text/Normalizer$Mode;

    if-eq p1, v5, :cond_0

    sget-object v5, Lcom/ibm/icu/text/Normalizer;->NONE:Lcom/ibm/icu/text/Normalizer$Mode;

    if-eq p1, v5, :cond_0

    .line 2778
    const-wide/16 v6, 0x4

    and-long/2addr v6, v2

    cmp-long v5, v6, v10

    if-eqz v5, :cond_0

    .line 2783
    invoke-static {v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32HangulOrJamo(J)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2785
    int-to-char v5, p0

    invoke-static {v5}, Lcom/ibm/icu/impl/NormalizerImpl;->isHangulWithoutJamoT(C)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v4

    goto :goto_0

    .line 2790
    :cond_2
    sget-boolean v5, Lcom/ibm/icu/impl/NormalizerImpl;->isFormatVersion_2_2:Z

    if-nez v5, :cond_3

    move v1, v4

    .line 2791
    goto :goto_0

    .line 2795
    :cond_3
    sget-object v5, Lcom/ibm/icu/impl/NormalizerImpl$AuxTrieImpl;->auxTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v5, p0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    .line 2796
    .local v0, "aux":C
    int-to-long v6, v0

    const-wide/16 v8, 0x1000

    and-long/2addr v6, v8

    cmp-long v5, v6, v10

    if-eqz v5, :cond_0

    move v1, v4

    goto :goto_0
.end method

.method private static isNorm32HangulOrJamo(J)Z
    .locals 2
    .param p0, "norm32"    # J

    .prologue
    .line 371
    const-wide v0, 0xfff00000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNorm32LeadSurrogate(J)Z
    .locals 2
    .param p0, "norm32"    # J

    .prologue
    .line 366
    const-wide v0, 0xfc000000L

    cmp-long v0, v0, p0

    if-gtz v0, :cond_0

    const-wide v0, 0xfff00000L

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNorm32Regular(J)Z
    .locals 2
    .param p0, "norm32"    # J

    .prologue
    .line 361
    const-wide v0, 0xfc000000L

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrueStarter(JII)Z
    .locals 10
    .param p0, "norm32"    # J
    .param p2, "ccOrQCMask"    # I
    .param p3, "decompQCMask"    # I

    .prologue
    const/4 v3, 0x1

    const-wide/16 v8, 0x0

    .line 650
    int-to-long v4, p2

    and-long/2addr v4, p0

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    .line 671
    :cond_0
    :goto_0
    return v3

    .line 655
    :cond_1
    int-to-long v4, p3

    and-long/2addr v4, p0

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    .line 657
    new-instance v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 659
    .local v0, "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    invoke-static {p0, p1, p3, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose(JILcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;)I

    move-result v1

    .line 661
    .local v1, "p":I
    iget v4, v0, Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;->cc:I

    if-nez v4, :cond_2

    .line 662
    and-int/lit8 v2, p2, 0x3f

    .line 665
    .local v2, "qcMask":I
    sget-object v4, Lcom/ibm/icu/impl/NormalizerImpl;->extraData:[C

    invoke-static {v4, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32([CII)J

    move-result-wide v4

    int-to-long v6, v2

    and-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-eqz v4, :cond_0

    .line 671
    .end local v0    # "args":Lcom/ibm/icu/impl/NormalizerImpl$DecomposeArgs;
    .end local v1    # "p":I
    .end local v2    # "qcMask":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static makeFCD([CII[CIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 18
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2432
    move/from16 v4, p1

    .line 2433
    .local v4, "decompStart":I
    move/from16 v12, p4

    .line 2434
    .local v12, "destIndex":I
    const/16 v16, 0x0

    .line 2435
    .local v16, "prevCC":I
    const/4 v9, 0x0

    .line 2436
    .local v9, "c":C
    const/4 v14, 0x0

    .line 2437
    .local v14, "fcd16":I
    const/4 v3, 0x1

    new-array v7, v3, [I

    .line 2438
    .local v7, "destIndexArr":[I
    const/4 v3, 0x0

    aput v12, v7, v3

    .line 2443
    :cond_0
    :goto_0
    move/from16 v17, p1

    .line 2446
    .local v17, "prevSrc":I
    :goto_1
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_3

    .line 2469
    :cond_1
    move/from16 v0, p1

    move/from16 v1, v17

    if-eq v0, v1, :cond_10

    .line 2470
    sub-int v15, p1, v17

    .line 2471
    .local v15, "length":I
    add-int v3, v12, v15

    move/from16 v0, p5

    if-gt v3, v0, :cond_2

    .line 2472
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v12, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2474
    :cond_2
    add-int/2addr v12, v15

    .line 2475
    move/from16 v17, p1

    .line 2479
    if-gez v16, :cond_10

    .line 2482
    move/from16 v0, v16

    neg-int v3, v0

    move-object/from16 v0, p6

    invoke-static {v0, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2483
    move/from16 v0, v16

    neg-int v3, v0

    invoke-static {v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(I)I

    move-result v3

    and-int/lit16 v0, v3, 0xff

    move/from16 v16, v0

    .line 2492
    :goto_2
    add-int/lit8 v4, v17, -0x1

    move v13, v12

    .line 2503
    .end local v12    # "destIndex":I
    .end local v15    # "length":I
    .local v13, "destIndex":I
    :goto_3
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_6

    .line 2580
    sub-int v3, v13, p4

    return v3

    .line 2448
    .end local v13    # "destIndex":I
    .restart local v12    # "destIndex":I
    :cond_3
    aget-char v9, p0, p1

    const/16 v3, 0x300

    if-ge v9, v3, :cond_4

    .line 2449
    neg-int v0, v9

    move/from16 v16, v0

    .line 2455
    :goto_4
    add-int/lit8 p1, p1, 0x1

    .line 2456
    goto :goto_1

    .line 2450
    :cond_4
    invoke-static {v9}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v14

    if-nez v14, :cond_1

    .line 2451
    const/16 v16, 0x0

    goto :goto_4

    .line 2485
    .restart local v15    # "length":I
    :cond_5
    const/16 v16, 0x0

    goto :goto_2

    .line 2508
    .end local v12    # "destIndex":I
    .end local v15    # "length":I
    .restart local v13    # "destIndex":I
    :cond_6
    if-nez v16, :cond_7

    .line 2509
    move/from16 v4, v17

    .line 2513
    :cond_7
    add-int/lit8 p1, p1, 0x1

    .line 2516
    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2518
    move/from16 v0, p1

    move/from16 v1, p2

    if-eq v0, v1, :cond_b

    aget-char v10, p0, p1

    .local v10, "c2":C
    invoke-static {v10}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2520
    add-int/lit8 p1, p1, 0x1

    .line 2521
    int-to-char v3, v14

    invoke-static {v3, v10}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v14

    .line 2531
    :goto_5
    move-object/from16 v0, p6

    invoke-static {v0, v9, v10}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2532
    const/4 v14, 0x0

    .line 2535
    :cond_8
    shr-int/lit8 v11, v14, 0x8

    .line 2536
    .local v11, "cc":I
    if-eqz v11, :cond_9

    move/from16 v0, v16

    if-lt v11, v0, :cond_f

    .line 2538
    :cond_9
    if-nez v11, :cond_a

    .line 2539
    move/from16 v4, v17

    .line 2541
    :cond_a
    and-int/lit16 v0, v14, 0xff

    move/from16 v16, v0

    .line 2544
    if-nez v10, :cond_d

    const/4 v15, 0x1

    .line 2545
    .restart local v15    # "length":I
    :goto_6
    add-int v3, v13, v15

    move/from16 v0, p5

    if-gt v3, v0, :cond_e

    .line 2546
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "destIndex":I
    .restart local v12    # "destIndex":I
    aput-char v9, p3, v13

    .line 2547
    if-eqz v10, :cond_0

    .line 2548
    add-int/lit8 v13, v12, 0x1

    .end local v12    # "destIndex":I
    .restart local v13    # "destIndex":I
    aput-char v10, p3, v12

    move v12, v13

    .line 2549
    .end local v13    # "destIndex":I
    .restart local v12    # "destIndex":I
    goto/16 :goto_0

    .line 2523
    .end local v10    # "c2":C
    .end local v11    # "cc":I
    .end local v12    # "destIndex":I
    .end local v15    # "length":I
    .restart local v13    # "destIndex":I
    :cond_b
    const/4 v10, 0x0

    .line 2524
    .restart local v10    # "c2":C
    const/4 v14, 0x0

    .line 2526
    goto :goto_5

    .line 2527
    .end local v10    # "c2":C
    :cond_c
    const/4 v10, 0x0

    .restart local v10    # "c2":C
    goto :goto_5

    .line 2544
    .restart local v11    # "cc":I
    :cond_d
    const/4 v15, 0x2

    goto :goto_6

    .line 2551
    .restart local v15    # "length":I
    :cond_e
    add-int v12, v13, v15

    .line 2553
    .end local v13    # "destIndex":I
    .restart local v12    # "destIndex":I
    goto/16 :goto_0

    .line 2559
    .end local v12    # "destIndex":I
    .end local v15    # "length":I
    .restart local v13    # "destIndex":I
    :cond_f
    sub-int v3, v17, v4

    sub-int v12, v13, v3

    .line 2566
    .end local v13    # "destIndex":I
    .restart local v12    # "destIndex":I
    int-to-char v3, v14

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/impl/NormalizerImpl;->findSafeFCD([CIIC)I

    move-result p1

    .line 2572
    const/4 v3, 0x0

    aput v12, v7, v3

    move-object/from16 v3, p0

    move/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v8, p6

    .line 2573
    invoke-static/range {v3 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->decomposeFCD([CII[C[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v16

    .line 2575
    move/from16 v4, p1

    .line 2576
    const/4 v3, 0x0

    aget v12, v7, v3

    .line 2578
    goto/16 :goto_0

    .end local v10    # "c2":C
    .end local v11    # "cc":I
    :cond_10
    move v13, v12

    .end local v12    # "destIndex":I
    .restart local v13    # "destIndex":I
    goto/16 :goto_3
.end method

.method private static mergeOrdered([CII[CII)I
    .locals 7
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "current"    # I
    .param p3, "data"    # [C
    .param p4, "next"    # I
    .param p5, "limit"    # I

    .prologue
    .line 843
    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerImpl;->mergeOrdered([CII[CIIZ)I

    move-result v0

    return v0
.end method

.method private static mergeOrdered([CII[CIIZ)I
    .locals 13
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "current"    # I
    .param p3, "data"    # [C
    .param p4, "next"    # I
    .param p5, "limit"    # I
    .param p6, "isOrdered"    # Z

    .prologue
    .line 780
    const/4 v12, 0x0

    .line 783
    .local v12, "trailCC":I
    move/from16 v0, p4

    if-ne p2, v0, :cond_2

    const/4 v8, 0x1

    .line 784
    .local v8, "adjacent":Z
    :goto_0
    new-instance v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 785
    .local v10, "ncArgs":Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;
    move-object/from16 v0, p3

    iput-object v0, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->source:[C

    .line 786
    move/from16 v0, p4

    iput v0, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    .line 787
    move/from16 v0, p5

    iput v0, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    .line 789
    if-ne p1, p2, :cond_0

    if-nez p6, :cond_1

    .line 791
    :cond_0
    :goto_1
    iget v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    iget v2, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    if-ge v1, v2, :cond_1

    .line 792
    invoke-static {v10}, Lcom/ibm/icu/impl/NormalizerImpl;->getNextCC(Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;)I

    move-result v7

    .line 793
    .local v7, "cc":I
    if-nez v7, :cond_5

    .line 795
    const/4 v12, 0x0

    .line 796
    if-eqz v8, :cond_3

    .line 797
    iget p2, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    .line 804
    :goto_2
    if-eqz p6, :cond_4

    .line 818
    .end local v7    # "cc":I
    :cond_1
    iget v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    iget v2, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    if-ne v1, v2, :cond_7

    .line 833
    .end local v12    # "trailCC":I
    :goto_3
    return v12

    .line 783
    .end local v8    # "adjacent":Z
    .end local v10    # "ncArgs":Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;
    .restart local v12    # "trailCC":I
    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    .line 799
    .restart local v7    # "cc":I
    .restart local v8    # "adjacent":Z
    .restart local v10    # "ncArgs":Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;
    :cond_3
    add-int/lit8 v9, p2, 0x1

    .end local p2    # "current":I
    .local v9, "current":I
    iget-char v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c:C

    aput-char v1, p3, p2

    .line 800
    iget-char v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    if-eqz v1, :cond_a

    .line 801
    add-int/lit8 p2, v9, 0x1

    .end local v9    # "current":I
    .restart local p2    # "current":I
    iget-char v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    aput-char v1, p3, v9

    goto :goto_2

    .line 807
    :cond_4
    move p1, p2

    .line 809
    goto :goto_1

    .line 810
    :cond_5
    iget-char v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_4
    add-int v4, p2, v1

    .line 811
    .local v4, "r":I
    iget-char v5, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c:C

    iget-char v6, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->c2:C

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-static/range {v1 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->insertOrdered([CIIICCI)I

    move-result v12

    .line 813
    move p2, v4

    .line 815
    goto :goto_1

    .line 810
    .end local v4    # "r":I
    :cond_6
    const/4 v1, 0x2

    goto :goto_4

    .line 822
    .end local v7    # "cc":I
    :cond_7
    if-nez v8, :cond_8

    .line 825
    :goto_5
    add-int/lit8 v9, p2, 0x1

    .end local p2    # "current":I
    .restart local v9    # "current":I
    iget v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    aget-char v1, p3, v1

    aput-char v1, p0, p2

    .line 826
    iget v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->next:I

    iget v2, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    if-ne v1, v2, :cond_9

    .line 827
    iput v9, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    move p2, v9

    .line 829
    .end local v9    # "current":I
    .restart local p2    # "current":I
    :cond_8
    new-instance v11, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;

    const/4 v1, 0x0

    invoke-direct {v11, v1}, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 830
    .local v11, "prevArgs":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    move-object/from16 v0, p3

    iput-object v0, v11, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->src:[C

    .line 831
    iput p1, v11, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->start:I

    .line 832
    iget v1, v10, Lcom/ibm/icu/impl/NormalizerImpl$NextCCArgs;->limit:I

    iput v1, v11, Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;->current:I

    .line 833
    invoke-static {v11}, Lcom/ibm/icu/impl/NormalizerImpl;->getPrevCC(Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;)I

    move-result v12

    goto :goto_3

    .end local v11    # "prevArgs":Lcom/ibm/icu/impl/NormalizerImpl$PrevArgs;
    .end local p2    # "current":I
    .restart local v9    # "current":I
    :cond_9
    move p2, v9

    .end local v9    # "current":I
    .restart local p2    # "current":I
    goto :goto_5

    .end local p2    # "current":I
    .restart local v7    # "cc":I
    .restart local v9    # "current":I
    :cond_a
    move p2, v9

    .end local v9    # "current":I
    .restart local p2    # "current":I
    goto :goto_2
.end method

.method private static moveToNext(II)I
    .locals 1
    .param p0, "pos"    # I
    .param p1, "offset"    # I

    .prologue
    .line 3149
    if-lez p1, :cond_0

    .line 3150
    add-int v0, p0, p1

    .line 3153
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p0, 0x1

    goto :goto_0
.end method

.method private static final nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z
    .locals 1
    .param p0, "nx"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p1, "c"    # C
    .param p2, "c2"    # C

    .prologue
    .line 3848
    if-eqz p0, :cond_1

    if-nez p2, :cond_0

    .end local p1    # "c":C
    :goto_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .restart local p1    # "c":C
    :cond_0
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result p1

    goto :goto_0

    .end local p1    # "c":C
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static final nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z
    .locals 1
    .param p0, "nx"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p1, "c"    # I

    .prologue
    .line 3844
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final quickCheck(II)I
    .locals 4
    .param p0, "c"    # I
    .param p1, "modeValue"    # I

    .prologue
    .line 2855
    const/4 v2, 0x6

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    .line 2859
    .local v1, "qcMask":[I
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(I)J

    move-result-wide v2

    long-to-int v2, v2

    aget v3, v1, p1

    and-int v0, v2, v3

    .line 2861
    .local v0, "norm32":I
    if-nez v0, :cond_0

    .line 2862
    const/4 v2, 0x1

    .line 2866
    :goto_0
    return v2

    .line 2863
    :cond_0
    and-int/lit8 v2, v0, 0xf

    if-eqz v2, :cond_1

    .line 2864
    const/4 v2, 0x0

    goto :goto_0

    .line 2866
    :cond_1
    const/4 v2, 0x2

    goto :goto_0

    .line 2855
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x4
        0x8
        0x11
        0x22
    .end array-data
.end method

.method public static quickCheck([CIIIIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 27
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "minNoMaybe"    # I
    .param p4, "qcMask"    # I
    .param p5, "options"    # I
    .param p6, "allowMaybe"    # Z
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 934
    new-instance v16, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 936
    .local v16, "args":Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;
    move/from16 v5, p1

    .line 938
    .local v5, "start":I
    sget-boolean v4, Lcom/ibm/icu/impl/NormalizerImpl;->isDataLoaded:Z

    if-nez v4, :cond_0

    .line 939
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->MAYBE:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 1035
    :goto_0
    return-object v23

    .line 942
    :cond_0
    const v4, 0xff00

    or-int v7, v4, p4

    .line 943
    .local v7, "ccOrQCMask":I
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 944
    .local v23, "result":Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    const/16 v22, 0x0

    .local v22, "prevCC":C
    move/from16 v26, p1

    .line 948
    .end local p1    # "srcStart":I
    .local v26, "srcStart":I
    :goto_1
    move/from16 v0, v26

    move/from16 v1, p2

    if-ne v0, v1, :cond_1

    move/from16 p1, v26

    .line 949
    .end local v26    # "srcStart":I
    .restart local p1    # "srcStart":I
    goto :goto_0

    .line 950
    .end local p1    # "srcStart":I
    .restart local v26    # "srcStart":I
    :cond_1
    add-int/lit8 p1, v26, 0x1

    .end local v26    # "srcStart":I
    .restart local p1    # "srcStart":I
    aget-char v17, p0, v26

    .local v17, "c":C
    move/from16 v0, v17

    move/from16 v1, p3

    if-lt v0, v1, :cond_3

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v20

    .local v20, "norm32":J
    int-to-long v10, v7

    and-long v10, v10, v20

    const-wide/16 v12, 0x0

    cmp-long v4, v10, v12

    if-eqz v4, :cond_3

    .line 959
    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/impl/NormalizerImpl;->isNorm32LeadSurrogate(J)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 961
    move/from16 v0, p1

    move/from16 v1, p2

    if-eq v0, v1, :cond_4

    aget-char v18, p0, p1

    .local v18, "c2":C
    invoke-static/range {v18 .. v18}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 962
    add-int/lit8 p1, p1, 0x1

    .line 963
    move-wide/from16 v0, v20

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v20

    .line 971
    :goto_2
    move-object/from16 v0, p7

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 973
    const-wide/16 v20, 0x0

    .line 977
    :cond_2
    const/16 v4, 0x8

    shr-long v10, v20, v4

    const-wide/16 v12, 0xff

    and-long/2addr v10, v12

    long-to-int v4, v10

    int-to-char v0, v4

    move/from16 v19, v0

    .line 978
    .local v19, "cc":C
    if-eqz v19, :cond_6

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    .line 979
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    goto :goto_0

    .line 954
    .end local v18    # "c2":C
    .end local v19    # "cc":C
    .end local v20    # "norm32":J
    :cond_3
    const/16 v22, 0x0

    move/from16 v26, p1

    .line 955
    .end local p1    # "srcStart":I
    .restart local v26    # "srcStart":I
    goto :goto_1

    .line 965
    .end local v26    # "srcStart":I
    .restart local v20    # "norm32":J
    .restart local p1    # "srcStart":I
    :cond_4
    const-wide/16 v20, 0x0

    .line 966
    const/16 v18, 0x0

    .line 968
    .restart local v18    # "c2":C
    goto :goto_2

    .line 969
    .end local v18    # "c2":C
    :cond_5
    const/16 v18, 0x0

    .restart local v18    # "c2":C
    goto :goto_2

    .line 981
    .restart local v19    # "cc":C
    :cond_6
    move/from16 v22, v19

    .line 984
    move/from16 v0, p4

    int-to-long v10, v0

    and-long v24, v20, v10

    .line 985
    .local v24, "qcNorm32":J
    const-wide/16 v10, 0xf

    and-long v10, v10, v24

    const-wide/16 v12, 0x1

    cmp-long v4, v10, v12

    if-ltz v4, :cond_7

    .line 986
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 987
    goto/16 :goto_0

    .line 988
    :cond_7
    const-wide/16 v10, 0x0

    cmp-long v4, v24, v10

    if-eqz v4, :cond_b

    .line 990
    if-eqz p6, :cond_8

    .line 991
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->MAYBE:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move/from16 v26, p1

    .line 992
    .end local p1    # "srcStart":I
    .restart local v26    # "srcStart":I
    goto/16 :goto_1

    .line 998
    .end local v26    # "srcStart":I
    .restart local p1    # "srcStart":I
    :cond_8
    shl-int/lit8 v4, p4, 0x2

    and-int/lit8 v8, v4, 0xf

    .line 1003
    .local v8, "decompQCMask":I
    add-int/lit8 v6, p1, -0x1

    .line 1004
    .local v6, "prevStarter":I
    aget-char v4, p0, v6

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1007
    add-int/lit8 v6, v6, -0x1

    .line 1010
    :cond_9
    move/from16 v0, p3

    int-to-char v9, v0

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v9}, Lcom/ibm/icu/impl/NormalizerImpl;->findPreviousStarter([CIIIIC)I

    move-result v6

    .line 1016
    move/from16 v0, p3

    int-to-char v14, v0

    move-object/from16 v9, p0

    move/from16 v10, p1

    move/from16 v11, p2

    move/from16 v12, p4

    move v13, v8

    invoke-static/range {v9 .. v14}, Lcom/ibm/icu/impl/NormalizerImpl;->findNextStarter([CIIIIC)I

    move-result p1

    .line 1020
    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->prevCC:I

    move-object/from16 v9, v16

    move v10, v6

    move-object/from16 v11, p0

    move/from16 v12, p1

    move/from16 v13, p2

    move/from16 v14, p5

    move-object/from16 v15, p7

    .line 1023
    invoke-static/range {v9 .. v15}, Lcom/ibm/icu/impl/NormalizerImpl;->composePart(Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;I[CIIILcom/ibm/icu/text/UnicodeSet;)[C

    move-result-object v9

    .line 1026
    .local v9, "buffer":[C
    const/4 v10, 0x0

    move-object/from16 v0, v16

    iget v11, v0, Lcom/ibm/icu/impl/NormalizerImpl$ComposePartArgs;->length:I

    const/4 v15, 0x0

    move-object/from16 v12, p0

    move v13, v6

    move/from16 v14, p1

    invoke-static/range {v9 .. v15}, Lcom/ibm/icu/impl/NormalizerImpl;->strCompare([CII[CIIZ)I

    move-result v4

    if-eqz v4, :cond_a

    .line 1027
    sget-object v23, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 1028
    goto/16 :goto_0

    :cond_a
    move/from16 v26, p1

    .line 1033
    .end local p1    # "srcStart":I
    .restart local v26    # "srcStart":I
    goto/16 :goto_1

    .end local v6    # "prevStarter":I
    .end local v8    # "decompQCMask":I
    .end local v9    # "buffer":[C
    .end local v26    # "srcStart":I
    .restart local p1    # "srcStart":I
    :cond_b
    move/from16 v26, p1

    .end local p1    # "srcStart":I
    .restart local v26    # "srcStart":I
    goto/16 :goto_1
.end method

.method private static recompose(Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;ILcom/ibm/icu/text/UnicodeSet;)C
    .locals 22
    .param p0, "args"    # Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;
    .param p1, "options"    # I
    .param p2, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1519
    const/16 v17, 0x0

    .local v17, "value":I
    const/16 v18, 0x0

    .line 1523
    .local v18, "value2":I
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v7, v0, [I

    .line 1524
    .local v7, "outValues":[I
    const/4 v15, -0x1

    .line 1525
    .local v15, "starter":I
    const/4 v5, 0x0

    .line 1526
    .local v5, "combineFwdIndex":I
    const/16 v16, 0x0

    .line 1527
    .local v16, "starterIsSupplementary":Z
    const/4 v8, 0x0

    .line 1529
    .local v8, "prevCC":I
    new-instance v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;-><init>(Lcom/ibm/icu/impl/NormalizerImpl$1;)V

    .line 1530
    .local v6, "ncArg":Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-object v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->source:[C

    .line 1532
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    .line 1533
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1536
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    .line 1537
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p2

    invoke-static {v6, v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNextCombining(Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v4

    .line 1538
    .local v4, "combineFlags":I
    iget v3, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->combiningIndex:I

    .line 1539
    .local v3, "combineBackIndex":I
    iget v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->start:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1541
    and-int/lit16 v0, v4, 0x80

    move/from16 v19, v0

    if-eqz v19, :cond_e

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v15, v0, :cond_e

    .line 1542
    const v19, 0x8000

    and-int v19, v19, v3

    if-eqz v19, :cond_a

    .line 1547
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x100

    move/from16 v19, v0

    if-nez v19, :cond_1

    if-nez v8, :cond_e

    .line 1548
    :cond_1
    const/4 v13, -0x1

    .line 1549
    .local v13, "remove":I
    const/4 v4, 0x0

    .line 1550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    aget-char v19, v19, v15

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1551
    const v19, 0xfff2

    move/from16 v0, v19

    if-ne v3, v0, :cond_6

    .line 1555
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    move/from16 v0, v19

    add-int/lit16 v0, v0, -0x1100

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1556
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    const/16 v20, 0x13

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    .line 1557
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x1

    .line 1558
    const v19, 0xac00

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v20, v0

    mul-int/lit8 v20, v20, 0x15

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v21, v0

    move/from16 v0, v21

    add-int/lit16 v0, v0, -0x1161

    move/from16 v21, v0

    add-int v20, v20, v21

    mul-int/lit8 v20, v20, 0x1c

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    .line 1560
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v20, v0

    aget-char v19, v19, v20

    move/from16 v0, v19

    add-int/lit16 v0, v0, -0x11a7

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    const/16 v20, 0x1c

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    .line 1563
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1564
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v19, v0

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    .line 1569
    :goto_1
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v19, v0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v19

    if-nez v19, :cond_4

    .line 1570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v20, v0

    aput-char v20, v19, v15

    .line 1601
    :cond_2
    :goto_2
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v13, v0, :cond_8

    .line 1603
    move v9, v13

    .line 1604
    .local v9, "q":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1605
    .local v11, "r":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_7

    .line 1606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "q":I
    .local v10, "q":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "r":I
    .local v12, "r":I
    aget-char v20, v20, v11

    aput-char v20, v19, v9

    move v11, v12

    .end local v12    # "r":I
    .restart local v11    # "r":I
    move v9, v10

    .line 1607
    .end local v10    # "q":I
    .restart local v9    # "q":I
    goto :goto_3

    .line 1567
    .end local v9    # "q":I
    .end local v11    # "r":I
    :cond_3
    const/16 v4, 0x40

    goto :goto_1

    .line 1573
    :cond_4
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/NormalizerImpl;->isHangulWithoutJamoT(C)Z

    move-result v19

    if-nez v19, :cond_5

    .line 1574
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1577
    :cond_5
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1579
    goto :goto_2

    .line 1592
    :cond_6
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/NormalizerImpl;->isHangulWithoutJamoT(C)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1593
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c:C

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v0, v0, -0x11a7

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1594
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;I)Z

    move-result v19

    if-nez v19, :cond_2

    .line 1595
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x1

    .line 1596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v20, v0

    aput-char v20, v19, v15

    goto/16 :goto_2

    .line 1608
    .restart local v9    # "q":I
    .restart local v11    # "r":I
    :cond_7
    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1609
    move-object/from16 v0, p0

    iput v9, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    .line 1612
    .end local v9    # "q":I
    .end local v11    # "r":I
    :cond_8
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    .line 1614
    if-eqz v4, :cond_e

    .line 1621
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    .line 1622
    int-to-char v0, v8

    move/from16 v19, v0

    .line 1730
    .end local v13    # "remove":I
    :goto_4
    return v19

    .line 1626
    .restart local v13    # "remove":I
    :cond_9
    const v5, 0xfff0

    .line 1629
    goto/16 :goto_0

    .line 1642
    .end local v13    # "remove":I
    :cond_a
    const v19, 0x8000

    and-int v19, v19, v5

    if-nez v19, :cond_e

    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x100

    move/from16 v19, v0

    if-eqz v19, :cond_d

    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v8, v0, :cond_b

    if-nez v8, :cond_e

    :cond_b
    sget-object v19, Lcom/ibm/icu/impl/NormalizerImpl;->combiningTable:[C

    move-object/from16 v0, v19

    invoke-static {v0, v5, v3, v7}, Lcom/ibm/icu/impl/NormalizerImpl;->combine([CII[I)I

    move-result v14

    .local v14, "result":I
    if-eqz v14, :cond_e

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    int-to-char v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->nx_contains(Lcom/ibm/icu/text/UnicodeSet;CC)Z

    move-result v19

    if-nez v19, :cond_e

    .line 1655
    const/16 v19, 0x0

    aget v17, v7, v19

    .line 1656
    const/16 v19, 0x1

    aget v18, v7, v19

    .line 1660
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    if-nez v19, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x1

    .line 1663
    .restart local v13    # "remove":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v20, v0

    aput-char v20, v19, v15

    .line 1664
    if-eqz v16, :cond_12

    .line 1665
    if-eqz v18, :cond_10

    .line 1667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    add-int/lit8 v20, v15, 0x1

    move/from16 v0, v18

    int-to-char v0, v0

    move/from16 v21, v0

    aput-char v21, v19, v20

    .line 1697
    :cond_c
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v13, v0, :cond_15

    .line 1698
    move v9, v13

    .line 1699
    .restart local v9    # "q":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1700
    .restart local v11    # "r":I
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_14

    .line 1701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "q":I
    .restart local v10    # "q":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v20, v0

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "r":I
    .restart local v12    # "r":I
    aget-char v20, v20, v11

    aput-char v20, v19, v9

    move v11, v12

    .end local v12    # "r":I
    .restart local v11    # "r":I
    move v9, v10

    .line 1702
    .end local v10    # "q":I
    .restart local v9    # "q":I
    goto :goto_7

    .line 1642
    .end local v9    # "q":I
    .end local v11    # "r":I
    .end local v13    # "remove":I
    .end local v14    # "result":I
    :cond_d
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v8, v0, :cond_b

    if-eqz v8, :cond_b

    .line 1728
    :cond_e
    iget-char v8, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    .line 1729
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_18

    .line 1730
    int-to-char v0, v8

    move/from16 v19, v0

    goto/16 :goto_4

    .line 1660
    .restart local v14    # "result":I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x2

    goto :goto_5

    .line 1671
    .restart local v13    # "remove":I
    :cond_10
    const/16 v16, 0x0

    .line 1672
    add-int/lit8 v9, v15, 0x1

    .line 1673
    .restart local v9    # "q":I
    add-int/lit8 v11, v9, 0x1

    .restart local v11    # "r":I
    move v12, v11

    .end local v11    # "r":I
    .restart local v12    # "r":I
    move v10, v9

    .line 1674
    .end local v9    # "q":I
    .restart local v10    # "q":I
    :goto_8
    if-ge v12, v13, :cond_11

    .line 1675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "q":I
    .restart local v9    # "q":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v20, v0

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "r":I
    .restart local v11    # "r":I
    aget-char v20, v20, v12

    aput-char v20, v19, v10

    move v12, v11

    .end local v11    # "r":I
    .restart local v12    # "r":I
    move v10, v9

    .line 1676
    .end local v9    # "q":I
    .restart local v10    # "q":I
    goto :goto_8

    .line 1677
    :cond_11
    add-int/lit8 v13, v13, -0x1

    .line 1679
    goto :goto_6

    .end local v10    # "q":I
    .end local v12    # "r":I
    :cond_12
    if-eqz v18, :cond_c

    .line 1682
    const/16 v16, 0x1

    .line 1684
    add-int/lit8 v15, v15, 0x1

    .line 1685
    move v9, v13

    .line 1686
    .restart local v9    # "q":I
    add-int/lit8 v13, v13, 0x1

    move v11, v13

    .line 1687
    .restart local v11    # "r":I
    :goto_9
    if-ge v15, v9, :cond_13

    .line 1688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    add-int/lit8 v11, v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v20, v0

    add-int/lit8 v9, v9, -0x1

    aget-char v20, v20, v9

    aput-char v20, v19, v11

    goto :goto_9

    .line 1690
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->source:[C

    move-object/from16 v19, v0

    move/from16 v0, v18

    int-to-char v0, v0

    move/from16 v20, v0

    aput-char v20, v19, v15

    .line 1691
    add-int/lit8 v15, v15, -0x1

    goto/16 :goto_6

    .line 1703
    :cond_14
    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    .line 1704
    move-object/from16 v0, p0

    iput v9, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    .line 1710
    .end local v9    # "q":I
    .end local v11    # "r":I
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->limit:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_16

    .line 1711
    int-to-char v0, v8

    move/from16 v19, v0

    goto/16 :goto_4

    .line 1715
    :cond_16
    const/16 v19, 0x1

    move/from16 v0, v19

    if-le v14, v0, :cond_17

    .line 1716
    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v18

    int-to-char v0, v0

    move/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/ibm/icu/impl/NormalizerImpl;->getCombiningIndexFromStarter(CC)I

    move-result v5

    .line 1718
    goto/16 :goto_0

    .line 1719
    :cond_17
    const/4 v15, -0x1

    .line 1723
    goto/16 :goto_0

    .line 1734
    .end local v13    # "remove":I
    .end local v14    # "result":I
    :cond_18
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->cc:C

    move/from16 v19, v0

    if-nez v19, :cond_1b

    .line 1736
    and-int/lit8 v19, v4, 0x40

    if-eqz v19, :cond_1a

    .line 1738
    iget-char v0, v6, Lcom/ibm/icu/impl/NormalizerImpl$NextCombiningArgs;->c2:C

    move/from16 v19, v0

    if-nez v19, :cond_19

    .line 1739
    const/16 v16, 0x0

    .line 1740
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v15, v19, -0x1

    .line 1745
    :goto_a
    move v5, v3

    .line 1746
    goto/16 :goto_0

    .line 1742
    :cond_19
    const/16 v16, 0x0

    .line 1743
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/NormalizerImpl$RecomposeArgs;->start:I

    move/from16 v19, v0

    add-int/lit8 v15, v19, -0x2

    goto :goto_a

    .line 1748
    :cond_1a
    const/4 v15, -0x1

    .line 1750
    goto/16 :goto_0

    :cond_1b
    move/from16 v0, p1

    and-int/lit16 v0, v0, 0x2000

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 1752
    const/4 v15, -0x1

    .line 1753
    goto/16 :goto_0
.end method

.method private static strCompare([CII[CIIZ)I
    .locals 10
    .param p0, "s1"    # [C
    .param p1, "s1Start"    # I
    .param p2, "s1Limit"    # I
    .param p3, "s2"    # [C
    .param p4, "s2Start"    # I
    .param p5, "s2Limit"    # I
    .param p6, "codePointOrder"    # Z

    .prologue
    .line 3553
    move v7, p1

    .line 3554
    .local v7, "start1":I
    move v8, p4

    .line 3558
    .local v8, "start2":I
    sub-int v2, p2, p1

    .line 3559
    .local v2, "length1":I
    sub-int v3, p5, p4

    .line 3563
    .local v3, "length2":I
    if-ge v2, v3, :cond_1

    .line 3564
    const/4 v4, -0x1

    .line 3565
    .local v4, "lengthResult":I
    add-int v5, v7, v2

    .line 3574
    .local v5, "limit1":I
    :goto_0
    if-ne p0, p3, :cond_4

    .line 3632
    .end local v4    # "lengthResult":I
    :cond_0
    :goto_1
    return v4

    .line 3566
    .end local v5    # "limit1":I
    :cond_1
    if-ne v2, v3, :cond_2

    .line 3567
    const/4 v4, 0x0

    .line 3568
    .restart local v4    # "lengthResult":I
    add-int v5, v7, v2

    .line 3569
    .restart local v5    # "limit1":I
    goto :goto_0

    .line 3570
    .end local v4    # "lengthResult":I
    .end local v5    # "limit1":I
    :cond_2
    const/4 v4, 0x1

    .line 3571
    .restart local v4    # "lengthResult":I
    add-int v5, v7, v3

    .restart local v5    # "limit1":I
    goto :goto_0

    .line 3589
    .local v0, "c1":C
    .local v1, "c2":C
    :cond_3
    add-int/lit8 p1, p1, 0x1

    .line 3590
    add-int/lit8 p4, p4, 0x1

    .line 3580
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_4
    if-eq p1, v5, :cond_0

    .line 3584
    aget-char v0, p0, p1

    .line 3585
    .restart local v0    # "c1":C
    aget-char v1, p3, p4

    .line 3586
    .restart local v1    # "c2":C
    if-eq v0, v1, :cond_3

    .line 3594
    add-int v5, v7, v2

    .line 3595
    add-int v6, v8, v3

    .line 3599
    .local v6, "limit2":I
    const v9, 0xd800

    if-lt v0, v9, :cond_8

    const v9, 0xd800

    if-lt v1, v9, :cond_8

    if-eqz p6, :cond_8

    .line 3602
    const v9, 0xdbff

    if-gt v0, v9, :cond_5

    add-int/lit8 v9, p1, 0x1

    if-eq v9, v5, :cond_5

    add-int/lit8 v9, p1, 0x1

    aget-char v9, p0, v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-nez v9, :cond_6

    :cond_5
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_9

    if-eq v7, p1, :cond_9

    add-int/lit8 v9, p1, -0x1

    aget-char v9, p0, v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 3616
    :cond_6
    :goto_2
    const v9, 0xdbff

    if-gt v1, v9, :cond_7

    add-int/lit8 v9, p4, 0x1

    if-eq v9, v6, :cond_7

    add-int/lit8 v9, p4, 0x1

    aget-char v9, p3, v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-nez v9, :cond_8

    :cond_7
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_a

    if-eq v8, p4, :cond_a

    add-int/lit8 v9, p4, -0x1

    aget-char v9, p3, v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 3632
    :cond_8
    :goto_3
    sub-int v4, v0, v1

    goto :goto_1

    .line 3613
    :cond_9
    add-int/lit16 v9, v0, -0x2800

    int-to-char v0, v9

    goto :goto_2

    .line 3627
    :cond_a
    add-int/lit16 v9, v1, -0x2800

    int-to-char v1, v9

    goto :goto_3
.end method


# virtual methods
.method public getFCDTrie()Lcom/ibm/icu/impl/CharTrie;
    .locals 1

    .prologue
    .line 2881
    sget-object v0, Lcom/ibm/icu/impl/NormalizerImpl$FCDTrieImpl;->fcdTrie:Lcom/ibm/icu/impl/CharTrie;

    return-object v0
.end method
