.class public final Lcom/ibm/icu/impl/Utility;
.super Ljava/lang/Object;
.source "Utility.java"


# static fields
.field private static final APOSTROPHE:C = '\''

.field private static final BACKSLASH:C = '\\'

.field static final DIGITS:[C

.field private static final ESCAPE:C = '\ua5a5'

.field static final ESCAPE_BYTE:B = -0x5bt

.field static final HEX_DIGIT:[C

.field private static final INT_CONST:[Ljava/lang/Integer;

.field public static LINE_SEPARATOR:Ljava/lang/String; = null

.field private static final MAGIC_UNSIGNED:I = -0x80000000

.field private static final MAX_INT_CONST:I = 0x40

.field private static final UNESCAPE_MAP:[C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x40

    const/16 v2, 0x10

    .line 600
    const-string/jumbo v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/Utility;->LINE_SEPARATOR:Ljava/lang/String;

    .line 655
    new-array v1, v2, [C

    fill-array-data v1, :array_0

    sput-object v1, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    .line 725
    new-array v1, v2, [C

    fill-array-data v1, :array_1

    sput-object v1, Lcom/ibm/icu/impl/Utility;->UNESCAPE_MAP:[C

    .line 1367
    const/16 v1, 0x24

    new-array v1, v1, [C

    fill-array-data v1, :array_2

    sput-object v1, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    .line 1930
    new-array v1, v3, [Ljava/lang/Integer;

    sput-object v1, Lcom/ibm/icu/impl/Utility;->INT_CONST:[Ljava/lang/Integer;

    .line 1933
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 1934
    sget-object v1, Lcom/ibm/icu/impl/Utility;->INT_CONST:[Ljava/lang/Integer;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v1, v0

    .line 1933
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1936
    :cond_0
    return-void

    .line 655
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data

    .line 725
    :array_1
    .array-data 2
        0x61s
        0x7s
        0x62s
        0x8s
        0x65s
        0x1bs
        0x66s
        0xcs
        0x6es
        0xas
        0x72s
        0xds
        0x74s
        0x9s
        0x76s
        0xbs
    .end array-data

    .line 1367
    :array_2
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final RLEStringToByteArray(Ljava/lang/String;)[B
    .locals 15
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v14, -0x5b

    .line 531
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    shl-int/lit8 v12, v12, 0x10

    const/4 v13, 0x1

    invoke-virtual {p0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    or-int v8, v12, v13

    .line 532
    .local v8, "length":I
    new-array v2, v8, [B

    .line 533
    .local v2, "array":[B
    const/4 v9, 0x1

    .line 534
    .local v9, "nextChar":Z
    const/4 v4, 0x0

    .line 535
    .local v4, "c":C
    const/4 v10, 0x0

    .line 536
    .local v10, "node":I
    const/4 v11, 0x0

    .line 537
    .local v11, "runLength":I
    const/4 v5, 0x2

    .line 538
    .local v5, "i":I
    const/4 v0, 0x0

    .local v0, "ai":I
    move v1, v0

    .end local v0    # "ai":I
    .local v1, "ai":I
    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    :goto_0
    if-ge v1, v8, :cond_5

    .line 544
    if-eqz v9, :cond_0

    .line 545
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 546
    shr-int/lit8 v12, v4, 0x8

    int-to-byte v3, v12

    .line 547
    .local v3, "b":B
    const/4 v9, 0x0

    .line 558
    :goto_1
    packed-switch v10, :pswitch_data_0

    move v0, v1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    :goto_2
    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    move v6, v5

    .line 589
    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_0

    .line 550
    .end local v3    # "b":B
    :cond_0
    and-int/lit16 v12, v4, 0xff

    int-to-byte v3, v12

    .line 551
    .restart local v3    # "b":B
    const/4 v9, 0x1

    move v5, v6

    .end local v6    # "i":I
    .restart local v5    # "i":I
    goto :goto_1

    .line 561
    :pswitch_0
    if-ne v3, v14, :cond_1

    .line 562
    const/4 v10, 0x1

    move v0, v1

    .line 563
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_2

    .line 565
    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :cond_1
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput-byte v3, v2, v1

    goto :goto_2

    .line 571
    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :pswitch_1
    if-ne v3, v14, :cond_2

    .line 572
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput-byte v14, v2, v1

    .line 573
    const/4 v10, 0x0

    .line 574
    goto :goto_2

    .line 576
    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :cond_2
    move v11, v3

    .line 578
    if-gez v11, :cond_3

    add-int/lit16 v11, v11, 0x100

    .line 579
    :cond_3
    const/4 v10, 0x2

    move v0, v1

    .line 581
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_2

    .line 585
    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :pswitch_2
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    if-ge v7, v11, :cond_4

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput-byte v3, v2, v1

    add-int/lit8 v7, v7, 0x1

    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    goto :goto_3

    .line 586
    :cond_4
    const/4 v10, 0x0

    move v0, v1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_2

    .line 591
    .end local v0    # "ai":I
    .end local v3    # "b":B
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v1    # "ai":I
    .restart local v6    # "i":I
    :cond_5
    if-eqz v10, :cond_6

    .line 592
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string/jumbo v13, "Bad run-length encoded byte array"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 594
    :cond_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    if-eq v6, v12, :cond_7

    .line 595
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string/jumbo v13, "Excess data in RLE byte array string"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 597
    :cond_7
    return-object v2

    .line 558
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static final RLEStringToCharArray(Ljava/lang/String;)[C
    .locals 12
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const v11, 0xa5a5

    .line 501
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    shl-int/lit8 v9, v9, 0x10

    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    or-int v6, v9, v10

    .line 502
    .local v6, "length":I
    new-array v2, v6, [C

    .line 503
    .local v2, "array":[C
    const/4 v0, 0x0

    .line 504
    .local v0, "ai":I
    const/4 v4, 0x2

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 505
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 506
    .local v3, "c":C
    if-ne v3, v11, :cond_2

    .line 507
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 508
    if-ne v3, v11, :cond_0

    .line 509
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "ai":I
    .local v1, "ai":I
    aput-char v3, v2, v0

    move v0, v1

    .line 504
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 511
    :cond_0
    move v7, v3

    .line 512
    .local v7, "runLength":I
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 513
    .local v8, "runValue":C
    const/4 v5, 0x0

    .local v5, "j":I
    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :goto_2
    if-ge v5, v7, :cond_1

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput-char v8, v2, v1

    add-int/lit8 v5, v5, 0x1

    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    goto :goto_2

    :cond_1
    move v0, v1

    .line 515
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_1

    .line 517
    .end local v5    # "j":I
    .end local v7    # "runLength":I
    .end local v8    # "runValue":C
    :cond_2
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    aput-char v3, v2, v0

    move v0, v1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_1

    .line 521
    .end local v3    # "c":C
    :cond_3
    if-eq v0, v6, :cond_4

    .line 522
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "Bad run-length encoded short array"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 524
    :cond_4
    return-object v2
.end method

.method public static final RLEStringToIntArray(Ljava/lang/String;)[I
    .locals 13
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const v12, 0xa5a5

    .line 432
    const/4 v11, 0x0

    invoke-static {p0, v11}, Lcom/ibm/icu/impl/Utility;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 433
    .local v7, "length":I
    new-array v2, v7, [I

    .line 434
    .local v2, "array":[I
    const/4 v0, 0x0

    .local v0, "ai":I
    const/4 v4, 0x1

    .line 436
    .local v4, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v11

    div-int/lit8 v8, v11, 0x2

    .local v8, "maxI":I
    move v5, v4

    .end local v4    # "i":I
    .local v5, "i":I
    move v1, v0

    .line 437
    .end local v0    # "ai":I
    .local v1, "ai":I
    :goto_0
    if-ge v1, v7, :cond_3

    if-ge v5, v8, :cond_3

    .line 438
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    invoke-static {p0, v5}, Lcom/ibm/icu/impl/Utility;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 440
    .local v3, "c":I
    if-ne v3, v12, :cond_2

    .line 441
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .restart local v5    # "i":I
    invoke-static {p0, v4}, Lcom/ibm/icu/impl/Utility;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 442
    if-ne v3, v12, :cond_0

    .line 443
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput v3, v2, v1

    move v4, v5

    .end local v5    # "i":I
    .restart local v4    # "i":I
    :goto_1
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    move v1, v0

    .line 455
    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    goto :goto_0

    .line 445
    :cond_0
    move v9, v3

    .line 446
    .local v9, "runLength":I
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    invoke-static {p0, v5}, Lcom/ibm/icu/impl/Utility;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 447
    .local v10, "runValue":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    if-ge v6, v9, :cond_1

    .line 448
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput v10, v2, v1

    .line 447
    add-int/lit8 v6, v6, 0x1

    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    goto :goto_2

    :cond_1
    move v0, v1

    .line 451
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_1

    .line 453
    .end local v0    # "ai":I
    .end local v6    # "j":I
    .end local v9    # "runLength":I
    .end local v10    # "runValue":I
    .restart local v1    # "ai":I
    :cond_2
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput v3, v2, v1

    goto :goto_1

    .line 457
    .end local v0    # "ai":I
    .end local v3    # "c":I
    .end local v4    # "i":I
    .restart local v1    # "ai":I
    .restart local v5    # "i":I
    :cond_3
    if-ne v1, v7, :cond_4

    if-eq v5, v8, :cond_5

    .line 458
    :cond_4
    new-instance v11, Ljava/lang/IllegalStateException;

    const-string/jumbo v12, "Bad run-length encoded int array"

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 461
    :cond_5
    return-object v2
.end method

.method public static final RLEStringToShortArray(Ljava/lang/String;)[S
    .locals 12
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const v11, 0xa5a5

    .line 471
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    shl-int/lit8 v9, v9, 0x10

    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    or-int v6, v9, v10

    .line 472
    .local v6, "length":I
    new-array v2, v6, [S

    .line 473
    .local v2, "array":[S
    const/4 v0, 0x0

    .line 474
    .local v0, "ai":I
    const/4 v4, 0x2

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v4, v9, :cond_3

    .line 475
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 476
    .local v3, "c":C
    if-ne v3, v11, :cond_2

    .line 477
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 478
    if-ne v3, v11, :cond_0

    .line 479
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "ai":I
    .local v1, "ai":I
    int-to-short v9, v3

    aput-short v9, v2, v0

    move v0, v1

    .line 474
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 481
    :cond_0
    move v7, v3

    .line 482
    .local v7, "runLength":I
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    int-to-short v8, v9

    .line 483
    .local v8, "runValue":S
    const/4 v5, 0x0

    .local v5, "j":I
    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    :goto_2
    if-ge v5, v7, :cond_1

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    aput-short v8, v2, v1

    add-int/lit8 v5, v5, 0x1

    move v1, v0

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    goto :goto_2

    :cond_1
    move v0, v1

    .line 485
    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_1

    .line 487
    .end local v5    # "j":I
    .end local v7    # "runLength":I
    .end local v8    # "runValue":S
    :cond_2
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "ai":I
    .restart local v1    # "ai":I
    int-to-short v9, v3

    aput-short v9, v2, v0

    move v0, v1

    .end local v1    # "ai":I
    .restart local v0    # "ai":I
    goto :goto_1

    .line 491
    .end local v3    # "c":C
    :cond_3
    if-eq v0, v6, :cond_4

    .line 492
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "Bad run-length encoded short array"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 494
    :cond_4
    return-object v2
.end method

.method private static final appendEncodedByte(Ljava/lang/StringBuffer;B[B)V
    .locals 4
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .param p1, "value"    # B
    .param p2, "state"    # [B

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 417
    aget-byte v1, p2, v3

    if-eqz v1, :cond_0

    .line 418
    aget-byte v1, p2, v2

    shl-int/lit8 v1, v1, 0x8

    and-int/lit16 v2, p1, 0xff

    or-int/2addr v1, v2

    int-to-char v0, v1

    .line 419
    .local v0, "c":C
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 420
    aput-byte v3, p2, v3

    .line 426
    .end local v0    # "c":C
    :goto_0
    return-void

    .line 423
    :cond_0
    aput-byte v2, p2, v3

    .line 424
    aput-byte p1, p2, v2

    goto :goto_0
.end method

.method private static final appendInt(Ljava/lang/StringBuffer;I)V
    .locals 1
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .param p1, "value"    # I

    .prologue
    .line 356
    ushr-int/lit8 v0, p1, 0x10

    int-to-char v0, v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 357
    const v0, 0xffff

    and-int/2addr v0, p1

    int-to-char v0, v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 358
    return-void
.end method

.method public static appendNumber(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "result"    # Ljava/lang/StringBuffer;
    .param p1, "n"    # I

    .prologue
    .line 1379
    const/16 v0, 0xa

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;
    .locals 4
    .param p0, "result"    # Ljava/lang/StringBuffer;
    .param p1, "n"    # I
    .param p2, "radix"    # I
    .param p3, "minDigits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1422
    const/4 v1, 0x2

    if-lt p2, v1, :cond_0

    const/16 v1, 0x24

    if-le p2, v1, :cond_1

    .line 1423
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Illegal radix "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1427
    :cond_1
    move v0, p1

    .line 1429
    .local v0, "abs":I
    if-gez p1, :cond_2

    .line 1430
    neg-int v0, p1

    .line 1431
    const-string/jumbo v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1434
    :cond_2
    invoke-static {p0, v0, p2, p3}, Lcom/ibm/icu/impl/Utility;->recursiveAppendNumber(Ljava/lang/StringBuffer;III)V

    .line 1436
    return-object p0
.end method

.method public static appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V
    .locals 9
    .param p0, "rule"    # Ljava/lang/StringBuffer;
    .param p1, "c"    # I
    .param p2, "isLiteral"    # Z
    .param p3, "escapeUnprintable"    # Z
    .param p4, "quoteBuf"    # Ljava/lang/StringBuffer;

    .prologue
    const/16 v8, 0x20

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v5, 0x5c

    const/16 v4, 0x27

    .line 1597
    if-nez p2, :cond_0

    if-eqz p3, :cond_9

    invoke-static {p1}, Lcom/ibm/icu/impl/Utility;->isUnprintable(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1599
    :cond_0
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_5

    .line 1609
    :goto_0
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lt v3, v7, :cond_1

    invoke-virtual {p4, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    invoke-virtual {p4, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_1

    .line 1610
    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1611
    invoke-virtual {p4, v6, v7}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1615
    :cond_1
    const/4 v1, 0x0

    .line 1618
    .local v1, "trailingCount":I
    :goto_1
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lt v3, v7, :cond_2

    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p4, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_2

    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p4, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_2

    .line 1619
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {p4, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1620
    add-int/lit8 v1, v1, 0x1

    .line 1621
    goto :goto_1

    .line 1622
    :cond_2
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 1623
    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1625
    sget-boolean v3, Lcom/ibm/icu/impl/ICUDebug;->isJDK14OrHigher:Z

    if-eqz v3, :cond_4

    .line 1626
    invoke-virtual {p0, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1630
    :goto_2
    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1631
    invoke-virtual {p4, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    :cond_3
    move v2, v1

    .line 1633
    .end local v1    # "trailingCount":I
    .local v2, "trailingCount":I
    :goto_3
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "trailingCount":I
    .restart local v1    # "trailingCount":I
    if-lez v2, :cond_5

    .line 1634
    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v1

    .line 1635
    .end local v1    # "trailingCount":I
    .restart local v2    # "trailingCount":I
    goto :goto_3

    .line 1628
    .end local v2    # "trailingCount":I
    .restart local v1    # "trailingCount":I
    :cond_4
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1637
    .end local v1    # "trailingCount":I
    :cond_5
    const/4 v3, -0x1

    if-eq p1, v3, :cond_6

    .line 1643
    if-ne p1, v8, :cond_7

    .line 1644
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 1645
    .local v0, "len":I
    if-lez v0, :cond_6

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-eq v3, v8, :cond_6

    .line 1646
    invoke-virtual {p0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1680
    .end local v0    # "len":I
    :cond_6
    :goto_4
    return-void

    .line 1648
    :cond_7
    if-eqz p3, :cond_8

    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->escapeUnprintable(Ljava/lang/StringBuffer;I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1649
    :cond_8
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 1655
    :cond_9
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-nez v3, :cond_b

    if-eq p1, v4, :cond_a

    if-ne p1, v5, :cond_b

    .line 1657
    :cond_a
    invoke-virtual {p0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    int-to-char v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 1663
    :cond_b
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-gtz v3, :cond_f

    const/16 v3, 0x21

    if-lt p1, v3, :cond_e

    const/16 v3, 0x7e

    if-gt p1, v3, :cond_e

    const/16 v3, 0x30

    if-lt p1, v3, :cond_c

    const/16 v3, 0x39

    if-le p1, v3, :cond_e

    :cond_c
    const/16 v3, 0x41

    if-lt p1, v3, :cond_d

    const/16 v3, 0x5a

    if-le p1, v3, :cond_e

    :cond_d
    const/16 v3, 0x61

    if-lt p1, v3, :cond_f

    const/16 v3, 0x7a

    if-gt p1, v3, :cond_f

    :cond_e
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1669
    :cond_f
    invoke-static {p4, p1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 1671
    if-ne p1, v4, :cond_6

    .line 1672
    int-to-char v3, p1

    invoke-virtual {p4, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 1678
    :cond_10
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_4
.end method

.method public static appendToRule(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeMatcher;ZLjava/lang/StringBuffer;)V
    .locals 2
    .param p0, "rule"    # Ljava/lang/StringBuffer;
    .param p1, "matcher"    # Lcom/ibm/icu/text/UnicodeMatcher;
    .param p2, "escapeUnprintable"    # Z
    .param p3, "quoteBuf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1705
    if-eqz p1, :cond_0

    .line 1706
    invoke-interface {p1, p2}, Lcom/ibm/icu/text/UnicodeMatcher;->toPattern(Z)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, p2, p3}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    .line 1709
    :cond_0
    return-void
.end method

.method public static appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V
    .locals 2
    .param p0, "rule"    # Ljava/lang/StringBuffer;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isLiteral"    # Z
    .param p3, "escapeUnprintable"    # Z
    .param p4, "quoteBuf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1691
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1693
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {p0, v1, p2, p3, p4}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 1691
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1695
    :cond_0
    return-void
.end method

.method public static final arrayEquals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 69
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 80
    .end local p0    # "source":Ljava/lang/Object;
    :goto_0
    return v0

    .line 69
    .restart local p0    # "source":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    :cond_1
    instance-of v0, p0, [Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 73
    check-cast p0, [Ljava/lang/Object;

    .end local p0    # "source":Ljava/lang/Object;
    check-cast p0, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 74
    .restart local p0    # "source":Ljava/lang/Object;
    :cond_2
    instance-of v0, p0, [I

    if-eqz v0, :cond_3

    .line 75
    check-cast p0, [I

    .end local p0    # "source":Ljava/lang/Object;
    check-cast p0, [I

    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->arrayEquals([ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 76
    .restart local p0    # "source":Ljava/lang/Object;
    :cond_3
    instance-of v0, p0, [D

    if-eqz v0, :cond_4

    .line 77
    check-cast p0, [I

    .end local p0    # "source":Ljava/lang/Object;
    check-cast p0, [I

    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->arrayEquals([ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 78
    .restart local p0    # "source":Ljava/lang/Object;
    :cond_4
    instance-of v0, p0, [B

    if-eqz v0, :cond_5

    .line 79
    check-cast p0, [B

    .end local p0    # "source":Ljava/lang/Object;
    check-cast p0, [B

    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->arrayEquals([BLjava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 80
    .restart local p0    # "source":Ljava/lang/Object;
    :cond_5
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final arrayEquals([BLjava/lang/Object;)Z
    .locals 5
    .param p0, "source"    # [B
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-nez p0, :cond_2

    if-nez p1, :cond_1

    .line 60
    .end local p1    # "target":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_1
    move v1, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_2
    instance-of v3, p1, [B

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 59
    :cond_3
    check-cast p1, [B

    .end local p1    # "target":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [B

    .line 60
    .local v0, "targ":[B
    array-length v3, p0

    array-length v4, v0

    if-ne v3, v4, :cond_4

    array-length v3, p0

    invoke-static {p0, v2, v0, v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([BI[BII)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static final arrayEquals([DLjava/lang/Object;)Z
    .locals 5
    .param p0, "source"    # [D
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-nez p0, :cond_2

    if-nez p1, :cond_1

    .line 53
    .end local p1    # "target":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_1
    move v1, v2

    .line 50
    goto :goto_0

    .line 51
    :cond_2
    instance-of v3, p1, [D

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 52
    :cond_3
    check-cast p1, [D

    .end local p1    # "target":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [D

    .line 53
    .local v0, "targ":[D
    array-length v3, p0

    array-length v4, v0

    if-ne v3, v4, :cond_4

    array-length v3, p0

    invoke-static {p0, v2, v0, v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([DI[DII)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static final arrayEquals([ILjava/lang/Object;)Z
    .locals 5
    .param p0, "source"    # [I
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-nez p0, :cond_2

    if-nez p1, :cond_1

    .line 41
    .end local p1    # "target":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_1
    move v1, v2

    .line 38
    goto :goto_0

    .line 39
    :cond_2
    instance-of v3, p1, [I

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 40
    :cond_3
    check-cast p1, [I

    .end local p1    # "target":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [I

    .line 41
    .local v0, "targ":[I
    array-length v3, p0

    array-length v4, v0

    if-ne v3, v4, :cond_4

    array-length v3, p0

    invoke-static {p0, v2, v0, v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([II[III)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static final arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 5
    .param p0, "source"    # [Ljava/lang/Object;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    if-nez p0, :cond_2

    if-nez p1, :cond_1

    .line 29
    .end local p1    # "target":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_1
    move v1, v2

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    instance-of v3, p1, [Ljava/lang/Object;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 28
    :cond_3
    check-cast p1, [Ljava/lang/Object;

    .end local p1    # "target":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [Ljava/lang/Object;

    .line 29
    .local v0, "targ":[Ljava/lang/Object;
    array-length v3, p0

    array-length v4, v0

    if-ne v3, v4, :cond_4

    array-length v3, p0

    invoke-static {p0, v2, v0, v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([Ljava/lang/Object;I[Ljava/lang/Object;II)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static final arrayRegionMatches([BI[BII)Z
    .locals 5
    .param p0, "source"    # [B
    .param p1, "sourceStart"    # I
    .param p2, "target"    # [B
    .param p3, "targetStart"    # I
    .param p4, "len"    # I

    .prologue
    .line 160
    add-int v2, p1, p4

    .line 161
    .local v2, "sourceEnd":I
    sub-int v0, p3, p1

    .line 162
    .local v0, "delta":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 163
    aget-byte v3, p0, v1

    add-int v4, v1, v0

    aget-byte v4, p2, v4

    if-eq v3, v4, :cond_0

    .line 164
    const/4 v3, 0x0

    .line 166
    :goto_1
    return v3

    .line 162
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static final arrayRegionMatches([CI[CII)Z
    .locals 5
    .param p0, "source"    # [C
    .param p1, "sourceStart"    # I
    .param p2, "target"    # [C
    .param p3, "targetStart"    # I
    .param p4, "len"    # I

    .prologue
    .line 112
    add-int v2, p1, p4

    .line 113
    .local v2, "sourceEnd":I
    sub-int v0, p3, p1

    .line 114
    .local v0, "delta":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 115
    aget-char v3, p0, v1

    add-int v4, v1, v0

    aget-char v4, p2, v4

    if-eq v3, v4, :cond_0

    .line 116
    const/4 v3, 0x0

    .line 118
    :goto_1
    return v3

    .line 114
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static final arrayRegionMatches([DI[DII)Z
    .locals 8
    .param p0, "source"    # [D
    .param p1, "sourceStart"    # I
    .param p2, "target"    # [D
    .param p3, "targetStart"    # I
    .param p4, "len"    # I

    .prologue
    .line 150
    add-int v2, p1, p4

    .line 151
    .local v2, "sourceEnd":I
    sub-int v0, p3, p1

    .line 152
    .local v0, "delta":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 153
    aget-wide v4, p0, v1

    add-int v3, v1, v0

    aget-wide v6, p2, v3

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    .line 154
    const/4 v3, 0x0

    .line 156
    :goto_1
    return v3

    .line 152
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static final arrayRegionMatches([II[III)Z
    .locals 5
    .param p0, "source"    # [I
    .param p1, "sourceStart"    # I
    .param p2, "target"    # [I
    .param p3, "targetStart"    # I
    .param p4, "len"    # I

    .prologue
    .line 131
    add-int v2, p1, p4

    .line 132
    .local v2, "sourceEnd":I
    sub-int v0, p3, p1

    .line 133
    .local v0, "delta":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 134
    aget v3, p0, v1

    add-int v4, v1, v0

    aget v4, p2, v4

    if-eq v3, v4, :cond_0

    .line 135
    const/4 v3, 0x0

    .line 137
    :goto_1
    return v3

    .line 133
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static final arrayRegionMatches([Ljava/lang/Object;I[Ljava/lang/Object;II)Z
    .locals 5
    .param p0, "source"    # [Ljava/lang/Object;
    .param p1, "sourceStart"    # I
    .param p2, "target"    # [Ljava/lang/Object;
    .param p3, "targetStart"    # I
    .param p4, "len"    # I

    .prologue
    .line 93
    add-int v2, p1, p4

    .line 94
    .local v2, "sourceEnd":I
    sub-int v0, p3, p1

    .line 95
    .local v0, "delta":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 96
    aget-object v3, p0, v1

    add-int v4, v1, v0

    aget-object v4, p2, v4

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 97
    const/4 v3, 0x0

    .line 99
    :goto_1
    return v3

    .line 95
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static final arrayToRLEString([B)Ljava/lang/String;
    .locals 8
    .param p0, "a"    # [B

    .prologue
    const/4 v7, 0x0

    .line 304
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 305
    .local v1, "buffer":Ljava/lang/StringBuffer;
    array-length v6, p0

    shr-int/lit8 v6, v6, 0x10

    int-to-char v6, v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 306
    array-length v6, p0

    int-to-char v6, v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 307
    aget-byte v4, p0, v7

    .line 308
    .local v4, "runValue":B
    const/4 v3, 0x1

    .line 309
    .local v3, "runLength":I
    const/4 v6, 0x2

    new-array v5, v6, [B

    .line 310
    .local v5, "state":[B
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    array-length v6, p0

    if-ge v2, v6, :cond_1

    .line 311
    aget-byte v0, p0, v2

    .line 312
    .local v0, "b":B
    if-ne v0, v4, :cond_0

    const/16 v6, 0xff

    if-ge v3, v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    .line 310
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 314
    :cond_0
    invoke-static {v1, v4, v3, v5}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;BI[B)V

    .line 315
    move v4, v0

    .line 316
    const/4 v3, 0x1

    goto :goto_1

    .line 319
    .end local v0    # "b":B
    :cond_1
    invoke-static {v1, v4, v3, v5}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;BI[B)V

    .line 323
    aget-byte v6, v5, v7

    if-eqz v6, :cond_2

    invoke-static {v1, v7, v5}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 325
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static final arrayToRLEString([C)Ljava/lang/String;
    .locals 6
    .param p0, "a"    # [C

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 272
    .local v0, "buffer":Ljava/lang/StringBuffer;
    array-length v5, p0

    shr-int/lit8 v5, v5, 0x10

    int-to-char v5, v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 273
    array-length v5, p0

    int-to-char v5, v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    const/4 v5, 0x0

    aget-char v3, p0, v5

    .line 275
    .local v3, "runValue":C
    const/4 v2, 0x1

    .line 276
    .local v2, "runLength":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_1

    .line 277
    aget-char v4, p0, v1

    .line 278
    .local v4, "s":C
    if-ne v4, v3, :cond_0

    const v5, 0xffff

    if-ge v2, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 276
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 280
    :cond_0
    int-to-short v5, v3

    invoke-static {v0, v5, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;SI)V

    .line 281
    move v3, v4

    .line 282
    const/4 v2, 0x1

    goto :goto_1

    .line 285
    .end local v4    # "s":C
    :cond_1
    int-to-short v5, v3

    invoke-static {v0, v5, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;SI)V

    .line 286
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static final arrayToRLEString([I)Ljava/lang/String;
    .locals 6
    .param p0, "a"    # [I

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 207
    .local v0, "buffer":Ljava/lang/StringBuffer;
    array-length v5, p0

    invoke-static {v0, v5}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 208
    const/4 v5, 0x0

    aget v3, p0, v5

    .line 209
    .local v3, "runValue":I
    const/4 v2, 0x1

    .line 210
    .local v2, "runLength":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_1

    .line 211
    aget v4, p0, v1

    .line 212
    .local v4, "s":I
    if-ne v4, v3, :cond_0

    const v5, 0xffff

    if-ge v2, v5, :cond_0

    .line 213
    add-int/lit8 v2, v2, 0x1

    .line 210
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    :cond_0
    invoke-static {v0, v3, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;II)V

    .line 216
    move v3, v4

    .line 217
    const/4 v2, 0x1

    goto :goto_1

    .line 220
    .end local v4    # "s":I
    :cond_1
    invoke-static {v0, v3, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;II)V

    .line 221
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static final arrayToRLEString([S)Ljava/lang/String;
    .locals 6
    .param p0, "a"    # [S

    .prologue
    .line 238
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 240
    .local v0, "buffer":Ljava/lang/StringBuffer;
    array-length v5, p0

    shr-int/lit8 v5, v5, 0x10

    int-to-char v5, v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 241
    array-length v5, p0

    int-to-char v5, v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 242
    const/4 v5, 0x0

    aget-short v3, p0, v5

    .line 243
    .local v3, "runValue":S
    const/4 v2, 0x1

    .line 244
    .local v2, "runLength":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_1

    .line 245
    aget-short v4, p0, v1

    .line 246
    .local v4, "s":S
    if-ne v4, v3, :cond_0

    const v5, 0xffff

    if-ge v2, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 244
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 248
    :cond_0
    invoke-static {v0, v3, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;SI)V

    .line 249
    move v3, v4

    .line 250
    const/4 v2, 0x1

    goto :goto_1

    .line 253
    .end local v4    # "s":S
    :cond_1
    invoke-static {v0, v3, v2}, Lcom/ibm/icu/impl/Utility;->encodeRun(Ljava/lang/StringBuffer;SI)V

    .line 254
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static final compareUnsigned(II)I
    .locals 1
    .param p0, "source"    # I
    .param p1, "target"    # I

    .prologue
    const/high16 v0, -0x80000000

    .line 1720
    sub-int/2addr p0, v0

    .line 1721
    sub-int/2addr p1, v0

    .line 1722
    if-ge p0, p1, :cond_0

    .line 1723
    const/4 v0, -0x1

    .line 1728
    :goto_0
    return v0

    .line 1725
    :cond_0
    if-le p0, p1, :cond_1

    .line 1726
    const/4 v0, 0x1

    goto :goto_0

    .line 1728
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static deleteRuleWhiteSpace(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 1103
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1104
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1105
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 1106
    .local v1, "ch":I
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1107
    invoke-static {v1}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1110
    invoke-static {v0, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1112
    .end local v1    # "ch":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static final encodeRun(Ljava/lang/StringBuffer;BI[B)V
    .locals 3
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .param p1, "value"    # B
    .param p2, "length"    # I
    .param p3, "state"    # [B

    .prologue
    const/16 v2, -0x5b

    .line 389
    const/4 v1, 0x4

    if-ge p2, v1, :cond_1

    .line 390
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-ge v0, p2, :cond_4

    .line 391
    if-ne p1, v2, :cond_0

    invoke-static {p0, v2, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 392
    :cond_0
    invoke-static {p0, p1, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 396
    .end local v0    # "j":I
    :cond_1
    if-ne p2, v2, :cond_3

    .line 397
    if-ne p1, v2, :cond_2

    invoke-static {p0, v2, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 398
    :cond_2
    invoke-static {p0, p1, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 399
    add-int/lit8 p2, p2, -0x1

    .line 401
    :cond_3
    invoke-static {p0, v2, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 402
    int-to-byte v1, p2

    invoke-static {p0, v1, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 403
    invoke-static {p0, p1, p3}, Lcom/ibm/icu/impl/Utility;->appendEncodedByte(Ljava/lang/StringBuffer;B[B)V

    .line 405
    :cond_4
    return-void
.end method

.method private static final encodeRun(Ljava/lang/StringBuffer;II)V
    .locals 3
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .param p1, "value"    # I
    .param p2, "length"    # I

    .prologue
    const v2, 0xa5a5

    .line 333
    const/4 v1, 0x4

    if-ge p2, v1, :cond_1

    .line 334
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-ge v0, p2, :cond_4

    .line 335
    if-ne p1, v2, :cond_0

    .line 336
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 338
    :cond_0
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 334
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    .end local v0    # "j":I
    :cond_1
    if-ne p2, v2, :cond_3

    .line 343
    if-ne p1, v2, :cond_2

    .line 344
    invoke-static {p0, v2}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 346
    :cond_2
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 347
    add-int/lit8 p2, p2, -0x1

    .line 349
    :cond_3
    invoke-static {p0, v2}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 350
    invoke-static {p0, p2}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 351
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->appendInt(Ljava/lang/StringBuffer;I)V

    .line 353
    :cond_4
    return-void
.end method

.method private static final encodeRun(Ljava/lang/StringBuffer;SI)V
    .locals 3
    .param p0, "buffer"    # Ljava/lang/StringBuffer;
    .param p1, "value"    # S
    .param p2, "length"    # I

    .prologue
    const v2, 0xa5a5

    .line 365
    const/4 v1, 0x4

    if-ge p2, v1, :cond_1

    .line 366
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-ge v0, p2, :cond_4

    .line 367
    if-ne p1, v2, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 368
    :cond_0
    int-to-char v1, p1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    .end local v0    # "j":I
    :cond_1
    if-ne p2, v2, :cond_3

    .line 373
    if-ne p1, v2, :cond_2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 374
    :cond_2
    int-to-char v1, p1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 375
    add-int/lit8 p2, p2, -0x1

    .line 377
    :cond_3
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 378
    int-to-char v1, p2

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 379
    int-to-char v1, p1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 381
    :cond_4
    return-void
.end method

.method public static final escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 705
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 706
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 707
    invoke-static {p0, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 708
    .local v1, "c":I
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 709
    const/16 v4, 0x20

    if-lt v1, v4, :cond_1

    const/16 v4, 0x7f

    if-gt v1, v4, :cond_1

    .line 710
    const/16 v4, 0x5c

    if-ne v1, v4, :cond_0

    .line 711
    const-string/jumbo v4, "\\\\"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 713
    :cond_0
    int-to-char v4, v1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 716
    :cond_1
    const v4, 0xffff

    if-gt v1, v4, :cond_2

    const/4 v2, 0x1

    .line 717
    .local v2, "four":Z
    :goto_1
    if-eqz v2, :cond_3

    const-string/jumbo v4, "\\u"

    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 718
    if-eqz v2, :cond_4

    const/4 v4, 0x4

    :goto_3
    invoke-static {v1, v4, v0}, Lcom/ibm/icu/impl/Utility;->hex(IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 716
    .end local v2    # "four":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 717
    .restart local v2    # "four":Z
    :cond_3
    const-string/jumbo v4, "\\U"

    goto :goto_2

    .line 718
    :cond_4
    const/16 v4, 0x8

    goto :goto_3

    .line 721
    .end local v1    # "c":I
    .end local v2    # "four":Z
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static escapeUnprintable(Ljava/lang/StringBuffer;I)Z
    .locals 2
    .param p0, "result"    # Ljava/lang/StringBuffer;
    .param p1, "c"    # I

    .prologue
    .line 1498
    invoke-static {p1}, Lcom/ibm/icu/impl/Utility;->isUnprintable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1499
    const/16 v0, 0x5c

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1500
    const/high16 v0, -0x10000

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 1501
    const/16 v0, 0x55

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1502
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x1c

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1503
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x18

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1504
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x14

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1505
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x10

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1509
    :goto_0
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0xc

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1510
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x8

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1511
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    shr-int/lit8 v1, p1, 0x4

    and-int/lit8 v1, v1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1512
    sget-object v0, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    and-int/lit8 v1, p1, 0xf

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1513
    const/4 v0, 0x1

    .line 1515
    :goto_1
    return v0

    .line 1507
    :cond_0
    const/16 v0, 0x75

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1515
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static final format1ForSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x5c

    const/16 v6, 0x22

    .line 663
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 664
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string/jumbo v4, "\""

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 665
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_6

    .line 666
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 667
    .local v1, "c":C
    const/16 v4, 0x20

    if-lt v1, v4, :cond_0

    if-eq v1, v6, :cond_0

    if-ne v1, v7, :cond_4

    .line 668
    :cond_0
    const/16 v4, 0xa

    if-ne v1, v4, :cond_1

    .line 669
    const-string/jumbo v4, "\\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    move v2, v3

    .line 695
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 670
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :cond_1
    const/16 v4, 0x9

    if-ne v1, v4, :cond_2

    .line 671
    const-string/jumbo v4, "\\t"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 672
    :cond_2
    const/16 v4, 0xd

    if-ne v1, v4, :cond_3

    .line 673
    const-string/jumbo v4, "\\r"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 679
    :cond_3
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 680
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v5, v1, 0x1c0

    shr-int/lit8 v5, v5, 0x6

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 681
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v5, v1, 0x38

    shr-int/lit8 v5, v5, 0x3

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 682
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v5, v1, 0x7

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 685
    :cond_4
    const/16 v4, 0x7e

    if-gt v1, v4, :cond_5

    .line 686
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 689
    :cond_5
    const-string/jumbo v4, "\\u"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 690
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    const v5, 0xf000

    and-int/2addr v5, v1

    shr-int/lit8 v5, v5, 0xc

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 691
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v5, v1, 0xf00

    shr-int/lit8 v5, v5, 0x8

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 692
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v5, v1, 0xf0

    shr-int/lit8 v5, v5, 0x4

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 693
    sget-object v4, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v5, v1, 0xf

    aget-char v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 696
    .end local v1    # "c":C
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 697
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static final formatForSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x5c

    const/16 v7, 0x22

    .line 608
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 609
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_8

    .line 610
    if-lez v3, :cond_0

    const/16 v5, 0x2b

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    sget-object v6, Lcom/ibm/icu/impl/Utility;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 611
    :cond_0
    const-string/jumbo v5, "        \""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 612
    const/16 v2, 0xb

    .line 613
    .local v2, "count":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_7

    const/16 v5, 0x50

    if-ge v2, v5, :cond_7

    .line 614
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 615
    .local v1, "c":C
    const/16 v5, 0x20

    if-lt v1, v5, :cond_1

    if-eq v1, v7, :cond_1

    if-ne v1, v8, :cond_5

    .line 616
    :cond_1
    const/16 v5, 0xa

    if-ne v1, v5, :cond_2

    .line 617
    const-string/jumbo v5, "\\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 618
    add-int/lit8 v2, v2, 0x2

    :goto_2
    move v3, v4

    .line 649
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 619
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_2
    const/16 v5, 0x9

    if-ne v1, v5, :cond_3

    .line 620
    const-string/jumbo v5, "\\t"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 621
    add-int/lit8 v2, v2, 0x2

    .line 622
    goto :goto_2

    :cond_3
    const/16 v5, 0xd

    if-ne v1, v5, :cond_4

    .line 623
    const-string/jumbo v5, "\\r"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 624
    add-int/lit8 v2, v2, 0x2

    .line 625
    goto :goto_2

    .line 630
    :cond_4
    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 631
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v6, v1, 0x1c0

    shr-int/lit8 v6, v6, 0x6

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 632
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v6, v1, 0x38

    shr-int/lit8 v6, v6, 0x3

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 633
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v6, v1, 0x7

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 634
    add-int/lit8 v2, v2, 0x4

    .line 636
    goto :goto_2

    .line 637
    :cond_5
    const/16 v5, 0x7e

    if-gt v1, v5, :cond_6

    .line 638
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 639
    add-int/lit8 v2, v2, 0x1

    .line 640
    goto :goto_2

    .line 642
    :cond_6
    const-string/jumbo v5, "\\u"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 643
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    const v6, 0xf000

    and-int/2addr v6, v1

    shr-int/lit8 v6, v6, 0xc

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 644
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v6, v1, 0xf00

    shr-int/lit8 v6, v6, 0x8

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 645
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit16 v6, v1, 0xf0

    shr-int/lit8 v6, v6, 0x4

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 646
    sget-object v5, Lcom/ibm/icu/impl/Utility;->HEX_DIGIT:[C

    and-int/lit8 v6, v1, 0xf

    aget-char v5, v5, v6

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 647
    add-int/lit8 v2, v2, 0x6

    goto :goto_2

    .line 650
    .end local v1    # "c":C
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_7
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 652
    .end local v2    # "count":I
    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getChars(Ljava/lang/StringBuffer;II[CI)V
    .locals 0
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "srcBegin"    # I
    .param p2, "srcEnd"    # I
    .param p3, "dst"    # [C
    .param p4, "dstBegin"    # I

    .prologue
    .line 1565
    if-ne p1, p2, :cond_0

    .line 1569
    :goto_0
    return-void

    .line 1568
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    goto :goto_0
.end method

.method static final getInt(Ljava/lang/String;I)I
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "i"    # I

    .prologue
    .line 464
    mul-int/lit8 v0, p1, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public static hex(C)Ljava/lang/String;
    .locals 2
    .param p0, "ch"    # C

    .prologue
    .line 924
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 925
    .local v0, "temp":Ljava/lang/StringBuffer;
    invoke-static {p0, v0}, Lcom/ibm/icu/impl/Utility;->hex(CLjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static hex(II)Ljava/lang/String;
    .locals 2
    .param p0, "ch"    # I
    .param p1, "width"    # I

    .prologue
    .line 969
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 970
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/16 v1, 0x10

    invoke-static {v0, p0, v1, p1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static hex(J)Ljava/lang/String;
    .locals 2
    .param p0, "ch"    # J

    .prologue
    .line 992
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/impl/Utility;->hex(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hex(JI)Ljava/lang/String;
    .locals 6
    .param p0, "i"    # J
    .param p2, "places"    # I

    .prologue
    .line 976
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, p0, v2

    if-nez v2, :cond_1

    const-string/jumbo v1, "-8000000000000000"

    .line 988
    :cond_0
    :goto_0
    return-object v1

    .line 977
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-gez v2, :cond_4

    const/4 v0, 0x1

    .line 978
    .local v0, "negative":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 979
    neg-long p0, p0

    .line 981
    :cond_2
    const/16 v2, 0x10

    invoke-static {p0, p1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 982
    .local v1, "result":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, p2, :cond_3

    .line 983
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "0000000000000000"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 985
    :cond_3
    if-eqz v0, :cond_0

    .line 986
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 977
    .end local v0    # "negative":Z
    .end local v1    # "result":Ljava/lang/String;
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static hex(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 933
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 934
    .local v0, "temp":Ljava/lang/StringBuffer;
    invoke-static {p0, v0}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static hex(Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/StringBuffer;

    .prologue
    .line 942
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hex(CLjava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "ch"    # C
    .param p1, "output"    # Ljava/lang/StringBuffer;

    .prologue
    .line 950
    const/16 v0, 0x10

    const/4 v1, 0x4

    invoke-static {p1, p0, v0, v1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static hex(IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "ch"    # I
    .param p1, "width"    # I
    .param p2, "output"    # Ljava/lang/StringBuffer;

    .prologue
    .line 960
    const/16 v0, 0x10

    invoke-static {p2, p0, v0, p1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static hex(Ljava/lang/String;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "result"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1001
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1002
    if-eqz v0, :cond_0

    const/16 v1, 0x2c

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1003
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1, p1}, Lcom/ibm/icu/impl/Utility;->hex(CLjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1001
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1005
    :cond_1
    return-object p1
.end method

.method public static final highBit(I)B
    .locals 3
    .param p0, "n"    # I

    .prologue
    const/16 v2, 0x10

    .line 1742
    if-gtz p0, :cond_1

    .line 1743
    const/4 v0, -0x1

    .line 1773
    :cond_0
    :goto_0
    return v0

    .line 1746
    :cond_1
    const/4 v0, 0x0

    .line 1748
    .local v0, "bit":B
    const/high16 v1, 0x10000

    if-lt p0, v1, :cond_2

    .line 1749
    shr-int/lit8 p0, p0, 0x10

    .line 1750
    int-to-byte v0, v2

    .line 1753
    :cond_2
    const/16 v1, 0x100

    if-lt p0, v1, :cond_3

    .line 1754
    shr-int/lit8 p0, p0, 0x8

    .line 1755
    add-int/lit8 v1, v0, 0x8

    int-to-byte v0, v1

    .line 1758
    :cond_3
    if-lt p0, v2, :cond_4

    .line 1759
    shr-int/lit8 p0, p0, 0x4

    .line 1760
    add-int/lit8 v1, v0, 0x4

    int-to-byte v0, v1

    .line 1763
    :cond_4
    const/4 v1, 0x4

    if-lt p0, v1, :cond_5

    .line 1764
    shr-int/lit8 p0, p0, 0x2

    .line 1765
    add-int/lit8 v1, v0, 0x2

    int-to-byte v0, v1

    .line 1768
    :cond_5
    const/4 v1, 0x2

    if-lt p0, v1, :cond_0

    .line 1769
    shr-int/lit8 p0, p0, 0x1

    .line 1770
    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_0
.end method

.method public static indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I
    .locals 1
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1810
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I
    .locals 1
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "i"    # I

    .prologue
    .line 1828
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static integerValueOf(I)Ljava/lang/Integer;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 1939
    if-ltz p0, :cond_0

    const/16 v0, 0x40

    if-ge p0, v0, :cond_0

    .line 1940
    sget-object v0, Lcom/ibm/icu/impl/Utility;->INT_CONST:[Ljava/lang/Integer;

    aget-object v0, v0, p0

    .line 1942
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p0}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0
.end method

.method public static isUnprintable(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 1487
    const/16 v0, 0x20

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7e

    if-le p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static lastIndexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I
    .locals 1
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1819
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static lastIndexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I
    .locals 1
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "i"    # I

    .prologue
    .line 1837
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static lookup(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "target"    # [Ljava/lang/String;

    .prologue
    .line 1069
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 1070
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1072
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 1069
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1072
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static final objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 173
    if-nez p0, :cond_1

    .line 174
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 176
    :goto_0
    return v0

    .line 174
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 176
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static parseChar(Ljava/lang/String;[IC)Z
    .locals 4
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "ch"    # C

    .prologue
    const/4 v1, 0x0

    .line 1128
    aget v0, p1, v1

    .line 1129
    .local v0, "start":I
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;[I)V

    .line 1130
    aget v2, p1, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v2, v3, :cond_0

    aget v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, p2, :cond_1

    .line 1132
    :cond_0
    aput v0, p1, v1

    .line 1136
    :goto_0
    return v1

    .line 1135
    :cond_1
    aget v2, p1, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p1, v1

    .line 1136
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static parseInteger(Ljava/lang/String;[II)I
    .locals 12
    .param p0, "rule"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "limit"    # I

    .prologue
    const/4 v4, 0x0

    .line 1278
    const/4 v6, 0x0

    .line 1279
    .local v6, "count":I
    const/4 v11, 0x0

    .line 1280
    .local v11, "value":I
    aget v2, p1, v4

    .line 1281
    .local v2, "p":I
    const/16 v9, 0xa

    .line 1283
    .local v9, "radix":I
    const/4 v1, 0x1

    const-string/jumbo v3, "0x"

    const/4 v5, 0x2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1284
    add-int/lit8 v2, v2, 0x2

    .line 1285
    const/16 v9, 0x10

    move v8, v2

    .line 1292
    .end local v2    # "p":I
    .local v8, "p":I
    :goto_0
    if-ge v8, p2, :cond_4

    .line 1293
    add-int/lit8 v2, v8, 0x1

    .end local v8    # "p":I
    .restart local v2    # "p":I
    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0, v9}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v7

    .line 1294
    .local v7, "d":I
    if-gez v7, :cond_3

    .line 1295
    add-int/lit8 v2, v2, -0x1

    .line 1309
    .end local v7    # "d":I
    :goto_1
    if-lez v6, :cond_0

    .line 1310
    aput v2, p1, v4

    :cond_0
    move v4, v11

    .line 1312
    :cond_1
    return v4

    .line 1286
    :cond_2
    if-ge v2, p2, :cond_5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_5

    .line 1287
    add-int/lit8 v2, v2, 0x1

    .line 1288
    const/4 v6, 0x1

    .line 1289
    const/16 v9, 0x8

    move v8, v2

    .end local v2    # "p":I
    .restart local v8    # "p":I
    goto :goto_0

    .line 1298
    .end local v8    # "p":I
    .restart local v2    # "p":I
    .restart local v7    # "d":I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    .line 1299
    mul-int v0, v11, v9

    add-int v10, v0, v7

    .line 1300
    .local v10, "v":I
    if-le v10, v11, :cond_1

    .line 1307
    move v11, v10

    move v8, v2

    .line 1308
    .end local v2    # "p":I
    .restart local v8    # "p":I
    goto :goto_0

    .end local v7    # "d":I
    .end local v10    # "v":I
    :cond_4
    move v2, v8

    .end local v8    # "p":I
    .restart local v2    # "p":I
    goto :goto_1

    :cond_5
    move v8, v2

    .end local v2    # "p":I
    .restart local v8    # "p":I
    goto :goto_0
.end method

.method public static parseNumber(Ljava/lang/String;[II)I
    .locals 7
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "radix"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 1459
    const/4 v2, 0x0

    .line 1460
    .local v2, "n":I
    aget v3, p1, v6

    .line 1461
    .local v3, "p":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 1462
    invoke-static {p0, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1463
    .local v0, "ch":I
    invoke-static {v0, p2}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v1

    .line 1464
    .local v1, "d":I
    if-gez v1, :cond_2

    .line 1475
    .end local v0    # "ch":I
    .end local v1    # "d":I
    :cond_0
    aget v5, p1, v6

    if-ne v3, v5, :cond_3

    .line 1479
    :cond_1
    :goto_1
    return v4

    .line 1467
    .restart local v0    # "ch":I
    .restart local v1    # "d":I
    :cond_2
    mul-int v5, p2, v2

    add-int v2, v5, v1

    .line 1470
    if-ltz v2, :cond_1

    .line 1473
    add-int/lit8 v3, v3, 0x1

    .line 1474
    goto :goto_0

    .line 1478
    .end local v0    # "ch":I
    .end local v1    # "d":I
    :cond_3
    aput v3, p1, v6

    move v4, v2

    .line 1479
    goto :goto_1
.end method

.method public static parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I
    .locals 10
    .param p0, "rule"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I
    .param p3, "pattern"    # Ljava/lang/String;
    .param p4, "parsedInts"    # [I

    .prologue
    const/4 v9, 0x0

    const/4 v7, -0x1

    .line 1161
    const/4 v8, 0x1

    new-array v5, v8, [I

    .line 1162
    .local v5, "p":[I
    const/4 v3, 0x0

    .line 1163
    .local v3, "intCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 1164
    invoke-virtual {p3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1166
    .local v1, "cpat":C
    sparse-switch v1, :sswitch_data_0

    .line 1189
    if-lt p1, p2, :cond_3

    .line 1199
    .end local v1    # "cpat":C
    :cond_0
    :goto_1
    return v7

    .line 1168
    .restart local v1    # "cpat":C
    :sswitch_0
    if-ge p1, p2, :cond_0

    .line 1171
    add-int/lit8 v6, p1, 0x1

    .end local p1    # "pos":I
    .local v6, "pos":I
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1172
    .local v0, "c":C
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v8

    if-nez v8, :cond_1

    move p1, v6

    .line 1173
    .end local v6    # "pos":I
    .restart local p1    # "pos":I
    goto :goto_1

    .end local p1    # "pos":I
    .restart local v6    # "pos":I
    :cond_1
    move p1, v6

    .line 1177
    .end local v0    # "c":C
    .end local v6    # "pos":I
    .restart local p1    # "pos":I
    :sswitch_1
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result p1

    .line 1163
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1180
    :sswitch_2
    aput p1, v5, v9

    .line 1181
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "intCount":I
    .local v4, "intCount":I
    invoke-static {p0, v5, p2}, Lcom/ibm/icu/impl/Utility;->parseInteger(Ljava/lang/String;[II)I

    move-result v8

    aput v8, p4, v3

    .line 1182
    aget v8, v5, v9

    if-ne v8, p1, :cond_2

    move v3, v4

    .line 1184
    .end local v4    # "intCount":I
    .restart local v3    # "intCount":I
    goto :goto_1

    .line 1186
    .end local v3    # "intCount":I
    .restart local v4    # "intCount":I
    :cond_2
    aget p1, v5, v9

    move v3, v4

    .line 1187
    .end local v4    # "intCount":I
    .restart local v3    # "intCount":I
    goto :goto_2

    .line 1192
    :cond_3
    add-int/lit8 v6, p1, 0x1

    .end local p1    # "pos":I
    .restart local v6    # "pos":I
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v8

    int-to-char v0, v8

    .line 1193
    .restart local v0    # "c":C
    if-eq v0, v1, :cond_5

    move p1, v6

    .line 1194
    .end local v6    # "pos":I
    .restart local p1    # "pos":I
    goto :goto_1

    .end local v0    # "c":C
    .end local v1    # "cpat":C
    :cond_4
    move v7, p1

    .line 1199
    goto :goto_1

    .end local p1    # "pos":I
    .restart local v0    # "c":C
    .restart local v1    # "cpat":C
    .restart local v6    # "pos":I
    :cond_5
    move p1, v6

    .end local v6    # "pos":I
    .restart local p1    # "pos":I
    goto :goto_2

    .line 1166
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x23 -> :sswitch_2
        0x7e -> :sswitch_1
    .end sparse-switch
.end method

.method public static parsePattern(Ljava/lang/String;Lcom/ibm/icu/text/Replaceable;II)I
    .locals 6
    .param p0, "pat"    # Ljava/lang/String;
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # I
    .param p3, "limit"    # I

    .prologue
    const/4 v4, -0x1

    .line 1222
    const/4 v2, 0x0

    .line 1225
    .local v2, "ipat":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v2, v5, :cond_1

    move v4, p2

    .line 1266
    :cond_0
    :goto_0
    return v4

    .line 1229
    :cond_1
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 1231
    .local v1, "cpat":I
    :goto_1
    if-ge p2, p3, :cond_0

    .line 1232
    invoke-interface {p1, p2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 1235
    .local v0, "c":I
    const/16 v5, 0x7e

    if-ne v1, v5, :cond_3

    .line 1236
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1237
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    add-int/2addr p2, v5

    .line 1238
    goto :goto_1

    .line 1240
    :cond_2
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v2, v5, :cond_4

    move v4, p2

    .line 1241
    goto :goto_0

    .line 1248
    :cond_3
    if-ne v0, v1, :cond_0

    .line 1249
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    .line 1250
    .local v3, "n":I
    add-int/2addr p2, v3

    .line 1251
    add-int/2addr v2, v3

    .line 1252
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v2, v5, :cond_4

    move v4, p2

    .line 1253
    goto :goto_0

    .line 1263
    .end local v3    # "n":I
    :cond_4
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 1264
    goto :goto_1
.end method

.method public static parseUnicodeIdentifier(Ljava/lang/String;[I)Ljava/lang/String;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "pos"    # [I

    .prologue
    const/4 v4, 0x0

    .line 1332
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1333
    .local v0, "buf":Ljava/lang/StringBuffer;
    aget v2, p1, v4

    .line 1334
    .local v2, "p":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1335
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 1336
    .local v1, "ch":I
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1337
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isUnicodeIdentifierStart(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1338
    invoke-static {v0, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 1349
    :goto_1
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1350
    goto :goto_0

    .line 1340
    :cond_0
    const/4 v3, 0x0

    .line 1352
    .end local v1    # "ch":I
    :goto_2
    return-object v3

    .line 1343
    .restart local v1    # "ch":I
    :cond_1
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isUnicodeIdentifierPart(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1344
    invoke-static {v0, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1351
    .end local v1    # "ch":I
    :cond_2
    aput v2, p1, v4

    .line 1352
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method public static quotedIndexOf(Ljava/lang/String;IILjava/lang/String;)I
    .locals 4
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "setOfChars"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x27

    .line 1536
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_3

    .line 1537
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1538
    .local v0, "c":C
    const/16 v2, 0x5c

    if-ne v0, v2, :cond_1

    .line 1539
    add-int/lit8 v1, v1, 0x1

    .line 1536
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1540
    :cond_1
    if-ne v0, v3, :cond_2

    .line 1542
    :goto_1
    add-int/lit8 v1, v1, 0x1

    if-ge v1, p2, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v3, :cond_0

    goto :goto_1

    .line 1543
    :cond_2
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    .line 1547
    .end local v0    # "c":C
    .end local v1    # "i":I
    :goto_2
    return v1

    .restart local v1    # "i":I
    :cond_3
    const/4 v1, -0x1

    goto :goto_2
.end method

.method private static recursiveAppendNumber(Ljava/lang/StringBuffer;III)V
    .locals 3
    .param p0, "result"    # Ljava/lang/StringBuffer;
    .param p1, "n"    # I
    .param p2, "radix"    # I
    .param p3, "minDigits"    # I

    .prologue
    .line 1396
    rem-int v0, p1, p2

    .line 1398
    .local v0, "digit":I
    if-ge p1, p2, :cond_0

    const/4 v1, 0x1

    if-le p3, v1, :cond_1

    .line 1399
    :cond_0
    div-int v1, p1, p2

    add-int/lit8 v2, p3, -0x1

    invoke-static {p0, v1, p2, v2}, Lcom/ibm/icu/impl/Utility;->recursiveAppendNumber(Ljava/lang/StringBuffer;III)V

    .line 1402
    :cond_1
    sget-object v1, Lcom/ibm/icu/impl/Utility;->DIGITS:[C

    aget-char v1, v1, v0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1403
    return-void
.end method

.method public static repeat(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "count"    # I

    .prologue
    .line 1795
    if-gtz p1, :cond_1

    const-string/jumbo p0, ""

    .line 1801
    .end local p0    # "s":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 1796
    .restart local p0    # "s":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    .line 1797
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1798
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_2

    .line 1799
    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1798
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1801
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static replaceAll(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "replacement"    # Ljava/lang/String;

    .prologue
    .line 1861
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static skipWhitespace(Ljava/lang/String;I)I
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .prologue
    .line 1081
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1082
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1083
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1088
    .end local v0    # "c":I
    :cond_0
    return p1

    .line 1086
    .restart local v0    # "c":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    add-int/2addr p1, v1

    .line 1087
    goto :goto_0
.end method

.method public static skipWhitespace(Ljava/lang/String;[I)V
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "pos"    # [I

    .prologue
    const/4 v1, 0x0

    .line 1096
    aget v0, p1, v1

    invoke-static {p0, v0}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result v0

    aput v0, p1, v1

    .line 1097
    return-void
.end method

.method public static split(Ljava/lang/String;C[Ljava/lang/String;)V
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "divider"    # C
    .param p2, "output"    # [Ljava/lang/String;

    .prologue
    .line 1020
    const/4 v3, 0x0

    .line 1021
    .local v3, "last":I
    const/4 v0, 0x0

    .line 1023
    .local v0, "current":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1024
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, p1, :cond_0

    .line 1025
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "current":I
    .local v1, "current":I
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, p2, v0

    .line 1026
    add-int/lit8 v3, v2, 0x1

    move v0, v1

    .line 1023
    .end local v1    # "current":I
    .restart local v0    # "current":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1029
    :cond_1
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "current":I
    .restart local v1    # "current":I
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, p2, v0

    move v0, v1

    .line 1030
    .end local v1    # "current":I
    .restart local v0    # "current":I
    :goto_1
    array-length v4, p2

    if-ge v0, v4, :cond_2

    .line 1031
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "current":I
    .restart local v1    # "current":I
    const-string/jumbo v4, ""

    aput-object v4, p2, v0

    move v0, v1

    .line 1032
    .end local v1    # "current":I
    .restart local v0    # "current":I
    goto :goto_1

    .line 1033
    :cond_2
    return-void
.end method

.method public static split(Ljava/lang/String;C)[Ljava/lang/String;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "divider"    # C

    .prologue
    .line 1045
    const/4 v1, 0x0

    .line 1047
    .local v1, "last":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1048
    .local v2, "output":Ljava/util/ArrayList;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1049
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, p1, :cond_0

    .line 1050
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1051
    add-int/lit8 v1, v0, 0x1

    .line 1048
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1054
    :cond_1
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1055
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    check-cast v3, [Ljava/lang/String;

    return-object v3
.end method

.method public static splitString(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 1887
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "\\Q"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "\\E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static splitWhitespace(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "src"    # Ljava/lang/String;

    .prologue
    .line 1923
    const-string/jumbo v0, "\\s+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static trim(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "b"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1361
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1362
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1363
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_1
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1364
    :cond_1
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    move-result-object v1

    return-object v1
.end method

.method public static unescape(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 874
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 875
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v6, 0x1

    new-array v5, v6, [I

    .line 876
    .local v5, "pos":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 877
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 878
    .local v1, "c":C
    const/16 v6, 0x5c

    if-ne v1, v6, :cond_1

    .line 879
    aput v4, v5, v7

    .line 880
    invoke-static {p0, v5}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v2

    .line 881
    .local v2, "e":I
    if-gez v2, :cond_0

    .line 882
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Invalid escape sequence "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    add-int/lit8 v8, v4, -0x1

    add-int/lit8 v9, v4, 0x8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {p0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 885
    :cond_0
    invoke-static {v0, v2}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 886
    aget v3, v5, v7

    .line 887
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 888
    .end local v2    # "e":I
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 891
    .end local v1    # "c":C
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static unescapeAt(Ljava/lang/String;[I)I
    .locals 15
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "offset16"    # [I

    .prologue
    .line 750
    const/4 v12, 0x0

    .line 751
    .local v12, "result":I
    const/4 v9, 0x0

    .line 752
    .local v9, "n":I
    const/4 v8, 0x0

    .line 753
    .local v8, "minDig":I
    const/4 v7, 0x0

    .line 754
    .local v7, "maxDig":I
    const/4 v1, 0x4

    .line 757
    .local v1, "bitsPerDigit":I
    const/4 v2, 0x0

    .line 760
    .local v2, "braces":Z
    const/4 v13, 0x0

    aget v11, p1, v13

    .line 761
    .local v11, "offset":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 762
    .local v6, "length":I
    if-ltz v11, :cond_0

    if-lt v11, v6, :cond_1

    .line 763
    :cond_0
    const/4 v3, -0x1

    .line 865
    :goto_0
    return v3

    .line 767
    :cond_1
    invoke-static {p0, v11}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v3

    .line 768
    .local v3, "c":I
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v13

    add-int/2addr v11, v13

    .line 771
    sparse-switch v3, :sswitch_data_0

    .line 789
    const/16 v13, 0x8

    invoke-static {v3, v13}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v4

    .line 790
    .local v4, "dig":I
    if-ltz v4, :cond_2

    .line 791
    const/4 v8, 0x1

    .line 792
    const/4 v7, 0x3

    .line 793
    const/4 v9, 0x1

    .line 794
    const/4 v1, 0x3

    .line 795
    move v12, v4

    .line 799
    .end local v4    # "dig":I
    :cond_2
    :goto_1
    if-eqz v8, :cond_e

    .line 800
    :goto_2
    if-ge v11, v6, :cond_3

    if-ge v9, v7, :cond_3

    .line 801
    invoke-static {p0, v11}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v3

    .line 802
    const/4 v13, 0x3

    if-ne v1, v13, :cond_5

    const/16 v13, 0x8

    :goto_3
    invoke-static {v3, v13}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v4

    .line 803
    .restart local v4    # "dig":I
    if-gez v4, :cond_6

    .line 810
    .end local v4    # "dig":I
    :cond_3
    if-ge v9, v8, :cond_7

    .line 811
    const/4 v3, -0x1

    goto :goto_0

    .line 773
    :sswitch_0
    const/4 v7, 0x4

    move v8, v7

    .line 774
    goto :goto_1

    .line 776
    :sswitch_1
    const/16 v7, 0x8

    move v8, v7

    .line 777
    goto :goto_1

    .line 779
    :sswitch_2
    const/4 v8, 0x1

    .line 780
    if-ge v11, v6, :cond_4

    invoke-static {p0, v11}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v13

    const/16 v14, 0x7b

    if-ne v13, v14, :cond_4

    .line 781
    add-int/lit8 v11, v11, 0x1

    .line 782
    const/4 v2, 0x1

    .line 783
    const/16 v7, 0x8

    .line 784
    goto :goto_1

    .line 785
    :cond_4
    const/4 v7, 0x2

    .line 787
    goto :goto_1

    .line 802
    :cond_5
    const/16 v13, 0x10

    goto :goto_3

    .line 806
    .restart local v4    # "dig":I
    :cond_6
    shl-int v13, v12, v1

    or-int v12, v13, v4

    .line 807
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v13

    add-int/2addr v11, v13

    .line 808
    add-int/lit8 v9, v9, 0x1

    .line 809
    goto :goto_2

    .line 813
    .end local v4    # "dig":I
    :cond_7
    if-eqz v2, :cond_9

    .line 814
    const/16 v13, 0x7d

    if-eq v3, v13, :cond_8

    .line 815
    const/4 v3, -0x1

    goto :goto_0

    .line 817
    :cond_8
    add-int/lit8 v11, v11, 0x1

    .line 819
    :cond_9
    if-ltz v12, :cond_a

    const/high16 v13, 0x110000

    if-lt v12, v13, :cond_b

    .line 820
    :cond_a
    const/4 v3, -0x1

    goto :goto_0

    .line 826
    :cond_b
    if-ge v11, v6, :cond_d

    int-to-char v13, v12

    invoke-static {v13}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 828
    add-int/lit8 v0, v11, 0x1

    .line 829
    .local v0, "ahead":I
    invoke-virtual {p0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 830
    const/16 v13, 0x5c

    if-ne v3, v13, :cond_c

    if-ge v0, v6, :cond_c

    .line 831
    const/4 v13, 0x1

    new-array v10, v13, [I

    const/4 v13, 0x0

    aput v0, v10, v13

    .line 832
    .local v10, "o":[I
    invoke-static {p0, v10}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v3

    .line 833
    const/4 v13, 0x0

    aget v0, v10, v13

    .line 835
    .end local v10    # "o":[I
    :cond_c
    int-to-char v13, v3

    invoke-static {v13}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 836
    move v11, v0

    .line 837
    int-to-char v13, v12

    int-to-char v14, v3

    invoke-static {v13, v14}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v12

    .line 841
    .end local v0    # "ahead":I
    :cond_d
    const/4 v13, 0x0

    aput v11, p1, v13

    move v3, v12

    .line 842
    goto/16 :goto_0

    .line 846
    :cond_e
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_4
    sget-object v13, Lcom/ibm/icu/impl/Utility;->UNESCAPE_MAP:[C

    array-length v13, v13

    if-ge v5, v13, :cond_10

    .line 847
    sget-object v13, Lcom/ibm/icu/impl/Utility;->UNESCAPE_MAP:[C

    aget-char v13, v13, v5

    if-ne v3, v13, :cond_f

    .line 848
    const/4 v13, 0x0

    aput v11, p1, v13

    .line 849
    sget-object v13, Lcom/ibm/icu/impl/Utility;->UNESCAPE_MAP:[C

    add-int/lit8 v14, v5, 0x1

    aget-char v3, v13, v14

    goto/16 :goto_0

    .line 850
    :cond_f
    sget-object v13, Lcom/ibm/icu/impl/Utility;->UNESCAPE_MAP:[C

    aget-char v13, v13, v5

    if-ge v3, v13, :cond_11

    .line 856
    :cond_10
    const/16 v13, 0x63

    if-ne v3, v13, :cond_12

    if-ge v11, v6, :cond_12

    .line 857
    invoke-static {p0, v11}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v3

    .line 858
    const/4 v13, 0x0

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v14

    add-int/2addr v14, v11

    aput v14, p1, v13

    .line 859
    and-int/lit8 v3, v3, 0x1f

    goto/16 :goto_0

    .line 846
    :cond_11
    add-int/lit8 v5, v5, 0x2

    goto :goto_4

    .line 864
    :cond_12
    const/4 v13, 0x0

    aput v11, p1, v13

    goto/16 :goto_0

    .line 771
    nop

    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_1
        0x75 -> :sswitch_0
        0x78 -> :sswitch_2
    .end sparse-switch
.end method

.method public static unescapeLeniently(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 899
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 900
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v6, 0x1

    new-array v5, v6, [I

    .line 901
    .local v5, "pos":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 902
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 903
    .local v1, "c":C
    const/16 v6, 0x5c

    if-ne v1, v6, :cond_1

    .line 904
    aput v4, v5, v7

    .line 905
    invoke-static {p0, v5}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v2

    .line 906
    .local v2, "e":I
    if-gez v2, :cond_0

    .line 907
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v3, v4

    .line 908
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 909
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_0
    invoke-static {v0, v2}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 910
    aget v3, v5, v7

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 913
    .end local v2    # "e":I
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 916
    .end local v1    # "c":C
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static valueOf([I)Ljava/lang/String;
    .locals 3
    .param p0, "source"    # [I

    .prologue
    .line 1781
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v2, p0

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1782
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 1783
    aget v2, p0, v0

    invoke-static {v1, v2}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 1782
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1785
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
