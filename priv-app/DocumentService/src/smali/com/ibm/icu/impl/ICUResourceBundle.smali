.class public Lcom/ibm/icu/impl/ICUResourceBundle;
.super Lcom/ibm/icu/util/UResourceBundle;
.source "ICUResourceBundle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DEFAULT_TAG:Ljava/lang/String; = "default"

.field public static final FROM_DEFAULT:I = 0x3

.field public static final FROM_FALLBACK:I = 0x1

.field public static final FROM_LOCALE:I = 0x4

.field public static final FROM_ROOT:I = 0x2

.field private static GET_AVAILABLE_CACHE:Ljava/lang/ref/SoftReference; = null

.field private static final HYPHEN:C = '-'

.field private static final ICUDATA:Ljava/lang/String; = "ICUDATA"

.field public static final ICU_BASE_NAME:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b"

.field public static final ICU_BRKITR_BASE_NAME:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b/brkitr"

.field public static final ICU_BRKITR_NAME:Ljava/lang/String; = "/brkitr"

.field public static final ICU_BUNDLE:Ljava/lang/String; = "data/icudt40b"

.field public static final ICU_COLLATION_BASE_NAME:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b/coll"

.field public static final ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

.field protected static final ICU_DATA_PATH:Ljava/lang/String; = "com/ibm/icu/impl/"

.field public static final ICU_RBNF_BASE_NAME:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b/rbnf"

.field private static final ICU_RESOURCE_INDEX:Ljava/lang/String; = "res_index"

.field public static final ICU_TRANSLIT_BASE_NAME:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b/translit"

.field protected static final INSTALLED_LOCALES:Ljava/lang/String; = "InstalledLocales"

.field private static final LOCALE:Ljava/lang/String; = "LOCALE"

.field private static final MAX_INITIAL_LOOKUP_SIZE:I = 0x40

.field private static final RES_PATH_SEP_CHAR:C = '/'

.field private static final RES_PATH_SEP_STR:Ljava/lang/String; = "/"

.field protected static final UNSIGNED_INT_MASK:J = 0xffffffffL

.field static class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;


# instance fields
.field protected baseName:Ljava/lang/String;

.field protected loader:Ljava/lang/ClassLoader;

.field private loadingStatus:I

.field protected localeID:Ljava/lang/String;

.field protected lookup:Lcom/ibm/icu/impl/ICUCache;

.field protected noFallback:Z

.field protected rawData:[B

.field protected resPath:Ljava/lang/String;

.field protected rootResource:J

.field protected ulocale:Lcom/ibm/icu/util/ULocale;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 87
    sget-object v1, Lcom/ibm/icu/impl/ICUResourceBundle;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string/jumbo v1, "com.ibm.icu.impl.ICUData"

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/impl/ICUResourceBundle;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 88
    .local v0, "loader":Ljava/lang/ClassLoader;
    if-nez v0, :cond_0

    .line 89
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 91
    :cond_0
    sput-object v0, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    .line 464
    const-string/jumbo v1, "localedata"

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/ibm/icu/impl/ICUResourceBundle;->DEBUG:Z

    return-void

    .line 87
    .end local v0    # "loader":Ljava/lang/ClassLoader;
    :cond_1
    sget-object v1, Lcom/ibm/icu/impl/ICUResourceBundle;->class$com$ibm$icu$impl$ICUData:Ljava/lang/Class;

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 896
    invoke-direct {p0}, Lcom/ibm/icu/util/UResourceBundle;-><init>()V

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->loadingStatus:I

    .line 896
    return-void
.end method

.method protected static final RES_GET_INT(J)I
    .locals 2
    .param p0, "res"    # J

    .prologue
    .line 906
    const/4 v0, 0x4

    shl-long v0, p0, v0

    long-to-int v0, v0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method static final RES_GET_KEY([BI)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "rawData"    # [B
    .param p1, "keyOffset"    # I

    .prologue
    .line 915
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 916
    .local v1, "key":Ljava/lang/StringBuffer;
    :goto_0
    aget-byte v2, p0, p1

    int-to-char v0, v2

    .local v0, "ch":C
    if-eqz v0, :cond_0

    .line 917
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 918
    add-int/lit8 p1, p1, 0x1

    .line 919
    goto :goto_0

    .line 920
    :cond_0
    return-object v1
.end method

.method protected static final RES_GET_OFFSET(J)I
    .locals 4
    .param p0, "res"    # J

    .prologue
    .line 902
    const-wide/32 v0, 0xfffffff

    and-long/2addr v0, p0

    const/4 v2, 0x2

    shl-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static final RES_GET_TYPE(J)I
    .locals 2
    .param p0, "res"    # J

    .prologue
    .line 899
    const/16 v0, 0x1c

    shr-long v0, p0, v0

    long-to-int v0, v0

    return v0
.end method

.method static final RES_GET_UINT(J)J
    .locals 4
    .param p0, "res"    # J

    .prologue
    .line 909
    const-wide/32 v2, 0xfffffff

    and-long v0, p0, v2

    .line 910
    .local v0, "t":J
    return-wide v0
.end method

.method static access$000(Ljava/lang/String;Ljava/lang/ClassLoader;)[Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/ClassLoader;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundle;->createULocaleList(Ljava/lang/String;Ljava/lang/ClassLoader;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method static access$100(Ljava/lang/String;)[Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->createLocaleList(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method static access$200(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->createLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static access$300(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->createFullLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static final assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 2
    .param p0, "b1"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p1, "b2"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 971
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    .line 972
    iget-wide v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->rootResource:J

    iput-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rootResource:J

    .line 973
    iget-boolean v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->noFallback:Z

    iput-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->noFallback:Z

    .line 974
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    .line 975
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->localeID:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->localeID:Ljava/lang/String;

    .line 976
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->ulocale:Lcom/ibm/icu/util/ULocale;

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 977
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->loader:Ljava/lang/ClassLoader;

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->loader:Ljava/lang/ClassLoader;

    .line 978
    iget-object v0, p1, Lcom/ibm/icu/impl/ICUResourceBundle;->parent:Ljava/util/ResourceBundle;

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->parent:Ljava/util/ResourceBundle;

    .line 979
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 87
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static createBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 2
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeID"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;

    .prologue
    .line 829
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getReader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundleReader;

    move-result-object v0

    .line 831
    .local v0, "reader":Lcom/ibm/icu/impl/ICUResourceBundleReader;
    if-nez v0, :cond_0

    .line 832
    const/4 v1, 0x0

    .line 834
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0, p0, p1, p2}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundle(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v1

    goto :goto_0
.end method

.method private static final createFullLocaleNameArray(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/util/ArrayList;
    .locals 2
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "root"    # Ljava/lang/ClassLoader;

    .prologue
    .line 514
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundle$1;

    invoke-direct {v1, p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundle$1;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    invoke-static {v1}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 560
    .local v0, "list":Ljava/util/ArrayList;
    return-object v0
.end method

.method private static createFullLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .param p0, "baseName"    # Ljava/lang/String;

    .prologue
    .line 564
    sget-object v2, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {p0, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->createFullLocaleNameArray(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 565
    .local v0, "list":Ljava/util/ArrayList;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 566
    .local v1, "set":Ljava/util/HashSet;
    if-nez v0, :cond_0

    .line 567
    new-instance v2, Ljava/util/MissingResourceException;

    const-string/jumbo v3, "Could not find res_index"

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 569
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 570
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    return-object v2
.end method

.method private static final createLocaleList(Ljava/lang/String;)[Ljava/util/Locale;
    .locals 2
    .param p0, "baseName"    # Ljava/lang/String;

    .prologue
    .line 491
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getULocaleList()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 492
    .local v0, "ulocales":[Lcom/ibm/icu/util/ULocale;
    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleList([Lcom/ibm/icu/util/ULocale;)[Ljava/util/Locale;

    move-result-object v1

    return-object v1
.end method

.method private static final createLocaleNameArray(Ljava/lang/String;Ljava/lang/ClassLoader;)[Ljava/lang/String;
    .locals 8
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "root"    # Ljava/lang/ClassLoader;

    .prologue
    .line 497
    const-string/jumbo v6, "res_index"

    const/4 v7, 0x1

    invoke-static {p0, v6, p1, v7}, Lcom/ibm/icu/util/UResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 498
    .local v0, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v6, "InstalledLocales"

    invoke-virtual {v0, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .end local v0    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 499
    .restart local v0    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v4

    .line 500
    .local v4, "length":I
    const/4 v1, 0x0

    .line 501
    .local v1, "i":I
    new-array v5, v4, [Ljava/lang/String;

    .line 502
    .local v5, "locales":[Ljava/lang/String;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getIterator()Lcom/ibm/icu/util/UResourceBundleIterator;

    move-result-object v3

    .line 503
    .local v3, "iter":Lcom/ibm/icu/util/UResourceBundleIterator;
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->reset()V

    .line 504
    :goto_0
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 505
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->next()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    invoke-virtual {v6}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    move v1, v2

    .line 506
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 507
    :cond_0
    const/4 v0, 0x0

    .line 508
    return-object v5
.end method

.method private static createLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .param p0, "baseName"    # Ljava/lang/String;

    .prologue
    .line 575
    :try_start_0
    sget-object v3, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {p0, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->createLocaleNameArray(Ljava/lang/String;Ljava/lang/ClassLoader;)[Ljava/lang/String;

    move-result-object v1

    .line 577
    .local v1, "locales":[Ljava/lang/String;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 578
    .local v2, "set":Ljava/util/HashSet;
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 579
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 586
    .end local v1    # "locales":[Ljava/lang/String;
    .end local v2    # "set":Ljava/util/HashSet;
    :goto_0
    return-object v3

    .line 580
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e":Ljava/util/MissingResourceException;
    sget-boolean v3, Lcom/ibm/icu/impl/ICUResourceBundle;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 582
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "couldn\'t find index for bundleName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 583
    invoke-static {}, Ljava/lang/Thread;->dumpStack()V

    .line 586
    :cond_0
    sget-object v3, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    goto :goto_0
.end method

.method private static final createULocaleList(Ljava/lang/String;Ljava/lang/ClassLoader;)[Lcom/ibm/icu/util/ULocale;
    .locals 8
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "root"    # Ljava/lang/ClassLoader;

    .prologue
    .line 475
    const-string/jumbo v6, "res_index"

    const/4 v7, 0x1

    invoke-static {p0, v6, p1, v7}, Lcom/ibm/icu/util/UResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 477
    .local v0, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v6, "InstalledLocales"

    invoke-virtual {v0, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .end local v0    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 478
    .restart local v0    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v4

    .line 479
    .local v4, "length":I
    const/4 v1, 0x0

    .line 480
    .local v1, "i":I
    new-array v5, v4, [Lcom/ibm/icu/util/ULocale;

    .line 481
    .local v5, "locales":[Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getIterator()Lcom/ibm/icu/util/UResourceBundleIterator;

    move-result-object v3

    .line 482
    .local v3, "iter":Lcom/ibm/icu/util/UResourceBundleIterator;
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->reset()V

    .line 483
    :goto_0
    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 484
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    new-instance v6, Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v3}, Lcom/ibm/icu/util/UResourceBundleIterator;->next()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    aput-object v6, v5, v1

    move v1, v2

    .line 485
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 486
    :cond_0
    const/4 v0, 0x0

    .line 487
    return-object v5
.end method

.method private findResource(Ljava/lang/String;JLjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 20
    .param p1, "_key"    # Ljava/lang/String;
    .param p2, "_resource"    # J
    .param p4, "table"    # Ljava/util/HashMap;
    .param p5, "requested"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 1085
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/ICUResourceBundle;->loader:Ljava/lang/ClassLoader;

    .line 1086
    .local v11, "loaderToUse":Ljava/lang/ClassLoader;
    const/4 v12, 0x0

    .local v12, "locale":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1088
    .local v10, "keyPath":Ljava/lang/String;
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringValue(J)Ljava/lang/String;

    move-result-object v13

    .line 1089
    .local v13, "rpath":Ljava/lang/String;
    if-nez p4, :cond_0

    .line 1090
    new-instance p4, Ljava/util/HashMap;

    .end local p4    # "table":Ljava/util/HashMap;
    invoke-direct/range {p4 .. p4}, Ljava/util/HashMap;-><init>()V

    .line 1092
    .restart local p4    # "table":Ljava/util/HashMap;
    :cond_0
    move-object/from16 v0, p4

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_1

    .line 1093
    new-instance v17, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v18, "Circular references in the resource bundles"

    invoke-direct/range {v17 .. v18}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1096
    :cond_1
    const-string/jumbo v17, ""

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v13, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    const/16 v17, 0x2f

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    if-nez v17, :cond_5

    .line 1098
    const/16 v17, 0x2f

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    .line 1099
    .local v7, "i":I
    const/16 v17, 0x2f

    add-int/lit8 v18, v7, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v9

    .line 1100
    .local v9, "j":I
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1101
    .local v5, "bundleName":Ljava/lang/String;
    add-int/lit8 v17, v7, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 1102
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v9, v0, :cond_2

    .line 1103
    add-int/lit8 v17, v7, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1104
    add-int/lit8 v17, v9, 0x1

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1107
    :cond_2
    const-string/jumbo v17, "ICUDATA"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1108
    const-string/jumbo v5, "com/ibm/icu/impl/data/icudt40b"

    .line 1109
    sget-object v11, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    .line 1129
    .end local v9    # "j":I
    :cond_3
    :goto_0
    const/4 v4, 0x0

    .line 1130
    .local v4, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/4 v15, 0x0

    .line 1131
    .local v15, "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v17, "LOCALE"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1132
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    move-object/from16 v4, p5

    .line 1133
    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1134
    const-string/jumbo v17, "LOCALE"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, 0x2

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v17, p5

    .line 1135
    check-cast v17, Lcom/ibm/icu/impl/ICUResourceBundle;

    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v12

    .line 1136
    const/16 v17, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, v17

    invoke-static {v10, v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->findResourceWithFallback(Ljava/lang/String;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v15

    .line 1137
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string/jumbo v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v15, Lcom/ibm/icu/impl/ICUResourceBundle;->resPath:Ljava/lang/String;

    .line 1166
    :goto_1
    if-nez v15, :cond_c

    .line 1167
    new-instance v17, Ljava/util/MissingResourceException;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUResourceBundle;->localeID:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v17

    .line 1110
    .end local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v9    # "j":I
    :cond_4
    const-string/jumbo v17, "ICUDATA"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_3

    .line 1111
    const/16 v17, 0x2d

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 1112
    .local v8, "idx":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v8, v0, :cond_3

    .line 1113
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v18, "com/ibm/icu/impl/data/icudt40b/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    add-int/lit8 v18, v8, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1114
    sget-object v11, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    goto/16 :goto_0

    .line 1119
    .end local v5    # "bundleName":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "idx":I
    .end local v9    # "j":I
    :cond_5
    const/16 v17, 0x2f

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 1120
    .restart local v7    # "i":I
    add-int/lit8 v17, v7, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1121
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v7, v0, :cond_6

    .line 1122
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v13, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1127
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    .restart local v5    # "bundleName":Ljava/lang/String;
    goto/16 :goto_0

    .line 1124
    .end local v5    # "bundleName":Ljava/lang/String;
    :cond_6
    move-object v12, v10

    .line 1125
    const/4 v10, 0x0

    goto :goto_2

    .line 1139
    .restart local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v5    # "bundleName":Ljava/lang/String;
    .restart local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_7
    if-nez v12, :cond_9

    .line 1141
    const-string/jumbo v17, ""

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v5, v0, v11, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .end local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1147
    .restart local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_3
    if-eqz v10, :cond_b

    .line 1148
    new-instance v14, Lcom/ibm/icu/util/StringTokenizer;

    const-string/jumbo v17, "/"

    move-object/from16 v0, v17

    invoke-direct {v14, v10, v0}, Lcom/ibm/icu/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    .local v14, "st":Lcom/ibm/icu/util/StringTokenizer;
    move-object v6, v4

    .line 1150
    .local v6, "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_4
    invoke-virtual {v14}, Lcom/ibm/icu/util/StringTokenizer;->hasMoreTokens()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1151
    invoke-virtual {v14}, Lcom/ibm/icu/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v16

    .line 1152
    .local v16, "subKey":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-virtual {v6, v0, v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1153
    .restart local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-nez v15, :cond_a

    .line 1164
    .end local v6    # "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v14    # "st":Lcom/ibm/icu/util/StringTokenizer;
    .end local v16    # "subKey":Ljava/lang/String;
    :cond_8
    :goto_5
    iput-object v13, v15, Lcom/ibm/icu/impl/ICUResourceBundle;->resPath:Ljava/lang/String;

    goto/16 :goto_1

    .line 1144
    :cond_9
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v5, v12, v11, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .end local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    .restart local v4    # "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    goto :goto_3

    .line 1156
    .restart local v6    # "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v14    # "st":Lcom/ibm/icu/util/StringTokenizer;
    .restart local v16    # "subKey":Ljava/lang/String;
    :cond_a
    move-object v6, v15

    .line 1157
    goto :goto_4

    .line 1162
    .end local v6    # "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v14    # "st":Lcom/ibm/icu/util/StringTokenizer;
    .end local v16    # "subKey":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .restart local v15    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    goto :goto_5

    .line 1169
    :cond_c
    return-object v15
.end method

.method protected static final findResourceWithFallback(Ljava/lang/String;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "actualBundle"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p2, "requested"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 661
    const/4 v2, 0x0

    .line 662
    .local v2, "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-nez p2, :cond_0

    .line 663
    move-object p2, p1

    .line 665
    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    .line 666
    new-instance v1, Lcom/ibm/icu/util/StringTokenizer;

    const-string/jumbo v4, "/"

    invoke-direct {v1, p0, v4}, Lcom/ibm/icu/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .local v1, "st":Lcom/ibm/icu/util/StringTokenizer;
    move-object v0, p1

    .line 667
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 668
    .local v0, "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_1
    invoke-virtual {v1}, Lcom/ibm/icu/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 669
    invoke-virtual {v1}, Lcom/ibm/icu/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 670
    .local v3, "subKey":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, p2}, Lcom/ibm/icu/impl/ICUResourceBundle;->handleGet(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .end local v2    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v2, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 671
    .restart local v2    # "sub":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-nez v2, :cond_4

    .line 676
    .end local v3    # "subKey":Ljava/lang/String;
    :cond_1
    if-eqz v2, :cond_5

    .line 687
    .end local v0    # "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v1    # "st":Lcom/ibm/icu/util/StringTokenizer;
    :cond_2
    if-eqz v2, :cond_3

    .line 688
    check-cast p2, Lcom/ibm/icu/impl/ICUResourceBundle;

    .end local p2    # "requested":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {p2}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(Ljava/lang/String;)V

    .line 690
    :cond_3
    return-object v2

    .line 674
    .restart local v0    # "current":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v1    # "st":Lcom/ibm/icu/util/StringTokenizer;
    .restart local v3    # "subKey":Ljava/lang/String;
    .restart local p2    # "requested":Lcom/ibm/icu/util/UResourceBundle;
    :cond_4
    move-object v0, v2

    .line 675
    goto :goto_1

    .end local v3    # "subKey":Ljava/lang/String;
    :cond_5
    move-object v4, p1

    .line 680
    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    iget-object v4, v4, Lcom/ibm/icu/impl/ICUResourceBundle;->resPath:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    .line 681
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    move-object v4, p1

    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    iget-object v4, v4, Lcom/ibm/icu/impl/ICUResourceBundle;->resPath:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 684
    :cond_6
    check-cast p1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .end local p1    # "actualBundle":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object p1

    .line 686
    .restart local p1    # "actualBundle":Lcom/ibm/icu/util/UResourceBundle;
    goto :goto_0
.end method

.method private static getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 636
    const/4 v0, 0x0

    .line 637
    .local v0, "ae":Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    const/4 v1, 0x0

    .line 638
    .local v1, "lcache":Ljava/util/Map;
    sget-object v2, Lcom/ibm/icu/impl/ICUResourceBundle;->GET_AVAILABLE_CACHE:Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_0

    .line 639
    sget-object v2, Lcom/ibm/icu/impl/ICUResourceBundle;->GET_AVAILABLE_CACHE:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "lcache":Ljava/util/Map;
    check-cast v1, Ljava/util/Map;

    .line 640
    .restart local v1    # "lcache":Ljava/util/Map;
    if-eqz v1, :cond_0

    .line 641
    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ae":Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    .line 645
    .restart local v0    # "ae":Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    :cond_0
    if-nez v0, :cond_1

    .line 646
    new-instance v0, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    .end local v0    # "ae":Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;-><init>(Ljava/lang/String;)V

    .line 647
    .restart local v0    # "ae":Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;
    if-nez v1, :cond_2

    .line 648
    new-instance v1, Ljava/util/HashMap;

    .end local v1    # "lcache":Ljava/util/Map;
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 649
    .restart local v1    # "lcache":Ljava/util/Map;
    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 650
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/ibm/icu/impl/ICUResourceBundle;->GET_AVAILABLE_CACHE:Ljava/lang/ref/SoftReference;

    .line 656
    :cond_1
    :goto_0
    return-object v0

    .line 652
    :cond_2
    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static getAvailableLocaleNameSet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 391
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getAvailableLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "bundlePrefix"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getLocaleNameSet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 423
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getLocaleList()[Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;
    .locals 1
    .param p0, "baseName"    # Ljava/lang/String;

    .prologue
    .line 415
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getLocaleList()[Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 407
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p0, "baseName"    # Ljava/lang/String;

    .prologue
    .line 399
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getULocaleList()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method private static getBundle(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 12
    .param p0, "reader"    # Lcom/ibm/icu/impl/ICUResourceBundleReader;
    .param p1, "baseName"    # Ljava/lang/String;
    .param p2, "localeID"    # Ljava/lang/String;
    .param p3, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 867
    const-wide v8, 0xffffffffL

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getRootResource()I

    move-result v10

    int-to-long v10, v10

    and-long v4, v8, v10

    .line 869
    .local v4, "rootResource":J
    invoke-static {v4, v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->RES_GET_TYPE(J)I

    move-result v7

    .line 870
    .local v7, "type":I
    const/4 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 871
    new-instance v6, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;-><init>(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 872
    .local v6, "table":Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;
    invoke-virtual {v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getSize()I

    move-result v8

    const/4 v9, 0x1

    if-lt v8, v9, :cond_0

    .line 873
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v6, v8, v9, v6, v10}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 874
    .local v1, "b":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 877
    .local v2, "itemKey":Ljava/lang/String;
    const-string/jumbo v8, "%%ALIAS"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 878
    invoke-virtual {v1}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v3

    .line 879
    .local v3, "locale":Ljava/lang/String;
    invoke-static {p1, v3}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 880
    .local v0, "actual":Lcom/ibm/icu/util/UResourceBundle;
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;

    .end local v0    # "actual":Lcom/ibm/icu/util/UResourceBundle;
    move-object v6, v0

    .line 890
    .end local v1    # "b":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "itemKey":Ljava/lang/String;
    .end local v3    # "locale":Ljava/lang/String;
    .end local v6    # "table":Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;
    :cond_0
    :goto_0
    return-object v6

    .line 887
    :cond_1
    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    .line 890
    new-instance v6, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;-><init>(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    goto :goto_0

    .line 892
    :cond_2
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string/jumbo v9, "Invalid format error"

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public static getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 5
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeID"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;
    .param p3, "disableFallback"    # Z

    .prologue
    .line 705
    invoke-static {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 706
    .local v0, "b":Lcom/ibm/icu/util/UResourceBundle;
    if-nez v0, :cond_0

    .line 707
    new-instance v1, Ljava/util/MissingResourceException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Could not find the bundle "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, ".res"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 709
    :cond_0
    return-object v0
.end method

.method static getChar([BI)C
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 1037
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundle;->makeChar([BI)C

    move-result v0

    return v0
.end method

.method static final getCharOffset(I)I
    .locals 1
    .param p0, "offset"    # I

    .prologue
    .line 926
    shl-int/lit8 v0, p0, 0x1

    return v0
.end method

.method public static getFullLocaleNameSet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 373
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getFullLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getFullLocaleNameSet(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "bundlePrefix"    # Ljava/lang/String;

    .prologue
    .line 383
    invoke-static {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getFullLocaleNameSet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final getFunctionalEquivalent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;[ZZ)Lcom/ibm/icu/util/ULocale;
    .locals 22
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "resName"    # Ljava/lang/String;
    .param p2, "keyword"    # Ljava/lang/String;
    .param p3, "locID"    # Lcom/ibm/icu/util/ULocale;
    .param p4, "isAvailable"    # [Z
    .param p5, "omitDefault"    # Z

    .prologue
    .line 154
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/ULocale;->getKeywordValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 155
    .local v12, "kwVal":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v5

    .line 156
    .local v5, "baseLoc":Ljava/lang/String;
    const/4 v8, 0x0

    .line 157
    .local v8, "defStr":Ljava/lang/String;
    new-instance v14, Lcom/ibm/icu/util/ULocale;

    invoke-direct {v14, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    .line 158
    .local v14, "parent":Lcom/ibm/icu/util/ULocale;
    const/4 v7, 0x0

    .line 159
    .local v7, "defLoc":Lcom/ibm/icu/util/ULocale;
    const/4 v13, 0x0

    .line 160
    .local v13, "lookForDefault":Z
    const/4 v9, 0x0

    .line 161
    .local v9, "fullBase":Lcom/ibm/icu/util/ULocale;
    const/4 v6, 0x0

    .line 162
    .local v6, "defDepth":I
    const/16 v16, 0x0

    .line 164
    .local v16, "resDepth":I
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_0

    const-string/jumbo v18, "default"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 166
    :cond_0
    const-string/jumbo v12, ""

    .line 167
    const/4 v13, 0x1

    .line 171
    :cond_1
    const/4 v15, 0x0

    .line 173
    .local v15, "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 174
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-eqz p4, :cond_2

    .line 175
    const/16 v18, 0x0

    const/16 v19, 0x0

    aput-boolean v19, p4, v18

    .line 176
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailEntry(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/ibm/icu/impl/ICUResourceBundle$AvailEntry;->getULocaleList()[Lcom/ibm/icu/util/ULocale;

    move-result-object v4

    .line 177
    .local v4, "availableULocales":[Lcom/ibm/icu/util/ULocale;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    array-length v0, v4

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_2

    .line 178
    aget-object v18, v4, v10

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 179
    const/16 v18, 0x0

    const/16 v19, 0x1

    aput-boolean v19, p4, v18

    .line 187
    .end local v4    # "availableULocales":[Lcom/ibm/icu/util/ULocale;
    .end local v10    # "i":I
    :cond_2
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v11

    check-cast v11, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 188
    .local v11, "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v18, "default"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 189
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_3

    .line 190
    move-object v12, v8

    .line 191
    const/4 v13, 0x0

    .line 193
    :cond_3
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v7

    .line 197
    .end local v11    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_1
    if-nez v7, :cond_4

    .line 198
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 199
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    add-int/lit8 v6, v6, 0x1

    .line 201
    :cond_4
    if-eqz v15, :cond_5

    if-eqz v7, :cond_2

    .line 204
    :cond_5
    new-instance v14, Lcom/ibm/icu/util/ULocale;

    .end local v14    # "parent":Lcom/ibm/icu/util/ULocale;
    invoke-direct {v14, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    .line 205
    .restart local v14    # "parent":Lcom/ibm/icu/util/ULocale;
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 209
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_6
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v11

    check-cast v11, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 210
    .restart local v11    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v11, v12}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    .line 211
    invoke-virtual {v11}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v9

    .line 215
    if-eqz v9, :cond_7

    move/from16 v0, v16

    if-le v0, v6, :cond_7

    .line 216
    const-string/jumbo v18, "default"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 217
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 218
    move/from16 v6, v16

    .line 223
    .end local v11    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_7
    :goto_2
    if-nez v9, :cond_8

    .line 224
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 225
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    add-int/lit8 v16, v16, 0x1

    .line 227
    :cond_8
    if-eqz v15, :cond_9

    if-eqz v9, :cond_6

    .line 229
    :cond_9
    if-nez v9, :cond_e

    if-eqz v8, :cond_e

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_e

    .line 233
    move-object v12, v8

    .line 234
    new-instance v14, Lcom/ibm/icu/util/ULocale;

    .end local v14    # "parent":Lcom/ibm/icu/util/ULocale;
    invoke-direct {v14, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    .line 235
    .restart local v14    # "parent":Lcom/ibm/icu/util/ULocale;
    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 236
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/16 v16, 0x0

    .line 240
    :cond_a
    :try_start_2
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v11

    check-cast v11, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 241
    .restart local v11    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v11, v12}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 244
    .local v17, "urb":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v9

    .line 248
    invoke-virtual {v9}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getLocale()Ljava/util/Locale;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_b

    .line 249
    const/4 v9, 0x0

    .line 254
    :cond_b
    if-eqz v9, :cond_c

    move/from16 v0, v16

    if-le v0, v6, :cond_c

    .line 255
    const-string/jumbo v18, "default"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 256
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v7

    .line 257
    move/from16 v6, v16

    .line 262
    .end local v11    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v17    # "urb":Lcom/ibm/icu/util/UResourceBundle;
    :cond_c
    :goto_3
    if-nez v9, :cond_d

    .line 263
    invoke-virtual {v15}, Lcom/ibm/icu/impl/ICUResourceBundle;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    .end local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 264
    .restart local v15    # "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    add-int/lit8 v16, v16, 0x1

    .line 266
    :cond_d
    if-eqz v15, :cond_e

    if-eqz v9, :cond_a

    .line 269
    :cond_e
    if-nez v9, :cond_10

    .line 270
    new-instance v18, Ljava/util/MissingResourceException;

    const-string/jumbo v19, "Could not find locale containing requested or default keyword."

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    const-string/jumbo v21, "="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p0

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v18

    .line 177
    .restart local v4    # "availableULocales":[Lcom/ibm/icu/util/ULocale;
    .restart local v10    # "i":I
    :cond_f
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 275
    .end local v4    # "availableULocales":[Lcom/ibm/icu/util/ULocale;
    .end local v10    # "i":I
    :cond_10
    if-eqz p5, :cond_11

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_11

    move/from16 v0, v16

    if-gt v0, v6, :cond_11

    .line 280
    .end local v9    # "fullBase":Lcom/ibm/icu/util/ULocale;
    :goto_4
    return-object v9

    .restart local v9    # "fullBase":Lcom/ibm/icu/util/ULocale;
    :cond_11
    new-instance v18, Lcom/ibm/icu/util/ULocale;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "@"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string/jumbo v20, "="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    move-object/from16 v9, v18

    goto :goto_4

    .line 259
    :catch_0
    move-exception v18

    goto/16 :goto_3

    .line 220
    :catch_1
    move-exception v18

    goto/16 :goto_2

    .line 194
    :catch_2
    move-exception v18

    goto/16 :goto_1
.end method

.method protected static final getIndex(Ljava/lang/String;)I
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 1077
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 1078
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1080
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected static getInt([BI)I
    .locals 1
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 1046
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundle;->makeInt([BI)I

    move-result v0

    return v0
.end method

.method protected static final getIntOffset(I)I
    .locals 1
    .param p0, "offset"    # I

    .prologue
    .line 923
    shl-int/lit8 v0, p0, 0x2

    return v0
.end method

.method public static final getKeywordValues(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 292
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 293
    .local v5, "keywords":Ljava/util/Set;
    sget-object v8, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {p0, v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->createULocaleList(Ljava/lang/String;Ljava/lang/ClassLoader;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v6

    .line 296
    .local v6, "locales":[Lcom/ibm/icu/util/ULocale;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v8, v6

    if-ge v3, v8, :cond_2

    .line 298
    :try_start_0
    aget-object v8, v6, v3

    invoke-static {p0, v8}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 300
    .local v1, "b":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v1, p1}, Lcom/ibm/icu/util/UResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ibm/icu/impl/ICUResourceBundle;

    move-object v0, v8

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    move-object v4, v0

    .line 301
    .local v4, "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getKeys()Ljava/util/Enumeration;

    move-result-object v2

    .line 303
    .local v2, "e":Ljava/util/Enumeration;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 304
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    .line 305
    .local v7, "s":Ljava/lang/Object;
    instance-of v8, v7, Ljava/lang/String;

    if-eqz v8, :cond_0

    const-string/jumbo v8, "default"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 307
    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 310
    .end local v1    # "b":Lcom/ibm/icu/util/UResourceBundle;
    .end local v2    # "e":Ljava/util/Enumeration;
    .end local v4    # "irb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v7    # "s":Ljava/lang/Object;
    :catch_0
    move-exception v8

    .line 296
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 316
    :cond_2
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    check-cast v8, [Ljava/lang/String;

    return-object v8
.end method

.method public static final getLocaleList([Lcom/ibm/icu/util/ULocale;)[Ljava/util/Locale;
    .locals 5
    .param p0, "ulocales"    # [Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 434
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .local v1, "list":Ljava/util/ArrayList;
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 436
    .local v3, "uniqueSet":Ljava/util/HashSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_1

    .line 437
    aget-object v4, p0, v0

    invoke-virtual {v4}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v2

    .line 438
    .local v2, "loc":Ljava/util/Locale;
    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 439
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 436
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 443
    .end local v2    # "loc":Ljava/util/Locale;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/util/Locale;

    check-cast v4, [Ljava/util/Locale;

    return-object v4
.end method

.method protected static declared-synchronized instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 17
    .param p0, "baseName"    # Ljava/lang/String;
    .param p1, "localeID"    # Ljava/lang/String;
    .param p2, "root"    # Ljava/lang/ClassLoader;
    .param p3, "disableFallback"    # Z

    .prologue
    .line 714
    const-class v14, Lcom/ibm/icu/impl/ICUResourceBundle;

    monitor-enter v14

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v6

    .line 715
    .local v6, "defaultLocale":Lcom/ibm/icu/util/ULocale;
    move-object/from16 v9, p1

    .line 716
    .local v9, "localeName":Ljava/lang/String;
    const/16 v13, 0x40

    invoke-virtual {v9, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    if-lez v13, :cond_0

    .line 717
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/util/ULocale;->getBaseName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 719
    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getFullName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 720
    .local v7, "fullName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-static {v0, v7, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->loadFromCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 729
    .local v3, "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/16 v13, 0x2e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    const/4 v15, -0x1

    if-ne v13, v15, :cond_5

    const-string/jumbo v11, "root"

    .line 730
    .local v11, "rootLocale":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v13

    invoke-virtual {v13}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v5

    .line 732
    .local v5, "defaultID":Ljava/lang/String;
    const-string/jumbo v13, ""

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 733
    move-object v9, v11

    .line 735
    :cond_1
    sget-boolean v13, Lcom/ibm/icu/impl/ICUResourceBundle;->DEBUG:Z

    if-eqz v13, :cond_2

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v16, "Creating "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string/jumbo v16, " currently b is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 736
    :cond_2
    if-nez v3, :cond_8

    .line 737
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v9, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->createBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v3

    .line 739
    sget-boolean v13, Lcom/ibm/icu/impl/ICUResourceBundle;->DEBUG:Z

    if-eqz v13, :cond_3

    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v16, "The bundle created is: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string/jumbo v16, " and disableFallback="

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string/jumbo v16, " and bundle.getNoFallback="

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getNoFallback()Z

    move-result v13

    if-eqz v13, :cond_6

    const/4 v13, 0x1

    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 740
    :cond_3
    if-nez p3, :cond_4

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getNoFallback()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 741
    :cond_4
    move-object/from16 v0, p2

    invoke-static {v0, v7, v6, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->addToCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/UResourceBundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v3

    .line 786
    .end local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .local v4, "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_2
    monitor-exit v14

    return-object v4

    .line 729
    .end local v4    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v5    # "defaultID":Ljava/lang/String;
    .end local v11    # "rootLocale":Ljava/lang/String;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_5
    :try_start_1
    const-string/jumbo v11, ""

    goto/16 :goto_0

    .line 739
    .restart local v5    # "defaultID":Ljava/lang/String;
    .restart local v11    # "rootLocale":Ljava/lang/String;
    :cond_6
    const/4 v13, 0x0

    goto :goto_1

    .line 747
    :cond_7
    if-nez v3, :cond_b

    .line 748
    const/16 v13, 0x5f

    invoke-virtual {v9, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 749
    .local v8, "i":I
    const/4 v13, -0x1

    if-eq v8, v13, :cond_9

    .line 750
    const/4 v13, 0x0

    invoke-virtual {v9, v13, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 751
    .local v12, "temp":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v12, v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .end local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v3, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 752
    .restart local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v13

    invoke-virtual {v13, v12}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 753
    const/4 v13, 0x1

    invoke-virtual {v3, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V

    .end local v8    # "i":I
    .end local v12    # "temp":Ljava/lang/String;
    :cond_8
    :goto_3
    move-object v4, v3

    .line 786
    .end local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v4    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    goto :goto_2

    .line 756
    .end local v4    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v8    # "i":I
    :cond_9
    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v15, -0x1

    if-ne v13, v15, :cond_a

    .line 757
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    .end local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v3, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 758
    .restart local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-eqz v3, :cond_8

    .line 759
    const/4 v13, 0x3

    invoke-virtual {v3, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 714
    .end local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v5    # "defaultID":Ljava/lang/String;
    .end local v6    # "defaultLocale":Lcom/ibm/icu/util/ULocale;
    .end local v7    # "fullName":Ljava/lang/String;
    .end local v8    # "i":I
    .end local v9    # "localeName":Ljava/lang/String;
    .end local v11    # "rootLocale":Ljava/lang/String;
    :catchall_0
    move-exception v13

    monitor-exit v14

    throw v13

    .line 761
    .restart local v3    # "b":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v5    # "defaultID":Ljava/lang/String;
    .restart local v6    # "defaultLocale":Lcom/ibm/icu/util/ULocale;
    .restart local v7    # "fullName":Ljava/lang/String;
    .restart local v8    # "i":I
    .restart local v9    # "localeName":Ljava/lang/String;
    .restart local v11    # "rootLocale":Ljava/lang/String;
    :cond_a
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_8

    .line 762
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v11, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->createBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v3

    .line 763
    if-eqz v3, :cond_8

    .line 764
    const/4 v13, 0x2

    invoke-virtual {v3, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V

    goto :goto_3

    .line 769
    .end local v8    # "i":I
    :cond_b
    const/4 v10, 0x0

    .line 770
    .local v10, "parent":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v9

    .line 771
    const/16 v13, 0x5f

    invoke-virtual {v9, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 773
    .restart local v8    # "i":I
    move-object/from16 v0, p2

    invoke-static {v0, v7, v6, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->addToCache(Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/UResourceBundle;)V

    .line 775
    const/4 v13, -0x1

    if-eq v8, v13, :cond_d

    .line 776
    const/4 v13, 0x0

    invoke-virtual {v9, v13, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v13, v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v10

    .line 781
    :cond_c
    :goto_4
    invoke-virtual {v3, v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_8

    .line 782
    invoke-virtual {v3, v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->setParent(Ljava/util/ResourceBundle;)V

    goto :goto_3

    .line 777
    :cond_d
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_c

    .line 778
    const/4 v13, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v11, v1, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->instantiateBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Z)Lcom/ibm/icu/util/UResourceBundle;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    goto :goto_4
.end method

.method private static final makeChar([BI)C
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 1034
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    aget-byte v1, p0, p1

    shl-int/lit8 v1, v1, 0x8

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    int-to-char v1, v1

    return v1
.end method

.method private static final makeInt([BI)I
    .locals 3
    .param p0, "data"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 1041
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    aget-byte v1, p0, p1

    shl-int/lit8 v1, v1, 0x18

    add-int/lit8 p1, v0, 0x1

    .end local v0    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .restart local v0    # "offset":I
    aget-byte v2, p0, p1

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    return v1
.end method


# virtual methods
.method protected final createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 8
    .param p1, "_key"    # Ljava/lang/String;
    .param p2, "_resource"    # J
    .param p4, "_resPath"    # Ljava/lang/String;
    .param p5, "table"    # Ljava/util/HashMap;
    .param p6, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p7, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p8, "isAlias"    # [Z

    .prologue
    const/4 v1, 0x0

    .line 931
    if-eqz p8, :cond_0

    .line 932
    aput-boolean v1, p8, v1

    .line 935
    :cond_0
    invoke-static {p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundle;->RES_GET_TYPE(J)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 964
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The resource type is unknown"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 937
    :pswitch_1
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceString;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceString;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 961
    :goto_0
    return-object v1

    .line 940
    :pswitch_2
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceBinary;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 943
    :pswitch_3
    if-eqz p8, :cond_1

    .line 944
    const/4 v0, 0x1

    aput-boolean v0, p8, v1

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p5

    move-object v5, p6

    .line 946
    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->findResource(Ljava/lang/String;JLjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v1

    goto :goto_0

    .line 949
    :pswitch_4
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceInt;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 952
    :pswitch_5
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceIntVector;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 955
    :pswitch_6
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceArray;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 958
    :pswitch_7
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable32;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 961
    :pswitch_8
    new-instance v1, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V

    goto :goto_0

    .line 935
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected createLookupCache()V
    .locals 4

    .prologue
    .line 1178
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    const/4 v1, 0x1

    iget v2, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->size:I

    mul-int/lit8 v2, v2, 0x2

    const/16 v3, 0x40

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/impl/SimpleCache;-><init>(II)V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    .line 1179
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 693
    instance-of v1, p1, Lcom/ibm/icu/impl/ICUResourceBundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 694
    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 695
    .local v0, "o":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBaseName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBaseName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 697
    const/4 v1, 0x1

    .line 700
    .end local v0    # "o":Lcom/ibm/icu/impl/ICUResourceBundle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method findKey(IILcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)I
    .locals 10
    .param p1, "siz"    # I
    .param p2, "currentOffset"    # I
    .param p3, "res"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p4, "target"    # Ljava/lang/String;

    .prologue
    .line 982
    const/4 v4, 0x0

    .local v4, "mid":I
    const/4 v6, 0x0

    .local v6, "start":I
    move v3, p1

    .line 983
    .local v3, "limit":I
    const/4 v2, -0x1

    .line 985
    .local v2, "lastMid":I
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v8

    .line 992
    .local v8, "targetLength":I
    :goto_0
    add-int v9, v6, v3

    shr-int/lit8 v4, v9, 0x1

    .line 993
    if-ne v2, v4, :cond_1

    .line 1026
    const/4 v4, -0x1

    .end local v4    # "mid":I
    :cond_0
    return v4

    .line 996
    .restart local v4    # "mid":I
    :cond_1
    move v2, v4

    .line 998
    invoke-virtual {p3, p2, v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getOffset(II)I

    move-result v5

    .line 1001
    .local v5, "offset":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v8, :cond_5

    .line 1002
    invoke-virtual {p4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 1003
    .local v7, "targetChar":C
    iget-object v9, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    aget-byte v9, v9, v5

    int-to-char v0, v9

    .line 1004
    .local v0, "actualChar":C
    if-eqz v0, :cond_2

    if-le v7, v0, :cond_3

    .line 1006
    :cond_2
    move v6, v4

    .line 1007
    goto :goto_0

    .line 1009
    :cond_3
    if-ge v7, v0, :cond_4

    .line 1011
    move v3, v4

    .line 1012
    goto :goto_0

    .line 1015
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 1001
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1017
    .end local v0    # "actualChar":C
    .end local v7    # "targetChar":C
    :cond_5
    iget-object v9, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    aget-byte v9, v9, v5

    int-to-char v0, v9

    .line 1018
    .restart local v0    # "actualChar":C
    if-eqz v0, :cond_0

    .line 1020
    move v3, v4

    .line 1021
    goto :goto_0
.end method

.method get(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 5
    .param p1, "aKey"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 789
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundle;->handleGet(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 790
    .local v1, "obj":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-nez v1, :cond_1

    .line 791
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getParent()Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .end local v1    # "obj":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 792
    .restart local v1    # "obj":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-eqz v1, :cond_0

    .line 794
    invoke-virtual {v1, p1, p2, p3}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .end local v1    # "obj":Lcom/ibm/icu/impl/ICUResourceBundle;
    check-cast v1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 796
    .restart local v1    # "obj":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_0
    if-nez v1, :cond_1

    .line 797
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getBaseName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getFullName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 799
    .local v0, "fullName":Ljava/lang/String;
    new-instance v2, Ljava/util/MissingResourceException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Can\'t find resource for bundle "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ", key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 804
    .end local v0    # "fullName":Ljava/lang/String;
    :cond_1
    check-cast p3, Lcom/ibm/icu/impl/ICUResourceBundle;

    .end local p3    # "requested":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {p3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(Ljava/lang/String;)V

    .line 805
    return-object v1
.end method

.method protected getBaseName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->baseName:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadingStatus()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->loadingStatus:I

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method protected getLocaleID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->localeID:Ljava/lang/String;

    return-object v0
.end method

.method protected getNoFallback()Z
    .locals 1

    .prologue
    .line 862
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->noFallback:Z

    return v0
.end method

.method public getOffset(II)I
    .locals 1
    .param p1, "currentOfset"    # I
    .param p2, "index"    # I

    .prologue
    .line 1030
    const/4 v0, -0x1

    return v0
.end method

.method public getParent()Lcom/ibm/icu/util/UResourceBundle;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->parent:Ljava/util/ResourceBundle;

    check-cast v0, Lcom/ibm/icu/util/UResourceBundle;

    return-object v0
.end method

.method public getResPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->resPath:Ljava/lang/String;

    return-object v0
.end method

.method getStringValue(J)Ljava/lang/String;
    .locals 9
    .param p1, "res"    # J

    .prologue
    .line 1050
    const-wide/16 v6, 0x0

    cmp-long v5, p1, v6

    if-nez v5, :cond_0

    .line 1058
    const-string/jumbo v5, ""

    .line 1068
    :goto_0
    return-object v5

    .line 1060
    :cond_0
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/ICUResourceBundle;->RES_GET_OFFSET(J)I

    move-result v3

    .line 1061
    .local v3, "offset":I
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    invoke-static {v5, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v2

    .line 1062
    .local v2, "length":I
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getIntOffset(I)I

    move-result v5

    add-int v4, v3, v5

    .line 1063
    .local v4, "stringOffset":I
    new-array v0, v2, [C

    .line 1065
    .local v0, "dst":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 1066
    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->rawData:[B

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getCharOffset(I)I

    move-result v6

    add-int/2addr v6, v4

    invoke-static {v5, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getChar([BI)C

    move-result v5

    aput-char v5, v0, v1

    .line 1065
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1068
    :cond_1
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 355
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getULocale()Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->ulocale:Lcom/ibm/icu/util/ULocale;

    return-object v0
.end method

.method public getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 338
    const/4 v1, 0x0

    .line 339
    .local v1, "result":Lcom/ibm/icu/impl/ICUResourceBundle;
    move-object v0, p0

    .line 342
    .local v0, "actualBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcom/ibm/icu/impl/ICUResourceBundle;->findResourceWithFallback(Ljava/lang/String;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v1

    .line 344
    if-nez v1, :cond_0

    .line 345
    new-instance v2, Ljava/util/MissingResourceException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Can\'t find resource for bundle "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ", key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, p1, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 350
    :cond_0
    return-object v1
.end method

.method protected handleGet(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 5
    .param p1, "index"    # I
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 1200
    const/4 v2, 0x0

    .line 1201
    .local v2, "res":Lcom/ibm/icu/util/UResourceBundle;
    const/4 v1, 0x0

    .line 1202
    .local v1, "indexKey":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    if-eqz v3, :cond_0

    .line 1203
    invoke-static {p1}, Lcom/ibm/icu/impl/Utility;->integerValueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1204
    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v3, v1}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "res":Lcom/ibm/icu/util/UResourceBundle;
    check-cast v2, Lcom/ibm/icu/util/UResourceBundle;

    .line 1206
    .restart local v2    # "res":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    if-nez v2, :cond_1

    .line 1207
    const/4 v3, 0x1

    new-array v0, v3, [Z

    .line 1208
    .local v0, "alias":[Z
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    .line 1209
    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    aget-boolean v3, v0, v3

    if-nez v3, :cond_1

    .line 1211
    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    invoke-virtual {v2}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1212
    iget-object v3, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v3, v1, v2}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1215
    .end local v0    # "alias":[Z
    :cond_1
    return-object v2
.end method

.method protected handleGet(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 8
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 1182
    const/4 v6, 0x0

    .line 1183
    .local v6, "res":Lcom/ibm/icu/util/UResourceBundle;
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    if-eqz v0, :cond_0

    .line 1184
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v0, p1}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "res":Lcom/ibm/icu/util/UResourceBundle;
    check-cast v6, Lcom/ibm/icu/util/UResourceBundle;

    .line 1186
    .restart local v6    # "res":Lcom/ibm/icu/util/UResourceBundle;
    :cond_0
    if-nez v6, :cond_1

    .line 1187
    new-array v4, v1, [I

    .line 1188
    .local v4, "index":[I
    new-array v5, v1, [Z

    .local v5, "alias":[Z
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 1189
    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->handleGetImpl(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[I[Z)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    .line 1190
    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    if-eqz v0, :cond_1

    aget-boolean v0, v5, v7

    if-nez v0, :cond_1

    .line 1192
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v0, p1, v6}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1193
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->lookup:Lcom/ibm/icu/impl/ICUCache;

    aget v1, v4, v7

    invoke-static {v1}, Lcom/ibm/icu/impl/Utility;->integerValueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1196
    .end local v4    # "index":[I
    .end local v5    # "alias":[Z
    :cond_1
    return-object v6
.end method

.method protected handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 1
    .param p1, "index"    # I
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "isAlias"    # [Z

    .prologue
    .line 1227
    const/4 v0, 0x0

    return-object v0
.end method

.method protected handleGetImpl(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[I[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 1
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "index"    # [I
    .param p5, "isAlias"    # [Z

    .prologue
    .line 1221
    const/4 v0, 0x0

    return-object v0
.end method

.method public setLoadingStatus(I)V
    .locals 0
    .param p1, "newStatus"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->loadingStatus:I

    .line 105
    return-void
.end method

.method public setLoadingStatus(Ljava/lang/String;)V
    .locals 2
    .param p1, "requestedLocale"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocaleID()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "locale":Ljava/lang/String;
    const-string/jumbo v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V

    .line 126
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V

    goto :goto_0

    .line 124
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->setLoadingStatus(I)V

    goto :goto_0
.end method

.method protected setParent(Ljava/util/ResourceBundle;)V
    .locals 0
    .param p1, "parent"    # Ljava/util/ResourceBundle;

    .prologue
    .line 854
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundle;->parent:Ljava/util/ResourceBundle;

    .line 855
    return-void
.end method
