.class final Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;
.super Lcom/ibm/icu/impl/ICUResourceBundle;
.source "ICUResourceBundleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/ICUResourceBundleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ResourceTable"
.end annotation


# direct methods
.method constructor <init>(Lcom/ibm/icu/impl/ICUResourceBundleReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 8
    .param p1, "reader"    # Lcom/ibm/icu/impl/ICUResourceBundleReader;
    .param p2, "baseName"    # Ljava/lang/String;
    .param p3, "localeID"    # Ljava/lang/String;
    .param p4, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 221
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getData()[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    .line 222
    const-wide v0, 0xffffffffL

    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getRootResource()I

    move-result v3

    int-to-long v4, v3

    and-long/2addr v0, v4

    iput-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rootResource:J

    .line 223
    invoke-virtual {p1}, Lcom/ibm/icu/impl/ICUResourceBundleReader;->getNoFallback()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->noFallback:Z

    .line 224
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->baseName:Ljava/lang/String;

    .line 225
    iput-object p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->localeID:Ljava/lang/String;

    .line 226
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, p3}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 227
    iput-object p4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->loader:Ljava/lang/ClassLoader;

    .line 228
    const-string/jumbo v3, ""

    iget-wide v4, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rootResource:J

    iget-boolean v7, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->isTopLevel:Z

    move-object v1, p0

    move-object v6, v2

    invoke-virtual/range {v1 .. v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 229
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;)V
    .locals 9
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;

    .prologue
    .line 217
    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 218
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "resPath"    # Ljava/lang/String;
    .param p3, "resource"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p6, "isTopLevel"    # Z

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundle;-><init>()V

    .line 244
    invoke-virtual/range {p0 .. p6}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V

    .line 245
    return-void
.end method

.method private countItems()I
    .locals 4

    .prologue
    .line 212
    iget-wide v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resource:J

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->RES_GET_OFFSET(J)I

    move-result v0

    .line 213
    .local v0, "offset":I
    iget-object v2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    invoke-static {v2, v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getChar([BI)C

    move-result v1

    .line 214
    .local v1, "value":I
    return v1
.end method


# virtual methods
.method public getOffset(II)I
    .locals 2
    .param p1, "currentOffset"    # I
    .param p2, "index"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    invoke-static {p2}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getCharOffset(I)I

    move-result v1

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getChar([BI)C

    move-result v0

    return v0
.end method

.method protected handleGetImpl(ILjava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 14
    .param p1, "index"    # I
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "isAlias"    # [Z

    .prologue
    .line 195
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    if-le p1, v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 198
    :cond_0
    iget-wide v6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resource:J

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->RES_GET_OFFSET(J)I

    move-result v11

    .line 201
    .local v11, "offset":I
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getCharOffset(I)I

    move-result v0

    add-int v10, v11, v0

    .line 202
    .local v10, "currentOffset":I
    invoke-virtual {p0, v10, p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getOffset(II)I

    move-result v9

    .line 203
    .local v9, "betterOffset":I
    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    invoke-static {v0, v9}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->RES_GET_KEY([BI)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "itemKey":Ljava/lang/String;
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    iget v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    xor-int/lit8 v5, v5, -0x1

    and-int/lit8 v5, v5, 0x1

    add-int/2addr v0, v5

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getCharOffset(I)I

    move-result v0

    invoke-static {p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getIntOffset(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v10, v0

    .line 206
    const-wide v6, 0xffffffffL

    iget-object v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    invoke-static {v0, v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v0

    int-to-long v12, v0

    and-long v2, v6, v12

    .line 207
    .local v2, "resOffset":J
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->isTopLevel:Z

    const/4 v5, 0x1

    if-ne v0, v5, :cond_1

    move-object v4, v1

    .local v4, "path":Ljava/lang/String;
    :goto_0
    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object v7, p0

    move-object/from16 v8, p4

    .line 209
    invoke-virtual/range {v0 .. v8}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    return-object v0

    .line 207
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resPath:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method protected handleGetImpl(Ljava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;[I[Z)Lcom/ibm/icu/util/UResourceBundle;
    .locals 11
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "table"    # Ljava/util/HashMap;
    .param p3, "requested"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p4, "index"    # [I
    .param p5, "isAlias"    # [Z

    .prologue
    .line 166
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    if-gtz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 187
    :goto_0
    return-object v0

    .line 169
    :cond_0
    iget-wide v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resource:J

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->RES_GET_OFFSET(J)I

    move-result v10

    .line 172
    .local v10, "offset":I
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getCharOffset(I)I

    move-result v0

    add-int v9, v10, v0

    .line 175
    .local v9, "currentOffset":I
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    invoke-virtual {p0, v1, v9, p0, p1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->findKey(IILcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)I

    move-result v1

    aput v1, p4, v0

    .line 176
    const/4 v0, 0x0

    aget v0, p4, v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 180
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    :cond_1
    iget v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    iget v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    xor-int/lit8 v1, v1, -0x1

    and-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getCharOffset(I)I

    move-result v0

    const/4 v1, 0x0

    aget v1, p4, v1

    invoke-static {v1}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->getIntOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v9, v0

    .line 184
    const-wide v0, 0xffffffffL

    iget-object v5, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->rawData:[B

    invoke-static {v5, v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getInt([BI)I

    move-result v5

    int-to-long v6, v5

    and-long v2, v0, v6

    .line 185
    .local v2, "resOffset":J
    iget-boolean v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->isTopLevel:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    move-object v4, p1

    .local v4, "path":Ljava/lang/String;
    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p0

    move-object/from16 v8, p5

    .line 187
    invoke-virtual/range {v0 .. v8}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->createBundleObject(Ljava/lang/String;JLjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;[Z)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v0

    goto :goto_0

    .line 185
    .end local v4    # "path":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method initialize(Ljava/lang/String;Ljava/lang/String;JLcom/ibm/icu/impl/ICUResourceBundle;Z)V
    .locals 1
    .param p1, "resKey"    # Ljava/lang/String;
    .param p2, "resourcePath"    # Ljava/lang/String;
    .param p3, "resOffset"    # J
    .param p5, "bundle"    # Lcom/ibm/icu/impl/ICUResourceBundle;
    .param p6, "topLevel"    # Z

    .prologue
    .line 232
    if-eqz p5, :cond_0

    .line 233
    invoke-static {p0, p5}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->assign(Lcom/ibm/icu/impl/ICUResourceBundle;Lcom/ibm/icu/impl/ICUResourceBundle;)V

    .line 235
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->key:Ljava/lang/String;

    .line 236
    iput-wide p3, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resource:J

    .line 237
    iput-boolean p6, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->isTopLevel:Z

    .line 238
    invoke-direct {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->countItems()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->size:I

    .line 239
    iput-object p2, p0, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->resPath:Ljava/lang/String;

    .line 240
    invoke-virtual {p0}, Lcom/ibm/icu/impl/ICUResourceBundleImpl$ResourceTable;->createLookupCache()V

    .line 241
    return-void
.end method
